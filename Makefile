OPTIM?=1

CXXFLAGS += -std=c++14 -Wall -Wextra -ggdb -fno-omit-frame-pointer #-fsanitize=address

ifeq ($(OPTIM),1)
CXXFLAGS += -O2
else
CXXFLAGS += -O0
endif

.PHONY: all clean

all: accesstest accessprof

accesstest: accesstest.cpp
	g++ $(CXXFLAGS) -fno-ipa-icf -o accesstest -Ilibfibre/src -Llibfibre/src -Wl,-rpath=$(shell pwd)/libfibre/src accesstest.cpp -lfibre -pthread

accessprof: accessprof.cpp
	g++ $(CXXFLAGS) -o accessprof accessprof.cpp

clean:
	rm -f accesstest accessprof

/*
 * Prevent different, but functionally identical, phase functions from being merged into one: -fno-ipa-icf
 *
 * g++ -std=c++14 -fno-ipa-icf -O2 -ggdb -fno-omit-frame-pointer -o accesstest -I../deps/libfibre/src -L../deps/libfibre/src -Wl,-rpath=$(pwd)/../deps/libfibre/src accesstest.cpp -lfibre -pthread
 */

#include <sched.h>
#include <sys/mman.h>
#include <signal.h>
#include <getopt.h>

#include <unistd.h>
#include <vector>
#include <tuple>
#include <utility>
#include <numeric>
#include <algorithm>
#include <functional>
#include <mutex>
#include <memory>
#include <atomic>
#include <set>
#include <unordered_set>
#include <thread>
#include <regex>
#include <limits>

#include "PathBuffer.hpp"
#include "libfibre/fibre.h"

// TODO: make elemens volatile not value inside?

//#define DEBUG_LOOP
//#define DEBUG_TRANSITIONS

/* Constants */
#define MAX_NUM_PHASES 4
const size_t DEFAULT_NUM_SESSIONS = 1;
const size_t DEFAULT_SEED = std::mt19937::default_seed;
const unsigned int DEFAULT_TEST_DURATION_SEC = 1;

const int START_TEST_SIGNAL = SIGUSR1;
const char* const START_TEST_SIGNAL_STR = "SIGUSR1";
const char PHASE_LOCAL_CHAR = 'L';
const char PHASE_GLOBAL_CHAR = 'G';

#define MY_INT_REGEX "(?:[+-]?[0-9]+)"
#define MY_UINT_REGEX "(?:\\+?[0-9]+)"
#define MY_FLOAT_REGEX "(?:" MY_INT_REGEX "\\.?|(?:[+-]?[0-9]*\\.[0-9]+))"
// Access as in memory access: store vs. load, access count scalar
#define MY_LOCAL_CHAR_REGEX "(?:(" MY_UINT_REGEX ")(?:,(" MY_FLOAT_REGEX ")(?:,(" MY_FLOAT_REGEX "))?)?)"
const std::regex PHASE_LOCAL_REGEX( MY_LOCAL_CHAR_REGEX "/" MY_LOCAL_CHAR_REGEX);
#define MY_GLOBAL_PHASE_INDEX "(?:" MY_UINT_REGEX "(?:," MY_FLOAT_REGEX "," MY_FLOAT_REGEX "(?:," MY_FLOAT_REGEX "," MY_FLOAT_REGEX ")?)?)"
const std::regex PHASE_GLOBAL_REGEX("(" MY_UINT_REGEX ")/(" MY_UINT_REGEX "):(" MY_GLOBAL_PHASE_INDEX "(?:/" MY_GLOBAL_PHASE_INDEX ")+)?");
#undef MY_INT_REGEX
#undef MY_UINT_REGEX
#undef MY_FLOAT_REGEX
#undef MY_LOCAL_CHAR_REGEX
#undef MY_GLOBAL_PHASE_INDEX
#define MY_CPU_ELEM "(?:[0-9]+(?:-[0-9]+)?)"
const std::regex CPU_SET_REGEX(MY_CPU_ELEM "(?:," MY_CPU_ELEM ")*");
#undef MY_CPU_ELEM

const std::string SESSION_DISTR_UNIFORM_STR = "u";
const long SESSION_DISTR_UNIFORM = -1;
const long DEFAULT_SESSION_DISTR = 0;

const double DEFAULT_UPDATE_RATIO = 0.;
const double DOUBLE_INFINITY = std::numeric_limits<double>::infinity();

const std::string SESS_TYPE_THREAD_STR = "thread";
const std::string SESS_TYPE_FIBRE_STR = "fibre";

const int DEFAULT_MAX_LOG_LEVEL = 0;

const char* const SESSION_OUTPUT_ENVVAR = "SESSION_OUTPUT";

const double DEFAULT_ZIPF_SHAPE = 1.;

//==============================================================================

class TestManager;
class SessionManager;

struct Config {
    // Set by standard config options
    size_t numPhases;
    size_t numSessions;
    std::mt19937 rbg;
    unsigned int testDurationSec;
    bool signalStartTest;
    std::string sessionOutputFilepath;
    long sessionDistribution;

    // Set by session-mode specific config options
    std::set<int> allCPUs;
    std::shared_ptr<TestManager> testManager;
    std::shared_ptr<SessionManager> sessionManager;

    Config();
};

Config::Config()
: numPhases(0 /* placeholder */)
, numSessions(DEFAULT_NUM_SESSIONS)
, rbg(DEFAULT_SEED)
, testDurationSec(DEFAULT_TEST_DURATION_SEC)
, signalStartTest(false)
, sessionOutputFilepath("")
, sessionDistribution(DEFAULT_SESSION_DISTR)
{}

Config g_config; // made global for signal callback

//==============================================================================

void startSignalCallback(int);

//==============================================================================

using TimePoint = std::chrono::time_point<std::chrono::steady_clock>;

class DebugLogger {
private:
    const TimePoint _startTime;
    int _maxLogLevel;

    template <bool PrintPrefix, typename ...Args>
    int printfInternal(Args...args) {
        using namespace std::chrono;

        int rval = 0;

        if (PrintPrefix) {
            long curTimeMs = duration_cast<milliseconds>(steady_clock::now() - _startTime).count();
            rval = ::printf("[%.3lfs] ", (double)curTimeMs / 1000.);
            if (rval <= 0) return rval;
        }

        int rval2 = ::printf(args...);
        if (rval2 <= 0) return rval2; else rval += rval2;
        fflush(stdout);
        return rval;
    }

public:
    DebugLogger(int maxLogLevel=DEFAULT_MAX_LOG_LEVEL)
    : _startTime(std::chrono::steady_clock::now())
    , _maxLogLevel(maxLogLevel)
    {}

    void setMaxLogLevel(int logLevel) {
        _maxLogLevel = logLevel;
    }

    template <typename ...Args>
    int printf(int logLevel, Args...args) {
        if (checkLevel(logLevel)) return printfInternal<true>(args...);
        else return 0;
    }
    
    template <typename ...Args>
    int printfChain(int logLevel, Args...args) {
        if (checkLevel(logLevel)) return printfInternal<false>(args...);
        else return 0;
    }

    bool checkLevel(int logLevel) const {
        return logLevel <= _maxLogLevel;
    }
};

// Bypass "format not a string literal and no format arguments" warning.
template <>
int DebugLogger::printfInternal<true,const char*>(const char* format) {
    return printfInternal<true>("%s", format);
}
template <>
int DebugLogger::printfInternal<false,const char*>(const char* format) {
    return printfInternal<false>("%s", format);
}

DebugLogger g_logger;

//==============================================================================

/*
 * Notes:
 * Cacheable memory regions are contiguous memory regions that are both the size of a
 * cacheline and starting at a physical address divisible by a cacheline size.
 */

class TestManager;
class SessionManager;

using Element = MyElement<64>;
using ElemValue = typename Element::Value;
using BufferPtr = std::shared_ptr<Buffer>;
using ElemPathBuffer = PathBuffer<Element>;
using ElemPathBufferPtr = std::shared_ptr<ElemPathBuffer>;
using ElemPointerChasingPath = PointerChasingPath<Element>;
using ElemPointerChasingPathPtr = std::shared_ptr<ElemPointerChasingPath>;

using PhaseReturn = std::tuple<unsigned long long,uint8_t,ElemValue,bool,bool>;
using PhaseCallback = PhaseReturn (*)(Element*,Element*,uint8_t,ElemValue,bool,const size_t,uint8_t);
using PostPhaseCallback = void (*)(size_t,void*);
using LoopArg = std::tuple<size_t,Element*,Element*,ElemValue,ElemValue,uint8_t,const Config*>;
using LoopReturn = std::tuple<long long int,long long int>;

//==============================================================================

template <typename Iter>
int signalSetCreate(sigset_t& sigset, Iter sigBegin, Iter sigEnd) {
    int err = sigemptyset(&sigset);
    if (0 == err) {
        for (auto it = sigBegin; it != sigEnd; ++it) {
            err = sigaddset(&sigset, *it);
            if (err) break;
        }
    }
    return err;
}

template <typename Iter>
int signalBlock(Iter sigBegin, Iter sigEnd) {
    sigset_t sigset;
    int err = signalSetCreate(sigset, sigBegin, sigEnd);
    if (0 == err) {
        err = pthread_sigmask(SIG_BLOCK, &sigset, NULL);
    }
    return err;        
}

int signalBlock(int signal) {
    return signalBlock(&signal, &signal + 1);
}

template <typename Iter>
int signalUnblock(Iter sigBegin, Iter sigEnd) {
    sigset_t sigset;
    int err = signalSetCreate(sigset, sigBegin, sigEnd);
    if (0 == err) {
        err = pthread_sigmask(SIG_UNBLOCK, &sigset, NULL);
    }
    return err;        

}

int signalUnblock(int signal) {
    return signalUnblock(&signal, &signal + 1);
}

//==============================================================================

#define GEN_PHASE_FUNC_DEC(PHASE_INDEX) \
        PhaseReturn phase##PHASE_INDEX(Element* buffer0, Element* buffer1, uint8_t bufferIndex, ElemValue index,\
                                       bool isWrite, const size_t sessionIndex, uint8_t globalBufferIndex);

// Note: extern C used to disable name mangling
extern "C" {
    GEN_PHASE_FUNC_DEC(0)
    GEN_PHASE_FUNC_DEC(1)
    GEN_PHASE_FUNC_DEC(2)
    GEN_PHASE_FUNC_DEC(3)
}

PhaseCallback g_phaseCallbacks[] = {&phase0, &phase1, &phase2, &phase3};
static_assert(4 == MAX_NUM_PHASES, "Changing number of phases is not yet automated...");

//==============================================================================
struct AccessCharacteristics {
    float sessionLocalUpdateRatio;
    float sessionLocalAccessCountScalar;

    float sessionGlobalUpdateRatio;
    float sessionGlobalAccessCountScalar;
};

struct PhaseCharacteristics {
    size_t numSessionLocalDatalocs;
    size_t numSessionGlobalDatalocs;
};

struct PathCharacteristics {
    const std::vector<std::pair<PhaseCharacteristics,AccessCharacteristics>> phaseLocalCharacteristics;
    const std::vector<std::pair<PhaseCharacteristics,std::vector<std::pair<size_t,AccessCharacteristics>>>> phaseGlobalCharacteristics;

    PathCharacteristics(const std::vector<std::pair<PhaseCharacteristics,AccessCharacteristics>>& phaseLocalChars,
                        const std::vector<std::pair<PhaseCharacteristics,std::vector<std::pair<size_t,AccessCharacteristics>>>>& phaseGlobalChars)
    : phaseLocalCharacteristics(phaseLocalChars)
    , phaseGlobalCharacteristics(phaseGlobalChars)
    {
#ifndef NDEBUG
        assert(phaseLocalCharacteristics.size() > 0);
        for (const auto& phaseGlobalPair : phaseGlobalCharacteristics) {
            for (const auto& phasePair : phaseGlobalPair.second) {
                assert(phasePair.first < phaseLocalCharacteristics.size());
            }
        }
#endif
    }
};

struct TestReturn {
    std::vector<std::shared_ptr<LoopReturn>> sessionLoopReturns;
    TimePoint startTime;
    TimePoint endTimeMin;
    TimePoint endTimeMax;

    template <typename Returns>
    TestReturn(Returns&& returns,
               TimePoint start, TimePoint endMin, TimePoint endMax)
    : sessionLoopReturns(std::forward<Returns>(returns))
    , startTime(start)
    , endTimeMin(endMin)
    , endTimeMax(endMax)
    {}
};

/*
template <typename Friend> struct _friend {
private:
    friend Friend;
    _friend() {}
};
*/

//==============================================================================

template <typename It>
void setCPUSet(It begin, It end, cpu_set_t& cpuset) {
    CPU_ZERO(&cpuset);
    for (auto it = begin; it != end; ++it) {
        CPU_SET(*it, &cpuset);
    }
}

std::set<int> getCPUSet(const cpu_set_t& cpuset) {
    std::set<int> cpus;
    size_t cpusetSize = (size_t)CPU_COUNT(&cpuset);
    for (int cpu = 0; cpus.size() < cpusetSize; ++cpu) {
        if (CPU_ISSET(cpu, &cpuset)) {
            cpus.emplace(cpu);
        }
    }
    return cpus;
}

template <typename It>
void setThreadAffinity(pthread_t thread, It cpuBegin, It cpuEnd) {
    cpu_set_t cpuset;
    setCPUSet(cpuBegin, cpuEnd, cpuset);
    int err = pthread_setaffinity_np(thread, sizeof(cpuset), &cpuset);
    assert(0 == err);
}

//==============================================================================

#ifndef MAP_HUGE_2MB
    #define MAP_HUGE_2MB (21 << MAP_HUGE_SHIFT)
#endif
#ifndef MAP_HUGE_1GB
    #define MAP_HUGE_1GB (30 << MAP_HUGE_SHIFT)
#endif

template <typename Iter>
int __attribute__((optimize(0))) clearCPUCaches(Iter cpusBegin, Iter cpusEnd) {
    using BufType = uint64_t;
    const size_t length = 1lu << 30; // 1GB
    const int prot = (PROT_READ | PROT_WRITE);
    // NOTE TO SELF: may need to allocate default-sized hugepages (2 MB) even if using
    // non-default-sized hugepages (1 GB).
    const int flags = (MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB | MAP_HUGE_1GB);

    // Remember this thread's cpu affinity
    cpu_set_t cpusetOriginal;
    CPU_ZERO(&cpusetOriginal);
    int err = pthread_getaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpusetOriginal);
    assert(0 == err);

    // Allocate 1GB hugepage of memory to operate on, thereby clearing cache.
    // Use hugepage since contiguous physically addressed memory will ensure that each
    // cacheset gets wiped out.
    volatile BufType *myBuffer = (volatile BufType *)mmap(NULL, length, prot, flags, 0, 0);
    if (MAP_FAILED == myBuffer) {
        perror("Failed to allocate hugepage for clearing cache");
        return -1;
    }

    // Clear cache of each core that can run a worker thread
    for (auto it = cpusBegin; it != cpusEnd; ++it) {
        g_logger.printf(2, "Clearing cpu %d's cache.\n", *it);
        setThreadAffinity(pthread_self(), it, std::next(it));
        for (size_t i = 0; i < (length / sizeof(BufType)); ++i) {
            // Write garbage data into buffer.
            myBuffer[i] = ~(i | *it);
        }    
    }    

    // Free memory
    err = munmap((void *)myBuffer, length);
    assert(0 == err);

    // Set original affinity
    err = pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpusetOriginal);
    assert(0 == err);

    return 0;
}

//==============================================================================

class TestManager {
protected:
    const size_t _numPhases;
    const size_t _numSessions;
    const bool _signalStartTest;

    std::vector<PhaseCallback> _phaseCallbacks;
    std::vector<PostPhaseCallback> _postPhaseCallbacks;

    static void postPhaseCallbackNOP(size_t sessId, void*) {
#ifdef DEBUG_TRANSITIONS
        printf("sess%zu nop\n", sessId);
#else
        (void)sessId;
#endif
    }

public:
    TestManager(size_t numPhases, size_t numSessions, bool signalStartTest)
    : _numPhases(numPhases)
    , _numSessions(numSessions)
    , _signalStartTest(signalStartTest)
    {
        assert(_numPhases > 0 && _numSessions > 0);
        _phaseCallbacks.reserve(_numPhases);
        _postPhaseCallbacks.reserve(_numPhases);

        for (size_t i = 0; i < _numPhases; ++i) {
            _phaseCallbacks.emplace_back(g_phaseCallbacks[i]);
        }
    }

    virtual void waitUntilTestStart() = 0;
    virtual void waitUntilSessionsWaiting() = 0;
    virtual void setStartSignalReceived() = 0;
    virtual void announceTestStart() = 0;

    const PhaseCallback* getPhaseCallbacks() const {
        return _phaseCallbacks.data();
    }

    const PostPhaseCallback* getPostPhaseCallbacks() const {
        return _postPhaseCallbacks.data();
    }

    virtual void* const* getPostPhaseCallbackArgs(size_t sessionIndex) const = 0;
};

class ThreadTestManager : public TestManager {
private:
    std::vector<void*> _postPhaseCallbackArgs;
    std::atomic<size_t> _numThreadsWaiting;
    volatile sig_atomic_t _startSignalReceived;
    std::atomic<bool> _testHasStarted;

public:
    enum PostPhaseAction {
        NONE, NOP
    };

    ThreadTestManager(size_t numPhases, size_t numSessions, bool signalStartTest,
                      PostPhaseAction action = PostPhaseAction::NOP)
    : TestManager(numPhases, numSessions, signalStartTest)
    , _postPhaseCallbackArgs(_numPhases, nullptr)
    , _numThreadsWaiting(0)
    , _startSignalReceived(0)
    , _testHasStarted(false)
    {
        PostPhaseCallback callback = (PostPhaseAction::NOP == action ?
                                      postPhaseCallbackNOP : nullptr);
        _postPhaseCallbacks.resize(_numPhases, callback);

        // Block START_TEST_SIGNAL on current pthread so it gets inherited by all spawned threads.
        // Remove block after all pthreads are spawned.
        int err = signalBlock(START_TEST_SIGNAL);
        assert(0 == err);
    }

    void waitUntilTestStart() {
        _numThreadsWaiting += 1;
        while (!_testHasStarted);
    }


    void waitUntilSessionsWaiting() {
        while (_numThreadsWaiting < _numSessions);

        if (_signalStartTest) {
            // Unblock START_TEST_SIGNAL now that all pthreads have been spawned.
            int err = signalUnblock(START_TEST_SIGNAL);
            assert(0 == err);

            // Install signal handler
            struct sigaction act;
            memset(&act, 0, sizeof(act));
            act.sa_handler = startSignalCallback;
            err = signalSetCreate(act.sa_mask, &START_TEST_SIGNAL, &START_TEST_SIGNAL + 1);
            assert(0 == err);
            err = sigaction(START_TEST_SIGNAL, &act, NULL);
            assert(0 == err);
        }
    }

    void setStartSignalReceived() {
        _startSignalReceived = 1;
    }

    void announceTestStart() {
        if (_signalStartTest) {
            while (0 == _startSignalReceived);
        }
        _testHasStarted = true;
    }

    void* const* getPostPhaseCallbackArgs(size_t) const {
        return _postPhaseCallbackArgs.data();
    }
};

class FibreSessionManager;

class FibreTestManager : public TestManager {
private:
    std::vector<Cluster*> _clusters; // getting segfault when Cluster's descructor gets called
    std::vector<std::vector<size_t>> _clusterGroupSizes;
    std::vector<std::pair<size_t,size_t>> _phaseClusterGroupIndexPairs;

    std::vector<size_t> _sessionIndexMap;
    std::vector<std::vector<size_t>> _workerIndexLocalities;

    std::atomic<size_t> _numFibresWaiting;
    volatile sig_atomic_t _startSignalReceived;
    std::atomic<bool> _testHasStarted;
    size_t _numThreads;

    std::vector<std::vector<void*>> _postPhaseCallbackArgs;

    static void postPhaseCallbackYield(size_t sessId, void*);
    static void postPhaseCallbackYieldGlobal(size_t sessId, void*);
    static void postPhaseCallbackMigrateGroup(size_t sessId, void* arg);
    static void postPhaseCallbackMigrateCluster(size_t sessId, void* arg);

    // Sets everything but per-session post-phase callback args.
    struct Internal {}; // removes ambiguiuty between this and first public constructor
    template <typename URBG>
    FibreTestManager(Internal, size_t numSessions,
                     const std::vector<std::vector<size_t>>& clusterGroupSizes,
                     const std::vector<std::pair<size_t,size_t>>& phaseClusterGroupIndexPairs,
                     bool signalStartTest, URBG& rbg);

public:
    enum PostPhaseAction {
        NONE, NOP, YIELD, YIELD_GLOBAL /*, YIELD_FORCE */
    };
    struct Override {};

    template <typename URBG>
    FibreTestManager(size_t numSessions,
                     const std::vector<std::vector<size_t>>& clusterGroupSizes,
                     const std::vector<std::pair<size_t,size_t>>& phaseClusterGroupIndexPairs,
                     bool signalStartTest, URBG& rbg,
                     PostPhaseAction phaseEndAction = PostPhaseAction::NOP,
                     PostPhaseAction lastPhaseEndAction = PostPhaseAction::YIELD);

    // Used for possibly creating phases that loop back on themselves.
    // Override default and force specific post-phase action.
    template <typename URBG>
    FibreTestManager(size_t numSessions,
                     const std::vector<std::vector<size_t>>& clusterGroupSizes,
                     const std::vector<std::pair<size_t,size_t>>& phaseClusterGroupIndexPairs,
                     bool signalStartTest, URBG& rbg,
                     Override overrid, PostPhaseAction overrideAction = PostPhaseAction::YIELD);

    void waitUntilTestStart();
    void waitUntilSessionsWaiting();
    void setStartSignalReceived();
    void announceTestStart();
    void* const* getPostPhaseCallbackArgs(size_t sessionIndex) const;

    void setThreadCPUAffinity(const std::vector<std::vector<std::vector<std::set<int>>>>& clusterGroupWorkerCPUAffinitySets);
    std::tuple<Cluster*,size_t,size_t> getSessionAffinity(
            size_t sessionIndex, size_t phaseIndex, _friend<FibreSessionManager>);
};

void FibreTestManager::postPhaseCallbackYield(size_t sessId, void*) {
#ifdef DEBUG_TRANSITIONS
    printf("sess%zu yield\n", sessId);
#else
    (void)sessId;
#endif
    (void)Fibre::yield();
}

void FibreTestManager::postPhaseCallbackYieldGlobal(size_t sessId, void*) {
#ifdef DEBUG_TRANSITIONS
    printf("sess%zu yield global\n", sessId);
#else
    (void)sessId;
#endif
    (void)Fibre::yieldGlobal();
}

void FibreTestManager::postPhaseCallbackMigrateGroup(size_t sessId, void* arg) {
#ifdef DEBUG_TRANSITIONS
    printf("sess%zu migrate to processor 0x%zx\n", sessId, (uintptr_t)arg);
#else
    (void)sessId;
#endif
    Fibre::migrate(*(BaseProcessor*)arg);
}

void FibreTestManager::postPhaseCallbackMigrateCluster(size_t sessId, void* arg) {
#ifdef DEBUG_TRANSITIONS
    printf("sess%zu migrate to cluster 0x%zu\n", sessId, (uintptr_t)arg);
#else
    (void)sessId;
#endif
    Fibre::migrate(*(BaseProcessor*)arg);
}

template <typename URBG>
FibreTestManager::FibreTestManager(Internal, size_t numSessions,
                                   const std::vector<std::vector<size_t>>& clusterGroupSizes,
                                   const std::vector<std::pair<size_t,size_t>>& phaseClusterGroupIndexPairs,
                                   bool signalStartTest, URBG& rbg)
: TestManager(phaseClusterGroupIndexPairs.size(), numSessions, signalStartTest)
, _clusterGroupSizes(clusterGroupSizes)
, _phaseClusterGroupIndexPairs(phaseClusterGroupIndexPairs)
, _numFibresWaiting(0)
, _startSignalReceived(0)
, _testHasStarted(false)
, _numThreads(0)
{
    // Block START_TEST_SIGNAL on current pthread so it gets inherited by all spawned threads.
    // Remove block after all pthreads are spawned.
    int err = signalBlock(START_TEST_SIGNAL);
    assert(0 == err);

    FibreInit();

    // Created clusters and cluster groups described by structure param.
    assert(_clusterGroupSizes.size() > 0);
    _clusters.reserve(_clusterGroupSizes.size());
    for (const std::vector<size_t>& clusterGroupsSizes : _clusterGroupSizes) {
        assert(clusterGroupsSizes.size() > 0);
#ifndef NDBUG
        std::for_each(clusterGroupsSizes.begin(), clusterGroupsSizes.end(),
                      [](size_t v) { assert(v > 0); });
#endif
        _clusters.emplace_back(new Cluster());
        size_t numWorkers = std::accumulate(clusterGroupsSizes.begin(),
                                            clusterGroupsSizes.end(), 0);
        assert(numWorkers > 0);
        _numThreads += numWorkers;

        _clusters.back()->addWorkers(numWorkers);
        _clusters.back()->formWorkerGroups(clusterGroupsSizes);
        // TODO/FIX:
        // Cluster::addGroup() would probabaly be better to use but isn't fully implemented.
        // Notice that, in the Libfibre code, this function doesn't set the processor rings.
        // Also, make sure stealing only traverses processors in local group once.
    }
    assert(_numThreads > 0);


    if (_signalStartTest) {
        // Unblock START_TEST_SIGNAL now that all pthreads have been spawned.
        int err = signalUnblock(START_TEST_SIGNAL);
        assert(0 == err);
    }

#ifndef NDEBUG
    // Make sure that there aren't extraneous clusters or cluster groups.
    // NOTE: using set because there's no default hashing function for pairs.
    std::set<std::pair<size_t,size_t>> unusedClusterGroupIndexPairs;
    for (size_t i = 0; i < _clusterGroupSizes.size(); ++i) {
        const std::vector<size_t>& clusterGroupsSizes = _clusterGroupSizes[i];
        for (size_t j = 0; j < clusterGroupsSizes.size(); ++j) {
            unusedClusterGroupIndexPairs.emplace(i, j);
        }
    }
    for (const auto& indexPair : _phaseClusterGroupIndexPairs) {
        unusedClusterGroupIndexPairs.erase(indexPair);
    }
    assert(0 == unusedClusterGroupIndexPairs.size());
#endif

    // Make two passes:
    // First pass counts the total number of unique lists consisting of worker indices with which
    // a session can have locality. Second pass creates these lists.
    size_t numUniqueWorkerLocalities = 1;
    for (int i = 0; i < 2; ++i) {
        if (1 == i) _workerIndexLocalities.resize(numUniqueWorkerLocalities);

        for (size_t curPhaseIdx = 0; curPhaseIdx < _numPhases; ++curPhaseIdx) {
            size_t curClusterIdx, curGroupIdx;
            std::tie(curClusterIdx, curGroupIdx) = _phaseClusterGroupIndexPairs[curPhaseIdx];

            size_t prevPhaseIdx = (curPhaseIdx + _numPhases - 1) % _numPhases;
            size_t prevClusterIdx, prevGroupIdx;
            std::tie(prevClusterIdx, prevGroupIdx) = _phaseClusterGroupIndexPairs[prevPhaseIdx];

            if (curClusterIdx == prevClusterIdx && curGroupIdx == prevGroupIdx) {
                if (0 == i) {
                    if (curPhaseIdx > 0) {
                        // Since the workers for the current and previous phases are the same,
                        // we assume each fibre stays with the same worker. Therefore, there's only one
                        // possible worker a fibre can have locality with for the current phase: the
                        // worker of the previous phase.
                        numUniqueWorkerLocalities *= 1;
                    } else {
                        // Haven't yet accounted for the workers for the previous (i.e., last)
                        // phase. Account for them here.
                        numUniqueWorkerLocalities *= _clusterGroupSizes[prevClusterIdx][prevGroupIdx];;
                    }
                } else {
                    // Use the worker index of the previous phase for this phase.
                    if (curPhaseIdx > 0) {
                        for (size_t j = 0; j < _workerIndexLocalities.size(); ++j) {
                            _workerIndexLocalities[j].emplace_back(_workerIndexLocalities[j].back());
                        }
                    } else {
                        size_t prevNumWorkers = _clusterGroupSizes[prevClusterIdx][prevGroupIdx];;
                        for (size_t j = 0; j < _workerIndexLocalities.size(); ++j) {
                            _workerIndexLocalities[j].emplace_back(j % prevNumWorkers);
                        }
                    }
                }
            } else {
                if (0 == i) {
                    // Since the cluster group is different for the previous and current phases,
                    // each fibre can migrate from any worker of the previous phase to any worker
                    // of the current phase.
                    numUniqueWorkerLocalities *= _clusterGroupSizes[curClusterIdx][curGroupIdx];
                } else {
                    size_t curNumWorkers = _clusterGroupSizes[curClusterIdx][curGroupIdx];
                    for (size_t j = 0; j < _workerIndexLocalities.size(); ++j) {
                        _workerIndexLocalities[j].emplace_back(j % curNumWorkers);
                    }
                }
            }
        }
    }

    // Associate each session with a worker index locality listing.
    _sessionIndexMap.resize(_numSessions);
    for (size_t sessIdx = 0; sessIdx < _numSessions; ++sessIdx) {
        _sessionIndexMap[sessIdx] = sessIdx % _workerIndexLocalities.size();
    }

    // Shuffle localities to prevent weird behavior caused by the cyclical nature of session assignment.
    std::shuffle(_sessionIndexMap.begin(), _sessionIndexMap.end(), rbg);
}

template <typename URBG>
FibreTestManager::FibreTestManager(size_t numSessions,
                                   const std::vector<std::vector<size_t>>& clusterGroupSizes,
                                   const std::vector<std::pair<size_t,size_t>>& phaseClusterGroupIndexPairs,
                                   bool signalStartTest, URBG& rbg,
                                   PostPhaseAction phaseEndAction, PostPhaseAction lastPhaseEndAction)
: FibreTestManager(Internal(), numSessions, clusterGroupSizes, phaseClusterGroupIndexPairs,
                   signalStartTest, rbg)
{
    // Determine the appropriate post-phase callback and all possible unique args to the callback.
    _postPhaseCallbackArgs.resize(_workerIndexLocalities.size());
    for (size_t curPhaseIdx = 0; curPhaseIdx < _numPhases; ++curPhaseIdx) {
        size_t curClusterIdx, curGroupIdx;
        std::tie(curClusterIdx, curGroupIdx) = _phaseClusterGroupIndexPairs[curPhaseIdx];
        size_t nextPhaseIdx = (curPhaseIdx + 1) % _numPhases;
        size_t nextClusterIdx, nextGroupIdx;
        std::tie(nextClusterIdx, nextGroupIdx) = _phaseClusterGroupIndexPairs[nextPhaseIdx];

        _postPhaseCallbacks.emplace_back();
        PostPhaseCallback& callback = _postPhaseCallbacks.back();

        if (curClusterIdx == nextClusterIdx && curGroupIdx == nextGroupIdx) {
            PostPhaseAction action = (curPhaseIdx + 1 != _numPhases ? phaseEndAction : lastPhaseEndAction);
            switch (action) {
                case PostPhaseAction::NONE:
                    callback = nullptr;
                    break;
                case PostPhaseAction::NOP:
                    callback = postPhaseCallbackNOP;
                    break;
                case PostPhaseAction::YIELD:
                    callback = postPhaseCallbackYield;
                    break;
                case PostPhaseAction::YIELD_GLOBAL:
                    callback = postPhaseCallbackYieldGlobal;
                    break;
            }
            for (auto& args : _postPhaseCallbackArgs) {
                args.emplace_back(nullptr);
            }
        } else {
            callback = (curClusterIdx == nextClusterIdx ? postPhaseCallbackMigrateGroup : postPhaseCallbackMigrateCluster);
            for (size_t i = 0; i < _postPhaseCallbackArgs.size(); ++i) {
                size_t nextWorkerIdx = _workerIndexLocalities[i][nextPhaseIdx];
                auto worker = &_clusters[nextClusterIdx]->getGroupWorker(nextGroupIdx, nextWorkerIdx);
                _postPhaseCallbackArgs[i].emplace_back((void*)(BaseProcessor*)worker);
            }
        }
    }
}

template <typename URBG>
FibreTestManager::FibreTestManager(size_t numSessions,
                                   const std::vector<std::vector<size_t>>& clusterGroupSizes,
                                   const std::vector<std::pair<size_t,size_t>>& phaseClusterGroupIndexPairs,
                                   bool signalStartTest, URBG& rbg,
                                   Override, PostPhaseAction overrideAction)
: FibreTestManager(Internal(), numSessions, clusterGroupSizes, phaseClusterGroupIndexPairs,
                   signalStartTest, rbg)
{
    PostPhaseCallback callback = nullptr;
    switch (overrideAction) {
        case PostPhaseAction::NONE:
            callback = nullptr;
            break;
        case PostPhaseAction::NOP:
            callback = postPhaseCallbackNOP;
            break;
        case PostPhaseAction::YIELD:
            callback = postPhaseCallbackYield;
            break;
        case PostPhaseAction::YIELD_GLOBAL:
            callback = postPhaseCallbackYieldGlobal;
            break;
    }
    _postPhaseCallbacks.resize(_numPhases, callback);
    _postPhaseCallbackArgs.resize(_workerIndexLocalities.size(),
                                  std::vector<void*>(_numPhases, nullptr));
}

void FibreTestManager::waitUntilTestStart() {
    _numFibresWaiting += 1;
    while (!_testHasStarted) {
        Fibre::yield();
    }
}

void FibreTestManager::waitUntilSessionsWaiting() {
    while (_numFibresWaiting < _numSessions);

    if (_signalStartTest) {
        // Install signal handler
        struct sigaction act;
        memset(&act, 0, sizeof(act));
        act.sa_handler = startSignalCallback;
        int err = signalSetCreate(act.sa_mask, &START_TEST_SIGNAL, &START_TEST_SIGNAL + 1);
        assert(0 == err);
        err = sigaction(START_TEST_SIGNAL, &act, NULL);
        assert(0 == err);
    }
}

void FibreTestManager::setStartSignalReceived() {
    _startSignalReceived = 1;
}

void FibreTestManager::announceTestStart() {
    if (_signalStartTest) {
        while (0 == _startSignalReceived);
    }
    _testHasStarted = true;
}

void* const* FibreTestManager::getPostPhaseCallbackArgs(size_t sessionIndex) const {
    assert(sessionIndex < _numSessions);
    return _postPhaseCallbackArgs[_sessionIndexMap[sessionIndex]].data();
}

std::tuple<Cluster*,size_t,size_t> FibreTestManager::getSessionAffinity(
        size_t sessionIndex, size_t phaseIndex, _friend<FibreSessionManager>)
{
    assert(sessionIndex < _numSessions);
    assert(phaseIndex < _numPhases);

    size_t clusterIdx, groupIdx;
    std::tie(clusterIdx, groupIdx) = _phaseClusterGroupIndexPairs[phaseIndex];
    size_t workerIdx = _workerIndexLocalities[_sessionIndexMap[sessionIndex]][phaseIndex];
    return std::make_tuple(_clusters[clusterIdx], groupIdx, workerIdx);
}

void FibreTestManager::setThreadCPUAffinity(const std::vector<std::vector<std::vector<std::set<int>>>>& clusterGroupWorkerCPUAffinitySets) {
    assert(clusterGroupWorkerCPUAffinitySets.size() == _clusters.size());
#ifndef NDEBUG
    std::for_each(clusterGroupWorkerCPUAffinitySets.begin(),
                  clusterGroupWorkerCPUAffinitySets.end(),
                  [](const auto& groupWorkerCPUAffSets) {
                      assert(groupWorkerCPUAffSets.size() > 0);
                      std::for_each(groupWorkerCPUAffSets.begin(), groupWorkerCPUAffSets.end(),
                                    [](const auto& cpuAffSet) {
                                        assert(cpuAffSet.size() > 0);
                                    });
                  });
#endif
    for (size_t clusterIdx = 0; clusterIdx < _clusters.size(); ++clusterIdx) {
        Cluster& cluster = *_clusters[clusterIdx];
        const std::vector<size_t>& groupSizes = _clusterGroupSizes[clusterIdx];
        const std::vector<std::vector<std::set<int>>>& groupWorkerCPUAffSets =
                clusterGroupWorkerCPUAffinitySets[clusterIdx % clusterGroupWorkerCPUAffinitySets.size()];
        for (size_t groupIdx = 0; groupIdx < groupSizes.size(); ++groupIdx) {
            size_t groupSize = groupSizes[groupIdx];
            const std::vector<std::set<int>>& workerCPUAffSets =
                    groupWorkerCPUAffSets[groupIdx % groupWorkerCPUAffSets.size()];
            for (size_t workerIdx = 0; workerIdx < groupSize; ++workerIdx) {
                const std::set<int>& cpuAffSet = workerCPUAffSets[workerIdx];
                pthread_t thread;
                // TODO/FIX: current release of Libfibre doesn't make it easy to get a group's system ids.
                size_t count = cluster.getGroupWorkerSysID(groupIdx, workerIdx, &thread);
                assert(1 == count);
                setThreadAffinity(thread, cpuAffSet.begin(), cpuAffSet.end());

                if (g_logger.checkLevel(2)) {
                    g_logger.printf(2, "Pinning worker %zu:%zu:%zu (%x) to cpus:",
                                    clusterIdx, groupIdx, workerIdx,
                                    (unsigned int)thread);
                    auto it = cpuAffSet.begin();
                    g_logger.printfChain(2, "%d", *it);
                    for (++it; it != cpuAffSet.end(); ++it) g_logger.printfChain(2, ",%d", *it);
                    g_logger.printfChain(2, "\n");
                }
            }
        }
    }
}

//==============================================================================

class SessionManager {
protected:
    const size_t _maxSessions;
    std::FILE* _stackOutputFile;

    static void printStack(std::FILE* file, void* stackaddr, size_t stacksize);

public:
    SessionManager(size_t maxSessions)
    : _maxSessions(maxSessions)
    , _stackOutputFile(nullptr)
    {}

    ~SessionManager() {
        if (nullptr != _stackOutputFile) {
            std::fclose(_stackOutputFile);
            _stackOutputFile = nullptr;
        }
    }

    void setSessionOutput(const std::string& sessOutputFilepath);

    virtual size_t sessionStart(void* (*callback)(void*), void* arg, size_t phaseIndex) = 0;
    virtual size_t getMySessionIndex() = 0;
    virtual void* sessionJoin(size_t sessionIndex) = 0;
};

void SessionManager::printStack(std::FILE* file, void* stackaddr, size_t stacksize) {
    // Note: format matches that printed by Libfibre and that expected by session extractor.
    fprintf(file, "\nStack:0x%lx,0x%lx\n", (unsigned long)stackaddr, (unsigned long)stacksize);
}

void SessionManager::setSessionOutput(const std::string& sessOutputFilepath) {
    _stackOutputFile = std::fopen(sessOutputFilepath.c_str(), "w");
}

//==============================================================================

class ThreadSessionManager : public SessionManager {
private:
    std::mutex _lock;
    size_t _nextSessionIndex;
    std::unordered_map<pthread_t,size_t> _threadIndexMap;
    std::unordered_map<size_t,pthread_t> _indexThreadMap;
    std::vector<std::set<int>> _sessionCPUAffinitySets;

public:
    ThreadSessionManager(size_t maxSessions)
    : SessionManager(maxSessions)
    , _nextSessionIndex(0)
    {
        _threadIndexMap.reserve(_maxSessions);
        _indexThreadMap.reserve(_maxSessions);
    }

    ~ThreadSessionManager() {
        std::lock_guard<std::mutex> lockguard(_lock);
        assert(0 == _threadIndexMap.size());
        assert(0 == _indexThreadMap.size());
    }

    size_t sessionStart(void* (*callback)(void*), void* arg, size_t phaseIndex);
    size_t getMySessionIndex();
    void* sessionJoin(size_t sessionIndex);

    void setThreadCPUAffinity(const std::vector<std::set<int>>& sessionCPUAffinitySets);
};

size_t ThreadSessionManager::sessionStart(void* (*callback)(void*), void* arg, size_t) {
    std::lock_guard<std::mutex> lockguard(_lock);

    size_t sessionIndex = _nextSessionIndex++;
    assert(sessionIndex < _maxSessions);

    pthread_t thread;
    int err = pthread_create(&thread, NULL, callback, arg);
    assert(0 == err);

    // Print stack for profiling
    if (nullptr != _stackOutputFile) {
        pthread_attr_t attr;
        err = pthread_getattr_np(thread, &attr);
        assert(0 == err);
        void* ptr;
        size_t size;
        err = pthread_attr_getstack(&attr, &ptr, &size);
        assert(0 == err);
        printStack(_stackOutputFile, ptr, size);
    }

    _threadIndexMap.emplace(thread, sessionIndex);
    _indexThreadMap.emplace(sessionIndex, thread);

    if (_sessionCPUAffinitySets.size() > 0) {
        std::set<int>& cpus = _sessionCPUAffinitySets[sessionIndex % _sessionCPUAffinitySets.size()];
        setThreadAffinity(thread, cpus.begin(), cpus.end());
        if (g_logger.checkLevel(2)) {
            g_logger.printf(2, "Pinning session %zu (%x) to cpus:",
                            sessionIndex, (unsigned int)thread);
            auto it = cpus.begin();
            g_logger.printfChain(2, "%d", *it);
            for (++it; it != cpus.end(); ++it) g_logger.printfChain(2, ",%d", *it);
            g_logger.printfChain(2, "\n");
        }
    }

    return sessionIndex;
}

size_t ThreadSessionManager::getMySessionIndex() {
    std::lock_guard<std::mutex>  lockguard(_lock);
    auto it = _threadIndexMap.find(pthread_self());
    assert(_threadIndexMap.end() != it);
    return it->second;
}

void* ThreadSessionManager::sessionJoin(size_t sessionIndex) {
    _lock.lock();
    auto it = _indexThreadMap.find(sessionIndex);
    assert(_indexThreadMap.end() != it);
    pthread_t thread = it->second;
    _lock.unlock();

    void* retval;
    int err = pthread_join(thread, &retval);
    assert(0 == err);

    std::lock_guard<std::mutex> lockguard(_lock);
    _threadIndexMap.erase(thread);
    _indexThreadMap.erase(sessionIndex);

    return retval;
}

void ThreadSessionManager::setThreadCPUAffinity(const std::vector<std::set<int>>& sessionCPUAffinitySets) {
    std::lock_guard<std::mutex> lockguard(_lock);
    _sessionCPUAffinitySets = sessionCPUAffinitySets;
#ifndef NDEBUG
    std::for_each(_sessionCPUAffinitySets.begin(), _sessionCPUAffinitySets.end(),
                  [](const auto& set) { assert(set.size() > 0); });
#endif
}

//==============================================================================

class FibreSessionManager : public SessionManager {
private:
    std::shared_ptr<FibreTestManager> _testManager;
    std::mutex _lock;
    size_t _nextSessionIndex;
    std::unordered_map<Fibre*,size_t> _fibreIndexMap;
    std::unordered_map<size_t,Fibre*> _indexFibreMap;

public:
    FibreSessionManager(size_t maxSessions, const std::shared_ptr<FibreTestManager>& testManager)
    : SessionManager(maxSessions)
    , _testManager(testManager)
    , _nextSessionIndex(0)
    {
        _fibreIndexMap.reserve(_maxSessions);
        _indexFibreMap.reserve(_maxSessions);
    }

    ~FibreSessionManager() {
        std::lock_guard<std::mutex> lockguard(_lock);
        assert(0 == _fibreIndexMap.size());
        assert(0 == _indexFibreMap.size());
    }

    size_t sessionStart(void* (*callback)(void*), void* arg, size_t phaseIndex);
    size_t getMySessionIndex();
    void* sessionJoin(size_t sessionIndex);
};

size_t FibreSessionManager::sessionStart(void* (*callback)(void*), void* arg, size_t phaseIndex) {
    std::lock_guard<std::mutex> lockguard(_lock);

    size_t sessionIndex = _nextSessionIndex++;
    assert(sessionIndex < _maxSessions);

    Cluster* cluster;
    size_t groupIdx, workerIdx;
    std::tie(cluster, groupIdx, workerIdx) =
            _testManager->getSessionAffinity(sessionIndex, phaseIndex,
                                             _friend<FibreSessionManager>());
    Fibre* fibre = new Fibre(groupIdx, workerIdx, *cluster);

    // Print stack for profiling
    if (nullptr != _stackOutputFile) {
        void* ptr;
        size_t size;
        int err = fibre->getstack(&ptr, &size);
        assert(0 == err);
        printStack(_stackOutputFile, ptr, size);
    }

    _fibreIndexMap[fibre] = sessionIndex;
    _indexFibreMap[sessionIndex] = fibre;
    fibre->run(callback, arg);

    return sessionIndex;
}

size_t FibreSessionManager::getMySessionIndex() {
    std::lock_guard<std::mutex> lockguard(_lock);
    auto it = _fibreIndexMap.find(CurrFibre());
    assert(_fibreIndexMap.end() != it);
    return it->second;
}

void* FibreSessionManager::sessionJoin(size_t sessionIndex) {
    _lock.lock();
    auto it = _indexFibreMap.find(sessionIndex);
    assert(_indexFibreMap.end() != it);
    Fibre* thread = it->second;
    _lock.unlock();

    void* retval = thread->join();

    std::lock_guard<std::mutex> lockguard(_lock);
    _fibreIndexMap.erase(thread);
    _indexFibreMap.erase(sessionIndex);
    delete thread;

    return retval;
}

//==============================================================================

void startSignalCallback(int) {
    g_config.testManager->setStartSignalReceived();
}

//==============================================================================

#ifdef DEBUG_LOOP
    #define LOG_LOOP_PHASE_START(sessionIndex, PHASE_INDEX, elem, globalBufferIndex) \
            if (!elem.isFinished())\
                printf("sess%zu phase%d start - %c:%u\n",\
                       sessionIndex, PHASE_INDEX,\
                       (globalBufferIndex == elem.getBufferIndex() ? PHASE_GLOBAL_CHAR : PHASE_LOCAL_CHAR),\
                        elem.getIndex());\
            else\
                printf("sess%zu phase%d start - FINISHED\n",\
                       sessionIndex, PHASE_INDEX)
            
    #define LOG_LOOP_ACCESS(sessionIndex, PHASE_INDEX, prevElem, elem, globalBufferIndex) \
            if (!elem.isFinished())\
                printf("sess%zu phase%d - %c:%u to %c:%u\n",\
                       sessionIndex, PHASE_INDEX,\
                       (prevElem.getBufferIndex() == globalBufferIndex ? PHASE_GLOBAL_CHAR : PHASE_LOCAL_CHAR),\
                       prevElem.getIndex(),\
                       (elem.getBufferIndex() == globalBufferIndex ? PHASE_GLOBAL_CHAR : PHASE_LOCAL_CHAR),\
                       elem.getIndex());\
            else\
                printf("sess%zu phase%d - %u:%u to FINISHED\n",\
                       sessionIndex, PHASE_INDEX, prevElem.getBufferIndex(), prevElem.getIndex())
    #define LOG_LOOP_START(sessionIndex, PHASE_INDEX) \
            printf("sess%zu - loop start - phase%zu\n", sessionIndex, PHASE_INDEX)

    #define LOG_LOOP_PHASE_CHANGE(sessionIndex, prevPhaseIndex, nextPhaseIndex) \
            printf("sess%zu - phase%zu to phase%zu\n",\
                   sessionIndex, prevPhaseIndex, nextPhaseIndex)
#else
    #define LOG_LOOP_PHASE_START(sessionIndex, PHASE_INDEX, elem, globalBufferIndex) \
            do { (void)sessionIndex; (void)PHASE_INDEX; (void)elem; (void)globalBufferIndex; } while (0)
    #define LOG_LOOP_ACCESS(sessionIndex, PHASE_INDEX, prevElem, elem, globalBufferIndex) \
            do { (void)sessionIndex; (void)PHASE_INDEX; (void)prevElem; (void)elem; (void)globalBufferIndex; } while (0)
    #define LOG_LOOP_START(sessionIndex, PHASE_INDEX) \
            do { (void)sessionIndex; (void)PHASE_INDEX; } while(0)
    #define LOG_LOOP_PHASE_CHANGE(sessionIndex, prevPhaseIndex, nextPhaseIndex) \
            do { (void)sessionIndex; (void)prevPhaseIndex; (void)nextPhaseIndex; } while (0)
#endif

#define GEN_PHASE_FUNC_DEF(PHASE_INDEX) \
        PhaseReturn phase##PHASE_INDEX(Element* buffer0, Element* buffer1, uint8_t bufferIndex, ElemValue index,\
                                       bool isWrite, const size_t sessionIndex, uint8_t globalBufferIndex)\
        {\
            unsigned long long numAccesses = 0;\
            bool finished = false;\
            Element prevElem;\
            Element* ptr = &(0 == bufferIndex ? buffer0 : buffer1)[index];\
            Element elem = (isWrite ? ptr->dummyWrite<false/*atomic*/>() : *ptr);\
            numAccesses += 1;\
            LOG_LOOP_PHASE_START(sessionIndex, PHASE_INDEX, elem, globalBufferIndex);\
            do {\
                if (elem.isFinished()) {\
                    finished = true;\
                    break;\
                }\
                bufferIndex = (uint8_t)elem.getBufferIndex();\
                index = elem.getIndex();\
                isWrite = elem.isWrite();\
                if (elem.isPhaseEnd()) break;\
                ptr = &(0 == bufferIndex ? buffer0 : buffer1)[index];\
                prevElem = elem;\
                elem = (isWrite ? ptr->dummyWrite<false/*atomic*/>() : *ptr);\
                numAccesses += 1;\
                LOG_LOOP_ACCESS(sessionIndex, PHASE_INDEX, prevElem, elem, globalBufferIndex);\
            } while (true);\
            return PhaseReturn(numAccesses, bufferIndex, index, isWrite, finished);\
        }

// TODO: make preprocessor function for repeating things like BOOST_PP_REPEAT
// Note: extern C used to disable name mangling
extern "C" {
    GEN_PHASE_FUNC_DEF(0)
    GEN_PHASE_FUNC_DEF(1)
    GEN_PHASE_FUNC_DEF(2)
    GEN_PHASE_FUNC_DEF(3)
}
static_assert(4 == MAX_NUM_PHASES, "Changing number of phases is not yet automated...");


// NOTE: Can I have the session manager handle packing and unpacking the arguments?
void *loop(void *voidArg) {
    LoopArg& arg = *(LoopArg*)voidArg;
    size_t phaseIndex = std::get<0>(arg);
    Element* const buffer0 = std::get<1>(arg);
    Element* const buffer1 = std::get<2>(arg);
    uint8_t bufferIndex = (uint8_t)std::get<3>(arg);
    ElemValue index = std::get<4>(arg);
    uint8_t globalBufferIndex = std::get<5>(arg);
    const Config* config = std::get<6>(arg);

    unsigned long long numAccesses = 0;
    unsigned long long numPhasesAccessed = 0;
    bool isWrite = false;
    const size_t sessionIndex = config->sessionManager->getMySessionIndex();
    const size_t numPhases = config->numPhases;
    const PhaseCallback* const phaseCallbacks = config->testManager->getPhaseCallbacks();
    const PostPhaseCallback* const postPhaseCallbacks = config->testManager->getPostPhaseCallbacks();
    void* const* const postPhaseCallbackArgs = config->testManager->getPostPhaseCallbackArgs(sessionIndex);

    config->testManager->waitUntilTestStart();

    LOG_LOOP_START(sessionIndex, phaseIndex);
    while (true) {
        unsigned long long numPhaseAccesses;
        bool finished;
        std::tie(numPhaseAccesses, bufferIndex, index, isWrite, finished) =
                phaseCallbacks[phaseIndex](buffer0, buffer1, bufferIndex, index, isWrite, sessionIndex, globalBufferIndex);
        numAccesses += numPhaseAccesses;
        numPhasesAccessed += 1;

        if (finished) {
            break;
        } else if (nullptr != postPhaseCallbacks[phaseIndex]) {
            postPhaseCallbacks[phaseIndex](sessionIndex, postPhaseCallbackArgs[phaseIndex]);
        }

        size_t nextPhaseIndex = phaseIndex + 1;
        if (nextPhaseIndex >= numPhases) {
            nextPhaseIndex = 0;
        }
        LOG_LOOP_PHASE_CHANGE(sessionIndex, phaseIndex, nextPhaseIndex);
        phaseIndex = nextPhaseIndex;
    }

    return (void*)new LoopReturn(numAccesses, numPhasesAccessed);
}

//==============================================================================

// TODO: add code to reconnect start and end of a phase if a session is only looping over that phase.
std::tuple<std::vector<ElemPointerChasingPathPtr>,std::vector<std::pair<ElemValue,ElemValue>>,uint8_t>
        createPaths(const PathCharacteristics& pathChars, Config& config)
{
    using PhaseIndex = size_t; // to distinguish between size and index
    enum SessionState : int {
        GLOBAL = 0, LOCAL = 1
    };
    auto toString = [](int state) -> const char* {
        return (GLOBAL == state ? "global" : "local");
    };

    struct AccessChars {
        double accessCountScalar;
        double updateRatio;
        double accessCountPercent;
    };
    using AccessCharsPtr = std::shared_ptr<AccessChars>;

    assert(pathChars.phaseLocalCharacteristics.size() == config.numPhases);
    const size_t numPhases = config.numPhases;

    // Log input.
    if (g_logger.checkLevel(1)) {
        g_logger.printf(1, "Creating path with %zu phases.\n", numPhases);
        for (size_t phaseIndex = 0; phaseIndex < numPhases; ++phaseIndex) {
            const PhaseCharacteristics& phaseChars = pathChars.phaseLocalCharacteristics[phaseIndex].first;
            const AccessCharacteristics& accessChars = pathChars.phaseLocalCharacteristics[phaseIndex].second;
            g_logger.printfChain(1, "  Phase %zu:\n"
                                 "    %zu sess-local datalocs, %lf update ratio, %lf access count scalar /\n"
                                 "    %zu sess-global datalocs, %lf update ratio, %lf access count scalar\n",
                                 phaseIndex,
                                 phaseChars.numSessionLocalDatalocs, accessChars.sessionLocalUpdateRatio,
                                 accessChars.sessionLocalAccessCountScalar, phaseChars.numSessionGlobalDatalocs,
                                 accessChars.sessionGlobalUpdateRatio, accessChars.sessionGlobalAccessCountScalar);
        }
        for (const auto& phaseGlobalPair : pathChars.phaseGlobalCharacteristics) {
            g_logger.printf(1, "Incorportation %zu sess-local & %zu sess-global phase-global datalocs across phases:\n",
                            phaseGlobalPair.first.numSessionLocalDatalocs,
                            phaseGlobalPair.first.numSessionGlobalDatalocs);
            for (const auto& accessingPhase : phaseGlobalPair.second) {
                PhaseIndex phaseIndex = accessingPhase.first;
                const AccessCharacteristics& accessChars = accessingPhase.second;
                g_logger.printfChain(1, "  Phase %zu:\n"
                                     "    sess-local - %lf update ratio, %lf access count scalar /\n"
                                     "    sess-global - %lf update ratio, %lf access count scalar\n",
                                     phaseIndex,
                                     accessChars.sessionLocalUpdateRatio, accessChars.sessionLocalAccessCountScalar,
                                     accessChars.sessionGlobalUpdateRatio, accessChars.sessionGlobalAccessCountScalar);
            }
        }
    }

    // NOTE: We retain associated phase index and access characteristics for each component through each
    // stage of the path creation process.

    // Figure out the size in bytes of each needed buffer.
    std::vector<std::tuple<size_t,PhaseIndex,AccessCharsPtr>> phaseLocalRegionSizesBytes[2];
    std::vector<std::tuple<size_t,std::vector<std::tuple<PhaseIndex,AccessCharsPtr>>>> phaseGlobalRegionSizesBytes[2];

    // Convert descriptions of Phase Local/Global Session Local/Global data locations into bytes.
    // Note that each session gets its own buffer for session-local accesses, so the number of session
    // local accesses is scaled down by the number of sessions.
    // Convert phase local characteristics.
    for (PhaseIndex phaseIdx = 0; phaseIdx < pathChars.phaseLocalCharacteristics.size(); ++phaseIdx) {
        const PhaseCharacteristics& phaseChars = pathChars.phaseLocalCharacteristics[phaseIdx].first;
        const AccessCharacteristics& accessChars = pathChars.phaseLocalCharacteristics[phaseIdx].second;

        if (phaseChars.numSessionLocalDatalocs > 0) {
            // TODO: log local data locations per session
            auto chars = std::make_shared<AccessChars>();
            chars->accessCountScalar = accessChars.sessionLocalAccessCountScalar;
            assert(chars->accessCountScalar > 0);
            chars->updateRatio = accessChars.sessionLocalUpdateRatio;
            size_t numDatalocs = std::max(1lu, (size_t)std::round((double)phaseChars.numSessionLocalDatalocs / config.numSessions));
            if (numDatalocs * config.numSessions != phaseChars.numSessionLocalDatalocs) {
                g_logger.printf(0, "Warning: number of phase-local, session-local data locations (%zu) of phase %zu "
                                "was rounded to nearest multiple of session count (%zu): %zu.\n",
                                phaseChars.numSessionLocalDatalocs, phaseIdx, config.numSessions, numDatalocs * config.numSessions);
            }
            phaseLocalRegionSizesBytes[LOCAL].emplace_back(
                    numDatalocs * Element::CACHELINE_SIZE_BYTES, phaseIdx, chars);
        }
        if (phaseChars.numSessionGlobalDatalocs > 0) {
            auto chars = std::make_shared<AccessChars>();
            chars->accessCountScalar = accessChars.sessionGlobalAccessCountScalar;
            assert(chars->accessCountScalar > 0);
            chars->updateRatio = accessChars.sessionGlobalUpdateRatio;
            phaseLocalRegionSizesBytes[GLOBAL].emplace_back(
                    phaseChars.numSessionGlobalDatalocs * Element::CACHELINE_SIZE_BYTES,
                    phaseIdx, chars);
        }
    }
    // Convert phase global characteristics.
    for (const auto& phaseGlobalPair : pathChars.phaseGlobalCharacteristics) {
        if (phaseGlobalPair.first.numSessionLocalDatalocs > 0) {
            // TODO: log local data locations per session
            phaseGlobalRegionSizesBytes[LOCAL].emplace_back();
            auto& phaseGlobalRegionSize = phaseGlobalRegionSizesBytes[LOCAL].back();

            size_t numDatalocs = std::max(1lu, (size_t)std::round((double)phaseGlobalPair.first.numSessionLocalDatalocs / config.numSessions));
            if (numDatalocs * config.numSessions != phaseGlobalPair.first.numSessionLocalDatalocs) {
                g_logger.printf(0, "Warning: number of phase-global, session-local data locations (%zu) of phases",
                                phaseGlobalPair.first.numSessionLocalDatalocs);
                for (const auto& phaseInfo : phaseGlobalPair.second) {
                    g_logger.printfChain(1, " %zu", phaseInfo.first);
                }
                g_logger.printfChain(0, "was rounded to nearest multiple of session count (%zu): %zu.\n",
                                     config.numSessions, numDatalocs * config.numSessions);
            }
            std::get<0>(phaseGlobalRegionSize) = numDatalocs * Element::CACHELINE_SIZE_BYTES;

            std::vector<std::tuple<PhaseIndex,AccessCharsPtr>>& phases = std::get<1>(phaseGlobalRegionSize);
            for (const std::pair<size_t,AccessCharacteristics>& phaseInfo : phaseGlobalPair.second) {
                auto chars = std::make_shared<AccessChars>();
                chars->accessCountScalar = phaseInfo.second.sessionLocalAccessCountScalar;
                assert(chars->accessCountScalar > 0);
                chars->updateRatio = phaseInfo.second.sessionLocalUpdateRatio;
                phases.emplace_back(phaseInfo.first, chars);
            }
        }
        if (phaseGlobalPair.first.numSessionGlobalDatalocs > 0) {
            phaseGlobalRegionSizesBytes[GLOBAL].emplace_back();
            auto& phaseGlobalRegionSize = phaseGlobalRegionSizesBytes[GLOBAL].back();

            std::get<0>(phaseGlobalRegionSize) =
                    phaseGlobalPair.first.numSessionGlobalDatalocs * Element::CACHELINE_SIZE_BYTES;

            std::vector<std::tuple<PhaseIndex,AccessCharsPtr>>& phases = std::get<1>(phaseGlobalRegionSize);
            for (const std::pair<size_t,AccessCharacteristics>& phaseInfo : phaseGlobalPair.second) {
                auto chars = std::make_shared<AccessChars>();
                chars->accessCountScalar = phaseInfo.second.sessionGlobalAccessCountScalar;
                assert(chars->accessCountScalar > 0);
                chars->updateRatio = phaseInfo.second.sessionGlobalUpdateRatio;
                phases.emplace_back(phaseInfo.first, chars);
            }
        }
    } 

    // Compute total size of local and global buffers, and create them.
    BufferPtr buffers[2] = {nullptr, nullptr};
    for (int state = 0; state < 2; ++state) {
        // Compute total size.
        size_t sizeBytes = 0;
        std::for_each(phaseLocalRegionSizesBytes[state].begin(), phaseLocalRegionSizesBytes[state].end(),
                      [&sizeBytes](const auto& tuple) { sizeBytes += std::get<0>(tuple); });
        std::for_each(phaseGlobalRegionSizesBytes[state].begin(), phaseGlobalRegionSizesBytes[state].end(),
                      [&sizeBytes](const auto& tuple) { sizeBytes += std::get<0>(tuple); });

        // Create buffer.
        if (sizeBytes > 0) {
            void* addr = alloc(sizeBytes);
            buffers[state] = std::make_shared<Buffer>(addr, sizeBytes);
            g_logger.printf(1, "Creating %s buffer at 0x%lx of size %zu bytes.\n",
                            toString(state), (uintptr_t)addr, sizeBytes);
        } else {
            assert(nullptr == buffers[state]);
        }
    }
    assert(!(nullptr == buffers[0] && nullptr == buffers[1]));

    // Divide the buffers into regions (stored in PathBuffers) into which paths can be constructed.
    std::vector<std::tuple<ElemPathBufferPtr,PhaseIndex,AccessCharsPtr>> phaseLocalPathBuffers[2];
    // In phaseGlobalPathBuffers, each inner vector contain PathBuffers created from a contiguous memory 
    // region split across cacheable memory regions in order for the data locations to be shared across phases.
    std::vector<std::vector<std::tuple<ElemPathBufferPtr,PhaseIndex,AccessCharsPtr>>> phaseGlobalPathBuffers[2];
    for (int state = 0; state < 2; ++state) {
        BufferPtr buffer = buffers[state];
        if (nullptr == buffer) {
            // No buffer since there are no global/local memory accesses.
            continue;
        }

        size_t bufferOffsetBytes = 0;
        // Allocate PathBuffers for phase local regions.
        for (const auto& tuple : phaseLocalRegionSizesBytes[state]) {
            size_t bufferSizeBytes;
            PhaseIndex phaseIndex;
            AccessCharsPtr chars;
            std::tie(bufferSizeBytes, phaseIndex, chars) = tuple;

            ElemPathBufferPtr pathBuffer = std::make_shared<ElemPathBuffer>(buffer, bufferSizeBytes, bufferOffsetBytes);
            phaseLocalPathBuffers[state].emplace_back(std::move(pathBuffer), phaseIndex, chars);
            g_logger.printf(3, "Reserving %zu bytes of %s buffer for phase%zu's phase-local accesses.\n",
                            bufferSizeBytes, toString(state), phaseIndex);

            bufferOffsetBytes += bufferSizeBytes;
        }
        // Allocate PathBuffers for phase global regions.
        for (const auto& tuple : phaseGlobalRegionSizesBytes[state]) {
            size_t bufferSizeBytes = std::get<0>(tuple);
            const std::vector<std::tuple<PhaseIndex,AccessCharsPtr>>& phaseInfos = std::get<1>(tuple);

            // We require that either all phases specify access count scalar or none do. Check this.
            bool useAccessCountScalar = (DOUBLE_INFINITY != std::get<1>(phaseInfos[0])->accessCountScalar);
            for (size_t i = 1; i < phaseInfos.size(); ++i) {
                const AccessCharsPtr& chars = std::get<1>(phaseInfos[i]);
                if ((useAccessCountScalar && DOUBLE_INFINITY == chars->accessCountScalar) ||
                    (!useAccessCountScalar && DOUBLE_INFINITY != chars->accessCountScalar))
                {
                    fprintf(stderr, "Error: Must specify access count scalar for all phases of phase-global"
                            " memory region or for none of them.\n");
                    exit(1);
                }
            }

            // Divide each cacheline of phase-global region amongst accessing phases.
            std::vector<size_t> cachelineSlicesBytes;
            cachelineSlicesBytes.reserve(phaseInfos.size());
            if (useAccessCountScalar) {
                // Divide each cacheline corresponding to access coutn scalar.
                double accessCountScalarSum = 0.;
                std::for_each(phaseInfos.begin(), phaseInfos.end(), [&accessCountScalarSum](const auto& phasePair) {
                    accessCountScalarSum += std::get<1>(phasePair)->accessCountScalar;
                });

                // Assign elements in cacheline to phases so that the ratio of elements different phase
                // get is similar to the ratio of their access count scalars.
                // Guarantee that each phase gets at least one entry in cacheline.
                std::vector<size_t> cachelineSlicesElements(phaseInfos.size(), 1);
                const size_t numElementsPerCacheline = Element::CACHELINE_SIZE_BYTES / sizeof(Element);
                size_t numElementsRemaining = numElementsPerCacheline - phaseInfos.size();
                if (numElementsRemaining > 0) {
                    std::map<double,std::set<size_t>> candidates;
                    for (size_t i = 0; i < phaseInfos.size(); ++i) {
                        double accessRatio = (double)cachelineSlicesElements[i] / numElementsPerCacheline;
                        double desiredRatio = std::get<1>(phaseInfos[i])->accessCountScalar / accessCountScalarSum;
                        candidates[accessRatio - desiredRatio].emplace(i);
                    }
                    while (numElementsRemaining > 0) {
                        auto it = candidates.begin();
                        auto idxIt = it->second.begin();
                        size_t i = *idxIt;
                        // This erasing is safe
                        it->second.erase(idxIt);
                        if (0 == it->second.size()) {
                            candidates.erase(it);
                        }
                        cachelineSlicesElements[i] += 1;
                        numElementsRemaining -= 1;
                        double accessRatio = (double)cachelineSlicesElements[i] / numElementsPerCacheline;
                        double desiredRatio = std::get<1>(phaseInfos[i])->accessCountScalar / accessCountScalarSum;
                        candidates[accessRatio - desiredRatio].emplace(i);
                    }
                    assert(0 == numElementsRemaining);
                }

                size_t sliceSumBytes = 0;
                for (size_t sliceElements : cachelineSlicesElements) {
                    assert(sliceElements > 0);
                    size_t sizeBytes = sliceElements * sizeof(Element);
                    cachelineSlicesBytes.emplace_back(sizeBytes);
                    sliceSumBytes += sizeBytes;
                }
                assert(Element::CACHELINE_SIZE_BYTES == sliceSumBytes);
            } else {
                // Assign each phase equally sized regions of cacheline
                size_t stepSizeBytes = ((Element::CACHELINE_SIZE_BYTES / phaseInfos.size()) / sizeof(Element)) * sizeof(Element);
                for (size_t i = 0; i < phaseInfos.size(); ++i) {
                    cachelineSlicesBytes.emplace_back(stepSizeBytes);
                }
            }

            // Construct path buffers
            phaseGlobalPathBuffers[state].emplace_back();
            std::vector<std::tuple<ElemPathBufferPtr,PhaseIndex,AccessCharsPtr>>& pathbufs =
                    phaseGlobalPathBuffers[state].back();
            g_logger.printf(3, "Reserving %zu bytes of %s buffer for phase-global accesses:",
                            bufferSizeBytes, toString(state));
            size_t cachelineOffsetBytes = 0;
            for (size_t i = 0; i < phaseInfos.size(); ++i) {
                size_t stepSizeBytes = cachelineSlicesBytes[i];
                PhaseIndex phaseIndex;
                AccessCharsPtr chars;
                std::tie(phaseIndex, chars) = phaseInfos[i];
                ElemPathBufferPtr pathBuffer =
                        std::make_shared<ElemPathBuffer>(buffer, bufferSizeBytes - cachelineOffsetBytes,
                                                         bufferOffsetBytes + cachelineOffsetBytes,
                                                         stepSizeBytes);
                pathbufs.emplace_back(pathBuffer, phaseIndex, chars);

                g_logger.printfChain(3, " cacheline bytes [%zu, %zu) to phase%zu,", cachelineOffsetBytes,
                                     cachelineOffsetBytes + stepSizeBytes, phaseIndex);
                cachelineOffsetBytes += stepSizeBytes;
            }
            g_logger.printfChain(3, "\b.\n");
            assert(Element::CACHELINE_SIZE_BYTES == cachelineOffsetBytes);
            bufferOffsetBytes += bufferSizeBytes;
        }
        assert(buffers[state]->Get_size() == bufferOffsetBytes);
    }

    // Sanity check that none of the PathBuffers overlap.
#ifndef NDEBUG
    {
        std::vector<ElemPathBufferPtr> pathBufs;
        auto addPathBuffers = [&pathBufs](const auto& tuples) {
            for (const auto& tuple : tuples) {
                pathBufs.emplace_back(std::get<0>(tuple));
            }
        };
        for (int state = 0; state < 2; ++state) {
            addPathBuffers(phaseLocalPathBuffers[state]);
            std::for_each(phaseGlobalPathBuffers[state].begin(), phaseGlobalPathBuffers[state].end(),
                          addPathBuffers);
        }
        for (size_t i = 0; i < pathBufs.size(); ++i) {
            for (size_t j = i + 1; j < pathBufs.size(); ++j) {
                assert(!pathBufs[i]->overlaps(pathBufs[j]));
            }
        }
    }
#endif

    // Compute the number of accesses per path traversal made in buffers for which
    // access count scalar was specified. Result is based on access count scalars
    // and the min and max capacities of each path buffer. (Min capacity is from the
    // fact that we require >= 1 access per cacheline.)
    double accessCountScalarSum = 0.;
    std::vector<std::tuple<ElemPathBufferPtr,AccessCharsPtr,PhaseIndex,SessionState>> accessCountBuffers;
    auto accumulateAccessChars = [&accessCountScalarSum,&accessCountBuffers](
            const std::tuple<ElemPathBufferPtr,PhaseIndex,AccessCharsPtr>& tuple, SessionState state)
    {
        double accessCountScalar = std::get<2>(tuple)->accessCountScalar;
        if (DOUBLE_INFINITY != accessCountScalar) {
            accessCountScalarSum += accessCountScalar;
        }
        accessCountBuffers.emplace_back(std::get<0>(tuple), std::get<2>(tuple), std::get<1>(tuple), state);
    };
    for (int state = 0; state < 2; ++state) {
        std::for_each(phaseLocalPathBuffers[state].begin(),
                      phaseLocalPathBuffers[state].end(),
                      std::bind(accumulateAccessChars, std::placeholders::_1, (SessionState)state));
        std::for_each(phaseGlobalPathBuffers[state].begin(),
                      phaseGlobalPathBuffers[state].end(),
                      [&accumulateAccessChars,state](const auto& tuples) {
                          std::for_each(tuples.begin(), tuples.end(),
                                        std::bind(accumulateAccessChars, std::placeholders::_1, (SessionState)state));
                      });
    }

    // Infer the minimum and maximum number of accesses.
    double minPathAccesses = -DOUBLE_INFINITY;
    double maxPathAccesses = DOUBLE_INFINITY;
    for (const std::tuple<ElemPathBufferPtr,AccessCharsPtr,PhaseIndex,SessionState>& tuple : accessCountBuffers) {
        ElemPathBufferPtr pathbuf;
        AccessCharsPtr chars;
        PhaseIndex phaseIndex;
        SessionState state;
        std::tie(pathbuf, chars, phaseIndex, state) = tuple;

        chars->accessCountPercent = DOUBLE_INFINITY;
        if (DOUBLE_INFINITY != chars->accessCountScalar) {
            chars->accessCountPercent = chars->accessCountScalar / accessCountScalarSum;
            double localMinPathAccesses = (pathbuf->_numCachelines * 1) / chars->accessCountPercent;
            double localMaxPathAccesses = (pathbuf->_numCachelines * pathbuf->_numElemsPerCacheline) /
                                           chars->accessCountPercent;

            g_logger.printf(1, "Access Count: phase %zu session %s buffer - percentage=%lf%% access_count_range=[%lf, %lf]\n",
                            phaseIndex, toString(state), chars->accessCountPercent * 100.,
                            localMinPathAccesses, localMaxPathAccesses);

            minPathAccesses = std::max(minPathAccesses, localMinPathAccesses);
            maxPathAccesses = std::min(maxPathAccesses, localMaxPathAccesses);

            g_logger.printf(1, "Path Access Count Range: [%lf, %lf]\n",
                            minPathAccesses, maxPathAccesses);
        }
    }

    double numPathAccesses;
    double (*round)(double);
    if (minPathAccesses > maxPathAccesses) {
        g_logger.printf(0, "Warning: Path could not be constructed with specified access count scalars."
                        " Making best attempt.\n");
        // We want some path buffers to hold more elements than is possible, so try to get as close as
        // possible to desired ratios.
        numPathAccesses = minPathAccesses;
        round = (double(*)(double))&std::ceil;
    } else {
        // Path access can be correctly distributed according to specified access count scalars, so use
        // the path access count that maximizes buffer utilization.
        numPathAccesses = maxPathAccesses;
        round = (double(*)(double))&std::floor;
    }

    // Distribute a path within each PathBuffer.
    // Collect a vector pointer chasing paths (with associated access characteristics) for each phase.
    std::vector<std::vector<std::pair<ElemPointerChasingPathPtr,AccessCharsPtr>>> phasePathCharsPairs(numPhases);
    for (int state = 0; state < 2; ++state) {
        for (const std::tuple<ElemPathBufferPtr,PhaseIndex,AccessCharsPtr>& tuple : phaseLocalPathBuffers[state]) {
            ElemPathBufferPtr pathBuffer;
            PhaseIndex phaseIndex;
            AccessCharsPtr chars;
            std::tie(pathBuffer, phaseIndex, chars) = tuple;

            ElemPointerChasingPathPtr path;
            if (DOUBLE_INFINITY == chars->accessCountPercent) {
                DistributePathZipf<Element> distribute(pathBuffer, DEFAULT_ZIPF_SHAPE, config.rbg());
                path = distribute.run();
            } else {
                size_t accessCount = (size_t)round(chars->accessCountPercent * numPathAccesses);
                assert(accessCount >= pathBuffer->_numCachelines);
                DistributePathZipf<Element> distribute(pathBuffer, accessCount, config.rbg());
                path = distribute.run();
            }
            phasePathCharsPairs[phaseIndex].emplace_back(path, chars);
        }
        // NOTE: assuming that access frequency of phase-global data locations is similar/same in all phases.
        for (const std::vector<std::tuple<ElemPathBufferPtr,PhaseIndex,AccessCharsPtr>>& pathBuffersSet : phaseGlobalPathBuffers[state]) {
            // Use the same cacheline seed for all path buffers that get a zipf distributed path so
            // the cachelines get the same ranking in each buffer.
            int cachelineSeed = config.rbg();
            for (const std::tuple<ElemPathBufferPtr,PhaseIndex,AccessCharsPtr>& tuple : pathBuffersSet) {
                ElemPathBufferPtr pathBuffer;
                PhaseIndex phaseIndex;
                AccessCharsPtr chars;
                std::tie(pathBuffer, phaseIndex, chars) = tuple;

                ElemPointerChasingPathPtr path;
                if (DOUBLE_INFINITY == chars->accessCountPercent) {
                    DistributePathZipf<Element> distribute(pathBuffer, DEFAULT_ZIPF_SHAPE, config.rbg(), cachelineSeed);
                    path = distribute.run();
                } else {
                    size_t accessCount = (size_t)round(chars->accessCountPercent * numPathAccesses);
                    assert(accessCount >= pathBuffer->_numCachelines);
                    DistributePathZipf<Element> distribute(pathBuffer, accessCount, config.rbg(), cachelineSeed);
                    path = distribute.run();
                }
                phasePathCharsPairs[phaseIndex].emplace_back(path, chars);
            }
        }
        // TODO: add logging
    }

    // Add read/write distributions to path buffers.
    // NOTE: this will be modified once all paths have been merged so the prior element indicates if the
    // following element must be written to. This way, we can read the next element and write to it at
    // the same time.
    std::vector<std::vector<ElemPointerChasingPathPtr>> phasePathsMap(numPhases);
    for (size_t phaseIndex = 0; phaseIndex < phasePathCharsPairs.size(); ++phaseIndex) {
        for (const auto& pathCharsPair : phasePathCharsPairs[phaseIndex]) {
            ReadWritePathUniformRandom<Element> rw(pathCharsPair.first, pathCharsPair.second->updateRatio, config.rbg());
            rw.run();
            phasePathsMap[phaseIndex].emplace_back(pathCharsPair.first);
            // TODO: add logging
        }
    }

    std::vector<ElemPointerChasingPathPtr> phasePaths;

    // Merge paths belonging to the same phase to produce one path per phase.
    for (std::vector<ElemPointerChasingPathPtr>& paths : phasePathsMap) {
        assert(paths.size() > 0);

        ElemPointerChasingPathPtr pathMerged;
        if (1 == paths.size()) {
            pathMerged = paths[0];
        } else {
            // Shuffle path merging order
            std::shuffle(paths.begin(), paths.end(), config.rbg);

            pathMerged = paths[0];
            size_t i = 1;
            do {
                MergePathsPeriodic<Element> merge(pathMerged, paths[i++]);
                pathMerged = merge.run();
            } while (i < paths.size());
        }
        phasePaths.emplace_back(std::move(pathMerged));
    }

    // Mark the last element in each phase to indicate end of phase.
    for (const ElemPointerChasingPathPtr& path : phasePaths) {
        path->getElement(path->_bufferIndexEnd, path->_indexEnd).setPhaseEnd();
    }

    // Catenate all the paths together into one.
    ElemPointerChasingPathPtr pathMerged = phasePaths[0];
    for (size_t i = 1; i < phasePaths.size(); ++i) {
        CatPaths<Element> cat(pathMerged, phasePaths[i]);
        pathMerged = cat.run();
    }

    // Move indicator that element should be written to to element prior in path.
    {
        Element* prevElem = &pathMerged->getElement(pathMerged->_bufferIndexBegin, pathMerged->_indexBegin);
        bool beginIsWrite = prevElem->isWrite();
        for (size_t i = 0; i < pathMerged->_length; ++i) {
            Element* nextElem = &pathMerged->getNextElement(*prevElem);
            if (nextElem->isWrite()) {
                prevElem->setWrite();
            } else {
                prevElem->setRead();
            }
            prevElem = nextElem;
        }
        prevElem = &pathMerged->getElement(pathMerged->_bufferIndexEnd, pathMerged->_indexEnd);
        if (beginIsWrite) {
            prevElem->setWrite();
        } else {
            prevElem->setRead();
        }
    }

    // Get indices of starting element for each phase.
    std::vector<std::pair<ElemValue,ElemValue>> phaseBeginIndexPairs;
    g_logger.printf(2, "Phase starts:");
    for (const ElemPointerChasingPathPtr& path : phasePaths) {
        Element& elem = path->getElement(path->_bufferIndexBegin, path->_indexBegin);
        ElemValue bufferIndex, index;
        bool found = pathMerged->getIndicesOf(&elem, bufferIndex, index);
        assert(found);
        phaseBeginIndexPairs.emplace_back(bufferIndex, index);
        g_logger.printfChain(2, " %u:%u", bufferIndex, index);
    }
    g_logger.printfChain(2, "\n");

    // Create PointerChasingPath for each session such that each session has its own local buffer.
    std::vector<ElemPointerChasingPathPtr> sessionPaths;
    uint8_t globalBufferIndex;
    sessionPaths.reserve(config.numSessions);

    sessionPaths.emplace_back(pathMerged);
    if (1 == pathMerged->_buffers.size()) {
        // Assert that the path has the expected local or global path buffer.
        assert(1 == pathMerged->_pathBuffers[0].size());

        ElemPathBufferPtr originalPathBuffer = *pathMerged->_pathBuffers[0].begin();
        if (buffers[LOCAL] == originalPathBuffer->_backendBuffer) {
            assert(buffers[LOCAL]->Get_size() == (originalPathBuffer->size() * sizeof(Element)));
            for (size_t sessIdx = 1; sessIdx < config.numSessions; ++sessIdx) {
                auto backendBuffer = std::make_shared<Buffer>(alloc(buffers[LOCAL]->Get_size()),
                                                              buffers[LOCAL]->Get_size());
                auto pathBuffer = std::make_shared<ElemPathBuffer>(backendBuffer);
                bool success = pathBuffer->copy(originalPathBuffer);
                assert(success);
                // TODO: add debug logging
                sessionPaths.emplace_back(std::make_shared<ElemPointerChasingPath>(
                        pathBuffer,
                        pathMerged->_indexBegin, pathMerged->_indexEnd,
                        pathMerged->_length));
            }
            globalBufferIndex = 1;
        } else {
            assert(buffers[GLOBAL]->Get_size() == (originalPathBuffer->size() * sizeof(Element)));
            for (size_t sessIdx = 1; sessIdx < config.numSessions; ++sessIdx) {
                // TODO: add debug logging
                sessionPaths.emplace_back(std::make_shared<ElemPointerChasingPath>(
                        originalPathBuffer,
                        pathMerged->_indexBegin, pathMerged->_indexEnd,
                        pathMerged->_length));
            }
            globalBufferIndex = 0;
        }
    } else if (2 == pathMerged->_buffers.size()) {
        // Assert that the path has the expected local and global path buffers.
        assert(1 == pathMerged->_pathBuffers[0].size() &&
               1 == pathMerged->_pathBuffers[1].size());

        uint8_t localBufIdx = (uint8_t)(buffers[LOCAL] == (*pathMerged->_pathBuffers[0].begin())->_backendBuffer ? 0 : 1);
        globalBufferIndex = (1 - localBufIdx);
        ElemPathBufferPtr templateLocalPathBuffer = *pathMerged->_pathBuffers[localBufIdx].begin();
        ElemPathBufferPtr globalPathBuffer = *pathMerged->_pathBuffers[1 - localBufIdx].begin();
        assert(buffers[LOCAL]->Get_size() == (templateLocalPathBuffer->size() * sizeof(Element)) &&
               buffers[GLOBAL]->Get_size() == (globalPathBuffer->size() * sizeof(Element)));
        assert(buffers[LOCAL] == templateLocalPathBuffer->_backendBuffer);

        for (size_t sessIdx = 1; sessIdx < config.numSessions; ++sessIdx) {
            auto backendBuffer =
                std::make_shared<Buffer>(alloc(buffers[LOCAL]->Get_size()), buffers[LOCAL]->Get_size());
            auto pathBuffer = std::make_shared<ElemPathBuffer>(backendBuffer);
            bool success = pathBuffer->copy(templateLocalPathBuffer);
            assert(success);
            // TODO: add debug logging

            std::vector<std::unordered_set<ElemPathBufferPtr>> pathBuffers(2);
            pathBuffers[localBufIdx].emplace(pathBuffer);
            pathBuffers[1 - localBufIdx].emplace(globalPathBuffer);
            sessionPaths.emplace_back(std::make_shared<ElemPointerChasingPath>(
                    pathBuffers,
                    pathMerged->_bufferIndexBegin, pathMerged->_indexBegin,
                    pathMerged->_bufferIndexEnd, pathMerged->_indexEnd,
                    pathMerged->_length));
        }
    } else {
        abort(); // this shouldnt' happen
    }

    return std::make_tuple(sessionPaths, phaseBeginIndexPairs, (uint8_t)globalBufferIndex);
}

std::shared_ptr<TestReturn> runTest(const std::vector<ElemPointerChasingPathPtr>& paths,
                                    const std::vector<std::pair<ElemValue,ElemValue>>& phaseBeginIndexPairs,
                                    uint8_t globalBufferIndex, Config& config)
{
    assert(paths.size() == config.numSessions);

    // TODO: may want to add ability to start sessions on different phases.
    std::vector<LoopArg> args;
    args.reserve(config.numSessions);
    for (size_t pathIdx = 0; pathIdx < paths.size(); ++pathIdx) {
        size_t phaseIdx;
        if (config.sessionDistribution >= 0) {
            phaseIdx = config.sessionDistribution;
        } else {
            assert(SESSION_DISTR_UNIFORM == config.sessionDistribution);
            phaseIdx = pathIdx % config.numPhases;
        }
        ElemValue bufferIndex, index;
        std::tie(bufferIndex, index) = phaseBeginIndexPairs[phaseIdx];

        const ElemPointerChasingPathPtr& path = paths[pathIdx];
        assert(1 == path->_buffers.size() || 2 == path->_buffers.size());
        Element* buffer1 = (path->_buffers.size() > 1 ? path->_buffers[1] : nullptr);
        args.emplace_back(phaseIdx, path->_buffers[0], buffer1, bufferIndex, index, globalBufferIndex, &config);
    }

    // Collect elements to be marked at the end of the test to notify sessions of test completion.
    std::vector<Element*> stopElements;
    stopElements.reserve(config.numSessions * config.numPhases);
    for (const ElemPointerChasingPathPtr& path : paths) {
        for (const std::pair<ElemValue,ElemValue>& indices : phaseBeginIndexPairs) {
            ElemValue bufferIndex, index;
            std::tie(bufferIndex, index) = indices;
            stopElements.emplace_back(&path->getElement(bufferIndex, index));
        }
    }

    // Misc prep for test.
    auto testDurationSec = std::chrono::seconds(config.testDurationSec);

    int err = clearCPUCaches(config.allCPUs.begin(), config.allCPUs.end());
    assert(0 == err);

    // Launch sessions.
    for (LoopArg& arg : args) {
        config.sessionManager->sessionStart(loop, &arg, std::get<0>(arg));
    }

    // Wait for all sessions to ready-up.
    config.testManager->waitUntilSessionsWaiting();

    g_logger.printf(0, "Test is ready to start.\n");

    // Tell all sessions to start.
    config.testManager->announceTestStart();
    auto startTime = std::chrono::steady_clock::now();

    // Wait for the specified amount of time to pass
    std::this_thread::sleep_for(testDurationSec);

    auto endTimeMin = std::chrono::steady_clock::now();

    // Add marker to buffer indicating that test is finished.
    std::for_each(stopElements.begin(), stopElements.end(),
            [](Element* e) {
                __atomic_store_n(&e->_value, Element::FINISHED_VALUE, __ATOMIC_RELAXED);
            });

    std::vector<std::shared_ptr<LoopReturn>> returns;
    returns.reserve(args.size());
    for (size_t sessIdx = 0; sessIdx < config.numSessions; ++sessIdx) {
        returns.emplace_back((LoopReturn*)config.sessionManager->sessionJoin(sessIdx));
        // Session index is ignored for now, but will be useful if
        // different sessions are started on different phases.
    }
    // By this time, we are certain that all sessions have finished.
    auto endTimeMax = std::chrono::steady_clock::now();

    return std::make_shared<TestReturn>(std::move(returns), startTime, endTimeMin, endTimeMax);
}

//==============================================================================

void printUsage(std::FILE* file, const char* exe, const char* mode = nullptr) {
    const char* header = "Usage: %s [test_option...] phase_local_chars... [phase_global_chars...]";
    if (nullptr == mode) {
        fprintf(file, header, exe);
        fprintf(file, " session_type ...\n");
        fprintf(file,
                "  test_option: one of the following\n"
                "    -s --sessions=UINT             Number of sessions to traverse the pointer chasing path. (default=%zu)\n"
                "    -d --duration=UINT             Duration to run in seconds (default=%u)\n"
                "    -r --seed=UINT                 Value used to seed random number generators. (default=%zu)\n"
                "    --sess-distr={UINT|%s}         Distribute session starting positions across phases. Specify phase\n"
                "                                   index to have all sessions start on that phase or \"%s\" to have them\n"
                "                                   spread evenly across phases. (default=%zu)\n"
                "    --signal                       Start test only after %s is received. (disabled by default)\n"
                "    --log-sessions=FILEPATH        Filepath to which session information gets saved. This information is\n"
                "                                   consumed by profiler. (disabled by default)\n"
                "    -v --verbose                   Increase logging verbosity.\n"
                "    -h --help                      Print help message and exit.\n"
                "\n"
                "  phase_local_chars: UINT[,FLOAT[,FLOAT]]/UINT[,FLOAT[,FLOAT]]\n"
                "    Specifies the local characteristics of the corresponding phase of execution. Consists of two parts\n"
                "    separated by a slash (/). The first part consists of:\n"
                "      - the number of session local data locations;\n"
                "      - optionally, the update ratio of accesses made to the data locations (default=%.1f); and\n"
                "      - optionally, a positive scalar indicating the relative number of accesses made to these data\n"
                "        locations (default=max accesses).\n"
                "    The second part consists of the same fields but in reference to session global data locations.\n"
                "    Note: the number of phase_local_chars specified indicates the number of phases.\n"
                "\n"
                "  phase_global_chars: UINT/UINT:UINT[,FLOAT,FLOAT[,FLOAT,FLOAT]]/UINT[,FLOAT,FLOAT[,FLOAT,FLOAT]]/...\n"
                "    Specifies the global characteristics of some set of phases of execution (>= 2). Consists of three parts\n"
                "    with the first two separated by a slash (/) and the latter two a colon (:). The first two parts are the\n"
                "    number of session local and session global data locations shared by the set of phases. The third part\n"
                "    is a list of two or more tuples (separated by a slash (/)) of:\n"
                "      - phase index;\n"
                "      - optionally, update ratios of accesses made by this phase to the session local and session global\n"
                "        data locations respectively (default=%.1f); and\n"
                "      - optionally, positive scalars indicating the relative number of accesses made by this phase to the\n"
                "        session local and session global data locations respectively (default=max accesses).\n",
                DEFAULT_NUM_SESSIONS, DEFAULT_TEST_DURATION_SEC, DEFAULT_SEED, SESSION_DISTR_UNIFORM_STR.c_str(),
                SESSION_DISTR_UNIFORM_STR.c_str(), DEFAULT_SESSION_DISTR, START_TEST_SIGNAL_STR, DEFAULT_UPDATE_RATIO,
                DEFAULT_UPDATE_RATIO);
        fprintf(file, "\n"
                "  session_type: one of the following\n"
                "    %16s               Represent each session with a Pthread.\n"
                "    %16s               Represent each session with a Fibre.\n",
                SESS_TYPE_THREAD_STR.c_str(), SESS_TYPE_FIBRE_STR.c_str());
    } else if (SESS_TYPE_THREAD_STR == mode) {
        fprintf(file, header, exe);
        fprintf(file,
                " %s [sess_option...] [sess_cpu_affinity...]\n"
                "\n"
                "  sess_option: one of the following\n"
                "    -h --help                      Print help message and exit.\n"
                "\n"
                "  sess_cpu_affinity: cpu set\n"
                "    The N-th sess_cpu_affinity specifies the cpu affinity of all sessions such that\n"
                "    #sessions %% #sess_cpu_affinity == N. Cpu set is a list of cpu ids or id ranges (e.g., A-B) delimited\n"
                "    by commas.\n",
                SESS_TYPE_THREAD_STR.c_str());
    } else if (SESS_TYPE_FIBRE_STR == mode) {
        fprintf(file, header, exe);
        fprintf(file,
                " %s layout_option [sess_option...]\n"
                "\n"
                "  layout_option: one of the following\n"
                "    -w --workers=UINT              Assign each phase to the same cluster and cluster group. Specify the number\n"
                "                                   of workers to assign.\n"
                "    -g --groups=UINT,...           Assign each phase its own cluster group (within a single cluster). Specify a\n"
                "                                   list of worker counts such that the N-th count is the number of workers be\n"
                "                                   assigned to the N-th group.\n"
                "    -c --clusters=UINT,...         Assign each phase its own cluster. Specify a list of worker counts such that\n"
                "                                   the N-th count is the number of workers to be assigned to the N-th cluster.\n"
                "\n"
                "  sess_option: one of the following\n"
                "    -h --help                      Print help message and exit.\n",
                SESS_TYPE_FIBRE_STR.c_str());
    } else {
        abort(); // this shouldn't happen.
    }
}

//==============================================================================

std::set<int> parseCPUSet(char* str) {
    std::set<int> cpus;
    while ('\0' != *str) {
        assert(*str >= '0' && '9' <= *str);
        size_t idx;
        int cpu = std::stoi(str, &idx, 10);
        str += idx;
        switch (*str) {
            case '-': {
                ++str;
                assert(*str >= '0' && '9' <= *str);
                int cpuEnd = std::stoi(str, &idx, 10);
                str += idx;
                assert(cpu <= cpuEnd);
                for (int i = cpu; i <= cpuEnd; ++i) cpus.emplace(i);
                break;
            }
            case ',':
                [[fallthrough]];
            case '\0':
                cpus.emplace(cpu);
                break;
        }
        assert(',' == *str || '\0' == *str);
        if (',' == *str) ++str;
    }
    return cpus;
}

void configThreadTest(int argc, char* argv[], Config& config) {
    const char* optstring = "+h";
    struct option longopts[] = {
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}};

    int c;
    while ((c = getopt_long(argc, argv, optstring, longopts, NULL)) != -1) {
        switch (c) {
            case 'h':
                printUsage(stdout, argv[0], SESS_TYPE_THREAD_STR.c_str());
                exit(0);
                break;
            case '?':
                fprintf(stderr, "Error: unrecognized option \"%s\".\n", argv[optind]);
                printUsage(stderr, argv[0], SESS_TYPE_THREAD_STR.c_str());
                exit(1);
                break;
            default:
                abort(); // this shouldn't happen
                break;
        }
    }

    // Get thread affinity sets.
    std::vector<std::set<int>> threadCPUAffinitySets;
    for (; optind < argc; ++optind) {
        if (!std::regex_match(argv[optind], CPU_SET_REGEX)) {
            fprintf(stderr, "Error: Cpu set \"%s\"not recognized.\n", argv[optind]);
            exit(1);
        }
        threadCPUAffinitySets.emplace_back(parseCPUSet(argv[optind]));
    }

    std::shared_ptr<ThreadTestManager> testManager = std::make_shared<ThreadTestManager>(config.numPhases, config.numSessions, g_config.signalStartTest);
    std::shared_ptr<ThreadSessionManager> sessManager = std::make_shared<ThreadSessionManager>(config.numSessions);

    if (config.sessionOutputFilepath.size() > 0) {
        sessManager->setSessionOutput(config.sessionOutputFilepath);
    }

    if (threadCPUAffinitySets.size() > 0) {
        sessManager->setThreadCPUAffinity(threadCPUAffinitySets);
        for (const std::set<int>& cpuSet : threadCPUAffinitySets) {
            config.allCPUs.insert(cpuSet.begin(), cpuSet.end());
        }
    } else {
        cpu_set_t cpuset;
        int err = pthread_getaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
        assert(0 == err);
        config.allCPUs = getCPUSet(cpuset);
    }

    config.testManager = std::static_pointer_cast<TestManager>(testManager);
    config.sessionManager = std::static_pointer_cast<SessionManager>(sessManager);
}

void configFibreTest(int argc, char* argv[], Config& config) {
    const char* optstring = ":w:g:c:h";
    struct option longopts[] = {
            {"workers", required_argument, 0, 'w'},
            {"groups", required_argument, 0, 'g'},
            {"clusters", required_argument, 0, 'c'},
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}};

    enum PhaseLayout {
        NILL, NONE, CLUSTERS, CLUSTER_GROUPS
    } layout = NILL;
    char* numWorkersStr = nullptr;

    bool layoutReset = false;
    int c;
    while (!layoutReset && (c = getopt_long(argc, argv, optstring, longopts, NULL)) != -1) {
        switch (c) {
            case 'w':
                if (!(layoutReset = (NILL != layout))) {
                    layout = NONE;
                    numWorkersStr = optarg;
                }
                break;
            case 'g':
                if (!(layoutReset = (NILL != layout))) {
                    layout = CLUSTER_GROUPS;
                    numWorkersStr = optarg;
                }
                break;
            case 'c':
                if (!(layoutReset = (NILL != layout))) {
                    layout = CLUSTERS;
                    numWorkersStr = optarg;
                }
                break;
            case 'h':
                printUsage(stdout, argv[0], SESS_TYPE_FIBRE_STR.c_str());
                exit(0);
                break;
            case '?':
                fprintf(stderr, "Error: unrecognized option \"%s\".\n", argv[optind]);
                printUsage(stderr, argv[0], SESS_TYPE_FIBRE_STR.c_str());
                exit(1);
                break;
            case ':':
                fprintf(stderr, "Error: argument must be specified for \"%s\".\n", argv[optind]);
                printUsage(stderr, argv[0], SESS_TYPE_FIBRE_STR.c_str());
                exit(1);
                break;
            default:
                abort(); // this shouldn't happen
                break;
        }
    }
    if (NILL == layout) {
        fprintf(stdout, "Error: layout not specified.\n");
        exit(1);
    } else if (layoutReset) {
        fprintf(stderr, "Error: multiple layouts specified.\n");
        exit(1);
    }
    // TODO: provide more configuration through command line.

    std::vector<size_t> workerCounts;
    switch (layout) {
        case NILL:
            // do nothing
            break;
        case NONE: {
            char* end;
            workerCounts.emplace_back((size_t)std::strtoul(numWorkersStr, &end, 10));
            assert('\0' == *end);
        } break;
        case CLUSTER_GROUPS:
            [[fallthrough]];
        case CLUSTERS: {
            assert(numWorkersStr);
            char* str = numWorkersStr;
            char* end;
            while ('\0' != *str) {
                workerCounts.emplace_back((size_t)std::strtoul(str, &end, 10));
                if (0 == workerCounts.back()) {
                    fprintf(stderr, "Error: worker count cannot be zero.\n");
                    exit(1);
                }
                str = end;
                if (',' == *str) {
                    str += 1;
                } else {
                    assert('\0' == *str);
                }
            }

            if (workerCounts.size() != config.numPhases) {
                fprintf(stderr, "Error: number of worker counts specified (%zu) differs from number of phases (%zu).\n",
                        workerCounts.size(), config.numPhases);
                exit(1);
            }
        } break;
        default:
            abort();
    }

    // TODO/FIX: this is wrong but I'm too tired...
    std::vector<std::vector<size_t>> clusterGroupSizes;
    std::vector<std::pair<size_t,size_t>> phaseClusterGroupIndexPairs;
    phaseClusterGroupIndexPairs.reserve(config.numPhases);
    switch (layout) {
        case NILL:
            // do nothing
            break;
        case NONE:
            clusterGroupSizes.emplace_back();
            clusterGroupSizes.back().emplace_back(workerCounts.back());
            for (size_t phaseIdx = 0; phaseIdx < config.numPhases; ++phaseIdx) {
                phaseClusterGroupIndexPairs.emplace_back(0,0);
            }
            break;
        case CLUSTER_GROUPS:
            clusterGroupSizes.resize(1);
            clusterGroupSizes[0].reserve(config.numPhases);
            for (size_t i = 0; i < workerCounts.size(); ++i) {
                clusterGroupSizes[0].emplace_back(workerCounts[i]);
                phaseClusterGroupIndexPairs.emplace_back(0,i);
            }
            break;
        case CLUSTERS:
            clusterGroupSizes.resize(config.numPhases);
            for (size_t i = 0; i < workerCounts.size(); ++i) {
                clusterGroupSizes[i].emplace_back(workerCounts[i]);
                phaseClusterGroupIndexPairs.emplace_back(i,0);
            }
            break;
        default:
            abort();
    }

    // TODO
    std::vector<std::vector<std::vector<std::set<int>>>> clusterGroupWorkerCPUAffinitySets;

    std::shared_ptr<FibreTestManager> testManager =
            std::make_shared<FibreTestManager>(config.numSessions, clusterGroupSizes, phaseClusterGroupIndexPairs,
                                               g_config.signalStartTest, config.rbg);
    if (clusterGroupWorkerCPUAffinitySets.size() > 0) {
        testManager->setThreadCPUAffinity(clusterGroupWorkerCPUAffinitySets);
    }
    std::shared_ptr<FibreSessionManager> sessManager =
            std::make_shared<FibreSessionManager>(config.numSessions, testManager);
    if (config.sessionOutputFilepath.size() > 0) {
        sessManager->setSessionOutput(config.sessionOutputFilepath);
    }

    if (clusterGroupWorkerCPUAffinitySets.size() > 0) {
        testManager->setThreadCPUAffinity(clusterGroupWorkerCPUAffinitySets);
        for (const auto& groupWorkerCPUSets : clusterGroupWorkerCPUAffinitySets) {
            for (const auto& workerCPUSets : groupWorkerCPUSets) {
                for (const auto& cpuSet : workerCPUSets) {
                    config.allCPUs.insert(cpuSet.begin(), cpuSet.end());
                }
            }
        }
    } else {
        cpu_set_t cpuset;
        int err = pthread_getaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
        assert(0 == err);
        config.allCPUs = getCPUSet(cpuset);
    }

    config.testManager = std::static_pointer_cast<TestManager>(testManager);
    config.sessionManager = std::static_pointer_cast<SessionManager>(sessManager);
}

int main(int argc, char* argv[]) {
    const char* optstring = "+:s:d:r:T:vh";
    struct option longopts[] = {
            {"sessions", required_argument, 0, 's'},
            {"duration", required_argument, 0, 'd'},
            {"seed", required_argument, 0, 'r'},
            {"sess-distr", required_argument, 0, 'T'},
            {"signal", no_argument, 0, 'S'},
            {"log-sessions", required_argument, 0, 'L'},
            {"verbose", no_argument, 0, 'v'},
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}};

    // Determine the session type and its index in the argv.
    bool sessTypeIsThread = true;
    int sessTypeIdx = -1;
    for (int i = 1; i < argc; ++i) {
        if ((sessTypeIsThread = (SESS_TYPE_THREAD_STR == argv[i])) ||
            SESS_TYPE_FIBRE_STR == argv[i])
        {
            sessTypeIdx = i;
            break;
        }
    }

    // Parse general options.
    // Even if session type isn't specified, need to be able to parse --help.
    int generalArgc = (sessTypeIdx > 0 ? sessTypeIdx : argc);
    int maxLogLevel = DEFAULT_MAX_LOG_LEVEL;
    int c;
    while ((c = getopt_long(generalArgc, argv, optstring, longopts, NULL)) != -1) {
        switch (c) {
            case 's':
                g_config.numSessions = (size_t)std::strtoull(optarg, NULL, 10);
                break;
            case 'd':
                g_config.testDurationSec = (unsigned)std::strtoul(optarg, NULL, 10);
                break;
            case 'r':
                g_config.rbg.seed((unsigned int)std::strtoul(optarg, NULL, 10));
                break;
            case 'T': {
                char* end;
                g_config.sessionDistribution = (long)std::strtoul(optarg, &end, 10);
                if (optarg == end) {
                    if (SESSION_DISTR_UNIFORM_STR == optarg) {
                        g_config.sessionDistribution = SESSION_DISTR_UNIFORM;
                    } else {
                        fprintf(stderr, "Error: session distribution \"%s\" not recognized.\n",
                                optarg);
                        exit(1);
                    }
                }
            } break;
            case 'S':
                g_config.signalStartTest = true;
                break;
            case 'L':
                g_config.sessionOutputFilepath = optarg;
                break;
            case 'v':
                ++maxLogLevel;
                break;
            case 'h':
                printUsage(stdout, argv[0]);
                exit(0);
                break;
            case '?':
                fprintf(stderr, "Error: unrecognized option \"%s\".\n", argv[optind]);
                printUsage(stderr, argv[0]);
                exit(1);
                break;
            case ':':
                fprintf(stderr, "Error: argument must be specified for \"%s\".\n", argv[optind]);
                printUsage(stderr, argv[0]);
                exit(1);
                break;
            default:
                abort(); // this shouldn't happen
                break;
        }
    }
    if (-1 == sessTypeIdx) {
        fprintf(stderr, "Error: session type not speicified or not recognized.\n");
        printUsage(stderr, argv[0]);
        exit(1);
    }
    g_logger.setMaxLogLevel(maxLogLevel);

    // Parse phase local path characteristics.
    std::vector<std::pair<PhaseCharacteristics,AccessCharacteristics>> phaseLocalCharacteristics;
    auto parsePhaseLocalChars = [](const std::cmatch& results, size_t offset) -> std::tuple<size_t,double,double> {
        size_t numDatalocs = (size_t)std::stoull(results[offset + 0]);

        float updateRatio = DEFAULT_UPDATE_RATIO;
        float accessCountScalar = DOUBLE_INFINITY;
        if (results[offset + 1].length() > 0) {
            updateRatio = std::stof(results[offset + 1]);
            if (updateRatio < 0. || updateRatio > 1.) {
                fprintf(stderr, "Error: update ratio %lf not a decimal percent in range [0,1].\n", updateRatio);
                exit(1);
            }
            if (results[offset + 2].length() > 0) {
                accessCountScalar = std::stof(results[offset + 2]);
                if (accessCountScalar <= 0. && numDatalocs > 0) {
                    fprintf(stderr, "Error: Access count scalar %lf not a positive scalar in range (0, +inf).\n",
                            accessCountScalar);
                    exit(1);
                }
            }
        } else {
            assert(0 == results[offset + 2].length());
        }
        return std::make_tuple(numDatalocs, updateRatio, accessCountScalar);
    };
    for (size_t phaseIndex = 0; optind < sessTypeIdx; ++optind, ++phaseIndex) {
        std::cmatch results;
        if (std::regex_match(argv[optind], results, PHASE_LOCAL_REGEX)) {
            assert(results.ready() && 7 == results.size());
            PhaseCharacteristics phaseChars;
            AccessCharacteristics accessChars;
            std::tie(phaseChars.numSessionLocalDatalocs,
                     accessChars.sessionLocalUpdateRatio,
                     accessChars.sessionLocalAccessCountScalar) = parsePhaseLocalChars(results, 1);
            std::tie(phaseChars.numSessionGlobalDatalocs,
                     accessChars.sessionGlobalUpdateRatio,
                     accessChars.sessionGlobalAccessCountScalar) = parsePhaseLocalChars(results, 4);
            phaseLocalCharacteristics.emplace_back(phaseChars, accessChars);
        } else {
            break;
        }
    }

    // Store number of phases.
    g_config.numPhases = phaseLocalCharacteristics.size();
    if (0 == g_config.numPhases) {
        fprintf(stderr, "Error: no phase local characteristics could be parse\n");
        exit(1);
    } else if (g_config.numPhases > MAX_NUM_PHASES) {
        fprintf(stderr, "Error: phase count too large (%zu < %d). Increase maximum by modifying MAX_NUM_PHASES.\n",
                g_config.numPhases, MAX_NUM_PHASES);
        exit(1);
    }

    if (g_config.sessionDistribution >= 0 &&
        (size_t)g_config.sessionDistribution >= g_config.numPhases)
    {
        fprintf(stderr, "Error: invalid phase index (%ld) specified for session distribution: %zu phases.\n",
                g_config.sessionDistribution, g_config.numPhases);
        exit(1);
    }

    // Parse phase global path characteristics.
    std::vector<std::pair<PhaseCharacteristics,std::vector<std::pair<size_t,AccessCharacteristics>>>> phaseGlobalCharacteristics;
    for (; optind < sessTypeIdx; ++optind) {
        std::cmatch results;
        if (std::regex_match(argv[optind], results, PHASE_GLOBAL_REGEX)) {
            assert(results.ready() && 4 == results.size());

            phaseGlobalCharacteristics.emplace_back();

            // Parse phase characteristics.
            PhaseCharacteristics& phaseChars = phaseGlobalCharacteristics.back().first;
            phaseChars.numSessionLocalDatalocs = (size_t)std::stoull(results[1]);
            phaseChars.numSessionGlobalDatalocs = (size_t)std::stoull(results[2]);
            if (0 == phaseChars.numSessionLocalDatalocs && 0 == phaseChars.numSessionGlobalDatalocs) {
                fprintf(stderr, "Error: Phase global data location counts can't both be zero.\n");
                exit(1);
            }

            // Parse phase indices and access characteristics.
            std::vector<std::pair<size_t,AccessCharacteristics>>& accessingPhases = phaseGlobalCharacteristics.back().second;
            std::string result = results[3].str();
            assert(result.size() > 0);
            char* str = &result[0];
            while ('\0' != *str) {
                accessingPhases.emplace_back();
                size_t& phaseIndex = accessingPhases.back().first;
                AccessCharacteristics& chars = accessingPhases.back().second;
                char* end;

                // Parse phase index.
                phaseIndex = (size_t)std::strtoull(str, &end, 10);
                str = end;
                if (phaseIndex >= phaseLocalCharacteristics.size()) {
                    fprintf(stderr, "Error: Phase-global phase index %zu does not refer to a valid phase.\n", phaseIndex);
                    exit(1);
                }

                // Parse access characteristics.
                chars.sessionLocalUpdateRatio = DEFAULT_UPDATE_RATIO;
                chars.sessionGlobalUpdateRatio = DEFAULT_UPDATE_RATIO;
                chars.sessionLocalAccessCountScalar = DOUBLE_INFINITY;
                chars.sessionGlobalAccessCountScalar = DOUBLE_INFINITY;
                if (',' == *str) {
                    str += 1;
                    chars.sessionLocalUpdateRatio = std::strtod(str, &end);
                    str = end;
                    assert(',' == *str);
                    str += 1;
                    chars.sessionGlobalUpdateRatio = std::strtod(str, &end);
                    str = end;
                    if (',' == *str) {
                        str += 1;
                        chars.sessionLocalAccessCountScalar = std::strtod(str, &end);
                        str = end;
                        assert(',' == *str);
                        str += 1;
                        chars.sessionGlobalAccessCountScalar = std::strtod(str, &end);
                        str = end;
                    }
                }
                if ('/' == *str) {
                    str += 1;
                } else {
                    assert('\0' == *str);
                }
            }
        } else {
            break;
        }
    }

    // Verify that there are no more phase characteristics to parse.
    if (sessTypeIdx != optind) {
        fprintf(stderr, "Error: Phase characteristic \"%s\" could not be parsed.\n", argv[optind]);
        exit(1);
    }
    PathCharacteristics pathChars(phaseLocalCharacteristics, phaseGlobalCharacteristics);

    // Modify getopt values in preperation for use during thread/fibre config.
    int sessArgc = argc - sessTypeIdx;
    char** sessArgv = argv + sessTypeIdx; // leave session mode at index 0
    optind = 1;

    // Initialize test for specified session type.
    if (sessTypeIsThread) {
        configThreadTest(sessArgc, sessArgv, g_config);
    } else {
        configFibreTest(sessArgc, sessArgv, g_config);
    }

    // Create the paths over which sessions traverse.
    std::vector<ElemPointerChasingPathPtr> sessionPaths;
    std::vector<std::pair<ElemValue,ElemValue>> phaseBeginIndexPairs;
    uint8_t globalBufferIndex;
    std::tie(sessionPaths, phaseBeginIndexPairs, globalBufferIndex) = createPaths(pathChars, g_config);

    // Run test.
    std::shared_ptr<TestReturn> testReturn = runTest(sessionPaths, phaseBeginIndexPairs, globalBufferIndex, g_config);

    // Log statistics.
    auto durationMin = std::chrono::duration_cast<std::chrono::milliseconds>(testReturn->endTimeMin - testReturn->startTime);
    auto durationMax = std::chrono::duration_cast<std::chrono::milliseconds>(testReturn->endTimeMax - testReturn->startTime);
    printf("Test duration in range [%.3f, %.3f] seconds\n",
           durationMin.count() / 1000., durationMax.count() / 1000.);

    auto computeMeanAndStd = [](const std::vector<long double>& vals) -> std::tuple<long double, long double,long double> {
        long double sum = std::accumulate(vals.begin(), vals.end(), 0.);
        long double mean = sum / vals.size();

        std::vector<long double> diff(vals.size());
        std::transform(vals.begin(), vals.end(), diff.begin(),
                       [mean](long double v) { return v - mean; });
        long double sqsum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
        long double std = std::sqrt(sqsum / vals.size());

        return std::make_tuple(sum, mean, std);
    };

    std::vector<long double> accessCounts, phaseCounts;
    accessCounts.reserve(testReturn->sessionLoopReturns.size());
    phaseCounts.reserve(testReturn->sessionLoopReturns.size());
    for (const std::shared_ptr<LoopReturn>& loopReturn : testReturn->sessionLoopReturns) {
        accessCounts.emplace_back(std::get<0>(*loopReturn));
        phaseCounts.emplace_back(std::get<1>(*loopReturn));
    }

    auto numAccessesSumMeanStd = computeMeanAndStd(accessCounts);
    printf("Num Accesses: %Lf\n", std::get<0>(numAccessesSumMeanStd));
    printf("Throughput: %Lf\n", std::get<0>(numAccessesSumMeanStd) / g_config.testDurationSec);
    printf("Num Accesses - Session Mean & Stddev: %Lf,%Lf\n",
           std::get<1>(numAccessesSumMeanStd), std::get<2>(numAccessesSumMeanStd));

    auto numPhasesSumMeanStd = computeMeanAndStd(phaseCounts);
    printf("Num Phases Traversed: %Lf\n", std::get<0>(numPhasesSumMeanStd));
    printf("Num Phases Traversed - Session Mean & Stddev: %Lf,%Lf\n",
           std::get<1>(numPhasesSumMeanStd), std::get<2>(numPhasesSumMeanStd));

    std::vector<long double> iterationCounts;
    iterationCounts.reserve(phaseCounts.size());
    std::for_each(phaseCounts.begin(), phaseCounts.end(),
            [&iterationCounts](const long double& phaseCount) {
                iterationCounts.emplace_back(phaseCount / g_config.numPhases);
            });
    auto numIterationsSumMeanStd = computeMeanAndStd(phaseCounts);
    printf("Num Path Iterations: %Lf\n", std::get<0>(numIterationsSumMeanStd));
    printf("Num Path Iterations - Session Mean & Stddev: %Lf,%Lf\n",
           std::get<1>(numIterationsSumMeanStd), std::get<2>(numIterationsSumMeanStd));

    return 0;
}

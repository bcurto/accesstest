
#pragma once

//#include "Buffer.hpp"

// TODO: differentiate between Element and Element::Value

#include <cstddef>
#include <cstdint>
#include <cassert>
#include <tuple>
#include <vector>
#include <random>
#include <array>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <sys/mman.h>

//==============================================================================
const size_t PAGE_SIZE = 4096;
#ifndef MAP_HUGE_2MB
    #define MAP_HUGE_2MB (21 << MAP_HUGE_SHIFT)
#endif
#ifndef MAP_HUGE_1GB
    #define MAP_HUGE_1GB (30 << MAP_HUGE_SHIFT)
#endif

void* alloc(size_t size) {
    int flags = MAP_PRIVATE | MAP_ANONYMOUS;
    if (size > PAGE_SIZE) {
        flags |= MAP_HUGETLB;
        if (size <= (1lu << 21)) {
            flags |= MAP_HUGE_2MB;
        } else if (size <= (1lu << 30)) {
            flags |= MAP_HUGE_1GB;
        } else {
            abort(); // don't supported sizes this large
        }
    }
    void* addr = mmap(NULL, size, PROT_READ | PROT_WRITE, flags, 0, 0);
    assert(MAP_FAILED != addr);
    return addr;
}

struct Buffer {
    void* _buffer;
    size_t _size;

    Buffer(void* buffer, size_t size)
    : _buffer(buffer), _size(size) {}

    void* Get_buffer_pointer() const {
        return _buffer;
    }
    size_t Get_size() const {
        return _size;
    }
};
/*
void printBuffer(const std::shared_ptr<Buffer>& buffer, size_t sizeBytes, size_t offsetBytes = 0) {
    assert((uintptr_t)buffer->Get_buffer_pointer() % Element::CACHELINE_SIZE_BYTES == 0);
    assert(Element::CACHELINE_SIZE_BYTES > 0 &&
           (Element::CACHELINE_SIZE_BYTES & (Element::CACHELINE_SIZE_BYTES - 1)) == 0);
    const size_t numElemsPerCacheline = Element::CACHELINE_SIZE_BYTES / sizeof(Element);

    // Round up to the nearest cacheline size.
    size_t alignedSizeBytes = (sizeBytes & ~(Element::CACHELINE_SIZE_BYTES - 1)) +
                              (sizeBytes & (Element::CACHELINE_SIZE_BYTES - 1) ? Element::CACHELINE_SIZE_BYTES : 0);
    // Round down to the nearest cacheline size. 
    size_t alignedOffsetBytes = offsetBytes & ~(Element::CACHELINE_SIZE_BYTES - 1);

    size_t numElements = alignedSizeBytes / sizeof(Element);
    size_t indexOffset = alignedOffsetBytes / sizeof(Element);
    size_t cachelineIdxOffset = (indexOffset / numElemsPerCacheline);

    printf("  cacheable dataline %zu ", cachelineIdxOffset);
    for (size_t elemIdx = 0; elemIdx < numElements; ++elemIdx) {
        if (elemIdx > 0 && 0 == elemIdx % numElemsPerCacheline && numElements - 1 != elemIdx) {
            printf("\n  cacheable dataline %zu ", cachelineIdxOffset + (elemIdx / numElemsPerCacheline));
        }
        printf(" 0x%x", ((Element*)buffer->Get_buffer_pointer())[indexOffset + elemIdx]._value);
    }
    printf("\n");
}
*/
//==============================================================================

template <typename T, size_t BitIndex, size_t BitWidth>
struct BitHelper {
    static_assert(BitWidth > 0 && BitIndex + BitWidth <= sizeof(T) * 8,
                  "Invalid BitWidth and/or BitIndex");

    // Considers bits to be zero indexed from least to most significant bit.
    static inline constexpr T enableBitsBehind(size_t bitIdx) {
        return bitIdx < (sizeof(T) * 8) ? ((T)1 << bitIdx) - 1 : ~(T)0;
    }

    static const T BITMASK = enableBitsBehind(BitIndex + BitWidth) ^
                             enableBitsBehind(BitIndex);

    static inline constexpr T getData(T v) {
        return (v & BITMASK) >> BitIndex;
    }
    static inline constexpr T stripData(T v) {
        return v & ~BITMASK;
    }
    static inline constexpr T stripOther(T v) {
        return v & BITMASK;
    }
    static inline constexpr T embedData(T v, T data) {
        return stripData(v) | stripOther(data << BitIndex);
    }

    static inline bool canEmbed(T v) {
        return (getData(embedData(0, v)) == v);
    }
};

//==============================================================================

template <typename Val, size_t CLSB>
struct ElementBase {
    using Value = Val;
    static const size_t CACHELINE_SIZE_BYTES = CLSB;

    volatile Value _value;

    ElementBase() : ElementBase(0) {}
    ElementBase(Value v) : _value(v) {}

    operator Value() const {
        return _value;
    }

    inline constexpr Value getIndex() const {
        return _value;
    }
    inline void setIndex(Value index) {
        _value = index;
    }

    inline constexpr Value getBufferIndex() const {
        // Assuming there's only ever one buffer.
        return 0;
    }
    inline void setBufferIndex(Value index) {
        assert(0 == index);
    }
};

template <bool Atomic>
struct DummyWriteHelper {
    template <typename T>
    static inline T write(volatile T* ptr) {
        // TODO: modify function based off template type size
        static_assert(4 == sizeof(T));
        T ret;
        asm("xor{l %%eax, %%eax | eax, eax}\n\t"
            "lock cmpxchg{l %%eax, %0 | %0, eax}"
            : "+m" (*ptr), "=a" (ret));
        return ret;
    }
};

template <> struct
DummyWriteHelper<false> {
    template <typename T>
    static inline T write(volatile T* ptr) {
        T val = *ptr;
        *ptr = val;
        return val;
    }
};

template <size_t CLSB>
struct MyElement : private ElementBase<uint32_t,CLSB> {
    // NOTE: Setters should only be used during buffer construction
    // while getters can be used during construction and experiments.
    using Base = ElementBase<uint32_t,CLSB>;

    using typename Base::Value;
    using Base::CACHELINE_SIZE_BYTES;
    using Base::_value;
    using Base::Base;

    using IndexHelper = BitHelper<Value,0,29>;
    using BufferIndexHelper = BitHelper<Value,29,1>;
    using RWHelper = BitHelper<Value,30,1>;
    using PhaseEndHelper = BitHelper<Value,31,1>;

    // Reserve max index to mark end of test.
    static const Value FINISHED_VALUE = ~0;

    inline constexpr Value getIndex() const {
        return IndexHelper::getData(_value);
    }
    inline void setIndex(Value index) {
        assert(IndexHelper::canEmbed(index));
        _value = IndexHelper::embedData(_value, index);
        assert(FINISHED_VALUE != _value);
    }

    inline constexpr Value getBufferIndex() const {
        return BufferIndexHelper::getData(_value);
    }
    inline void setBufferIndex(Value index) {
        assert(BufferIndexHelper::canEmbed(index));
        _value = BufferIndexHelper::embedData(_value, index);
        assert(FINISHED_VALUE != _value);
    }

    // If element is marked write, perform dummy write on the next element.
    inline constexpr bool isWrite() const {
        return RWHelper::getData(_value);
    }
    inline void setWrite() {
        _value = RWHelper::embedData(_value, 1);
        assert(FINISHED_VALUE != _value);
    }
    inline void setRead() {
        _value = RWHelper::embedData(_value, 0);
        assert(FINISHED_VALUE != _value);
    }

    // Perform dummy write on element.
    template <bool Atomic>
    inline MyElement<CLSB> dummyWrite() {
        return DummyWriteHelper<Atomic>::write(&_value);
    }

    inline constexpr bool isPhaseEnd() const {
        return PhaseEndHelper::getData(_value);
    }
    inline void setPhaseEnd() {
        _value = PhaseEndHelper::embedData(_value, 1);
        assert(FINISHED_VALUE != _value);
    }
    inline void clearPhaseEnd() {
        _value = PhaseEndHelper::embedData(_value, 0);
        assert(FINISHED_VALUE != _value);
    }

    inline constexpr bool isFinished() const {
        return FINISHED_VALUE == _value;
    }
};

//==============================================================================

template <typename Element>
struct PathBuffer {
    static const size_t CACHELINE_SIZE_BYTES = Element::CACHELINE_SIZE_BYTES;
    static const size_t MAX_ELEMENTS_PER_CACHELINE = CACHELINE_SIZE_BYTES / sizeof(Element);
    static_assert(CACHELINE_SIZE_BYTES % sizeof(Element) == 0,
                  "Element not divisor of cacheline size");

    std::shared_ptr<Buffer> _backendBuffer;
    Element* _buffer;
    size_t _numCachelines;
    size_t _numElemsPerCacheline;
    size_t _indexOffset;

    struct SetDirectly {};

    PathBuffer(const std::shared_ptr<Buffer>& backendBuffer);
    PathBuffer(const std::shared_ptr<Buffer>& backendBuffer,
               size_t sizeBytes, size_t offsetBytes = 0, size_t bytesPerCacheline = CACHELINE_SIZE_BYTES);
    PathBuffer(SetDirectly, const std::shared_ptr<Buffer>& backendBuffer,
               size_t numCachelines, size_t numElemsPerCacheline, size_t indexOffset);

    size_t size() const;

    bool contains(const Element* element) const;
    bool overlaps(const std::shared_ptr<PathBuffer<Element>>& pbuf1) const;
    bool copy(const std::shared_ptr<PathBuffer<Element>>& src);

    static std::shared_ptr<PathBuffer<Element>> merge(const std::shared_ptr<PathBuffer<Element>>& pbuf1,
                                                      const std::shared_ptr<PathBuffer<Element>>& pbuf2);
};

template <typename Element>
PathBuffer<Element>::PathBuffer(const std::shared_ptr<Buffer>& backendBuffer)
: PathBuffer(backendBuffer, backendBuffer->Get_size(), 0, CACHELINE_SIZE_BYTES)
{}

template <typename Element>
PathBuffer<Element>::PathBuffer(const std::shared_ptr<Buffer>& backendBuffer, size_t sizeBytes, size_t offsetBytes, size_t bytesPerCacheline)
: PathBuffer(SetDirectly(), backendBuffer,
             // Count all cachelines such that bytesPerCacheline bytes fit.
             sizeBytes / CACHELINE_SIZE_BYTES + ((sizeBytes % CACHELINE_SIZE_BYTES) >= bytesPerCacheline ? 1 : 0),
             bytesPerCacheline / sizeof(Element),
             offsetBytes / sizeof(Element))
{
    assert(0 == offsetBytes % sizeof(Element));
}

template <typename Element>
PathBuffer<Element>::PathBuffer(SetDirectly, const std::shared_ptr<Buffer>& backendBuffer,
                                size_t numCachelines, size_t numElemsPerCacheline, size_t indexOffset)
: _backendBuffer(backendBuffer)
, _buffer((Element*)_backendBuffer->Get_buffer_pointer())
, _numCachelines(numCachelines)
, _numElemsPerCacheline(numElemsPerCacheline)
, _indexOffset(indexOffset)
{
    assert(0 == (uintptr_t)_buffer % sizeof(Element));
    assert(_numCachelines > 0); 
    assert(_numElemsPerCacheline > 0 && _numElemsPerCacheline <= MAX_ELEMENTS_PER_CACHELINE);

    // Make sure that all elements fit in the buffer.
    assert(indexOffset * sizeof(Element) + (_numCachelines - 1) * CACHELINE_SIZE_BYTES +
           _numElemsPerCacheline * sizeof(Element) <= backendBuffer->Get_size());

    // Make sure that cacheline elements are actually in the same cacheline.
    assert((uintptr_t)_buffer / CACHELINE_SIZE_BYTES ==
           ((uintptr_t)(_buffer + _numElemsPerCacheline) - 1) / CACHELINE_SIZE_BYTES);
}

template <typename Element>
size_t PathBuffer<Element>::size() const {
    return _numCachelines * _numElemsPerCacheline;
}

template <typename Element>
bool PathBuffer<Element>::contains(const Element* element) const {
    bool rval = false;

    // First, check if element is within lower and upper bounds of PathBuffer.
    Element* lower = _buffer + _indexOffset;
    Element* upper = lower + (_numCachelines - 1) * MAX_ELEMENTS_PER_CACHELINE + _numElemsPerCacheline;
    if (element >= lower && element < upper) {
        // Then, check if element is one of the elements in a given cacheable data region that is owned by the PathBuffer.
        size_t cachelineIndex = (element - (_buffer + _indexOffset)) % MAX_ELEMENTS_PER_CACHELINE;
        rval = (cachelineIndex < _numElemsPerCacheline);
    }
    return rval;
}

template <typename Element>
std::shared_ptr<PathBuffer<Element>> PathBuffer<Element>::merge(const std::shared_ptr<PathBuffer<Element>>& pbuf1,
                                                                const std::shared_ptr<PathBuffer<Element>>& pbuf2)
{
    std::shared_ptr<PathBuffer<Element>> merged = nullptr;
    if (pbuf1 != pbuf2 && pbuf1->_backendBuffer == pbuf2->_backendBuffer) {
        // Check if it's a merge length wise.
        if (pbuf1->_numElemsPerCacheline == pbuf2->_numElemsPerCacheline &&
            (pbuf1->_indexOffset + (pbuf1->_numCachelines * MAX_ELEMENTS_PER_CACHELINE) == pbuf2->_indexOffset ||
             pbuf2->_indexOffset + (pbuf2->_numCachelines * MAX_ELEMENTS_PER_CACHELINE) == pbuf1->_indexOffset))
        {
            size_t numCachelines = pbuf1->_numCachelines + pbuf2->_numCachelines;
            size_t indexOffset = std::min(pbuf1->_indexOffset, pbuf2->_indexOffset);
            merged = std::make_shared<PathBuffer<Element>>(
                    SetDirectly(), pbuf1->_backendBuffer, numCachelines,
                    pbuf1->_numElemsPerCacheline, indexOffset);
        } else {
            // Check if it's a merge width wise.
            if (pbuf1->_numCachelines == pbuf2->_numCachelines &&
                (pbuf1->_indexOffset + pbuf1->_numElemsPerCacheline == pbuf2->_indexOffset ||
                 pbuf2->_indexOffset + pbuf2->_numElemsPerCacheline == pbuf1->_indexOffset))
            {
                size_t numElemsPerCacheline = pbuf1->_numElemsPerCacheline + pbuf2->_numElemsPerCacheline;
                size_t indexOffset = std::min(pbuf1->_indexOffset, pbuf2->_indexOffset);
                merged = std::make_shared<PathBuffer<Element>>(
                        SetDirectly(), pbuf1->_backendBuffer, pbuf1->_numCachelines,
                        numElemsPerCacheline, indexOffset);
            }
        }
    }
    return merged;
}

template <typename Element>
bool PathBuffer<Element>::copy(const std::shared_ptr<PathBuffer<Element>>& src) {
    bool success;
    if ((success = ((_buffer != src->_buffer) &&
                    (_numCachelines == src->_numCachelines) &&
                    (_numElemsPerCacheline == src->_numElemsPerCacheline) &&
                    (_indexOffset == src->_indexOffset))))
    {
        for (size_t cachelineIndex = 0; cachelineIndex < src->_numCachelines; ++cachelineIndex) {
            size_t srcCachelineOffset = src->_indexOffset + cachelineIndex * MAX_ELEMENTS_PER_CACHELINE;
            size_t dstCachelineOffset = _indexOffset + cachelineIndex * MAX_ELEMENTS_PER_CACHELINE;
            for (size_t cachelineElemIndex = 0; cachelineElemIndex < src->_numElemsPerCacheline; ++cachelineElemIndex) {
                _buffer[dstCachelineOffset + cachelineElemIndex] = src->_buffer[srcCachelineOffset + cachelineElemIndex];
            }
        }
    }
    return success;
}

template <typename Element>
bool PathBuffer<Element>::overlaps(const std::shared_ptr<PathBuffer<Element>>& pbuf) const {
    bool rval = false;

    const PathBuffer<Element>* tmp[2][2] = {{this, pbuf.get()},{pbuf.get(), this}};
    for (int i = 0; !rval && i < 2; ++i) {
        const PathBuffer<Element>* pb1 = tmp[i][0];
        const PathBuffer<Element>* pb2 = tmp[i][1];

        Element* pb1Begin = pb1->_buffer + pb1->_indexOffset;
        for (size_t i = 0; !rval && i < pb1->_numElemsPerCacheline; ++i) {
            if (pb2->contains(pb1Begin + i)) {
                rval = true;
            }
        }
    }
    return rval;

    /*
    // TODO: rewrite this using contains()
    using PathBufferPtr = std::shared_ptr<PathBuffer<Element>>;

    // Determine which (if either) PathBuffer have their first element in the memory
    // range of the second PathBuffer.
    const std::vector<std::pair<PathBufferPtr,PathBufferPtr>> tmp({{pbuf1,pbuf2},{pbuf2,pbuf1}});
    for (const auto& pair : tmp) {
        PathBufferPtr pb1, pb2;
        std::tie(pb1, pb2) = pair;

        Element* pb1Begin = pb1->_buffer + pb1->_indexOffset;
        Element* pb2Begin = pb2->_buffer + pb2->_indexOffset;
        Element* pb2End = pb2Begin + (pb2->_numCachelines - 1) * MAX_ELEMENTS_PER_CACHELINE + pb2->_numElemsPerCacheline;

        if (pb1Begin >= pb2Begin && pb1Begin < pb2End) {
            // Compute index offset (from base of the second PathBuffer) to the first element in a cacheline contained in both PathBuffers.
            // (This shared cacheline is the first element of the first PathBuffer.)
            // This is enough since a PathBuffer uses the same element locations with each cacheline over which it spans.
            size_t pb1BeginIndex = pb1Begin - (pb2->_buffer + pb2->_indexOffset);
            // Get starting element of second PathBuffer in cacheline in which first PathBuffer starts.
            size_t pb2CachelineBeginIndex = (pb1BeginIndex / MAX_ELEMENTS_PER_CACHELINE) * MAX_ELEMENTS_PER_CACHELINE;
            Element* pb2CachelineBegin = pb2->_buffer + pb2->_indexOffset + pb2CachelineBeginIndex;

            if ((pb1Begin >= pb2CachelineBegin && pb1Begin < pb2CachelineBegin + pb2->_numElemsPerCacheline) ||
                (pb2CachelineBegin >= pb1Begin && pb2CachelineBegin < pb1Begin + pb1->_numElemsPerCacheline))
            {
                return true;
            }
        }
    }
    return false;
    */
}

//==============================================================================
template <typename Element>
struct PointerChasingPath {
    using Value = typename Element::Value;

    std::vector<std::unordered_set<std::shared_ptr<PathBuffer<Element>>>> _pathBuffers;
    std::vector<Element*> _buffers;

    Value _bufferIndexBegin;
    Value _indexBegin;

    Value _bufferIndexEnd;
    Value _indexEnd;

    size_t _length;

    void validate();

    PointerChasingPath(const std::shared_ptr<PathBuffer<Element>>& pathBuffer);
    PointerChasingPath(const std::vector<std::unordered_set<std::shared_ptr<PathBuffer<Element>>>>& pathBuffers);
    PointerChasingPath(const std::shared_ptr<PathBuffer<Element>>& pathBuffer,
                       Value indexBegin, Value indexEnd, size_t length);
    PointerChasingPath(const std::vector<std::unordered_set<std::shared_ptr<PathBuffer<Element>>>>& pathBuffers,
                       Value bufferIndexBegin, Value indexBegin, Value bufferIndexEnd, Value indexEnd,
                       size_t length);

    void update(Value indexBegin, Value indexEnd, size_t length);
    void update(Value bufferIndexBegin, Value indexBegin, Value bufferIndexEnd, Value indexEnd,
                size_t length);

    // NOTE: these methods are not meant to be fast, just safe.
    inline Element* getBuffer(Value idx) {
        assert(idx < _buffers.size());
        return _buffers[idx];
    }

    inline Element& getElement(Value bufferIndex, Value index) {
        Element& element = getBuffer(bufferIndex)[index];
        assert(contains(&element));
        return element;
    }

    static inline constexpr std::pair<Value,Value> getNextIndices(const Element& element) {
        return std::make_pair(element.getBufferIndex(), element.getIndex());
    }
    inline std::pair<Value,Value> getNextIndices(Value bufferIndex, Value index) {
        return getNextIndices(getElement(bufferIndex, index));
    }

    inline Element& getNextElement(const Element& element) {
        Value bufferIndex, index;
        assert(contains(&element));
        std::tie(bufferIndex, index) = getNextIndices(element);
        return getElement(bufferIndex, index);
    }
    inline Element& getNextElement(Value bufferIndex, Value index) {
        return getNextElement(getElement(bufferIndex, index));
    }

    static inline constexpr void setNextDirect(Element& element, Value bufferIndex, Value index) {
        element.setBufferIndex(bufferIndex);
        element.setIndex(index);
    }
    inline void setNext(Element& element, Value bufferIndex, Value index) {
        // Assumes that element and element refered to by indices are in path.
        assert(contains(&element));
        assert(contains(&getElement(bufferIndex, index)));
        setNextDirect(element, bufferIndex, index);
    }

    inline bool getIndicesOf(const Element* element, Value& bufferIndex, Value& index) const {
        for (size_t bufIdx = 0; bufIdx < _buffers.size(); ++bufIdx) {
            for (const auto& pathBuf : _pathBuffers[bufIdx]) {
                if (pathBuf->contains(element)) {
                    bufferIndex = bufIdx;
                    index = (element - _buffers[bufIdx]);
                    assert(element == &_buffers[bufferIndex][index]);
                    return true;
                }
            }
        }
        return false;
    }

    inline bool contains(const Element* element) const {
        Value bufferIndex, index;
        return getIndicesOf(element, bufferIndex, index);
    }
};

template <typename Element>
void PointerChasingPath<Element>::validate() {
#ifndef NDEBUG
    {
        // Assert that no path buffers overlap.
        std::vector<std::shared_ptr<PathBuffer<Element>>> allPathBuffers;
        for (const auto& pathBufSet : _pathBuffers) {
            for (const auto& pathBuf : pathBufSet) {
                allPathBuffers.emplace_back(pathBuf);
            }
        }
        for (size_t i = 0; i < allPathBuffers.size(); ++i) {
            for (size_t j = i + 1; j < allPathBuffers.size(); ++j) {
                assert(!allPathBuffers[i]->overlaps(allPathBuffers[j]));
            }
        }
    }
#endif

#ifndef NDEBUG
    // Assert that all PathBuffers in the same set have the same backend buffer.
    for (const auto& pbufs : _pathBuffers) {
        assert(pbufs.size() > 0);
        auto begin = pbufs.begin();
        for (auto it = std::next(begin); it != pbufs.end(); ++it) {
            assert((*begin)->_buffer == (*it)->_buffer);
        }
    }
#endif

#ifndef NDEBUG
    // Assert that begin indices, end indices, and length are correct.
    if (_length > 0) {
        Value prevBufferIndex, prevIndex;
        Value nextBufferIndex = _bufferIndexBegin, nextIndex = _indexBegin;
        size_t computedLength = 0;
        do {
            prevBufferIndex = nextBufferIndex;
            prevIndex = nextIndex;
            computedLength++;
            std::tie(nextBufferIndex, nextIndex) = getNextIndices(prevBufferIndex, prevIndex);
        } while (computedLength < _length &&
                 !(prevBufferIndex == _bufferIndexEnd && prevIndex == _indexEnd) &&
                 !(nextBufferIndex == _bufferIndexBegin && nextIndex == _indexBegin));
        assert(computedLength == _length);
        assert(prevBufferIndex == _bufferIndexEnd && prevIndex == _indexEnd);
        assert(nextBufferIndex == _bufferIndexBegin && nextIndex == _indexBegin);
    }
#endif
}

template <typename Element>
PointerChasingPath<Element>::PointerChasingPath(const std::shared_ptr<PathBuffer<Element>>& pathBuffer)
: PointerChasingPath(std::vector<std::unordered_set<std::shared_ptr<PathBuffer<Element>>>>({{pathBuffer}}))
{}

template <typename Element>
PointerChasingPath<Element>::PointerChasingPath(const std::vector<std::unordered_set<std::shared_ptr<PathBuffer<Element>>>>& pathBuffers)
: PointerChasingPath(pathBuffers, 0, 0, 0, 0, 0)
{}

template <typename Element>
PointerChasingPath<Element>::PointerChasingPath(const std::shared_ptr<PathBuffer<Element>>& pathBuffer,
                                                Value indexBegin, Value indexEnd, size_t length)
: PointerChasingPath(std::vector<std::unordered_set<std::shared_ptr<PathBuffer<Element>>>>({{pathBuffer}}),
                     0, indexBegin, 0, indexEnd, length)
{}

template <typename Element>
PointerChasingPath<Element>::PointerChasingPath(const std::vector<std::unordered_set<std::shared_ptr<PathBuffer<Element>>>>& pathBuffers,
                                                Value bufferIndexBegin, Value indexBegin, Value bufferIndexEnd, Value indexEnd,
                                                size_t length)
: _pathBuffers(pathBuffers)
, _buffers(pathBuffers.size())
, _bufferIndexBegin(bufferIndexBegin)
, _indexBegin(indexBegin)
, _bufferIndexEnd(bufferIndexEnd)
, _indexEnd(indexEnd)
, _length(length)
{
    for (size_t i = 0; i < _pathBuffers.size(); ++i) {
        _buffers[i] = (*_pathBuffers[i].begin())->_buffer;
    }

    validate();
}
template <typename Element>
void PointerChasingPath<Element>::update(Value indexBegin, Value indexEnd, size_t length) {
    assert(_buffers.size() == 1);
    update(0, indexBegin, 0, indexEnd, length);
}

template <typename Element>
void PointerChasingPath<Element>::update(Value bufferIndexBegin, Value indexBegin, Value bufferIndexEnd, Value indexEnd,
                                         size_t length)
{
    _bufferIndexBegin = bufferIndexBegin;
    _indexBegin = indexBegin;
    _bufferIndexEnd = bufferIndexEnd;
    _indexEnd = indexEnd;
    _length = length;

    validate();
}

//==============================================================================

template <typename Element>
class DistributePath {
private:
    std::shared_ptr<PointerChasingPath<Element>> _result;

protected:
    using Value = typename Element::Value;
    static const size_t CACHELINE_SIZE_BYTES = Element::CACHELINE_SIZE_BYTES;
    static const size_t MAX_ELEMENTS_PER_CACHELINE = CACHELINE_SIZE_BYTES / sizeof(Element);

    std::shared_ptr<PathBuffer<Element>> _pathBuffer;

    virtual std::shared_ptr<PointerChasingPath<Element>> compute() = 0;

public:
    // TODO: allow for multiple pathbuffersto be used, possibly from the same buffer
    DistributePath(const std::shared_ptr<PathBuffer<Element>>& pathBuffer)
    : _result(nullptr)
    , _pathBuffer(pathBuffer)
    {}

    std::shared_ptr<PointerChasingPath<Element>> run() {
        if (nullptr == _result) {
            _result = compute();
        }
        return _result;
    }
};

//==============================================================================

template <typename Element>
class DistributePathDuplicate : public DistributePath<Element> {
private:
    // See https://stackoverflow.com/a/1121016/6573510
    using Value = typename DistributePath<Element>::Value;
    using DistributePath<Element>::_pathBuffer;
    using DistributePath<Element>::MAX_ELEMENTS_PER_CACHELINE;

    std::shared_ptr<PointerChasingPath<Element>> _srcPath;

    std::shared_ptr<PointerChasingPath<Element>> compute();

public:
    DistributePathDuplicate(const std::shared_ptr<PathBuffer<Element>>& pathBuffer,
                            const std::shared_ptr<PointerChasingPath<Element>>& srcPath)
    : DistributePath<Element>(pathBuffer)
    , _srcPath(srcPath)
    {}
};

template <typename Element>
std::shared_ptr<PointerChasingPath<Element>> DistributePathDuplicate<Element>::compute() {
    assert(1 == _srcPath->_buffers.size() && 1 == _srcPath->_pathBuffers[0].size());
    std::shared_ptr<PathBuffer<Element>> srcPathBuffer = *_srcPath->_pathBuffers[0].begin();

    size_t srcIdxOffset = srcPathBuffer->_indexOffset;
    size_t dstIdxOffset = _pathBuffer->_indexOffset;
    auto path = std::make_shared<PointerChasingPath<Element>>(_pathBuffer);

    Value prevBufIdx = _srcPath->_bufferIndexBegin;
    Value prevIdx = _srcPath->_indexBegin;
    for (size_t i = 0; i < _srcPath->_length; ++i) {
        assert(0 == prevBufIdx);
        Value nextBufIdx, nextIdx;
        std::tie(nextBufIdx, nextIdx) = _srcPath->getNextIndices(prevBufIdx, prevIdx);
        assert(0 == nextBufIdx);

        Value dstPrevIdx = (prevIdx - srcIdxOffset) + dstIdxOffset;
        Value dstNextIdx = (nextIdx - srcIdxOffset) + dstIdxOffset;
        path->setNext(path->getElement(prevBufIdx, dstPrevIdx), nextBufIdx, dstNextIdx);

        prevBufIdx = nextBufIdx;
        prevIdx = nextIdx;
    }

    Value dstIdxBegin = (_srcPath->_indexBegin - srcIdxOffset) + dstIdxOffset;
    Value dstIdxEnd = (_srcPath->_indexEnd - srcIdxOffset) + dstIdxOffset;
    path->update(dstIdxBegin, dstIdxEnd, _srcPath->_length);
    return path;
}

//==============================================================================

template <typename Element>
class DistributePathUniformEqual : public DistributePath<Element> {
private:
    // See https://stackoverflow.com/a/1121016/6573510
    using typename DistributePath<Element>::Value;
    using DistributePath<Element>::_pathBuffer;
    using DistributePath<Element>::MAX_ELEMENTS_PER_CACHELINE;
    
    int _seed;

    std::shared_ptr<PointerChasingPath<Element>> compute();

public:
    DistributePathUniformEqual(std::shared_ptr<PathBuffer<Element>>& pathBuffer, int seed)
    : DistributePath<Element>(pathBuffer)
    , _seed(seed)
    {}
};

template <typename Element>
std::shared_ptr<PointerChasingPath<Element>> DistributePathUniformEqual<Element>::compute() {
    // Create buffer holding the indices of a sequential traversal all elements in buffer.
    size_t length = _pathBuffer->_numCachelines * _pathBuffer->_numElemsPerCacheline;

    std::vector<Value> tmp(length);
    for (size_t i = 0, line = 0; line < _pathBuffer->_numCachelines; line++) {
        for (size_t elem = 0; elem < _pathBuffer->_numElemsPerCacheline; elem++, i++) {
            tmp[i] = _pathBuffer->_indexOffset + (line * MAX_ELEMENTS_PER_CACHELINE) + elem;
        }
    }
    auto path = std::make_shared<PointerChasingPath<Element>>(_pathBuffer);

    // Shuffle entries to get the pointer chasing path
    std::mt19937 g(_seed);
    std::shuffle(tmp.begin(), tmp.end(), g);

    // Write path to buffer
    auto it = tmp.cbegin();
    Value indexBegin, prevIndex;
    indexBegin = prevIndex = *it++;
    for (; it != tmp.cend(); it++) {
        Value nextIndex = *it;
        path->setNext(_pathBuffer->_buffer[prevIndex], 0, nextIndex);
        prevIndex = nextIndex;
    }
    Value indexEnd = prevIndex;
    path->setNext(_pathBuffer->_buffer[indexEnd], 0, indexBegin);

    path->update(indexBegin, indexEnd, length);
    return path;
}

//==============================================================================

// TODO: Create a class of pointer chasing path classes using templated distribution
template <typename Element>
class DistributePathUniform : public DistributePath<Element> {
private:
    // See https://stackoverflow.com/a/1121016/6573510
    using typename DistributePath<Element>::Value;
    using DistributePath<Element>::_pathBuffer;
    using DistributePath<Element>::MAX_ELEMENTS_PER_CACHELINE;

    int _seed;

    std::shared_ptr<PointerChasingPath<Element>> compute();

public:
    DistributePathUniform(const std::shared_ptr<PathBuffer<Element>>& pathBuffer, int seed)
    : DistributePath<Element>(pathBuffer)
    , _seed(seed)
    {}
};

template <typename Element>
std::shared_ptr<PointerChasingPath<Element>> DistributePathUniform<Element>::compute() {
    // Initialize elements' indices with 0 to mark which elements are uninitialized.
    for (int line = 0; line < _pathBuffer->_numCachelines; line++) {
        for (int elem = 0; elem < _pathBuffer->_numElemsPerCacheline; elem++) {
            Value index = _pathBuffer->_indexOffset + (line * MAX_ELEMENTS_PER_CACHELINE) + elem;
            PointerChasingPath<Element>::setNextDirect(_pathBuffer->_buffer[index], 0, 0);
        }
    }

    // Since we're using 0 to indicate that an element is uninitialized, what happens if
    // we actually want to store 0? Keep note of the index intentionally holding 0.
    bool zeroIndexEncountered = false;
    Value indexOfZeroIndex = 0;

    std::mt19937 g(_seed);
    std::uniform_int_distribution<size_t> cachelineDistribution(0, _pathBuffer->_numCachelines - 1);

    // Pick the first element of a random cacheline as our starting point.
    Value prevIndex, indexBegin;
    prevIndex = indexBegin = _pathBuffer->_indexOffset + (Value)(cachelineDistribution(g) * MAX_ELEMENTS_PER_CACHELINE);
    size_t length = 1;

    auto path = std::make_shared<PointerChasingPath<Element>>(_pathBuffer);
    while (true) {
        // Prevent prevIndex from being selected again by marking it as taken.
        PointerChasingPath<Element>::setNextDirect(_pathBuffer->_buffer[prevIndex], 0, 1);

        Value cachelineIndexOffset = _pathBuffer->_indexOffset + (Value)(cachelineDistribution(g) * MAX_ELEMENTS_PER_CACHELINE);
        size_t elementIndex = 0;
        for (; elementIndex < _pathBuffer->_numElemsPerCacheline; elementIndex++) {
            if (0 == _pathBuffer->_buffer[cachelineIndexOffset + elementIndex].getIndex() &&
                (!zeroIndexEncountered || indexOfZeroIndex != cachelineIndexOffset + elementIndex))
            {
                break;
            }
        }
        if (elementIndex < _pathBuffer->_numElemsPerCacheline) {
            Value nextIndex = cachelineIndexOffset + elementIndex;
            path->setNext(_pathBuffer->_buffer[prevIndex], 0, nextIndex);
            length++;
            if (0 == nextIndex) {
                zeroIndexEncountered = true;
                indexOfZeroIndex = prevIndex;
            }
            prevIndex = nextIndex;
        } else {
            // Cacheline filled, consider the path constructed.
            break;
        }
    }
    Value indexEnd = prevIndex;
    path->setNext(_pathBuffer->_buffer[indexEnd], 0, indexBegin);

    path->update(indexBegin, indexEnd, length);
    return path;
}

//==============================================================================

template <typename Element>
class DistributePathZipf : public DistributePath<Element> {
    // https://en.wikipedia.org/wiki/Zipf's_law#Theoretical_review
private:
    // See https://stackoverflow.com/a/1121016/6573510
    using typename DistributePath<Element>::Value;
    using DistributePath<Element>::_pathBuffer;
    using DistributePath<Element>::MAX_ELEMENTS_PER_CACHELINE;

    int _seed;
    // Used for ranking cachelines to allow for different buffers to get
    // the same zipf ranking if given the same cacheline seed.
    int _seedCacheline;

    size_t _numAccesses;
    std::vector<size_t> _rankedAccessCounts;

    std::shared_ptr<PointerChasingPath<Element>> compute();

    static double computeZipfDenominator(double s, size_t N);
    // Given the number of elements (N) and the frequency of the rank 1 element
    // (rank1Freq), estimate the shape parameter of the zipf distribution.
    static double approximateShapeParam(double rank1Freq, size_t N);
    static double computeZipf(size_t k, double s, double denominator);

public:
    DistributePathZipf(const std::shared_ptr<PathBuffer<Element>>& pathBuffer, double shape, int seed);
    DistributePathZipf(const std::shared_ptr<PathBuffer<Element>>& pathBuffer, double shape,
                       int seed, int seedCacheline);

    DistributePathZipf(const std::shared_ptr<PathBuffer<Element>>& pathBuffer, size_t numAccesses, int seed);
    DistributePathZipf(const std::shared_ptr<PathBuffer<Element>>& pathBuffer, size_t numAccesses,
                       int seed, int seedCacheline);
};

template <typename Element>
DistributePathZipf<Element>::DistributePathZipf(const std::shared_ptr<PathBuffer<Element>>& pathBuffer,
                                                double shape, int seed)
: DistributePathZipf(pathBuffer, shape, seed, seed)
{}

template <typename Element>
DistributePathZipf<Element>::DistributePathZipf(const std::shared_ptr<PathBuffer<Element>>& pathBuffer,
                                                double shape, int seed, int seedCacheline)
: DistributePath<Element>(pathBuffer)
, _seed(seed)
, _seedCacheline(seedCacheline)
, _numAccesses(0)
{
    assert(shape >= 0);

    if (0 == shape) {
        // Do nothing else as compute() should call uniform equal distribution class.
        _numAccesses = _pathBuffer->_numCachelines * _pathBuffer->_numElemsPerCacheline;
    } else {
        _rankedAccessCounts.resize(_pathBuffer->_numCachelines, 0);

        double zipfDenominator = computeZipfDenominator(shape, _pathBuffer->_numCachelines);

        // Find scalar to multiply with frequencies to get access counts. Want rank 1 cacheline to get as
        // many accesses as buffer allows.
        _rankedAccessCounts[0] = _pathBuffer->_numElemsPerCacheline;
        double scalar = _rankedAccessCounts[0] / computeZipf(1, shape, zipfDenominator);
        for (size_t i = 1; i < _pathBuffer->_numCachelines; ++i) {
            size_t accessCount = (size_t)std::round(scalar * computeZipf(i + 1 /* rank */, shape, zipfDenominator));
            _rankedAccessCounts[i] = std::min(std::max(accessCount, 1lu), _pathBuffer->_numElemsPerCacheline);
        }

        for (size_t accessCount : _rankedAccessCounts) {
            assert(accessCount <= _pathBuffer->_numElemsPerCacheline);
            _numAccesses += accessCount;
        }
    }
}

template <typename Element>
DistributePathZipf<Element>::DistributePathZipf(const std::shared_ptr<PathBuffer<Element>>& pathBuffer,
                                                size_t numAccesses, int seed)
: DistributePathZipf(pathBuffer, numAccesses, seed, seed)
{}

template <typename Element>
DistributePathZipf<Element>::DistributePathZipf(const std::shared_ptr<PathBuffer<Element>>& pathBuffer,
                                                size_t numAccesses, int seed, int seedCacheline)
: DistributePath<Element>(pathBuffer)
, _seed(seed)
, _seedCacheline(seedCacheline)
, _numAccesses(numAccesses)
{
    // Require each cacheline to be accessed at least once.
    assert(_numAccesses >= _pathBuffer->_numCachelines);

    if (_numAccesses >= _pathBuffer->_numCachelines * _pathBuffer->_numElemsPerCacheline) {
        // Do nothing as compute() should call uniform equal distribution class.
    } else {
        _rankedAccessCounts.resize(_pathBuffer->_numCachelines, 1);

        const size_t maxFreeAccesses = _numAccesses - _pathBuffer->_numCachelines;
        if (maxFreeAccesses <= _pathBuffer->_numElemsPerCacheline - 1) {
            // There are fewer accesses available to assign than are free slots in the first cacheline.
            // Just put all of the elements in the first cacheline. This corresponds to a very large
            // shape parameter for zipf.
            _rankedAccessCounts[0] += maxFreeAccesses;
        } else {
            // Find zipf shape parameter such that the rank 1 cacheline gets the maximum number of accesses that fits.
            double rank1Freq = (double)_pathBuffer->_numElemsPerCacheline / _numAccesses;
            double shape = approximateShapeParam(rank1Freq, _pathBuffer->_numCachelines);
            double zipfDenominator = computeZipfDenominator(shape, _pathBuffer->_numCachelines);

            // We won't be able to perfectly distribute accesses to match the zipf distribution, so assign
            // accesses to ranks to get as close as possible. For as many accesses are still unassigned to a rank,
            // assign an access to the rank whose access frequency is farthest from it's zipf frequency.
            // Preference is given to negative distances.
            std::vector<double> zipfAccessCounts(_pathBuffer->_numCachelines);
            std::map<double,std::set<size_t>> candidates; // maps signed distance from zipf freqency to rank indices
            for (size_t i = 0; i < _pathBuffer->_numCachelines; ++i) {
                double numAccessesFloat = _numAccesses * computeZipf(i + 1 /* rank */, shape, zipfDenominator);
                // Just as a sanity check, cap this value to valid number of entries in cacheline.
                numAccessesFloat = std::min(numAccessesFloat, (double)_pathBuffer->_numElemsPerCacheline);

                zipfAccessCounts[i] = numAccessesFloat;
                candidates[_rankedAccessCounts[i] - numAccessesFloat].emplace(i);
            }
            size_t numFreeAccesses = maxFreeAccesses;
            while (numFreeAccesses > 0) {
                auto it = candidates.begin();
                auto idxIt = it->second.begin();
                size_t i = *idxIt;
                // erasing is safe
                it->second.erase(idxIt);
                if (0 == it->second.size()) {
                    candidates.erase(it);
                }
                _rankedAccessCounts[i] += 1;
                numFreeAccesses -= 1;
                candidates[_rankedAccessCounts[i] - zipfAccessCounts[i]].emplace(i);
            }
        }
    }
}

template <typename Element>
double DistributePathZipf<Element>::computeZipfDenominator(double s, size_t N) {
    double denominator = 0.;
    for (size_t n = 1; n <= N; ++n) {
        denominator += 1. / std::pow(n, s);
    }
    return denominator;
}

template <typename Element>
double DistributePathZipf<Element>::approximateShapeParam(double rank1Freq, size_t N) {
    // TODO: think more about numerical accuracy in this function. It's rather messy.
    static constexpr double SHAPE_MIN = 0.;
    static constexpr double SHAPE_MAX = 10.; // essentually 100%
    static constexpr double SHAPE_STEP = 0.05;
    static_assert(SHAPE_MIN == (size_t)(SHAPE_MIN / SHAPE_STEP) * SHAPE_STEP &&
                  SHAPE_MAX == (size_t)(SHAPE_MAX / SHAPE_STEP) * SHAPE_STEP);
    static std::map<size_t,std::vector<double>> cache; // maps N to rank1 frequencies

    if (0 == cache.count(N)) {
        // Given the number of elements, compute the frequence of the rank 1 element
        // for the predetermined set of shape values.
        size_t numSteps = (size_t)((SHAPE_MAX - SHAPE_MIN) / SHAPE_STEP) + 1;
        cache[N].reserve(numSteps);
        for (size_t i = 0; i < numSteps; ++i) {
            double shape = SHAPE_MIN + i * SHAPE_STEP;
            double rank1FreqGivenShape = 1. / computeZipfDenominator(shape, N);
            cache[N].emplace_back(rank1FreqGivenShape);
        }
    }

    // Use linear interpolation to find the shape parameter whose rank 1 frequency is closest
    // to that specified.
    const std::vector<double>& rank1Freqs = cache[N];
    // It seems straight forward that, as N increases, the above function increases strictly.
    // Numerical precision may affect this, but that should be fine...
    auto it = std::upper_bound(rank1Freqs.begin(), rank1Freqs.end(), rank1Freq);
    if (rank1Freqs.begin() == it) {
        return SHAPE_MIN;
    } else if (rank1Freqs.end() == it) {
        return SHAPE_MAX;
    } else {
        size_t maxShapeIdx = it - rank1Freqs.begin();
        size_t minShapeIdx = maxShapeIdx - 1;
        assert(rank1Freq >= rank1Freqs[minShapeIdx] && rank1Freq <= rank1Freqs[maxShapeIdx]);

        // Perform linear interpolation to approximate fractional shape step.
        double x0 = 0., x1 = 1.;
        double y0 = rank1Freqs[minShapeIdx], y1 = rank1Freqs[maxShapeIdx];
        double fractionalStep = (rank1Freq - y0) * (x1 - x0) / (y1 - y0) + x0;

        return SHAPE_MIN + (SHAPE_STEP * minShapeIdx) + (SHAPE_STEP * fractionalStep);
    }
}

template <typename Element>
double DistributePathZipf<Element>::computeZipf(size_t k, double s, double denominator) {
    assert(k >= 1);
    return 1. / (std::pow((size_t)k, s) * denominator);
}

template <typename Element>
std::shared_ptr<PointerChasingPath<Element>> DistributePathZipf<Element>::compute() {
    if (_numAccesses >= _pathBuffer->_numCachelines * _pathBuffer->_numElemsPerCacheline) {
        DistributePathUniformEqual<Element> helper(_pathBuffer, _seed);
        return helper.run();
    }

#ifndef NDEBUG
    // Sanity check that ranked access counts makes sense.
    assert(_rankedAccessCounts.size() == _pathBuffer->_numCachelines);
    size_t numAccessesComputed = 0;
    for (size_t i = 0; i < _pathBuffer->_numCachelines; ++i) {
        assert(_rankedAccessCounts[i] <= _pathBuffer->_numElemsPerCacheline);
        assert(_rankedAccessCounts[i] > 0);
        assert(i + 1 == _pathBuffer->_numCachelines || _rankedAccessCounts[i] >= _rankedAccessCounts[i + 1]);
        numAccessesComputed += _rankedAccessCounts[i];
    }
    assert(_numAccesses == numAccessesComputed);
#endif

    // Assign a random rank order to the cachelines.
    std::vector<size_t> rankedCachelines(_pathBuffer->_numCachelines);
    for (size_t i = 0; i < _pathBuffer->_numCachelines; ++i) {
        rankedCachelines[i] = i;
    }
    {
        std::mt19937 rng(_seedCacheline);
        std::shuffle(rankedCachelines.begin(), rankedCachelines.end(), rng);
    }

    // Get and shuffle the indices of the buffer elements from which the path will be created.
    std::vector<Value> indices(_numAccesses);
    for (size_t i = 0, rank = 0; rank < _pathBuffer->_numCachelines; ++rank) {
        size_t cacheline = rankedCachelines[rank];
        size_t accessCount = _rankedAccessCounts[rank];
        for (size_t elem = 0; elem < accessCount; ++elem, ++i) {
            indices[i] = _pathBuffer->_indexOffset + (cacheline * MAX_ELEMENTS_PER_CACHELINE) + elem;
        }
    }
    std::mt19937 rng(_seed);
    std::shuffle(indices.begin(), indices.end(), rng);

    // Write path to buffer
    auto path = std::make_shared<PointerChasingPath<Element>>(_pathBuffer);
    auto it = indices.begin();
    Value indexBegin, prevIndex;
    indexBegin = prevIndex = *it++;
    for (; it != indices.end(); ++it) {
        Value nextIndex = *it;
        path->setNext(_pathBuffer->_buffer[prevIndex], 0, nextIndex);
        prevIndex = nextIndex;
    }
    Value indexEnd = prevIndex;
    path->setNext(_pathBuffer->_buffer[indexEnd], 0, indexBegin);

    path->update(indexBegin, indexEnd, indices.size());
    return path;
}

//==============================================================================
/*
template <typename Element>
struct CutPath {
    PointerChasingPath<Element>* _path;
    size_t _steps;

    CutPath(PointerChasingPath<Element>* path, size_t steps)
    : _path(path)
    , _steps(steps)
    {
        assert(_steps < _path->_length - 1);
    }

    std::pair<PointerChasingPath<Element>*,PointerChasingPath<Element>*> run();
};

template<typename Element>
std::pair<PointerChasingPath<Element>*,PointerChasingPath<Element>*> CutPath<Element>::run() {
    using Path = PointerChasingPath<Element>;

    Element prevBufferIndex = _path->_bufferIndexBegin;
    Element prevIndex = _path->_indexBegin;
    for (size_t step = 0; step < _steps; step++) {
        std::tie(prevBufferIndex, prevIndex) = _path->getNextIndices(prevBufferIndex, prevIndex);
    }
    Element nextBufferIndex, nextIndex;
    std::tie(nextBufferIndex, nextIndex) = _path->getNextIndices(prevBufferIndex, prevIndex);

    // Close new paths
    Path::setNext(_path->getElement(prevBufferIndex, prevIndex), _path->_bufferIndexBegin, _path->_indexBegin);
    Path::setNext(_path->getElement(_path->_bufferIndexEnd, _path->_indexEnd), nextBufferIndex, nextIndex);

    // TODO: Reduce BufferPaths used to create new PointerChasingPaths in case the cut
    // removed all elements of a PointerChasingPath.
    Path* newPath1 = new Path(_path->_pathBuffers, _path->_numBuffers,
                              _path->_bufferIndexBegin, _path->_indexBegin, prevBufferIndex, prevIndex, _steps + 1);
    Path* newPath2 = new Path(_path->_pathBuffers, _path->_numBuffers,
                              nextBufferIndex, nextIndex, _path->_bufferIndexEnd, _path->_indexEnd, _path->_length - (_steps + 1));

    return std::make_pair(newPath1, newPath2);
}
*/
//==============================================================================

template <typename Element>
class MergePaths {
private:
    std::shared_ptr<PointerChasingPath<Element>> _result;

    void initMerge();

protected:
    using Value = typename Element::Value;

    std::shared_ptr<PointerChasingPath<Element>> _path1;
    std::shared_ptr<PointerChasingPath<Element>> _path2;

    // Created to use PointerChasingPath helper functions
    std::shared_ptr<PointerChasingPath<Element>> _merged;
    size_t _length;

    // The buffer indices of some elements may need to be relabeled in order to merge the paths.
    // Traverse paths using below start and end values instead of those provided by each path instance.
    Value _path1BufferIndexBegin;
    Value _path1IndexBegin;
    Value _path1BufferIndexEnd;
    Value _path1IndexEnd;

    Value _path2BufferIndexBegin;
    Value _path2IndexBegin;
    Value _path2BufferIndexEnd;
    Value _path2IndexEnd;

    virtual std::shared_ptr<PointerChasingPath<Element>> compute() = 0;

public:
    MergePaths(const std::shared_ptr<PointerChasingPath<Element>>& path1,
               const std::shared_ptr<PointerChasingPath<Element>>& path2);

    std::shared_ptr<PointerChasingPath<Element>> run() {
        if (nullptr == _result) {
            initMerge();
            _result = compute();
        }
        return _result;
    }
};

template <typename Element>
void MergePaths<Element>::initMerge() {
#ifndef NDEBUG
    {
        // Assert that no path buffers overlap.
        std::vector<std::shared_ptr<PathBuffer<Element>>> allPathBuffers;
        auto addPathBuffers = [&allPathBuffers](const auto& pathBuffers) {
            for (const auto& pathBufSet : pathBuffers) {
                for (const auto& pathBuf : pathBufSet) {
                    allPathBuffers.emplace_back(pathBuf);
                }
            }
        };
        addPathBuffers(_path1->_pathBuffers);
        addPathBuffers(_path2->_pathBuffers);

        for (size_t i = 0; i < allPathBuffers.size(); ++i) {
            for (size_t j = i + 1; j < allPathBuffers.size(); ++j) {
                assert(!allPathBuffers[i]->overlaps(allPathBuffers[j]));
            }
        }
    }
#endif

    std::vector<std::unordered_set<std::shared_ptr<PathBuffer<Element>>>> pathBuffers;

    // Create mapping from buffer pointer to buffer index by first adding buffers from path1.
    std::unordered_map<Element*,Value> bufferIndexMap;
    for (size_t bufIdx = 0; bufIdx < _path1->_buffers.size(); ++bufIdx) {
        Element* buf = _path1->getBuffer(bufIdx);
        assert(bufferIndexMap.end() == bufferIndexMap.find(buf));
        bufferIndexMap[buf] = bufIdx;
        pathBuffers.emplace_back(_path1->_pathBuffers[bufIdx]);
    }
    // Then add all new buffers from path2 with updated buffer index.
    bool updatePath2BufferIndices = false;
    std::unordered_map<Value,Value> path2BufferIndexRemap;
    for (size_t bufIdx = 0; bufIdx < _path2->_buffers.size(); ++bufIdx) {
        Element* buf = _path2->getBuffer(bufIdx);
        if (bufferIndexMap.end() == bufferIndexMap.find(buf)) {
            bufferIndexMap[buf] = bufferIndexMap.size();
            pathBuffers.emplace_back();
        }
        size_t newBufIdx = bufferIndexMap[buf];
        path2BufferIndexRemap[bufIdx] = newBufIdx;
        updatePath2BufferIndices = updatePath2BufferIndices || (bufIdx != newBufIdx);
        pathBuffers[newBufIdx].insert(_path2->_pathBuffers[bufIdx].begin(), _path2->_pathBuffers[bufIdx].end());
    }

    // Update buffer index in the elements as a result of the above change in path2's
    // buffer indices.
    if (updatePath2BufferIndices) {
        Element* element = &_path2->getElement(_path2->_bufferIndexBegin, _path2->_indexBegin);
        for (size_t i = 0; i < _path2->_length; i++) {
            Value oldBufIdx = element->getBufferIndex();
            Value newBufIdx = path2BufferIndexRemap[oldBufIdx];
            Element* nextElement = &_path2->getNextElement(*element);
            if (oldBufIdx != newBufIdx) {
                element->setBufferIndex(newBufIdx);
            }
            element = nextElement;
        }
    }

    // Try to merge PathBuffers as much as possible.
    for (std::unordered_set<std::shared_ptr<PathBuffer<Element>>>& buffers : pathBuffers) {
        bool performedMerge;
        do {
            performedMerge = false;
            for (auto it1 = buffers.begin(); !performedMerge && it1 != buffers.end(); ++it1) {
                for (auto it2 = std::next(it1); !performedMerge && it2 != buffers.end(); ++it2) {
                    std::shared_ptr<PathBuffer<Element>> merge = PathBuffer<Element>::merge(*it1, *it2);
                    if (nullptr != merge) {
                        // Iterators to non-erase elements are still valid.
                        buffers.erase(it1);
                        buffers.erase(it2);
                        buffers.emplace(merge);
                        performedMerge = true;
                    }
                }
            }
        } while (performedMerge);
    }

    // Store info to help subclass perform merge.
    _path1BufferIndexBegin = _path1->_bufferIndexBegin;
    _path1IndexBegin = _path1->_indexBegin;
    _path1BufferIndexEnd = _path1->_bufferIndexEnd;
    _path1IndexEnd = _path1->_indexEnd;
    _path2BufferIndexBegin = path2BufferIndexRemap[_path2->_bufferIndexBegin];
    _path2IndexBegin = _path2->_indexBegin;
    _path2BufferIndexEnd = path2BufferIndexRemap[_path2->_bufferIndexEnd];
    _path2IndexEnd = _path2->_indexEnd;

    _merged = std::make_shared<PointerChasingPath<Element>>(pathBuffers);
}

template <typename Element>
MergePaths<Element>::MergePaths(const std::shared_ptr<PointerChasingPath<Element>>& path1,
                                const std::shared_ptr<PointerChasingPath<Element>>& path2)
: _result(nullptr)
, _path1(path1)
, _path2(path2)
, _merged(nullptr)
, _length(_path1->_length + _path2->_length)
{}


//==============================================================================

template <typename Element>
class CatPaths : public MergePaths<Element> {
private:
    // See https://stackoverflow.com/a/1121016/6573510
    using MergePaths<Element>::_merged;
    using MergePaths<Element>::_length;
    using MergePaths<Element>::_path1BufferIndexBegin;
    using MergePaths<Element>::_path1IndexBegin;
    using MergePaths<Element>::_path1BufferIndexEnd;
    using MergePaths<Element>::_path1IndexEnd;
    using MergePaths<Element>::_path2BufferIndexBegin;
    using MergePaths<Element>::_path2IndexBegin;
    using MergePaths<Element>::_path2BufferIndexEnd;
    using MergePaths<Element>::_path2IndexEnd;

    std::shared_ptr<PointerChasingPath<Element>> compute();

public:
    CatPaths(const std::shared_ptr<PointerChasingPath<Element>>& path1,
             const std::shared_ptr<PointerChasingPath<Element>>& path2)
    : MergePaths<Element>(path1, path2)
    {}
};

template <typename Element>
std::shared_ptr<PointerChasingPath<Element>> CatPaths<Element>::compute() {
    using Path = PointerChasingPath<Element>;

    _merged->setNext(_merged->getElement(_path1BufferIndexEnd, _path1IndexEnd),
                     _path2BufferIndexBegin, _path2IndexBegin);
    _merged->setNext(_merged->getElement(_path2BufferIndexEnd, _path2IndexEnd),
                     _path1BufferIndexBegin, _path1IndexBegin);

    return std::make_shared<Path>(_merged->_pathBuffers, _path1BufferIndexBegin, _path1IndexBegin,
                                  _path2BufferIndexEnd, _path2IndexEnd, _length);
}

//==============================================================================

template <typename Element>
class MergePathsPeriodic : public MergePaths<Element> {
private:
    using Value = typename Element::Value;
    // See https://stackoverflow.com/a/1121016/6573510
    using MergePaths<Element>::_path1;
    using MergePaths<Element>::_path2;
    using MergePaths<Element>::_merged;
    using MergePaths<Element>::_length;
    using MergePaths<Element>::_path1BufferIndexBegin;
    using MergePaths<Element>::_path1IndexBegin;
    using MergePaths<Element>::_path1BufferIndexEnd;
    using MergePaths<Element>::_path1IndexEnd;
    using MergePaths<Element>::_path2BufferIndexBegin;
    using MergePaths<Element>::_path2IndexBegin;
    using MergePaths<Element>::_path2BufferIndexEnd;
    using MergePaths<Element>::_path2IndexEnd;

    std::shared_ptr<PointerChasingPath<Element>> compute();

public:
    MergePathsPeriodic(const std::shared_ptr<PointerChasingPath<Element>>& path1,
                       const std::shared_ptr<PointerChasingPath<Element>>& path2)
    : MergePaths<Element>(path1, path2)
    {}
};

template <typename Element>
std::shared_ptr<PointerChasingPath<Element>> MergePathsPeriodic<Element>::compute() {
    using Path = PointerChasingPath<Element>;

    size_t numPhases = std::min(_path1->_length, _path2->_length);
    size_t basePhaseSteps1 = _path1->_length / numPhases;
    size_t remainderPhaseSteps1 = _path1->_length % numPhases;
    size_t basePhaseSteps2 = _path2->_length / numPhases;
    size_t remainderPhaseSteps2 = _path2->_length % numPhases;

    Value prevBufferIndex1 = _path1BufferIndexBegin;
    Value prevIndex1 = _path1IndexBegin;
    Value prevBufferIndex2 = _path2BufferIndexBegin;
    Value prevIndex2 = _path2IndexBegin;
    for (size_t phase = 0; phase < numPhases; ++phase) {
        size_t phaseSteps1 = basePhaseSteps1 + (remainderPhaseSteps1 > 0 ? (--remainderPhaseSteps1, 1) : 0);
        for (size_t step1 = 1; step1 < phaseSteps1; ++step1) {
            std::tie(prevBufferIndex1, prevIndex1) = _merged->getNextIndices(prevBufferIndex1, prevIndex1);
        }

        Element& element1 = _merged->getElement(prevBufferIndex1, prevIndex1);
        std::tie(prevBufferIndex1, prevIndex1) = _merged->getNextIndices(element1);
        _merged->setNext(element1, prevBufferIndex2, prevIndex2);

        size_t phaseSteps2 = basePhaseSteps2 + (remainderPhaseSteps2 > 0 ? (--remainderPhaseSteps2, 1) : 0);
        for (size_t step2 = 1; step2 < phaseSteps2; ++step2) {
            std::tie(prevBufferIndex2, prevIndex2) = _merged->getNextIndices(prevBufferIndex2, prevIndex2);
        }

        Element& element2 = _merged->getElement(prevBufferIndex2, prevIndex2);
        std::tie(prevBufferIndex2, prevIndex2) = _merged->getNextIndices(element2);
        _merged->setNext(element2, prevBufferIndex1, prevIndex1);
    }

    _merged->setNext(_merged->getElement(_path2BufferIndexEnd, _path2IndexEnd),
                     _path1BufferIndexBegin, _path1IndexBegin);

    return std::make_shared<Path>(_merged->_pathBuffers, _path1BufferIndexBegin, _path1IndexBegin,
                                  _path2BufferIndexEnd, _path2IndexEnd, _length);
}

//==============================================================================

// TODO: Version that sets read/write of a path according to cacheline
// TODO: Add ability to get number of writes actually put in path.
template <typename Element>
class ReadWritePath {
private:
    bool wasRun;

    virtual void compute() = 0;

protected:
    std::shared_ptr<PointerChasingPath<Element>> _path;

public:
    ReadWritePath(const std::shared_ptr<PointerChasingPath<Element>>& path)
    : wasRun(false)
    , _path(path)
    {}

    void run() {
        if (!wasRun) {
            compute();
            wasRun = true;
        }
    }
};

//==============================================================================

template <typename Element>
class ReadWritePathPeriodic : public ReadWritePath<Element> {
private:
    using Value = typename Element::Value;
    // See https://stackoverflow.com/a/1121016/6573510
    using ReadWritePath<Element>::_path;

    double _updateRatio;

    void compute();

public:
    ReadWritePathPeriodic(const std::shared_ptr<PointerChasingPath<Element>>& path,
                          double updateRatio)
    : ReadWritePath<Element>(path)
    , _updateRatio(updateRatio)
    {}
};

template <typename Element>
void ReadWritePathPeriodic<Element>::compute() {
    size_t numWrites = _updateRatio * _path->_length;
    size_t numReads = _path->_length - numWrites;

    size_t numPhases = std::max(std::min(numWrites, numReads), (size_t)1);
    size_t numPhaseWriteSteps = numWrites / numPhases;
    size_t remainderPhaseWriteSteps = numWrites % numPhases;
    size_t numPhaseReadSteps = numReads / numPhases;
    size_t remainderPhaseReadSteps = numReads % numPhases;

    Value prevBufferIndex = _path->_bufferIndexBegin;
    Value prevIndex = _path->_indexBegin;
    for (size_t phase = 0; phase < numPhases; phase++) {
        for (size_t step = 0; step < numPhaseWriteSteps + (remainderPhaseWriteSteps > 0 ? (remainderPhaseWriteSteps--, 1) : 0); step++) {
            Element& element = _path->getElement(prevBufferIndex, prevIndex);
            element.setWrite();
            std::tie(prevBufferIndex, prevIndex) = _path->getNextIndices(element);
        }
        for (size_t step = 0; step < numPhaseReadSteps + (remainderPhaseReadSteps > 0 ? (remainderPhaseReadSteps--, 1) : 0); step++) {
            Element& element = _path->getElement(prevBufferIndex, prevIndex);
            element.setRead();
            std::tie(prevBufferIndex, prevIndex) = _path->getNextIndices(element);
        }
    }
}

//==============================================================================

template <typename Element>
class ReadWritePathUniformRandom : public ReadWritePath<Element> {
private:
    using Value = typename Element::Value;
    // See https://stackoverflow.com/a/1121016/6573510
    using ReadWritePath<Element>::_path;

    double _updateRatio;
    int _seed;

    void compute();

public:
    ReadWritePathUniformRandom(const std::shared_ptr<PointerChasingPath<Element>>& path,
                               double updateRatio, int seed)
    : ReadWritePath<Element>(path)
    , _updateRatio(updateRatio)
    , _seed(seed)
    {}
};

template <typename Element>
void ReadWritePathUniformRandom<Element>::compute() {
    size_t numWrites = 0;
    if (_updateRatio > 0.) {
        numWrites = std::max((size_t)std::round(_updateRatio * _path->_length), 1lu);
    }

    // Pick numWrites locations along the path to set as write, all else to read.
    std::vector<size_t> writeStepIndices;
    if (numWrites > 0) {
        writeStepIndices.resize(_path->_length);
        for (size_t i = 0; i < _path->_length; ++i) {
            writeStepIndices[i] = i;
        }
        std::mt19937 g(_seed);
        std::shuffle(writeStepIndices.begin(), writeStepIndices.end(), g);
        writeStepIndices.erase(std::next(writeStepIndices.begin(), numWrites), writeStepIndices.end());
        assert(writeStepIndices.size() == numWrites);
    }

    // Traverse the path, setting elements to write if in list, else read.
    std::sort(writeStepIndices.begin(), writeStepIndices.end());
    size_t writeIdx = 0;
    Value prevBufIdx = _path->_bufferIndexBegin;
    Value prevIdx = _path->_indexBegin;
    for (size_t pathStepIdx = 0; pathStepIdx < _path->_length; ++pathStepIdx) {
        if (writeIdx < writeStepIndices.size() && pathStepIdx == writeStepIndices[writeIdx]) {
            _path->getElement(prevBufIdx, prevIdx).setWrite();
            writeIdx += 1;
        } else {
            _path->getElement(prevBufIdx, prevIdx).setRead();
        }
        std::tie(prevBufIdx, prevIdx) = _path->getNextIndices(prevBufIdx, prevIdx);
    }
    assert(writeStepIndices.size() == writeIdx);
}

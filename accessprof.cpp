
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <inttypes.h>
#include <cassert>
#include <iostream>
#include <vector>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <string>
#include <fstream>
#include <algorithm>
#include <chrono>
#include <memory>
#include <regex>
#include <cmath>

const size_t DEFAULT_CACHELINE_SIZE_BYTES = 64;
const size_t DEFAULT_MIN_SAMPLES_PER_DATALOC = 1;

const int DEFAULT_MAX_LOG_LEVEL = 0;
const long DEFAULT_LOG_INTERVAL_MS = 1000;

const double BUCKET_ACCESS_COUNT_LOG_BASE = 2.;
const double BUCKET_ACCESS_UPDATE_RATIO_STEP = 0.1;

constexpr bool systemIsBigEndian();
template <typename T> T changeEndianness(T t);

struct Sample {
    enum Type {
        // Corresponds with those found in extract_samples_cpp.py
        _MIN = 0,
        LOAD = 0,
        STORE = 1,
        _MAX = 1
    };

    static size_t _cachelineSizeBytes;

    Type _type;
    uintptr_t _addr;
    uintptr_t _sp;
    std::unordered_set<std::string> _callchainSet;

    static void setCachelineSize(size_t cachelineSizeBytes) {
        assert(cachelineSizeBytes > 0 && 0 == ((cachelineSizeBytes - 1) & cachelineSizeBytes));
        _cachelineSizeBytes = cachelineSizeBytes;
    }

    Sample(Type type, uintptr_t addr, uintptr_t sp, const std::vector<std::string>& callchain)
    : _type(type)
    , _addr(addr)
    , _sp(sp)
    , _callchainSet(callchain.begin(), callchain.end())
    {
        assert((unsigned int)_MIN <= (unsigned int)type &&
               (unsigned int)type <= (unsigned int)_MAX);
        assert((void*)_addr != nullptr);
        assert((void*)_sp != nullptr);
        assert(_callchainSet.size() > 0);
    }

    uintptr_t dataloc() const {
        assert(0 != _cachelineSizeBytes);
        return _addr & ~(_cachelineSizeBytes - 1);
    }
};
size_t Sample::_cachelineSizeBytes = 0;
using SamplePtr = std::shared_ptr<Sample>;

// TODO: Check for invalid sessions during extraction
struct Session {
    uintptr_t _stackBase;
    unsigned int _stackSize;

    Session(uintptr_t stackBase, unsigned int stackSize)
    : _stackBase(stackBase)
    , _stackSize(stackSize)
    {
        assert((void*)_stackBase != nullptr);
        assert(stackSize > 0);
    }
};
using SessionPtr = std::shared_ptr<Session>;

struct SessionSet {
    std::vector<SessionPtr> _sessions;
    std::vector<uintptr_t> _sessionStackCeils;

    template <typename T>
    SessionSet(T&& sessions)
    : _sessions(std::forward<T>(sessions))
    {
        assert(_sessions.size() > 0);
        for (const SessionPtr& session : _sessions) {
            assert(nullptr != session);
        }

        // Sort sessions by stack base
        std::sort(_sessions.begin(), _sessions.end(),
                [](const SessionPtr& s1, const SessionPtr& s2) -> bool {
                    return s1->_stackBase < s2->_stackBase;
                });
        // Assert that stacks don't overlap
        for (size_t i = 0; i < _sessions.size() - 1; i++) {
            assert(_sessions[i]->_stackBase + _sessions[i]->_stackSize <= _sessions[i + 1]->_stackBase);
        }

        _sessionStackCeils.reserve(_sessions.size());
        std::for_each(_sessions.begin(), _sessions.end(),
                [this](const SessionPtr& sess) {
                    _sessionStackCeils.emplace_back(sess->_stackBase + sess->_stackSize);
                });
    }

    SessionPtr match(uintptr_t stackAddr) const {
        auto it = std::upper_bound(_sessionStackCeils.begin(), _sessionStackCeils.end(), stackAddr);
        SessionPtr rval = nullptr;
        if (_sessionStackCeils.end() != it) {
            const SessionPtr& session = _sessions[it - _sessionStackCeils.begin()];
            if (stackAddr >= session->_stackBase && stackAddr < session->_stackBase + session->_stackSize) {
                rval = session;
            }
        }
        return rval;
    }

    size_t size() const {
        return _sessions.size();
    }
};

using DatalocClassifValue = unsigned int;
enum DatalocClassif : DatalocClassifValue {
    // Values assumed to start at 0 and be contiguous.
    _MIN = 0,
    PHASE_LOCAL_SESS_LOCAL = 0,
    PHASE_LOCAL_SESS_GLOBAL = 1,
    PHASE_GLOBAL_SESS_LOCAL = 2,
    PHASE_GLOBAL_SESS_GLOBAL = 3,
    _MAX = 3
};
DatalocClassif& operator++(DatalocClassif& classif) {
    classif = (DatalocClassif)((DatalocClassifValue)classif + 1);
    return classif;
}

inline std::string datalocClassifToString(DatalocClassif classif) {
    switch(classif) {
        case DatalocClassif::PHASE_LOCAL_SESS_LOCAL:
            return std::string("Phase Local  Session Local ");
            break;
        case DatalocClassif::PHASE_LOCAL_SESS_GLOBAL:
            return std::string("Phase Local  Session Global");
            break;
        case DatalocClassif::PHASE_GLOBAL_SESS_LOCAL:
            return std::string("Phase Global Session Local ");
            break;
        case DatalocClassif::PHASE_GLOBAL_SESS_GLOBAL:
            return std::string("Phase Global Session Global");
            break;
        default:
            abort();
    };
}

template <typename Format, typename ...Args>
struct DebugLoggerHelper {
    static int printf(Format&& format, Args&&... args) {
        return ::printf(format, args...);
    }
};

template <typename Format>
struct DebugLoggerHelper<Format> {
    static int printf(Format&& format) {
        return ::printf("%s", format);
    }
};

// TODO: make logger global, have functions create subloggers if they want to log timestamps.
struct DebugLogger {
    static const long _startTimeMs;
    static long _logIntervalMs;
    static int _maxLogLevel;

    long _prevLogTimeMs;

    DebugLogger()
    : _prevLogTimeMs(0)
    {}

    template <typename ...Args>
    int printfInternal(int logLevel, bool ignoreTime, Args&&... args) {
        using namespace std::chrono;
        if (!checkLevel(logLevel)) return 0;

        int rval = 0;
        long curTimeMs = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        if (ignoreTime || checkTime(curTimeMs)) {
            _prevLogTimeMs = curTimeMs;

            rval = ::printf("[%.3lfs] ", (double)(curTimeMs - _startTimeMs) / 1000);
            if (rval <= 0) return rval;

            int rval2 = DebugLoggerHelper<Args...>::printf(std::forward<Args>(args)...);
            if (rval2 <= 0) return rval2; else rval += rval2;
            fflush(stdout);
        }
        return rval;
    }

    template <typename ...Args>
    int printf(int logLevel, Args&&... args) {
        return printfInternal(logLevel, false, std::forward<Args>(args)...);
    }

    template <typename ...Args>
    int printfForce(int logLevel, Args&&... args) {
        return printfInternal(logLevel, true, std::forward<Args>(args)...);
    }

    template <typename ...Args>
    int printfForceChain(int logLevel, Args&&... args) {
        if (!checkLevel(logLevel)) return 0;
        else return DebugLoggerHelper<Args...>::printf(std::forward<Args>(args)...);
    }

    bool checkLevel(int logLevel) {
        return logLevel <= _maxLogLevel;
    }

    bool checkTime(long curTimeMs = -1) {
        using namespace std::chrono;
        if (-1 == curTimeMs) {
            curTimeMs = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        }
        return curTimeMs - _prevLogTimeMs >= _logIntervalMs;
    }
};
const long DebugLogger::_startTimeMs = std::chrono::duration_cast<std::chrono::milliseconds>(
        std::chrono::system_clock::now().time_since_epoch()).count();
int DebugLogger::_maxLogLevel = DEFAULT_MAX_LOG_LEVEL;
long DebugLogger::_logIntervalMs = DEFAULT_LOG_INTERVAL_MS;

constexpr bool systemIsBigEndian() {
    // https://stackoverflow.com/a/1001373/6573510
    union {
        uint16_t i;
        uint8_t b[2];
    } data = {.i = 0x0102};
    return data.b[0] == 1; 
}

template <typename T>
T changeEndianness(T t) {
    union {
        T t;
        uint8_t b[sizeof(T)];
    } data = {.t = t};
    for (size_t i = 0, j = sizeof(T) - 1; i < j; i++, j--) {
        uint8_t tmp = data.b[i];
        data.b[i] = data.b[j];
        data.b[j] = tmp;
    }
    return data.t;
}

// https://stackoverflow.com/a/217605
inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
}

inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

template <typename T>
T readInt(std::fstream& fs) {
    T t;
    fs.read((char*)&t, sizeof(T));
    assert(fs.gcount() == sizeof(T));
    if (!systemIsBigEndian()) {
        return changeEndianness(t);
    }
    return t;
}

std::string readStr(std::fstream& fs, size_t len) {
    char* buf = new char[len + 1];
    fs.read(buf, len);
    buf[len] = '\0';
    assert(fs.gcount() >= 0 && (size_t)fs.gcount() == len);

    std::string str(buf);
    delete[] buf;
    return str;
}

std::vector<SamplePtr> loadSamples(std::string filepath) {
    DebugLogger log;
    std::fstream fs(filepath, std::ios_base::in | std::ios_base::binary);
    assert(fs.good());
    fs.seekg(0);

    unsigned int numSamples = readInt<unsigned int>(fs);
    std::vector<SamplePtr> samples(numSamples);
    for (unsigned int i = 0; i < numSamples; i++) {
        Sample::Type type = (Sample::Type)readInt<unsigned char>(fs);
        uintptr_t addr = (uintptr_t)readInt<unsigned long long>(fs);
        uintptr_t sp = (uintptr_t)readInt<unsigned long long>(fs);

        unsigned int callchainLen = readInt<unsigned int>(fs);
        std::vector<unsigned int> callLens(callchainLen);
        for (unsigned int j = 0; j < callchainLen; j++) {
            callLens[j] = readInt<unsigned int>(fs);
        }
        std::vector<std::string> callchain(callchainLen);
        for (unsigned int j = 0; j < callchainLen; j++) {
            callchain[j] = std::move(readStr(fs, callLens[j])); 
        }

        samples[i] = std::make_shared<Sample>(type, addr, sp, callchain);
        log.printf(2, "Loading %u/%u samples...\r", i + 1, numSamples);
    }
    assert(fs.peek() == EOF);
    fs.close();

    log.printfForce(2, "Loading %u/%u samples...done\n", numSamples, numSamples);
    return samples;
}

SessionSet loadSessions(std::string filepath) {
    DebugLogger log;
    std::fstream fs(filepath, std::ios_base::in | std::ios_base::binary);
    assert(fs.good());
    fs.seekg(0);

    unsigned int numSessions = readInt<unsigned int>(fs);
    std::vector<SessionPtr> sessions(numSessions);
    for (unsigned int i = 0; i < numSessions; ++i) {
        uintptr_t stackBase = (uintptr_t)(uintptr_t)readInt<unsigned long long>(fs);
        unsigned int stackSize = readInt<unsigned int>(fs);

        sessions[i] = std::make_shared<Session>(stackBase, stackSize);
        log.printf(2, "Loading %u/%u sessions...\r", i + 1, numSessions);
    }
    assert(fs.peek() == EOF);
    fs.close();

    log.printfForce(2, "Loading %u/%u sessions...done\n", numSessions, numSessions);
    return SessionSet(std::move(sessions));
}

// TODO: make multithreaded
template <typename Element, typename UnaryPredicate>
std::vector<Element> filter(std::vector<Element>& elements, UnaryPredicate&& pred,
                            const char* debugElementName, const char* debugFilterMethod)
{
    DebugLogger log;
    std::vector<Element> filteredElements;

    size_t i = 0;
    for (Element& element : elements) {
        if (pred(element)) {
            elements[i++] = std::move(element);
        } else {
            filteredElements.emplace_back(std::move(element));
        }
        log.printf(2, "Filtering %zu/%zu %s by %s...\r",
                   i + filteredElements.size(), elements.size(),
                   debugElementName, debugFilterMethod);
    }
    elements.erase(elements.begin() + i, elements.end());
    log.printfForce(2, "Filtering %zu/%zu %s by %s...done\n",
                    elements.size(), elements.size(),
                    debugElementName, debugFilterMethod);

    return filteredElements;
}

std::unordered_map<uintptr_t,std::vector<SamplePtr>> mapDatalocToSamples(const std::vector<SamplePtr>& samples) {
    DebugLogger log;
    std::unordered_map<uintptr_t,std::vector<SamplePtr>> datalocSamplesMap;

    size_t i = 0;
    for (const SamplePtr& sample : samples) {
        datalocSamplesMap[sample->dataloc()].emplace_back(sample);
        log.printf(2, "Mapping %zu/%zu samples to dataloc...\r", ++i, samples.size());
    }

    log.printf(2, "Mapping %zu/%zu samples to dataloc...done\n", samples.size(), samples.size());
    return datalocSamplesMap;
}

void filterSamples(std::vector<SamplePtr>& samples, size_t minSamplesPerDataloc, const std::string& callchainFilter) {
    DebugLogger log;

    // Filter out samples if corresponding data location doesn't contain enough samples.
    if (minSamplesPerDataloc > 1) {
        std::unordered_map<uintptr_t,std::vector<SamplePtr>> datalocSamplesMap = mapDatalocToSamples(samples);
        std::vector<SamplePtr> filteredSamples = filter(samples,
                [&datalocSamplesMap,minSamplesPerDataloc](const SamplePtr& sample) {
                    size_t numSamples = datalocSamplesMap[sample->dataloc()].size();
                    assert(numSamples > 0);
                    return numSamples >= minSamplesPerDataloc;
                }, "samples", "dataloc");
        log.printfForce(1, "Filter Samples By Data Location: %zu samples dropped, %zu samples remain\n",
                        filteredSamples.size(), samples.size());
    }

    // Filter samples if callchain contains function name matching regex.
    if (callchainFilter.size() > 0) {
        std::regex ccRegex(callchainFilter);
        bool collectMatchedCalls = log.checkLevel(3);
        std::set<std::string> matchedCalls;
        std::vector<SamplePtr> filteredSamples = filter(samples,
                [&ccRegex,&matchedCalls,collectMatchedCalls](const SamplePtr& sample) {
                    for (const std::string& call : sample->_callchainSet) {
                        if (std::regex_match(call, ccRegex)) {
                            if (collectMatchedCalls) matchedCalls.emplace(call);
                            return false;
                        }
                    }
                    return true;
                }, "samples", "callchain filter");
        log.printfForce(1, "Filter Samples By Callchain Filter: %zu samples dropped, %zu samples remain\n",
                        filteredSamples.size(), samples.size());

        if (collectMatchedCalls) {
            log.printfForce(3, "Samples filtered based calls:");
            for (const std::string& call : matchedCalls) {
                log.printfForceChain(3, " %s", call.c_str());
            }
            log.printfForceChain(3, "\n");
        }
    }
}

std::map<std::string,std::pair<size_t,size_t>> mapFunctionToCounts(
        const std::unordered_map<uintptr_t,std::vector<SamplePtr>>& datalocSamplesMap)
{
    DebugLogger log;
    std::map<std::string,std::pair<size_t,size_t>> funcCountsMap;

    size_t i = 0;
    for (auto& datalocSamplesPair : datalocSamplesMap) {
        const std::vector<SamplePtr>& samples = datalocSamplesPair.second;

        // Map functions to samples
        std::unordered_map<std::string,std::vector<SamplePtr>> funcSamplesMap;
        for (const SamplePtr& sample : samples) {
            for (const std::string& call : sample->_callchainSet) {
                funcSamplesMap[call].emplace_back(sample);
            }
        }

        // Add sample counts and increment dataloc count for each function
        for (const auto& pair : funcSamplesMap) {
            funcCountsMap[pair.first].first += pair.second.size();
            funcCountsMap[pair.first].second += 1;
        }

        log.printf(2, "Processing %zu/%zu datalocs for function-counts...\r", ++i, datalocSamplesMap.size());
    }
    log.printfForce(2, "Processing %zu/%zu datalocs for function-counts...done\n", datalocSamplesMap.size(),
                         datalocSamplesMap.size());
    return funcCountsMap;
}

std::unordered_map<uintptr_t,SessionSet> mapDatalocToSessions(const std::unordered_map<uintptr_t,std::vector<SamplePtr>>& datalocSamplesMap,
                                                              const SessionSet& sessionSet)
{
    DebugLogger log;
    std::unordered_map<uintptr_t,SessionSet> datalocSessionsMap;

    size_t i = 0;
    for (auto& pair : datalocSamplesMap) {
        uintptr_t dataloc = pair.first;
        std::unordered_set<SessionPtr> sessions;
        for (const SamplePtr& sample : pair.second) {
            SessionPtr session = sessionSet.match(sample->_sp);
            if (nullptr != session) sessions.emplace(session);
        }
        datalocSessionsMap.emplace(std::piecewise_construct,
                                   std::forward_as_tuple(dataloc),
                                   std::forward_as_tuple(std::vector<SessionPtr>(sessions.begin(), sessions.end())));
        log.printf(2, "Mapping %zu/%zu datalocs to session sets...\r", ++i, datalocSamplesMap.size());
    }
    log.printfForce(2, "Mapping %zu/%zu datalocs to session sets...done\n", datalocSamplesMap.size(),
                         datalocSamplesMap.size());
    return datalocSessionsMap;
}

std::pair<std::unordered_map<uintptr_t,std::vector<SamplePtr>>,std::unordered_map<uintptr_t,std::vector<SamplePtr>>>
        groupSamplesByPhase(const std::unordered_map<uintptr_t,std::vector<SamplePtr>>& datalocSamplesMap,
                            const std::string& phaseFunctionName, size_t numSamples)
{
    DebugLogger log;
    std::unordered_map<uintptr_t,std::vector<SamplePtr>> phaseDatalocSamplesMap, extraPhaseDatalocSamplesMap;

    // Possible optimiation is to precompute hash for phaseFunctionName
    size_t i = 0;
    for (auto pair : datalocSamplesMap) {
        for (const SamplePtr& sample : pair.second) {
            if (sample->_callchainSet.count(phaseFunctionName) > 0) {
                phaseDatalocSamplesMap[pair.first].emplace_back(sample);
            } else {
                extraPhaseDatalocSamplesMap[pair.first].emplace_back(sample);
            }
            log.printf(2, "Grouping %zu/%zu samples by phase...\r", ++i, numSamples);
        }
    }
    log.printfForce(2, "Grouping %zu/%zu samples by phase...done\n", numSamples, numSamples);
    return std::make_pair(phaseDatalocSamplesMap, extraPhaseDatalocSamplesMap);
}

std::array<std::vector<uintptr_t>,4> classifyDatalocs(const std::unordered_map<uintptr_t,std::vector<SamplePtr>>& phaseDatalocSamplesMap,
                                                      const std::unordered_map<uintptr_t,std::vector<SamplePtr>>& extraPhaseDatalocSamplesMap,
                                                      const std::unordered_map<uintptr_t,SessionSet>& datalocSessionsMap)
{
    DebugLogger log;
    std::array<std::vector<uintptr_t>,4> classifiedDatalocs;

    size_t i = 0;
    for (auto& pair : phaseDatalocSamplesMap) {
        uintptr_t dataloc = pair.first;

        bool isSessGlobal = (datalocSessionsMap.find(dataloc)->second.size() > 1);
        bool isPhaseGlobal = (extraPhaseDatalocSamplesMap.count(dataloc) > 0);
        DatalocClassif classif;
        if (isPhaseGlobal && isSessGlobal) {
            classif = PHASE_GLOBAL_SESS_GLOBAL;
        } else if (isPhaseGlobal) {
            classif = PHASE_GLOBAL_SESS_LOCAL;
        } else if (isSessGlobal) {
            classif = PHASE_LOCAL_SESS_GLOBAL;
        } else {
            classif = PHASE_LOCAL_SESS_LOCAL;
        }
        classifiedDatalocs[classif].emplace_back(dataloc);
        log.printf(2, "Classifying %zu/%zu datalocs...\r", ++i, phaseDatalocSamplesMap.size());
    }
    log.printfForce(2, "Classifying %zu/%zu datalocs...done\n", phaseDatalocSamplesMap.size(),
                         phaseDatalocSamplesMap.size());
    return classifiedDatalocs;
}

template <typename ...Args> size_t countMappedSamples(Args...args);
template <typename Map>
size_t countMappedSamples(const Map& map) {
    size_t numSamples = 0;
    for (auto& pair : map) {
        numSamples += pair.second.size();
    }
    return numSamples;
}
template <typename Map, typename Keys>
size_t countMappedSamples(const Map& map, const Keys& keys) {
    size_t numSamples = 0;
    for (auto& key : keys) {
        auto it = map.find(key);
        if (map.end() != it) numSamples += it->second.size();
    }
    return numSamples;
}

std::tuple<double,std::map<unsigned int,std::vector<uintptr_t>>> computeDatalocUpdateRatioDistribution(
        const std::unordered_map<uintptr_t,std::vector<SamplePtr>>& datalocSamplesMap,
        const std::vector<uintptr_t>& datalocs,
        double ratioStep)
{
    std::map<unsigned int,std::vector<uintptr_t>> updateRatioDatalocsMap;
    size_t numSamples = 0;
    size_t numStores = 0;

    for (uintptr_t dataloc : datalocs) {
        const std::vector<SamplePtr>& samples = datalocSamplesMap.at(dataloc);
        size_t numDatalocStores = 0;
        for (const SamplePtr& sample : samples) {
            if (Sample::Type::STORE == sample->_type) {
                numDatalocStores += 1;
            }
        }
        double datalocUpdateRatio = (double)numDatalocStores / samples.size();
        unsigned int key = (unsigned int)(datalocUpdateRatio / ratioStep);
        updateRatioDatalocsMap[key].emplace_back(dataloc);

        numSamples += samples.size();
        numStores += numDatalocStores;
    }

    double overallUpdateRatio = (double)numStores / numSamples;
    return std::make_tuple(overallUpdateRatio, updateRatioDatalocsMap);
}

std::tuple<double,double,std::map<unsigned int,std::vector<uintptr_t>>> computeDatalocAccessCountDistribution(
        const std::unordered_map<uintptr_t,std::vector<SamplePtr>>& datalocSamplesMap,
        const std::vector<uintptr_t>& datalocs,
        double logBase)

{
    std::map<unsigned int,std::vector<uintptr_t>> numAccessesDatalocsMap;
    size_t sum = 0;
    size_t sumsquared = 0;

    double logDenom = std::log(logBase);
    for (uintptr_t dataloc : datalocs) {
        const std::vector<SamplePtr>& samples = datalocSamplesMap.at(dataloc);
        assert(samples.size() > 0);

        unsigned int key = (unsigned int)(std::log((double)samples.size()) / logDenom);
        numAccessesDatalocsMap[key].emplace_back(dataloc);
        sum += samples.size();
        sumsquared += (samples.size() * samples.size());
    }

    double num = (double)datalocs.size();
    double mean = sum / num;
    double std = std::sqrt((sumsquared - ((sum * sum) / num)) / num);
    return std::make_tuple(mean, std, numAccessesDatalocsMap);
}

void printUsage(const char* scriptname) {
    fprintf(stderr,
            "Usage: %s DATA_OPTIONS... [OPTIONS ...]\n"
            "DATA_OPTIONS:\n"
            "  -m --samples PATH              Specify path to samples.data file.\n"
            "  -s --sessions PATH             Specify path to sessions.data file.\n"
            "OPTIONS:\n"
            "  -c --cacheline UINT            Specify system's cacheline size. (default=%zu)\n"
            "  -p --samples-per-dataloc UINT  Minimum samples a data location needs to avoid being filtered out. (default=%zu)\n"
            "  --log-phase-chars              Log phase characteristics to pass to accesstest.\n"
            "  --extended                     Log extended statistics.\n"
            "  --filter REGEX                 Filter samples whose call chain contains function name matching (C++) regex. (default=none)\n"
            "  -v --verbose                   Increase logging verbosity.\n"
            "  -h --help                      Print help message and exit.\n",
            scriptname,
            DEFAULT_CACHELINE_SIZE_BYTES, DEFAULT_MIN_SAMPLES_PER_DATALOC);
}

void handleListCmd(const std::string& listCmd, const std::string& subcmd, const std::map<std::string,std::pair<size_t,size_t>>& funcCountsMap) {
    if ("alpha" == subcmd) {
        for (auto& pair : funcCountsMap) {
            printf("%s\n", pair.first.c_str());
        }
    } else if ("samples" == subcmd || "datalocs" == subcmd) {
        std::vector<std::pair<const std::string*,size_t>> funcCounts;
        funcCounts.reserve(funcCountsMap.size());
        bool shouldGetSamples = "samples" == subcmd;
        auto getCount = [shouldGetSamples](auto& counts) {
                return shouldGetSamples ? counts.first : counts.second; };

        // Add pairs to vector
        for (auto& pair : funcCountsMap) {
            funcCounts.emplace_back(&pair.first, getCount(pair.second));
        }
        // Sort pairs in vector
        std::sort(funcCounts.begin(), funcCounts.end(),
                [](const auto& p1, const auto& p2) -> bool {
                    return p1.second < p2.second ||
                           (p1.second == p2.second && *p1.first < *p2.first);
                });

        // Print sorted values
        for (auto& pair : funcCounts) {
            printf("%5zu %s\n", pair.second, pair.first->c_str());
        }
    } else {
        std::FILE* file = stdout;
        if (subcmd.size() > 0 && "help" != subcmd) {
            file = stderr;
            fprintf(stderr, "Error: %s subcommand \"%s\" not recoginized.\n", listCmd.c_str(), subcmd.c_str());
        }
        fprintf(file,
                "Usage: %s [subcommand]\n"
                "  subcommand: one of the following\n"
                "    alpha              List function names in alphabetical order.\n"
                "    samples            List sample counts and function names sorted by counts.\n"
                "    datalocs           List data location counts and function names sorted by counts.\n"
                "    help (default)     Print this help message.\n",
                listCmd.c_str());
    }
}

// Return true if samples need to be pre-processed again. False otherwise.
bool handleSetCmd(const std::string& setCmd, const std::string& subcmd,
                  size_t& minSamplesPerDataloc)
{
    DebugLogger log;

    bool rval = true;
    if (0 == subcmd.find("samples-per-dataloc")) {
        std::string arg = subcmd.substr(19);
        trim(arg);
        size_t newMinSamplesPerDataloc = (arg.size() > 0 ? (size_t)std::stoul(arg) : DEFAULT_MIN_SAMPLES_PER_DATALOC);
        log.printf(0, "Updated minimum number of samples per dataloc from %zu to %zu.\n",
                   minSamplesPerDataloc, newMinSamplesPerDataloc);
        minSamplesPerDataloc = newMinSamplesPerDataloc;
    } else if (0 == subcmd.find("extended")) {
        fprintf(stderr, "Not implemented\n");
        rval = false;
    } else if (0 == subcmd.find("log-phase-chars")) {
        fprintf(stderr, "Not implemented\n");
        rval = false;
    } else if (0 == subcmd.find("filter")) {
        fprintf(stderr, "Not implemented\n");
    } else {
        std::FILE* file = stdout;
        if (subcmd.size() > 0 && "help" != subcmd) {
            file = stderr;
            fprintf(stderr, "Error: %s subcommand \"%s\" not recoginized.\n", setCmd.c_str(), subcmd.c_str());
        }
        fprintf(file,
                "Usage: %s [subcommand]\n"
                "  subcommand: one of the following\n"
                "    samples-per-dataloc [UINT]  Minimum samples a data location needs to avoid being filtered out. (default=%zu)\n"
                "    log-phase-chars [{on,off}]  Log phase characteristics to pass to accesstest. (default=on)\n"
                "    extended [{on,off}]         Log extended statistics. (default=on)\n"
                "    filter [REGEX]              Filter samples whose call chain contains function name matching (C++) regex. (default=none)\n"
                "    help (default)              Print this help message.\n",
                setCmd.c_str(), DEFAULT_MIN_SAMPLES_PER_DATALOC);
        rval = false;
    }
    return rval;
}

bool enterInteractiveMode(const std::vector<SamplePtr>& validSamples, const SessionSet& sessionSet,
                          size_t& minSamplesPerDataloc, bool& logExtendedStats,
                          bool& logPhaseChars, std::string& callchainFilter);

int main(int argc, char* argv[]) {
    // Parse arguments
    std::string samplesFilepath;
    std::string sessionsFilepath;
    Sample::setCachelineSize(DEFAULT_CACHELINE_SIZE_BYTES);
    size_t minSamplesPerDataloc = DEFAULT_MIN_SAMPLES_PER_DATALOC;
    bool logExtendedStats = false;
    bool logPhaseChars = false;
    std::string callchainFilter;

    const struct option longopts[] = {
            {"samples", required_argument, NULL, 'm'},
            {"sessions", required_argument, NULL, 's'},
            {"cacheline", required_argument, NULL, 'c'},
            {"samples-per-dataloc", required_argument, NULL, 'p'},
            {"log-phase-chars", no_argument, NULL, 'L'},
            {"extended", no_argument, NULL, 'E'},
            {"filter", required_argument, NULL, 'F'},
            {"verbose", no_argument, NULL, 'v'},
            {"help", no_argument, NULL, 'h'},
            {0, 0, 0, 0}};
    const char optstring[] = "m:s:c:p:LEF:vh";

    int opt;
    while ((opt = getopt_long(argc, argv, optstring, longopts, NULL)) != -1) {
        switch (opt) {
        case 'm':
            samplesFilepath = optarg;
            break;
        case 's':
            sessionsFilepath = optarg;
            break;
        case 'c':
            Sample::setCachelineSize((size_t)std::stoi(optarg));
            break;
        case 'p':
            minSamplesPerDataloc = (size_t)std::stoi(optarg);
            break;
        case 'L':
            logPhaseChars = true;
            break;
        case 'E':
            logExtendedStats = true;
            break;
        case 'F':
            callchainFilter = optarg;
            break;
        case 'v':
            DebugLogger::_maxLogLevel += 1;
            break;
        case 'h':
            printUsage(argv[0]);
            exit(0);
            break;
        case '?':
            fprintf(stderr, "Error: Command line arguments could not be parsed.\n");
            printUsage(argv[0]);
            exit(1);
            break;
        default:
            fprintf(stderr, "Error: Unknown option encountered.\n");
            printUsage(argv[0]);
            exit(1);
            break;
        }
    }
    if (0 == samplesFilepath.size()) {
        fprintf(stderr, "Error: Samples filepath not specified.\n");
        exit(1);
    }
    if (0 == sessionsFilepath.size()) {
        fprintf(stderr, "Error: Sessions filepath not specified.\n");
        exit(1);
    }
    DebugLogger log;

    // Load samples and sessions.
    std::vector<SamplePtr> samples = loadSamples(samplesFilepath);
    SessionSet sessionSet = loadSessions(sessionsFilepath);
    log.printfForce(1, "Imported %zu samples and %zu sessions.\n", samples.size(), sessionSet.size());

    // Filter out samples that don't correspond to session.
    {
        std::vector<SamplePtr> filteredSamples = filter(samples,
                [&sessionSet](const SamplePtr& sample) -> bool {
                    return nullptr != sessionSet.match(sample->_sp);
                }, "samples", "session");
        log.printfForce(1, "Filter Samples By Session: %zu samples dropped, %zu samples remain\n",
                        filteredSamples.size(), samples.size());
        filteredSamples.clear();
    }

    while (enterInteractiveMode(samples, sessionSet,
                                minSamplesPerDataloc, logExtendedStats,
                                logPhaseChars, callchainFilter));
    return 0;
}

bool enterInteractiveMode(const std::vector<SamplePtr>& validSamples, const SessionSet& sessionSet,
                          size_t& minSamplesPerDataloc, bool& logExtendedStats,
                          bool& logPhaseChars, std::string& callchainFilter)
{
    DebugLogger log;
    bool fullExit = false;

    // TODO: not pretty, but good enough
    std::vector<SamplePtr> samples = validSamples;
    filterSamples(samples, minSamplesPerDataloc, callchainFilter);

    std::unordered_map<uintptr_t,std::vector<SamplePtr>> datalocSamplesMap = mapDatalocToSamples(samples);
    std::unordered_map<uintptr_t,SessionSet> datalocSessionsMap = mapDatalocToSessions(datalocSamplesMap, sessionSet);

    std::map<std::string,std::pair<size_t,size_t>> funcCountsMap = mapFunctionToCounts(datalocSamplesMap);

    bool interactive = false;
    while(true) {
        if (std::cin.fail() || std::cin.bad()) {
            log.printf(1, "Warning: Error encountered with stdin, exiting. (Not an issue if pipe was used.)\n");
            fullExit = true;
            break;
        }
        fflush(stdout);
        fflush(stderr);
        std::string input;
        if (interactive) {
            printf("> ");
            if (!std::getline(std::cin, input)) {
                log.printf(1, "Warning: Error encountered with stdin, exiting. (Not an issue if pipe was used.)\n");
                fullExit = true;
                break;
            }
            printf("\n");
            trim(input);
        } else {
            interactive = true;
        }
        if (0 == input.size() || "@help" == input) {
            printf("Enter a function name whose access statistics will be computed.\n");
            printf("Use `@list` to list function names. Further usage information can be found using `@list help`.\n");
            printf("Use `@set` to update profiler's state. Further usage information can be found using `@set help`.\n");
            continue;
        } else if (0 == input.find("@list")) {
            std::string subcmd = input.substr(5);
            trim(subcmd);
            handleListCmd("@list", subcmd, funcCountsMap);
            continue;
        } else if (0 == input.find("@set")) {
            std::string subcmd = input.substr(4);
            trim(subcmd);
            if (handleSetCmd("@set", subcmd, minSamplesPerDataloc)) {
                fullExit = true;
                break;
            } else {
                continue;
            }
        }
        printf("== Overall Stats ==\n"
               "  Num Data Locations: %zu\n"
               "  Num Samples: %zu\n\n\n",
               datalocSamplesMap.size(), samples.size());

        // TODO: Add multi-function options and other commands.
        // Possibly allow user to specify sets of included and excluded function names used to group samples.
        // This can be done by specifying 1 or 2 regexes, which are matched to names in input and passed to groupSamplesByPhase.
        // Also allow custom commands: e.g., trying all combinations of functions that have the largest overlap w/r/t phase-local datalocs.
        std::unordered_map<uintptr_t,std::vector<SamplePtr>> phaseDatalocSamplesMap, extraPhaseDatalocSamplesMap;
        std::tie(phaseDatalocSamplesMap, extraPhaseDatalocSamplesMap) =
                groupSamplesByPhase(datalocSamplesMap, input, samples.size());

        using DatalocSampleMap = std::unordered_map<uintptr_t,std::vector<SamplePtr>>;
        std::array<std::tuple<std::string,DatalocSampleMap*,DatalocSampleMap*>,2> phases({{
                {"Phase " + input, &phaseDatalocSamplesMap, &extraPhaseDatalocSamplesMap},
                {std::string("Everything Else"), &extraPhaseDatalocSamplesMap, &phaseDatalocSamplesMap}}});
        std::vector<std::array<std::tuple<size_t,size_t,double>,2>> phaseLocalChars;
        std::pair<std::array<size_t,2>,std::vector<std::array<std::pair<size_t,double>,2>>> phaseGlobalChars;
        if (logPhaseChars) {
            phaseLocalChars.resize(phases.size());
            phaseGlobalChars.second.resize(phases.size());
        }
        for (size_t phaseIndex = 0; phaseIndex < phases.size(); ++phaseIndex) {
            const std::string& phaseName = std::get<0>(phases[phaseIndex]);
            const DatalocSampleMap& currPhaseDatalocSamplesMap = *std::get<1>(phases[phaseIndex]);
            const DatalocSampleMap& extraCurrPhaseDatalocSamplesMap = *std::get<2>(phases[phaseIndex]);

            size_t numPhaseDatalocs = currPhaseDatalocSamplesMap.size();
            size_t numPhaseSamples = countMappedSamples(currPhaseDatalocSamplesMap);
            printf("== %s ==\n"
                   "  Num Data Locations: %zu (%.2lf%%)\n"
                   "  Num Samples: %zu (%.2lf%%)\n"
                   "\n",
                   phaseName.c_str(),
                   numPhaseDatalocs, (double)numPhaseDatalocs / datalocSamplesMap.size() * 100,
                   numPhaseSamples, (double)numPhaseSamples / samples.size() * 100);
            if (0 == numPhaseSamples) {
                continue;
            }

            std::array<std::vector<uintptr_t>,4> classifiedDatalocs =
                    classifyDatalocs(currPhaseDatalocSamplesMap, extraCurrPhaseDatalocSamplesMap, datalocSessionsMap);

            // For all datalocs of a given classification, print their characteristics.
            for (DatalocClassif classif = DatalocClassif::_MIN; classif <= DatalocClassif::_MAX; ++classif) {
                const std::vector<uintptr_t>& classifDatalocs = classifiedDatalocs[classif];

                size_t numClassifDatalocs = classifDatalocs.size();
                size_t numClassifSamples = countMappedSamples(currPhaseDatalocSamplesMap, classifDatalocs);
                printf("  %s\n", datalocClassifToString(classif).c_str());
                if (logExtendedStats) {
                    printf("    Num Data Locations: %zu (%.2lf%% phase, %.2lf%% overall)\n"
                           "    Num Samples: %zu (%.2lf%% phase, %.2lf%% overall)\n",
                           numClassifDatalocs, (double)numClassifDatalocs / currPhaseDatalocSamplesMap.size() * 100,
                           (double)numClassifDatalocs / datalocSamplesMap.size() * 100,
                           numClassifSamples, (double)numClassifSamples / numPhaseSamples * 100,
                           (double)numClassifSamples / samples.size() * 100);
                } else {
                    printf("    Num Data Locations: %zu (%.2lf%%)\n"
                           "    Num Samples: %zu (%.2lf%%)\n",
                           numClassifDatalocs, (double)numClassifDatalocs / datalocSamplesMap.size() * 100,
                           numClassifSamples, (double)numClassifSamples / samples.size() * 100);
                }
                if (0 == numClassifSamples) {
                    printf("\n");
                    continue;
                }

                // Log access count distribution.
                if (logExtendedStats) {
                    std::tuple<double,double,std::map<unsigned int,std::vector<uintptr_t>>> numDatalocsAccessesStats =
                            computeDatalocAccessCountDistribution(currPhaseDatalocSamplesMap, classifDatalocs, BUCKET_ACCESS_COUNT_LOG_BASE);
                    printf("    Dataloc Access Count Distribution: mean=%.2lf std=%.2lf\n     ",
                           std::get<0>(numDatalocsAccessesStats), std::get<1>(numDatalocsAccessesStats));
                    for (const auto& pair : std::get<2>(numDatalocsAccessesStats)) {
                        printf(" %zu:%zu", (size_t)std::pow(BUCKET_ACCESS_COUNT_LOG_BASE, pair.first), pair.second.size());
                    }
                    printf("\n");
                }

                // Log distribution of dataloc update ratio.
                std::tuple<double,std::map<unsigned int,std::vector<uintptr_t>>> updateRatioDatalocsStats =
                        computeDatalocUpdateRatioDistribution(currPhaseDatalocSamplesMap, classifDatalocs, BUCKET_ACCESS_UPDATE_RATIO_STEP);
                double updateRatio = std::get<0>(updateRatioDatalocsStats);
                printf("    Update Ratio: %.2lf%%\n", updateRatio * 100.);
                if (logExtendedStats) {
                    printf("    Dataloc Update Ratio Distribution:\n     ");
                    for (const auto& pair : std::get<1>(updateRatioDatalocsStats)) {
                        printf(" %.2lf%%:%zu", (double)pair.first * BUCKET_ACCESS_UPDATE_RATIO_STEP * 100., pair.second.size());
                    }
                    printf("\n");
                }

                if (logPhaseChars) {
                    if (DatalocClassif::PHASE_LOCAL_SESS_LOCAL == classif ||
                        DatalocClassif::PHASE_LOCAL_SESS_GLOBAL == classif)
                    {
                        int idx = (DatalocClassif::PHASE_LOCAL_SESS_LOCAL == classif ? 0 : 1);
                        phaseLocalChars[phaseIndex][idx] = std::make_tuple(numClassifDatalocs, numClassifSamples, updateRatio);
                    } else {
                        int idx = (DatalocClassif::PHASE_GLOBAL_SESS_LOCAL == classif ? 0 : 1);
                        if (0 == phaseGlobalChars.first[idx]) phaseGlobalChars.first[idx] = numClassifDatalocs;
                        phaseGlobalChars.second[phaseIndex][idx] = std::make_pair(numClassifSamples, updateRatio);
                    }
                }

                // Log access count/update ratio distribution.
                if (logExtendedStats) {
                    printf("    Dataloc Access Count Distribution given Dataloc Update Ratio:\n");
                    for (const auto& pair : std::get<1>(updateRatioDatalocsStats)) {
                        std::tuple<double,double,std::map<unsigned int,std::vector<uintptr_t>>> urNumAccessesDatalocsDistribution =
                                computeDatalocAccessCountDistribution(currPhaseDatalocSamplesMap, pair.second, BUCKET_ACCESS_COUNT_LOG_BASE);
                        printf("      %6.2lf | mean=%6.2lf std=%6.2lf | ", (double)pair.first * BUCKET_ACCESS_UPDATE_RATIO_STEP * 100.,
                               std::get<0>(urNumAccessesDatalocsDistribution), std::get<1>(urNumAccessesDatalocsDistribution));
                        for (const auto& pair : std::get<2>(urNumAccessesDatalocsDistribution)) {
                            printf(" %zu:%zu", (size_t)std::pow(BUCKET_ACCESS_COUNT_LOG_BASE, pair.first), pair.second.size());
                        }
                        printf("\n");
                    }
                }
                printf("\n");
            }
            printf("\n");
        }
        if (logPhaseChars) {
            printf("Phase Characteristics:");
            for (size_t phaseIdx = 0; phaseIdx < phases.size(); ++phaseIdx) {
                const auto& localChars = phaseLocalChars[phaseIdx][0];
                const auto& globalChars = phaseLocalChars[phaseIdx][1];
                printf(" %zu,%lf,%zu/%zu,%lf,%zu",
                       std::get<0>(localChars), std::get<2>(localChars), std::get<1>(localChars),
                       std::get<0>(globalChars), std::get<2>(globalChars), std::get<1>(globalChars));
            }
            printf(" %zu/%zu:", phaseGlobalChars.first[0], phaseGlobalChars.first[1]);
            for (size_t phaseIdx = 0; phaseIdx < phases.size(); ++phaseIdx) {
                const auto& localChars = phaseGlobalChars.second[phaseIdx][0];
                const auto& globalChars = phaseGlobalChars.second[phaseIdx][1];
                printf("%zu,%lf,%lf,%zu,%zu", phaseIdx,
                       localChars.second, globalChars.second,
                       localChars.first, globalChars.first);
                if (phaseIdx + 1 != phases.size()) printf("/");
            }
            printf("\n\n\n");
        }
    }
    return !fullExit;
}

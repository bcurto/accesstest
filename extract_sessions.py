#!/usr/bin/env python3

import sys, os
import argparse
import struct
import random
import re

def store_session(fh, base_addr, size):
    data = struct.pack("!QI", base_addr, size)
    fh.write(data)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", default="sessions.log")
    parser.add_argument("-o", "--output", default="sessions.data")
    args = parser.parse_args()

    hex_re = re.compile("^0x[0-9a-zA-Z]{1,16}$")
    sanity_check_value = random.randint(0, 2 ** 32 - 1)
    num_sessions = 0
    with open(args.input, 'r') as input_fh, open(args.output, "w+b") as output_fh:
        # Reserve space for session count
        output_fh.write(struct.pack("!I", sanity_check_value))

        for line in input_fh:
            if not line.startswith("Stack:"):
                continue
            session_params = line[6:].split(',')

            assert(len(session_params) == 2)
            base_addr, size = session_params

            assert(hex_re.match(base_addr) and hex_re.match(size))
            store_session(output_fh, int(base_addr, 16), int(size, 16))
            num_sessions += 1

        output_fh.seek(0)
        read_sanity_check_value = output_fh.read(4)
        assert(len(read_sanity_check_value) > 0)
        assert(sanity_check_value == struct.unpack("!I", read_sanity_check_value)[0])
        output_fh.seek(0)
        output_fh.write(struct.pack("!I", num_sessions))

if "__main__" == __name__:
    main()

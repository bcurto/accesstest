++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8255c00000 of size 40960 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2845639960.000000
Throughput: 284563996.000000
Num Accesses - Session Mean & Stddev: 28456399.600000,142.800000
Num Phases Traversed: 7971080.000000
Num Phases Traversed - Session Mean & Stddev: 79710.800000,0.400000
Num Path Iterations: 7971080.000000
Num Path Iterations - Session Mean & Stddev: 79710.800000,0.400000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc216200000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2808576577.000000
Throughput: 280857657.700000
Num Accesses - Session Mean & Stddev: 28085765.770000,174.126727
Num Phases Traversed: 7867261.000000
Num Phases Traversed - Session Mean & Stddev: 78672.610000,0.487750
Num Path Iterations: 7867261.000000
Num Path Iterations - Session Mean & Stddev: 78672.610000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f473ca00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2799902905.000000
Throughput: 279990290.500000
Num Accesses - Session Mean & Stddev: 27999029.050000,170.278147
Num Phases Traversed: 7842965.000000
Num Phases Traversed - Session Mean & Stddev: 78429.650000,0.476970
Num Path Iterations: 7842965.000000
Num Path Iterations - Session Mean & Stddev: 78429.650000,0.476970
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f051de00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2829202966.000000
Throughput: 282920296.600000
Num Accesses - Session Mean & Stddev: 28292029.660000,173.282961
Num Phases Traversed: 7925038.000000
Num Phases Traversed - Session Mean & Stddev: 79250.380000,0.485386
Num Path Iterations: 7925038.000000
Num Path Iterations - Session Mean & Stddev: 79250.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f557fe00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2834769310.000000
Throughput: 283476931.000000
Num Accesses - Session Mean & Stddev: 28347693.100000,163.597952
Num Phases Traversed: 7940630.000000
Num Phases Traversed - Session Mean & Stddev: 79406.300000,0.458258
Num Path Iterations: 7940630.000000
Num Path Iterations - Session Mean & Stddev: 79406.300000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fced6e00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2832234967.000000
Throughput: 283223496.700000
Num Accesses - Session Mean & Stddev: 28322349.670000,165.110088
Num Phases Traversed: 7933531.000000
Num Phases Traversed - Session Mean & Stddev: 79335.310000,0.462493
Num Path Iterations: 7933531.000000
Num Path Iterations - Session Mean & Stddev: 79335.310000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f99f8e00000 of size 40960 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2837684215.000000
Throughput: 283768421.500000
Num Accesses - Session Mean & Stddev: 28376842.150000,77.806346
Num Phases Traversed: 7948795.000000
Num Phases Traversed - Session Mean & Stddev: 79487.950000,0.217945
Num Path Iterations: 7948795.000000
Num Path Iterations - Session Mean & Stddev: 79487.950000,0.217945
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2087e00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2806477060.000000
Throughput: 280647706.000000
Num Accesses - Session Mean & Stddev: 28064770.600000,142.800000
Num Phases Traversed: 7861380.000000
Num Phases Traversed - Session Mean & Stddev: 78613.800000,0.400000
Num Path Iterations: 7861380.000000
Num Path Iterations - Session Mean & Stddev: 78613.800000,0.400000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2e54200000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2838780205.000000
Throughput: 283878020.500000
Num Accesses - Session Mean & Stddev: 28387802.050000,170.278147
Num Phases Traversed: 7951865.000000
Num Phases Traversed - Session Mean & Stddev: 79518.650000,0.476970
Num Path Iterations: 7951865.000000
Num Path Iterations - Session Mean & Stddev: 79518.650000,0.476970
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fce57800000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2834407312.000000
Throughput: 283440731.200000
Num Accesses - Session Mean & Stddev: 28344073.120000,130.878362
Num Phases Traversed: 7939616.000000
Num Phases Traversed - Session Mean & Stddev: 79396.160000,0.366606
Num Path Iterations: 7939616.000000
Num Path Iterations - Session Mean & Stddev: 79396.160000,0.366606
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdedae00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2821179748.000000
Throughput: 282117974.800000
Num Accesses - Session Mean & Stddev: 28211797.480000,171.360000
Num Phases Traversed: 7902564.000000
Num Phases Traversed - Session Mean & Stddev: 79025.640000,0.480000
Num Path Iterations: 7902564.000000
Num Path Iterations - Session Mean & Stddev: 79025.640000,0.480000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff326a00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2837989093.000000
Throughput: 283798909.300000
Num Accesses - Session Mean & Stddev: 28379890.930000,178.464296
Num Phases Traversed: 7949649.000000
Num Phases Traversed - Session Mean & Stddev: 79496.490000,0.499900
Num Path Iterations: 7949649.000000
Num Path Iterations - Session Mean & Stddev: 79496.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9495600000 of size 40960 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2831544529.000000
Throughput: 283154452.900000
Num Accesses - Session Mean & Stddev: 28315445.290000,79.105916
Num Phases Traversed: 7931597.000000
Num Phases Traversed - Session Mean & Stddev: 79315.970000,0.221585
Num Path Iterations: 7931597.000000
Num Path Iterations - Session Mean & Stddev: 79315.970000,0.221585
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa1fb200000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2821097995.000000
Throughput: 282109799.500000
Num Accesses - Session Mean & Stddev: 28210979.950000,170.278147
Num Phases Traversed: 7902335.000000
Num Phases Traversed - Session Mean & Stddev: 79023.350000,0.476970
Num Path Iterations: 7902335.000000
Num Path Iterations - Session Mean & Stddev: 79023.350000,0.476970
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f526ac00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2827879567.000000
Throughput: 282787956.700000
Num Accesses - Session Mean & Stddev: 28278795.670000,165.110088
Num Phases Traversed: 7921331.000000
Num Phases Traversed - Session Mean & Stddev: 79213.310000,0.462493
Num Path Iterations: 7921331.000000
Num Path Iterations - Session Mean & Stddev: 79213.310000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f79c9a00000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2845280461.000000
Throughput: 284528046.100000
Num Accesses - Session Mean & Stddev: 28452804.610000,158.493526
Num Phases Traversed: 7970073.000000
Num Phases Traversed - Session Mean & Stddev: 79700.730000,0.443959
Num Path Iterations: 7970073.000000
Num Path Iterations - Session Mean & Stddev: 79700.730000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f768e200000 of size 40960 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2839165051.000000
Throughput: 283916505.100000
Num Accesses - Session Mean & Stddev: 28391650.510000,176.742043
Num Phases Traversed: 7952943.000000
Num Phases Traversed - Session Mean & Stddev: 79529.430000,0.495076
Num Path Iterations: 7952943.000000
Num Path Iterations - Session Mean & Stddev: 79529.430000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5865200000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2831749804.000000
Throughput: 283174980.400000
Num Accesses - Session Mean & Stddev: 28317498.040000,160.292602
Num Phases Traversed: 7932172.000000
Num Phases Traversed - Session Mean & Stddev: 79321.720000,0.448999
Num Path Iterations: 7932172.000000
Num Path Iterations - Session Mean & Stddev: 79321.720000,0.448999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1f34c00000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2806090072.000000
Throughput: 280609007.200000
Num Accesses - Session Mean & Stddev: 28060900.720000,69.957427
Num Phases Traversed: 7860296.000000
Num Phases Traversed - Session Mean & Stddev: 78602.960000,0.195959
Num Path Iterations: 7860296.000000
Num Path Iterations - Session Mean & Stddev: 78602.960000,0.195959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.001 0/320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.001 0/320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3927800000 of size 40960 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2823764428.000000
Throughput: 282376442.800000
Num Accesses - Session Mean & Stddev: 28237644.280000,122.841205
Num Phases Traversed: 7909804.000000
Num Phases Traversed - Session Mean & Stddev: 79098.040000,0.344093
Num Path Iterations: 7909804.000000
Num Path Iterations - Session Mean & Stddev: 79098.040000,0.344093
+ set +x

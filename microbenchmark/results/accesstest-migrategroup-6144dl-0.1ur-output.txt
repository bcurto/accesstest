++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa47ee00000 of size 393216 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1967201196.000000
Throughput: 196720119.600000
Num Accesses - Session Mean & Stddev: 19672011.960000,2123.244427
Num Phases Traversed: 632844.000000
Num Phases Traversed - Session Mean & Stddev: 6328.440000,0.682935
Num Path Iterations: 632844.000000
Num Path Iterations - Session Mean & Stddev: 6328.440000,0.682935
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa13d200000 of size 393216 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969374387.000000
Throughput: 196937438.700000
Num Accesses - Session Mean & Stddev: 19693743.870000,2120.283295
Num Phases Traversed: 633543.000000
Num Phases Traversed - Session Mean & Stddev: 6335.430000,0.681982
Num Path Iterations: 633543.000000
Num Path Iterations - Session Mean & Stddev: 6335.430000,0.681982
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5ba6600000 of size 393216 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969862500.000000
Throughput: 196986250.000000
Num Accesses - Session Mean & Stddev: 19698625.000000,2198.394983
Num Phases Traversed: 633700.000000
Num Phases Traversed - Session Mean & Stddev: 6337.000000,0.707107
Num Path Iterations: 633700.000000
Num Path Iterations - Session Mean & Stddev: 6337.000000,0.707107
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9482000000 of size 393216 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1967599148.000000
Throughput: 196759914.800000
Num Accesses - Session Mean & Stddev: 19675991.480000,2018.696468
Num Phases Traversed: 632972.000000
Num Phases Traversed - Session Mean & Stddev: 6329.720000,0.649307
Num Path Iterations: 632972.000000
Num Path Iterations - Session Mean & Stddev: 6329.720000,0.649307
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd27b800000 of size 393216 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969887372.000000
Throughput: 196988737.200000
Num Accesses - Session Mean & Stddev: 19698873.720000,2093.906307
Num Phases Traversed: 633708.000000
Num Phases Traversed - Session Mean & Stddev: 6337.080000,0.673498
Num Path Iterations: 633708.000000
Num Path Iterations - Session Mean & Stddev: 6337.080000,0.673498
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa9f7000000 of size 393216 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1968662426.000000
Throughput: 196866242.600000
Num Accesses - Session Mean & Stddev: 19686624.260000,2199.274165
Num Phases Traversed: 633314.000000
Num Phases Traversed - Session Mean & Stddev: 6333.140000,0.707390
Num Path Iterations: 633314.000000
Num Path Iterations - Session Mean & Stddev: 6333.140000,0.707390
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb3b0600000 of size 393216 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969044833.000000
Throughput: 196904483.300000
Num Accesses - Session Mean & Stddev: 19690448.330000,1998.243089
Num Phases Traversed: 633437.000000
Num Phases Traversed - Session Mean & Stddev: 6334.370000,0.642729
Num Path Iterations: 633437.000000
Num Path Iterations - Session Mean & Stddev: 6334.370000,0.642729
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2713200000 of size 393216 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1967468570.000000
Throughput: 196746857.000000
Num Accesses - Session Mean & Stddev: 19674685.700000,1990.731325
Num Phases Traversed: 632930.000000
Num Phases Traversed - Session Mean & Stddev: 6329.300000,0.640312
Num Path Iterations: 632930.000000
Num Path Iterations - Session Mean & Stddev: 6329.300000,0.640312
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe42c400000 of size 393216 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1968124569.000000
Throughput: 196812456.900000
Num Accesses - Session Mean & Stddev: 19681245.690000,2066.725142
Num Phases Traversed: 633141.000000
Num Phases Traversed - Session Mean & Stddev: 6331.410000,0.664756
Num Path Iterations: 633141.000000
Num Path Iterations - Session Mean & Stddev: 6331.410000,0.664756
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa52fa00000 of size 393216 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969567145.000000
Throughput: 196956714.500000
Num Accesses - Session Mean & Stddev: 19695671.450000,2079.779255
Num Phases Traversed: 633605.000000
Num Phases Traversed - Session Mean & Stddev: 6336.050000,0.668954
Num Path Iterations: 633605.000000
Num Path Iterations - Session Mean & Stddev: 6336.050000,0.668954
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2b0de00000 of size 393216 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1967390845.000000
Throughput: 196739084.500000
Num Accesses - Session Mean & Stddev: 19673908.450000,2125.746779
Num Phases Traversed: 632905.000000
Num Phases Traversed - Session Mean & Stddev: 6329.050000,0.683740
Num Path Iterations: 632905.000000
Num Path Iterations - Session Mean & Stddev: 6329.050000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc4d9200000 of size 393216 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969278008.000000
Throughput: 196927800.800000
Num Accesses - Session Mean & Stddev: 19692780.080000,2075.359384
Num Phases Traversed: 633512.000000
Num Phases Traversed - Session Mean & Stddev: 6335.120000,0.667533
Num Path Iterations: 633512.000000
Num Path Iterations - Session Mean & Stddev: 6335.120000,0.667533
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6d75800000 of size 393216 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1967499660.000000
Throughput: 196749966.000000
Num Accesses - Session Mean & Stddev: 19674996.600000,2153.978384
Num Phases Traversed: 632940.000000
Num Phases Traversed - Session Mean & Stddev: 6329.400000,0.692820
Num Path Iterations: 632940.000000
Num Path Iterations - Session Mean & Stddev: 6329.400000,0.692820
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fde3ae00000 of size 393216 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1968006427.000000
Throughput: 196800642.700000
Num Accesses - Session Mean & Stddev: 19680064.270000,2174.300439
Num Phases Traversed: 633103.000000
Num Phases Traversed - Session Mean & Stddev: 6331.030000,0.699357
Num Path Iterations: 633103.000000
Num Path Iterations - Session Mean & Stddev: 6331.030000,0.699357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efef0800000 of size 393216 bytes.
[1.248s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1964173030.000000
Throughput: 196417303.000000
Num Accesses - Session Mean & Stddev: 19641730.300000,2798.100000
Num Phases Traversed: 631870.000000
Num Phases Traversed - Session Mean & Stddev: 6318.700000,0.900000
Num Path Iterations: 631870.000000
Num Path Iterations - Session Mean & Stddev: 6318.700000,0.900000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f841e200000 of size 393216 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1968360853.000000
Throughput: 196836085.300000
Num Accesses - Session Mean & Stddev: 19683608.530000,2064.853532
Num Phases Traversed: 633217.000000
Num Phases Traversed - Session Mean & Stddev: 6332.170000,0.664154
Num Path Iterations: 633217.000000
Num Path Iterations - Session Mean & Stddev: 6332.170000,0.664154
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fad9f000000 of size 393216 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1968622009.000000
Throughput: 196862200.900000
Num Accesses - Session Mean & Stddev: 19686220.090000,2085.348859
Num Phases Traversed: 633301.000000
Num Phases Traversed - Session Mean & Stddev: 6333.010000,0.670746
Num Path Iterations: 633301.000000
Num Path Iterations - Session Mean & Stddev: 6333.010000,0.670746
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7e7e600000 of size 393216 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1967390845.000000
Throughput: 196739084.500000
Num Accesses - Session Mean & Stddev: 19673908.450000,2125.746779
Num Phases Traversed: 632905.000000
Num Phases Traversed - Session Mean & Stddev: 6329.050000,0.683740
Num Path Iterations: 632905.000000
Num Path Iterations - Session Mean & Stddev: 6329.050000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1223e00000 of size 393216 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969986860.000000
Throughput: 196998686.000000
Num Accesses - Session Mean & Stddev: 19699868.600000,1916.516314
Num Phases Traversed: 633740.000000
Num Phases Traversed - Session Mean & Stddev: 6337.400000,0.616441
Num Path Iterations: 633740.000000
Num Path Iterations - Session Mean & Stddev: 6337.400000,0.616441
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3072,0.1 0/3072,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3072,0.1 0/3072,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3072 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1c8d200000 of size 393216 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969156757.000000
Throughput: 196915675.700000
Num Accesses - Session Mean & Stddev: 19691567.570000,2101.969126
Num Phases Traversed: 633473.000000
Num Phases Traversed - Session Mean & Stddev: 6334.730000,0.676092
Num Path Iterations: 633473.000000
Num Path Iterations - Session Mean & Stddev: 6334.730000,0.676092
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f85bb800000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 948975829.000000
Throughput: 94897582.900000
Num Accesses - Session Mean & Stddev: 9489758.290000,3122.440908
Num Phases Traversed: 193729.000000
Num Phases Traversed - Session Mean & Stddev: 1937.290000,0.637103
Num Path Iterations: 193729.000000
Num Path Iterations - Session Mean & Stddev: 1937.290000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc431c00000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 948946423.000000
Throughput: 94894642.300000
Num Accesses - Session Mean & Stddev: 9489464.230000,3240.224532
Num Phases Traversed: 193723.000000
Num Phases Traversed - Session Mean & Stddev: 1937.230000,0.661135
Num Path Iterations: 193723.000000
Num Path Iterations - Session Mean & Stddev: 1937.230000,0.661135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6002000000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 947152657.000000
Throughput: 94715265.700000
Num Accesses - Session Mean & Stddev: 9471526.570000,2707.109397
Num Phases Traversed: 193357.000000
Num Phases Traversed - Session Mean & Stddev: 1933.570000,0.552359
Num Path Iterations: 193357.000000
Num Path Iterations - Session Mean & Stddev: 1933.570000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f672f000000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 951279299.000000
Throughput: 95127929.900000
Num Accesses - Session Mean & Stddev: 9512792.990000,3430.349911
Num Phases Traversed: 194199.000000
Num Phases Traversed - Session Mean & Stddev: 1941.990000,0.699929
Num Path Iterations: 194199.000000
Num Path Iterations - Session Mean & Stddev: 1941.990000,0.699929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0c65200000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 950867615.000000
Throughput: 95086761.500000
Num Accesses - Session Mean & Stddev: 9508676.150000,1750.007036
Num Phases Traversed: 194115.000000
Num Phases Traversed - Session Mean & Stddev: 1941.150000,0.357071
Num Path Iterations: 194115.000000
Num Path Iterations - Session Mean & Stddev: 1941.150000,0.357071
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb45a800000 of size 622592 bytes.
[1.300s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 949706078.000000
Throughput: 94970607.800000
Num Accesses - Session Mean & Stddev: 9497060.780000,3219.774233
Num Phases Traversed: 193878.000000
Num Phases Traversed - Session Mean & Stddev: 1938.780000,0.656963
Num Path Iterations: 193878.000000
Num Path Iterations - Session Mean & Stddev: 1938.780000,0.656963
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f689ee00000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 949956029.000000
Throughput: 94995602.900000
Num Accesses - Session Mean & Stddev: 9499560.290000,3122.440908
Num Phases Traversed: 193929.000000
Num Phases Traversed - Session Mean & Stddev: 1939.290000,0.637103
Num Path Iterations: 193929.000000
Num Path Iterations - Session Mean & Stddev: 1939.290000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5214400000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 949813900.000000
Throughput: 94981390.000000
Num Accesses - Session Mean & Stddev: 9498139.000000,3395.512403
Num Phases Traversed: 193900.000000
Num Phases Traversed - Session Mean & Stddev: 1939.000000,0.692820
Num Path Iterations: 193900.000000
Num Path Iterations - Session Mean & Stddev: 1939.000000,0.692820
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f05b2200000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 951455735.000000
Throughput: 95145573.500000
Num Accesses - Session Mean & Stddev: 9514557.350000,2971.073353
Num Phases Traversed: 194235.000000
Num Phases Traversed - Session Mean & Stddev: 1942.350000,0.606218
Num Path Iterations: 194235.000000
Num Path Iterations - Session Mean & Stddev: 1942.350000,0.606218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f54f3000000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 950916625.000000
Throughput: 95091662.500000
Num Accesses - Session Mean & Stddev: 9509166.250000,3204.444558
Num Phases Traversed: 194125.000000
Num Phases Traversed - Session Mean & Stddev: 1941.250000,0.653835
Num Path Iterations: 194125.000000
Num Path Iterations - Session Mean & Stddev: 1941.250000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbc63400000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 952122271.000000
Throughput: 95212227.100000
Num Accesses - Session Mean & Stddev: 9521222.710000,2223.887818
Num Phases Traversed: 194371.000000
Num Phases Traversed - Session Mean & Stddev: 1943.710000,0.453762
Num Path Iterations: 194371.000000
Num Path Iterations - Session Mean & Stddev: 1943.710000,0.453762
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6403000000 of size 622592 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 949132661.000000
Throughput: 94913266.100000
Num Accesses - Session Mean & Stddev: 9491326.610000,2848.910451
Num Phases Traversed: 193761.000000
Num Phases Traversed - Session Mean & Stddev: 1937.610000,0.581292
Num Path Iterations: 193761.000000
Num Path Iterations - Session Mean & Stddev: 1937.610000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7828200000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 949583553.000000
Throughput: 94958355.300000
Num Accesses - Session Mean & Stddev: 9495835.530000,2446.085123
Num Phases Traversed: 193853.000000
Num Phases Traversed - Session Mean & Stddev: 1938.530000,0.499099
Num Path Iterations: 193853.000000
Num Path Iterations - Session Mean & Stddev: 1938.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f266d800000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 950843110.000000
Throughput: 95084311.000000
Num Accesses - Session Mean & Stddev: 9508431.100000,3359.956320
Num Phases Traversed: 194110.000000
Num Phases Traversed - Session Mean & Stddev: 1941.100000,0.685565
Num Path Iterations: 194110.000000
Num Path Iterations - Session Mean & Stddev: 1941.100000,0.685565
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f09c6400000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 950426525.000000
Throughput: 95042652.500000
Num Accesses - Session Mean & Stddev: 9504265.250000,3204.444558
Num Phases Traversed: 194025.000000
Num Phases Traversed - Session Mean & Stddev: 1940.250000,0.653835
Num Path Iterations: 194025.000000
Num Path Iterations - Session Mean & Stddev: 1940.250000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7e26e00000 of size 622592 bytes.
[1.293s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 949083651.000000
Throughput: 94908365.100000
Num Accesses - Session Mean & Stddev: 9490836.510000,2450.009851
Num Phases Traversed: 193751.000000
Num Phases Traversed - Session Mean & Stddev: 1937.510000,0.499900
Num Path Iterations: 193751.000000
Num Path Iterations - Session Mean & Stddev: 1937.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1491a00000 of size 622592 bytes.
[1.300s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 949691375.000000
Throughput: 94969137.500000
Num Accesses - Session Mean & Stddev: 9496913.750000,3204.444558
Num Phases Traversed: 193875.000000
Num Phases Traversed - Session Mean & Stddev: 1938.750000,0.653835
Num Path Iterations: 193875.000000
Num Path Iterations - Session Mean & Stddev: 1938.750000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcfde600000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 949887415.000000
Throughput: 94988741.500000
Num Accesses - Session Mean & Stddev: 9498874.150000,1750.007036
Num Phases Traversed: 193915.000000
Num Phases Traversed - Session Mean & Stddev: 1939.150000,0.357071
Num Path Iterations: 193915.000000
Num Path Iterations - Session Mean & Stddev: 1939.150000,0.357071
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f79ed200000 of size 622592 bytes.
[1.257s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 942506509.000000
Throughput: 94250650.900000
Num Accesses - Session Mean & Stddev: 9425065.090000,3402.226051
Num Phases Traversed: 192409.000000
Num Phases Traversed - Session Mean & Stddev: 1924.090000,0.694190
Num Path Iterations: 192409.000000
Num Path Iterations - Session Mean & Stddev: 1924.090000,0.694190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,1 0/4864,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,1 0/4864,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5d8f800000 of size 622592 bytes.
[1.257s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 950808803.000000
Throughput: 95080880.300000
Num Accesses - Session Mean & Stddev: 9508088.030000,3564.951151
Num Phases Traversed: 194103.000000
Num Phases Traversed - Session Mean & Stddev: 1941.030000,0.727393
Num Path Iterations: 194103.000000
Num Path Iterations - Session Mean & Stddev: 1941.030000,0.727393
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f87e7400000 of size 36864 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2832838875.000000
Throughput: 283283887.500000
Num Accesses - Session Mean & Stddev: 28328388.750000,144.286824
Num Phases Traversed: 8716527.000000
Num Phases Traversed - Session Mean & Stddev: 87165.270000,0.443959
Num Path Iterations: 8716527.000000
Num Path Iterations - Session Mean & Stddev: 87165.270000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efef5600000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2831092325.000000
Throughput: 283109232.500000
Num Accesses - Session Mean & Stddev: 28310923.250000,162.207236
Num Phases Traversed: 8711153.000000
Num Phases Traversed - Session Mean & Stddev: 87111.530000,0.499099
Num Path Iterations: 8711153.000000
Num Path Iterations - Session Mean & Stddev: 87111.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff11a600000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2837506200.000000
Throughput: 283750620.000000
Num Accesses - Session Mean & Stddev: 28375062.000000,105.612499
Num Phases Traversed: 8730888.000000
Num Phases Traversed - Session Mean & Stddev: 87308.880000,0.324962
Num Path Iterations: 8730888.000000
Num Path Iterations - Session Mean & Stddev: 87308.880000,0.324962
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4416800000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2804366600.000000
Throughput: 280436660.000000
Num Accesses - Session Mean & Stddev: 28043666.000000,130.000000
Num Phases Traversed: 8628920.000000
Num Phases Traversed - Session Mean & Stddev: 86289.200000,0.400000
Num Path Iterations: 8628920.000000
Num Path Iterations - Session Mean & Stddev: 86289.200000,0.400000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb9a8e00000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2805701375.000000
Throughput: 280570137.500000
Num Accesses - Session Mean & Stddev: 28057013.750000,144.286824
Num Phases Traversed: 8633027.000000
Num Phases Traversed - Session Mean & Stddev: 86330.270000,0.443959
Num Path Iterations: 8633027.000000
Num Path Iterations - Session Mean & Stddev: 86330.270000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f811fc00000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2836181500.000000
Throughput: 283618150.000000
Num Accesses - Session Mean & Stddev: 28361815.000000,105.612499
Num Phases Traversed: 8726812.000000
Num Phases Traversed - Session Mean & Stddev: 87268.120000,0.324962
Num Path Iterations: 8726812.000000
Num Path Iterations - Session Mean & Stddev: 87268.120000,0.324962
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f93be800000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2838291075.000000
Throughput: 283829107.500000
Num Accesses - Session Mean & Stddev: 28382910.750000,72.015189
Num Phases Traversed: 8733303.000000
Num Phases Traversed - Session Mean & Stddev: 87333.030000,0.221585
Num Path Iterations: 8733303.000000
Num Path Iterations - Session Mean & Stddev: 87333.030000,0.221585
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa558800000 of size 36864 bytes.
[1.249s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2835749575.000000
Throughput: 283574957.500000
Num Accesses - Session Mean & Stddev: 28357495.750000,122.080660
Num Phases Traversed: 8725483.000000
Num Phases Traversed - Session Mean & Stddev: 87254.830000,0.375633
Num Path Iterations: 8725483.000000
Num Path Iterations - Session Mean & Stddev: 87254.830000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5020800000 of size 36864 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2828725025.000000
Throughput: 282872502.500000
Num Accesses - Session Mean & Stddev: 28287250.250000,150.310304
Num Phases Traversed: 8703869.000000
Num Phases Traversed - Session Mean & Stddev: 87038.690000,0.462493
Num Path Iterations: 8703869.000000
Num Path Iterations - Session Mean & Stddev: 87038.690000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3bc6e00000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2813798425.000000
Throughput: 281379842.500000
Num Accesses - Session Mean & Stddev: 28137984.250000,159.845824
Num Phases Traversed: 8657941.000000
Num Phases Traversed - Session Mean & Stddev: 86579.410000,0.491833
Num Path Iterations: 8657941.000000
Num Path Iterations - Session Mean & Stddev: 86579.410000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd878a00000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2793746575.000000
Throughput: 279374657.500000
Num Accesses - Session Mean & Stddev: 27937465.750000,160.899619
Num Phases Traversed: 8596243.000000
Num Phases Traversed - Session Mean & Stddev: 85962.430000,0.495076
Num Path Iterations: 8596243.000000
Num Path Iterations - Session Mean & Stddev: 85962.430000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7a76000000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2809067400.000000
Throughput: 280906740.000000
Num Accesses - Session Mean & Stddev: 28090674.000000,119.146968
Num Phases Traversed: 8643384.000000
Num Phases Traversed - Session Mean & Stddev: 86433.840000,0.366606
Num Path Iterations: 8643384.000000
Num Path Iterations - Session Mean & Stddev: 86433.840000,0.366606
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f41ed600000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2808347200.000000
Throughput: 280834720.000000
Num Accesses - Session Mean & Stddev: 28083472.000000,151.604749
Num Phases Traversed: 8641168.000000
Num Phases Traversed - Session Mean & Stddev: 86411.680000,0.466476
Num Path Iterations: 8641168.000000
Num Path Iterations - Session Mean & Stddev: 86411.680000,0.466476
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f421e600000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2828999325.000000
Throughput: 282899932.500000
Num Accesses - Session Mean & Stddev: 28289993.250000,109.298616
Num Phases Traversed: 8704713.000000
Num Phases Traversed - Session Mean & Stddev: 87047.130000,0.336303
Num Path Iterations: 8704713.000000
Num Path Iterations - Session Mean & Stddev: 87047.130000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff9aa000000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2821166825.000000
Throughput: 282116682.500000
Num Accesses - Session Mean & Stddev: 28211668.250000,109.298616
Num Phases Traversed: 8680613.000000
Num Phases Traversed - Session Mean & Stddev: 86806.130000,0.336303
Num Path Iterations: 8680613.000000
Num Path Iterations - Session Mean & Stddev: 86806.130000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8606400000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2802828375.000000
Throughput: 280282837.500000
Num Accesses - Session Mean & Stddev: 28028283.750000,109.298616
Num Phases Traversed: 8624187.000000
Num Phases Traversed - Session Mean & Stddev: 86241.870000,0.336303
Num Path Iterations: 8624187.000000
Num Path Iterations - Session Mean & Stddev: 86241.870000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8c51200000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2827040225.000000
Throughput: 282704022.500000
Num Accesses - Session Mean & Stddev: 28270402.250000,116.048212
Num Phases Traversed: 8698685.000000
Num Phases Traversed - Session Mean & Stddev: 86986.850000,0.357071
Num Path Iterations: 8698685.000000
Num Path Iterations - Session Mean & Stddev: 86986.850000,0.357071
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f985a200000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2818072175.000000
Throughput: 281807217.500000
Num Accesses - Session Mean & Stddev: 28180721.750000,93.009072
Num Phases Traversed: 8671091.000000
Num Phases Traversed - Session Mean & Stddev: 86710.910000,0.286182
Num Path Iterations: 8671091.000000
Num Path Iterations - Session Mean & Stddev: 86710.910000,0.286182
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcc62c00000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2823695650.000000
Throughput: 282369565.000000
Num Accesses - Session Mean & Stddev: 28236956.500000,77.183224
Num Phases Traversed: 8688394.000000
Num Phases Traversed - Session Mean & Stddev: 86883.940000,0.237487
Num Path Iterations: 8688394.000000
Num Path Iterations - Session Mean & Stddev: 86883.940000,0.237487
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.1 0/288,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.1 0/288,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe78c400000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2812502000.000000
Throughput: 281250200.000000
Num Accesses - Session Mean & Stddev: 28125020.000000,162.369948
Num Phases Traversed: 8653952.000000
Num Phases Traversed - Session Mean & Stddev: 86539.520000,0.499600
Num Path Iterations: 8653952.000000
Num Path Iterations - Session Mean & Stddev: 86539.520000,0.499600
+ set +x

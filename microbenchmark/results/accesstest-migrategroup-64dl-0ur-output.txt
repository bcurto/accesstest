+ for i in {1..20}
+ set -x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f17dd9e8000 of size 4096 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1519688668.000000
Throughput: 151968866.800000
Num Accesses - Session Mean & Stddev: 15196886.680000,35.291325
Num Phases Traversed: 22024572.000000
Num Phases Traversed - Session Mean & Stddev: 220245.720000,0.511468
Num Path Iterations: 22024572.000000
Num Path Iterations - Session Mean & Stddev: 220245.720000,0.511468
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faf92ea5000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1515392107.000000
Throughput: 151539210.700000
Num Accesses - Session Mean & Stddev: 15153921.070000,47.258704
Num Phases Traversed: 21962303.000000
Num Phases Traversed - Session Mean & Stddev: 219623.030000,0.684909
Num Path Iterations: 21962303.000000
Num Path Iterations - Session Mean & Stddev: 219623.030000,0.684909
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6a77025000 of size 4096 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1519127146.000000
Throughput: 151912714.600000
Num Accesses - Session Mean & Stddev: 15191271.460000,34.111412
Num Phases Traversed: 22016434.000000
Num Phases Traversed - Session Mean & Stddev: 220164.340000,0.494368
Num Path Iterations: 22016434.000000
Num Path Iterations - Session Mean & Stddev: 220164.340000,0.494368
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7bd5a26000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1533338110.000000
Throughput: 153333811.000000
Num Accesses - Session Mean & Stddev: 15333381.100000,40.820951
Num Phases Traversed: 22222390.000000
Num Phases Traversed - Session Mean & Stddev: 222223.900000,0.591608
Num Path Iterations: 22222390.000000
Num Path Iterations - Session Mean & Stddev: 222223.900000,0.591608
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f284f0bc000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1528712143.000000
Throughput: 152871214.300000
Num Accesses - Session Mean & Stddev: 15287121.430000,37.099934
Num Phases Traversed: 22155347.000000
Num Phases Traversed - Session Mean & Stddev: 221553.470000,0.537680
Num Path Iterations: 22155347.000000
Num Path Iterations - Session Mean & Stddev: 221553.470000,0.537680
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3b58f95000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1533825388.000000
Throughput: 153382538.800000
Num Accesses - Session Mean & Stddev: 15338253.880000,34.472389
Num Phases Traversed: 22229452.000000
Num Phases Traversed - Session Mean & Stddev: 222294.520000,0.499600
Num Path Iterations: 22229452.000000
Num Path Iterations - Session Mean & Stddev: 222294.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8b8c850000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1518700657.000000
Throughput: 151870065.700000
Num Accesses - Session Mean & Stddev: 15187006.570000,37.099934
Num Phases Traversed: 22010253.000000
Num Phases Traversed - Session Mean & Stddev: 220102.530000,0.537680
Num Path Iterations: 22010253.000000
Num Path Iterations - Session Mean & Stddev: 220102.530000,0.537680
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f83233d6000 of size 4096 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1526788285.000000
Throughput: 152678828.500000
Num Accesses - Session Mean & Stddev: 15267882.850000,44.046651
Num Phases Traversed: 22127465.000000
Num Phases Traversed - Session Mean & Stddev: 221274.650000,0.638357
Num Path Iterations: 22127465.000000
Num Path Iterations - Session Mean & Stddev: 221274.650000,0.638357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f163f81a000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1517363299.000000
Throughput: 151736329.900000
Num Accesses - Session Mean & Stddev: 15173632.990000,42.863386
Num Phases Traversed: 21990871.000000
Num Phases Traversed - Session Mean & Stddev: 219908.710000,0.621208
Num Path Iterations: 21990871.000000
Num Path Iterations - Session Mean & Stddev: 219908.710000,0.621208
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f07eec5e000 of size 4096 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1514794222.000000
Throughput: 151479422.200000
Num Accesses - Session Mean & Stddev: 15147942.220000,37.514685
Num Phases Traversed: 21953638.000000
Num Phases Traversed - Session Mean & Stddev: 219536.380000,0.543691
Num Path Iterations: 21953638.000000
Num Path Iterations - Session Mean & Stddev: 219536.380000,0.543691
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f18f61b5000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1514068204.000000
Throughput: 151406820.400000
Num Accesses - Session Mean & Stddev: 15140682.040000,43.332879
Num Phases Traversed: 21943116.000000
Num Phases Traversed - Session Mean & Stddev: 219431.160000,0.628013
Num Path Iterations: 21943116.000000
Num Path Iterations - Session Mean & Stddev: 219431.160000,0.628013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fde35b1c000 of size 4096 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1523866963.000000
Throughput: 152386696.300000
Num Accesses - Session Mean & Stddev: 15238669.630000,36.321799
Num Phases Traversed: 22085127.000000
Num Phases Traversed - Session Mean & Stddev: 220851.270000,0.526403
Num Path Iterations: 22085127.000000
Num Path Iterations - Session Mean & Stddev: 220851.270000,0.526403
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd9b41e7000 of size 4096 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1523342563.000000
Throughput: 152334256.300000
Num Accesses - Session Mean & Stddev: 15233425.630000,37.609747
Num Phases Traversed: 22077527.000000
Num Phases Traversed - Session Mean & Stddev: 220775.270000,0.545069
Num Path Iterations: 22077527.000000
Num Path Iterations - Session Mean & Stddev: 220775.270000,0.545069
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fefc861d000 of size 4096 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1523932651.000000
Throughput: 152393265.100000
Num Accesses - Session Mean & Stddev: 15239326.510000,45.030100
Num Phases Traversed: 22086079.000000
Num Phases Traversed - Session Mean & Stddev: 220860.790000,0.652610
Num Path Iterations: 22086079.000000
Num Path Iterations - Session Mean & Stddev: 220860.790000,0.652610
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe9249f0000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1523337250.000000
Throughput: 152333725.000000
Num Accesses - Session Mean & Stddev: 15233372.500000,35.853452
Num Phases Traversed: 22077450.000000
Num Phases Traversed - Session Mean & Stddev: 220774.500000,0.519615
Num Path Iterations: 22077450.000000
Num Path Iterations - Session Mean & Stddev: 220774.500000,0.519615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fef438a6000 of size 4096 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1537821040.000000
Throughput: 153782104.000000
Num Accesses - Session Mean & Stddev: 15378210.400000,41.400000
Num Phases Traversed: 22287360.000000
Num Phases Traversed - Session Mean & Stddev: 222873.600000,0.600000
Num Path Iterations: 22287360.000000
Num Path Iterations - Session Mean & Stddev: 222873.600000,0.600000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f26c6101000 of size 4096 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1527666241.000000
Throughput: 152766624.100000
Num Accesses - Session Mean & Stddev: 15276662.410000,35.040860
Num Phases Traversed: 22140189.000000
Num Phases Traversed - Session Mean & Stddev: 221401.890000,0.507839
Num Path Iterations: 22140189.000000
Num Path Iterations - Session Mean & Stddev: 221401.890000,0.507839
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f05169e6000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1515243481.000000
Throughput: 151524348.100000
Num Accesses - Session Mean & Stddev: 15152434.810000,35.846812
Num Phases Traversed: 21960149.000000
Num Phases Traversed - Session Mean & Stddev: 219601.490000,0.519519
Num Path Iterations: 21960149.000000
Num Path Iterations - Session Mean & Stddev: 219601.490000,0.519519
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9875930000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1527013846.000000
Throughput: 152701384.600000
Num Accesses - Session Mean & Stddev: 15270138.460000,35.479690
Num Phases Traversed: 22130734.000000
Num Phases Traversed - Session Mean & Stddev: 221307.340000,0.514198
Num Path Iterations: 22130734.000000
Num Path Iterations - Session Mean & Stddev: 221307.340000,0.514198
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f36e3e78000 of size 4096 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 912425050.000000
Throughput: 91242505.000000
Num Accesses - Session Mean & Stddev: 9124250.500000,2021.181845
Num Phases Traversed: 13223650.000000
Num Phases Traversed - Session Mean & Stddev: 132236.500000,29.292491
Num Path Iterations: 13223650.000000
Num Path Iterations - Session Mean & Stddev: 132236.500000,29.292491
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f01db000000 of size 655360 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 850698820.000000
Throughput: 85069882.000000
Num Accesses - Session Mean & Stddev: 8506988.200000,2629.564363
Num Phases Traversed: 165060.000000
Num Phases Traversed - Session Mean & Stddev: 1650.600000,0.509902
Num Path Iterations: 165060.000000
Num Path Iterations - Session Mean & Stddev: 1650.600000,0.509902
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f77d8400000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 852292333.000000
Throughput: 85229233.300000
Num Accesses - Session Mean & Stddev: 8522923.330000,3236.608138
Num Phases Traversed: 165369.000000
Num Phases Traversed - Session Mean & Stddev: 1653.690000,0.627615
Num Path Iterations: 165369.000000
Num Path Iterations - Session Mean & Stddev: 1653.690000,0.627615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f921a600000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 850662721.000000
Throughput: 85066272.100000
Num Accesses - Session Mean & Stddev: 8506627.210000,2675.185983
Num Phases Traversed: 165053.000000
Num Phases Traversed - Session Mean & Stddev: 1650.530000,0.518748
Num Path Iterations: 165053.000000
Num Path Iterations - Session Mean & Stddev: 1650.530000,0.518748
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5846400000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 851410486.000000
Throughput: 85141048.600000
Num Accesses - Session Mean & Stddev: 8514104.860000,3571.385398
Num Phases Traversed: 165198.000000
Num Phases Traversed - Session Mean & Stddev: 1651.980000,0.692532
Num Path Iterations: 165198.000000
Num Path Iterations - Session Mean & Stddev: 1651.980000,0.692532
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3231000000 of size 655360 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 850636936.000000
Throughput: 85063693.600000
Num Accesses - Session Mean & Stddev: 8506369.360000,2677.670138
Num Phases Traversed: 165048.000000
Num Phases Traversed - Session Mean & Stddev: 1650.480000,0.519230
Num Path Iterations: 165048.000000
Num Path Iterations - Session Mean & Stddev: 1650.480000,0.519230
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7d06a00000 of size 655360 bytes.
[1.292s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 850049038.000000
Throughput: 85004903.800000
Num Accesses - Session Mean & Stddev: 8500490.380000,3113.051573
Num Phases Traversed: 164934.000000
Num Phases Traversed - Session Mean & Stddev: 1649.340000,0.603656
Num Path Iterations: 164934.000000
Num Path Iterations - Session Mean & Stddev: 1649.340000,0.603656
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd3bdc00000 of size 655360 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848125477.000000
Throughput: 84812547.700000
Num Accesses - Session Mean & Stddev: 8481254.770000,4118.825876
Num Phases Traversed: 164561.000000
Num Phases Traversed - Session Mean & Stddev: 1645.610000,0.798686
Num Path Iterations: 164561.000000
Num Path Iterations - Session Mean & Stddev: 1645.610000,0.798686
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f63f6a00000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 851090752.000000
Throughput: 85109075.200000
Num Accesses - Session Mean & Stddev: 8510907.520000,3052.665231
Num Phases Traversed: 165136.000000
Num Phases Traversed - Session Mean & Stddev: 1651.360000,0.591946
Num Path Iterations: 165136.000000
Num Path Iterations - Session Mean & Stddev: 1651.360000,0.591946
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd478600000 of size 655360 bytes.
[1.290s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 851183578.000000
Throughput: 85118357.800000
Num Accesses - Session Mean & Stddev: 8511835.780000,2671.704286
Num Phases Traversed: 165154.000000
Num Phases Traversed - Session Mean & Stddev: 1651.540000,0.518073
Num Path Iterations: 165154.000000
Num Path Iterations - Session Mean & Stddev: 1651.540000,0.518073
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff8a2600000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 851508469.000000
Throughput: 85150846.900000
Num Accesses - Session Mean & Stddev: 8515084.690000,3501.827045
Num Phases Traversed: 165217.000000
Num Phases Traversed - Session Mean & Stddev: 1652.170000,0.679043
Num Path Iterations: 165217.000000
Num Path Iterations - Session Mean & Stddev: 1652.170000,0.679043
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1aa6600000 of size 655360 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 412343506.000000
Throughput: 41234350.600000
Num Accesses - Session Mean & Stddev: 4123435.060000,2933.603316
Num Phases Traversed: 80058.000000
Num Phases Traversed - Session Mean & Stddev: 800.580000,0.568859
Num Path Iterations: 80058.000000
Num Path Iterations - Session Mean & Stddev: 800.580000,0.568859
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff27ce00000 of size 655360 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 851575510.000000
Throughput: 85157551.000000
Num Accesses - Session Mean & Stddev: 8515755.100000,3220.545468
Num Phases Traversed: 165230.000000
Num Phases Traversed - Session Mean & Stddev: 1652.300000,0.624500
Num Path Iterations: 165230.000000
Num Path Iterations - Session Mean & Stddev: 1652.300000,0.624500
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f89a0600000 of size 655360 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 847640719.000000
Throughput: 84764071.900000
Num Accesses - Session Mean & Stddev: 8476407.190000,3346.491699
Num Phases Traversed: 164467.000000
Num Phases Traversed - Session Mean & Stddev: 1644.670000,0.648922
Num Path Iterations: 164467.000000
Num Path Iterations - Session Mean & Stddev: 1644.670000,0.648922
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f43c0a00000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 851271247.000000
Throughput: 85127124.700000
Num Accesses - Session Mean & Stddev: 8512712.470000,2944.010209
Num Phases Traversed: 165171.000000
Num Phases Traversed - Session Mean & Stddev: 1651.710000,0.570877
Num Path Iterations: 165171.000000
Num Path Iterations - Session Mean & Stddev: 1651.710000,0.570877
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3f57200000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 851565196.000000
Throughput: 85156519.600000
Num Accesses - Session Mean & Stddev: 8515651.960000,3268.089815
Num Phases Traversed: 165228.000000
Num Phases Traversed - Session Mean & Stddev: 1652.280000,0.633719
Num Path Iterations: 165228.000000
Num Path Iterations - Session Mean & Stddev: 1652.280000,0.633719
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efcfb600000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 850693663.000000
Throughput: 85069366.300000
Num Accesses - Session Mean & Stddev: 8506936.630000,2536.384354
Num Phases Traversed: 165059.000000
Num Phases Traversed - Session Mean & Stddev: 1650.590000,0.491833
Num Path Iterations: 165059.000000
Num Path Iterations - Session Mean & Stddev: 1650.590000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7390800000 of size 655360 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 851183578.000000
Throughput: 85118357.800000
Num Accesses - Session Mean & Stddev: 8511835.780000,2570.235556
Num Phases Traversed: 165154.000000
Num Phases Traversed - Session Mean & Stddev: 1651.540000,0.498397
Num Path Iterations: 165154.000000
Num Path Iterations - Session Mean & Stddev: 1651.540000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0a65800000 of size 655360 bytes.
[1.292s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 850018096.000000
Throughput: 85001809.600000
Num Accesses - Session Mean & Stddev: 8500180.960000,3268.089815
Num Phases Traversed: 164928.000000
Num Phases Traversed - Session Mean & Stddev: 1649.280000,0.633719
Num Path Iterations: 164928.000000
Num Path Iterations - Session Mean & Stddev: 1649.280000,0.633719
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6058e00000 of size 655360 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 851621923.000000
Throughput: 85162192.300000
Num Accesses - Session Mean & Stddev: 8516219.230000,2997.721117
Num Phases Traversed: 165239.000000
Num Phases Traversed - Session Mean & Stddev: 1652.390000,0.581292
Num Path Iterations: 165239.000000
Num Path Iterations - Session Mean & Stddev: 1652.390000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,1 0/5120,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,1 0/5120,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f94fae00000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 851498155.000000
Throughput: 85149815.500000
Num Accesses - Session Mean & Stddev: 8514981.550000,3526.045718
Num Phases Traversed: 165215.000000
Num Phases Traversed - Session Mean & Stddev: 1652.150000,0.683740
Num Path Iterations: 165215.000000
Num Path Iterations - Session Mean & Stddev: 1652.150000,0.683740
+ set +x

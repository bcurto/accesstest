++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7844e00000 of size 327680 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121606265.000000
Throughput: 212160626.500000
Num Accesses - Session Mean & Stddev: 21216062.650000,1291.991187
Num Phases Traversed: 817045.000000
Num Phases Traversed - Session Mean & Stddev: 8170.450000,0.497494
Num Path Iterations: 817045.000000
Num Path Iterations - Session Mean & Stddev: 8170.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7809e00000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121286834.000000
Throughput: 212128683.400000
Num Accesses - Session Mean & Stddev: 21212868.340000,1402.380000
Num Phases Traversed: 816922.000000
Num Phases Traversed - Session Mean & Stddev: 8169.220000,0.540000
Num Path Iterations: 816922.000000
Num Path Iterations - Session Mean & Stddev: 8169.220000,0.540000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faf85000000 of size 327680 bytes.
[1.275s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121292028.000000
Throughput: 212129202.800000
Num Accesses - Session Mean & Stddev: 21212920.280000,1330.313092
Num Phases Traversed: 816924.000000
Num Phases Traversed - Session Mean & Stddev: 8169.240000,0.512250
Num Path Iterations: 816924.000000
Num Path Iterations - Session Mean & Stddev: 8169.240000,0.512250
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feb44e00000 of size 327680 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121525758.000000
Throughput: 212152575.800000
Num Accesses - Session Mean & Stddev: 21215257.580000,1273.324752
Num Phases Traversed: 817014.000000
Num Phases Traversed - Session Mean & Stddev: 8170.140000,0.490306
Num Path Iterations: 817014.000000
Num Path Iterations - Session Mean & Stddev: 8170.140000,0.490306
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fad0a000000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121491997.000000
Throughput: 212149199.700000
Num Accesses - Session Mean & Stddev: 21214919.970000,258.398237
Num Phases Traversed: 817001.000000
Num Phases Traversed - Session Mean & Stddev: 8170.010000,0.099499
Num Path Iterations: 817001.000000
Num Path Iterations - Session Mean & Stddev: 8170.010000,0.099499
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdccac00000 of size 327680 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121107641.000000
Throughput: 212110764.100000
Num Accesses - Session Mean & Stddev: 21211076.410000,1296.160593
Num Phases Traversed: 816853.000000
Num Phases Traversed - Session Mean & Stddev: 8168.530000,0.499099
Num Path Iterations: 816853.000000
Num Path Iterations - Session Mean & Stddev: 8168.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f15e2a00000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121341371.000000
Throughput: 212134137.100000
Num Accesses - Session Mean & Stddev: 21213413.710000,1285.711727
Num Phases Traversed: 816943.000000
Num Phases Traversed - Session Mean & Stddev: 8169.430000,0.495076
Num Path Iterations: 816943.000000
Num Path Iterations - Session Mean & Stddev: 8169.430000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc336a00000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121603668.000000
Throughput: 212160366.800000
Num Accesses - Session Mean & Stddev: 21216036.680000,1289.116898
Num Phases Traversed: 817044.000000
Num Phases Traversed - Session Mean & Stddev: 8170.440000,0.496387
Num Path Iterations: 817044.000000
Num Path Iterations - Session Mean & Stddev: 8170.440000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0be6800000 of size 327680 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121751697.000000
Throughput: 212175169.700000
Num Accesses - Session Mean & Stddev: 21217516.970000,936.001458
Num Phases Traversed: 817101.000000
Num Phases Traversed - Session Mean & Stddev: 8171.010000,0.360416
Num Path Iterations: 817101.000000
Num Path Iterations - Session Mean & Stddev: 8171.010000,0.360416
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6a1e400000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121606265.000000
Throughput: 212160626.500000
Num Accesses - Session Mean & Stddev: 21216062.650000,1291.991187
Num Phases Traversed: 817045.000000
Num Phases Traversed - Session Mean & Stddev: 8170.450000,0.497494
Num Path Iterations: 817045.000000
Num Path Iterations - Session Mean & Stddev: 8170.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9376c00000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121533549.000000
Throughput: 212153354.900000
Num Accesses - Session Mean & Stddev: 21215335.490000,975.518380
Num Phases Traversed: 817017.000000
Num Phases Traversed - Session Mean & Stddev: 8170.170000,0.375633
Num Path Iterations: 817017.000000
Num Path Iterations - Session Mean & Stddev: 8170.170000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa890a00000 of size 327680 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121502385.000000
Throughput: 212150238.500000
Num Accesses - Session Mean & Stddev: 21215023.850000,674.720392
Num Phases Traversed: 817005.000000
Num Phases Traversed - Session Mean & Stddev: 8170.050000,0.259808
Num Path Iterations: 817005.000000
Num Path Iterations - Session Mean & Stddev: 8170.050000,0.259808
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8379600000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121549131.000000
Throughput: 212154913.100000
Num Accesses - Session Mean & Stddev: 21215491.310000,1152.962711
Num Phases Traversed: 817023.000000
Num Phases Traversed - Session Mean & Stddev: 8170.230000,0.443959
Num Path Iterations: 817023.000000
Num Path Iterations - Session Mean & Stddev: 8170.230000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe9f3e00000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121897129.000000
Throughput: 212189712.900000
Num Accesses - Session Mean & Stddev: 21218971.290000,1285.711727
Num Phases Traversed: 817157.000000
Num Phases Traversed - Session Mean & Stddev: 8171.570000,0.495076
Num Path Iterations: 817157.000000
Num Path Iterations - Session Mean & Stddev: 8171.570000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3ec7600000 of size 327680 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2122091904.000000
Throughput: 212209190.400000
Num Accesses - Session Mean & Stddev: 21220919.040000,1211.438566
Num Phases Traversed: 817232.000000
Num Phases Traversed - Session Mean & Stddev: 8172.320000,0.466476
Num Path Iterations: 817232.000000
Num Path Iterations - Session Mean & Stddev: 8172.320000,0.466476
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2381400000 of size 327680 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121258267.000000
Throughput: 212125826.700000
Num Accesses - Session Mean & Stddev: 21212582.670000,1464.256679
Num Phases Traversed: 816911.000000
Num Phases Traversed - Session Mean & Stddev: 8169.110000,0.563826
Num Path Iterations: 816911.000000
Num Path Iterations - Session Mean & Stddev: 8169.110000,0.563826
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f94d1400000 of size 327680 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2121616653.000000
Throughput: 212161665.300000
Num Accesses - Session Mean & Stddev: 21216166.530000,1298.240274
Num Phases Traversed: 817049.000000
Num Phases Traversed - Session Mean & Stddev: 8170.490000,0.499900
Num Path Iterations: 817049.000000
Num Path Iterations - Session Mean & Stddev: 8170.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5393e00000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121393311.000000
Throughput: 212139331.100000
Num Accesses - Session Mean & Stddev: 21213933.110000,1253.842788
Num Phases Traversed: 816963.000000
Num Phases Traversed - Session Mean & Stddev: 8169.630000,0.482804
Num Path Iterations: 816963.000000
Num Path Iterations - Session Mean & Stddev: 8169.630000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4249a00000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121608862.000000
Throughput: 212160886.200000
Num Accesses - Session Mean & Stddev: 21216088.620000,1294.338130
Num Phases Traversed: 817046.000000
Num Phases Traversed - Session Mean & Stddev: 8170.460000,0.498397
Num Path Iterations: 817046.000000
Num Path Iterations - Session Mean & Stddev: 8170.460000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4dd4600000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121434863.000000
Throughput: 212143486.300000
Num Accesses - Session Mean & Stddev: 21214348.630000,1178.420050
Num Phases Traversed: 816979.000000
Num Phases Traversed - Session Mean & Stddev: 8169.790000,0.453762
Num Path Iterations: 816979.000000
Num Path Iterations - Session Mean & Stddev: 8169.790000,0.453762
+ set +x

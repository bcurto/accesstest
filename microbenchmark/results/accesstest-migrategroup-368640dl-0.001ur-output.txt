++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8c80000000 of size 23592960 bytes.
[5.158s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8300000000 of size 23592960 bytes.
[5.169s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3f00000000 of size 23592960 bytes.
[5.160s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8640000000 of size 23592960 bytes.
[5.124s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 211273222.000000
Throughput: 21127322.200000
Num Accesses - Session Mean & Stddev: 2112732.220000,91883.055330
Num Phases Traversed: 1246.000000
Num Phases Traversed - Session Mean & Stddev: 12.460000,0.498397
Num Path Iterations: 1246.000000
Num Path Iterations - Session Mean & Stddev: 12.460000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb840000000 of size 23592960 bytes.
[5.153s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fec80000000 of size 23592960 bytes.
[5.158s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4540000000 of size 23592960 bytes.
[5.156s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0580000000 of size 23592960 bytes.
[5.153s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f80c0000000 of size 23592960 bytes.
[5.152s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8b80000000 of size 23592960 bytes.
[5.117s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0680000000 of size 23592960 bytes.
[5.160s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc700000000 of size 23592960 bytes.
[5.152s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5340000000 of size 23592960 bytes.
[5.153s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5040000000 of size 23592960 bytes.
[5.149s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5c00000000 of size 23592960 bytes.
[5.158s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe2c0000000 of size 23592960 bytes.
[5.125s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 211457579.000000
Throughput: 21145757.900000
Num Accesses - Session Mean & Stddev: 2114575.790000,92012.429101
Num Phases Traversed: 1247.000000
Num Phases Traversed - Session Mean & Stddev: 12.470000,0.499099
Num Path Iterations: 1247.000000
Num Path Iterations - Session Mean & Stddev: 12.470000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7400000000 of size 23592960 bytes.
[5.154s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efd00000000 of size 23592960 bytes.
[5.153s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdc00000000 of size 23592960 bytes.
[5.153s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0000000000 of size 23592960 bytes.
[5.157s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 211641936.000000
Throughput: 21164193.600000
Num Accesses - Session Mean & Stddev: 2116419.360000,92104.727679
Num Phases Traversed: 1248.000000
Num Phases Traversed - Session Mean & Stddev: 12.480000,0.499600
Num Path Iterations: 1248.000000
Num Path Iterations - Session Mean & Stddev: 12.480000,0.499600
+ set +x

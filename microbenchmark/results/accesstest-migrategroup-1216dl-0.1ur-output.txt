++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd7f2000000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1977041200.000000
Throughput: 197704120.000000
Num Accesses - Session Mean & Stddev: 19770412.000000,427.844598
Num Phases Traversed: 3065280.000000
Num Phases Traversed - Session Mean & Stddev: 30652.800000,0.663325
Num Path Iterations: 3065280.000000
Num Path Iterations - Session Mean & Stddev: 30652.800000,0.663325
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efc76200000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1976566480.000000
Throughput: 197656648.000000
Num Accesses - Session Mean & Stddev: 19765664.800000,332.909988
Num Phases Traversed: 3064544.000000
Num Phases Traversed - Session Mean & Stddev: 30645.440000,0.516140
Num Path Iterations: 3064544.000000
Num Path Iterations - Session Mean & Stddev: 30645.440000,0.516140
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5bed000000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1988997565.000000
Throughput: 198899756.500000
Num Accesses - Session Mean & Stddev: 19889975.650000,365.493676
Num Phases Traversed: 3083817.000000
Num Phases Traversed - Session Mean & Stddev: 30838.170000,0.566657
Num Path Iterations: 3083817.000000
Num Path Iterations - Session Mean & Stddev: 30838.170000,0.566657
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8a13800000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1970967235.000000
Throughput: 197096723.500000
Num Accesses - Session Mean & Stddev: 19709672.350000,311.408779
Num Phases Traversed: 3055863.000000
Num Phases Traversed - Session Mean & Stddev: 30558.630000,0.482804
Num Path Iterations: 3055863.000000
Num Path Iterations - Session Mean & Stddev: 30558.630000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4bea800000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1963979305.000000
Throughput: 196397930.500000
Num Accesses - Session Mean & Stddev: 19639793.050000,319.844568
Num Phases Traversed: 3045029.000000
Num Phases Traversed - Session Mean & Stddev: 30450.290000,0.495883
Num Path Iterations: 3045029.000000
Num Path Iterations - Session Mean & Stddev: 30450.290000,0.495883
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2ebaa00000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1977395305.000000
Throughput: 197739530.500000
Num Accesses - Session Mean & Stddev: 19773953.050000,400.679482
Num Phases Traversed: 3065829.000000
Num Phases Traversed - Session Mean & Stddev: 30658.290000,0.621208
Num Path Iterations: 3065829.000000
Num Path Iterations - Session Mean & Stddev: 30658.290000,0.621208
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe5f9800000 of size 77824 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1975935025.000000
Throughput: 197593502.500000
Num Accesses - Session Mean & Stddev: 19759350.250000,307.645392
Num Phases Traversed: 3063565.000000
Num Phases Traversed - Session Mean & Stddev: 30635.650000,0.476970
Num Path Iterations: 3063565.000000
Num Path Iterations - Session Mean & Stddev: 30635.650000,0.476970
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f394ba00000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974248995.000000
Throughput: 197424899.500000
Num Accesses - Session Mean & Stddev: 19742489.950000,322.435494
Num Phases Traversed: 3060951.000000
Num Phases Traversed - Session Mean & Stddev: 30609.510000,0.499900
Num Path Iterations: 3060951.000000
Num Path Iterations - Session Mean & Stddev: 30609.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9365e00000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1978490515.000000
Throughput: 197849051.500000
Num Accesses - Session Mean & Stddev: 19784905.150000,406.452368
Num Phases Traversed: 3067527.000000
Num Phases Traversed - Session Mean & Stddev: 30675.270000,0.630159
Num Path Iterations: 3067527.000000
Num Path Iterations - Session Mean & Stddev: 30675.270000,0.630159
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f00dd400000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1972959640.000000
Throughput: 197295964.000000
Num Accesses - Session Mean & Stddev: 19729596.400000,334.903479
Num Phases Traversed: 3058952.000000
Num Phases Traversed - Session Mean & Stddev: 30589.520000,0.519230
Num Path Iterations: 3058952.000000
Num Path Iterations - Session Mean & Stddev: 30589.520000,0.519230
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2397400000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1973456935.000000
Throughput: 197345693.500000
Num Accesses - Session Mean & Stddev: 19734569.350000,314.069304
Num Phases Traversed: 3059723.000000
Num Phases Traversed - Session Mean & Stddev: 30597.230000,0.486929
Num Path Iterations: 3059723.000000
Num Path Iterations - Session Mean & Stddev: 30597.230000,0.486929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faa80400000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974491515.000000
Throughput: 197449151.500000
Num Accesses - Session Mean & Stddev: 19744915.150000,385.438098
Num Phases Traversed: 3061327.000000
Num Phases Traversed - Session Mean & Stddev: 30613.270000,0.597578
Num Path Iterations: 3061327.000000
Num Path Iterations - Session Mean & Stddev: 30613.270000,0.597578
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe2b6000000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1971282640.000000
Throughput: 197128264.000000
Num Accesses - Session Mean & Stddev: 19712826.400000,334.903479
Num Phases Traversed: 3056352.000000
Num Phases Traversed - Session Mean & Stddev: 30563.520000,0.519230
Num Path Iterations: 3056352.000000
Num Path Iterations - Session Mean & Stddev: 30563.520000,0.519230
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe062600000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1991689795.000000
Throughput: 199168979.500000
Num Accesses - Session Mean & Stddev: 19916897.950000,428.767358
Num Phases Traversed: 3087991.000000
Num Phases Traversed - Session Mean & Stddev: 30879.910000,0.664756
Num Path Iterations: 3087991.000000
Num Path Iterations - Session Mean & Stddev: 30879.910000,0.664756
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0772200000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1972031485.000000
Throughput: 197203148.500000
Num Accesses - Session Mean & Stddev: 19720314.850000,434.166935
Num Phases Traversed: 3057513.000000
Num Phases Traversed - Session Mean & Stddev: 30575.130000,0.673127
Num Path Iterations: 3057513.000000
Num Path Iterations - Session Mean & Stddev: 30575.130000,0.673127
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe989000000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1971043345.000000
Throughput: 197104334.500000
Num Accesses - Session Mean & Stddev: 19710433.450000,424.868506
Num Phases Traversed: 3055981.000000
Num Phases Traversed - Session Mean & Stddev: 30559.810000,0.658711
Num Path Iterations: 3055981.000000
Num Path Iterations - Session Mean & Stddev: 30559.810000,0.658711
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2972a00000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974446365.000000
Throughput: 197444636.500000
Num Accesses - Session Mean & Stddev: 19744463.650000,319.323860
Num Phases Traversed: 3061257.000000
Num Phases Traversed - Session Mean & Stddev: 30612.570000,0.495076
Num Path Iterations: 3061257.000000
Num Path Iterations - Session Mean & Stddev: 30612.570000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fddd5600000 of size 77824 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1991105425.000000
Throughput: 199110542.500000
Num Accesses - Session Mean & Stddev: 19911054.250000,279.293193
Num Phases Traversed: 3087085.000000
Num Phases Traversed - Session Mean & Stddev: 30870.850000,0.433013
Num Path Iterations: 3087085.000000
Num Path Iterations - Session Mean & Stddev: 30870.850000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4752e00000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1986895510.000000
Throughput: 198689551.000000
Num Accesses - Session Mean & Stddev: 19868955.100000,355.394415
Num Phases Traversed: 3080558.000000
Num Phases Traversed - Session Mean & Stddev: 30805.580000,0.550999
Num Path Iterations: 3080558.000000
Num Path Iterations - Session Mean & Stddev: 30805.580000,0.550999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.1 0/608,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.1 0/608,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbcad000000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1983183535.000000
Throughput: 198318353.500000
Num Accesses - Session Mean & Stddev: 19831835.350000,441.766146
Num Phases Traversed: 3074803.000000
Num Phases Traversed - Session Mean & Stddev: 30748.030000,0.684909
Num Path Iterations: 3074803.000000
Num Path Iterations - Session Mean & Stddev: 30748.030000,0.684909
+ set +x

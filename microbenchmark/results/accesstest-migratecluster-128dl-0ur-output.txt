++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7389a00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1953193145.000000
Throughput: 195319314.500000
Num Accesses - Session Mean & Stddev: 19531931.450000,50.246866
Num Phases Traversed: 19338645.000000
Num Phases Traversed - Session Mean & Stddev: 193386.450000,0.497494
Num Path Iterations: 19338645.000000
Num Path Iterations - Session Mean & Stddev: 193386.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f91c3e00000 of size 8192 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1947422611.000000
Throughput: 194742261.100000
Num Accesses - Session Mean & Stddev: 19474226.110000,31.601865
Num Phases Traversed: 19281511.000000
Num Phases Traversed - Session Mean & Stddev: 192815.110000,0.312890
Num Path Iterations: 19281511.000000
Num Path Iterations - Session Mean & Stddev: 192815.110000,0.312890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1d4a000000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1955246980.000000
Throughput: 195524698.000000
Num Accesses - Session Mean & Stddev: 19552469.800000,40.400000
Num Phases Traversed: 19358980.000000
Num Phases Traversed - Session Mean & Stddev: 193589.800000,0.400000
Num Path Iterations: 19358980.000000
Num Path Iterations - Session Mean & Stddev: 193589.800000,0.400000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9420c00000 of size 8192 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1930435926.000000
Throughput: 193043592.600000
Num Accesses - Session Mean & Stddev: 19304359.260000,46.547743
Num Phases Traversed: 19113326.000000
Num Phases Traversed - Session Mean & Stddev: 191133.260000,0.460869
Num Path Iterations: 19113326.000000
Num Path Iterations - Session Mean & Stddev: 191133.260000,0.460869
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0696e00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1947847619.000000
Throughput: 194784761.900000
Num Accesses - Session Mean & Stddev: 19478476.190000,39.622391
Num Phases Traversed: 19285719.000000
Num Phases Traversed - Session Mean & Stddev: 192857.190000,0.392301
Num Path Iterations: 19285719.000000
Num Path Iterations - Session Mean & Stddev: 192857.190000,0.392301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbdea600000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1936518045.000000
Throughput: 193651804.500000
Num Accesses - Session Mean & Stddev: 19365180.450000,50.246866
Num Phases Traversed: 19173545.000000
Num Phases Traversed - Session Mean & Stddev: 191735.450000,0.497494
Num Path Iterations: 19173545.000000
Num Path Iterations - Session Mean & Stddev: 191735.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6fec400000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1948909129.000000
Throughput: 194890912.900000
Num Accesses - Session Mean & Stddev: 19489091.290000,45.829967
Num Phases Traversed: 19296229.000000
Num Phases Traversed - Session Mean & Stddev: 192962.290000,0.453762
Num Path Iterations: 19296229.000000
Num Path Iterations - Session Mean & Stddev: 192962.290000,0.453762
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff3f0200000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1921082417.000000
Throughput: 192108241.700000
Num Accesses - Session Mean & Stddev: 19210824.170000,37.938913
Num Phases Traversed: 19020717.000000
Num Phases Traversed - Session Mean & Stddev: 190207.170000,0.375633
Num Path Iterations: 19020717.000000
Num Path Iterations - Session Mean & Stddev: 190207.170000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f818ea00000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1939282314.000000
Throughput: 193928231.400000
Num Accesses - Session Mean & Stddev: 19392823.140000,35.045690
Num Phases Traversed: 19200914.000000
Num Phases Traversed - Session Mean & Stddev: 192009.140000,0.346987
Num Path Iterations: 19200914.000000
Num Path Iterations - Session Mean & Stddev: 192009.140000,0.346987
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3c9f600000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1954080026.000000
Throughput: 195408002.600000
Num Accesses - Session Mean & Stddev: 19540800.260000,44.302059
Num Phases Traversed: 19347426.000000
Num Phases Traversed - Session Mean & Stddev: 193474.260000,0.438634
Num Path Iterations: 19347426.000000
Num Path Iterations - Session Mean & Stddev: 193474.260000,0.438634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0c28c00000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1955079118.000000
Throughput: 195507911.800000
Num Accesses - Session Mean & Stddev: 19550791.180000,38.802933
Num Phases Traversed: 19357318.000000
Num Phases Traversed - Session Mean & Stddev: 193573.180000,0.384187
Num Path Iterations: 19357318.000000
Num Path Iterations - Session Mean & Stddev: 193573.180000,0.384187
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc1b2600000 of size 8192 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1955505944.000000
Throughput: 195550594.400000
Num Accesses - Session Mean & Stddev: 19555059.440000,50.135082
Num Phases Traversed: 19361544.000000
Num Phases Traversed - Session Mean & Stddev: 193615.440000,0.496387
Num Path Iterations: 19361544.000000
Num Path Iterations - Session Mean & Stddev: 193615.440000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff60a400000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1936325539.000000
Throughput: 193632553.900000
Num Accesses - Session Mean & Stddev: 19363255.390000,49.262744
Num Phases Traversed: 19171639.000000
Num Phases Traversed - Session Mean & Stddev: 191716.390000,0.487750
Num Path Iterations: 19171639.000000
Num Path Iterations - Session Mean & Stddev: 191716.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9309000000 of size 8192 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1957249406.000000
Throughput: 195724940.600000
Num Accesses - Session Mean & Stddev: 19572494.060000,27.916955
Num Phases Traversed: 19378806.000000
Num Phases Traversed - Session Mean & Stddev: 193788.060000,0.276405
Num Path Iterations: 19378806.000000
Num Path Iterations - Session Mean & Stddev: 193788.060000,0.276405
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcd57e00000 of size 8192 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1938984465.000000
Throughput: 193898446.500000
Num Accesses - Session Mean & Stddev: 19389844.650000,50.246866
Num Phases Traversed: 19197965.000000
Num Phases Traversed - Session Mean & Stddev: 191979.650000,0.497494
Num Path Iterations: 19197965.000000
Num Path Iterations - Session Mean & Stddev: 191979.650000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff1adc00000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1945930235.000000
Throughput: 194593023.500000
Num Accesses - Session Mean & Stddev: 19459302.350000,48.173930
Num Phases Traversed: 19266735.000000
Num Phases Traversed - Session Mean & Stddev: 192667.350000,0.476970
Num Path Iterations: 19266735.000000
Num Path Iterations - Session Mean & Stddev: 192667.350000,0.476970
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f819ca00000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1942535423.000000
Throughput: 194253542.300000
Num Accesses - Session Mean & Stddev: 19425354.230000,42.504083
Num Phases Traversed: 19233123.000000
Num Phases Traversed - Session Mean & Stddev: 192331.230000,0.420833
Num Path Iterations: 19233123.000000
Num Path Iterations - Session Mean & Stddev: 192331.230000,0.420833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f500e600000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1929319674.000000
Throughput: 192931967.400000
Num Accesses - Session Mean & Stddev: 19293196.740000,44.302059
Num Phases Traversed: 19102274.000000
Num Phases Traversed - Session Mean & Stddev: 191022.740000,0.438634
Num Path Iterations: 19102274.000000
Num Path Iterations - Session Mean & Stddev: 191022.740000,0.438634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd178c00000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1944004367.000000
Throughput: 194400436.700000
Num Accesses - Session Mean & Stddev: 19440043.670000,47.491484
Num Phases Traversed: 19247667.000000
Num Phases Traversed - Session Mean & Stddev: 192476.670000,0.470213
Num Path Iterations: 19247667.000000
Num Path Iterations - Session Mean & Stddev: 192476.670000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0 0/64,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0 0/64,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8f21200000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1954803186.000000
Throughput: 195480318.600000
Num Accesses - Session Mean & Stddev: 19548031.860000,35.045690
Num Phases Traversed: 19354586.000000
Num Phases Traversed - Session Mean & Stddev: 193545.860000,0.346987
Num Path Iterations: 19354586.000000
Num Path Iterations - Session Mean & Stddev: 193545.860000,0.346987
+ set +x

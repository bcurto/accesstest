++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fea3c800000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823371855.000000
Throughput: 82337185.500000
Num Accesses - Session Mean & Stddev: 8233718.550000,3420.237382
Num Phases Traversed: 244787.000000
Num Phases Traversed - Session Mean & Stddev: 2447.870000,1.016415
Num Path Iterations: 244787.000000
Num Path Iterations - Session Mean & Stddev: 2447.870000,1.016415
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb623e00000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823445885.000000
Throughput: 82344588.500000
Num Accesses - Session Mean & Stddev: 8234458.850000,3300.274250
Num Phases Traversed: 244809.000000
Num Phases Traversed - Session Mean & Stddev: 2448.090000,0.980765
Num Path Iterations: 244809.000000
Num Path Iterations - Session Mean & Stddev: 2448.090000,0.980765
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1a54c00000 of size 425984 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823314650.000000
Throughput: 82331465.000000
Num Accesses - Session Mean & Stddev: 8233146.500000,3762.184372
Num Phases Traversed: 244770.000000
Num Phases Traversed - Session Mean & Stddev: 2447.700000,1.118034
Num Path Iterations: 244770.000000
Num Path Iterations - Session Mean & Stddev: 2447.700000,1.118034
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6523400000 of size 425984 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823381950.000000
Throughput: 82338195.000000
Num Accesses - Session Mean & Stddev: 8233819.500000,3448.098933
Num Phases Traversed: 244790.000000
Num Phases Traversed - Session Mean & Stddev: 2447.900000,1.024695
Num Path Iterations: 244790.000000
Num Path Iterations - Session Mean & Stddev: 2447.900000,1.024695
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa5a0a00000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823324745.000000
Throughput: 82332474.500000
Num Accesses - Session Mean & Stddev: 8233247.450000,3650.831158
Num Phases Traversed: 244773.000000
Num Phases Traversed - Session Mean & Stddev: 2447.730000,1.084942
Num Path Iterations: 244773.000000
Num Path Iterations - Session Mean & Stddev: 2447.730000,1.084942
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcd69200000 of size 425984 bytes.
[1.287s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823492995.000000
Throughput: 82349299.500000
Num Accesses - Session Mean & Stddev: 8234929.950000,2685.894013
Num Phases Traversed: 244823.000000
Num Phases Traversed - Session Mean & Stddev: 2448.230000,0.798185
Num Path Iterations: 244823.000000
Num Path Iterations - Session Mean & Stddev: 2448.230000,0.798185
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbcb9400000 of size 425984 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823368490.000000
Throughput: 82336849.000000
Num Accesses - Session Mean & Stddev: 8233684.900000,3529.883396
Num Phases Traversed: 244786.000000
Num Phases Traversed - Session Mean & Stddev: 2447.860000,1.049000
Num Path Iterations: 244786.000000
Num Path Iterations - Session Mean & Stddev: 2447.860000,1.049000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff99e200000 of size 425984 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823654515.000000
Throughput: 82365451.500000
Num Accesses - Session Mean & Stddev: 8236545.150000,2575.709131
Num Phases Traversed: 244871.000000
Num Phases Traversed - Session Mean & Stddev: 2448.710000,0.765441
Num Path Iterations: 244871.000000
Num Path Iterations - Session Mean & Stddev: 2448.710000,0.765441
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6ab1000000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823385315.000000
Throughput: 82338531.500000
Num Accesses - Session Mean & Stddev: 8233853.150000,3595.829616
Num Phases Traversed: 244791.000000
Num Phases Traversed - Session Mean & Stddev: 2447.910000,1.068597
Num Path Iterations: 244791.000000
Num Path Iterations - Session Mean & Stddev: 2447.910000,1.068597
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f49ffc00000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823331475.000000
Throughput: 82333147.500000
Num Accesses - Session Mean & Stddev: 8233314.750000,3728.170957
Num Phases Traversed: 244775.000000
Num Phases Traversed - Session Mean & Stddev: 2447.750000,1.107926
Num Path Iterations: 244775.000000
Num Path Iterations - Session Mean & Stddev: 2447.750000,1.107926
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5265000000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823324745.000000
Throughput: 82332474.500000
Num Accesses - Session Mean & Stddev: 8233247.450000,3920.044470
Num Phases Traversed: 244773.000000
Num Phases Traversed - Session Mean & Stddev: 2447.730000,1.164946
Num Path Iterations: 244773.000000
Num Path Iterations - Session Mean & Stddev: 2447.730000,1.164946
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8dad800000 of size 425984 bytes.
[1.279s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823432425.000000
Throughput: 82343242.500000
Num Accesses - Session Mean & Stddev: 8234324.250000,3343.902613
Num Phases Traversed: 244805.000000
Num Phases Traversed - Session Mean & Stddev: 2448.050000,0.993730
Num Path Iterations: 244805.000000
Num Path Iterations - Session Mean & Stddev: 2448.050000,0.993730
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff3a0600000 of size 425984 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823287730.000000
Throughput: 82328773.000000
Num Accesses - Session Mean & Stddev: 8232877.300000,3859.641798
Num Phases Traversed: 244762.000000
Num Phases Traversed - Session Mean & Stddev: 2447.620000,1.146996
Num Path Iterations: 244762.000000
Num Path Iterations - Session Mean & Stddev: 2447.620000,1.146996
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4747600000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823533375.000000
Throughput: 82353337.500000
Num Accesses - Session Mean & Stddev: 8235333.750000,2622.757935
Num Phases Traversed: 244835.000000
Num Phases Traversed - Session Mean & Stddev: 2448.350000,0.779423
Num Path Iterations: 244835.000000
Num Path Iterations - Session Mean & Stddev: 2448.350000,0.779423
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0676600000 of size 425984 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823466075.000000
Throughput: 82346607.500000
Num Accesses - Session Mean & Stddev: 8234660.750000,2947.978152
Num Phases Traversed: 244815.000000
Num Phases Traversed - Session Mean & Stddev: 2448.150000,0.876071
Num Path Iterations: 244815.000000
Num Path Iterations - Session Mean & Stddev: 2448.150000,0.876071
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc299e00000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823439155.000000
Throughput: 82343915.500000
Num Accesses - Session Mean & Stddev: 8234391.550000,3406.969026
Num Phases Traversed: 244807.000000
Num Phases Traversed - Session Mean & Stddev: 2448.070000,1.012472
Num Path Iterations: 244807.000000
Num Path Iterations - Session Mean & Stddev: 2448.070000,1.012472
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f31a3800000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823270905.000000
Throughput: 82327090.500000
Num Accesses - Session Mean & Stddev: 8232709.050000,3693.997624
Num Phases Traversed: 244757.000000
Num Phases Traversed - Session Mean & Stddev: 2447.570000,1.097770
Num Path Iterations: 244757.000000
Num Path Iterations - Session Mean & Stddev: 2447.570000,1.097770
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f11b2e00000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823445885.000000
Throughput: 82344588.500000
Num Accesses - Session Mean & Stddev: 8234458.850000,3265.784075
Num Phases Traversed: 244809.000000
Num Phases Traversed - Session Mean & Stddev: 2448.090000,0.970515
Num Path Iterations: 244809.000000
Num Path Iterations - Session Mean & Stddev: 2448.090000,0.970515
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fabdf000000 of size 425984 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823492995.000000
Throughput: 82349299.500000
Num Accesses - Session Mean & Stddev: 8234929.950000,2849.541129
Num Phases Traversed: 244823.000000
Num Phases Traversed - Session Mean & Stddev: 2448.230000,0.846818
Num Path Iterations: 244823.000000
Num Path Iterations - Session Mean & Stddev: 2448.230000,0.846818
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa2a2a00000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823432425.000000
Throughput: 82343242.500000
Num Accesses - Session Mean & Stddev: 8234324.250000,3275.477933
Num Phases Traversed: 244805.000000
Num Phases Traversed - Session Mean & Stddev: 2448.050000,0.973396
Num Path Iterations: 244805.000000
Num Path Iterations - Session Mean & Stddev: 2448.050000,0.973396
+ set +x

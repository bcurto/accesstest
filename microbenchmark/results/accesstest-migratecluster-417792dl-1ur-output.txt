++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1400000000 of size 26738688 bytes.
[5.360s] Test is ready to start.
Test duration in range [10.000, 10.013] seconds
Num Accesses: 229617467.000000
Throughput: 22961746.700000
Num Accesses - Session Mean & Stddev: 2296174.670000,36127.893030
Num Phases Traversed: 1199.000000
Num Phases Traversed - Session Mean & Stddev: 11.990000,0.172916
Num Path Iterations: 1199.000000
Num Path Iterations - Session Mean & Stddev: 11.990000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f72c0000000 of size 26738688 bytes.
[5.288s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 229617467.000000
Throughput: 22961746.700000
Num Accesses - Session Mean & Stddev: 2296174.670000,36127.893030
Num Phases Traversed: 1199.000000
Num Phases Traversed - Session Mean & Stddev: 11.990000,0.172916
Num Path Iterations: 1199.000000
Num Path Iterations - Session Mean & Stddev: 11.990000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff040000000 of size 26738688 bytes.
[5.350s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 229617467.000000
Throughput: 22961746.700000
Num Accesses - Session Mean & Stddev: 2296174.670000,36127.893030
Num Phases Traversed: 1199.000000
Num Phases Traversed - Session Mean & Stddev: 11.990000,0.172916
Num Path Iterations: 1199.000000
Num Path Iterations - Session Mean & Stddev: 11.990000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1440000000 of size 26738688 bytes.
[5.349s] Test is ready to start.
Test duration in range [10.000, 10.018] seconds
Num Accesses: 229826400.000000
Throughput: 22982640.000000
Num Accesses - Session Mean & Stddev: 2298264.000000,29547.588223
Num Phases Traversed: 1200.000000
Num Phases Traversed - Session Mean & Stddev: 12.000000,0.141421
Num Path Iterations: 1200.000000
Num Path Iterations - Session Mean & Stddev: 12.000000,0.141421
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3840000000 of size 26738688 bytes.
[5.347s] Test is ready to start.
Test duration in range [10.000, 10.017] seconds
Num Accesses: 229826400.000000
Throughput: 22982640.000000
Num Accesses - Session Mean & Stddev: 2298264.000000,29547.588223
Num Phases Traversed: 1200.000000
Num Phases Traversed - Session Mean & Stddev: 12.000000,0.141421
Num Path Iterations: 1200.000000
Num Path Iterations - Session Mean & Stddev: 12.000000,0.141421
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faa80000000 of size 26738688 bytes.
[5.351s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 229617467.000000
Throughput: 22961746.700000
Num Accesses - Session Mean & Stddev: 2296174.670000,36127.893030
Num Phases Traversed: 1199.000000
Num Phases Traversed - Session Mean & Stddev: 11.990000,0.172916
Num Path Iterations: 1199.000000
Num Path Iterations - Session Mean & Stddev: 11.990000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdd40000000 of size 26738688 bytes.
[5.349s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 229617467.000000
Throughput: 22961746.700000
Num Accesses - Session Mean & Stddev: 2296174.670000,36127.893030
Num Phases Traversed: 1199.000000
Num Phases Traversed - Session Mean & Stddev: 11.990000,0.172916
Num Path Iterations: 1199.000000
Num Path Iterations - Session Mean & Stddev: 11.990000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1e80000000 of size 26738688 bytes.
[5.351s] Test is ready to start.
Test duration in range [10.000, 10.013] seconds
Num Accesses: 229617467.000000
Throughput: 22961746.700000
Num Accesses - Session Mean & Stddev: 2296174.670000,36127.893030
Num Phases Traversed: 1199.000000
Num Phases Traversed - Session Mean & Stddev: 11.990000,0.172916
Num Path Iterations: 1199.000000
Num Path Iterations - Session Mean & Stddev: 11.990000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f96c0000000 of size 26738688 bytes.
[5.348s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 229826400.000000
Throughput: 22982640.000000
Num Accesses - Session Mean & Stddev: 2298264.000000,29547.588223
Num Phases Traversed: 1200.000000
Num Phases Traversed - Session Mean & Stddev: 12.000000,0.141421
Num Path Iterations: 1200.000000
Num Path Iterations - Session Mean & Stddev: 12.000000,0.141421
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f72c0000000 of size 26738688 bytes.
[5.341s] Test is ready to start.
Test duration in range [10.000, 10.013] seconds
Num Accesses: 229617467.000000
Throughput: 22961746.700000
Num Accesses - Session Mean & Stddev: 2296174.670000,36127.893030
Num Phases Traversed: 1199.000000
Num Phases Traversed - Session Mean & Stddev: 11.990000,0.172916
Num Path Iterations: 1199.000000
Num Path Iterations - Session Mean & Stddev: 11.990000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4380000000 of size 26738688 bytes.
[5.288s] Test is ready to start.
Test duration in range [10.000, 10.017] seconds
Num Accesses: 229199601.000000
Throughput: 22919960.100000
Num Accesses - Session Mean & Stddev: 2291996.010000,35641.299864
Num Phases Traversed: 1197.000000
Num Phases Traversed - Session Mean & Stddev: 11.970000,0.170587
Num Path Iterations: 1197.000000
Num Path Iterations - Session Mean & Stddev: 11.970000,0.170587
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb040000000 of size 26738688 bytes.
[5.351s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 229617467.000000
Throughput: 22961746.700000
Num Accesses - Session Mean & Stddev: 2296174.670000,36127.893030
Num Phases Traversed: 1199.000000
Num Phases Traversed - Session Mean & Stddev: 11.990000,0.172916
Num Path Iterations: 1199.000000
Num Path Iterations - Session Mean & Stddev: 11.990000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5980000000 of size 26738688 bytes.
[5.346s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 229617467.000000
Throughput: 22961746.700000
Num Accesses - Session Mean & Stddev: 2296174.670000,36127.893030
Num Phases Traversed: 1199.000000
Num Phases Traversed - Session Mean & Stddev: 11.990000,0.172916
Num Path Iterations: 1199.000000
Num Path Iterations - Session Mean & Stddev: 11.990000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe5c0000000 of size 26738688 bytes.
[5.347s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 229617467.000000
Throughput: 22961746.700000
Num Accesses - Session Mean & Stddev: 2296174.670000,36127.893030
Num Phases Traversed: 1199.000000
Num Phases Traversed - Session Mean & Stddev: 11.990000,0.172916
Num Path Iterations: 1199.000000
Num Path Iterations - Session Mean & Stddev: 11.990000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdf40000000 of size 26738688 bytes.
[5.350s] Test is ready to start.
Test duration in range [10.000, 10.017] seconds
Num Accesses: 229826400.000000
Throughput: 22982640.000000
Num Accesses - Session Mean & Stddev: 2298264.000000,29547.588223
Num Phases Traversed: 1200.000000
Num Phases Traversed - Session Mean & Stddev: 12.000000,0.141421
Num Path Iterations: 1200.000000
Num Path Iterations - Session Mean & Stddev: 12.000000,0.141421
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0040000000 of size 26738688 bytes.
[5.358s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 229617467.000000
Throughput: 22961746.700000
Num Accesses - Session Mean & Stddev: 2296174.670000,36127.893030
Num Phases Traversed: 1199.000000
Num Phases Traversed - Session Mean & Stddev: 11.990000,0.172916
Num Path Iterations: 1199.000000
Num Path Iterations - Session Mean & Stddev: 11.990000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0c80000000 of size 26738688 bytes.
[5.349s] Test is ready to start.
Test duration in range [10.000, 10.017] seconds
Num Accesses: 229826400.000000
Throughput: 22982640.000000
Num Accesses - Session Mean & Stddev: 2298264.000000,29547.588223
Num Phases Traversed: 1200.000000
Num Phases Traversed - Session Mean & Stddev: 12.000000,0.141421
Num Path Iterations: 1200.000000
Num Path Iterations - Session Mean & Stddev: 12.000000,0.141421
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6ac0000000 of size 26738688 bytes.
[5.347s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 229617467.000000
Throughput: 22961746.700000
Num Accesses - Session Mean & Stddev: 2296174.670000,36127.893030
Num Phases Traversed: 1199.000000
Num Phases Traversed - Session Mean & Stddev: 11.990000,0.172916
Num Path Iterations: 1199.000000
Num Path Iterations - Session Mean & Stddev: 11.990000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faf40000000 of size 26738688 bytes.
[5.349s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 229617467.000000
Throughput: 22961746.700000
Num Accesses - Session Mean & Stddev: 2296174.670000,36127.893030
Num Phases Traversed: 1199.000000
Num Phases Traversed - Session Mean & Stddev: 11.990000,0.172916
Num Path Iterations: 1199.000000
Num Path Iterations - Session Mean & Stddev: 11.990000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/208896,1 0/208896,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/208896,1 0/208896,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    208896 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc3c0000000 of size 26738688 bytes.
[5.359s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 229617467.000000
Throughput: 22961746.700000
Num Accesses - Session Mean & Stddev: 2296174.670000,36127.893030
Num Phases Traversed: 1199.000000
Num Phases Traversed - Session Mean & Stddev: 11.990000,0.172916
Num Path Iterations: 1199.000000
Num Path Iterations - Session Mean & Stddev: 11.990000,0.172916
+ set +x

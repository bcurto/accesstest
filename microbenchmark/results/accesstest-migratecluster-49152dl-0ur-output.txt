++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3740000000 of size 3145728 bytes.
[1.524s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831254949.000000
Throughput: 83125494.900000
Num Accesses - Session Mean & Stddev: 8312549.490000,15895.882601
Num Phases Traversed: 33873.000000
Num Phases Traversed - Session Mean & Stddev: 338.730000,0.645833
Num Path Iterations: 33873.000000
Num Path Iterations - Session Mean & Stddev: 338.730000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbbc0000000 of size 3145728 bytes.
[1.529s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831304175.000000
Throughput: 83130417.500000
Num Accesses - Session Mean & Stddev: 8313041.750000,16092.836955
Num Phases Traversed: 33875.000000
Num Phases Traversed - Session Mean & Stddev: 338.750000,0.653835
Num Path Iterations: 33875.000000
Num Path Iterations - Session Mean & Stddev: 338.750000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2a40000000 of size 3145728 bytes.
[1.496s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830713463.000000
Throughput: 83071346.300000
Num Accesses - Session Mean & Stddev: 8307134.630000,12304.038454
Num Phases Traversed: 33851.000000
Num Phases Traversed - Session Mean & Stddev: 338.510000,0.499900
Num Path Iterations: 33851.000000
Num Path Iterations - Session Mean & Stddev: 338.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4640000000 of size 3145728 bytes.
[1.529s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831107271.000000
Throughput: 83110727.100000
Num Accesses - Session Mean & Stddev: 8311072.710000,15194.416473
Num Phases Traversed: 33867.000000
Num Phases Traversed - Session Mean & Stddev: 338.670000,0.617333
Num Path Iterations: 33867.000000
Num Path Iterations - Session Mean & Stddev: 338.670000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbe00000000 of size 3145728 bytes.
[1.516s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831107271.000000
Throughput: 83110727.100000
Num Accesses - Session Mean & Stddev: 8311072.710000,15194.416473
Num Phases Traversed: 33867.000000
Num Phases Traversed - Session Mean & Stddev: 338.670000,0.617333
Num Path Iterations: 33867.000000
Num Path Iterations - Session Mean & Stddev: 338.670000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb100000000 of size 3145728 bytes.
[1.539s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831156497.000000
Throughput: 83115649.700000
Num Accesses - Session Mean & Stddev: 8311564.970000,15447.476461
Num Phases Traversed: 33869.000000
Num Phases Traversed - Session Mean & Stddev: 338.690000,0.627615
Num Path Iterations: 33869.000000
Num Path Iterations - Session Mean & Stddev: 338.690000,0.627615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7c40000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831156497.000000
Throughput: 83115649.700000
Num Accesses - Session Mean & Stddev: 8311564.970000,15447.476461
Num Phases Traversed: 33869.000000
Num Phases Traversed - Session Mean & Stddev: 338.690000,0.627615
Num Path Iterations: 33869.000000
Num Path Iterations - Session Mean & Stddev: 338.690000,0.627615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8a40000000 of size 3145728 bytes.
[1.529s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831254949.000000
Throughput: 83125494.900000
Num Accesses - Session Mean & Stddev: 8312549.490000,15895.882601
Num Phases Traversed: 33873.000000
Num Phases Traversed - Session Mean & Stddev: 338.730000,0.645833
Num Path Iterations: 33873.000000
Num Path Iterations - Session Mean & Stddev: 338.730000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7febc0000000 of size 3145728 bytes.
[1.524s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831254949.000000
Throughput: 83125494.900000
Num Accesses - Session Mean & Stddev: 8312549.490000,15895.882601
Num Phases Traversed: 33873.000000
Num Phases Traversed - Session Mean & Stddev: 338.730000,0.645833
Num Path Iterations: 33873.000000
Num Path Iterations - Session Mean & Stddev: 338.730000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc640000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831156497.000000
Throughput: 83115649.700000
Num Accesses - Session Mean & Stddev: 8311564.970000,15447.476461
Num Phases Traversed: 33869.000000
Num Phases Traversed - Session Mean & Stddev: 338.690000,0.627615
Num Path Iterations: 33869.000000
Num Path Iterations - Session Mean & Stddev: 338.690000,0.627615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd940000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831205723.000000
Throughput: 83120572.300000
Num Accesses - Session Mean & Stddev: 8312057.230000,15681.011646
Num Phases Traversed: 33871.000000
Num Phases Traversed - Session Mean & Stddev: 338.710000,0.637103
Num Path Iterations: 33871.000000
Num Path Iterations - Session Mean & Stddev: 338.710000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd100000000 of size 3145728 bytes.
[1.527s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831156497.000000
Throughput: 83115649.700000
Num Accesses - Session Mean & Stddev: 8311564.970000,15447.476461
Num Phases Traversed: 33869.000000
Num Phases Traversed - Session Mean & Stddev: 338.690000,0.627615
Num Path Iterations: 33869.000000
Num Path Iterations - Session Mean & Stddev: 338.690000,0.627615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9fc0000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831254949.000000
Throughput: 83125494.900000
Num Accesses - Session Mean & Stddev: 8312549.490000,15895.882601
Num Phases Traversed: 33873.000000
Num Phases Traversed - Session Mean & Stddev: 338.730000,0.645833
Num Path Iterations: 33873.000000
Num Path Iterations - Session Mean & Stddev: 338.730000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f76c0000000 of size 3145728 bytes.
[1.524s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831107271.000000
Throughput: 83110727.100000
Num Accesses - Session Mean & Stddev: 8311072.710000,15194.416473
Num Phases Traversed: 33867.000000
Num Phases Traversed - Session Mean & Stddev: 338.670000,0.617333
Num Path Iterations: 33867.000000
Num Path Iterations - Session Mean & Stddev: 338.670000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb540000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831205723.000000
Throughput: 83120572.300000
Num Accesses - Session Mean & Stddev: 8312057.230000,15681.011646
Num Phases Traversed: 33871.000000
Num Phases Traversed - Session Mean & Stddev: 338.710000,0.637103
Num Path Iterations: 33871.000000
Num Path Iterations - Session Mean & Stddev: 338.710000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1080000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831205723.000000
Throughput: 83120572.300000
Num Accesses - Session Mean & Stddev: 8312057.230000,15681.011646
Num Phases Traversed: 33871.000000
Num Phases Traversed - Session Mean & Stddev: 338.710000,0.637103
Num Path Iterations: 33871.000000
Num Path Iterations - Session Mean & Stddev: 338.710000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbe80000000 of size 3145728 bytes.
[1.525s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831107271.000000
Throughput: 83110727.100000
Num Accesses - Session Mean & Stddev: 8311072.710000,15194.416473
Num Phases Traversed: 33867.000000
Num Phases Traversed - Session Mean & Stddev: 338.670000,0.617333
Num Path Iterations: 33867.000000
Num Path Iterations - Session Mean & Stddev: 338.670000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f73c0000000 of size 3145728 bytes.
[1.539s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831254949.000000
Throughput: 83125494.900000
Num Accesses - Session Mean & Stddev: 8312549.490000,15895.882601
Num Phases Traversed: 33873.000000
Num Phases Traversed - Session Mean & Stddev: 338.730000,0.645833
Num Path Iterations: 33873.000000
Num Path Iterations - Session Mean & Stddev: 338.730000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fad80000000 of size 3145728 bytes.
[1.529s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831205723.000000
Throughput: 83120572.300000
Num Accesses - Session Mean & Stddev: 8312057.230000,15681.011646
Num Phases Traversed: 33871.000000
Num Phases Traversed - Session Mean & Stddev: 338.710000,0.637103
Num Path Iterations: 33871.000000
Num Path Iterations - Session Mean & Stddev: 338.710000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0 0/24576,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0 0/24576,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8040000000 of size 3145728 bytes.
[1.525s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831254949.000000
Throughput: 83125494.900000
Num Accesses - Session Mean & Stddev: 8312549.490000,15895.882601
Num Phases Traversed: 33873.000000
Num Phases Traversed - Session Mean & Stddev: 338.730000,0.645833
Num Path Iterations: 33873.000000
Num Path Iterations - Session Mean & Stddev: 338.730000,0.645833
+ set +x

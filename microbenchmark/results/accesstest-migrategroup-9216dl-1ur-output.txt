++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f841be00000 of size 589824 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1065070730.000000
Throughput: 106507073.000000
Num Accesses - Session Mean & Stddev: 10650707.300000,3206.059624
Num Phases Traversed: 229394.000000
Num Phases Traversed - Session Mean & Stddev: 2293.940000,0.690217
Num Path Iterations: 229394.000000
Num Path Iterations - Session Mean & Stddev: 2293.940000,0.690217
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2329400000 of size 589824 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1061178220.000000
Throughput: 106117822.000000
Num Accesses - Session Mean & Stddev: 10611782.200000,2485.834580
Num Phases Traversed: 228556.000000
Num Phases Traversed - Session Mean & Stddev: 2285.560000,0.535164
Num Path Iterations: 228556.000000
Num Path Iterations - Session Mean & Stddev: 2285.560000,0.535164
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f51ba800000 of size 589824 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1054777410.000000
Throughput: 105477741.000000
Num Accesses - Session Mean & Stddev: 10547774.100000,3051.591780
Num Phases Traversed: 227178.000000
Num Phases Traversed - Session Mean & Stddev: 2271.780000,0.656963
Num Path Iterations: 227178.000000
Num Path Iterations - Session Mean & Stddev: 2271.780000,0.656963
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f75efa00000 of size 589824 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1057675890.000000
Throughput: 105767589.000000
Num Accesses - Session Mean & Stddev: 10576758.900000,3216.809225
Num Phases Traversed: 227802.000000
Num Phases Traversed - Session Mean & Stddev: 2278.020000,0.692532
Num Path Iterations: 227802.000000
Num Path Iterations - Session Mean & Stddev: 2278.020000,0.692532
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f905ec00000 of size 589824 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1064596940.000000
Throughput: 106459694.000000
Num Accesses - Session Mean & Stddev: 10645969.400000,3196.624069
Num Phases Traversed: 229292.000000
Num Phases Traversed - Session Mean & Stddev: 2292.920000,0.688186
Num Path Iterations: 229292.000000
Num Path Iterations - Session Mean & Stddev: 2292.920000,0.688186
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9ad6600000 of size 589824 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1060253865.000000
Throughput: 106025386.500000
Num Accesses - Session Mean & Stddev: 10602538.650000,2565.705600
Num Phases Traversed: 228357.000000
Num Phases Traversed - Session Mean & Stddev: 2283.570000,0.552359
Num Path Iterations: 228357.000000
Num Path Iterations - Session Mean & Stddev: 2283.570000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff15c800000 of size 589824 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1061768135.000000
Throughput: 106176813.500000
Num Accesses - Session Mean & Stddev: 10617681.350000,3154.156801
Num Phases Traversed: 228683.000000
Num Phases Traversed - Session Mean & Stddev: 2286.830000,0.679043
Num Path Iterations: 228683.000000
Num Path Iterations - Session Mean & Stddev: 2286.830000,0.679043
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb1c2e00000 of size 589824 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1057164940.000000
Throughput: 105716494.000000
Num Accesses - Session Mean & Stddev: 10571649.400000,3196.624069
Num Phases Traversed: 227692.000000
Num Phases Traversed - Session Mean & Stddev: 2276.920000,0.688186
Num Path Iterations: 227692.000000
Num Path Iterations - Session Mean & Stddev: 2276.920000,0.688186
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5609c00000 of size 589824 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 452683220.000000
Throughput: 45268322.000000
Num Accesses - Session Mean & Stddev: 4526832.200000,2653.754804
Num Phases Traversed: 97556.000000
Num Phases Traversed - Session Mean & Stddev: 975.560000,0.571314
Num Path Iterations: 97556.000000
Num Path Iterations - Session Mean & Stddev: 975.560000,0.571314
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f06a9a00000 of size 589824 bytes.
[1.293s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1062404500.000000
Throughput: 106240450.000000
Num Accesses - Session Mean & Stddev: 10624045.000000,3081.144430
Num Phases Traversed: 228820.000000
Num Phases Traversed - Session Mean & Stddev: 2288.200000,0.663325
Num Path Iterations: 228820.000000
Num Path Iterations - Session Mean & Stddev: 2288.200000,0.663325
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3b08600000 of size 589824 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 452297685.000000
Throughput: 45229768.500000
Num Accesses - Session Mean & Stddev: 4522976.850000,3140.445992
Num Phases Traversed: 97473.000000
Num Phases Traversed - Session Mean & Stddev: 974.730000,0.676092
Num Path Iterations: 97473.000000
Num Path Iterations - Session Mean & Stddev: 974.730000,0.676092
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdf42600000 of size 589824 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1062752875.000000
Throughput: 106275287.500000
Num Accesses - Session Mean & Stddev: 10627528.750000,3175.970983
Num Phases Traversed: 228895.000000
Num Phases Traversed - Session Mean & Stddev: 2288.950000,0.683740
Num Path Iterations: 228895.000000
Num Path Iterations - Session Mean & Stddev: 2288.950000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f117a800000 of size 589824 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1067249235.000000
Throughput: 106724923.500000
Num Accesses - Session Mean & Stddev: 10672492.350000,2760.162029
Num Phases Traversed: 229863.000000
Num Phases Traversed - Session Mean & Stddev: 2298.630000,0.594222
Num Path Iterations: 229863.000000
Num Path Iterations - Session Mean & Stddev: 2298.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0579a00000 of size 589824 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1064610875.000000
Throughput: 106461087.500000
Num Accesses - Session Mean & Stddev: 10646108.750000,3243.194750
Num Phases Traversed: 229295.000000
Num Phases Traversed - Session Mean & Stddev: 2292.950000,0.698212
Num Path Iterations: 229295.000000
Num Path Iterations - Session Mean & Stddev: 2292.950000,0.698212
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f33faa00000 of size 589824 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1062869000.000000
Throughput: 106286900.000000
Num Accesses - Session Mean & Stddev: 10628690.000000,3284.510999
Num Phases Traversed: 228920.000000
Num Phases Traversed - Session Mean & Stddev: 2289.200000,0.707107
Num Path Iterations: 228920.000000
Num Path Iterations - Session Mean & Stddev: 2289.200000,0.707107
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6a9ba00000 of size 589824 bytes.
[1.292s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1059710400.000000
Throughput: 105971040.000000
Num Accesses - Session Mean & Stddev: 10597104.000000,2627.608799
Num Phases Traversed: 228240.000000
Num Phases Traversed - Session Mean & Stddev: 2282.400000,0.565685
Num Path Iterations: 228240.000000
Num Path Iterations - Session Mean & Stddev: 2282.400000,0.565685
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7c73e00000 of size 589824 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1066510680.000000
Throughput: 106651068.000000
Num Accesses - Session Mean & Stddev: 10665106.800000,3711.352093
Num Phases Traversed: 229704.000000
Num Phases Traversed - Session Mean & Stddev: 2297.040000,0.798999
Num Path Iterations: 229704.000000
Num Path Iterations - Session Mean & Stddev: 2297.040000,0.798999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7562600000 of size 589824 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1062362695.000000
Throughput: 106236269.500000
Num Accesses - Session Mean & Stddev: 10623626.950000,3143.192938
Num Phases Traversed: 228811.000000
Num Phases Traversed - Session Mean & Stddev: 2288.110000,0.676683
Num Path Iterations: 228811.000000
Num Path Iterations - Session Mean & Stddev: 2288.110000,0.676683
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb287000000 of size 589824 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1066561775.000000
Throughput: 106656177.500000
Num Accesses - Session Mean & Stddev: 10665617.750000,3175.970983
Num Phases Traversed: 229715.000000
Num Phases Traversed - Session Mean & Stddev: 2297.150000,0.683740
Num Path Iterations: 229715.000000
Num Path Iterations - Session Mean & Stddev: 2297.150000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,1 0/4608,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,1 0/4608,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa35c600000 of size 589824 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 452980500.000000
Throughput: 45298050.000000
Num Accesses - Session Mean & Stddev: 4529805.000000,3218.150400
Num Phases Traversed: 97620.000000
Num Phases Traversed - Session Mean & Stddev: 976.200000,0.692820
Num Path Iterations: 97620.000000
Num Path Iterations - Session Mean & Stddev: 976.200000,0.692820
+ set +x

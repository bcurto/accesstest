++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efd1ba00000 of size 24576 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 857898678.000000
Throughput: 85789867.800000
Num Accesses - Session Mean & Stddev: 8578986.780000,299.246139
Num Phases Traversed: 3746382.000000
Num Phases Traversed - Session Mean & Stddev: 37463.820000,1.306752
Num Path Iterations: 3746382.000000
Num Path Iterations - Session Mean & Stddev: 37463.820000,1.306752
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb7c4c00000 of size 24576 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 855857830.000000
Throughput: 85585783.000000
Num Accesses - Session Mean & Stddev: 8558578.300000,302.938525
Num Phases Traversed: 3737470.000000
Num Phases Traversed - Session Mean & Stddev: 37374.700000,1.322876
Num Path Iterations: 3737470.000000
Num Path Iterations - Session Mean & Stddev: 37374.700000,1.322876
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f33da800000 of size 24576 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 856798104.000000
Throughput: 85679810.400000
Num Accesses - Session Mean & Stddev: 8567981.040000,263.340043
Num Phases Traversed: 3741576.000000
Num Phases Traversed - Session Mean & Stddev: 37415.760000,1.149957
Num Path Iterations: 3741576.000000
Num Path Iterations - Session Mean & Stddev: 37415.760000,1.149957
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff404a00000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 850784106.000000
Throughput: 85078410.600000
Num Accesses - Session Mean & Stddev: 8507841.060000,240.220891
Num Phases Traversed: 3715314.000000
Num Phases Traversed - Session Mean & Stddev: 37153.140000,1.049000
Num Path Iterations: 3715314.000000
Num Path Iterations - Session Mean & Stddev: 37153.140000,1.049000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7eff67e00000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 857938066.000000
Throughput: 85793806.600000
Num Accesses - Session Mean & Stddev: 8579380.660000,245.404247
Num Phases Traversed: 3746554.000000
Num Phases Traversed - Session Mean & Stddev: 37465.540000,1.071634
Num Path Iterations: 3746554.000000
Num Path Iterations - Session Mean & Stddev: 37465.540000,1.071634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3e6f600000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 856267282.000000
Throughput: 85626728.200000
Num Accesses - Session Mean & Stddev: 8562672.820000,386.162178
Num Phases Traversed: 3739258.000000
Num Phases Traversed - Session Mean & Stddev: 37392.580000,1.686298
Num Path Iterations: 3739258.000000
Num Path Iterations - Session Mean & Stddev: 37392.580000,1.686298
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faae3400000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 857837306.000000
Throughput: 85783730.600000
Num Accesses - Session Mean & Stddev: 8578373.060000,263.140830
Num Phases Traversed: 3746114.000000
Num Phases Traversed - Session Mean & Stddev: 37461.140000,1.149087
Num Path Iterations: 3746114.000000
Num Path Iterations - Session Mean & Stddev: 37461.140000,1.149087
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f55db000000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 859154743.000000
Throughput: 85915474.300000
Num Accesses - Session Mean & Stddev: 8591547.430000,303.897754
Num Phases Traversed: 3751867.000000
Num Phases Traversed - Session Mean & Stddev: 37518.670000,1.327064
Num Path Iterations: 3751867.000000
Num Path Iterations - Session Mean & Stddev: 37518.670000,1.327064
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f46d3400000 of size 24576 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 853795914.000000
Throughput: 85379591.400000
Num Accesses - Session Mean & Stddev: 8537959.140000,165.831422
Num Phases Traversed: 3728466.000000
Num Phases Traversed - Session Mean & Stddev: 37284.660000,0.724155
Num Path Iterations: 3728466.000000
Num Path Iterations - Session Mean & Stddev: 37284.660000,0.724155
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7382200000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 855168540.000000
Throughput: 85516854.000000
Num Accesses - Session Mean & Stddev: 8551685.400000,269.014089
Num Phases Traversed: 3734460.000000
Num Phases Traversed - Session Mean & Stddev: 37344.600000,1.174734
Num Path Iterations: 3734460.000000
Num Path Iterations - Session Mean & Stddev: 37344.600000,1.174734
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8436800000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 854453602.000000
Throughput: 85445360.200000
Num Accesses - Session Mean & Stddev: 8544536.020000,262.662102
Num Phases Traversed: 3731338.000000
Num Phases Traversed - Session Mean & Stddev: 37313.380000,1.146996
Num Path Iterations: 3731338.000000
Num Path Iterations - Session Mean & Stddev: 37313.380000,1.146996
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f29bc600000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 856896574.000000
Throughput: 85689657.400000
Num Accesses - Session Mean & Stddev: 8568965.740000,226.281666
Num Phases Traversed: 3742006.000000
Num Phases Traversed - Session Mean & Stddev: 37420.060000,0.988130
Num Path Iterations: 3742006.000000
Num Path Iterations - Session Mean & Stddev: 37420.060000,0.988130
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f67b1600000 of size 24576 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 854687640.000000
Throughput: 85468764.000000
Num Accesses - Session Mean & Stddev: 8546876.400000,265.086665
Num Phases Traversed: 3732360.000000
Num Phases Traversed - Session Mean & Stddev: 37323.600000,1.157584
Num Path Iterations: 3732360.000000
Num Path Iterations - Session Mean & Stddev: 37323.600000,1.157584
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa8a5600000 of size 24576 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 856601164.000000
Throughput: 85660116.400000
Num Accesses - Session Mean & Stddev: 8566011.640000,278.062961
Num Phases Traversed: 3740716.000000
Num Phases Traversed - Session Mean & Stddev: 37407.160000,1.214249
Num Path Iterations: 3740716.000000
Num Path Iterations - Session Mean & Stddev: 37407.160000,1.214249
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8b8ca00000 of size 24576 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 852571680.000000
Throughput: 85257168.000000
Num Accesses - Session Mean & Stddev: 8525716.800000,242.350820
Num Phases Traversed: 3723120.000000
Num Phases Traversed - Session Mean & Stddev: 37231.200000,1.058301
Num Path Iterations: 3723120.000000
Num Path Iterations - Session Mean & Stddev: 37231.200000,1.058301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4b14a00000 of size 24576 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 850990664.000000
Throughput: 85099066.400000
Num Accesses - Session Mean & Stddev: 8509906.640000,221.361402
Num Phases Traversed: 3716216.000000
Num Phases Traversed - Session Mean & Stddev: 37162.160000,0.966644
Num Path Iterations: 3716216.000000
Num Path Iterations - Session Mean & Stddev: 37162.160000,0.966644
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f85b6400000 of size 24576 bytes.
[1.244s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 855850044.000000
Throughput: 85585004.400000
Num Accesses - Session Mean & Stddev: 8558500.440000,166.777416
Num Phases Traversed: 3737436.000000
Num Phases Traversed - Session Mean & Stddev: 37374.360000,0.728286
Num Path Iterations: 3737436.000000
Num Path Iterations - Session Mean & Stddev: 37374.360000,0.728286
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f86f7a00000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 857494264.000000
Throughput: 85749426.400000
Num Accesses - Session Mean & Stddev: 8574942.640000,223.717881
Num Phases Traversed: 3744616.000000
Num Phases Traversed - Session Mean & Stddev: 37446.160000,0.976934
Num Path Iterations: 3744616.000000
Num Path Iterations - Session Mean & Stddev: 37446.160000,0.976934
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2880200000 of size 24576 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 855377846.000000
Throughput: 85537784.600000
Num Accesses - Session Mean & Stddev: 8553778.460000,250.061289
Num Phases Traversed: 3735374.000000
Num Phases Traversed - Session Mean & Stddev: 37353.740000,1.091971
Num Path Iterations: 3735374.000000
Num Path Iterations - Session Mean & Stddev: 37353.740000,1.091971
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,1 0/192,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,1 0/192,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faf95c00000 of size 24576 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 856751159.000000
Throughput: 85675115.900000
Num Accesses - Session Mean & Stddev: 8567511.590000,279.145270
Num Phases Traversed: 3741371.000000
Num Phases Traversed - Session Mean & Stddev: 37413.710000,1.218975
Num Path Iterations: 3741371.000000
Num Path Iterations - Session Mean & Stddev: 37413.710000,1.218975
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd540000000 of size 12582912 bytes.
[2.130s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 714840829.000000
Throughput: 71484082.900000
Num Accesses - Session Mean & Stddev: 7148408.290000,45482.048029
Num Phases Traversed: 7369.000000
Num Phases Traversed - Session Mean & Stddev: 73.690000,0.462493
Num Path Iterations: 7369.000000
Num Path Iterations - Session Mean & Stddev: 73.690000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8340000000 of size 12582912 bytes.
[2.088s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 714152442.000000
Throughput: 71415244.200000
Num Accesses - Session Mean & Stddev: 7141524.420000,53467.125360
Num Phases Traversed: 7362.000000
Num Phases Traversed - Session Mean & Stddev: 73.620000,0.543691
Num Path Iterations: 7362.000000
Num Path Iterations - Session Mean & Stddev: 73.620000,0.543691
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9fc0000000 of size 12582912 bytes.
[2.116s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 712382304.000000
Throughput: 71238230.400000
Num Accesses - Session Mean & Stddev: 7123823.040000,48815.188641
Num Phases Traversed: 7344.000000
Num Phases Traversed - Session Mean & Stddev: 73.440000,0.496387
Num Path Iterations: 7344.000000
Num Path Iterations - Session Mean & Stddev: 73.440000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa1c0000000 of size 12582912 bytes.
[2.154s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 714545806.000000
Throughput: 71454580.600000
Num Accesses - Session Mean & Stddev: 7145458.060000,46584.994278
Num Phases Traversed: 7366.000000
Num Phases Traversed - Session Mean & Stddev: 73.660000,0.473709
Num Path Iterations: 7366.000000
Num Path Iterations - Session Mean & Stddev: 73.660000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe900000000 of size 12582912 bytes.
[2.121s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 719659538.000000
Throughput: 71965953.800000
Num Accesses - Session Mean & Stddev: 7196595.380000,37781.378438
Num Phases Traversed: 7418.000000
Num Phases Traversed - Session Mean & Stddev: 74.180000,0.384187
Num Path Iterations: 7418.000000
Num Path Iterations - Session Mean & Stddev: 74.180000,0.384187
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe1c0000000 of size 12582912 bytes.
[2.114s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 718381105.000000
Throughput: 71838110.500000
Num Accesses - Session Mean & Stddev: 7183811.050000,42582.902117
Num Phases Traversed: 7405.000000
Num Phases Traversed - Session Mean & Stddev: 74.050000,0.433013
Num Path Iterations: 7405.000000
Num Path Iterations - Session Mean & Stddev: 74.050000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb780000000 of size 12582912 bytes.
[2.121s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 719167833.000000
Throughput: 71916783.300000
Num Accesses - Session Mean & Stddev: 7191678.330000,33072.416044
Num Phases Traversed: 7413.000000
Num Phases Traversed - Session Mean & Stddev: 74.130000,0.336303
Num Path Iterations: 7413.000000
Num Path Iterations - Session Mean & Stddev: 74.130000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2480000000 of size 12582912 bytes.
[2.095s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 715824239.000000
Throughput: 71582423.900000
Num Accesses - Session Mean & Stddev: 7158242.390000,56140.567760
Num Phases Traversed: 7379.000000
Num Phases Traversed - Session Mean & Stddev: 73.790000,0.570877
Num Path Iterations: 7379.000000
Num Path Iterations - Session Mean & Stddev: 73.790000,0.570877
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd7c0000000 of size 12582912 bytes.
[2.118s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 715922580.000000
Throughput: 71592258.000000
Num Accesses - Session Mean & Stddev: 7159225.800000,65232.039702
Num Phases Traversed: 7380.000000
Num Phases Traversed - Session Mean & Stddev: 73.800000,0.663325
Num Path Iterations: 7380.000000
Num Path Iterations - Session Mean & Stddev: 73.800000,0.663325
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd340000000 of size 12582912 bytes.
[2.102s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 718381105.000000
Throughput: 71838110.500000
Num Accesses - Session Mean & Stddev: 7183811.050000,32243.253096
Num Phases Traversed: 7405.000000
Num Phases Traversed - Session Mean & Stddev: 74.050000,0.327872
Num Path Iterations: 7405.000000
Num Path Iterations - Session Mean & Stddev: 74.050000,0.327872
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f34c0000000 of size 12582912 bytes.
[2.083s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 719856220.000000
Throughput: 71985622.000000
Num Accesses - Session Mean & Stddev: 7198562.200000,39336.400000
Num Phases Traversed: 7420.000000
Num Phases Traversed - Session Mean & Stddev: 74.200000,0.400000
Num Path Iterations: 7420.000000
Num Path Iterations - Session Mean & Stddev: 74.200000,0.400000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa440000000 of size 12582912 bytes.
[2.178s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 719069492.000000
Throughput: 71906949.200000
Num Accesses - Session Mean & Stddev: 7190694.920000,31957.042430
Num Phases Traversed: 7412.000000
Num Phases Traversed - Session Mean & Stddev: 74.120000,0.324962
Num Path Iterations: 7412.000000
Num Path Iterations - Session Mean & Stddev: 74.120000,0.324962
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fed00000000 of size 12582912 bytes.
[2.149s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 755160639.000000
Throughput: 75516063.900000
Num Accesses - Session Mean & Stddev: 7551606.390000,64178.334167
Num Phases Traversed: 7779.000000
Num Phases Traversed - Session Mean & Stddev: 77.790000,0.652610
Num Path Iterations: 7779.000000
Num Path Iterations - Session Mean & Stddev: 77.790000,0.652610
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbf00000000 of size 12582912 bytes.
[2.127s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 721134653.000000
Throughput: 72113465.300000
Num Accesses - Session Mean & Stddev: 7211346.530000,46241.188883
Num Phases Traversed: 7433.000000
Num Phases Traversed - Session Mean & Stddev: 74.330000,0.470213
Num Path Iterations: 7433.000000
Num Path Iterations - Session Mean & Stddev: 74.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe000000000 of size 12582912 bytes.
[2.160s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 718184423.000000
Throughput: 71818442.300000
Num Accesses - Session Mean & Stddev: 7181844.230000,32482.316633
Num Phases Traversed: 7403.000000
Num Phases Traversed - Session Mean & Stddev: 74.030000,0.330303
Num Path Iterations: 7403.000000
Num Path Iterations - Session Mean & Stddev: 74.030000,0.330303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1a40000000 of size 12582912 bytes.
[2.174s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 720544607.000000
Throughput: 72054460.700000
Num Accesses - Session Mean & Stddev: 7205446.070000,43659.417021
Num Phases Traversed: 7427.000000
Num Phases Traversed - Session Mean & Stddev: 74.270000,0.443959
Num Path Iterations: 7427.000000
Num Path Iterations - Session Mean & Stddev: 74.270000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6f80000000 of size 12582912 bytes.
[2.156s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 723888201.000000
Throughput: 72388820.100000
Num Accesses - Session Mean & Stddev: 7238882.010000,47965.816449
Num Phases Traversed: 7461.000000
Num Phases Traversed - Session Mean & Stddev: 74.610000,0.487750
Num Path Iterations: 7461.000000
Num Path Iterations - Session Mean & Stddev: 74.610000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2c40000000 of size 12582912 bytes.
[2.149s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 721036312.000000
Throughput: 72103631.200000
Num Accesses - Session Mean & Stddev: 7210363.120000,58807.589099
Num Phases Traversed: 7432.000000
Num Phases Traversed - Session Mean & Stddev: 74.320000,0.597997
Num Path Iterations: 7432.000000
Num Path Iterations - Session Mean & Stddev: 74.320000,0.597997
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd340000000 of size 12582912 bytes.
[2.141s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 725363316.000000
Throughput: 72536331.600000
Num Accesses - Session Mean & Stddev: 7253633.160000,41999.782095
Num Phases Traversed: 7476.000000
Num Phases Traversed - Session Mean & Stddev: 74.760000,0.427083
Num Path Iterations: 7476.000000
Num Path Iterations - Session Mean & Stddev: 74.760000,0.427083
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.001 0/98304,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.001 0/98304,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3e80000000 of size 12582912 bytes.
[2.151s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 721921381.000000
Throughput: 72192138.100000
Num Accesses - Session Mean & Stddev: 7219213.810000,55794.977724
Num Phases Traversed: 7441.000000
Num Phases Traversed - Session Mean & Stddev: 74.410000,0.567362
Num Path Iterations: 7441.000000
Num Path Iterations - Session Mean & Stddev: 74.410000,0.567362
+ set +x

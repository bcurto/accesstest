++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2ae9000000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1941971581.000000
Throughput: 194197158.100000
Num Accesses - Session Mean & Stddev: 19419715.810000,1886.219259
Num Phases Traversed: 680777.000000
Num Phases Traversed - Session Mean & Stddev: 6807.770000,0.661135
Num Path Iterations: 680777.000000
Num Path Iterations - Session Mean & Stddev: 6807.770000,0.661135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe7c8200000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1942171291.000000
Throughput: 194217129.100000
Num Accesses - Session Mean & Stddev: 19421712.910000,1479.989453
Num Phases Traversed: 680847.000000
Num Phases Traversed - Session Mean & Stddev: 6808.470000,0.518748
Num Path Iterations: 680847.000000
Num Path Iterations - Session Mean & Stddev: 6808.470000,0.518748
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5dba600000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1941982993.000000
Throughput: 194198299.300000
Num Accesses - Session Mean & Stddev: 19419829.930000,1922.126043
Num Phases Traversed: 680781.000000
Num Phases Traversed - Session Mean & Stddev: 6807.810000,0.673721
Num Path Iterations: 680781.000000
Num Path Iterations - Session Mean & Stddev: 6807.810000,0.673721
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe9c2a00000 of size 360448 bytes.
[1.275s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1944037153.000000
Throughput: 194403715.300000
Num Accesses - Session Mean & Stddev: 19440371.530000,1996.896204
Num Phases Traversed: 681501.000000
Num Phases Traversed - Session Mean & Stddev: 6815.010000,0.699929
Num Path Iterations: 681501.000000
Num Path Iterations - Session Mean & Stddev: 6815.010000,0.699929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f41b0c00000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1943141311.000000
Throughput: 194314131.100000
Num Accesses - Session Mean & Stddev: 19431413.110000,1962.358025
Num Phases Traversed: 681187.000000
Num Phases Traversed - Session Mean & Stddev: 6811.870000,0.687823
Num Path Iterations: 681187.000000
Num Path Iterations - Session Mean & Stddev: 6811.870000,0.687823
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f24d0c00000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1942085701.000000
Throughput: 194208570.100000
Num Accesses - Session Mean & Stddev: 19420857.010000,1937.310948
Num Phases Traversed: 680817.000000
Num Phases Traversed - Session Mean & Stddev: 6808.170000,0.679043
Num Path Iterations: 680817.000000
Num Path Iterations - Session Mean & Stddev: 6808.170000,0.679043
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f872b000000 of size 360448 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1942216939.000000
Throughput: 194221693.900000
Num Accesses - Session Mean & Stddev: 19422169.390000,1695.315881
Num Phases Traversed: 680863.000000
Num Phases Traversed - Session Mean & Stddev: 6808.630000,0.594222
Num Path Iterations: 680863.000000
Num Path Iterations - Session Mean & Stddev: 6808.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0490c00000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1944904465.000000
Throughput: 194490446.500000
Num Accesses - Session Mean & Stddev: 19449044.650000,1991.998842
Num Phases Traversed: 681805.000000
Num Phases Traversed - Session Mean & Stddev: 6818.050000,0.698212
Num Path Iterations: 681805.000000
Num Path Iterations - Session Mean & Stddev: 6818.050000,0.698212
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff4af200000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1944510751.000000
Throughput: 194451075.100000
Num Accesses - Session Mean & Stddev: 19445107.510000,1761.250973
Num Phases Traversed: 681667.000000
Num Phases Traversed - Session Mean & Stddev: 6816.670000,0.617333
Num Path Iterations: 681667.000000
Num Path Iterations - Session Mean & Stddev: 6816.670000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f833fe00000 of size 360448 bytes.
[1.275s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1944316747.000000
Throughput: 194431674.700000
Num Accesses - Session Mean & Stddev: 19443167.470000,1996.896204
Num Phases Traversed: 681599.000000
Num Phases Traversed - Session Mean & Stddev: 6815.990000,0.699929
Num Path Iterations: 681599.000000
Num Path Iterations - Session Mean & Stddev: 6815.990000,0.699929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdb3e600000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1945589185.000000
Throughput: 194558918.500000
Num Accesses - Session Mean & Stddev: 19455891.850000,1529.750825
Num Phases Traversed: 682045.000000
Num Phases Traversed - Session Mean & Stddev: 6820.450000,0.536190
Num Path Iterations: 682045.000000
Num Path Iterations - Session Mean & Stddev: 6820.450000,0.536190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f19d8600000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1943786089.000000
Throughput: 194378608.900000
Num Accesses - Session Mean & Stddev: 19437860.890000,1962.358025
Num Phases Traversed: 681413.000000
Num Phases Traversed - Session Mean & Stddev: 6814.130000,0.687823
Num Path Iterations: 681413.000000
Num Path Iterations - Session Mean & Stddev: 6814.130000,0.687823
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f39cbc00000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1939386763.000000
Throughput: 193938676.300000
Num Accesses - Session Mean & Stddev: 19393867.630000,1817.654338
Num Phases Traversed: 679871.000000
Num Phases Traversed - Session Mean & Stddev: 6798.710000,0.637103
Num Path Iterations: 679871.000000
Num Path Iterations - Session Mean & Stddev: 6798.710000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7149800000 of size 360448 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1943152723.000000
Throughput: 194315272.300000
Num Accesses - Session Mean & Stddev: 19431527.230000,1980.524571
Num Phases Traversed: 681191.000000
Num Phases Traversed - Session Mean & Stddev: 6811.910000,0.694190
Num Path Iterations: 681191.000000
Num Path Iterations - Session Mean & Stddev: 6811.910000,0.694190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4bbac00000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1942690537.000000
Throughput: 194269053.700000
Num Accesses - Session Mean & Stddev: 19426905.370000,1817.654338
Num Phases Traversed: 681029.000000
Num Phases Traversed - Session Mean & Stddev: 6810.290000,0.637103
Num Path Iterations: 681029.000000
Num Path Iterations - Session Mean & Stddev: 6810.290000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f353a000000 of size 360448 bytes.
[1.275s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1944499339.000000
Throughput: 194449933.900000
Num Accesses - Session Mean & Stddev: 19444993.390000,1695.315881
Num Phases Traversed: 681663.000000
Num Phases Traversed - Session Mean & Stddev: 6816.630000,0.594222
Num Path Iterations: 681663.000000
Num Path Iterations - Session Mean & Stddev: 6816.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f944dc00000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1941127093.000000
Throughput: 194112709.300000
Num Accesses - Session Mean & Stddev: 19411270.930000,1922.126043
Num Phases Traversed: 680481.000000
Num Phases Traversed - Session Mean & Stddev: 6804.810000,0.673721
Num Path Iterations: 680481.000000
Num Path Iterations - Session Mean & Stddev: 6804.810000,0.673721
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4c1a400000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1945172647.000000
Throughput: 194517264.700000
Num Accesses - Session Mean & Stddev: 19451726.470000,1996.896204
Num Phases Traversed: 681899.000000
Num Phases Traversed - Session Mean & Stddev: 6818.990000,0.699929
Num Path Iterations: 681899.000000
Num Path Iterations - Session Mean & Stddev: 6818.990000,0.699929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6306200000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1942034347.000000
Throughput: 194203434.700000
Num Accesses - Session Mean & Stddev: 19420343.470000,1996.896204
Num Phases Traversed: 680799.000000
Num Phases Traversed - Session Mean & Stddev: 6807.990000,0.699929
Num Path Iterations: 680799.000000
Num Path Iterations - Session Mean & Stddev: 6807.990000,0.699929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,1 0/2816,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,1 0/2816,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd08be00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1942673419.000000
Throughput: 194267341.900000
Num Accesses - Session Mean & Stddev: 19426734.190000,1886.219259
Num Phases Traversed: 681023.000000
Num Phases Traversed - Session Mean & Stddev: 6810.230000,0.661135
Num Path Iterations: 681023.000000
Num Path Iterations - Session Mean & Stddev: 6810.230000,0.661135
+ set +x

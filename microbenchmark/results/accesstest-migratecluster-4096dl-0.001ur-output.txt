++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1867e00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118178705.000000
Throughput: 211817870.500000
Num Accesses - Session Mean & Stddev: 21181787.050000,1089.599535
Num Phases Traversed: 1016013.000000
Num Phases Traversed - Session Mean & Stddev: 10160.130000,0.522590
Num Path Iterations: 1016013.000000
Num Path Iterations - Session Mean & Stddev: 10160.130000,0.522590
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0c96600000 of size 262144 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118237085.000000
Throughput: 211823708.500000
Num Accesses - Session Mean & Stddev: 21182370.850000,1025.472441
Num Phases Traversed: 1016041.000000
Num Phases Traversed - Session Mean & Stddev: 10160.410000,0.491833
Num Path Iterations: 1016041.000000
Num Path Iterations - Session Mean & Stddev: 10160.410000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f24c2a00000 of size 262144 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118257935.000000
Throughput: 211825793.500000
Num Accesses - Session Mean & Stddev: 21182579.350000,1042.291479
Num Phases Traversed: 1016051.000000
Num Phases Traversed - Session Mean & Stddev: 10160.510000,0.499900
Num Path Iterations: 1016051.000000
Num Path Iterations - Session Mean & Stddev: 10160.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff41fa00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118312145.000000
Throughput: 211831214.500000
Num Accesses - Session Mean & Stddev: 21183121.450000,1015.247284
Num Phases Traversed: 1016077.000000
Num Phases Traversed - Session Mean & Stddev: 10160.770000,0.486929
Num Path Iterations: 1016077.000000
Num Path Iterations - Session Mean & Stddev: 10160.770000,0.486929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f93e5400000 of size 262144 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118374695.000000
Throughput: 211837469.500000
Num Accesses - Session Mean & Stddev: 21183746.950000,1151.667638
Num Phases Traversed: 1016107.000000
Num Phases Traversed - Session Mean & Stddev: 10161.070000,0.552359
Num Path Iterations: 1016107.000000
Num Path Iterations - Session Mean & Stddev: 10161.070000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f65ea600000 of size 262144 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118458095.000000
Throughput: 211845809.500000
Num Accesses - Session Mean & Stddev: 21184580.950000,1040.621808
Num Phases Traversed: 1016147.000000
Num Phases Traversed - Session Mean & Stddev: 10161.470000,0.499099
Num Path Iterations: 1016147.000000
Num Path Iterations - Session Mean & Stddev: 10161.470000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcff5c00000 of size 262144 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118147430.000000
Throughput: 211814743.000000
Num Accesses - Session Mean & Stddev: 21181474.300000,1178.716722
Num Phases Traversed: 1015998.000000
Num Phases Traversed - Session Mean & Stddev: 10159.980000,0.565332
Num Path Iterations: 1015998.000000
Num Path Iterations - Session Mean & Stddev: 10159.980000,0.565332
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6de0c00000 of size 262144 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118378865.000000
Throughput: 211837886.500000
Num Accesses - Session Mean & Stddev: 21183788.650000,1182.950433
Num Phases Traversed: 1016109.000000
Num Phases Traversed - Session Mean & Stddev: 10161.090000,0.567362
Num Path Iterations: 1016109.000000
Num Path Iterations - Session Mean & Stddev: 10161.090000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f34b7a00000 of size 262144 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2117840935.000000
Throughput: 211784093.500000
Num Accesses - Session Mean & Stddev: 21178409.350000,1042.291479
Num Phases Traversed: 1015851.000000
Num Phases Traversed - Session Mean & Stddev: 10158.510000,0.499900
Num Path Iterations: 1015851.000000
Num Path Iterations - Session Mean & Stddev: 10158.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffa6de00000 of size 262144 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118624895.000000
Throughput: 211862489.500000
Num Accesses - Session Mean & Stddev: 21186248.950000,1097.550021
Num Phases Traversed: 1016227.000000
Num Phases Traversed - Session Mean & Stddev: 10162.270000,0.526403
Num Path Iterations: 1016227.000000
Num Path Iterations - Session Mean & Stddev: 10162.270000,0.526403
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8af4400000 of size 262144 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118260020.000000
Throughput: 211826002.000000
Num Accesses - Session Mean & Stddev: 21182600.200000,1041.665666
Num Phases Traversed: 1016052.000000
Num Phases Traversed - Session Mean & Stddev: 10160.520000,0.499600
Num Path Iterations: 1016052.000000
Num Path Iterations - Session Mean & Stddev: 10160.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0fec800000 of size 262144 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118422650.000000
Throughput: 211842265.000000
Num Accesses - Session Mean & Stddev: 21184226.500000,1042.500000
Num Phases Traversed: 1016130.000000
Num Phases Traversed - Session Mean & Stddev: 10161.300000,0.500000
Num Path Iterations: 1016130.000000
Num Path Iterations - Session Mean & Stddev: 10161.300000,0.500000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe911a00000 of size 262144 bytes.
[1.267s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118176620.000000
Throughput: 211817662.000000
Num Accesses - Session Mean & Stddev: 21181766.200000,1189.729574
Num Phases Traversed: 1016012.000000
Num Phases Traversed - Session Mean & Stddev: 10160.120000,0.570614
Num Path Iterations: 1016012.000000
Num Path Iterations - Session Mean & Stddev: 10160.120000,0.570614
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb724200000 of size 262144 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118101560.000000
Throughput: 211810156.000000
Num Accesses - Session Mean & Stddev: 21181015.600000,1068.041123
Num Phases Traversed: 1015976.000000
Num Phases Traversed - Session Mean & Stddev: 10159.760000,0.512250
Num Path Iterations: 1015976.000000
Num Path Iterations - Session Mean & Stddev: 10159.760000,0.512250
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdb5d000000 of size 262144 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118335080.000000
Throughput: 211833508.000000
Num Accesses - Session Mean & Stddev: 21183350.800000,1152.610932
Num Phases Traversed: 1016088.000000
Num Phases Traversed - Session Mean & Stddev: 10160.880000,0.552811
Num Path Iterations: 1016088.000000
Num Path Iterations - Session Mean & Stddev: 10160.880000,0.552811
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9171000000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118393460.000000
Throughput: 211839346.000000
Num Accesses - Session Mean & Stddev: 21183934.600000,1092.189791
Num Phases Traversed: 1016116.000000
Num Phases Traversed - Session Mean & Stddev: 10161.160000,0.523832
Num Path Iterations: 1016116.000000
Num Path Iterations - Session Mean & Stddev: 10161.160000,0.523832
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1e7d000000 of size 262144 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118216235.000000
Throughput: 211821623.500000
Num Accesses - Session Mean & Stddev: 21182162.350000,964.298412
Num Phases Traversed: 1016031.000000
Num Phases Traversed - Session Mean & Stddev: 10160.310000,0.462493
Num Path Iterations: 1016031.000000
Num Path Iterations - Session Mean & Stddev: 10160.310000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6b4c400000 of size 262144 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2117999395.000000
Throughput: 211799939.500000
Num Accesses - Session Mean & Stddev: 21179993.950000,1015.247284
Num Phases Traversed: 1015927.000000
Num Phases Traversed - Session Mean & Stddev: 10159.270000,0.486929
Num Path Iterations: 1015927.000000
Num Path Iterations - Session Mean & Stddev: 10159.270000,0.486929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa6b7e00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2118191215.000000
Throughput: 211819121.500000
Num Accesses - Session Mean & Stddev: 21181912.150000,1168.158349
Num Phases Traversed: 1016019.000000
Num Phases Traversed - Session Mean & Stddev: 10160.190000,0.560268
Num Path Iterations: 1016019.000000
Num Path Iterations - Session Mean & Stddev: 10160.190000,0.560268
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.001 0/2048,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.001 0/2048,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8512400000 of size 262144 bytes.
[1.237s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2117394745.000000
Throughput: 211739474.500000
Num Accesses - Session Mean & Stddev: 21173947.450000,1048.943586
Num Phases Traversed: 1015637.000000
Num Phases Traversed - Session Mean & Stddev: 10156.370000,0.503090
Num Path Iterations: 1015637.000000
Num Path Iterations - Session Mean & Stddev: 10156.370000,0.503090
+ set +x

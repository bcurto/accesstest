++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f825ac00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2800423903.000000
Throughput: 280042390.300000
Num Accesses - Session Mean & Stddev: 28004239.030000,226.454695
Num Phases Traversed: 6182051.000000
Num Phases Traversed - Session Mean & Stddev: 61820.510000,0.499900
Num Path Iterations: 6182051.000000
Num Path Iterations - Session Mean & Stddev: 61820.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd626e00000 of size 53248 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2805043144.000000
Throughput: 280504314.400000
Num Accesses - Session Mean & Stddev: 28050431.440000,226.318727
Num Phases Traversed: 6192248.000000
Num Phases Traversed - Session Mean & Stddev: 61922.480000,0.499600
Num Path Iterations: 6192248.000000
Num Path Iterations - Session Mean & Stddev: 61922.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff303800000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2798292538.000000
Throughput: 279829253.800000
Num Accesses - Session Mean & Stddev: 27982925.380000,225.774037
Num Phases Traversed: 6177346.000000
Num Phases Traversed - Session Mean & Stddev: 61773.460000,0.498397
Num Path Iterations: 6177346.000000
Num Path Iterations - Session Mean & Stddev: 61773.460000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f98a0c00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2809721728.000000
Throughput: 280972172.800000
Num Accesses - Session Mean & Stddev: 28097217.280000,193.468658
Num Phases Traversed: 6202576.000000
Num Phases Traversed - Session Mean & Stddev: 62025.760000,0.427083
Num Path Iterations: 6202576.000000
Num Path Iterations - Session Mean & Stddev: 62025.760000,0.427083
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8964600000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2795798320.000000
Throughput: 279579832.000000
Num Accesses - Session Mean & Stddev: 27957983.200000,221.923771
Num Phases Traversed: 6171840.000000
Num Phases Traversed - Session Mean & Stddev: 61718.400000,0.489898
Num Path Iterations: 6171840.000000
Num Path Iterations - Session Mean & Stddev: 61718.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efc5d000000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2817574030.000000
Throughput: 281757403.000000
Num Accesses - Session Mean & Stddev: 28175740.300000,135.900000
Num Phases Traversed: 6219910.000000
Num Phases Traversed - Session Mean & Stddev: 62199.100000,0.300000
Num Path Iterations: 6219910.000000
Num Path Iterations - Session Mean & Stddev: 62199.100000,0.300000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe542200000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2808234076.000000
Throughput: 280823407.600000
Num Accesses - Session Mean & Stddev: 28082340.760000,152.681834
Num Phases Traversed: 6199292.000000
Num Phases Traversed - Session Mean & Stddev: 61992.920000,0.337046
Num Path Iterations: 6199292.000000
Num Path Iterations - Session Mean & Stddev: 61992.920000,0.337046
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff9c7800000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2804979724.000000
Throughput: 280497972.400000
Num Accesses - Session Mean & Stddev: 28049797.240000,122.895819
Num Phases Traversed: 6192108.000000
Num Phases Traversed - Session Mean & Stddev: 61921.080000,0.271293
Num Path Iterations: 6192108.000000
Num Path Iterations - Session Mean & Stddev: 61921.080000,0.271293
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcaf5000000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2790327892.000000
Throughput: 279032789.200000
Num Accesses - Session Mean & Stddev: 27903278.920000,217.440000
Num Phases Traversed: 6159764.000000
Num Phases Traversed - Session Mean & Stddev: 61597.640000,0.480000
Num Path Iterations: 6159764.000000
Num Path Iterations - Session Mean & Stddev: 61597.640000,0.480000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f94b2200000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2795289601.000000
Throughput: 279528960.100000
Num Accesses - Session Mean & Stddev: 27952896.010000,170.161658
Num Phases Traversed: 6170717.000000
Num Phases Traversed - Session Mean & Stddev: 61707.170000,0.375633
Num Path Iterations: 6170717.000000
Num Path Iterations - Session Mean & Stddev: 61707.170000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f410dc00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2806528078.000000
Throughput: 280652807.800000
Num Accesses - Session Mean & Stddev: 28065280.780000,198.701313
Num Phases Traversed: 6195526.000000
Num Phases Traversed - Session Mean & Stddev: 61955.260000,0.438634
Num Path Iterations: 6195526.000000
Num Path Iterations - Session Mean & Stddev: 61955.260000,0.438634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f346f600000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2802448813.000000
Throughput: 280244881.300000
Num Accesses - Session Mean & Stddev: 28024488.130000,184.510631
Num Phases Traversed: 6186521.000000
Num Phases Traversed - Session Mean & Stddev: 61865.210000,0.407308
Num Path Iterations: 6186521.000000
Num Path Iterations - Session Mean & Stddev: 61865.210000,0.407308
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6d1ec00000 of size 53248 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2807489797.000000
Throughput: 280748979.700000
Num Accesses - Session Mean & Stddev: 28074897.970000,226.454695
Num Phases Traversed: 6197649.000000
Num Phases Traversed - Session Mean & Stddev: 61976.490000,0.499900
Num Path Iterations: 6197649.000000
Num Path Iterations - Session Mean & Stddev: 61976.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f98cdc00000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2803607587.000000
Throughput: 280360758.700000
Num Accesses - Session Mean & Stddev: 28036075.870000,184.510631
Num Phases Traversed: 6189079.000000
Num Phases Traversed - Session Mean & Stddev: 61890.790000,0.407308
Num Path Iterations: 6189079.000000
Num Path Iterations - Session Mean & Stddev: 61890.790000,0.407308
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7fcae00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2805851296.000000
Throughput: 280585129.600000
Num Accesses - Session Mean & Stddev: 28058512.960000,211.313697
Num Phases Traversed: 6194032.000000
Num Phases Traversed - Session Mean & Stddev: 61940.320000,0.466476
Num Path Iterations: 6194032.000000
Num Path Iterations - Session Mean & Stddev: 61940.320000,0.466476
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4b20a00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2801069881.000000
Throughput: 280106988.100000
Num Accesses - Session Mean & Stddev: 28010698.810000,190.637126
Num Phases Traversed: 6183477.000000
Num Phases Traversed - Session Mean & Stddev: 61834.770000,0.420833
Num Path Iterations: 6183477.000000
Num Path Iterations - Session Mean & Stddev: 61834.770000,0.420833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd550400000 of size 53248 bytes.
[1.267s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2807998063.000000
Throughput: 280799806.300000
Num Accesses - Session Mean & Stddev: 28079980.630000,205.554210
Num Phases Traversed: 6198771.000000
Num Phases Traversed - Session Mean & Stddev: 61987.710000,0.453762
Num Path Iterations: 6198771.000000
Num Path Iterations - Session Mean & Stddev: 61987.710000,0.453762
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1427800000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2802887317.000000
Throughput: 280288731.700000
Num Accesses - Session Mean & Stddev: 28028873.170000,141.739060
Num Phases Traversed: 6187489.000000
Num Phases Traversed - Session Mean & Stddev: 61874.890000,0.312890
Num Path Iterations: 6187489.000000
Num Path Iterations - Session Mean & Stddev: 61874.890000,0.312890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8c3ec00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2795623915.000000
Throughput: 279562391.500000
Num Accesses - Session Mean & Stddev: 27956239.150000,225.364655
Num Phases Traversed: 6171455.000000
Num Phases Traversed - Session Mean & Stddev: 61714.550000,0.497494
Num Path Iterations: 6171455.000000
Num Path Iterations - Session Mean & Stddev: 61714.550000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fced4c00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2798089141.000000
Throughput: 279808914.100000
Num Accesses - Session Mean & Stddev: 27980891.410000,100.378095
Num Phases Traversed: 6176897.000000
Num Phases Traversed - Session Mean & Stddev: 61768.970000,0.221585
Num Path Iterations: 6176897.000000
Num Path Iterations - Session Mean & Stddev: 61768.970000,0.221585
+ set +x

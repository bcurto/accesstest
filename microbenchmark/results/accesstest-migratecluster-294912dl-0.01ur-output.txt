++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efc00000000 of size 18874368 bytes.
[3.708s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 309292921.000000
Throughput: 30929292.100000
Num Accesses - Session Mean & Stddev: 3092929.210000,103150.239493
Num Phases Traversed: 2197.000000
Num Phases Traversed - Session Mean & Stddev: 21.970000,0.699357
Num Path Iterations: 2197.000000
Num Path Iterations - Session Mean & Stddev: 21.970000,0.699357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5b40000000 of size 18874368 bytes.
[3.711s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 287168971.000000
Throughput: 28716897.100000
Num Accesses - Session Mean & Stddev: 2871689.710000,76511.771622
Num Phases Traversed: 2047.000000
Num Phases Traversed - Session Mean & Stddev: 20.470000,0.518748
Num Path Iterations: 2047.000000
Num Path Iterations - Session Mean & Stddev: 20.470000,0.518748
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1900000000 of size 18874368 bytes.
[3.714s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 273894601.000000
Throughput: 27389460.100000
Num Accesses - Session Mean & Stddev: 2738946.010000,81469.023920
Num Phases Traversed: 1957.000000
Num Phases Traversed - Session Mean & Stddev: 19.570000,0.552359
Num Path Iterations: 1957.000000
Num Path Iterations - Session Mean & Stddev: 19.570000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe500000000 of size 18874368 bytes.
[3.707s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 274484573.000000
Throughput: 27448457.300000
Num Accesses - Session Mean & Stddev: 2744845.730000,85736.451571
Num Phases Traversed: 1961.000000
Num Phases Traversed - Session Mean & Stddev: 19.610000,0.581292
Num Path Iterations: 1961.000000
Num Path Iterations - Session Mean & Stddev: 19.610000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc480000000 of size 18874368 bytes.
[3.720s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 275959503.000000
Throughput: 27595950.300000
Num Accesses - Session Mean & Stddev: 2759595.030000,93968.205854
Num Phases Traversed: 1971.000000
Num Phases Traversed - Session Mean & Stddev: 19.710000,0.637103
Num Path Iterations: 1971.000000
Num Path Iterations - Session Mean & Stddev: 19.710000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2a80000000 of size 18874368 bytes.
[3.721s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 284809083.000000
Throughput: 28480908.300000
Num Accesses - Session Mean & Stddev: 2848090.830000,92568.750077
Num Phases Traversed: 2031.000000
Num Phases Traversed - Session Mean & Stddev: 20.310000,0.627615
Num Path Iterations: 2031.000000
Num Path Iterations - Session Mean & Stddev: 20.310000,0.627615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa840000000 of size 18874368 bytes.
[3.720s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 275369531.000000
Throughput: 27536953.100000
Num Accesses - Session Mean & Stddev: 2753695.310000,91052.292240
Num Phases Traversed: 1967.000000
Num Phases Traversed - Session Mean & Stddev: 19.670000,0.617333
Num Path Iterations: 1967.000000
Num Path Iterations - Session Mean & Stddev: 19.670000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8c80000000 of size 18874368 bytes.
[3.725s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 274189587.000000
Throughput: 27418958.700000
Num Accesses - Session Mean & Stddev: 2741895.870000,83681.970384
Num Phases Traversed: 1959.000000
Num Phases Traversed - Session Mean & Stddev: 19.590000,0.567362
Num Path Iterations: 1959.000000
Num Path Iterations - Session Mean & Stddev: 19.590000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f19c0000000 of size 18874368 bytes.
[3.712s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 285989027.000000
Throughput: 28598902.700000
Num Accesses - Session Mean & Stddev: 2859890.270000,85736.451571
Num Phases Traversed: 2039.000000
Num Phases Traversed - Session Mean & Stddev: 20.390000,0.581292
Num Path Iterations: 2039.000000
Num Path Iterations - Session Mean & Stddev: 20.390000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0c40000000 of size 18874368 bytes.
[3.704s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 286873985.000000
Throughput: 28687398.500000
Num Accesses - Session Mean & Stddev: 2868739.850000,79084.310717
Num Phases Traversed: 2045.000000
Num Phases Traversed - Session Mean & Stddev: 20.450000,0.536190
Num Path Iterations: 2045.000000
Num Path Iterations - Session Mean & Stddev: 20.450000,0.536190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2300000000 of size 18874368 bytes.
[3.705s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 286578999.000000
Throughput: 28657899.900000
Num Accesses - Session Mean & Stddev: 2865789.990000,81469.023920
Num Phases Traversed: 2043.000000
Num Phases Traversed - Session Mean & Stddev: 20.430000,0.552359
Num Path Iterations: 2043.000000
Num Path Iterations - Session Mean & Stddev: 20.430000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd880000000 of size 18874368 bytes.
[3.712s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 273599615.000000
Throughput: 27359961.500000
Num Accesses - Session Mean & Stddev: 2735996.150000,79084.310717
Num Phases Traversed: 1955.000000
Num Phases Traversed - Session Mean & Stddev: 19.550000,0.536190
Num Path Iterations: 1955.000000
Num Path Iterations - Session Mean & Stddev: 19.550000,0.536190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe8c0000000 of size 18874368 bytes.
[3.712s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 285989027.000000
Throughput: 28598902.700000
Num Accesses - Session Mean & Stddev: 2859890.270000,85736.451571
Num Phases Traversed: 2039.000000
Num Phases Traversed - Session Mean & Stddev: 20.390000,0.581292
Num Path Iterations: 2039.000000
Num Path Iterations - Session Mean & Stddev: 20.390000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7840000000 of size 18874368 bytes.
[3.711s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 276254489.000000
Throughput: 27625448.900000
Num Accesses - Session Mean & Stddev: 2762544.890000,95255.816536
Num Phases Traversed: 1973.000000
Num Phases Traversed - Session Mean & Stddev: 19.730000,0.645833
Num Path Iterations: 1973.000000
Num Path Iterations - Session Mean & Stddev: 19.730000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8f00000000 of size 18874368 bytes.
[3.707s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 274484573.000000
Throughput: 27448457.300000
Num Accesses - Session Mean & Stddev: 2744845.730000,85736.451571
Num Phases Traversed: 1961.000000
Num Phases Traversed - Session Mean & Stddev: 19.610000,0.581292
Num Path Iterations: 1961.000000
Num Path Iterations - Session Mean & Stddev: 19.610000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc800000000 of size 18874368 bytes.
[3.710s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 273894601.000000
Throughput: 27389460.100000
Num Accesses - Session Mean & Stddev: 2738946.010000,81469.023920
Num Phases Traversed: 1957.000000
Num Phases Traversed - Session Mean & Stddev: 19.570000,0.552359
Num Path Iterations: 1957.000000
Num Path Iterations - Session Mean & Stddev: 19.570000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7d40000000 of size 18874368 bytes.
[3.715s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 285399055.000000
Throughput: 28539905.500000
Num Accesses - Session Mean & Stddev: 2853990.550000,89412.879416
Num Phases Traversed: 2035.000000
Num Phases Traversed - Session Mean & Stddev: 20.350000,0.606218
Num Path Iterations: 2035.000000
Num Path Iterations - Session Mean & Stddev: 20.350000,0.606218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f03c0000000 of size 18874368 bytes.
[3.718s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 284809083.000000
Throughput: 28480908.300000
Num Accesses - Session Mean & Stddev: 2848090.830000,92568.750077
Num Phases Traversed: 2031.000000
Num Phases Traversed - Session Mean & Stddev: 20.310000,0.627615
Num Path Iterations: 2031.000000
Num Path Iterations - Session Mean & Stddev: 20.310000,0.627615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2b00000000 of size 18874368 bytes.
[3.708s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 285989027.000000
Throughput: 28598902.700000
Num Accesses - Session Mean & Stddev: 2859890.270000,85736.451571
Num Phases Traversed: 2039.000000
Num Phases Traversed - Session Mean & Stddev: 20.390000,0.581292
Num Path Iterations: 2039.000000
Num Path Iterations - Session Mean & Stddev: 20.390000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/147456,0.01 0/147456,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/147456,0.01 0/147456,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    147456 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0d80000000 of size 18874368 bytes.
[3.702s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 271239727.000000
Throughput: 27123972.700000
Num Accesses - Session Mean & Stddev: 2712397.270000,85736.451571
Num Phases Traversed: 1939.000000
Num Phases Traversed - Session Mean & Stddev: 19.390000,0.581292
Num Path Iterations: 1939.000000
Num Path Iterations - Session Mean & Stddev: 19.390000,0.581292
+ set +x

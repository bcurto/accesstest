++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe112e00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1879576669.000000
Throughput: 187957666.900000
Num Accesses - Session Mean & Stddev: 18795766.690000,48.846841
Num Phases Traversed: 18609769.000000
Num Phases Traversed - Session Mean & Stddev: 186097.690000,0.483632
Num Path Iterations: 18609769.000000
Num Path Iterations - Session Mean & Stddev: 186097.690000,0.483632
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6331200000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1852356563.000000
Throughput: 185235656.300000
Num Accesses - Session Mean & Stddev: 18523565.630000,60.016440
Num Phases Traversed: 18340263.000000
Num Phases Traversed - Session Mean & Stddev: 183402.630000,0.594222
Num Path Iterations: 18340263.000000
Num Path Iterations - Session Mean & Stddev: 183402.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa979600000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1855852274.000000
Throughput: 185585227.400000
Num Accesses - Session Mean & Stddev: 18558522.740000,64.860561
Num Phases Traversed: 18374874.000000
Num Phases Traversed - Session Mean & Stddev: 183748.740000,0.642184
Num Path Iterations: 18374874.000000
Num Path Iterations - Session Mean & Stddev: 183748.740000,0.642184
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2af8800000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1873061058.000000
Throughput: 187306105.800000
Num Accesses - Session Mean & Stddev: 18730610.580000,49.849409
Num Phases Traversed: 18545258.000000
Num Phases Traversed - Session Mean & Stddev: 185452.580000,0.493559
Num Path Iterations: 18545258.000000
Num Path Iterations - Session Mean & Stddev: 185452.580000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fedb3600000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1873351736.000000
Throughput: 187335173.600000
Num Accesses - Session Mean & Stddev: 18733517.360000,52.520000
Num Phases Traversed: 18548136.000000
Num Phases Traversed - Session Mean & Stddev: 185481.360000,0.520000
Num Path Iterations: 18548136.000000
Num Path Iterations - Session Mean & Stddev: 185481.360000,0.520000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb4cce00000 of size 8192 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1857286676.000000
Throughput: 185728667.600000
Num Accesses - Session Mean & Stddev: 18572866.760000,62.456884
Num Phases Traversed: 18389076.000000
Num Phases Traversed - Session Mean & Stddev: 183890.760000,0.618385
Num Path Iterations: 18389076.000000
Num Path Iterations - Session Mean & Stddev: 183890.760000,0.618385
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3a9b000000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1865748658.000000
Throughput: 186574865.800000
Num Accesses - Session Mean & Stddev: 18657486.580000,49.849409
Num Phases Traversed: 18472858.000000
Num Phases Traversed - Session Mean & Stddev: 184728.580000,0.493559
Num Path Iterations: 18472858.000000
Num Path Iterations - Session Mean & Stddev: 184728.580000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa589000000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1884741506.000000
Throughput: 188474150.600000
Num Accesses - Session Mean & Stddev: 18847415.060000,58.580000
Num Phases Traversed: 18660906.000000
Num Phases Traversed - Session Mean & Stddev: 186609.060000,0.580000
Num Path Iterations: 18660906.000000
Num Path Iterations - Session Mean & Stddev: 186609.060000,0.580000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbee6600000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1866449901.000000
Throughput: 186644990.100000
Num Accesses - Session Mean & Stddev: 18664499.010000,67.745331
Num Phases Traversed: 18479801.000000
Num Phases Traversed - Session Mean & Stddev: 184798.010000,0.670746
Num Path Iterations: 18479801.000000
Num Path Iterations - Session Mean & Stddev: 184798.010000,0.670746
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f16c7600000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1875445567.000000
Throughput: 187544556.700000
Num Accesses - Session Mean & Stddev: 18754455.670000,51.608925
Num Phases Traversed: 18568867.000000
Num Phases Traversed - Session Mean & Stddev: 185688.670000,0.510979
Num Path Iterations: 18568867.000000
Num Path Iterations - Session Mean & Stddev: 185688.670000,0.510979
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0595400000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1879087021.000000
Throughput: 187908702.100000
Num Accesses - Session Mean & Stddev: 18790870.210000,54.004314
Num Phases Traversed: 18604921.000000
Num Phases Traversed - Session Mean & Stddev: 186049.210000,0.534696
Num Path Iterations: 18604921.000000
Num Path Iterations - Session Mean & Stddev: 186049.210000,0.534696
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fada0400000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1861525242.000000
Throughput: 186152524.200000
Num Accesses - Session Mean & Stddev: 18615252.420000,49.849409
Num Phases Traversed: 18431042.000000
Num Phases Traversed - Session Mean & Stddev: 184310.420000,0.493559
Num Path Iterations: 18431042.000000
Num Path Iterations - Session Mean & Stddev: 184310.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8a7aa00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1864019942.000000
Throughput: 186401994.200000
Num Accesses - Session Mean & Stddev: 18640199.420000,49.849409
Num Phases Traversed: 18455742.000000
Num Phases Traversed - Session Mean & Stddev: 184557.420000,0.493559
Num Path Iterations: 18455742.000000
Num Path Iterations - Session Mean & Stddev: 184557.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3e14c00000 of size 8192 bytes.
[1.227s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1861000042.000000
Throughput: 186100004.200000
Num Accesses - Session Mean & Stddev: 18610000.420000,2568.665709
Num Phases Traversed: 18425842.000000
Num Phases Traversed - Session Mean & Stddev: 184258.420000,25.432334
Num Path Iterations: 18425842.000000
Num Path Iterations - Session Mean & Stddev: 184258.420000,25.432334
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f678d800000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1872067521.000000
Throughput: 187206752.100000
Num Accesses - Session Mean & Stddev: 18720675.210000,57.658528
Num Phases Traversed: 18535421.000000
Num Phases Traversed - Session Mean & Stddev: 185354.210000,0.570877
Num Path Iterations: 18535421.000000
Num Path Iterations - Session Mean & Stddev: 185354.210000,0.570877
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7a41000000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1856430600.000000
Throughput: 185643060.000000
Num Accesses - Session Mean & Stddev: 18564306.000000,66.995821
Num Phases Traversed: 18380600.000000
Num Phases Traversed - Session Mean & Stddev: 183806.000000,0.663325
Num Path Iterations: 18380600.000000
Num Path Iterations - Session Mean & Stddev: 183806.000000,0.663325
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe403e00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1856032256.000000
Throughput: 185603225.600000
Num Accesses - Session Mean & Stddev: 18560322.560000,54.051516
Num Phases Traversed: 18376656.000000
Num Phases Traversed - Session Mean & Stddev: 183766.560000,0.535164
Num Path Iterations: 18376656.000000
Num Path Iterations - Session Mean & Stddev: 183766.560000,0.535164
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faa03200000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1857906816.000000
Throughput: 185790681.600000
Num Accesses - Session Mean & Stddev: 18579068.160000,63.429287
Num Phases Traversed: 18395216.000000
Num Phases Traversed - Session Mean & Stddev: 183952.160000,0.628013
Num Path Iterations: 18395216.000000
Num Path Iterations - Session Mean & Stddev: 183952.160000,0.628013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7effe5a00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1856951861.000000
Throughput: 185695186.100000
Num Accesses - Session Mean & Stddev: 18569518.610000,53.243384
Num Phases Traversed: 18385761.000000
Num Phases Traversed - Session Mean & Stddev: 183857.610000,0.527162
Num Path Iterations: 18385761.000000
Num Path Iterations - Session Mean & Stddev: 183857.610000,0.527162
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcb87000000 of size 8192 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1877070455.000000
Throughput: 187707045.500000
Num Accesses - Session Mean & Stddev: 18770704.550000,50.246866
Num Phases Traversed: 18584955.000000
Num Phases Traversed - Session Mean & Stddev: 185849.550000,0.497494
Num Path Iterations: 18584955.000000
Num Path Iterations - Session Mean & Stddev: 185849.550000,0.497494
+ set +x

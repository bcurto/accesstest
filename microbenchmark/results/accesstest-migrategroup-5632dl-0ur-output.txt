++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbfd8e00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072733130.000000
Throughput: 207273313.000000
Num Accesses - Session Mean & Stddev: 20727331.300000,1536.387519
Num Phases Traversed: 726610.000000
Num Phases Traversed - Session Mean & Stddev: 7266.100000,0.538516
Num Path Iterations: 726610.000000
Num Path Iterations - Session Mean & Stddev: 7266.100000,0.538516
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7162000000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2071155421.000000
Throughput: 207115542.100000
Num Accesses - Session Mean & Stddev: 20711554.210000,1412.451120
Num Phases Traversed: 726057.000000
Num Phases Traversed - Session Mean & Stddev: 7260.570000,0.495076
Num Path Iterations: 726057.000000
Num Path Iterations - Session Mean & Stddev: 7260.570000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1ebc400000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072658952.000000
Throughput: 207265895.200000
Num Accesses - Session Mean & Stddev: 20726589.520000,1321.034507
Num Phases Traversed: 726584.000000
Num Phases Traversed - Session Mean & Stddev: 7265.840000,0.463033
Num Path Iterations: 726584.000000
Num Path Iterations - Session Mean & Stddev: 7265.840000,0.463033
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd08ee00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2071609048.000000
Throughput: 207160904.800000
Num Accesses - Session Mean & Stddev: 20716090.480000,1745.700321
Num Phases Traversed: 726216.000000
Num Phases Traversed - Session Mean & Stddev: 7262.160000,0.611882
Num Path Iterations: 726216.000000
Num Path Iterations - Session Mean & Stddev: 7262.160000,0.611882
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5c63a00000 of size 360448 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2070941446.000000
Throughput: 207094144.600000
Num Accesses - Session Mean & Stddev: 20709414.460000,1865.608964
Num Phases Traversed: 725982.000000
Num Phases Traversed - Session Mean & Stddev: 7259.820000,0.653911
Num Path Iterations: 725982.000000
Num Path Iterations - Session Mean & Stddev: 7259.820000,0.653911
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7008e00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072462095.000000
Throughput: 207246209.500000
Num Accesses - Session Mean & Stddev: 20724620.950000,1235.385238
Num Phases Traversed: 726515.000000
Num Phases Traversed - Session Mean & Stddev: 7265.150000,0.433013
Num Path Iterations: 726515.000000
Num Path Iterations - Session Mean & Stddev: 7265.150000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f923d400000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2071823023.000000
Throughput: 207182302.300000
Num Accesses - Session Mean & Stddev: 20718230.230000,1853.132763
Num Phases Traversed: 726291.000000
Num Phases Traversed - Session Mean & Stddev: 7262.910000,0.649538
Num Path Iterations: 726291.000000
Num Path Iterations - Session Mean & Stddev: 7262.910000,0.649538
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faa70600000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2069928631.000000
Throughput: 206992863.100000
Num Accesses - Session Mean & Stddev: 20699286.310000,1656.460743
Num Phases Traversed: 725627.000000
Num Phases Traversed - Session Mean & Stddev: 7256.270000,0.580603
Num Path Iterations: 725627.000000
Num Path Iterations - Session Mean & Stddev: 7256.270000,0.580603
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3d61e00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2070002809.000000
Throughput: 207000280.900000
Num Accesses - Session Mean & Stddev: 20700028.090000,1423.929985
Num Phases Traversed: 725653.000000
Num Phases Traversed - Session Mean & Stddev: 7256.530000,0.499099
Num Path Iterations: 725653.000000
Num Path Iterations - Session Mean & Stddev: 7256.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efccdc00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072322298.000000
Throughput: 207232229.800000
Num Accesses - Session Mean & Stddev: 20723222.980000,1467.008050
Num Phases Traversed: 726466.000000
Num Phases Traversed - Session Mean & Stddev: 7264.660000,0.514198
Num Path Iterations: 726466.000000
Num Path Iterations - Session Mean & Stddev: 7264.660000,0.514198
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3bdec00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072678923.000000
Throughput: 207267892.300000
Num Accesses - Session Mean & Stddev: 20726789.230000,1514.779118
Num Phases Traversed: 726591.000000
Num Phases Traversed - Session Mean & Stddev: 7265.910000,0.530943
Num Path Iterations: 726591.000000
Num Path Iterations - Session Mean & Stddev: 7265.910000,0.530943
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7224400000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072310886.000000
Throughput: 207231088.600000
Num Accesses - Session Mean & Stddev: 20723108.860000,1384.807525
Num Phases Traversed: 726462.000000
Num Phases Traversed - Session Mean & Stddev: 7264.620000,0.485386
Num Path Iterations: 726462.000000
Num Path Iterations - Session Mean & Stddev: 7264.620000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f874d000000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2073055519.000000
Throughput: 207305551.900000
Num Accesses - Session Mean & Stddev: 20730555.190000,1389.208873
Num Phases Traversed: 726723.000000
Num Phases Traversed - Session Mean & Stddev: 7267.230000,0.486929
Num Path Iterations: 726723.000000
Num Path Iterations - Session Mean & Stddev: 7267.230000,0.486929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fced6c00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2071494928.000000
Throughput: 207149492.800000
Num Accesses - Session Mean & Stddev: 20714949.280000,1619.941339
Num Phases Traversed: 726176.000000
Num Phases Traversed - Session Mean & Stddev: 7261.760000,0.567803
Num Path Iterations: 726176.000000
Num Path Iterations - Session Mean & Stddev: 7261.760000,0.567803
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0acc200000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2071449280.000000
Throughput: 207144928.000000
Num Accesses - Session Mean & Stddev: 20714492.800000,1613.900517
Num Phases Traversed: 726160.000000
Num Phases Traversed - Session Mean & Stddev: 7261.600000,0.565685
Num Path Iterations: 726160.000000
Num Path Iterations - Session Mean & Stddev: 7261.600000,0.565685
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd24be00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2070644734.000000
Throughput: 207064473.400000
Num Accesses - Session Mean & Stddev: 20706447.340000,1830.372384
Num Phases Traversed: 725878.000000
Num Phases Traversed - Session Mean & Stddev: 7258.780000,0.641561
Num Path Iterations: 725878.000000
Num Path Iterations - Session Mean & Stddev: 7258.780000,0.641561
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdd73800000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072621863.000000
Throughput: 207262186.300000
Num Accesses - Session Mean & Stddev: 20726218.630000,1471.163496
Num Phases Traversed: 726571.000000
Num Phases Traversed - Session Mean & Stddev: 7265.710000,0.515655
Num Path Iterations: 726571.000000
Num Path Iterations - Session Mean & Stddev: 7265.710000,0.515655
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd58b000000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2070981388.000000
Throughput: 207098138.800000
Num Accesses - Session Mean & Stddev: 20709813.880000,1931.630598
Num Phases Traversed: 725996.000000
Num Phases Traversed - Session Mean & Stddev: 7259.960000,0.677052
Num Path Iterations: 725996.000000
Num Path Iterations - Session Mean & Stddev: 7259.960000,0.677052
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd6c7600000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2071914319.000000
Throughput: 207191431.900000
Num Accesses - Session Mean & Stddev: 20719143.190000,1797.842800
Num Phases Traversed: 726323.000000
Num Phases Traversed - Session Mean & Stddev: 7263.230000,0.630159
Num Path Iterations: 726323.000000
Num Path Iterations - Session Mean & Stddev: 7263.230000,0.630159
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1381000000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2070756001.000000
Throughput: 207075600.100000
Num Accesses - Session Mean & Stddev: 20707560.010000,1761.250973
Num Phases Traversed: 725917.000000
Num Phases Traversed - Session Mean & Stddev: 7259.170000,0.617333
Num Path Iterations: 725917.000000
Num Path Iterations - Session Mean & Stddev: 7259.170000,0.617333
+ set +x

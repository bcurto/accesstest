++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2d00000000 of size 3145728 bytes.
[1.496s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810235447.000000
Throughput: 81023544.700000
Num Accesses - Session Mean & Stddev: 8102354.470000,16582.295232
Num Phases Traversed: 33019.000000
Num Phases Traversed - Session Mean & Stddev: 330.190000,0.673721
Num Path Iterations: 33019.000000
Num Path Iterations - Session Mean & Stddev: 330.190000,0.673721
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0d00000000 of size 3145728 bytes.
[1.531s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810481577.000000
Throughput: 81048157.700000
Num Accesses - Session Mean & Stddev: 8104815.770000,15681.011646
Num Phases Traversed: 33029.000000
Num Phases Traversed - Session Mean & Stddev: 330.290000,0.637103
Num Path Iterations: 33029.000000
Num Path Iterations - Session Mean & Stddev: 330.290000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb5c0000000 of size 3145728 bytes.
[1.496s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811613775.000000
Throughput: 81161377.500000
Num Accesses - Session Mean & Stddev: 8116137.750000,16092.836955
Num Phases Traversed: 33075.000000
Num Phases Traversed - Session Mean & Stddev: 330.750000,0.653835
Num Path Iterations: 33075.000000
Num Path Iterations - Session Mean & Stddev: 330.750000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f38c0000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810973837.000000
Throughput: 81097383.700000
Num Accesses - Session Mean & Stddev: 8109738.370000,12304.038454
Num Phases Traversed: 33049.000000
Num Phases Traversed - Session Mean & Stddev: 330.490000,0.499900
Num Path Iterations: 33049.000000
Num Path Iterations - Session Mean & Stddev: 330.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa600000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810678481.000000
Throughput: 81067848.100000
Num Accesses - Session Mean & Stddev: 8106784.810000,14625.590533
Num Phases Traversed: 33037.000000
Num Phases Traversed - Session Mean & Stddev: 330.370000,0.594222
Num Path Iterations: 33037.000000
Num Path Iterations - Session Mean & Stddev: 330.370000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1140000000 of size 3145728 bytes.
[1.496s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810333899.000000
Throughput: 81033389.900000
Num Accesses - Session Mean & Stddev: 8103338.990000,16272.525281
Num Phases Traversed: 33023.000000
Num Phases Traversed - Session Mean & Stddev: 330.230000,0.661135
Num Path Iterations: 33023.000000
Num Path Iterations - Session Mean & Stddev: 330.230000,0.661135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdd80000000 of size 3145728 bytes.
[1.496s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811269193.000000
Throughput: 81126919.300000
Num Accesses - Session Mean & Stddev: 8112691.930000,14307.331755
Num Phases Traversed: 33061.000000
Num Phases Traversed - Session Mean & Stddev: 330.610000,0.581292
Num Path Iterations: 33061.000000
Num Path Iterations - Session Mean & Stddev: 330.610000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdf80000000 of size 3145728 bytes.
[1.527s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810776933.000000
Throughput: 81077693.300000
Num Accesses - Session Mean & Stddev: 8107769.330000,13964.488735
Num Phases Traversed: 33041.000000
Num Phases Traversed - Session Mean & Stddev: 330.410000,0.567362
Num Path Iterations: 33041.000000
Num Path Iterations - Session Mean & Stddev: 330.410000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2c40000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810629255.000000
Throughput: 81062925.500000
Num Accesses - Session Mean & Stddev: 8106292.550000,14920.838284
Num Phases Traversed: 33035.000000
Num Phases Traversed - Session Mean & Stddev: 330.350000,0.606218
Num Path Iterations: 33035.000000
Num Path Iterations - Session Mean & Stddev: 330.350000,0.606218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9d00000000 of size 3145728 bytes.
[1.496s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 798962693.000000
Throughput: 79896269.300000
Num Accesses - Session Mean & Stddev: 7989626.930000,13877.454614
Num Phases Traversed: 32561.000000
Num Phases Traversed - Session Mean & Stddev: 325.610000,0.563826
Num Path Iterations: 32561.000000
Num Path Iterations - Session Mean & Stddev: 325.610000,0.563826
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0540000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810580029.000000
Throughput: 81058002.900000
Num Accesses - Session Mean & Stddev: 8105800.290000,15194.416473
Num Phases Traversed: 33033.000000
Num Phases Traversed - Session Mean & Stddev: 330.330000,0.617333
Num Path Iterations: 33033.000000
Num Path Iterations - Session Mean & Stddev: 330.330000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0b00000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810580029.000000
Throughput: 81058002.900000
Num Accesses - Session Mean & Stddev: 8105800.290000,15194.416473
Num Phases Traversed: 33033.000000
Num Phases Traversed - Session Mean & Stddev: 330.330000,0.617333
Num Path Iterations: 33033.000000
Num Path Iterations - Session Mean & Stddev: 330.330000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7e80000000 of size 3145728 bytes.
[1.529s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810530803.000000
Throughput: 81053080.300000
Num Accesses - Session Mean & Stddev: 8105308.030000,15447.476461
Num Phases Traversed: 33031.000000
Num Phases Traversed - Session Mean & Stddev: 330.310000,0.627615
Num Path Iterations: 33031.000000
Num Path Iterations - Session Mean & Stddev: 330.310000,0.627615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3140000000 of size 3145728 bytes.
[1.531s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810629255.000000
Throughput: 81062925.500000
Num Accesses - Session Mean & Stddev: 8106292.550000,14920.838284
Num Phases Traversed: 33035.000000
Num Phases Traversed - Session Mean & Stddev: 330.350000,0.606218
Num Path Iterations: 33035.000000
Num Path Iterations - Session Mean & Stddev: 330.350000,0.606218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8900000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810530803.000000
Throughput: 81053080.300000
Num Accesses - Session Mean & Stddev: 8105308.030000,15447.476461
Num Phases Traversed: 33031.000000
Num Phases Traversed - Session Mean & Stddev: 330.310000,0.627615
Num Path Iterations: 33031.000000
Num Path Iterations - Session Mean & Stddev: 330.310000,0.627615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8e80000000 of size 3145728 bytes.
[1.484s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810284673.000000
Throughput: 81028467.300000
Num Accesses - Session Mean & Stddev: 8102846.730000,16435.513895
Num Phases Traversed: 33021.000000
Num Phases Traversed - Session Mean & Stddev: 330.210000,0.667757
Num Path Iterations: 33021.000000
Num Path Iterations - Session Mean & Stddev: 330.210000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdfc0000000 of size 3145728 bytes.
[1.500s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 807429565.000000
Throughput: 80742956.500000
Num Accesses - Session Mean & Stddev: 8074295.650000,17185.092010
Num Phases Traversed: 32905.000000
Num Phases Traversed - Session Mean & Stddev: 329.050000,0.698212
Num Path Iterations: 32905.000000
Num Path Iterations - Session Mean & Stddev: 329.050000,0.698212
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3980000000 of size 3145728 bytes.
[1.524s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810629255.000000
Throughput: 81062925.500000
Num Accesses - Session Mean & Stddev: 8106292.550000,14920.838284
Num Phases Traversed: 33035.000000
Num Phases Traversed - Session Mean & Stddev: 330.350000,0.606218
Num Path Iterations: 33035.000000
Num Path Iterations - Session Mean & Stddev: 330.350000,0.606218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc400000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810481577.000000
Throughput: 81048157.700000
Num Accesses - Session Mean & Stddev: 8104815.770000,15681.011646
Num Phases Traversed: 33029.000000
Num Phases Traversed - Session Mean & Stddev: 330.290000,0.637103
Num Path Iterations: 33029.000000
Num Path Iterations - Session Mean & Stddev: 330.290000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.1 0/24576,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.1 0/24576,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4540000000 of size 3145728 bytes.
[1.527s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810383125.000000
Throughput: 81038312.500000
Num Accesses - Session Mean & Stddev: 8103831.250000,16092.836955
Num Phases Traversed: 33025.000000
Num Phases Traversed - Session Mean & Stddev: 330.250000,0.653835
Num Path Iterations: 33025.000000
Num Path Iterations - Session Mean & Stddev: 330.250000,0.653835
+ set +x

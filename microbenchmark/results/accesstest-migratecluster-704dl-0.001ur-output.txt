++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0dc5a00000 of size 45056 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2832596960.000000
Throughput: 283259696.000000
Num Accesses - Session Mean & Stddev: 28325969.600000,190.570302
Num Phases Traversed: 7281840.000000
Num Phases Traversed - Session Mean & Stddev: 72818.400000,0.489898
Num Path Iterations: 7281840.000000
Num Path Iterations - Session Mean & Stddev: 72818.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa9cb800000 of size 45056 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2829911304.000000
Throughput: 282991130.400000
Num Accesses - Session Mean & Stddev: 28299113.040000,186.720000
Num Phases Traversed: 7274936.000000
Num Phases Traversed - Session Mean & Stddev: 72749.360000,0.480000
Num Path Iterations: 7274936.000000
Num Path Iterations - Session Mean & Stddev: 72749.360000,0.480000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faf62400000 of size 45056 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2828993264.000000
Throughput: 282899326.400000
Num Accesses - Session Mean & Stddev: 28289932.640000,166.135338
Num Phases Traversed: 7272576.000000
Num Phases Traversed - Session Mean & Stddev: 72725.760000,0.427083
Num Path Iterations: 7272576.000000
Num Path Iterations - Session Mean & Stddev: 72725.760000,0.427083
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7b95600000 of size 45056 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2833723893.000000
Throughput: 283372389.300000
Num Accesses - Session Mean & Stddev: 28337238.930000,187.810876
Num Phases Traversed: 7284737.000000
Num Phases Traversed - Session Mean & Stddev: 72847.370000,0.482804
Num Path Iterations: 7284737.000000
Num Path Iterations - Session Mean & Stddev: 72847.370000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd31dc00000 of size 45056 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2808684741.000000
Throughput: 280868474.100000
Num Accesses - Session Mean & Stddev: 28086847.410000,179.909872
Num Phases Traversed: 7220369.000000
Num Phases Traversed - Session Mean & Stddev: 72203.690000,0.462493
Num Path Iterations: 7220369.000000
Num Path Iterations - Session Mean & Stddev: 72203.690000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6c22600000 of size 45056 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2830963549.000000
Throughput: 283096354.900000
Num Accesses - Session Mean & Stddev: 28309635.490000,191.323156
Num Phases Traversed: 7277641.000000
Num Phases Traversed - Session Mean & Stddev: 72776.410000,0.491833
Num Path Iterations: 7277641.000000
Num Path Iterations - Session Mean & Stddev: 72776.410000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f512f800000 of size 45056 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2836555035.000000
Throughput: 283655503.500000
Num Accesses - Session Mean & Stddev: 28365550.350000,159.205111
Num Phases Traversed: 7292015.000000
Num Phases Traversed - Session Mean & Stddev: 72920.150000,0.409268
Num Path Iterations: 7292015.000000
Num Path Iterations - Session Mean & Stddev: 72920.150000,0.409268
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faa1dc00000 of size 45056 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2783740116.000000
Throughput: 278374011.600000
Num Accesses - Session Mean & Stddev: 27837401.160000,193.094522
Num Phases Traversed: 7156244.000000
Num Phases Traversed - Session Mean & Stddev: 71562.440000,0.496387
Num Path Iterations: 7156244.000000
Num Path Iterations - Session Mean & Stddev: 71562.440000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f296f200000 of size 45056 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2829371761.000000
Throughput: 282937176.100000
Num Accesses - Session Mean & Stddev: 28293717.610000,194.461096
Num Phases Traversed: 7273549.000000
Num Phases Traversed - Session Mean & Stddev: 72735.490000,0.499900
Num Path Iterations: 7273549.000000
Num Path Iterations - Session Mean & Stddev: 72735.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1a08c00000 of size 45056 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2833218971.000000
Throughput: 283321897.100000
Num Accesses - Session Mean & Stddev: 28332189.710000,189.734725
Num Phases Traversed: 7283439.000000
Num Phases Traversed - Session Mean & Stddev: 72834.390000,0.487750
Num Path Iterations: 7283439.000000
Num Path Iterations - Session Mean & Stddev: 72834.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fde1fc00000 of size 45056 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2837525979.000000
Throughput: 283752597.900000
Num Accesses - Session Mean & Stddev: 28375259.790000,173.050357
Num Phases Traversed: 7294511.000000
Num Phases Traversed - Session Mean & Stddev: 72945.110000,0.444860
Num Path Iterations: 7294511.000000
Num Path Iterations - Session Mean & Stddev: 72945.110000,0.444860
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9792400000 of size 45056 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2786786375.000000
Throughput: 278678637.500000
Num Accesses - Session Mean & Stddev: 27867863.750000,168.441941
Num Phases Traversed: 7164075.000000
Num Phases Traversed - Session Mean & Stddev: 71640.750000,0.433013
Num Path Iterations: 7164075.000000
Num Path Iterations - Session Mean & Stddev: 71640.750000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7421800000 of size 45056 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2818397293.000000
Throughput: 281839729.300000
Num Accesses - Session Mean & Stddev: 28183972.930000,187.810876
Num Phases Traversed: 7245337.000000
Num Phases Traversed - Session Mean & Stddev: 72453.370000,0.482804
Num Path Iterations: 7245337.000000
Num Path Iterations - Session Mean & Stddev: 72453.370000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9b3ca00000 of size 45056 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2830413503.000000
Throughput: 283041350.300000
Num Accesses - Session Mean & Stddev: 28304135.030000,172.700229
Num Phases Traversed: 7276227.000000
Num Phases Traversed - Session Mean & Stddev: 72762.270000,0.443959
Num Path Iterations: 7276227.000000
Num Path Iterations - Session Mean & Stddev: 72762.270000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff4bfe00000 of size 45056 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2830019446.000000
Throughput: 283001944.600000
Num Accesses - Session Mean & Stddev: 28300194.460000,145.758253
Num Phases Traversed: 7275214.000000
Num Phases Traversed - Session Mean & Stddev: 72752.140000,0.374700
Num Path Iterations: 7275214.000000
Num Path Iterations - Session Mean & Stddev: 72752.140000,0.374700
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7580600000 of size 45056 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2807260223.000000
Throughput: 280726022.300000
Num Accesses - Session Mean & Stddev: 28072602.230000,158.060422
Num Phases Traversed: 7216707.000000
Num Phases Traversed - Session Mean & Stddev: 72167.070000,0.406325
Num Path Iterations: 7216707.000000
Num Path Iterations - Session Mean & Stddev: 72167.070000,0.406325
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f08fd400000 of size 45056 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2835537411.000000
Throughput: 283553741.100000
Num Accesses - Session Mean & Stddev: 28355374.110000,178.219746
Num Phases Traversed: 7289399.000000
Num Phases Traversed - Session Mean & Stddev: 72893.990000,0.458148
Num Path Iterations: 7289399.000000
Num Path Iterations - Session Mean & Stddev: 72893.990000,0.458148
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa5a5600000 of size 45056 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2829061339.000000
Throughput: 282906133.900000
Num Accesses - Session Mean & Stddev: 28290613.390000,194.461096
Num Phases Traversed: 7272751.000000
Num Phases Traversed - Session Mean & Stddev: 72727.510000,0.499900
Num Path Iterations: 7272751.000000
Num Path Iterations - Session Mean & Stddev: 72727.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f61d6a00000 of size 45056 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2831763722.000000
Throughput: 283176372.200000
Num Accesses - Session Mean & Stddev: 28317637.220000,182.291227
Num Phases Traversed: 7279698.000000
Num Phases Traversed - Session Mean & Stddev: 72796.980000,0.468615
Num Path Iterations: 7279698.000000
Num Path Iterations - Session Mean & Stddev: 72796.980000,0.468615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/352,0.001 0/352,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/352,0.001 0/352,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    352 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc7cf400000 of size 45056 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2789869978.000000
Throughput: 278986997.800000
Num Accesses - Session Mean & Stddev: 27898699.780000,155.405378
Num Phases Traversed: 7172002.000000
Num Phases Traversed - Session Mean & Stddev: 71720.020000,0.399500
Num Path Iterations: 7172002.000000
Num Path Iterations - Session Mean & Stddev: 71720.020000,0.399500
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc9db200000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1454550600.000000
Throughput: 145455060.000000
Num Accesses - Session Mean & Stddev: 14545506.000000,13271.704393
Num Phases Traversed: 4475640.000000
Num Phases Traversed - Session Mean & Stddev: 44756.400000,40.836014
Num Path Iterations: 4475640.000000
Num Path Iterations - Session Mean & Stddev: 44756.400000,40.836014
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbcf3e00000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1451857000.000000
Throughput: 145185700.000000
Num Accesses - Session Mean & Stddev: 14518570.000000,360.383823
Num Phases Traversed: 4467352.000000
Num Phases Traversed - Session Mean & Stddev: 44673.520000,1.108873
Num Path Iterations: 4467352.000000
Num Path Iterations - Session Mean & Stddev: 44673.520000,1.108873
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f12f2400000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1450429925.000000
Throughput: 145042992.500000
Num Accesses - Session Mean & Stddev: 14504299.250000,3306.032847
Num Phases Traversed: 4462961.000000
Num Phases Traversed - Session Mean & Stddev: 44629.610000,10.172409
Num Path Iterations: 4462961.000000
Num Path Iterations - Session Mean & Stddev: 44629.610000,10.172409
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f695c800000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1452884975.000000
Throughput: 145288497.500000
Num Accesses - Session Mean & Stddev: 14528849.750000,6749.112326
Num Phases Traversed: 4470515.000000
Num Phases Traversed - Session Mean & Stddev: 44705.150000,20.766499
Num Path Iterations: 4470515.000000
Num Path Iterations - Session Mean & Stddev: 44705.150000,20.766499
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7a99200000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1452118300.000000
Throughput: 145211830.000000
Num Accesses - Session Mean & Stddev: 14521183.000000,1552.751912
Num Phases Traversed: 4468156.000000
Num Phases Traversed - Session Mean & Stddev: 44681.560000,4.777698
Num Path Iterations: 4468156.000000
Num Path Iterations - Session Mean & Stddev: 44681.560000,4.777698
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0962c00000 of size 36864 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1449444525.000000
Throughput: 144944452.500000
Num Accesses - Session Mean & Stddev: 14494445.250000,10068.423943
Num Phases Traversed: 4459929.000000
Num Phases Traversed - Session Mean & Stddev: 44599.290000,30.979766
Num Path Iterations: 4459929.000000
Num Path Iterations - Session Mean & Stddev: 44599.290000,30.979766
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6e30a00000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1456293250.000000
Throughput: 145629325.000000
Num Accesses - Session Mean & Stddev: 14562932.500000,5357.076651
Num Phases Traversed: 4481002.000000
Num Phases Traversed - Session Mean & Stddev: 44810.020000,16.483313
Num Path Iterations: 4481002.000000
Num Path Iterations - Session Mean & Stddev: 44810.020000,16.483313
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7cec600000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1454093000.000000
Throughput: 145409300.000000
Num Accesses - Session Mean & Stddev: 14540930.000000,18871.162431
Num Phases Traversed: 4474232.000000
Num Phases Traversed - Session Mean & Stddev: 44742.320000,58.065115
Num Path Iterations: 4474232.000000
Num Path Iterations - Session Mean & Stddev: 44742.320000,58.065115
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fee73e00000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1448296950.000000
Throughput: 144829695.000000
Num Accesses - Session Mean & Stddev: 14482969.500000,3634.185982
Num Phases Traversed: 4456398.000000
Num Phases Traversed - Session Mean & Stddev: 44563.980000,11.182111
Num Path Iterations: 4456398.000000
Num Path Iterations - Session Mean & Stddev: 44563.980000,11.182111
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6edd600000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1455478800.000000
Throughput: 145547880.000000
Num Accesses - Session Mean & Stddev: 14554788.000000,26099.135876
Num Phases Traversed: 4478496.000000
Num Phases Traversed - Session Mean & Stddev: 44784.960000,80.305033
Num Path Iterations: 4478496.000000
Num Path Iterations - Session Mean & Stddev: 44784.960000,80.305033
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4f4f000000 of size 36864 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1448841325.000000
Throughput: 144884132.500000
Num Accesses - Session Mean & Stddev: 14488413.250000,10694.905618
Num Phases Traversed: 4458073.000000
Num Phases Traversed - Session Mean & Stddev: 44580.730000,32.907402
Num Path Iterations: 4458073.000000
Num Path Iterations - Session Mean & Stddev: 44580.730000,32.907402
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0021e00000 of size 36864 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1453990625.000000
Throughput: 145399062.500000
Num Accesses - Session Mean & Stddev: 14539906.250000,21220.934915
Num Phases Traversed: 4473917.000000
Num Phases Traversed - Session Mean & Stddev: 44739.170000,65.295184
Num Path Iterations: 4473917.000000
Num Path Iterations - Session Mean & Stddev: 44739.170000,65.295184
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb743c00000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1450681150.000000
Throughput: 145068115.000000
Num Accesses - Session Mean & Stddev: 14506811.500000,8038.902428
Num Phases Traversed: 4463734.000000
Num Phases Traversed - Session Mean & Stddev: 44637.340000,24.735084
Num Path Iterations: 4463734.000000
Num Path Iterations - Session Mean & Stddev: 44637.340000,24.735084
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f11c5800000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1455400150.000000
Throughput: 145540015.000000
Num Accesses - Session Mean & Stddev: 14554001.500000,9069.820133
Num Phases Traversed: 4478254.000000
Num Phases Traversed - Session Mean & Stddev: 44782.540000,27.907139
Num Path Iterations: 4478254.000000
Num Path Iterations - Session Mean & Stddev: 44782.540000,27.907139
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f45fd000000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1451543375.000000
Throughput: 145154337.500000
Num Accesses - Session Mean & Stddev: 14515433.750000,12168.345047
Num Phases Traversed: 4466387.000000
Num Phases Traversed - Session Mean & Stddev: 44663.870000,37.441062
Num Path Iterations: 4466387.000000
Num Path Iterations - Session Mean & Stddev: 44663.870000,37.441062
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0b1d200000 of size 36864 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1455223025.000000
Throughput: 145522302.500000
Num Accesses - Session Mean & Stddev: 14552230.250000,977.266948
Num Phases Traversed: 4477709.000000
Num Phases Traversed - Session Mean & Stddev: 44777.090000,3.006975
Num Path Iterations: 4477709.000000
Num Path Iterations - Session Mean & Stddev: 44777.090000,3.006975
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4b29400000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1457772975.000000
Throughput: 145777297.500000
Num Accesses - Session Mean & Stddev: 14577729.750000,2335.199068
Num Phases Traversed: 4485555.000000
Num Phases Traversed - Session Mean & Stddev: 44855.550000,7.185228
Num Path Iterations: 4485555.000000
Num Path Iterations - Session Mean & Stddev: 44855.550000,7.185228
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f37c0600000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1453140100.000000
Throughput: 145314010.000000
Num Accesses - Session Mean & Stddev: 14531401.000000,18521.407546
Num Phases Traversed: 4471300.000000
Num Phases Traversed - Session Mean & Stddev: 44713.000000,56.988946
Num Path Iterations: 4471300.000000
Num Path Iterations - Session Mean & Stddev: 44713.000000,56.988946
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4b36000000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1453177800.000000
Throughput: 145317780.000000
Num Accesses - Session Mean & Stddev: 14531778.000000,46027.291725
Num Phases Traversed: 4471416.000000
Num Phases Traversed - Session Mean & Stddev: 44714.160000,141.622436
Num Path Iterations: 4471416.000000
Num Path Iterations - Session Mean & Stddev: 44714.160000,141.622436
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.01 0/288,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.01 0/288,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa8ae400000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1446630350.000000
Throughput: 144663035.000000
Num Accesses - Session Mean & Stddev: 14466303.500000,13446.558342
Num Phases Traversed: 4451270.000000
Num Phases Traversed - Session Mean & Stddev: 44512.700000,41.374026
Num Path Iterations: 4451270.000000
Num Path Iterations - Session Mean & Stddev: 44512.700000,41.374026
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f48ac200000 of size 81920 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 874802575.000000
Throughput: 87480257.500000
Num Accesses - Session Mean & Stddev: 8748025.750000,518.910674
Num Phases Traversed: 1292275.000000
Num Phases Traversed - Session Mean & Stddev: 12922.750000,0.766485
Num Path Iterations: 1292275.000000
Num Path Iterations - Session Mean & Stddev: 12922.750000,0.766485
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3b1da00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 879798158.000000
Throughput: 87979815.800000
Num Accesses - Session Mean & Stddev: 8797981.580000,510.405920
Num Phases Traversed: 1299654.000000
Num Phases Traversed - Session Mean & Stddev: 12996.540000,0.753923
Num Path Iterations: 1299654.000000
Num Path Iterations - Session Mean & Stddev: 12996.540000,0.753923
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f38a4e00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 878143570.000000
Throughput: 87814357.000000
Num Accesses - Session Mean & Stddev: 8781435.700000,652.875157
Num Phases Traversed: 1297210.000000
Num Phases Traversed - Session Mean & Stddev: 12972.100000,0.964365
Num Path Iterations: 1297210.000000
Num Path Iterations - Session Mean & Stddev: 12972.100000,0.964365
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5cd2400000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 872848753.000000
Throughput: 87284875.300000
Num Accesses - Session Mean & Stddev: 8728487.530000,722.172119
Num Phases Traversed: 1289389.000000
Num Phases Traversed - Session Mean & Stddev: 12893.890000,1.066724
Num Path Iterations: 1289389.000000
Num Path Iterations - Session Mean & Stddev: 12893.890000,1.066724
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2498400000 of size 81920 bytes.
[1.220s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 876411127.000000
Throughput: 87641112.700000
Num Accesses - Session Mean & Stddev: 8764111.270000,4115.241271
Num Phases Traversed: 1294651.000000
Num Phases Traversed - Session Mean & Stddev: 12946.510000,6.078643
Num Path Iterations: 1294651.000000
Num Path Iterations - Session Mean & Stddev: 12946.510000,6.078643
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbf3fe00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 878605284.000000
Throughput: 87860528.400000
Num Accesses - Session Mean & Stddev: 8786052.840000,625.483520
Num Phases Traversed: 1297892.000000
Num Phases Traversed - Session Mean & Stddev: 12978.920000,0.923905
Num Path Iterations: 1297892.000000
Num Path Iterations - Session Mean & Stddev: 12978.920000,0.923905
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa12bc00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 878975603.000000
Throughput: 87897560.300000
Num Accesses - Session Mean & Stddev: 8789756.030000,447.998068
Num Phases Traversed: 1298439.000000
Num Phases Traversed - Session Mean & Stddev: 12984.390000,0.661740
Num Path Iterations: 1298439.000000
Num Path Iterations - Session Mean & Stddev: 12984.390000,0.661740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f99d0a00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 878653351.000000
Throughput: 87865335.100000
Num Accesses - Session Mean & Stddev: 8786533.510000,726.979484
Num Phases Traversed: 1297963.000000
Num Phases Traversed - Session Mean & Stddev: 12979.630000,1.073825
Num Path Iterations: 1297963.000000
Num Path Iterations - Session Mean & Stddev: 12979.630000,1.073825
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdcc6200000 of size 81920 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 871907723.000000
Throughput: 87190772.300000
Num Accesses - Session Mean & Stddev: 8719077.230000,631.427270
Num Phases Traversed: 1287999.000000
Num Phases Traversed - Session Mean & Stddev: 12879.990000,0.932684
Num Path Iterations: 1287999.000000
Num Path Iterations - Session Mean & Stddev: 12879.990000,0.932684
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9117800000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 871738473.000000
Throughput: 87173847.300000
Num Accesses - Session Mean & Stddev: 8717384.730000,750.798799
Num Phases Traversed: 1287749.000000
Num Phases Traversed - Session Mean & Stddev: 12877.490000,1.109009
Num Path Iterations: 1287749.000000
Num Path Iterations - Session Mean & Stddev: 12877.490000,1.109009
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3e0d400000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 871360707.000000
Throughput: 87136070.700000
Num Accesses - Session Mean & Stddev: 8713607.070000,691.037615
Num Phases Traversed: 1287191.000000
Num Phases Traversed - Session Mean & Stddev: 12871.910000,1.020735
Num Path Iterations: 1287191.000000
Num Path Iterations - Session Mean & Stddev: 12871.910000,1.020735
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa363800000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 872883280.000000
Throughput: 87288328.000000
Num Accesses - Session Mean & Stddev: 8728832.800000,582.377421
Num Phases Traversed: 1289440.000000
Num Phases Traversed - Session Mean & Stddev: 12894.400000,0.860233
Num Path Iterations: 1289440.000000
Num Path Iterations - Session Mean & Stddev: 12894.400000,0.860233
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f29e6a00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 879415653.000000
Throughput: 87941565.300000
Num Accesses - Session Mean & Stddev: 8794156.530000,634.324073
Num Phases Traversed: 1299089.000000
Num Phases Traversed - Session Mean & Stddev: 12990.890000,0.936963
Num Path Iterations: 1299089.000000
Num Path Iterations - Session Mean & Stddev: 12990.890000,0.936963
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd535e00000 of size 81920 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 879400759.000000
Throughput: 87940075.900000
Num Accesses - Session Mean & Stddev: 8794007.590000,533.543008
Num Phases Traversed: 1299067.000000
Num Phases Traversed - Session Mean & Stddev: 12990.670000,0.788099
Num Path Iterations: 1299067.000000
Num Path Iterations - Session Mean & Stddev: 12990.670000,0.788099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9328e00000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 873753902.000000
Throughput: 87375390.200000
Num Accesses - Session Mean & Stddev: 8737539.020000,739.264161
Num Phases Traversed: 1290726.000000
Num Phases Traversed - Session Mean & Stddev: 12907.260000,1.091971
Num Path Iterations: 1290726.000000
Num Path Iterations - Session Mean & Stddev: 12907.260000,1.091971
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f43ce600000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 875101132.000000
Throughput: 87510113.200000
Num Accesses - Session Mean & Stddev: 8751011.320000,681.856977
Num Phases Traversed: 1292716.000000
Num Phases Traversed - Session Mean & Stddev: 12927.160000,1.007174
Num Path Iterations: 1292716.000000
Num Path Iterations - Session Mean & Stddev: 12927.160000,1.007174
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3cc5600000 of size 81920 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 875721264.000000
Throughput: 87572126.400000
Num Accesses - Session Mean & Stddev: 8757212.640000,523.352090
Num Phases Traversed: 1293632.000000
Num Phases Traversed - Session Mean & Stddev: 12936.320000,0.773046
Num Path Iterations: 1293632.000000
Num Path Iterations - Session Mean & Stddev: 12936.320000,0.773046
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f207f000000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 876142358.000000
Throughput: 87614235.800000
Num Accesses - Session Mean & Stddev: 8761423.580000,528.060000
Num Phases Traversed: 1294254.000000
Num Phases Traversed - Session Mean & Stddev: 12942.540000,0.780000
Num Path Iterations: 1294254.000000
Num Path Iterations - Session Mean & Stddev: 12942.540000,0.780000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc12a000000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 877958072.000000
Throughput: 87795807.200000
Num Accesses - Session Mean & Stddev: 8779580.720000,732.412631
Num Phases Traversed: 1296936.000000
Num Phases Traversed - Session Mean & Stddev: 12969.360000,1.081850
Num Path Iterations: 1296936.000000
Num Path Iterations - Session Mean & Stddev: 12969.360000,1.081850
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,1 0/640,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,1 0/640,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f09d4400000 of size 81920 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 877937085.000000
Throughput: 87793708.500000
Num Accesses - Session Mean & Stddev: 8779370.850000,651.997046
Num Phases Traversed: 1296905.000000
Num Phases Traversed - Session Mean & Stddev: 12969.050000,0.963068
Num Path Iterations: 1296905.000000
Num Path Iterations - Session Mean & Stddev: 12969.050000,0.963068
+ set +x

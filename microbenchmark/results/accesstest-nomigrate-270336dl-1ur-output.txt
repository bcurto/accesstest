++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff8c0000000 of size 17301504 bytes.
[3.432s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 261216160.000000
Throughput: 26121616.000000
Num Accesses - Session Mean & Stddev: 2612161.600000,104519.674101
Num Phases Traversed: 2032.000000
Num Phases Traversed - Session Mean & Stddev: 20.320000,0.773046
Num Path Iterations: 2032.000000
Num Path Iterations - Session Mean & Stddev: 20.320000,0.773046
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcbc0000000 of size 17301504 bytes.
[3.438s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 261216160.000000
Throughput: 26121616.000000
Num Accesses - Session Mean & Stddev: 2612161.600000,106254.271042
Num Phases Traversed: 2032.000000
Num Phases Traversed - Session Mean & Stddev: 20.320000,0.785875
Num Path Iterations: 2032.000000
Num Path Iterations - Session Mean & Stddev: 20.320000,0.785875
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd6c0000000 of size 17301504 bytes.
[3.382s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 260675340.000000
Throughput: 26067534.000000
Num Accesses - Session Mean & Stddev: 2606753.400000,106597.803367
Num Phases Traversed: 2028.000000
Num Phases Traversed - Session Mean & Stddev: 20.280000,0.788416
Num Path Iterations: 2028.000000
Num Path Iterations - Session Mean & Stddev: 20.280000,0.788416
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb740000000 of size 17301504 bytes.
[3.426s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 260945750.000000
Throughput: 26094575.000000
Num Accesses - Session Mean & Stddev: 2609457.500000,109005.755886
Num Phases Traversed: 2030.000000
Num Phases Traversed - Session Mean & Stddev: 20.300000,0.806226
Num Path Iterations: 2030.000000
Num Path Iterations - Session Mean & Stddev: 20.300000,0.806226
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4a40000000 of size 17301504 bytes.
[3.432s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 260945750.000000
Throughput: 26094575.000000
Num Accesses - Session Mean & Stddev: 2609457.500000,98430.725758
Num Phases Traversed: 2030.000000
Num Phases Traversed - Session Mean & Stddev: 20.300000,0.728011
Num Path Iterations: 2030.000000
Num Path Iterations - Session Mean & Stddev: 20.300000,0.728011
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2f80000000 of size 17301504 bytes.
[3.378s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 260404930.000000
Throughput: 26040493.000000
Num Accesses - Session Mean & Stddev: 2604049.300000,110868.100000
Num Phases Traversed: 2026.000000
Num Phases Traversed - Session Mean & Stddev: 20.260000,0.820000
Num Path Iterations: 2026.000000
Num Path Iterations - Session Mean & Stddev: 20.260000,0.820000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5900000000 of size 17301504 bytes.
[3.424s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 261216160.000000
Throughput: 26121616.000000
Num Accesses - Session Mean & Stddev: 2612161.600000,104519.674101
Num Phases Traversed: 2032.000000
Num Phases Traversed - Session Mean & Stddev: 20.320000,0.773046
Num Path Iterations: 2032.000000
Num Path Iterations - Session Mean & Stddev: 20.320000,0.773046
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd9c0000000 of size 17301504 bytes.
[3.393s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 260404930.000000
Throughput: 26040493.000000
Num Accesses - Session Mean & Stddev: 2604049.300000,110868.100000
Num Phases Traversed: 2026.000000
Num Phases Traversed - Session Mean & Stddev: 20.260000,0.820000
Num Path Iterations: 2026.000000
Num Path Iterations - Session Mean & Stddev: 20.260000,0.820000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3a00000000 of size 17301504 bytes.
[3.380s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 259593700.000000
Throughput: 25959370.000000
Num Accesses - Session Mean & Stddev: 2595937.000000,114725.246820
Num Phases Traversed: 2020.000000
Num Phases Traversed - Session Mean & Stddev: 20.200000,0.848528
Num Path Iterations: 2020.000000
Num Path Iterations - Session Mean & Stddev: 20.200000,0.848528
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb200000000 of size 17301504 bytes.
[3.432s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 260675340.000000
Throughput: 26067534.000000
Num Accesses - Session Mean & Stddev: 2606753.400000,108299.120602
Num Phases Traversed: 2028.000000
Num Phases Traversed - Session Mean & Stddev: 20.280000,0.800999
Num Path Iterations: 2028.000000
Num Path Iterations - Session Mean & Stddev: 20.280000,0.800999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fde80000000 of size 17301504 bytes.
[3.387s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 260675340.000000
Throughput: 26067534.000000
Num Accesses - Session Mean & Stddev: 2606753.400000,103110.988754
Num Phases Traversed: 2028.000000
Num Phases Traversed - Session Mean & Stddev: 20.280000,0.762627
Num Path Iterations: 2028.000000
Num Path Iterations - Session Mean & Stddev: 20.280000,0.762627
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff400000000 of size 17301504 bytes.
[3.431s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 260945750.000000
Throughput: 26094575.000000
Num Accesses - Session Mean & Stddev: 2609457.500000,102077.536482
Num Phases Traversed: 2030.000000
Num Phases Traversed - Session Mean & Stddev: 20.300000,0.754983
Num Path Iterations: 2030.000000
Num Path Iterations - Session Mean & Stddev: 20.300000,0.754983
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fed00000000 of size 17301504 bytes.
[3.397s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 259864110.000000
Throughput: 25986411.000000
Num Accesses - Session Mean & Stddev: 2598641.100000,112439.846694
Num Phases Traversed: 2022.000000
Num Phases Traversed - Session Mean & Stddev: 20.220000,0.831625
Num Path Iterations: 2022.000000
Num Path Iterations - Session Mean & Stddev: 20.220000,0.831625
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fac00000000 of size 17301504 bytes.
[3.378s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 258512060.000000
Throughput: 25851206.000000
Num Accesses - Session Mean & Stddev: 2585120.600000,127236.453807
Num Phases Traversed: 2012.000000
Num Phases Traversed - Session Mean & Stddev: 20.120000,0.941063
Num Path Iterations: 2012.000000
Num Path Iterations - Session Mean & Stddev: 20.120000,0.941063
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc9c0000000 of size 17301504 bytes.
[3.425s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 260134520.000000
Throughput: 26013452.000000
Num Accesses - Session Mean & Stddev: 2601345.200000,114916.296489
Num Phases Traversed: 2024.000000
Num Phases Traversed - Session Mean & Stddev: 20.240000,0.849941
Num Path Iterations: 2024.000000
Num Path Iterations - Session Mean & Stddev: 20.240000,0.849941
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc900000000 of size 17301504 bytes.
[3.388s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 260675340.000000
Throughput: 26067534.000000
Num Accesses - Session Mean & Stddev: 2606753.400000,103110.988754
Num Phases Traversed: 2028.000000
Num Phases Traversed - Session Mean & Stddev: 20.280000,0.762627
Num Path Iterations: 2028.000000
Num Path Iterations - Session Mean & Stddev: 20.280000,0.762627
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3580000000 of size 17301504 bytes.
[3.438s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 260404930.000000
Throughput: 26040493.000000
Num Accesses - Session Mean & Stddev: 2604049.300000,109206.811862
Num Phases Traversed: 2026.000000
Num Phases Traversed - Session Mean & Stddev: 20.260000,0.807713
Num Path Iterations: 2026.000000
Num Path Iterations - Session Mean & Stddev: 20.260000,0.807713
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5f80000000 of size 17301504 bytes.
[3.443s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 260675340.000000
Throughput: 26067534.000000
Num Accesses - Session Mean & Stddev: 2606753.400000,108299.120602
Num Phases Traversed: 2028.000000
Num Phases Traversed - Session Mean & Stddev: 20.280000,0.800999
Num Path Iterations: 2028.000000
Num Path Iterations - Session Mean & Stddev: 20.280000,0.800999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd540000000 of size 17301504 bytes.
[3.382s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 261216160.000000
Throughput: 26121616.000000
Num Accesses - Session Mean & Stddev: 2612161.600000,104519.674101
Num Phases Traversed: 2032.000000
Num Phases Traversed - Session Mean & Stddev: 20.320000,0.773046
Num Path Iterations: 2032.000000
Num Path Iterations - Session Mean & Stddev: 20.320000,0.773046
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f79c0000000 of size 17301504 bytes.
[3.428s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 260675340.000000
Throughput: 26067534.000000
Num Accesses - Session Mean & Stddev: 2606753.400000,103110.988754
Num Phases Traversed: 2028.000000
Num Phases Traversed - Session Mean & Stddev: 20.280000,0.762627
Num Path Iterations: 2028.000000
Num Path Iterations - Session Mean & Stddev: 20.280000,0.762627
+ set +x

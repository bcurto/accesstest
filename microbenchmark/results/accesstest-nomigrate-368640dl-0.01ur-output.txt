++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5640000000 of size 23592960 bytes.
[4.803s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 236714488.000000
Throughput: 23671448.800000
Num Accesses - Session Mean & Stddev: 2367144.880000,194616.735775
Num Phases Traversed: 1384.000000
Num Phases Traversed - Session Mean & Stddev: 13.840000,1.055651
Num Path Iterations: 1384.000000
Num Path Iterations - Session Mean & Stddev: 13.840000,1.055651
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9b00000000 of size 23592960 bytes.
[4.807s] Test is ready to start.
Test duration in range [10.000, 10.013] seconds
Num Accesses: 236714488.000000
Throughput: 23671448.800000
Num Accesses - Session Mean & Stddev: 2367144.880000,196355.351118
Num Phases Traversed: 1384.000000
Num Phases Traversed - Session Mean & Stddev: 13.840000,1.065082
Num Path Iterations: 1384.000000
Num Path Iterations - Session Mean & Stddev: 13.840000,1.065082
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0180000000 of size 23592960 bytes.
[4.803s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 237083202.000000
Throughput: 23708320.200000
Num Accesses - Session Mean & Stddev: 2370832.020000,200296.902301
Num Phases Traversed: 1386.000000
Num Phases Traversed - Session Mean & Stddev: 13.860000,1.086462
Num Path Iterations: 1386.000000
Num Path Iterations - Session Mean & Stddev: 13.860000,1.086462
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5a80000000 of size 23592960 bytes.
[4.743s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 236714488.000000
Throughput: 23671448.800000
Num Accesses - Session Mean & Stddev: 2367144.880000,183840.075690
Num Phases Traversed: 1384.000000
Num Phases Traversed - Session Mean & Stddev: 13.840000,0.997196
Num Path Iterations: 1384.000000
Num Path Iterations - Session Mean & Stddev: 13.840000,0.997196
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5d00000000 of size 23592960 bytes.
[4.775s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 235608346.000000
Throughput: 23560834.600000
Num Accesses - Session Mean & Stddev: 2356083.460000,199548.888849
Num Phases Traversed: 1378.000000
Num Phases Traversed - Session Mean & Stddev: 13.780000,1.082405
Num Path Iterations: 1378.000000
Num Path Iterations - Session Mean & Stddev: 13.780000,1.082405
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7580000000 of size 23592960 bytes.
[4.742s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 237451916.000000
Throughput: 23745191.600000
Num Accesses - Session Mean & Stddev: 2374519.160000,192085.485512
Num Phases Traversed: 1388.000000
Num Phases Traversed - Session Mean & Stddev: 13.880000,1.041921
Num Path Iterations: 1388.000000
Num Path Iterations - Session Mean & Stddev: 13.880000,1.041921
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5240000000 of size 23592960 bytes.
[4.802s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 236345774.000000
Throughput: 23634577.400000
Num Accesses - Session Mean & Stddev: 2363457.740000,207563.386395
Num Phases Traversed: 1382.000000
Num Phases Traversed - Session Mean & Stddev: 13.820000,1.125877
Num Path Iterations: 1382.000000
Num Path Iterations - Session Mean & Stddev: 13.820000,1.125877
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4d80000000 of size 23592960 bytes.
[4.804s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 234502204.000000
Throughput: 23450220.400000
Num Accesses - Session Mean & Stddev: 2345022.040000,207071.568187
Num Phases Traversed: 1372.000000
Num Phases Traversed - Session Mean & Stddev: 13.720000,1.123210
Num Path Iterations: 1372.000000
Num Path Iterations - Session Mean & Stddev: 13.720000,1.123210
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb0c0000000 of size 23592960 bytes.
[4.811s] Test is ready to start.
Test duration in range [10.000, 10.013] seconds
Num Accesses: 237820630.000000
Throughput: 23782063.000000
Num Accesses - Session Mean & Stddev: 2378206.300000,204461.803871
Num Phases Traversed: 1390.000000
Num Phases Traversed - Session Mean & Stddev: 13.900000,1.109054
Num Path Iterations: 1390.000000
Num Path Iterations - Session Mean & Stddev: 13.900000,1.109054
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa200000000 of size 23592960 bytes.
[4.810s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 237083202.000000
Throughput: 23708320.200000
Num Accesses - Session Mean & Stddev: 2370832.020000,211842.157169
Num Phases Traversed: 1386.000000
Num Phases Traversed - Session Mean & Stddev: 13.860000,1.149087
Num Path Iterations: 1386.000000
Num Path Iterations - Session Mean & Stddev: 13.860000,1.149087
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbd40000000 of size 23592960 bytes.
[4.804s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 236714488.000000
Throughput: 23671448.800000
Num Accesses - Session Mean & Stddev: 2367144.880000,198078.706532
Num Phases Traversed: 1384.000000
Num Phases Traversed - Session Mean & Stddev: 13.840000,1.074430
Num Path Iterations: 1384.000000
Num Path Iterations - Session Mean & Stddev: 13.840000,1.074430
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbec0000000 of size 23592960 bytes.
[4.806s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 236714488.000000
Throughput: 23671448.800000
Num Accesses - Session Mean & Stddev: 2367144.880000,185679.625965
Num Phases Traversed: 1384.000000
Num Phases Traversed - Session Mean & Stddev: 13.840000,1.007174
Num Path Iterations: 1384.000000
Num Path Iterations - Session Mean & Stddev: 13.840000,1.007174
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb240000000 of size 23592960 bytes.
[4.803s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 236714488.000000
Throughput: 23671448.800000
Num Accesses - Session Mean & Stddev: 2367144.880000,198078.706532
Num Phases Traversed: 1384.000000
Num Phases Traversed - Session Mean & Stddev: 13.840000,1.074430
Num Path Iterations: 1384.000000
Num Path Iterations - Session Mean & Stddev: 13.840000,1.074430
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff880000000 of size 23592960 bytes.
[4.746s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 236714488.000000
Throughput: 23671448.800000
Num Accesses - Session Mean & Stddev: 2367144.880000,183840.075690
Num Phases Traversed: 1384.000000
Num Phases Traversed - Session Mean & Stddev: 13.840000,0.997196
Num Path Iterations: 1384.000000
Num Path Iterations - Session Mean & Stddev: 13.840000,0.997196
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3ec0000000 of size 23592960 bytes.
[4.804s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 236714488.000000
Throughput: 23671448.800000
Num Accesses - Session Mean & Stddev: 2367144.880000,198078.706532
Num Phases Traversed: 1384.000000
Num Phases Traversed - Session Mean & Stddev: 13.840000,1.074430
Num Path Iterations: 1384.000000
Num Path Iterations - Session Mean & Stddev: 13.840000,1.074430
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb4c0000000 of size 23592960 bytes.
[4.804s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 237083202.000000
Throughput: 23708320.200000
Num Accesses - Session Mean & Stddev: 2370832.020000,200296.902301
Num Phases Traversed: 1386.000000
Num Phases Traversed - Session Mean & Stddev: 13.860000,1.086462
Num Path Iterations: 1386.000000
Num Path Iterations - Session Mean & Stddev: 13.860000,1.086462
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7d40000000 of size 23592960 bytes.
[4.803s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 236345774.000000
Throughput: 23634577.400000
Num Accesses - Session Mean & Stddev: 2363457.740000,197494.452981
Num Phases Traversed: 1382.000000
Num Phases Traversed - Session Mean & Stddev: 13.820000,1.071261
Num Path Iterations: 1382.000000
Num Path Iterations - Session Mean & Stddev: 13.820000,1.071261
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcfc0000000 of size 23592960 bytes.
[4.807s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 237083202.000000
Throughput: 23708320.200000
Num Accesses - Session Mean & Stddev: 2370832.020000,195139.946870
Num Phases Traversed: 1386.000000
Num Phases Traversed - Session Mean & Stddev: 13.860000,1.058489
Num Path Iterations: 1386.000000
Num Path Iterations - Session Mean & Stddev: 13.860000,1.058489
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa400000000 of size 23592960 bytes.
[4.790s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 236714488.000000
Throughput: 23671448.800000
Num Accesses - Session Mean & Stddev: 2367144.880000,187501.129510
Num Phases Traversed: 1384.000000
Num Phases Traversed - Session Mean & Stddev: 13.840000,1.017055
Num Path Iterations: 1384.000000
Num Path Iterations - Session Mean & Stddev: 13.840000,1.017055
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.01 0/184320,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.01 0/184320,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f00c0000000 of size 23592960 bytes.
[4.748s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 237451916.000000
Throughput: 23745191.600000
Num Accesses - Session Mean & Stddev: 2374519.160000,188513.483885
Num Phases Traversed: 1388.000000
Num Phases Traversed - Session Mean & Stddev: 13.880000,1.022546
Num Path Iterations: 1388.000000
Num Path Iterations - Session Mean & Stddev: 13.880000,1.022546
+ set +x

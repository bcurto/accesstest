++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe73a400000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1320947845.000000
Throughput: 132094784.500000
Num Accesses - Session Mean & Stddev: 13209478.450000,1746.735741
Num Phases Traversed: 2048081.000000
Num Phases Traversed - Session Mean & Stddev: 20480.810000,2.708117
Num Path Iterations: 2048081.000000
Num Path Iterations - Session Mean & Stddev: 20480.810000,2.708117
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feb85200000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1322175925.000000
Throughput: 132217592.500000
Num Accesses - Session Mean & Stddev: 13221759.250000,678.783977
Num Phases Traversed: 2049985.000000
Num Phases Traversed - Session Mean & Stddev: 20499.850000,1.052378
Num Path Iterations: 2049985.000000
Num Path Iterations - Session Mean & Stddev: 20499.850000,1.052378
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff17c400000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1322426185.000000
Throughput: 132242618.500000
Num Accesses - Session Mean & Stddev: 13224261.850000,2966.796678
Num Phases Traversed: 2050373.000000
Num Phases Traversed - Session Mean & Stddev: 20503.730000,4.599685
Num Path Iterations: 2050373.000000
Num Path Iterations - Session Mean & Stddev: 20503.730000,4.599685
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc95c400000 of size 77824 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1320705970.000000
Throughput: 132070597.000000
Num Accesses - Session Mean & Stddev: 13207059.700000,1433.061691
Num Phases Traversed: 2047706.000000
Num Phases Traversed - Session Mean & Stddev: 20477.060000,2.221801
Num Path Iterations: 2047706.000000
Num Path Iterations - Session Mean & Stddev: 20477.060000,2.221801
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7eff0b800000 of size 77824 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1320154495.000000
Throughput: 132015449.500000
Num Accesses - Session Mean & Stddev: 13201544.950000,3962.941098
Num Phases Traversed: 2046851.000000
Num Phases Traversed - Session Mean & Stddev: 20468.510000,6.144095
Num Path Iterations: 2046851.000000
Num Path Iterations - Session Mean & Stddev: 20468.510000,6.144095
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8287a00000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1322023705.000000
Throughput: 132202370.500000
Num Accesses - Session Mean & Stddev: 13220237.050000,932.443375
Num Phases Traversed: 2049749.000000
Num Phases Traversed - Session Mean & Stddev: 20497.490000,1.445649
Num Path Iterations: 2049749.000000
Num Path Iterations - Session Mean & Stddev: 20497.490000,1.445649
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fde0c800000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1320656305.000000
Throughput: 132065630.500000
Num Accesses - Session Mean & Stddev: 13206563.050000,3181.223436
Num Phases Traversed: 2047629.000000
Num Phases Traversed - Session Mean & Stddev: 20476.290000,4.932129
Num Path Iterations: 2047629.000000
Num Path Iterations - Session Mean & Stddev: 20476.290000,4.932129
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2fbb600000 of size 77824 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1321716685.000000
Throughput: 132171668.500000
Num Accesses - Session Mean & Stddev: 13217166.850000,4861.824146
Num Phases Traversed: 2049273.000000
Num Phases Traversed - Session Mean & Stddev: 20492.730000,7.537712
Num Path Iterations: 2049273.000000
Num Path Iterations - Session Mean & Stddev: 20492.730000,7.537712
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa5e0400000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1320284785.000000
Throughput: 132028478.500000
Num Accesses - Session Mean & Stddev: 13202847.850000,3518.006442
Num Phases Traversed: 2047053.000000
Num Phases Traversed - Session Mean & Stddev: 20470.530000,5.454274
Num Path Iterations: 2047053.000000
Num Path Iterations - Session Mean & Stddev: 20470.530000,5.454274
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdb11c00000 of size 77824 bytes.
[1.232s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1321093615.000000
Throughput: 132109361.500000
Num Accesses - Session Mean & Stddev: 13210936.150000,14225.212889
Num Phases Traversed: 2048307.000000
Num Phases Traversed - Session Mean & Stddev: 20483.070000,22.054594
Num Path Iterations: 2048307.000000
Num Path Iterations - Session Mean & Stddev: 20483.070000,22.054594
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2a80e00000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1320346060.000000
Throughput: 132034606.000000
Num Accesses - Session Mean & Stddev: 13203460.600000,3081.831329
Num Phases Traversed: 2047148.000000
Num Phases Traversed - Session Mean & Stddev: 20471.480000,4.778033
Num Path Iterations: 2047148.000000
Num Path Iterations - Session Mean & Stddev: 20471.480000,4.778033
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7531800000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1320923980.000000
Throughput: 132092398.000000
Num Accesses - Session Mean & Stddev: 13209239.800000,4718.932142
Num Phases Traversed: 2048044.000000
Num Phases Traversed - Session Mean & Stddev: 20480.440000,7.316174
Num Path Iterations: 2048044.000000
Num Path Iterations - Session Mean & Stddev: 20480.440000,7.316174
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9533000000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1320558265.000000
Throughput: 132055826.500000
Num Accesses - Session Mean & Stddev: 13205582.650000,1131.437372
Num Phases Traversed: 2047477.000000
Num Phases Traversed - Session Mean & Stddev: 20474.770000,1.754166
Num Path Iterations: 2047477.000000
Num Path Iterations - Session Mean & Stddev: 20474.770000,1.754166
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd9dc400000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1320875605.000000
Throughput: 132087560.500000
Num Accesses - Session Mean & Stddev: 13208756.050000,704.763966
Num Phases Traversed: 2047969.000000
Num Phases Traversed - Session Mean & Stddev: 20479.690000,1.092657
Num Path Iterations: 2047969.000000
Num Path Iterations - Session Mean & Stddev: 20479.690000,1.092657
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f97a1e00000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1321721200.000000
Throughput: 132172120.000000
Num Accesses - Session Mean & Stddev: 13217212.000000,2198.683811
Num Phases Traversed: 2049280.000000
Num Phases Traversed - Session Mean & Stddev: 20492.800000,3.408812
Num Path Iterations: 2049280.000000
Num Path Iterations - Session Mean & Stddev: 20492.800000,3.408812
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f564a800000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1321285180.000000
Throughput: 132128518.000000
Num Accesses - Session Mean & Stddev: 13212851.800000,3279.899520
Num Phases Traversed: 2048604.000000
Num Phases Traversed - Session Mean & Stddev: 20486.040000,5.085116
Num Path Iterations: 2048604.000000
Num Path Iterations - Session Mean & Stddev: 20486.040000,5.085116
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3d44e00000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1321153600.000000
Throughput: 132115360.000000
Num Accesses - Session Mean & Stddev: 13211536.000000,3382.408535
Num Phases Traversed: 2048400.000000
Num Phases Traversed - Session Mean & Stddev: 20484.000000,5.244044
Num Path Iterations: 2048400.000000
Num Path Iterations - Session Mean & Stddev: 20484.000000,5.244044
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcc93e00000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1321697335.000000
Throughput: 132169733.500000
Num Accesses - Session Mean & Stddev: 13216973.350000,1941.985640
Num Phases Traversed: 2049243.000000
Num Phases Traversed - Session Mean & Stddev: 20492.430000,3.010830
Num Path Iterations: 2049243.000000
Num Path Iterations - Session Mean & Stddev: 20492.430000,3.010830
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa50f600000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1322582275.000000
Throughput: 132258227.500000
Num Accesses - Session Mean & Stddev: 13225822.750000,1248.620714
Num Phases Traversed: 2050615.000000
Num Phases Traversed - Session Mean & Stddev: 20506.150000,1.935846
Num Path Iterations: 2050615.000000
Num Path Iterations - Session Mean & Stddev: 20506.150000,1.935846
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5651400000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1320248020.000000
Throughput: 132024802.000000
Num Accesses - Session Mean & Stddev: 13202480.200000,1470.600000
Num Phases Traversed: 2046996.000000
Num Phases Traversed - Session Mean & Stddev: 20469.960000,2.280000
Num Path Iterations: 2046996.000000
Num Path Iterations - Session Mean & Stddev: 20469.960000,2.280000
+ set +x

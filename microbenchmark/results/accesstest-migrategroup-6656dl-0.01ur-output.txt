++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff4a5200000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2057256785.000000
Throughput: 205725678.500000
Num Accesses - Session Mean & Stddev: 20572567.850000,2057.608764
Num Phases Traversed: 611469.000000
Num Phases Traversed - Session Mean & Stddev: 6114.690000,0.611474
Num Path Iterations: 611469.000000
Num Path Iterations - Session Mean & Stddev: 6114.690000,0.611474
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff78d000000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2053168310.000000
Throughput: 205316831.000000
Num Accesses - Session Mean & Stddev: 20531683.100000,1677.107358
Num Phases Traversed: 610254.000000
Num Phases Traversed - Session Mean & Stddev: 6102.540000,0.498397
Num Path Iterations: 610254.000000
Num Path Iterations - Session Mean & Stddev: 6102.540000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3d95e00000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2038234440.000000
Throughput: 203823444.000000
Num Accesses - Session Mean & Stddev: 20382344.400000,2217.838799
Num Phases Traversed: 605816.000000
Num Phases Traversed - Session Mean & Stddev: 6058.160000,0.659090
Num Path Iterations: 605816.000000
Num Path Iterations - Session Mean & Stddev: 6058.160000,0.659090
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f44dd200000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2053232245.000000
Throughput: 205323224.500000
Num Accesses - Session Mean & Stddev: 20532322.450000,2120.484060
Num Phases Traversed: 610273.000000
Num Phases Traversed - Session Mean & Stddev: 6102.730000,0.630159
Num Path Iterations: 610273.000000
Num Path Iterations - Session Mean & Stddev: 6102.730000,0.630159
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f166b800000 of size 425984 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2055106550.000000
Throughput: 205510655.000000
Num Accesses - Session Mean & Stddev: 20551065.500000,2101.441826
Num Phases Traversed: 610830.000000
Num Phases Traversed - Session Mean & Stddev: 6108.300000,0.624500
Num Path Iterations: 610830.000000
Num Path Iterations - Session Mean & Stddev: 6108.300000,0.624500
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f83af600000 of size 425984 bytes.
[1.248s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2050560435.000000
Throughput: 205056043.500000
Num Accesses - Session Mean & Stddev: 20505604.350000,2196.033134
Num Phases Traversed: 609479.000000
Num Phases Traversed - Session Mean & Stddev: 6094.790000,0.652610
Num Path Iterations: 609479.000000
Num Path Iterations - Session Mean & Stddev: 6094.790000,0.652610
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8c09800000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2045193260.000000
Throughput: 204519326.000000
Num Accesses - Session Mean & Stddev: 20451932.600000,2166.181996
Num Phases Traversed: 607884.000000
Num Phases Traversed - Session Mean & Stddev: 6078.840000,0.643739
Num Path Iterations: 607884.000000
Num Path Iterations - Session Mean & Stddev: 6078.840000,0.643739
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff95dc00000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2048814000.000000
Throughput: 204881400.000000
Num Accesses - Session Mean & Stddev: 20488140.000000,1903.531455
Num Phases Traversed: 608960.000000
Num Phases Traversed - Session Mean & Stddev: 6089.600000,0.565685
Num Path Iterations: 608960.000000
Num Path Iterations - Session Mean & Stddev: 6089.600000,0.565685
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1d8e200000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2040912980.000000
Throughput: 204091298.000000
Num Accesses - Session Mean & Stddev: 20409129.800000,2296.103996
Num Phases Traversed: 606612.000000
Num Phases Traversed - Session Mean & Stddev: 6066.120000,0.682349
Num Path Iterations: 606612.000000
Num Path Iterations - Session Mean & Stddev: 6066.120000,0.682349
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0cfd000000 of size 425984 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2053834580.000000
Throughput: 205383458.000000
Num Accesses - Session Mean & Stddev: 20538345.800000,1681.153461
Num Phases Traversed: 610452.000000
Num Phases Traversed - Session Mean & Stddev: 6104.520000,0.499600
Num Path Iterations: 610452.000000
Num Path Iterations - Session Mean & Stddev: 6104.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd017e00000 of size 425984 bytes.
[1.248s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2037948415.000000
Throughput: 203794841.500000
Num Accesses - Session Mean & Stddev: 20379484.150000,3286.521615
Num Phases Traversed: 605731.000000
Num Phases Traversed - Session Mean & Stddev: 6057.310000,0.976678
Num Path Iterations: 605731.000000
Num Path Iterations - Session Mean & Stddev: 6057.310000,0.976678
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbf7da00000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2051748280.000000
Throughput: 205174828.000000
Num Accesses - Session Mean & Stddev: 20517482.800000,2067.764435
Num Phases Traversed: 609832.000000
Num Phases Traversed - Session Mean & Stddev: 6098.320000,0.614492
Num Path Iterations: 609832.000000
Num Path Iterations - Session Mean & Stddev: 6098.320000,0.614492
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4259200000 of size 425984 bytes.
[1.248s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 595911315.000000
Throughput: 59591131.500000
Num Accesses - Session Mean & Stddev: 5959113.150000,2430.965061
Num Phases Traversed: 177191.000000
Num Phases Traversed - Session Mean & Stddev: 1771.910000,0.722426
Num Path Iterations: 177191.000000
Num Path Iterations - Session Mean & Stddev: 1771.910000,0.722426
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0efde00000 of size 425984 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2047982845.000000
Throughput: 204798284.500000
Num Accesses - Session Mean & Stddev: 20479828.450000,2214.517723
Num Phases Traversed: 608713.000000
Num Phases Traversed - Session Mean & Stddev: 6087.130000,0.658103
Num Path Iterations: 608713.000000
Num Path Iterations - Session Mean & Stddev: 6087.130000,0.658103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5e48800000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2038214250.000000
Throughput: 203821425.000000
Num Accesses - Session Mean & Stddev: 20382142.500000,2206.578063
Num Phases Traversed: 605810.000000
Num Phases Traversed - Session Mean & Stddev: 6058.100000,0.655744
Num Path Iterations: 605810.000000
Num Path Iterations - Session Mean & Stddev: 6058.100000,0.655744
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb10a000000 of size 425984 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2038893980.000000
Throughput: 203889398.000000
Num Accesses - Session Mean & Stddev: 20388939.800000,2195.259566
Num Phases Traversed: 606012.000000
Num Phases Traversed - Session Mean & Stddev: 6060.120000,0.652380
Num Path Iterations: 606012.000000
Num Path Iterations - Session Mean & Stddev: 6060.120000,0.652380
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4883200000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2056085765.000000
Throughput: 205608576.500000
Num Accesses - Session Mean & Stddev: 20560857.650000,1920.999487
Num Phases Traversed: 611121.000000
Num Phases Traversed - Session Mean & Stddev: 6111.210000,0.570877
Num Path Iterations: 611121.000000
Num Path Iterations - Session Mean & Stddev: 6111.210000,0.570877
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f775a400000 of size 425984 bytes.
[1.248s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2046266695.000000
Throughput: 204626669.500000
Num Accesses - Session Mean & Stddev: 20462666.950000,3026.817032
Num Phases Traversed: 608203.000000
Num Phases Traversed - Session Mean & Stddev: 6082.030000,0.899500
Num Path Iterations: 608203.000000
Num Path Iterations - Session Mean & Stddev: 6082.030000,0.899500
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5883200000 of size 425984 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2033594105.000000
Throughput: 203359410.500000
Num Accesses - Session Mean & Stddev: 20335941.050000,1942.103563
Num Phases Traversed: 604437.000000
Num Phases Traversed - Session Mean & Stddev: 6044.370000,0.577148
Num Path Iterations: 604437.000000
Num Path Iterations - Session Mean & Stddev: 6044.370000,0.577148
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3328,0.01 0/3328,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3328,0.01 0/3328,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3328 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feef2000000 of size 425984 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2057145740.000000
Throughput: 205714574.000000
Num Accesses - Session Mean & Stddev: 20571457.400000,1991.898100
Num Phases Traversed: 611436.000000
Num Phases Traversed - Session Mean & Stddev: 6114.360000,0.591946
Num Path Iterations: 611436.000000
Num Path Iterations - Session Mean & Stddev: 6114.360000,0.591946
+ set +x

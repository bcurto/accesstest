++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0251800000 of size 12288 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2168281411.000000
Throughput: 216828141.100000
Num Accesses - Session Mean & Stddev: 21682814.110000,62.538291
Num Phases Traversed: 16302967.000000
Num Phases Traversed - Session Mean & Stddev: 163029.670000,0.470213
Num Path Iterations: 16302967.000000
Num Path Iterations - Session Mean & Stddev: 163029.670000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7abb800000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2163444733.000000
Throughput: 216344473.300000
Num Accesses - Session Mean & Stddev: 21634447.330000,22.997850
Num Phases Traversed: 16266601.000000
Num Phases Traversed - Session Mean & Stddev: 162666.010000,0.172916
Num Path Iterations: 16266601.000000
Num Path Iterations - Session Mean & Stddev: 162666.010000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f691fe00000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2167522380.000000
Throughput: 216752238.000000
Num Accesses - Session Mean & Stddev: 21675223.800000,65.156427
Num Phases Traversed: 16297260.000000
Num Phases Traversed - Session Mean & Stddev: 162972.600000,0.489898
Num Path Iterations: 16297260.000000
Num Path Iterations - Session Mean & Stddev: 162972.600000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7381800000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2181087316.000000
Throughput: 218108731.600000
Num Accesses - Session Mean & Stddev: 21810873.160000,66.446779
Num Phases Traversed: 16399252.000000
Num Phases Traversed - Session Mean & Stddev: 163992.520000,0.499600
Num Path Iterations: 16399252.000000
Num Path Iterations - Session Mean & Stddev: 163992.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2318200000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2191623044.000000
Throughput: 219162304.400000
Num Accesses - Session Mean & Stddev: 21916230.440000,62.041328
Num Phases Traversed: 16478468.000000
Num Phases Traversed - Session Mean & Stddev: 164784.680000,0.466476
Num Path Iterations: 16478468.000000
Num Path Iterations - Session Mean & Stddev: 164784.680000,0.466476
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7b62600000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2191777856.000000
Throughput: 219177785.600000
Num Accesses - Session Mean & Stddev: 21917778.560000,62.041328
Num Phases Traversed: 16479632.000000
Num Phases Traversed - Session Mean & Stddev: 164796.320000,0.466476
Num Path Iterations: 16479632.000000
Num Path Iterations - Session Mean & Stddev: 164796.320000,0.466476
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe704600000 of size 12288 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2164638142.000000
Throughput: 216463814.200000
Num Accesses - Session Mean & Stddev: 21646381.420000,58.338354
Num Phases Traversed: 16275574.000000
Num Phases Traversed - Session Mean & Stddev: 162755.740000,0.438634
Num Path Iterations: 16275574.000000
Num Path Iterations - Session Mean & Stddev: 162755.740000,0.438634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fabe9600000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2156328302.000000
Throughput: 215632830.200000
Num Accesses - Session Mean & Stddev: 21563283.020000,31.585750
Num Phases Traversed: 16213094.000000
Num Phases Traversed - Session Mean & Stddev: 162130.940000,0.237487
Num Path Iterations: 16213094.000000
Num Path Iterations - Session Mean & Stddev: 162130.940000,0.237487
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7b25600000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2187294559.000000
Throughput: 218729455.900000
Num Accesses - Session Mean & Stddev: 21872945.590000,55.970724
Num Phases Traversed: 16445923.000000
Num Phases Traversed - Session Mean & Stddev: 164459.230000,0.420833
Num Path Iterations: 16445923.000000
Num Path Iterations - Session Mean & Stddev: 164459.230000,0.420833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4401a00000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2193245112.000000
Throughput: 219324511.200000
Num Accesses - Session Mean & Stddev: 21932451.120000,63.840000
Num Phases Traversed: 16490664.000000
Num Phases Traversed - Session Mean & Stddev: 164906.640000,0.480000
Num Path Iterations: 16490664.000000
Num Path Iterations - Session Mean & Stddev: 164906.640000,0.480000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd3c5600000 of size 12288 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2159990856.000000
Throughput: 215999085.600000
Num Accesses - Session Mean & Stddev: 21599908.560000,64.829826
Num Phases Traversed: 16240632.000000
Num Phases Traversed - Session Mean & Stddev: 162406.320000,0.487442
Num Path Iterations: 16240632.000000
Num Path Iterations - Session Mean & Stddev: 162406.320000,0.487442
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe84ae00000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2161809099.000000
Throughput: 216180909.900000
Num Accesses - Session Mean & Stddev: 21618090.990000,22.688100
Num Phases Traversed: 16254303.000000
Num Phases Traversed - Session Mean & Stddev: 162543.030000,0.170587
Num Path Iterations: 16254303.000000
Num Path Iterations - Session Mean & Stddev: 162543.030000,0.170587
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6c3b400000 of size 12288 bytes.
[1.267s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2168221162.000000
Throughput: 216822116.200000
Num Accesses - Session Mean & Stddev: 21682211.620000,46.149275
Num Phases Traversed: 16302514.000000
Num Phases Traversed - Session Mean & Stddev: 163025.140000,0.346987
Num Path Iterations: 16302514.000000
Num Path Iterations - Session Mean & Stddev: 163025.140000,0.346987
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8c39200000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2174060261.000000
Throughput: 217406026.100000
Num Accesses - Session Mean & Stddev: 21740602.610000,49.959162
Num Phases Traversed: 16346417.000000
Num Phases Traversed - Session Mean & Stddev: 163464.170000,0.375633
Num Path Iterations: 16346417.000000
Num Path Iterations - Session Mean & Stddev: 163464.170000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0992c00000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2166767073.000000
Throughput: 216676707.300000
Num Accesses - Session Mean & Stddev: 21667670.730000,52.176020
Num Phases Traversed: 16291581.000000
Num Phases Traversed - Session Mean & Stddev: 162915.810000,0.392301
Num Path Iterations: 16291581.000000
Num Path Iterations - Session Mean & Stddev: 162915.810000,0.392301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f11f9a00000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2159190861.000000
Throughput: 215919086.100000
Num Accesses - Session Mean & Stddev: 21591908.610000,49.959162
Num Phases Traversed: 16234617.000000
Num Phases Traversed - Session Mean & Stddev: 162346.170000,0.375633
Num Path Iterations: 16234617.000000
Num Path Iterations - Session Mean & Stddev: 162346.170000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2050a00000 of size 12288 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2185273624.000000
Throughput: 218527362.400000
Num Accesses - Session Mean & Stddev: 21852736.240000,59.716852
Num Phases Traversed: 16430728.000000
Num Phases Traversed - Session Mean & Stddev: 164307.280000,0.448999
Num Path Iterations: 16430728.000000
Num Path Iterations - Session Mean & Stddev: 164307.280000,0.448999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbe03000000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2178278223.000000
Throughput: 217827822.300000
Num Accesses - Session Mean & Stddev: 21782782.230000,61.511601
Num Phases Traversed: 16378131.000000
Num Phases Traversed - Session Mean & Stddev: 163781.310000,0.462493
Num Path Iterations: 16378131.000000
Num Path Iterations - Session Mean & Stddev: 163781.310000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f04b6600000 of size 12288 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2138714314.000000
Throughput: 213871431.400000
Num Accesses - Session Mean & Stddev: 21387143.140000,65.643281
Num Phases Traversed: 16080658.000000
Num Phases Traversed - Session Mean & Stddev: 160806.580000,0.493559
Num Path Iterations: 16080658.000000
Num Path Iterations - Session Mean & Stddev: 160806.580000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,1 0/96,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,1 0/96,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fda56000000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2179617134.000000
Throughput: 217961713.400000
Num Accesses - Session Mean & Stddev: 21796171.340000,26.466666
Num Phases Traversed: 16388198.000000
Num Phases Traversed - Session Mean & Stddev: 163881.980000,0.198997
Num Path Iterations: 16388198.000000
Num Path Iterations - Session Mean & Stddev: 163881.980000,0.198997
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff2afa00000 of size 40960 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2845497160.000000
Throughput: 284549716.000000
Num Accesses - Session Mean & Stddev: 28454971.600000,142.800000
Num Phases Traversed: 7970680.000000
Num Phases Traversed - Session Mean & Stddev: 79706.800000,0.400000
Num Path Iterations: 7970680.000000
Num Path Iterations - Session Mean & Stddev: 79706.800000,0.400000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f31efa00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2827922050.000000
Throughput: 282792205.000000
Num Accesses - Session Mean & Stddev: 28279220.500000,178.500000
Num Phases Traversed: 7921450.000000
Num Phases Traversed - Session Mean & Stddev: 79214.500000,0.500000
Num Path Iterations: 7921450.000000
Num Path Iterations - Session Mean & Stddev: 79214.500000,0.500000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f96f2800000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2840939341.000000
Throughput: 284093934.100000
Num Accesses - Session Mean & Stddev: 28409393.410000,120.060326
Num Phases Traversed: 7957913.000000
Num Phases Traversed - Session Mean & Stddev: 79579.130000,0.336303
Num Path Iterations: 7957913.000000
Num Path Iterations - Session Mean & Stddev: 79579.130000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff220600000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2833184944.000000
Throughput: 283318494.400000
Num Accesses - Session Mean & Stddev: 28331849.440000,96.851672
Num Phases Traversed: 7936192.000000
Num Phases Traversed - Session Mean & Stddev: 79361.920000,0.271293
Num Path Iterations: 7936192.000000
Num Path Iterations - Session Mean & Stddev: 79361.920000,0.271293
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa024400000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2839115785.000000
Throughput: 283911578.500000
Num Accesses - Session Mean & Stddev: 28391157.850000,77.806346
Num Phases Traversed: 7952805.000000
Num Phases Traversed - Session Mean & Stddev: 79528.050000,0.217945
Num Path Iterations: 7952805.000000
Num Path Iterations - Session Mean & Stddev: 79528.050000,0.217945
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f97ec400000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2843578642.000000
Throughput: 284357864.200000
Num Accesses - Session Mean & Stddev: 28435786.420000,110.842607
Num Phases Traversed: 7965306.000000
Num Phases Traversed - Session Mean & Stddev: 79653.060000,0.310483
Num Path Iterations: 7965306.000000
Num Path Iterations - Session Mean & Stddev: 79653.060000,0.310483
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0685400000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2835582199.000000
Throughput: 283558219.900000
Num Accesses - Session Mean & Stddev: 28355821.990000,115.736295
Num Phases Traversed: 7942907.000000
Num Phases Traversed - Session Mean & Stddev: 79429.070000,0.324191
Num Path Iterations: 7942907.000000
Num Path Iterations - Session Mean & Stddev: 79429.070000,0.324191
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcface00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2823619129.000000
Throughput: 282361912.900000
Num Accesses - Session Mean & Stddev: 28236191.290000,117.918132
Num Phases Traversed: 7909397.000000
Num Phases Traversed - Session Mean & Stddev: 79093.970000,0.330303
Num Path Iterations: 7909397.000000
Num Path Iterations - Session Mean & Stddev: 79093.970000,0.330303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f99c0600000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2830269682.000000
Throughput: 283026968.200000
Num Accesses - Session Mean & Stddev: 28302696.820000,156.592425
Num Phases Traversed: 7928026.000000
Num Phases Traversed - Session Mean & Stddev: 79280.260000,0.438634
Num Path Iterations: 7928026.000000
Num Path Iterations - Session Mean & Stddev: 79280.260000,0.438634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2334200000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2810305885.000000
Throughput: 281030588.500000
Num Accesses - Session Mean & Stddev: 28103058.850000,117.050278
Num Phases Traversed: 7872105.000000
Num Phases Traversed - Session Mean & Stddev: 78721.050000,0.327872
Num Path Iterations: 7872105.000000
Num Path Iterations - Session Mean & Stddev: 78721.050000,0.327872
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8582e00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2836027378.000000
Throughput: 283602737.800000
Num Accesses - Session Mean & Stddev: 28360273.780000,177.927883
Num Phases Traversed: 7944154.000000
Num Phases Traversed - Session Mean & Stddev: 79441.540000,0.498397
Num Path Iterations: 7944154.000000
Num Path Iterations - Session Mean & Stddev: 79441.540000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f54d6400000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2842013911.000000
Throughput: 284201391.100000
Num Accesses - Session Mean & Stddev: 28420139.110000,150.237205
Num Phases Traversed: 7960923.000000
Num Phases Traversed - Session Mean & Stddev: 79609.230000,0.420833
Num Path Iterations: 7960923.000000
Num Path Iterations - Session Mean & Stddev: 79609.230000,0.420833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5f91800000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2852806378.000000
Throughput: 285280637.800000
Num Accesses - Session Mean & Stddev: 28528063.780000,177.927883
Num Phases Traversed: 7991154.000000
Num Phases Traversed - Session Mean & Stddev: 79911.540000,0.498397
Num Path Iterations: 7991154.000000
Num Path Iterations - Session Mean & Stddev: 79911.540000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffa59800000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2838468187.000000
Throughput: 283846818.700000
Num Accesses - Session Mean & Stddev: 28384681.870000,113.960752
Num Phases Traversed: 7950991.000000
Num Phases Traversed - Session Mean & Stddev: 79509.910000,0.319218
Num Path Iterations: 7950991.000000
Num Path Iterations - Session Mean & Stddev: 79509.910000,0.319218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5525a00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2851676473.000000
Throughput: 285167647.300000
Num Accesses - Session Mean & Stddev: 28516764.730000,111.701643
Num Phases Traversed: 7987989.000000
Num Phases Traversed - Session Mean & Stddev: 79879.890000,0.312890
Num Path Iterations: 7987989.000000
Num Path Iterations - Session Mean & Stddev: 79879.890000,0.312890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f34b6200000 of size 40960 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2834282005.000000
Throughput: 283428200.500000
Num Accesses - Session Mean & Stddev: 28342820.050000,170.278147
Num Phases Traversed: 7939265.000000
Num Phases Traversed - Session Mean & Stddev: 79392.650000,0.476970
Num Path Iterations: 7939265.000000
Num Path Iterations - Session Mean & Stddev: 79392.650000,0.476970
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5e72800000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2820986254.000000
Throughput: 282098625.400000
Num Accesses - Session Mean & Stddev: 28209862.540000,147.885930
Num Phases Traversed: 7902022.000000
Num Phases Traversed - Session Mean & Stddev: 79020.220000,0.414246
Num Path Iterations: 7902022.000000
Num Path Iterations - Session Mean & Stddev: 79020.220000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe95ec00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2814921538.000000
Throughput: 281492153.800000
Num Accesses - Session Mean & Stddev: 28149215.380000,169.114031
Num Phases Traversed: 7885034.000000
Num Phases Traversed - Session Mean & Stddev: 78850.340000,0.473709
Num Path Iterations: 7885034.000000
Num Path Iterations - Session Mean & Stddev: 78850.340000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5ba9a00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2810450827.000000
Throughput: 281045082.700000
Num Accesses - Session Mean & Stddev: 28104508.270000,111.701643
Num Phases Traversed: 7872511.000000
Num Phases Traversed - Session Mean & Stddev: 78725.110000,0.312890
Num Path Iterations: 7872511.000000
Num Path Iterations - Session Mean & Stddev: 78725.110000,0.312890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f52adc00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2824494850.000000
Throughput: 282449485.000000
Num Accesses - Session Mean & Stddev: 28244948.500000,178.500000
Num Phases Traversed: 7911850.000000
Num Phases Traversed - Session Mean & Stddev: 79118.500000,0.500000
Num Path Iterations: 7911850.000000
Num Path Iterations - Session Mean & Stddev: 79118.500000,0.500000
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0a00000000 of size 3145728 bytes.
[1.540s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829285909.000000
Throughput: 82928590.900000
Num Accesses - Session Mean & Stddev: 8292859.090000,16785.632921
Num Phases Traversed: 33793.000000
Num Phases Traversed - Session Mean & Stddev: 337.930000,0.681982
Num Path Iterations: 33793.000000
Num Path Iterations - Session Mean & Stddev: 337.930000,0.681982
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff4c0000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 828990553.000000
Throughput: 82899055.300000
Num Accesses - Session Mean & Stddev: 8289905.530000,16582.295232
Num Phases Traversed: 33781.000000
Num Phases Traversed - Session Mean & Stddev: 337.810000,0.673721
Num Path Iterations: 33781.000000
Num Path Iterations - Session Mean & Stddev: 337.810000,0.673721
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff900000000 of size 3145728 bytes.
[1.496s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 826726157.000000
Throughput: 82672615.700000
Num Accesses - Session Mean & Stddev: 8267261.570000,17015.043626
Num Phases Traversed: 33689.000000
Num Phases Traversed - Session Mean & Stddev: 336.890000,0.691303
Num Path Iterations: 33689.000000
Num Path Iterations - Session Mean & Stddev: 336.890000,0.691303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd980000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829335135.000000
Throughput: 82933513.500000
Num Accesses - Session Mean & Stddev: 8293351.350000,17185.092010
Num Phases Traversed: 33795.000000
Num Phases Traversed - Session Mean & Stddev: 337.950000,0.698212
Num Path Iterations: 33795.000000
Num Path Iterations - Session Mean & Stddev: 337.950000,0.698212
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe9c0000000 of size 3145728 bytes.
[1.496s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 826086219.000000
Throughput: 82608621.900000
Num Accesses - Session Mean & Stddev: 8260862.190000,15431.781789
Num Phases Traversed: 33663.000000
Num Phases Traversed - Session Mean & Stddev: 336.630000,0.626977
Num Path Iterations: 33663.000000
Num Path Iterations - Session Mean & Stddev: 336.630000,0.626977
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1480000000 of size 3145728 bytes.
[1.500s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 826824609.000000
Throughput: 82682460.900000
Num Accesses - Session Mean & Stddev: 8268246.090000,17142.738053
Num Phases Traversed: 33693.000000
Num Phases Traversed - Session Mean & Stddev: 336.930000,0.696491
Num Path Iterations: 33693.000000
Num Path Iterations - Session Mean & Stddev: 336.930000,0.696491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4080000000 of size 3145728 bytes.
[1.496s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 828990553.000000
Throughput: 82899055.300000
Num Accesses - Session Mean & Stddev: 8289905.530000,16582.295232
Num Phases Traversed: 33781.000000
Num Phases Traversed - Session Mean & Stddev: 337.810000,0.673721
Num Path Iterations: 33781.000000
Num Path Iterations - Session Mean & Stddev: 337.810000,0.673721
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc200000000 of size 3145728 bytes.
[1.527s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829212070.000000
Throughput: 82921207.000000
Num Accesses - Session Mean & Stddev: 8292120.700000,16139.823440
Num Phases Traversed: 33790.000000
Num Phases Traversed - Session Mean & Stddev: 337.900000,0.655744
Num Path Iterations: 33790.000000
Num Path Iterations - Session Mean & Stddev: 337.900000,0.655744
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa980000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829359748.000000
Throughput: 82935974.800000
Num Accesses - Session Mean & Stddev: 8293597.480000,16664.291587
Num Phases Traversed: 33796.000000
Num Phases Traversed - Session Mean & Stddev: 337.960000,0.677052
Num Path Iterations: 33796.000000
Num Path Iterations - Session Mean & Stddev: 337.960000,0.677052
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0c40000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829236683.000000
Throughput: 82923668.300000
Num Accesses - Session Mean & Stddev: 8292366.830000,16361.629440
Num Phases Traversed: 33791.000000
Num Phases Traversed - Session Mean & Stddev: 337.910000,0.664756
Num Path Iterations: 33791.000000
Num Path Iterations - Session Mean & Stddev: 337.910000,0.664756
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff340000000 of size 3145728 bytes.
[1.496s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 828055259.000000
Throughput: 82805525.900000
Num Accesses - Session Mean & Stddev: 8280552.590000,13595.201709
Num Phases Traversed: 33743.000000
Num Phases Traversed - Session Mean & Stddev: 337.430000,0.552359
Num Path Iterations: 33743.000000
Num Path Iterations - Session Mean & Stddev: 337.430000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9400000000 of size 3145728 bytes.
[1.529s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829408974.000000
Throughput: 82940897.400000
Num Accesses - Session Mean & Stddev: 8294089.740000,17045.279969
Num Phases Traversed: 33798.000000
Num Phases Traversed - Session Mean & Stddev: 337.980000,0.692532
Num Path Iterations: 33798.000000
Num Path Iterations - Session Mean & Stddev: 337.980000,0.692532
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feb80000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829285909.000000
Throughput: 82928590.900000
Num Accesses - Session Mean & Stddev: 8292859.090000,16420.763599
Num Phases Traversed: 33793.000000
Num Phases Traversed - Session Mean & Stddev: 337.930000,0.667158
Num Path Iterations: 33793.000000
Num Path Iterations - Session Mean & Stddev: 337.930000,0.667158
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4900000000 of size 3145728 bytes.
[1.531s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829285909.000000
Throughput: 82928590.900000
Num Accesses - Session Mean & Stddev: 8292859.090000,16047.600500
Num Phases Traversed: 33793.000000
Num Phases Traversed - Session Mean & Stddev: 337.930000,0.651997
Num Path Iterations: 33793.000000
Num Path Iterations - Session Mean & Stddev: 337.930000,0.651997
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f75c0000000 of size 3145728 bytes.
[1.496s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 827464547.000000
Throughput: 82746454.700000
Num Accesses - Session Mean & Stddev: 8274645.470000,16582.295232
Num Phases Traversed: 33719.000000
Num Phases Traversed - Session Mean & Stddev: 337.190000,0.673721
Num Path Iterations: 33719.000000
Num Path Iterations - Session Mean & Stddev: 337.190000,0.673721
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc440000000 of size 3145728 bytes.
[1.524s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829359748.000000
Throughput: 82935974.800000
Num Accesses - Session Mean & Stddev: 8293597.480000,17023.942243
Num Phases Traversed: 33796.000000
Num Phases Traversed - Session Mean & Stddev: 337.960000,0.691665
Num Path Iterations: 33796.000000
Num Path Iterations - Session Mean & Stddev: 337.960000,0.691665
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efc00000000 of size 3145728 bytes.
[1.528s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829187457.000000
Throughput: 82918745.700000
Num Accesses - Session Mean & Stddev: 8291874.570000,16287.409826
Num Phases Traversed: 33789.000000
Num Phases Traversed - Session Mean & Stddev: 337.890000,0.661740
Num Path Iterations: 33789.000000
Num Path Iterations - Session Mean & Stddev: 337.890000,0.661740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa700000000 of size 3145728 bytes.
[1.484s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 827956807.000000
Throughput: 82795680.700000
Num Accesses - Session Mean & Stddev: 8279568.070000,14307.331755
Num Phases Traversed: 33739.000000
Num Phases Traversed - Session Mean & Stddev: 337.390000,0.581292
Num Path Iterations: 33739.000000
Num Path Iterations - Session Mean & Stddev: 337.390000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff200000000 of size 3145728 bytes.
[1.496s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 828399841.000000
Throughput: 82839984.100000
Num Accesses - Session Mean & Stddev: 8283998.410000,13595.201709
Num Phases Traversed: 33757.000000
Num Phases Traversed - Session Mean & Stddev: 337.570000,0.552359
Num Path Iterations: 33757.000000
Num Path Iterations - Session Mean & Stddev: 337.570000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,0.01 0/24576,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,0.01 0/24576,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1b00000000 of size 3145728 bytes.
[1.527s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829408974.000000
Throughput: 82940897.400000
Num Accesses - Session Mean & Stddev: 8294089.740000,17045.279969
Num Phases Traversed: 33798.000000
Num Phases Traversed - Session Mean & Stddev: 337.980000,0.692532
Num Path Iterations: 33798.000000
Num Path Iterations - Session Mean & Stddev: 337.980000,0.692532
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5362400000 of size 53248 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2863582069.000000
Throughput: 286358206.900000
Num Accesses - Session Mean & Stddev: 28635820.690000,285.461896
Num Phases Traversed: 6321473.000000
Num Phases Traversed - Session Mean & Stddev: 63214.730000,0.630159
Num Path Iterations: 6321473.000000
Num Path Iterations - Session Mean & Stddev: 63214.730000,0.630159
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3a80a00000 of size 53248 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2838159256.000000
Throughput: 283815925.600000
Num Accesses - Session Mean & Stddev: 28381592.560000,243.779668
Num Phases Traversed: 6265352.000000
Num Phases Traversed - Session Mean & Stddev: 62653.520000,0.538145
Num Path Iterations: 6265352.000000
Num Path Iterations - Session Mean & Stddev: 62653.520000,0.538145
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4947a00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2836066396.000000
Throughput: 283606639.600000
Num Accesses - Session Mean & Stddev: 28360663.960000,270.892485
Num Phases Traversed: 6260732.000000
Num Phases Traversed - Session Mean & Stddev: 62607.320000,0.597997
Num Path Iterations: 6260732.000000
Num Path Iterations - Session Mean & Stddev: 62607.320000,0.597997
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efcfbc00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2830468675.000000
Throughput: 283046867.500000
Num Accesses - Session Mean & Stddev: 28304686.750000,289.175842
Num Phases Traversed: 6248375.000000
Num Phases Traversed - Session Mean & Stddev: 62483.750000,0.638357
Num Path Iterations: 6248375.000000
Num Path Iterations - Session Mean & Stddev: 62483.750000,0.638357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6999400000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2822460994.000000
Throughput: 282246099.400000
Num Accesses - Session Mean & Stddev: 28224609.940000,307.105937
Num Phases Traversed: 6230698.000000
Num Phases Traversed - Session Mean & Stddev: 62306.980000,0.677938
Num Path Iterations: 6230698.000000
Num Path Iterations - Session Mean & Stddev: 62306.980000,0.677938
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9ff2a00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2827837198.000000
Throughput: 282783719.800000
Num Accesses - Session Mean & Stddev: 28278371.980000,265.845782
Num Phases Traversed: 6242566.000000
Num Phases Traversed - Session Mean & Stddev: 62425.660000,0.586856
Num Path Iterations: 6242566.000000
Num Path Iterations - Session Mean & Stddev: 62425.660000,0.586856
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdccfc00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2843751994.000000
Throughput: 284375199.400000
Num Accesses - Session Mean & Stddev: 28437519.940000,307.105937
Num Phases Traversed: 6277698.000000
Num Phases Traversed - Session Mean & Stddev: 62776.980000,0.677938
Num Path Iterations: 6277698.000000
Num Path Iterations - Session Mean & Stddev: 62776.980000,0.677938
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa3c8400000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2847176674.000000
Throughput: 284717667.400000
Num Accesses - Session Mean & Stddev: 28471766.740000,241.241108
Num Phases Traversed: 6285258.000000
Num Phases Traversed - Session Mean & Stddev: 62852.580000,0.532541
Num Path Iterations: 6285258.000000
Num Path Iterations - Session Mean & Stddev: 62852.580000,0.532541
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1ec3c00000 of size 53248 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2841173518.000000
Throughput: 284117351.800000
Num Accesses - Session Mean & Stddev: 28411735.180000,306.034945
Num Phases Traversed: 6272006.000000
Num Phases Traversed - Session Mean & Stddev: 62720.060000,0.675574
Num Path Iterations: 6272006.000000
Num Path Iterations - Session Mean & Stddev: 62720.060000,0.675574
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2aeaa00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2821752049.000000
Throughput: 282175204.900000
Num Accesses - Session Mean & Stddev: 28217520.490000,272.214933
Num Phases Traversed: 6229133.000000
Num Phases Traversed - Session Mean & Stddev: 62291.330000,0.600916
Num Path Iterations: 6229133.000000
Num Path Iterations - Session Mean & Stddev: 62291.330000,0.600916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe2b9200000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2864289655.000000
Throughput: 286428965.500000
Num Accesses - Session Mean & Stddev: 28642896.550000,267.039562
Num Phases Traversed: 6323035.000000
Num Phases Traversed - Session Mean & Stddev: 63230.350000,0.589491
Num Path Iterations: 6323035.000000
Num Path Iterations - Session Mean & Stddev: 63230.350000,0.589491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f22f9000000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2829244216.000000
Throughput: 282924421.600000
Num Accesses - Session Mean & Stddev: 28292442.160000,279.835227
Num Phases Traversed: 6245672.000000
Num Phases Traversed - Session Mean & Stddev: 62456.720000,0.617738
Num Path Iterations: 6245672.000000
Num Path Iterations - Session Mean & Stddev: 62456.720000,0.617738
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f983c800000 of size 53248 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2818203247.000000
Throughput: 281820324.700000
Num Accesses - Session Mean & Stddev: 28182032.470000,303.847872
Num Phases Traversed: 6221299.000000
Num Phases Traversed - Session Mean & Stddev: 62212.990000,0.670746
Num Path Iterations: 6221299.000000
Num Path Iterations - Session Mean & Stddev: 62212.990000,0.670746
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3ebb200000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2824568803.000000
Throughput: 282456880.300000
Num Accesses - Session Mean & Stddev: 28245688.030000,243.905902
Num Phases Traversed: 6235351.000000
Num Phases Traversed - Session Mean & Stddev: 62353.510000,0.538424
Num Path Iterations: 6235351.000000
Num Path Iterations - Session Mean & Stddev: 62353.510000,0.538424
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f472e400000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2821191235.000000
Throughput: 282119123.500000
Num Accesses - Session Mean & Stddev: 28211912.350000,303.036347
Num Phases Traversed: 6227895.000000
Num Phases Traversed - Session Mean & Stddev: 62278.950000,0.668954
Num Path Iterations: 6227895.000000
Num Path Iterations - Session Mean & Stddev: 62278.950000,0.668954
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f62a5200000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2847467500.000000
Throughput: 284746750.000000
Num Accesses - Session Mean & Stddev: 28474675.000000,307.239548
Num Phases Traversed: 6285900.000000
Num Phases Traversed - Session Mean & Stddev: 62859.000000,0.678233
Num Path Iterations: 6285900.000000
Num Path Iterations - Session Mean & Stddev: 62859.000000,0.678233
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f020d400000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2828446030.000000
Throughput: 282844603.000000
Num Accesses - Session Mean & Stddev: 28284460.300000,297.051965
Num Phases Traversed: 6243910.000000
Num Phases Traversed - Session Mean & Stddev: 62439.100000,0.655744
Num Path Iterations: 6243910.000000
Num Path Iterations - Session Mean & Stddev: 62439.100000,0.655744
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1e08200000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2845595704.000000
Throughput: 284559570.400000
Num Accesses - Session Mean & Stddev: 28455957.040000,270.892485
Num Phases Traversed: 6281768.000000
Num Phases Traversed - Session Mean & Stddev: 62817.680000,0.597997
Num Path Iterations: 6281768.000000
Num Path Iterations - Session Mean & Stddev: 62817.680000,0.597997
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb7c5e00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2856876310.000000
Throughput: 285687631.000000
Num Accesses - Session Mean & Stddev: 28568763.100000,275.549143
Num Phases Traversed: 6306670.000000
Num Phases Traversed - Session Mean & Stddev: 63066.700000,0.608276
Num Path Iterations: 6306670.000000
Num Path Iterations - Session Mean & Stddev: 63066.700000,0.608276
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd83d600000 of size 53248 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2844883135.000000
Throughput: 284488313.500000
Num Accesses - Session Mean & Stddev: 28448831.350000,309.734092
Num Phases Traversed: 6280195.000000
Num Phases Traversed - Session Mean & Stddev: 62801.950000,0.683740
Num Path Iterations: 6280195.000000
Num Path Iterations - Session Mean & Stddev: 62801.950000,0.683740
+ set +x

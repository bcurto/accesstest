++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f02afc00000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2359228840.000000
Throughput: 235922884.000000
Num Accesses - Session Mean & Stddev: 23592288.400000,81.903846
Num Phases Traversed: 14298456.000000
Num Phases Traversed - Session Mean & Stddev: 142984.560000,0.496387
Num Path Iterations: 14298456.000000
Num Path Iterations - Session Mean & Stddev: 142984.560000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe7ac200000 of size 16384 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2379931390.000000
Throughput: 237993139.000000
Num Accesses - Session Mean & Stddev: 23799313.900000,76.043343
Num Phases Traversed: 14423926.000000
Num Phases Traversed - Session Mean & Stddev: 144239.260000,0.460869
Num Path Iterations: 14423926.000000
Num Path Iterations - Session Mean & Stddev: 144239.260000,0.460869
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f476c200000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2354778790.000000
Throughput: 235477879.000000
Num Accesses - Session Mean & Stddev: 23547787.900000,57.252860
Num Phases Traversed: 14271486.000000
Num Phases Traversed - Session Mean & Stddev: 142714.860000,0.346987
Num Path Iterations: 14271486.000000
Num Path Iterations - Session Mean & Stddev: 142714.860000,0.346987
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8a97600000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2339374390.000000
Throughput: 233937439.000000
Num Accesses - Session Mean & Stddev: 23393743.900000,72.374650
Num Phases Traversed: 14178126.000000
Num Phases Traversed - Session Mean & Stddev: 141781.260000,0.438634
Num Path Iterations: 14178126.000000
Num Path Iterations - Session Mean & Stddev: 141781.260000,0.438634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7510600000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2326403905.000000
Throughput: 232640390.500000
Num Accesses - Session Mean & Stddev: 23264039.050000,61.979412
Num Phases Traversed: 14099517.000000
Num Phases Traversed - Session Mean & Stddev: 140995.170000,0.375633
Num Path Iterations: 14099517.000000
Num Path Iterations - Session Mean & Stddev: 140995.170000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f60c2c00000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2346544465.000000
Throughput: 234654446.500000
Num Accesses - Session Mean & Stddev: 23465444.650000,64.729649
Num Phases Traversed: 14221581.000000
Num Phases Traversed - Session Mean & Stddev: 142215.810000,0.392301
Num Path Iterations: 14221581.000000
Num Path Iterations - Session Mean & Stddev: 142215.810000,0.392301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6004800000 of size 16384 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2356078990.000000
Throughput: 235607899.000000
Num Accesses - Session Mean & Stddev: 23560789.900000,78.161947
Num Phases Traversed: 14279366.000000
Num Phases Traversed - Session Mean & Stddev: 142793.660000,0.473709
Num Path Iterations: 14279366.000000
Num Path Iterations - Session Mean & Stddev: 142793.660000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe099600000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2320245610.000000
Throughput: 232024561.000000
Num Accesses - Session Mean & Stddev: 23202456.100000,39.185329
Num Phases Traversed: 14062194.000000
Num Phases Traversed - Session Mean & Stddev: 140621.940000,0.237487
Num Path Iterations: 14062194.000000
Num Path Iterations - Session Mean & Stddev: 140621.940000,0.237487
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7b86800000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2322623590.000000
Throughput: 232262359.000000
Num Accesses - Session Mean & Stddev: 23226235.900000,39.185329
Num Phases Traversed: 14076606.000000
Num Phases Traversed - Session Mean & Stddev: 140766.060000,0.237487
Num Path Iterations: 14076606.000000
Num Path Iterations - Session Mean & Stddev: 140766.060000,0.237487
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f80cb000000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2367170785.000000
Throughput: 236717078.500000
Num Accesses - Session Mean & Stddev: 23671707.850000,51.626810
Num Phases Traversed: 14346589.000000
Num Phases Traversed - Session Mean & Stddev: 143465.890000,0.312890
Num Path Iterations: 14346589.000000
Num Path Iterations - Session Mean & Stddev: 143465.890000,0.312890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7309200000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2363886460.000000
Throughput: 236388646.000000
Num Accesses - Session Mean & Stddev: 23638864.600000,60.489999
Num Phases Traversed: 14326684.000000
Num Phases Traversed - Session Mean & Stddev: 143266.840000,0.366606
Num Path Iterations: 14326684.000000
Num Path Iterations - Session Mean & Stddev: 143266.840000,0.366606
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0e63600000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2354800735.000000
Throughput: 235480073.500000
Num Accesses - Session Mean & Stddev: 23548007.350000,64.729649
Num Phases Traversed: 14271619.000000
Num Phases Traversed - Session Mean & Stddev: 142716.190000,0.392301
Num Path Iterations: 14271619.000000
Num Path Iterations - Session Mean & Stddev: 142716.190000,0.392301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8a67c00000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2355987745.000000
Throughput: 235598774.500000
Num Accesses - Session Mean & Stddev: 23559877.450000,55.490067
Num Phases Traversed: 14278813.000000
Num Phases Traversed - Session Mean & Stddev: 142788.130000,0.336303
Num Path Iterations: 14278813.000000
Num Path Iterations - Session Mean & Stddev: 142788.130000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f683d600000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2352603430.000000
Throughput: 235260343.000000
Num Accesses - Session Mean & Stddev: 23526034.300000,23.100000
Num Phases Traversed: 14258302.000000
Num Phases Traversed - Session Mean & Stddev: 142583.020000,0.140000
Num Path Iterations: 14258302.000000
Num Path Iterations - Session Mean & Stddev: 142583.020000,0.140000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3973e00000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2327642890.000000
Throughput: 232764289.000000
Num Accesses - Session Mean & Stddev: 23276428.900000,72.374650
Num Phases Traversed: 14107026.000000
Num Phases Traversed - Session Mean & Stddev: 141070.260000,0.438634
Num Path Iterations: 14107026.000000
Num Path Iterations - Session Mean & Stddev: 141070.260000,0.438634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f44b0800000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2339763790.000000
Throughput: 233976379.000000
Num Accesses - Session Mean & Stddev: 23397637.900000,57.252860
Num Phases Traversed: 14180486.000000
Num Phases Traversed - Session Mean & Stddev: 141804.860000,0.346987
Num Path Iterations: 14180486.000000
Num Path Iterations - Session Mean & Stddev: 141804.860000,0.346987
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f272ee00000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2356999525.000000
Throughput: 235699952.500000
Num Accesses - Session Mean & Stddev: 23569995.250000,82.086464
Num Phases Traversed: 14284945.000000
Num Phases Traversed - Session Mean & Stddev: 142849.450000,0.497494
Num Path Iterations: 14284945.000000
Num Path Iterations - Session Mean & Stddev: 142849.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f11b0a00000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2353014610.000000
Throughput: 235301461.000000
Num Accesses - Session Mean & Stddev: 23530146.100000,39.185329
Num Phases Traversed: 14260794.000000
Num Phases Traversed - Session Mean & Stddev: 142607.940000,0.237487
Num Path Iterations: 14260794.000000
Num Path Iterations - Session Mean & Stddev: 142607.940000,0.237487
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1abbc00000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2365119835.000000
Throughput: 236511983.500000
Num Accesses - Session Mean & Stddev: 23651198.350000,81.152495
Num Phases Traversed: 14334159.000000
Num Phases Traversed - Session Mean & Stddev: 143341.590000,0.491833
Num Path Iterations: 14334159.000000
Num Path Iterations - Session Mean & Stddev: 143341.590000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0.001 0/128,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0.001 0/128,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1b92a00000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2338688980.000000
Throughput: 233868898.000000
Num Accesses - Session Mean & Stddev: 23386889.800000,74.084816
Num Phases Traversed: 14173972.000000
Num Phases Traversed - Session Mean & Stddev: 141739.720000,0.448999
Num Path Iterations: 14173972.000000
Num Path Iterations - Session Mean & Stddev: 141739.720000,0.448999
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb895a00000 of size 61440 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2581219279.000000
Throughput: 258121927.900000
Num Accesses - Session Mean & Stddev: 25812192.790000,348.006675
Num Phases Traversed: 4992787.000000
Num Phases Traversed - Session Mean & Stddev: 49927.870000,0.673127
Num Path Iterations: 4992787.000000
Num Path Iterations - Session Mean & Stddev: 49927.870000,0.673127
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1ad3000000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2581549642.000000
Throughput: 258154964.200000
Num Accesses - Session Mean & Stddev: 25815496.420000,323.858308
Num Phases Traversed: 4993426.000000
Num Phases Traversed - Session Mean & Stddev: 49934.260000,0.626418
Num Path Iterations: 4993426.000000
Num Path Iterations - Session Mean & Stddev: 49934.260000,0.626418
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1e88800000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2582966739.000000
Throughput: 258296673.900000
Num Accesses - Session Mean & Stddev: 25829667.390000,310.673555
Num Phases Traversed: 4996167.000000
Num Phases Traversed - Session Mean & Stddev: 49961.670000,0.600916
Num Path Iterations: 4996167.000000
Num Path Iterations - Session Mean & Stddev: 49961.670000,0.600916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9aaf200000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2582663777.000000
Throughput: 258266377.700000
Num Accesses - Session Mean & Stddev: 25826637.770000,340.553516
Num Phases Traversed: 4995581.000000
Num Phases Traversed - Session Mean & Stddev: 49955.810000,0.658711
Num Path Iterations: 4995581.000000
Num Path Iterations - Session Mean & Stddev: 49955.810000,0.658711
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7917c00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2582456977.000000
Throughput: 258245697.700000
Num Accesses - Session Mean & Stddev: 25824569.770000,340.553516
Num Phases Traversed: 4995181.000000
Num Phases Traversed - Session Mean & Stddev: 49951.810000,0.658711
Num Path Iterations: 4995181.000000
Num Path Iterations - Session Mean & Stddev: 49951.810000,0.658711
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7962000000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2580289713.000000
Throughput: 258028971.300000
Num Accesses - Session Mean & Stddev: 25802897.130000,349.845156
Num Phases Traversed: 4990989.000000
Num Phases Traversed - Session Mean & Stddev: 49909.890000,0.676683
Num Path Iterations: 4990989.000000
Num Path Iterations - Session Mean & Stddev: 49909.890000,0.676683
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa2a8e00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2582309115.000000
Throughput: 258230911.500000
Num Accesses - Session Mean & Stddev: 25823091.150000,353.493433
Num Phases Traversed: 4994895.000000
Num Phases Traversed - Session Mean & Stddev: 49948.950000,0.683740
Num Path Iterations: 4994895.000000
Num Path Iterations - Session Mean & Stddev: 49948.950000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f105a600000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2581436419.000000
Throughput: 258143641.900000
Num Accesses - Session Mean & Stddev: 25814364.190000,352.584903
Num Phases Traversed: 4993207.000000
Num Phases Traversed - Session Mean & Stddev: 49932.070000,0.681982
Num Path Iterations: 4993207.000000
Num Path Iterations - Session Mean & Stddev: 49932.070000,0.681982
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa4bd000000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2580344515.000000
Throughput: 258034451.500000
Num Accesses - Session Mean & Stddev: 25803445.150000,353.493433
Num Phases Traversed: 4991095.000000
Num Phases Traversed - Session Mean & Stddev: 49910.950000,0.683740
Num Path Iterations: 4991095.000000
Num Path Iterations - Session Mean & Stddev: 49910.950000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0a57c00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2581922916.000000
Throughput: 258192291.600000
Num Accesses - Session Mean & Stddev: 25819229.160000,258.293117
Num Phases Traversed: 4994148.000000
Num Phases Traversed - Session Mean & Stddev: 49941.480000,0.499600
Num Path Iterations: 4994148.000000
Num Path Iterations - Session Mean & Stddev: 49941.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0788200000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2582441467.000000
Throughput: 258244146.700000
Num Accesses - Session Mean & Stddev: 25824414.670000,258.448295
Num Phases Traversed: 4995151.000000
Num Phases Traversed - Session Mean & Stddev: 49951.510000,0.499900
Num Path Iterations: 4995151.000000
Num Path Iterations - Session Mean & Stddev: 49951.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3150e00000 of size 61440 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2581904821.000000
Throughput: 258190482.100000
Num Accesses - Session Mean & Stddev: 25819048.210000,348.006675
Num Phases Traversed: 4994113.000000
Num Phases Traversed - Session Mean & Stddev: 49941.130000,0.673127
Num Path Iterations: 4994113.000000
Num Path Iterations - Session Mean & Stddev: 49941.130000,0.673127
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f50b7200000 of size 61440 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2581172749.000000
Throughput: 258117274.900000
Num Accesses - Session Mean & Stddev: 25811727.490000,354.097825
Num Phases Traversed: 4992697.000000
Num Phases Traversed - Session Mean & Stddev: 49926.970000,0.684909
Num Path Iterations: 4992697.000000
Num Path Iterations - Session Mean & Stddev: 49926.970000,0.684909
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7070a00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2580275237.000000
Throughput: 258027523.700000
Num Accesses - Session Mean & Stddev: 25802752.370000,291.498153
Num Phases Traversed: 4990961.000000
Num Phases Traversed - Session Mean & Stddev: 49909.610000,0.563826
Num Path Iterations: 4990961.000000
Num Path Iterations - Session Mean & Stddev: 49909.610000,0.563826
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4259a00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2581354733.000000
Throughput: 258135473.300000
Num Accesses - Session Mean & Stddev: 25813547.330000,258.448295
Num Phases Traversed: 4993049.000000
Num Phases Traversed - Session Mean & Stddev: 49930.490000,0.499900
Num Path Iterations: 4993049.000000
Num Path Iterations - Session Mean & Stddev: 49930.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0f4f000000 of size 61440 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2582188137.000000
Throughput: 258218813.700000
Num Accesses - Session Mean & Stddev: 25821881.370000,291.498153
Num Phases Traversed: 4994661.000000
Num Phases Traversed - Session Mean & Stddev: 49946.610000,0.563826
Num Path Iterations: 4994661.000000
Num Path Iterations - Session Mean & Stddev: 49946.610000,0.563826
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3e58400000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2583076343.000000
Throughput: 258307634.300000
Num Accesses - Session Mean & Stddev: 25830763.430000,337.399444
Num Phases Traversed: 4996379.000000
Num Phases Traversed - Session Mean & Stddev: 49963.790000,0.652610
Num Path Iterations: 4996379.000000
Num Path Iterations - Session Mean & Stddev: 49963.790000,0.652610
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8e8e800000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2582135403.000000
Throughput: 258213540.300000
Num Accesses - Session Mean & Stddev: 25821354.030000,284.067860
Num Phases Traversed: 4994559.000000
Num Phases Traversed - Session Mean & Stddev: 49945.590000,0.549454
Num Path Iterations: 4994559.000000
Num Path Iterations - Session Mean & Stddev: 49945.590000,0.549454
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1112a00000 of size 61440 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2581713531.000000
Throughput: 258171353.100000
Num Accesses - Session Mean & Stddev: 25817135.310000,276.050890
Num Phases Traversed: 4993743.000000
Num Phases Traversed - Session Mean & Stddev: 49937.430000,0.533948
Num Path Iterations: 4993743.000000
Num Path Iterations - Session Mean & Stddev: 49937.430000,0.533948
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0 0/480,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0 0/480,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc3c5200000 of size 61440 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2580037417.000000
Throughput: 258003741.700000
Num Accesses - Session Mean & Stddev: 25800374.170000,369.175651
Num Phases Traversed: 4990501.000000
Num Phases Traversed - Session Mean & Stddev: 49905.010000,0.714073
Num Path Iterations: 4990501.000000
Num Path Iterations - Session Mean & Stddev: 49905.010000,0.714073
+ set +x

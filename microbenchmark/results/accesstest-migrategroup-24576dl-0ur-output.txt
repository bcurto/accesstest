++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd875c00000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823285450.000000
Throughput: 82328545.000000
Num Accesses - Session Mean & Stddev: 8232854.500000,8535.451819
Num Phases Traversed: 66898.000000
Num Phases Traversed - Session Mean & Stddev: 668.980000,0.692532
Num Path Iterations: 66898.000000
Num Path Iterations - Session Mean & Stddev: 668.980000,0.692532
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6d86c00000 of size 1572864 bytes.
[1.328s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823248475.000000
Throughput: 82324847.500000
Num Accesses - Session Mean & Stddev: 8232484.750000,8427.092007
Num Phases Traversed: 66895.000000
Num Phases Traversed - Session Mean & Stddev: 668.950000,0.683740
Num Path Iterations: 66895.000000
Num Path Iterations - Session Mean & Stddev: 668.950000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fab8ae00000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824037275.000000
Throughput: 82403727.500000
Num Accesses - Session Mean & Stddev: 8240372.750000,6772.023936
Num Phases Traversed: 66959.000000
Num Phases Traversed - Session Mean & Stddev: 669.590000,0.549454
Num Path Iterations: 66959.000000
Num Path Iterations - Session Mean & Stddev: 669.590000,0.549454
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f246c200000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823063600.000000
Throughput: 82306360.000000
Num Accesses - Session Mean & Stddev: 8230636.000000,8175.480108
Num Phases Traversed: 66880.000000
Num Phases Traversed - Session Mean & Stddev: 668.800000,0.663325
Num Path Iterations: 66880.000000
Num Path Iterations - Session Mean & Stddev: 668.800000,0.663325
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9304c00000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823901700.000000
Throughput: 82390170.000000
Num Accesses - Session Mean & Stddev: 8239017.000000,6157.568026
Num Phases Traversed: 66948.000000
Num Phases Traversed - Session Mean & Stddev: 669.480000,0.499600
Num Path Iterations: 66948.000000
Num Path Iterations - Session Mean & Stddev: 669.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffbe1400000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823605900.000000
Throughput: 82360590.000000
Num Accesses - Session Mean & Stddev: 8236059.000000,8010.301867
Num Phases Traversed: 66924.000000
Num Phases Traversed - Session Mean & Stddev: 669.240000,0.649923
Num Path Iterations: 66924.000000
Num Path Iterations - Session Mean & Stddev: 669.240000,0.649923
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f253cc00000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823963325.000000
Throughput: 82396332.500000
Num Accesses - Session Mean & Stddev: 8239633.250000,6393.575188
Num Phases Traversed: 66953.000000
Num Phases Traversed - Session Mean & Stddev: 669.530000,0.518748
Num Path Iterations: 66953.000000
Num Path Iterations - Session Mean & Stddev: 669.530000,0.518748
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0699000000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 822545950.000000
Throughput: 82254595.000000
Num Accesses - Session Mean & Stddev: 8225459.500000,7139.994940
Num Phases Traversed: 66838.000000
Num Phases Traversed - Session Mean & Stddev: 668.380000,0.579310
Num Path Iterations: 66838.000000
Num Path Iterations - Session Mean & Stddev: 668.380000,0.579310
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa12ae00000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823075925.000000
Throughput: 82307592.500000
Num Accesses - Session Mean & Stddev: 8230759.250000,8118.611377
Num Phases Traversed: 66881.000000
Num Phases Traversed - Session Mean & Stddev: 668.810000,0.658711
Num Path Iterations: 66881.000000
Num Path Iterations - Session Mean & Stddev: 668.810000,0.658711
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcd63200000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823371725.000000
Throughput: 82337172.500000
Num Accesses - Session Mean & Stddev: 8233717.250000,8427.092007
Num Phases Traversed: 66905.000000
Num Phases Traversed - Session Mean & Stddev: 669.050000,0.683740
Num Path Iterations: 66905.000000
Num Path Iterations - Session Mean & Stddev: 669.050000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe720c00000 of size 1572864 bytes.
[1.331s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823322425.000000
Throughput: 82332242.500000
Num Accesses - Session Mean & Stddev: 8233224.250000,8626.619598
Num Phases Traversed: 66901.000000
Num Phases Traversed - Session Mean & Stddev: 669.010000,0.699929
Num Path Iterations: 66901.000000
Num Path Iterations - Session Mean & Stddev: 669.010000,0.699929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3e03200000 of size 1572864 bytes.
[1.328s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823507300.000000
Throughput: 82350730.000000
Num Accesses - Session Mean & Stddev: 8235073.000000,8308.183676
Num Phases Traversed: 66916.000000
Num Phases Traversed - Session Mean & Stddev: 669.160000,0.674092
Num Path Iterations: 66916.000000
Num Path Iterations - Session Mean & Stddev: 669.160000,0.674092
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5703a00000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 822878725.000000
Throughput: 82287872.500000
Num Accesses - Session Mean & Stddev: 8228787.250000,7471.634171
Num Phases Traversed: 66865.000000
Num Phases Traversed - Session Mean & Stddev: 668.650000,0.606218
Num Path Iterations: 66865.000000
Num Path Iterations - Session Mean & Stddev: 668.650000,0.606218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f29f1800000 of size 1572864 bytes.
[1.336s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823396375.000000
Throughput: 82339637.500000
Num Accesses - Session Mean & Stddev: 8233963.750000,8584.254114
Num Phases Traversed: 66907.000000
Num Phases Traversed - Session Mean & Stddev: 669.070000,0.696491
Num Path Iterations: 66907.000000
Num Path Iterations - Session Mean & Stddev: 669.070000,0.696491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc2bfe00000 of size 1572864 bytes.
[1.328s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823371725.000000
Throughput: 82337172.500000
Num Accesses - Session Mean & Stddev: 8233717.250000,8605.462927
Num Phases Traversed: 66905.000000
Num Phases Traversed - Session Mean & Stddev: 669.050000,0.698212
Num Path Iterations: 66905.000000
Num Path Iterations - Session Mean & Stddev: 669.050000,0.698212
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbbd6000000 of size 1572864 bytes.
[1.300s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 821227175.000000
Throughput: 82122717.500000
Num Accesses - Session Mean & Stddev: 8212271.750000,7929.296197
Num Phases Traversed: 66731.000000
Num Phases Traversed - Session Mean & Stddev: 667.310000,0.643351
Num Path Iterations: 66731.000000
Num Path Iterations - Session Mean & Stddev: 667.310000,0.643351
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5fdf000000 of size 1572864 bytes.
[1.340s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 822706175.000000
Throughput: 82270617.500000
Num Accesses - Session Mean & Stddev: 8227061.750000,6161.267377
Num Phases Traversed: 66851.000000
Num Phases Traversed - Session Mean & Stddev: 668.510000,0.499900
Num Path Iterations: 66851.000000
Num Path Iterations - Session Mean & Stddev: 668.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f815ac00000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823433350.000000
Throughput: 82343335.000000
Num Accesses - Session Mean & Stddev: 8234333.500000,8449.594295
Num Phases Traversed: 66910.000000
Num Phases Traversed - Session Mean & Stddev: 669.100000,0.685565
Num Path Iterations: 66910.000000
Num Path Iterations - Session Mean & Stddev: 669.100000,0.685565
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb8a9200000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824172850.000000
Throughput: 82417285.000000
Num Accesses - Session Mean & Stddev: 8241728.500000,7080.173462
Num Phases Traversed: 66970.000000
Num Phases Traversed - Session Mean & Stddev: 669.700000,0.574456
Num Path Iterations: 66970.000000
Num Path Iterations - Session Mean & Stddev: 669.700000,0.574456
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0 0/12288,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0 0/12288,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd162e00000 of size 1572864 bytes.
[1.328s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823568925.000000
Throughput: 82356892.500000
Num Accesses - Session Mean & Stddev: 8235689.250000,8043.420024
Num Phases Traversed: 66921.000000
Num Phases Traversed - Session Mean & Stddev: 669.210000,0.652610
Num Path Iterations: 66921.000000
Num Path Iterations - Session Mean & Stddev: 669.210000,0.652610
+ set +x

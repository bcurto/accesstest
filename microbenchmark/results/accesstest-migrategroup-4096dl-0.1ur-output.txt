++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe8afe00000 of size 262144 bytes.
[1.267s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1954245580.000000
Throughput: 195424558.000000
Num Accesses - Session Mean & Stddev: 19542455.800000,1391.805827
Num Phases Traversed: 937388.000000
Num Phases Traversed - Session Mean & Stddev: 9373.880000,0.667533
Num Path Iterations: 937388.000000
Num Path Iterations - Session Mean & Stddev: 9373.880000,0.667533
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f422e000000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1954216390.000000
Throughput: 195421639.000000
Num Accesses - Session Mean & Stddev: 19542163.900000,1338.953169
Num Phases Traversed: 937374.000000
Num Phases Traversed - Session Mean & Stddev: 9373.740000,0.642184
Num Path Iterations: 937374.000000
Num Path Iterations - Session Mean & Stddev: 9373.740000,0.642184
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7850a00000 of size 262144 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1953699310.000000
Throughput: 195369931.000000
Num Accesses - Session Mean & Stddev: 19536993.100000,1338.953169
Num Phases Traversed: 937126.000000
Num Phases Traversed - Session Mean & Stddev: 9371.260000,0.642184
Num Path Iterations: 937126.000000
Num Path Iterations - Session Mean & Stddev: 9371.260000,0.642184
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4b4f600000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1955104600.000000
Throughput: 195510460.000000
Num Accesses - Session Mean & Stddev: 19551046.000000,1444.530374
Num Phases Traversed: 937800.000000
Num Phases Traversed - Session Mean & Stddev: 9378.000000,0.692820
Num Path Iterations: 937800.000000
Num Path Iterations - Session Mean & Stddev: 9378.000000,0.692820
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f31a7c00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1954216390.000000
Throughput: 195421639.000000
Num Accesses - Session Mean & Stddev: 19542163.900000,1306.082344
Num Phases Traversed: 937374.000000
Num Phases Traversed - Session Mean & Stddev: 9373.740000,0.626418
Num Path Iterations: 937374.000000
Num Path Iterations - Session Mean & Stddev: 9373.740000,0.626418
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9dcd400000 of size 262144 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1953982870.000000
Throughput: 195398287.000000
Num Accesses - Session Mean & Stddev: 19539828.700000,1207.861213
Num Phases Traversed: 937262.000000
Num Phases Traversed - Session Mean & Stddev: 9372.620000,0.579310
Num Path Iterations: 937262.000000
Num Path Iterations - Session Mean & Stddev: 9372.620000,0.579310
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f206a200000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1955279740.000000
Throughput: 195527974.000000
Num Accesses - Session Mean & Stddev: 19552797.400000,1374.203238
Num Phases Traversed: 937884.000000
Num Phases Traversed - Session Mean & Stddev: 9378.840000,0.659090
Num Path Iterations: 937884.000000
Num Path Iterations - Session Mean & Stddev: 9378.840000,0.659090
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f90fc800000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1953907810.000000
Throughput: 195390781.000000
Num Accesses - Session Mean & Stddev: 19539078.100000,1338.953169
Num Phases Traversed: 937226.000000
Num Phases Traversed - Session Mean & Stddev: 9372.260000,0.642184
Num Path Iterations: 937226.000000
Num Path Iterations - Session Mean & Stddev: 9372.260000,0.642184
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fccafa00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1954591690.000000
Throughput: 195459169.000000
Num Accesses - Session Mean & Stddev: 19545916.900000,1194.834168
Num Phases Traversed: 937554.000000
Num Phases Traversed - Session Mean & Stddev: 9375.540000,0.573062
Num Path Iterations: 937554.000000
Num Path Iterations - Session Mean & Stddev: 9375.540000,0.573062
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb909400000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1954698025.000000
Throughput: 195469802.500000
Num Accesses - Session Mean & Stddev: 19546980.250000,1425.597309
Num Phases Traversed: 937605.000000
Num Phases Traversed - Session Mean & Stddev: 9376.050000,0.683740
Num Path Iterations: 937605.000000
Num Path Iterations - Session Mean & Stddev: 9376.050000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe9b9c00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1954568755.000000
Throughput: 195456875.500000
Num Accesses - Session Mean & Stddev: 19545687.550000,1113.280669
Num Phases Traversed: 937543.000000
Num Phases Traversed - Session Mean & Stddev: 9375.430000,0.533948
Num Path Iterations: 937543.000000
Num Path Iterations - Session Mean & Stddev: 9375.430000,0.533948
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f72a4800000 of size 262144 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1955942770.000000
Throughput: 195594277.000000
Num Accesses - Session Mean & Stddev: 19559427.700000,1443.928360
Num Phases Traversed: 938202.000000
Num Phases Traversed - Session Mean & Stddev: 9382.020000,0.692532
Num Path Iterations: 938202.000000
Num Path Iterations - Session Mean & Stddev: 9382.020000,0.692532
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff247c00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1954647985.000000
Throughput: 195464798.500000
Num Accesses - Session Mean & Stddev: 19546479.850000,1373.412148
Num Phases Traversed: 937581.000000
Num Phases Traversed - Session Mean & Stddev: 9375.810000,0.658711
Num Path Iterations: 937581.000000
Num Path Iterations - Session Mean & Stddev: 9375.810000,0.658711
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f494bc00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1955467390.000000
Throughput: 195546739.000000
Num Accesses - Session Mean & Stddev: 19554673.900000,1338.953169
Num Phases Traversed: 937974.000000
Num Phases Traversed - Session Mean & Stddev: 9379.740000,0.642184
Num Path Iterations: 937974.000000
Num Path Iterations - Session Mean & Stddev: 9379.740000,0.642184
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa558200000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1953903640.000000
Throughput: 195390364.000000
Num Accesses - Session Mean & Stddev: 19539036.400000,1355.089606
Num Phases Traversed: 937224.000000
Num Phases Traversed - Session Mean & Stddev: 9372.240000,0.649923
Num Path Iterations: 937224.000000
Num Path Iterations - Session Mean & Stddev: 9372.240000,0.649923
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcb36200000 of size 262144 bytes.
[1.236s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1954337320.000000
Throughput: 195433732.000000
Num Accesses - Session Mean & Stddev: 19543373.200000,1281.215111
Num Phases Traversed: 937432.000000
Num Phases Traversed - Session Mean & Stddev: 9374.320000,0.614492
Num Path Iterations: 937432.000000
Num Path Iterations - Session Mean & Stddev: 9374.320000,0.614492
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd296200000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1954739725.000000
Throughput: 195473972.500000
Num Accesses - Session Mean & Stddev: 19547397.250000,1330.974901
Num Phases Traversed: 937625.000000
Num Phases Traversed - Session Mean & Stddev: 9376.250000,0.638357
Num Path Iterations: 937625.000000
Num Path Iterations - Session Mean & Stddev: 9376.250000,0.638357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5ce7200000 of size 262144 bytes.
[1.236s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1950096430.000000
Throughput: 195009643.000000
Num Accesses - Session Mean & Stddev: 19500964.300000,2858.503824
Num Phases Traversed: 935398.000000
Num Phases Traversed - Session Mean & Stddev: 9353.980000,1.370985
Num Path Iterations: 935398.000000
Num Path Iterations - Session Mean & Stddev: 9353.980000,1.370985
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fece1800000 of size 262144 bytes.
[1.267s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1954700110.000000
Throughput: 195470011.000000
Num Accesses - Session Mean & Stddev: 19547001.100000,1439.103189
Num Phases Traversed: 937606.000000
Num Phases Traversed - Session Mean & Stddev: 9376.060000,0.690217
Num Path Iterations: 937606.000000
Num Path Iterations - Session Mean & Stddev: 9376.060000,0.690217
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0.1 0/2048,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0.1 0/2048,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff633600000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1958006920.000000
Throughput: 195800692.000000
Num Accesses - Session Mean & Stddev: 19580069.200000,1434.867855
Num Phases Traversed: 939192.000000
Num Phases Traversed - Session Mean & Stddev: 9391.920000,0.688186
Num Path Iterations: 939192.000000
Num Path Iterations - Session Mean & Stddev: 9391.920000,0.688186
+ set +x

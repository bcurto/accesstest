++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff6c0000000 of size 17301504 bytes.
[3.350s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 314351725.000000
Throughput: 31435172.500000
Num Accesses - Session Mean & Stddev: 3143517.250000,88401.739749
Num Phases Traversed: 2425.000000
Num Phases Traversed - Session Mean & Stddev: 24.250000,0.653835
Num Path Iterations: 2425.000000
Num Path Iterations - Session Mean & Stddev: 24.250000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffa80000000 of size 17301504 bytes.
[3.329s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 339635060.000000
Throughput: 33963506.000000
Num Accesses - Session Mean & Stddev: 3396350.600000,61306.187292
Num Phases Traversed: 2612.000000
Num Phases Traversed - Session Mean & Stddev: 26.120000,0.453431
Num Path Iterations: 2612.000000
Num Path Iterations - Session Mean & Stddev: 26.120000,0.453431
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fee80000000 of size 17301504 bytes.
[3.396s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 341122315.000000
Throughput: 34112231.500000
Num Accesses - Session Mean & Stddev: 3411223.150000,89388.810005
Num Phases Traversed: 2623.000000
Num Phases Traversed - Session Mean & Stddev: 26.230000,0.661135
Num Path Iterations: 2623.000000
Num Path Iterations - Session Mean & Stddev: 26.230000,0.661135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdc00000000 of size 17301504 bytes.
[3.351s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 314081315.000000
Throughput: 31408131.500000
Num Accesses - Session Mean & Stddev: 3140813.150000,89388.810005
Num Phases Traversed: 2423.000000
Num Phases Traversed - Session Mean & Stddev: 24.230000,0.661135
Num Path Iterations: 2423.000000
Num Path Iterations - Session Mean & Stddev: 24.230000,0.661135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe180000000 of size 17301504 bytes.
[3.396s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 332739605.000000
Throughput: 33273960.500000
Num Accesses - Session Mean & Stddev: 3327396.050000,78593.539590
Num Phases Traversed: 2561.000000
Num Phases Traversed - Session Mean & Stddev: 25.610000,0.581292
Num Path Iterations: 2561.000000
Num Path Iterations - Session Mean & Stddev: 25.610000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7fc0000000 of size 17301504 bytes.
[3.362s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 341663135.000000
Throughput: 34166313.500000
Num Accesses - Session Mean & Stddev: 3416631.350000,87319.823142
Num Phases Traversed: 2627.000000
Num Phases Traversed - Session Mean & Stddev: 26.270000,0.645833
Num Path Iterations: 2627.000000
Num Path Iterations - Session Mean & Stddev: 26.270000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3e00000000 of size 17301504 bytes.
[3.402s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 341663135.000000
Throughput: 34166313.500000
Num Accesses - Session Mean & Stddev: 3416631.350000,87319.823142
Num Phases Traversed: 2627.000000
Num Phases Traversed - Session Mean & Stddev: 26.270000,0.645833
Num Path Iterations: 2627.000000
Num Path Iterations - Session Mean & Stddev: 26.270000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7fc0000000 of size 17301504 bytes.
[3.340s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 344232030.000000
Throughput: 34423203.000000
Num Accesses - Session Mean & Stddev: 3442320.300000,67385.824763
Num Phases Traversed: 2646.000000
Num Phases Traversed - Session Mean & Stddev: 26.460000,0.498397
Num Path Iterations: 2646.000000
Num Path Iterations - Session Mean & Stddev: 26.460000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffa40000000 of size 17301504 bytes.
[3.354s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 341122315.000000
Throughput: 34112231.500000
Num Accesses - Session Mean & Stddev: 3411223.150000,80795.593891
Num Phases Traversed: 2623.000000
Num Phases Traversed - Session Mean & Stddev: 26.230000,0.597578
Num Path Iterations: 2623.000000
Num Path Iterations - Session Mean & Stddev: 26.230000,0.597578
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc580000000 of size 17301504 bytes.
[3.358s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 332198785.000000
Throughput: 33219878.500000
Num Accesses - Session Mean & Stddev: 3321987.850000,74681.641699
Num Phases Traversed: 2557.000000
Num Phases Traversed - Session Mean & Stddev: 25.570000,0.552359
Num Path Iterations: 2557.000000
Num Path Iterations - Session Mean & Stddev: 25.570000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9840000000 of size 17301504 bytes.
[3.345s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 341122315.000000
Throughput: 34112231.500000
Num Accesses - Session Mean & Stddev: 3411223.150000,89388.810005
Num Phases Traversed: 2623.000000
Num Phases Traversed - Session Mean & Stddev: 26.230000,0.661135
Num Path Iterations: 2623.000000
Num Path Iterations - Session Mean & Stddev: 26.230000,0.661135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f16c0000000 of size 17301504 bytes.
[3.402s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 332739605.000000
Throughput: 33273960.500000
Num Accesses - Session Mean & Stddev: 3327396.050000,78593.539590
Num Phases Traversed: 2561.000000
Num Phases Traversed - Session Mean & Stddev: 25.610000,0.581292
Num Path Iterations: 2561.000000
Num Path Iterations - Session Mean & Stddev: 25.610000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f94c0000000 of size 17301504 bytes.
[3.354s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 341122315.000000
Throughput: 34112231.500000
Num Accesses - Session Mean & Stddev: 3411223.150000,89388.810005
Num Phases Traversed: 2623.000000
Num Phases Traversed - Session Mean & Stddev: 26.230000,0.661135
Num Path Iterations: 2623.000000
Num Path Iterations - Session Mean & Stddev: 26.230000,0.661135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe280000000 of size 17301504 bytes.
[3.357s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 332198785.000000
Throughput: 33219878.500000
Num Accesses - Session Mean & Stddev: 3321987.850000,74681.641699
Num Phases Traversed: 2557.000000
Num Phases Traversed - Session Mean & Stddev: 25.570000,0.552359
Num Path Iterations: 2557.000000
Num Path Iterations - Session Mean & Stddev: 25.570000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fed00000000 of size 17301504 bytes.
[3.386s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 333415630.000000
Throughput: 33341563.000000
Num Accesses - Session Mean & Stddev: 3334156.300000,64047.794423
Num Phases Traversed: 2566.000000
Num Phases Traversed - Session Mean & Stddev: 25.660000,0.473709
Num Path Iterations: 2566.000000
Num Path Iterations - Session Mean & Stddev: 25.660000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa840000000 of size 17301504 bytes.
[3.353s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 332469195.000000
Throughput: 33246919.500000
Num Accesses - Session Mean & Stddev: 3324691.950000,76710.222219
Num Phases Traversed: 2559.000000
Num Phases Traversed - Session Mean & Stddev: 25.590000,0.567362
Num Path Iterations: 2559.000000
Num Path Iterations - Session Mean & Stddev: 25.590000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3c00000000 of size 17301504 bytes.
[3.356s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 322734435.000000
Throughput: 32273443.500000
Num Accesses - Session Mean & Stddev: 3227344.350000,91010.140240
Num Phases Traversed: 2487.000000
Num Phases Traversed - Session Mean & Stddev: 24.870000,0.673127
Num Path Iterations: 2487.000000
Num Path Iterations - Session Mean & Stddev: 24.870000,0.673127
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fda40000000 of size 17301504 bytes.
[3.357s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 333010015.000000
Throughput: 33301001.500000
Num Accesses - Session Mean & Stddev: 3330100.150000,80341.809937
Num Phases Traversed: 2563.000000
Num Phases Traversed - Session Mean & Stddev: 25.630000,0.594222
Num Path Iterations: 2563.000000
Num Path Iterations - Session Mean & Stddev: 25.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa1c0000000 of size 17301504 bytes.
[3.352s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 314351725.000000
Throughput: 31435172.500000
Num Accesses - Session Mean & Stddev: 3143517.250000,88401.739749
Num Phases Traversed: 2425.000000
Num Phases Traversed - Session Mean & Stddev: 24.250000,0.653835
Num Path Iterations: 2425.000000
Num Path Iterations - Session Mean & Stddev: 24.250000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd280000000 of size 17301504 bytes.
[3.359s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 340581495.000000
Throughput: 34058149.500000
Num Accesses - Session Mean & Stddev: 3405814.950000,91090.449226
Num Phases Traversed: 2619.000000
Num Phases Traversed - Session Mean & Stddev: 26.190000,0.673721
Num Path Iterations: 2619.000000
Num Path Iterations - Session Mean & Stddev: 26.190000,0.673721
+ set +x

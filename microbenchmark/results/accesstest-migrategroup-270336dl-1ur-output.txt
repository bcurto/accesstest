++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f80c0000000 of size 17301504 bytes.
[3.551s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 234310365.000000
Throughput: 23431036.500000
Num Accesses - Session Mean & Stddev: 2343103.650000,63575.110513
Num Phases Traversed: 1833.000000
Num Phases Traversed - Session Mean & Stddev: 18.330000,0.470213
Num Path Iterations: 1833.000000
Num Path Iterations - Session Mean & Stddev: 18.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3640000000 of size 17301504 bytes.
[3.617s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 233904750.000000
Throughput: 23390475.000000
Num Accesses - Session Mean & Stddev: 2339047.500000,70254.578831
Num Phases Traversed: 1830.000000
Num Phases Traversed - Session Mean & Stddev: 18.300000,0.519615
Num Path Iterations: 1830.000000
Num Path Iterations - Session Mean & Stddev: 18.300000,0.519615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6e40000000 of size 17301504 bytes.
[3.579s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 234310365.000000
Throughput: 23431036.500000
Num Accesses - Session Mean & Stddev: 2343103.650000,63575.110513
Num Phases Traversed: 1833.000000
Num Phases Traversed - Session Mean & Stddev: 18.330000,0.470213
Num Path Iterations: 1833.000000
Num Path Iterations - Session Mean & Stddev: 18.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4800000000 of size 17301504 bytes.
[3.619s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 233904750.000000
Throughput: 23390475.000000
Num Accesses - Session Mean & Stddev: 2339047.500000,70254.578831
Num Phases Traversed: 1830.000000
Num Phases Traversed - Session Mean & Stddev: 18.300000,0.519615
Num Path Iterations: 1830.000000
Num Path Iterations - Session Mean & Stddev: 18.300000,0.519615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd6c0000000 of size 17301504 bytes.
[3.587s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 234310365.000000
Throughput: 23431036.500000
Num Accesses - Session Mean & Stddev: 2343103.650000,63575.110513
Num Phases Traversed: 1833.000000
Num Phases Traversed - Session Mean & Stddev: 18.330000,0.470213
Num Path Iterations: 1833.000000
Num Path Iterations - Session Mean & Stddev: 18.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6e40000000 of size 17301504 bytes.
[3.576s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 233769545.000000
Throughput: 23376954.500000
Num Accesses - Session Mean & Stddev: 2337695.450000,61350.898265
Num Phases Traversed: 1829.000000
Num Phases Traversed - Session Mean & Stddev: 18.290000,0.453762
Num Path Iterations: 1829.000000
Num Path Iterations - Session Mean & Stddev: 18.290000,0.453762
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6100000000 of size 17301504 bytes.
[3.587s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 234175160.000000
Throughput: 23417516.000000
Num Accesses - Session Mean & Stddev: 2341751.600000,63069.908075
Num Phases Traversed: 1832.000000
Num Phases Traversed - Session Mean & Stddev: 18.320000,0.466476
Num Path Iterations: 1832.000000
Num Path Iterations - Session Mean & Stddev: 18.320000,0.466476
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9800000000 of size 17301504 bytes.
[3.593s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 233499135.000000
Throughput: 23349913.500000
Num Accesses - Session Mean & Stddev: 2334991.350000,71172.302409
Num Phases Traversed: 1827.000000
Num Phases Traversed - Session Mean & Stddev: 18.270000,0.526403
Num Path Iterations: 1827.000000
Num Path Iterations - Session Mean & Stddev: 18.270000,0.526403
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0600000000 of size 17301504 bytes.
[3.630s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 233904750.000000
Throughput: 23390475.000000
Num Accesses - Session Mean & Stddev: 2339047.500000,70254.578831
Num Phases Traversed: 1830.000000
Num Phases Traversed - Session Mean & Stddev: 18.300000,0.519615
Num Path Iterations: 1830.000000
Num Path Iterations - Session Mean & Stddev: 18.300000,0.519615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe2c0000000 of size 17301504 bytes.
[3.617s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 233904750.000000
Throughput: 23390475.000000
Num Accesses - Session Mean & Stddev: 2339047.500000,70254.578831
Num Phases Traversed: 1830.000000
Num Phases Traversed - Session Mean & Stddev: 18.300000,0.519615
Num Path Iterations: 1830.000000
Num Path Iterations - Session Mean & Stddev: 18.300000,0.519615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7a40000000 of size 17301504 bytes.
[3.549s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 233769545.000000
Throughput: 23376954.500000
Num Accesses - Session Mean & Stddev: 2337695.450000,72293.596397
Num Phases Traversed: 1829.000000
Num Phases Traversed - Session Mean & Stddev: 18.290000,0.534696
Num Path Iterations: 1829.000000
Num Path Iterations - Session Mean & Stddev: 18.290000,0.534696
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f88c0000000 of size 17301504 bytes.
[3.620s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 233904750.000000
Throughput: 23390475.000000
Num Accesses - Session Mean & Stddev: 2339047.500000,70254.578831
Num Phases Traversed: 1830.000000
Num Phases Traversed - Session Mean & Stddev: 18.300000,0.519615
Num Path Iterations: 1830.000000
Num Path Iterations - Session Mean & Stddev: 18.300000,0.519615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f35c0000000 of size 17301504 bytes.
[3.570s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 234445570.000000
Throughput: 23444557.000000
Num Accesses - Session Mean & Stddev: 2344455.700000,64047.794423
Num Phases Traversed: 1834.000000
Num Phases Traversed - Session Mean & Stddev: 18.340000,0.473709
Num Path Iterations: 1834.000000
Num Path Iterations - Session Mean & Stddev: 18.340000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3a40000000 of size 17301504 bytes.
[3.622s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 233904750.000000
Throughput: 23390475.000000
Num Accesses - Session Mean & Stddev: 2339047.500000,70254.578831
Num Phases Traversed: 1830.000000
Num Phases Traversed - Session Mean & Stddev: 18.300000,0.519615
Num Path Iterations: 1830.000000
Num Path Iterations - Session Mean & Stddev: 18.300000,0.519615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb180000000 of size 17301504 bytes.
[3.619s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 233904750.000000
Throughput: 23390475.000000
Num Accesses - Session Mean & Stddev: 2339047.500000,70254.578831
Num Phases Traversed: 1830.000000
Num Phases Traversed - Session Mean & Stddev: 18.300000,0.519615
Num Path Iterations: 1830.000000
Num Path Iterations - Session Mean & Stddev: 18.300000,0.519615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0200000000 of size 17301504 bytes.
[3.550s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 234039955.000000
Throughput: 23403995.500000
Num Accesses - Session Mean & Stddev: 2340399.550000,68127.758918
Num Phases Traversed: 1831.000000
Num Phases Traversed - Session Mean & Stddev: 18.310000,0.503885
Num Path Iterations: 1831.000000
Num Path Iterations - Session Mean & Stddev: 18.310000,0.503885
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8a80000000 of size 17301504 bytes.
[3.625s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 233904750.000000
Throughput: 23390475.000000
Num Accesses - Session Mean & Stddev: 2339047.500000,70254.578831
Num Phases Traversed: 1830.000000
Num Phases Traversed - Session Mean & Stddev: 18.300000,0.519615
Num Path Iterations: 1830.000000
Num Path Iterations - Session Mean & Stddev: 18.300000,0.519615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffa40000000 of size 17301504 bytes.
[3.594s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 233499135.000000
Throughput: 23349913.500000
Num Accesses - Session Mean & Stddev: 2334991.350000,71172.302409
Num Phases Traversed: 1827.000000
Num Phases Traversed - Session Mean & Stddev: 18.270000,0.526403
Num Path Iterations: 1827.000000
Num Path Iterations - Session Mean & Stddev: 18.270000,0.526403
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f47c0000000 of size 17301504 bytes.
[3.644s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 234445570.000000
Throughput: 23444557.000000
Num Accesses - Session Mean & Stddev: 2344455.700000,64047.794423
Num Phases Traversed: 1834.000000
Num Phases Traversed - Session Mean & Stddev: 18.340000,0.473709
Num Path Iterations: 1834.000000
Num Path Iterations - Session Mean & Stddev: 18.340000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,1 0/135168,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,1 0/135168,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6f40000000 of size 17301504 bytes.
[3.636s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 233904750.000000
Throughput: 23390475.000000
Num Accesses - Session Mean & Stddev: 2339047.500000,70254.578831
Num Phases Traversed: 1830.000000
Num Phases Traversed - Session Mean & Stddev: 18.300000,0.519615
Num Path Iterations: 1830.000000
Num Path Iterations - Session Mean & Stddev: 18.300000,0.519615
+ set +x

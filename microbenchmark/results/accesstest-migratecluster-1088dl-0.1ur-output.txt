++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd161600000 of size 69632 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2192304249.000000
Throughput: 219230424.900000
Num Accesses - Session Mean & Stddev: 21923042.490000,360.922138
Num Phases Traversed: 3773429.000000
Num Phases Traversed - Session Mean & Stddev: 37734.290000,0.621208
Num Path Iterations: 3773429.000000
Num Path Iterations - Session Mean & Stddev: 37734.290000,0.621208
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2c37e00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2174724932.000000
Throughput: 217472493.200000
Num Accesses - Session Mean & Stddev: 21747249.320000,368.190844
Num Phases Traversed: 3743172.000000
Num Phases Traversed - Session Mean & Stddev: 37431.720000,0.633719
Num Path Iterations: 3743172.000000
Num Path Iterations - Session Mean & Stddev: 37431.720000,0.633719
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff4b0c00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2205088573.000000
Throughput: 220508857.300000
Num Accesses - Session Mean & Stddev: 22050885.730000,358.670458
Num Phases Traversed: 3795433.000000
Num Phases Traversed - Session Mean & Stddev: 37954.330000,0.617333
Num Path Iterations: 3795433.000000
Num Path Iterations - Session Mean & Stddev: 37954.330000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe178e00000 of size 69632 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2174710988.000000
Throughput: 217471098.800000
Num Accesses - Session Mean & Stddev: 21747109.880000,290.267507
Num Phases Traversed: 3743148.000000
Num Phases Traversed - Session Mean & Stddev: 37431.480000,0.499600
Num Path Iterations: 3743148.000000
Num Path Iterations - Session Mean & Stddev: 37431.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0200a00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2214199815.000000
Throughput: 221419981.500000
Num Accesses - Session Mean & Stddev: 22141998.150000,397.252775
Num Phases Traversed: 3811115.000000
Num Phases Traversed - Session Mean & Stddev: 38111.150000,0.683740
Num Path Iterations: 3811115.000000
Num Path Iterations - Session Mean & Stddev: 38111.150000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f62d3400000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2208433971.000000
Throughput: 220843397.100000
Num Accesses - Session Mean & Stddev: 22084339.710000,403.324492
Num Phases Traversed: 3801191.000000
Num Phases Traversed - Session Mean & Stddev: 38011.910000,0.694190
Num Path Iterations: 3801191.000000
Num Path Iterations - Session Mean & Stddev: 38011.910000,0.694190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f64e4e00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2173891778.000000
Throughput: 217389177.800000
Num Accesses - Session Mean & Stddev: 21738917.780000,336.579072
Num Phases Traversed: 3741738.000000
Num Phases Traversed - Session Mean & Stddev: 37417.380000,0.579310
Num Path Iterations: 3741738.000000
Num Path Iterations - Session Mean & Stddev: 37417.380000,0.579310
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe5dd600000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2174825445.000000
Throughput: 217482544.500000
Num Accesses - Session Mean & Stddev: 21748254.450000,311.526544
Num Phases Traversed: 3743345.000000
Num Phases Traversed - Session Mean & Stddev: 37433.450000,0.536190
Num Path Iterations: 3743345.000000
Num Path Iterations - Session Mean & Stddev: 37433.450000,0.536190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f26fcc00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2208061550.000000
Throughput: 220806155.000000
Num Accesses - Session Mean & Stddev: 22080615.500000,290.500000
Num Phases Traversed: 3800550.000000
Num Phases Traversed - Session Mean & Stddev: 38005.500000,0.500000
Num Path Iterations: 3800550.000000
Num Path Iterations - Session Mean & Stddev: 38005.500000,0.500000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9622600000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2190789582.000000
Throughput: 219078958.200000
Num Accesses - Session Mean & Stddev: 21907895.820000,381.695333
Num Phases Traversed: 3770822.000000
Num Phases Traversed - Session Mean & Stddev: 37708.220000,0.656963
Num Path Iterations: 3770822.000000
Num Path Iterations - Session Mean & Stddev: 37708.220000,0.656963
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f12e6800000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2179092309.000000
Throughput: 217909230.900000
Num Accesses - Session Mean & Stddev: 21790923.090000,393.152873
Num Phases Traversed: 3750689.000000
Num Phases Traversed - Session Mean & Stddev: 37506.890000,0.676683
Num Path Iterations: 3750689.000000
Num Path Iterations - Session Mean & Stddev: 37506.890000,0.676683
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f57e0600000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2176796778.000000
Throughput: 217679677.800000
Num Accesses - Session Mean & Stddev: 21767967.780000,336.579072
Num Phases Traversed: 3746738.000000
Num Phases Traversed - Session Mean & Stddev: 37467.380000,0.579310
Num Path Iterations: 3746738.000000
Num Path Iterations - Session Mean & Stddev: 37467.380000,0.579310
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1c76400000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2185075447.000000
Throughput: 218507544.700000
Num Accesses - Session Mean & Stddev: 21850754.470000,399.624961
Num Phases Traversed: 3760987.000000
Num Phases Traversed - Session Mean & Stddev: 37609.870000,0.687823
Num Path Iterations: 3760987.000000
Num Path Iterations - Session Mean & Stddev: 37609.870000,0.687823
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f460c200000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2202089451.000000
Throughput: 220208945.100000
Num Accesses - Session Mean & Stddev: 22020894.510000,370.156737
Num Phases Traversed: 3790271.000000
Num Phases Traversed - Session Mean & Stddev: 37902.710000,0.637103
Num Path Iterations: 3790271.000000
Num Path Iterations - Session Mean & Stddev: 37902.710000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f305ea00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2211968775.000000
Throughput: 221196877.500000
Num Accesses - Session Mean & Stddev: 22119687.750000,379.878043
Num Phases Traversed: 3807275.000000
Num Phases Traversed - Session Mean & Stddev: 38072.750000,0.653835
Num Path Iterations: 3807275.000000
Num Path Iterations - Session Mean & Stddev: 38072.750000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1783e00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2173219561.000000
Throughput: 217321956.100000
Num Accesses - Session Mean & Stddev: 21732195.610000,391.431907
Num Phases Traversed: 3740581.000000
Num Phases Traversed - Session Mean & Stddev: 37405.810000,0.673721
Num Path Iterations: 3740581.000000
Num Path Iterations - Session Mean & Stddev: 37405.810000,0.673721
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5512600000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2168979423.000000
Throughput: 216897942.300000
Num Accesses - Session Mean & Stddev: 21689794.230000,385.873240
Num Phases Traversed: 3733283.000000
Num Phases Traversed - Session Mean & Stddev: 37332.830000,0.664154
Num Path Iterations: 3733283.000000
Num Path Iterations - Session Mean & Stddev: 37332.830000,0.664154
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb336400000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2180942213.000000
Throughput: 218094221.300000
Num Accesses - Session Mean & Stddev: 21809422.130000,366.122211
Num Phases Traversed: 3753873.000000
Num Phases Traversed - Session Mean & Stddev: 37538.730000,0.630159
Num Path Iterations: 3753873.000000
Num Path Iterations - Session Mean & Stddev: 37538.730000,0.630159
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1653800000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2165170968.000000
Throughput: 216517096.800000
Num Accesses - Session Mean & Stddev: 21651709.680000,368.190844
Num Phases Traversed: 3726728.000000
Num Phases Traversed - Session Mean & Stddev: 37267.280000,0.633719
Num Path Iterations: 3726728.000000
Num Path Iterations - Session Mean & Stddev: 37267.280000,0.633719
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f10dd800000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2194428385.000000
Throughput: 219442838.500000
Num Accesses - Session Mean & Stddev: 21944283.850000,388.662511
Num Phases Traversed: 3777085.000000
Num Phases Traversed - Session Mean & Stddev: 37770.850000,0.668954
Num Path Iterations: 3777085.000000
Num Path Iterations - Session Mean & Stddev: 37770.850000,0.668954
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb140000000 of size 23592960 bytes.
[4.807s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f12c0000000 of size 23592960 bytes.
[4.777s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 230077636.000000
Throughput: 23007763.600000
Num Accesses - Session Mean & Stddev: 2300776.360000,92104.727679
Num Phases Traversed: 1348.000000
Num Phases Traversed - Session Mean & Stddev: 13.480000,0.499600
Num Path Iterations: 1348.000000
Num Path Iterations - Session Mean & Stddev: 13.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fce80000000 of size 23592960 bytes.
[4.748s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 230815064.000000
Throughput: 23081506.400000
Num Accesses - Session Mean & Stddev: 2308150.640000,92104.727679
Num Phases Traversed: 1352.000000
Num Phases Traversed - Session Mean & Stddev: 13.520000,0.499600
Num Path Iterations: 1352.000000
Num Path Iterations - Session Mean & Stddev: 13.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f09c0000000 of size 23592960 bytes.
[4.806s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feec0000000 of size 23592960 bytes.
[4.805s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f10c0000000 of size 23592960 bytes.
[4.801s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 230815064.000000
Throughput: 23081506.400000
Num Accesses - Session Mean & Stddev: 2308150.640000,92104.727679
Num Phases Traversed: 1352.000000
Num Phases Traversed - Session Mean & Stddev: 13.520000,0.499600
Num Path Iterations: 1352.000000
Num Path Iterations - Session Mean & Stddev: 13.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3cc0000000 of size 23592960 bytes.
[4.805s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe200000000 of size 23592960 bytes.
[4.796s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7140000000 of size 23592960 bytes.
[4.805s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0c40000000 of size 23592960 bytes.
[4.803s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8680000000 of size 23592960 bytes.
[4.806s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb300000000 of size 23592960 bytes.
[4.802s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4680000000 of size 23592960 bytes.
[4.796s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5e40000000 of size 23592960 bytes.
[4.805s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7ec0000000 of size 23592960 bytes.
[4.805s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 230815064.000000
Throughput: 23081506.400000
Num Accesses - Session Mean & Stddev: 2308150.640000,92104.727679
Num Phases Traversed: 1352.000000
Num Phases Traversed - Session Mean & Stddev: 13.520000,0.499600
Num Path Iterations: 1352.000000
Num Path Iterations - Session Mean & Stddev: 13.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3580000000 of size 23592960 bytes.
[4.796s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3c40000000 of size 23592960 bytes.
[4.774s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 230815064.000000
Throughput: 23081506.400000
Num Accesses - Session Mean & Stddev: 2308150.640000,92104.727679
Num Phases Traversed: 1352.000000
Num Phases Traversed - Session Mean & Stddev: 13.520000,0.499600
Num Path Iterations: 1352.000000
Num Path Iterations - Session Mean & Stddev: 13.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fde40000000 of size 23592960 bytes.
[4.803s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6dc0000000 of size 23592960 bytes.
[4.805s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.001 0/184320,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.001 0/184320,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6200000000 of size 23592960 bytes.
[4.801s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 230999421.000000
Throughput: 23099942.100000
Num Accesses - Session Mean & Stddev: 2309994.210000,92012.429101
Num Phases Traversed: 1353.000000
Num Phases Traversed - Session Mean & Stddev: 13.530000,0.499099
Num Path Iterations: 1353.000000
Num Path Iterations - Session Mean & Stddev: 13.530000,0.499099
+ set +x

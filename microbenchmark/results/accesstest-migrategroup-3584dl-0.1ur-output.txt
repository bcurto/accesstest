++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd789a00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969114303.000000
Throughput: 196911430.300000
Num Accesses - Session Mean & Stddev: 19691143.030000,1247.345818
Num Phases Traversed: 1076707.000000
Num Phases Traversed - Session Mean & Stddev: 10767.070000,0.681982
Num Path Iterations: 1076707.000000
Num Path Iterations - Session Mean & Stddev: 10767.070000,0.681982
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2416000000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969657516.000000
Throughput: 196965751.600000
Num Accesses - Session Mean & Stddev: 19696575.160000,1265.054661
Num Phases Traversed: 1077004.000000
Num Phases Traversed - Session Mean & Stddev: 10770.040000,0.691665
Num Path Iterations: 1077004.000000
Num Path Iterations - Session Mean & Stddev: 10770.040000,0.691665
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f506fc00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1970553726.000000
Throughput: 197055372.600000
Num Accesses - Session Mean & Stddev: 19705537.260000,1235.624535
Num Phases Traversed: 1077494.000000
Num Phases Traversed - Session Mean & Stddev: 10774.940000,0.675574
Num Path Iterations: 1077494.000000
Num Path Iterations - Session Mean & Stddev: 10774.940000,0.675574
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb4cf800000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969975762.000000
Throughput: 196997576.200000
Num Accesses - Session Mean & Stddev: 19699757.620000,757.656489
Num Phases Traversed: 1077178.000000
Num Phases Traversed - Session Mean & Stddev: 10771.780000,0.414246
Num Path Iterations: 1077178.000000
Num Path Iterations - Session Mean & Stddev: 10771.780000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9243800000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1968651566.000000
Throughput: 196865156.600000
Num Accesses - Session Mean & Stddev: 19686515.660000,911.568903
Num Phases Traversed: 1076454.000000
Num Phases Traversed - Session Mean & Stddev: 10764.540000,0.498397
Num Path Iterations: 1076454.000000
Num Path Iterations - Session Mean & Stddev: 10764.540000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1dbd600000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1955007226.000000
Throughput: 195500722.600000
Num Accesses - Session Mean & Stddev: 19550072.260000,1262.407546
Num Phases Traversed: 1068994.000000
Num Phases Traversed - Session Mean & Stddev: 10689.940000,0.690217
Num Path Iterations: 1068994.000000
Num Path Iterations - Session Mean & Stddev: 10689.940000,0.690217
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0ecd800000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969964788.000000
Throughput: 196996478.800000
Num Accesses - Session Mean & Stddev: 19699647.880000,1129.842452
Num Phases Traversed: 1077172.000000
Num Phases Traversed - Session Mean & Stddev: 10771.720000,0.617738
Num Path Iterations: 1077172.000000
Num Path Iterations - Session Mean & Stddev: 10771.720000,0.617738
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f750a400000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1970572016.000000
Throughput: 197057201.600000
Num Accesses - Session Mean & Stddev: 19705720.160000,1265.054661
Num Phases Traversed: 1077504.000000
Num Phases Traversed - Session Mean & Stddev: 10775.040000,0.691665
Num Path Iterations: 1077504.000000
Num Path Iterations - Session Mean & Stddev: 10775.040000,0.691665
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f75f8800000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969814810.000000
Throughput: 196981481.000000
Num Accesses - Session Mean & Stddev: 19698148.100000,1253.899226
Num Phases Traversed: 1077090.000000
Num Phases Traversed - Session Mean & Stddev: 10770.900000,0.685565
Num Path Iterations: 1077090.000000
Num Path Iterations - Session Mean & Stddev: 10770.900000,0.685565
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbf66800000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1968761306.000000
Throughput: 196876130.600000
Num Accesses - Session Mean & Stddev: 19687613.060000,1213.772687
Num Phases Traversed: 1076514.000000
Num Phases Traversed - Session Mean & Stddev: 10765.140000,0.663626
Num Path Iterations: 1076514.000000
Num Path Iterations - Session Mean & Stddev: 10765.140000,0.663626
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe15f800000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969496564.000000
Throughput: 196949656.400000
Num Accesses - Session Mean & Stddev: 19694965.640000,1205.476126
Num Phases Traversed: 1076916.000000
Num Phases Traversed - Session Mean & Stddev: 10769.160000,0.659090
Num Path Iterations: 1076916.000000
Num Path Iterations - Session Mean & Stddev: 10769.160000,0.659090
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f395d600000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969655687.000000
Throughput: 196965568.700000
Num Accesses - Session Mean & Stddev: 19696556.870000,1225.702955
Num Phases Traversed: 1077003.000000
Num Phases Traversed - Session Mean & Stddev: 10770.030000,0.670149
Num Path Iterations: 1077003.000000
Num Path Iterations - Session Mean & Stddev: 10770.030000,0.670149
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc27d000000 of size 229376 bytes.
[1.263s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969705070.000000
Throughput: 196970507.000000
Num Accesses - Session Mean & Stddev: 19697050.700000,1142.210134
Num Phases Traversed: 1077030.000000
Num Phases Traversed - Session Mean & Stddev: 10770.300000,0.624500
Num Path Iterations: 1077030.000000
Num Path Iterations - Session Mean & Stddev: 10770.300000,0.624500
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbcf6600000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1970540923.000000
Throughput: 197054092.300000
Num Accesses - Session Mean & Stddev: 19705409.230000,1231.149340
Num Phases Traversed: 1077487.000000
Num Phases Traversed - Session Mean & Stddev: 10774.870000,0.673127
Num Path Iterations: 1077487.000000
Num Path Iterations - Session Mean & Stddev: 10774.870000,0.673127
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f54bf800000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969897115.000000
Throughput: 196989711.500000
Num Accesses - Session Mean & Stddev: 19698971.150000,1078.179599
Num Phases Traversed: 1077135.000000
Num Phases Traversed - Session Mean & Stddev: 10771.350000,0.589491
Num Path Iterations: 1077135.000000
Num Path Iterations - Session Mean & Stddev: 10771.350000,0.589491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff07be00000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1970756745.000000
Throughput: 197075674.500000
Num Accesses - Session Mean & Stddev: 19707567.450000,1250.559942
Num Phases Traversed: 1077605.000000
Num Phases Traversed - Session Mean & Stddev: 10776.050000,0.683740
Num Path Iterations: 1077605.000000
Num Path Iterations - Session Mean & Stddev: 10776.050000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f80fa200000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969511196.000000
Throughput: 196951119.600000
Num Accesses - Session Mean & Stddev: 19695111.960000,1160.226262
Num Phases Traversed: 1076924.000000
Num Phases Traversed - Session Mean & Stddev: 10769.240000,0.634350
Num Path Iterations: 1076924.000000
Num Path Iterations - Session Mean & Stddev: 10769.240000,0.634350
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffb4dc00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1968016903.000000
Throughput: 196801690.300000
Num Accesses - Session Mean & Stddev: 19680169.030000,1247.345818
Num Phases Traversed: 1076107.000000
Num Phases Traversed - Session Mean & Stddev: 10761.070000,0.681982
Num Path Iterations: 1076107.000000
Num Path Iterations - Session Mean & Stddev: 10761.070000,0.681982
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7a7b800000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1969368534.000000
Throughput: 196936853.400000
Num Accesses - Session Mean & Stddev: 19693685.340000,947.556164
Num Phases Traversed: 1076846.000000
Num Phases Traversed - Session Mean & Stddev: 10768.460000,0.518073
Num Path Iterations: 1076846.000000
Num Path Iterations - Session Mean & Stddev: 10768.460000,0.518073
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8c72a00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1970149517.000000
Throughput: 197014951.700000
Num Accesses - Session Mean & Stddev: 19701495.170000,1152.560281
Num Phases Traversed: 1077273.000000
Num Phases Traversed - Session Mean & Stddev: 10772.730000,0.630159
Num Path Iterations: 1077273.000000
Num Path Iterations - Session Mean & Stddev: 10772.730000,0.630159
+ set +x

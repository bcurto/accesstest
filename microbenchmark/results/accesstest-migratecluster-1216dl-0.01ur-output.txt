++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2377000000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2088989335.000000
Throughput: 208898933.500000
Num Accesses - Session Mean & Stddev: 20889893.350000,319.323860
Num Phases Traversed: 3238843.000000
Num Phases Traversed - Session Mean & Stddev: 32388.430000,0.495076
Num Path Iterations: 3238843.000000
Num Path Iterations - Session Mean & Stddev: 32388.430000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc2ce200000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2089772365.000000
Throughput: 208977236.500000
Num Accesses - Session Mean & Stddev: 20897723.650000,319.323860
Num Phases Traversed: 3240057.000000
Num Phases Traversed - Session Mean & Stddev: 32400.570000,0.495076
Num Path Iterations: 3240057.000000
Num Path Iterations - Session Mean & Stddev: 32400.570000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdad4c00000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2089248625.000000
Throughput: 208924862.500000
Num Accesses - Session Mean & Stddev: 20892486.250000,320.883448
Num Phases Traversed: 3239245.000000
Num Phases Traversed - Session Mean & Stddev: 32392.450000,0.497494
Num Path Iterations: 3239245.000000
Num Path Iterations - Session Mean & Stddev: 32392.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb4f9400000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2088380455.000000
Throughput: 208838045.500000
Num Accesses - Session Mean & Stddev: 20883804.550000,232.468595
Num Phases Traversed: 3237899.000000
Num Phases Traversed - Session Mean & Stddev: 32378.990000,0.360416
Num Path Iterations: 3237899.000000
Num Path Iterations - Session Mean & Stddev: 32378.990000,0.360416
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f75c8200000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086495120.000000
Throughput: 208649512.000000
Num Accesses - Session Mean & Stddev: 20864951.200000,275.468619
Num Phases Traversed: 3234976.000000
Num Phases Traversed - Session Mean & Stddev: 32349.760000,0.427083
Num Path Iterations: 3234976.000000
Num Path Iterations - Session Mean & Stddev: 32349.760000,0.427083
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb15e600000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2087245255.000000
Throughput: 208724525.500000
Num Accesses - Session Mean & Stddev: 20872452.550000,314.598709
Num Phases Traversed: 3236139.000000
Num Phases Traversed - Session Mean & Stddev: 32361.390000,0.487750
Num Path Iterations: 3236139.000000
Num Path Iterations - Session Mean & Stddev: 32361.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbac3200000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2088657805.000000
Throughput: 208865780.500000
Num Accesses - Session Mean & Stddev: 20886578.050000,292.676524
Num Phases Traversed: 3238329.000000
Num Phases Traversed - Session Mean & Stddev: 32383.290000,0.453762
Num Path Iterations: 3238329.000000
Num Path Iterations - Session Mean & Stddev: 32383.290000,0.453762
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2bbaa00000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2087161405.000000
Throughput: 208716140.500000
Num Accesses - Session Mean & Stddev: 20871614.050000,225.196464
Num Phases Traversed: 3236009.000000
Num Phases Traversed - Session Mean & Stddev: 32360.090000,0.349142
Num Path Iterations: 3236009.000000
Num Path Iterations - Session Mean & Stddev: 32360.090000,0.349142
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f39c3a00000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2087810275.000000
Throughput: 208781027.500000
Num Accesses - Session Mean & Stddev: 20878102.750000,230.311067
Num Phases Traversed: 3237015.000000
Num Phases Traversed - Session Mean & Stddev: 32370.150000,0.357071
Num Path Iterations: 3237015.000000
Num Path Iterations - Session Mean & Stddev: 32370.150000,0.357071
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1707800000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2087725780.000000
Throughput: 208772578.000000
Num Accesses - Session Mean & Stddev: 20877257.800000,236.460906
Num Phases Traversed: 3236884.000000
Num Phases Traversed - Session Mean & Stddev: 32368.840000,0.366606
Num Path Iterations: 3236884.000000
Num Path Iterations - Session Mean & Stddev: 32368.840000,0.366606
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4c74200000 of size 77824 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2089169935.000000
Throughput: 208916993.500000
Num Accesses - Session Mean & Stddev: 20891699.350000,271.436968
Num Phases Traversed: 3239123.000000
Num Phases Traversed - Session Mean & Stddev: 32391.230000,0.420833
Num Path Iterations: 3239123.000000
Num Path Iterations - Session Mean & Stddev: 32391.230000,0.420833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd563c00000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2087431015.000000
Throughput: 208743101.500000
Num Accesses - Session Mean & Stddev: 20874310.150000,286.353850
Num Phases Traversed: 3236427.000000
Num Phases Traversed - Session Mean & Stddev: 32364.270000,0.443959
Num Path Iterations: 3236427.000000
Num Path Iterations - Session Mean & Stddev: 32364.270000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9464200000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2087674825.000000
Throughput: 208767482.500000
Num Accesses - Session Mean & Stddev: 20876748.250000,230.311067
Num Phases Traversed: 3236805.000000
Num Phases Traversed - Session Mean & Stddev: 32368.050000,0.357071
Num Path Iterations: 3236805.000000
Num Path Iterations - Session Mean & Stddev: 32368.050000,0.357071
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9c0f800000 of size 77824 bytes.
[1.225s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2097422710.000000
Throughput: 209742271.000000
Num Accesses - Session Mean & Stddev: 20974227.100000,264.056414
Num Phases Traversed: 3251918.000000
Num Phases Traversed - Session Mean & Stddev: 32519.180000,0.409390
Num Path Iterations: 3251918.000000
Num Path Iterations - Session Mean & Stddev: 32519.180000,0.409390
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5f0b400000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2088321115.000000
Throughput: 208832111.500000
Num Accesses - Session Mean & Stddev: 20883211.150000,228.133135
Num Phases Traversed: 3237807.000000
Num Phases Traversed - Session Mean & Stddev: 32378.070000,0.353695
Num Path Iterations: 3237807.000000
Num Path Iterations - Session Mean & Stddev: 32378.070000,0.353695
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc59b000000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2087721910.000000
Throughput: 208772191.000000
Num Accesses - Session Mean & Stddev: 20877219.100000,267.188866
Num Phases Traversed: 3236878.000000
Num Phases Traversed - Session Mean & Stddev: 32368.780000,0.414246
Num Path Iterations: 3236878.000000
Num Path Iterations - Session Mean & Stddev: 32368.780000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbb61200000 of size 77824 bytes.
[1.263s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2087130445.000000
Throughput: 208713044.500000
Num Accesses - Session Mean & Stddev: 20871304.450000,314.598709
Num Phases Traversed: 3235961.000000
Num Phases Traversed - Session Mean & Stddev: 32359.610000,0.487750
Num Path Iterations: 3235961.000000
Num Path Iterations - Session Mean & Stddev: 32359.610000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb597800000 of size 77824 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2087381995.000000
Throughput: 208738199.500000
Num Accesses - Session Mean & Stddev: 20873819.950000,322.435494
Num Phases Traversed: 3236351.000000
Num Phases Traversed - Session Mean & Stddev: 32363.510000,0.499900
Num Path Iterations: 3236351.000000
Num Path Iterations - Session Mean & Stddev: 32363.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f11ef600000 of size 77824 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2088899035.000000
Throughput: 208889903.500000
Num Accesses - Session Mean & Stddev: 20888990.350000,231.751650
Num Phases Traversed: 3238703.000000
Num Phases Traversed - Session Mean & Stddev: 32387.030000,0.359305
Num Path Iterations: 3238703.000000
Num Path Iterations - Session Mean & Stddev: 32387.030000,0.359305
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.01 0/608,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.01 0/608,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3565400000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2088039250.000000
Throughput: 208803925.000000
Num Accesses - Session Mean & Stddev: 20880392.500000,295.576132
Num Phases Traversed: 3237370.000000
Num Phases Traversed - Session Mean & Stddev: 32373.700000,0.458258
Num Path Iterations: 3237370.000000
Num Path Iterations - Session Mean & Stddev: 32373.700000,0.458258
+ set +x

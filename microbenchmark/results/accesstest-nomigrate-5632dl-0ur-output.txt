++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2ed2200000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835338529.000000
Throughput: 83533852.900000
Num Accesses - Session Mean & Stddev: 8353385.290000,12792.484235
Num Phases Traversed: 292893.000000
Num Phases Traversed - Session Mean & Stddev: 2928.930000,4.483871
Num Path Iterations: 292893.000000
Num Path Iterations - Session Mean & Stddev: 2928.930000,4.483871
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f60a1200000 of size 360448 bytes.
[1.274s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835521121.000000
Throughput: 83552112.100000
Num Accesses - Session Mean & Stddev: 8355211.210000,13984.095258
Num Phases Traversed: 292957.000000
Num Phases Traversed - Session Mean & Stddev: 2929.570000,4.901541
Num Path Iterations: 292957.000000
Num Path Iterations - Session Mean & Stddev: 2929.570000,4.901541
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff388200000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835629535.000000
Throughput: 83562953.500000
Num Accesses - Session Mean & Stddev: 8356295.350000,15523.964489
Num Phases Traversed: 292995.000000
Num Phases Traversed - Session Mean & Stddev: 2929.950000,5.441277
Num Path Iterations: 292995.000000
Num Path Iterations - Session Mean & Stddev: 2929.950000,5.441277
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9ce1800000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835809274.000000
Throughput: 83580927.400000
Num Accesses - Session Mean & Stddev: 8358092.740000,16469.398121
Num Phases Traversed: 293058.000000
Num Phases Traversed - Session Mean & Stddev: 2930.580000,5.772660
Num Path Iterations: 293058.000000
Num Path Iterations - Session Mean & Stddev: 2930.580000,5.772660
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9043a00000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835218703.000000
Throughput: 83521870.300000
Num Accesses - Session Mean & Stddev: 8352187.030000,16121.318228
Num Phases Traversed: 292851.000000
Num Phases Traversed - Session Mean & Stddev: 2928.510000,5.650655
Num Path Iterations: 292851.000000
Num Path Iterations - Session Mean & Stddev: 2928.510000,5.650655
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f34b5200000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835629535.000000
Throughput: 83562953.500000
Num Accesses - Session Mean & Stddev: 8356295.350000,15983.803599
Num Phases Traversed: 292995.000000
Num Phases Traversed - Session Mean & Stddev: 2929.950000,5.602455
Num Path Iterations: 292995.000000
Num Path Iterations - Session Mean & Stddev: 2929.950000,5.602455
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2357000000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835578181.000000
Throughput: 83557818.100000
Num Accesses - Session Mean & Stddev: 8355781.810000,16945.258943
Num Phases Traversed: 292977.000000
Num Phases Traversed - Session Mean & Stddev: 2929.770000,5.939453
Num Path Iterations: 292977.000000
Num Path Iterations - Session Mean & Stddev: 2929.770000,5.939453
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbf91a00000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835561063.000000
Throughput: 83556106.300000
Num Accesses - Session Mean & Stddev: 8355610.630000,14169.360664
Num Phases Traversed: 292971.000000
Num Phases Traversed - Session Mean & Stddev: 2929.710000,4.966478
Num Path Iterations: 292971.000000
Num Path Iterations - Session Mean & Stddev: 2929.710000,4.966478
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f05fb200000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835743655.000000
Throughput: 83574365.500000
Num Accesses - Session Mean & Stddev: 8357436.550000,16156.017814
Num Phases Traversed: 293035.000000
Num Phases Traversed - Session Mean & Stddev: 2930.350000,5.662817
Num Path Iterations: 293035.000000
Num Path Iterations - Session Mean & Stddev: 2930.350000,5.662817
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f69e7200000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835692301.000000
Throughput: 83569230.100000
Num Accesses - Session Mean & Stddev: 8356923.010000,15316.412274
Num Phases Traversed: 293017.000000
Num Phases Traversed - Session Mean & Stddev: 2930.170000,5.368529
Num Path Iterations: 293017.000000
Num Path Iterations - Session Mean & Stddev: 2930.170000,5.368529
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2313200000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835601005.000000
Throughput: 83560100.500000
Num Accesses - Session Mean & Stddev: 8356010.050000,15835.435083
Num Phases Traversed: 292985.000000
Num Phases Traversed - Session Mean & Stddev: 2929.850000,5.550450
Num Path Iterations: 292985.000000
Num Path Iterations - Session Mean & Stddev: 2929.850000,5.550450
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f35c1e00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835757920.000000
Throughput: 83575792.000000
Num Accesses - Session Mean & Stddev: 8357579.200000,16640.597891
Num Phases Traversed: 293040.000000
Num Phases Traversed - Session Mean & Stddev: 2930.400000,5.832667
Num Path Iterations: 293040.000000
Num Path Iterations - Session Mean & Stddev: 2930.400000,5.832667
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4c06c00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835469767.000000
Throughput: 83546976.700000
Num Accesses - Session Mean & Stddev: 8354697.670000,13794.454148
Num Phases Traversed: 292939.000000
Num Phases Traversed - Session Mean & Stddev: 2929.390000,4.835070
Num Path Iterations: 292939.000000
Num Path Iterations - Session Mean & Stddev: 2929.390000,4.835070
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f294d000000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835601005.000000
Throughput: 83560100.500000
Num Accesses - Session Mean & Stddev: 8356010.050000,15184.688800
Num Phases Traversed: 292985.000000
Num Phases Traversed - Session Mean & Stddev: 2929.850000,5.322358
Num Path Iterations: 292985.000000
Num Path Iterations - Session Mean & Stddev: 2929.850000,5.322358
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd059c00000 of size 360448 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835658065.000000
Throughput: 83565806.500000
Num Accesses - Session Mean & Stddev: 8356580.650000,14854.105526
Num Phases Traversed: 293005.000000
Num Phases Traversed - Session Mean & Stddev: 2930.050000,5.206486
Num Path Iterations: 293005.000000
Num Path Iterations - Session Mean & Stddev: 2930.050000,5.206486
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fde74800000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835626682.000000
Throughput: 83562668.200000
Num Accesses - Session Mean & Stddev: 8356266.820000,15003.742278
Num Phases Traversed: 292994.000000
Num Phases Traversed - Session Mean & Stddev: 2929.940000,5.258935
Num Path Iterations: 292994.000000
Num Path Iterations - Session Mean & Stddev: 2929.940000,5.258935
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9b38600000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835620976.000000
Throughput: 83562097.600000
Num Accesses - Session Mean & Stddev: 8356209.760000,14981.265725
Num Phases Traversed: 292992.000000
Num Phases Traversed - Session Mean & Stddev: 2929.920000,5.251057
Num Path Iterations: 292992.000000
Num Path Iterations - Session Mean & Stddev: 2929.920000,5.251057
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5e21200000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835666624.000000
Throughput: 83566662.400000
Num Accesses - Session Mean & Stddev: 8356666.240000,14817.373496
Num Phases Traversed: 293008.000000
Num Phases Traversed - Session Mean & Stddev: 2930.080000,5.193611
Num Path Iterations: 293008.000000
Num Path Iterations - Session Mean & Stddev: 2930.080000,5.193611
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9eea200000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835523974.000000
Throughput: 83552397.400000
Num Accesses - Session Mean & Stddev: 8355239.740000,13248.368678
Num Phases Traversed: 292958.000000
Num Phases Traversed - Session Mean & Stddev: 2929.580000,4.643662
Num Path Iterations: 292958.000000
Num Path Iterations - Session Mean & Stddev: 2929.580000,4.643662
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0 0/2816,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0 0/2816,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0ed6200000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 835752214.000000
Throughput: 83575221.400000
Num Accesses - Session Mean & Stddev: 8357522.140000,15770.562348
Num Phases Traversed: 293038.000000
Num Phases Traversed - Session Mean & Stddev: 2930.380000,5.527712
Num Path Iterations: 293038.000000
Num Path Iterations - Session Mean & Stddev: 2930.380000,5.527712
+ set +x

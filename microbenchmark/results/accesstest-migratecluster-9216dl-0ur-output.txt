++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fce8fe00000 of size 589824 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 843861895.000000
Throughput: 84386189.500000
Num Accesses - Session Mean & Stddev: 8438618.950000,2959.342587
Num Phases Traversed: 181771.000000
Num Phases Traversed - Session Mean & Stddev: 1817.710000,0.637103
Num Path Iterations: 181771.000000
Num Path Iterations - Session Mean & Stddev: 1817.710000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdc83e00000 of size 589824 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 845018500.000000
Throughput: 84501850.000000
Num Accesses - Session Mean & Stddev: 8450185.000000,3081.144430
Num Phases Traversed: 182020.000000
Num Phases Traversed - Session Mean & Stddev: 1820.200000,0.663325
Num Path Iterations: 182020.000000
Num Path Iterations - Session Mean & Stddev: 1820.200000,0.663325
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7be2a00000 of size 589824 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 845761700.000000
Throughput: 84576170.000000
Num Accesses - Session Mean & Stddev: 8457617.000000,3081.144430
Num Phases Traversed: 182180.000000
Num Phases Traversed - Session Mean & Stddev: 1821.800000,0.663325
Num Path Iterations: 182180.000000
Num Path Iterations - Session Mean & Stddev: 1821.800000,0.663325
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0d6cc00000 of size 589824 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 846472385.000000
Throughput: 84647238.500000
Num Accesses - Session Mean & Stddev: 8464723.850000,2867.511661
Num Phases Traversed: 182333.000000
Num Phases Traversed - Session Mean & Stddev: 1823.330000,0.617333
Num Path Iterations: 182333.000000
Num Path Iterations - Session Mean & Stddev: 1823.330000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9ee9e00000 of size 589824 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 842877155.000000
Throughput: 84287715.500000
Num Accesses - Session Mean & Stddev: 8428771.550000,2635.397968
Num Phases Traversed: 181559.000000
Num Phases Traversed - Session Mean & Stddev: 1815.590000,0.567362
Num Path Iterations: 181559.000000
Num Path Iterations - Session Mean & Stddev: 1815.590000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0837000000 of size 589824 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 842291885.000000
Throughput: 84229188.500000
Num Accesses - Session Mean & Stddev: 8422918.850000,2867.511661
Num Phases Traversed: 181433.000000
Num Phases Traversed - Session Mean & Stddev: 1814.330000,0.617333
Num Path Iterations: 181433.000000
Num Path Iterations - Session Mean & Stddev: 1814.330000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe839a00000 of size 589824 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 843193015.000000
Throughput: 84319301.500000
Num Accesses - Session Mean & Stddev: 8431930.150000,2999.893336
Num Phases Traversed: 181627.000000
Num Phases Traversed - Session Mean & Stddev: 1816.270000,0.645833
Num Path Iterations: 181627.000000
Num Path Iterations - Session Mean & Stddev: 1816.270000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7c8fe00000 of size 589824 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 846328390.000000
Throughput: 84632839.000000
Num Accesses - Session Mean & Stddev: 8463283.900000,3216.809225
Num Phases Traversed: 182302.000000
Num Phases Traversed - Session Mean & Stddev: 1823.020000,0.692532
Num Path Iterations: 182302.000000
Num Path Iterations - Session Mean & Stddev: 1823.020000,0.692532
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3964c00000 of size 589824 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 844159175.000000
Throughput: 84415917.500000
Num Accesses - Session Mean & Stddev: 8441591.750000,2815.881600
Num Phases Traversed: 181835.000000
Num Phases Traversed - Session Mean & Stddev: 1818.350000,0.606218
Num Path Iterations: 181835.000000
Num Path Iterations - Session Mean & Stddev: 1818.350000,0.606218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6f15a00000 of size 589824 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 843397395.000000
Throughput: 84339739.500000
Num Accesses - Session Mean & Stddev: 8433973.950000,2959.342587
Num Phases Traversed: 181671.000000
Num Phases Traversed - Session Mean & Stddev: 1816.710000,0.637103
Num Path Iterations: 181671.000000
Num Path Iterations - Session Mean & Stddev: 1816.710000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd096400000 of size 589824 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 845775635.000000
Throughput: 84577563.500000
Num Accesses - Session Mean & Stddev: 8457756.350000,3154.156801
Num Phases Traversed: 182183.000000
Num Phases Traversed - Session Mean & Stddev: 1821.830000,0.679043
Num Path Iterations: 182183.000000
Num Path Iterations - Session Mean & Stddev: 1821.830000,0.679043
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feeca000000 of size 589824 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 845566610.000000
Throughput: 84556661.000000
Num Accesses - Session Mean & Stddev: 8455666.100000,2690.894645
Num Phases Traversed: 182138.000000
Num Phases Traversed - Session Mean & Stddev: 1821.380000,0.579310
Num Path Iterations: 182138.000000
Num Path Iterations - Session Mean & Stddev: 1821.380000,0.579310
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fce9e400000 of size 589824 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 843866540.000000
Throughput: 84386654.000000
Num Accesses - Session Mean & Stddev: 8438665.400000,2943.625594
Num Phases Traversed: 181772.000000
Num Phases Traversed - Session Mean & Stddev: 1817.720000,0.633719
Num Path Iterations: 181772.000000
Num Path Iterations - Session Mean & Stddev: 1817.720000,0.633719
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1649a00000 of size 589824 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 842970055.000000
Throughput: 84297005.500000
Num Accesses - Session Mean & Stddev: 8429700.550000,3101.733313
Num Phases Traversed: 181579.000000
Num Phases Traversed - Session Mean & Stddev: 1815.790000,0.667757
Num Path Iterations: 181579.000000
Num Path Iterations - Session Mean & Stddev: 1815.790000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1f5a800000 of size 589824 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 845427260.000000
Throughput: 84542726.000000
Num Accesses - Session Mean & Stddev: 8454272.600000,3196.624069
Num Phases Traversed: 182108.000000
Num Phases Traversed - Session Mean & Stddev: 1821.080000,0.688186
Num Path Iterations: 182108.000000
Num Path Iterations - Session Mean & Stddev: 1821.080000,0.688186
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f936fe00000 of size 589824 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 843090825.000000
Throughput: 84309082.500000
Num Accesses - Session Mean & Stddev: 8430908.250000,3243.194750
Num Phases Traversed: 181605.000000
Num Phases Traversed - Session Mean & Stddev: 1816.050000,0.698212
Num Path Iterations: 181605.000000
Num Path Iterations - Session Mean & Stddev: 1816.050000,0.698212
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f617ee00000 of size 589824 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 843759705.000000
Throughput: 84375970.500000
Num Accesses - Session Mean & Stddev: 8437597.050000,2322.035454
Num Phases Traversed: 181749.000000
Num Phases Traversed - Session Mean & Stddev: 1817.490000,0.499900
Num Path Iterations: 181749.000000
Num Path Iterations - Session Mean & Stddev: 1817.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdeabc00000 of size 589824 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 843778285.000000
Throughput: 84377828.500000
Num Accesses - Session Mean & Stddev: 8437782.850000,2409.586755
Num Phases Traversed: 181753.000000
Num Phases Traversed - Session Mean & Stddev: 1817.530000,0.518748
Num Path Iterations: 181753.000000
Num Path Iterations - Session Mean & Stddev: 1817.530000,0.518748
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f734ee00000 of size 589824 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 846370195.000000
Throughput: 84637019.500000
Num Accesses - Session Mean & Stddev: 8463701.950000,3211.102980
Num Phases Traversed: 182311.000000
Num Phases Traversed - Session Mean & Stddev: 1823.110000,0.691303
Num Path Iterations: 182311.000000
Num Path Iterations - Session Mean & Stddev: 1823.110000,0.691303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4608,0 0/4608,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4608,0 0/4608,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4608 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0145000000 of size 589824 bytes.
[1.283s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 846435225.000000
Throughput: 84643522.500000
Num Accesses - Session Mean & Stddev: 8464352.250000,3037.062839
Num Phases Traversed: 182325.000000
Num Phases Traversed - Session Mean & Stddev: 1823.250000,0.653835
Num Path Iterations: 182325.000000
Num Path Iterations - Session Mean & Stddev: 1823.250000,0.653835
+ set +x

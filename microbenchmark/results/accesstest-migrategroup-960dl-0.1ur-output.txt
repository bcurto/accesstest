++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7c8ac00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2527219146.000000
Throughput: 252721914.600000
Num Accesses - Session Mean & Stddev: 25272191.460000,250.944792
Num Phases Traversed: 4888338.000000
Num Phases Traversed - Session Mean & Stddev: 48883.380000,0.485386
Num Path Iterations: 4888338.000000
Num Path Iterations - Session Mean & Stddev: 48883.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f09a4400000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2530585333.000000
Throughput: 253058533.300000
Num Accesses - Session Mean & Stddev: 25305853.330000,258.448295
Num Phases Traversed: 4894849.000000
Num Phases Traversed - Session Mean & Stddev: 48948.490000,0.499900
Num Path Iterations: 4894849.000000
Num Path Iterations - Session Mean & Stddev: 48948.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7e0de00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2532044824.000000
Throughput: 253204482.400000
Num Accesses - Session Mean & Stddev: 25320448.240000,302.168699
Num Phases Traversed: 4897672.000000
Num Phases Traversed - Session Mean & Stddev: 48976.720000,0.584466
Num Path Iterations: 4897672.000000
Num Path Iterations - Session Mean & Stddev: 48976.720000,0.584466
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7450e00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2517448880.000000
Throughput: 251744888.000000
Num Accesses - Session Mean & Stddev: 25174488.800000,253.277239
Num Phases Traversed: 4869440.000000
Num Phases Traversed - Session Mean & Stddev: 48694.400000,0.489898
Num Path Iterations: 4869440.000000
Num Path Iterations - Session Mean & Stddev: 48694.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fea56a00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2527053189.000000
Throughput: 252705318.900000
Num Accesses - Session Mean & Stddev: 25270531.890000,194.202157
Num Phases Traversed: 4888017.000000
Num Phases Traversed - Session Mean & Stddev: 48880.170000,0.375633
Num Path Iterations: 4888017.000000
Num Path Iterations - Session Mean & Stddev: 48880.170000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8553400000 of size 61440 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2515878751.000000
Throughput: 251587875.100000
Num Accesses - Session Mean & Stddev: 25158787.510000,287.435262
Num Phases Traversed: 4866403.000000
Num Phases Traversed - Session Mean & Stddev: 48664.030000,0.555968
Num Path Iterations: 4866403.000000
Num Path Iterations - Session Mean & Stddev: 48664.030000,0.555968
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6449200000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2536746422.000000
Throughput: 253674642.200000
Num Accesses - Session Mean & Stddev: 25367464.220000,244.907435
Num Phases Traversed: 4906766.000000
Num Phases Traversed - Session Mean & Stddev: 49067.660000,0.473709
Num Path Iterations: 4906766.000000
Num Path Iterations - Session Mean & Stddev: 49067.660000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb721800000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2519524118.000000
Throughput: 251952411.800000
Num Accesses - Session Mean & Stddev: 25195241.180000,257.671472
Num Phases Traversed: 4873454.000000
Num Phases Traversed - Session Mean & Stddev: 48734.540000,0.498397
Num Path Iterations: 4873454.000000
Num Path Iterations - Session Mean & Stddev: 48734.540000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4d37c00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2537464535.000000
Throughput: 253746453.500000
Num Accesses - Session Mean & Stddev: 25374645.350000,257.204252
Num Phases Traversed: 4908155.000000
Num Phases Traversed - Session Mean & Stddev: 49081.550000,0.497494
Num Path Iterations: 4908155.000000
Num Path Iterations - Session Mean & Stddev: 49081.550000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f53c3400000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2532843589.000000
Throughput: 253284358.900000
Num Accesses - Session Mean & Stddev: 25328435.890000,194.202157
Num Phases Traversed: 4899217.000000
Num Phases Traversed - Session Mean & Stddev: 48992.170000,0.375633
Num Path Iterations: 4899217.000000
Num Path Iterations - Session Mean & Stddev: 48992.170000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc2eee00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2531208318.000000
Throughput: 253120831.800000
Num Accesses - Session Mean & Stddev: 25312083.180000,257.671472
Num Phases Traversed: 4896054.000000
Num Phases Traversed - Session Mean & Stddev: 48960.540000,0.498397
Num Path Iterations: 4896054.000000
Num Path Iterations - Session Mean & Stddev: 48960.540000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa008800000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2526885164.000000
Throughput: 252688516.400000
Num Accesses - Session Mean & Stddev: 25268851.640000,202.621792
Num Phases Traversed: 4887692.000000
Num Phases Traversed - Session Mean & Stddev: 48876.920000,0.391918
Num Path Iterations: 4887692.000000
Num Path Iterations - Session Mean & Stddev: 48876.920000,0.391918
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f17cc200000 of size 61440 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2531768229.000000
Throughput: 253176822.900000
Num Accesses - Session Mean & Stddev: 25317682.290000,249.609827
Num Phases Traversed: 4897137.000000
Num Phases Traversed - Session Mean & Stddev: 48971.370000,0.482804
Num Path Iterations: 4897137.000000
Num Path Iterations - Session Mean & Stddev: 48971.370000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6004400000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2525371388.000000
Throughput: 252537138.800000
Num Accesses - Session Mean & Stddev: 25253713.880000,248.160000
Num Phases Traversed: 4884764.000000
Num Phases Traversed - Session Mean & Stddev: 48847.640000,0.480000
Num Path Iterations: 4884764.000000
Num Path Iterations - Session Mean & Stddev: 48847.640000,0.480000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff074200000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2530720270.000000
Throughput: 253072027.000000
Num Accesses - Session Mean & Stddev: 25307202.700000,225.355075
Num Phases Traversed: 4895110.000000
Num Phases Traversed - Session Mean & Stddev: 48951.100000,0.435890
Num Path Iterations: 4895110.000000
Num Path Iterations - Session Mean & Stddev: 48951.100000,0.435890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd222c00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2529435525.000000
Throughput: 252943552.500000
Num Accesses - Session Mean & Stddev: 25294355.250000,330.030707
Num Phases Traversed: 4892625.000000
Num Phases Traversed - Session Mean & Stddev: 48926.250000,0.638357
Num Path Iterations: 4892625.000000
Num Path Iterations - Session Mean & Stddev: 48926.250000,0.638357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f59dd800000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2526785900.000000
Throughput: 252678590.000000
Num Accesses - Session Mean & Stddev: 25267859.000000,206.800000
Num Phases Traversed: 4887500.000000
Num Phases Traversed - Session Mean & Stddev: 48875.000000,0.400000
Num Path Iterations: 4887500.000000
Num Path Iterations - Session Mean & Stddev: 48875.000000,0.400000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe51c200000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2533572559.000000
Throughput: 253357255.900000
Num Accesses - Session Mean & Stddev: 25335725.590000,229.527040
Num Phases Traversed: 4900627.000000
Num Phases Traversed - Session Mean & Stddev: 49006.270000,0.443959
Num Path Iterations: 4900627.000000
Num Path Iterations - Session Mean & Stddev: 49006.270000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff5f2000000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2522597166.000000
Throughput: 252259716.600000
Num Accesses - Session Mean & Stddev: 25225971.660000,193.167141
Num Phases Traversed: 4879398.000000
Num Phases Traversed - Session Mean & Stddev: 48793.980000,0.373631
Num Path Iterations: 4879398.000000
Num Path Iterations - Session Mean & Stddev: 48793.980000,0.373631
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc3f1000000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2527968279.000000
Throughput: 252796827.900000
Num Accesses - Session Mean & Stddev: 25279682.790000,173.868876
Num Phases Traversed: 4889787.000000
Num Phases Traversed - Session Mean & Stddev: 48897.870000,0.336303
Num Path Iterations: 4889787.000000
Num Path Iterations - Session Mean & Stddev: 48897.870000,0.336303
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe000000000 of size 12582912 bytes.
[2.091s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 724871611.000000
Throughput: 72487161.100000
Num Accesses - Session Mean & Stddev: 7248716.110000,61090.265061
Num Phases Traversed: 7471.000000
Num Phases Traversed - Session Mean & Stddev: 74.710000,0.621208
Num Path Iterations: 7471.000000
Num Path Iterations - Session Mean & Stddev: 74.710000,0.621208
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc280000000 of size 12582912 bytes.
[2.060s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 728411887.000000
Throughput: 72841188.700000
Num Accesses - Session Mean & Stddev: 7284118.870000,50633.679006
Num Phases Traversed: 7507.000000
Num Phases Traversed - Session Mean & Stddev: 75.070000,0.514879
Num Path Iterations: 7507.000000
Num Path Iterations - Session Mean & Stddev: 75.070000,0.514879
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5340000000 of size 12582912 bytes.
[2.082s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 729001933.000000
Throughput: 72900193.300000
Num Accesses - Session Mean & Stddev: 7290019.330000,33072.416044
Num Phases Traversed: 7513.000000
Num Phases Traversed - Session Mean & Stddev: 75.130000,0.336303
Num Path Iterations: 7513.000000
Num Path Iterations - Session Mean & Stddev: 75.130000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f86c0000000 of size 12582912 bytes.
[2.082s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 702449863.000000
Throughput: 70244986.300000
Num Accesses - Session Mean & Stddev: 7024498.630000,54319.495036
Num Phases Traversed: 7243.000000
Num Phases Traversed - Session Mean & Stddev: 72.430000,0.552359
Num Path Iterations: 7243.000000
Num Path Iterations - Session Mean & Stddev: 72.430000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2700000000 of size 12582912 bytes.
[2.086s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 734902393.000000
Throughput: 73490239.300000
Num Accesses - Session Mean & Stddev: 7349023.930000,63511.842962
Num Phases Traversed: 7573.000000
Num Phases Traversed - Session Mean & Stddev: 75.730000,0.645833
Num Path Iterations: 7573.000000
Num Path Iterations - Session Mean & Stddev: 75.730000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb8c0000000 of size 12582912 bytes.
[2.091s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 725658339.000000
Throughput: 72565833.900000
Num Accesses - Session Mean & Stddev: 7256583.390000,65667.934505
Num Phases Traversed: 7479.000000
Num Phases Traversed - Session Mean & Stddev: 74.790000,0.667757
Num Path Iterations: 7479.000000
Num Path Iterations - Session Mean & Stddev: 74.790000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc440000000 of size 12582912 bytes.
[2.127s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 733132255.000000
Throughput: 73313225.500000
Num Accesses - Session Mean & Stddev: 7331322.550000,48924.029776
Num Phases Traversed: 7555.000000
Num Phases Traversed - Session Mean & Stddev: 75.550000,0.497494
Num Path Iterations: 7555.000000
Num Path Iterations - Session Mean & Stddev: 75.550000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc3c0000000 of size 12582912 bytes.
[2.086s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 716807649.000000
Throughput: 71680764.900000
Num Accesses - Session Mean & Stddev: 7168076.490000,67983.439859
Num Phases Traversed: 7389.000000
Num Phases Traversed - Session Mean & Stddev: 73.890000,0.691303
Num Path Iterations: 7389.000000
Num Path Iterations - Session Mean & Stddev: 73.890000,0.691303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcac0000000 of size 12582912 bytes.
[2.059s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 729887002.000000
Throughput: 72988700.200000
Num Accesses - Session Mean & Stddev: 7298870.020000,40737.395737
Num Phases Traversed: 7522.000000
Num Phases Traversed - Session Mean & Stddev: 75.220000,0.414246
Num Path Iterations: 7522.000000
Num Path Iterations - Session Mean & Stddev: 75.220000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc540000000 of size 12582912 bytes.
[2.064s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 735394098.000000
Throughput: 73539409.800000
Num Accesses - Session Mean & Stddev: 7353940.980000,49327.594649
Num Phases Traversed: 7578.000000
Num Phases Traversed - Session Mean & Stddev: 75.780000,0.501597
Num Path Iterations: 7578.000000
Num Path Iterations - Session Mean & Stddev: 75.780000,0.501597
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9c80000000 of size 12582912 bytes.
[2.070s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 735492439.000000
Throughput: 73549243.900000
Num Accesses - Session Mean & Stddev: 7354924.390000,40055.099344
Num Phases Traversed: 7579.000000
Num Phases Traversed - Session Mean & Stddev: 75.790000,0.407308
Num Path Iterations: 7579.000000
Num Path Iterations - Session Mean & Stddev: 75.790000,0.407308
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9700000000 of size 12582912 bytes.
[2.071s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 731460458.000000
Throughput: 73146045.800000
Num Accesses - Session Mean & Stddev: 7314604.580000,49718.159691
Num Phases Traversed: 7538.000000
Num Phases Traversed - Session Mean & Stddev: 75.380000,0.505569
Num Path Iterations: 7538.000000
Num Path Iterations - Session Mean & Stddev: 75.380000,0.505569
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc2c0000000 of size 12582912 bytes.
[2.067s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 729296956.000000
Throughput: 72929695.600000
Num Accesses - Session Mean & Stddev: 7292969.560000,55141.158830
Num Phases Traversed: 7516.000000
Num Phases Traversed - Session Mean & Stddev: 75.160000,0.560714
Num Path Iterations: 7516.000000
Num Path Iterations - Session Mean & Stddev: 75.160000,0.560714
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb400000000 of size 12582912 bytes.
[2.086s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 737065895.000000
Throughput: 73706589.500000
Num Accesses - Session Mean & Stddev: 7370658.950000,25549.741270
Num Phases Traversed: 7595.000000
Num Phases Traversed - Session Mean & Stddev: 75.950000,0.259808
Num Path Iterations: 7595.000000
Num Path Iterations - Session Mean & Stddev: 75.950000,0.259808
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb1c0000000 of size 12582912 bytes.
[2.084s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 737065895.000000
Throughput: 73706589.500000
Num Accesses - Session Mean & Stddev: 7370658.950000,68662.866507
Num Phases Traversed: 7595.000000
Num Phases Traversed - Session Mean & Stddev: 75.950000,0.698212
Num Path Iterations: 7595.000000
Num Path Iterations - Session Mean & Stddev: 75.950000,0.698212
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc280000000 of size 12582912 bytes.
[2.088s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 741884604.000000
Throughput: 74188460.400000
Num Accesses - Session Mean & Stddev: 7418846.040000,48815.188641
Num Phases Traversed: 7644.000000
Num Phases Traversed - Session Mean & Stddev: 76.440000,0.496387
Num Path Iterations: 7644.000000
Num Path Iterations - Session Mean & Stddev: 76.440000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0240000000 of size 12582912 bytes.
[2.103s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 732935573.000000
Throughput: 73293557.300000
Num Accesses - Session Mean & Stddev: 7329355.730000,49081.913300
Num Phases Traversed: 7553.000000
Num Phases Traversed - Session Mean & Stddev: 75.530000,0.499099
Num Path Iterations: 7553.000000
Num Path Iterations - Session Mean & Stddev: 75.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8bc0000000 of size 12582912 bytes.
[2.067s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 708153641.000000
Throughput: 70815364.100000
Num Accesses - Session Mean & Stddev: 7081536.410000,68831.675284
Num Phases Traversed: 7301.000000
Num Phases Traversed - Session Mean & Stddev: 73.010000,0.699929
Num Path Iterations: 7301.000000
Num Path Iterations - Session Mean & Stddev: 73.010000,0.699929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcc00000000 of size 12582912 bytes.
[2.091s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 726150044.000000
Throughput: 72615004.400000
Num Accesses - Session Mean & Stddev: 7261500.440000,61759.400739
Num Phases Traversed: 7484.000000
Num Phases Traversed - Session Mean & Stddev: 74.840000,0.628013
Num Path Iterations: 7484.000000
Num Path Iterations - Session Mean & Stddev: 74.840000,0.628013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7f00000000 of size 12582912 bytes.
[2.061s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 725658339.000000
Throughput: 72565833.900000
Num Accesses - Session Mean & Stddev: 7256583.390000,52582.556586
Num Phases Traversed: 7479.000000
Num Phases Traversed - Session Mean & Stddev: 74.790000,0.534696
Num Path Iterations: 7479.000000
Num Path Iterations - Session Mean & Stddev: 74.790000,0.534696
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd540000000 of size 23592960 bytes.
[4.807s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faf40000000 of size 23592960 bytes.
[4.820s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbf80000000 of size 23592960 bytes.
[4.810s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f73c0000000 of size 23592960 bytes.
[4.777s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 228787137.000000
Throughput: 22878713.700000
Num Accesses - Session Mean & Stddev: 2287871.370000,90672.912627
Num Phases Traversed: 1341.000000
Num Phases Traversed - Session Mean & Stddev: 13.410000,0.491833
Num Path Iterations: 1341.000000
Num Path Iterations - Session Mean & Stddev: 13.410000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb5c0000000 of size 23592960 bytes.
[4.808s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc1c0000000 of size 23592960 bytes.
[4.813s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7900000000 of size 23592960 bytes.
[4.812s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7facc0000000 of size 23592960 bytes.
[4.808s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc200000000 of size 23592960 bytes.
[4.819s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4b40000000 of size 23592960 bytes.
[4.809s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3340000000 of size 23592960 bytes.
[4.811s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1780000000 of size 23592960 bytes.
[4.811s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9040000000 of size 23592960 bytes.
[4.811s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7940000000 of size 23592960 bytes.
[4.808s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5940000000 of size 23592960 bytes.
[4.809s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1340000000 of size 23592960 bytes.
[4.809s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1280000000 of size 23592960 bytes.
[4.748s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5900000000 of size 23592960 bytes.
[4.807s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f33c0000000 of size 23592960 bytes.
[4.809s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/184320,0.1 0/184320,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/184320,0.1 0/184320,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    184320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4400000000 of size 23592960 bytes.
[4.810s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 228971494.000000
Throughput: 22897149.400000
Num Accesses - Session Mean & Stddev: 2289714.940000,90990.965706
Num Phases Traversed: 1342.000000
Num Phases Traversed - Session Mean & Stddev: 13.420000,0.493559
Num Path Iterations: 1342.000000
Num Path Iterations - Session Mean & Stddev: 13.420000,0.493559
+ set +x

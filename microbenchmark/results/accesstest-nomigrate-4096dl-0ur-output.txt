++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efcf5400000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1844943625.000000
Throughput: 184494362.500000
Num Accesses - Session Mean & Stddev: 18449436.250000,18768.185146
Num Phases Traversed: 884965.000000
Num Phases Traversed - Session Mean & Stddev: 8849.650000,9.001528
Num Path Iterations: 884965.000000
Num Path Iterations - Session Mean & Stddev: 8849.650000,9.001528
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f888dc00000 of size 262144 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1833511570.000000
Throughput: 183351157.000000
Num Accesses - Session Mean & Stddev: 18335115.700000,10237.243839
Num Phases Traversed: 879482.000000
Num Phases Traversed - Session Mean & Stddev: 8794.820000,4.909949
Num Path Iterations: 879482.000000
Num Path Iterations - Session Mean & Stddev: 8794.820000,4.909949
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc3ad400000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1830196420.000000
Throughput: 183019642.000000
Num Accesses - Session Mean & Stddev: 18301964.200000,15743.282608
Num Phases Traversed: 877892.000000
Num Phases Traversed - Session Mean & Stddev: 8778.920000,7.550735
Num Path Iterations: 877892.000000
Num Path Iterations - Session Mean & Stddev: 8778.920000,7.550735
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4e3ec00000 of size 262144 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1836901780.000000
Throughput: 183690178.000000
Num Accesses - Session Mean & Stddev: 18369017.800000,2258.739308
Num Phases Traversed: 881108.000000
Num Phases Traversed - Session Mean & Stddev: 8811.080000,1.083328
Num Path Iterations: 881108.000000
Num Path Iterations - Session Mean & Stddev: 8811.080000,1.083328
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f95a7600000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1831468270.000000
Throughput: 183146827.000000
Num Accesses - Session Mean & Stddev: 18314682.700000,80006.957101
Num Phases Traversed: 878502.000000
Num Phases Traversed - Session Mean & Stddev: 8785.020000,38.372641
Num Path Iterations: 878502.000000
Num Path Iterations - Session Mean & Stddev: 8785.020000,38.372641
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1ad3000000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1855114255.000000
Throughput: 185511425.500000
Num Accesses - Session Mean & Stddev: 18551142.550000,65066.870632
Num Phases Traversed: 889843.000000
Num Phases Traversed - Session Mean & Stddev: 8898.430000,31.207132
Num Path Iterations: 889843.000000
Num Path Iterations - Session Mean & Stddev: 8898.430000,31.207132
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8741a00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1837431370.000000
Throughput: 183743137.000000
Num Accesses - Session Mean & Stddev: 18374313.700000,91727.100525
Num Phases Traversed: 881362.000000
Num Phases Traversed - Session Mean & Stddev: 8813.620000,43.993813
Num Path Iterations: 881362.000000
Num Path Iterations - Session Mean & Stddev: 8813.620000,43.993813
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1653600000 of size 262144 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1838161120.000000
Throughput: 183816112.000000
Num Accesses - Session Mean & Stddev: 18381611.200000,5772.701964
Num Phases Traversed: 881712.000000
Num Phases Traversed - Session Mean & Stddev: 8817.120000,2.768682
Num Path Iterations: 881712.000000
Num Path Iterations - Session Mean & Stddev: 8817.120000,2.768682
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faebee00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1834034905.000000
Throughput: 183403490.500000
Num Accesses - Session Mean & Stddev: 18340349.050000,2246.678203
Num Phases Traversed: 879733.000000
Num Phases Traversed - Session Mean & Stddev: 8797.330000,1.077544
Num Path Iterations: 879733.000000
Num Path Iterations - Session Mean & Stddev: 8797.330000,1.077544
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7d46c00000 of size 262144 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1842975385.000000
Throughput: 184297538.500000
Num Accesses - Session Mean & Stddev: 18429753.850000,7254.451814
Num Phases Traversed: 884021.000000
Num Phases Traversed - Session Mean & Stddev: 8840.210000,3.479353
Num Path Iterations: 884021.000000
Num Path Iterations - Session Mean & Stddev: 8840.210000,3.479353
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd58ac00000 of size 262144 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1853563015.000000
Throughput: 185356301.500000
Num Accesses - Session Mean & Stddev: 18535630.150000,19912.557785
Num Phases Traversed: 889099.000000
Num Phases Traversed - Session Mean & Stddev: 8890.990000,9.550387
Num Path Iterations: 889099.000000
Num Path Iterations - Session Mean & Stddev: 8890.990000,9.550387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faac9c00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1835012770.000000
Throughput: 183501277.000000
Num Accesses - Session Mean & Stddev: 18350127.700000,4835.941647
Num Phases Traversed: 880202.000000
Num Phases Traversed - Session Mean & Stddev: 8802.020000,2.319396
Num Path Iterations: 880202.000000
Num Path Iterations - Session Mean & Stddev: 8802.020000,2.319396
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5f82e00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1839001375.000000
Throughput: 183900137.500000
Num Accesses - Session Mean & Stddev: 18390013.750000,125431.553453
Num Phases Traversed: 882115.000000
Num Phases Traversed - Session Mean & Stddev: 8821.150000,60.159018
Num Path Iterations: 882115.000000
Num Path Iterations - Session Mean & Stddev: 8821.150000,60.159018
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f094f800000 of size 262144 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1843183885.000000
Throughput: 184318388.500000
Num Accesses - Session Mean & Stddev: 18431838.850000,112923.990744
Num Phases Traversed: 884121.000000
Num Phases Traversed - Session Mean & Stddev: 8841.210000,54.160187
Num Path Iterations: 884121.000000
Num Path Iterations - Session Mean & Stddev: 8841.210000,54.160187
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0664800000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1842456220.000000
Throughput: 184245622.000000
Num Accesses - Session Mean & Stddev: 18424562.200000,2341.892196
Num Phases Traversed: 883772.000000
Num Phases Traversed - Session Mean & Stddev: 8837.720000,1.123210
Num Path Iterations: 883772.000000
Num Path Iterations - Session Mean & Stddev: 8837.720000,1.123210
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1ddf600000 of size 262144 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1845777625.000000
Throughput: 184577762.500000
Num Accesses - Session Mean & Stddev: 18457776.250000,60088.799291
Num Phases Traversed: 885365.000000
Num Phases Traversed - Session Mean & Stddev: 8853.650000,28.819568
Num Path Iterations: 885365.000000
Num Path Iterations - Session Mean & Stddev: 8853.650000,28.819568
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f62dee00000 of size 262144 bytes.
[1.237s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1829733550.000000
Throughput: 182973355.000000
Num Accesses - Session Mean & Stddev: 18297335.500000,16800.753532
Num Phases Traversed: 877670.000000
Num Phases Traversed - Session Mean & Stddev: 8776.700000,8.057915
Num Path Iterations: 877670.000000
Num Path Iterations - Session Mean & Stddev: 8776.700000,8.057915
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f218f400000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1827029305.000000
Throughput: 182702930.500000
Num Accesses - Session Mean & Stddev: 18270293.050000,46406.924678
Num Phases Traversed: 876373.000000
Num Phases Traversed - Session Mean & Stddev: 8763.730000,22.257518
Num Path Iterations: 876373.000000
Num Path Iterations - Session Mean & Stddev: 8763.730000,22.257518
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fde82200000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1840106425.000000
Throughput: 184010642.500000
Num Accesses - Session Mean & Stddev: 18401064.250000,9601.758911
Num Phases Traversed: 882645.000000
Num Phases Traversed - Session Mean & Stddev: 8826.450000,4.605160
Num Path Iterations: 882645.000000
Num Path Iterations - Session Mean & Stddev: 8826.450000,4.605160
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,0 0/2048,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,0 0/2048,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7b6d200000 of size 262144 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1845936085.000000
Throughput: 184593608.500000
Num Accesses - Session Mean & Stddev: 18459360.850000,73531.713061
Num Phases Traversed: 885441.000000
Num Phases Traversed - Session Mean & Stddev: 8854.410000,35.267009
Num Path Iterations: 885441.000000
Num Path Iterations - Session Mean & Stddev: 8854.410000,35.267009
+ set +x

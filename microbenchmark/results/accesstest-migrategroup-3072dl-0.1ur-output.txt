++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7feb800000 of size 196608 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1973542528.000000
Throughput: 197354252.800000
Num Accesses - Session Mean & Stddev: 19735425.280000,931.130969
Num Phases Traversed: 1254736.000000
Num Phases Traversed - Session Mean & Stddev: 12547.360000,0.591946
Num Path Iterations: 1254736.000000
Num Path Iterations - Session Mean & Stddev: 12547.360000,0.591946
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f071dc00000 of size 196608 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1977113238.000000
Throughput: 197711323.800000
Num Accesses - Session Mean & Stddev: 19771132.380000,1062.677635
Num Phases Traversed: 1257006.000000
Num Phases Traversed - Session Mean & Stddev: 12570.060000,0.675574
Num Path Iterations: 1257006.000000
Num Path Iterations - Session Mean & Stddev: 12570.060000,0.675574
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f622b200000 of size 196608 bytes.
[1.263s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1977311436.000000
Throughput: 197731143.600000
Num Accesses - Session Mean & Stddev: 19773114.360000,940.648739
Num Phases Traversed: 1257132.000000
Num Phases Traversed - Session Mean & Stddev: 12571.320000,0.597997
Num Path Iterations: 1257132.000000
Num Path Iterations - Session Mean & Stddev: 12571.320000,0.597997
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6c94a00000 of size 196608 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974171728.000000
Throughput: 197417172.800000
Num Accesses - Session Mean & Stddev: 19741717.280000,931.130969
Num Phases Traversed: 1255136.000000
Num Phases Traversed - Session Mean & Stddev: 12551.360000,0.591946
Num Path Iterations: 1255136.000000
Num Path Iterations - Session Mean & Stddev: 12551.360000,0.591946
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd47f400000 of size 196608 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1970934494.000000
Throughput: 197093449.400000
Num Accesses - Session Mean & Stddev: 19709344.940000,1033.402340
Num Phases Traversed: 1253078.000000
Num Phases Traversed - Session Mean & Stddev: 12530.780000,0.656963
Num Path Iterations: 1253078.000000
Num Path Iterations - Session Mean & Stddev: 12530.780000,0.656963
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff4cbe00000 of size 196608 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1973358487.000000
Throughput: 197335848.700000
Num Accesses - Session Mean & Stddev: 19733584.870000,1036.152186
Num Phases Traversed: 1254619.000000
Num Phases Traversed - Session Mean & Stddev: 12546.190000,0.658711
Num Path Iterations: 1254619.000000
Num Path Iterations - Session Mean & Stddev: 12546.190000,0.658711
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0476c00000 of size 196608 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1977173012.000000
Throughput: 197717301.200000
Num Accesses - Session Mean & Stddev: 19771730.120000,841.812227
Num Phases Traversed: 1257044.000000
Num Phases Traversed - Session Mean & Stddev: 12570.440000,0.535164
Num Path Iterations: 1257044.000000
Num Path Iterations - Session Mean & Stddev: 12570.440000,0.535164
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbe53800000 of size 196608 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974995980.000000
Throughput: 197499598.000000
Num Accesses - Session Mean & Stddev: 19749959.800000,889.823173
Num Phases Traversed: 1255660.000000
Num Phases Traversed - Session Mean & Stddev: 12556.600000,0.565685
Num Path Iterations: 1255660.000000
Num Path Iterations - Session Mean & Stddev: 12556.600000,0.565685
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff82ca00000 of size 196608 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974789917.000000
Throughput: 197478991.700000
Num Accesses - Session Mean & Stddev: 19747899.170000,977.160970
Num Phases Traversed: 1255529.000000
Num Phases Traversed - Session Mean & Stddev: 12555.290000,0.621208
Num Path Iterations: 1255529.000000
Num Path Iterations - Session Mean & Stddev: 12555.290000,0.621208
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0c41200000 of size 196608 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1977967377.000000
Throughput: 197796737.700000
Num Accesses - Session Mean & Stddev: 19779673.770000,817.203400
Num Phases Traversed: 1257549.000000
Num Phases Traversed - Session Mean & Stddev: 12575.490000,0.519519
Num Path Iterations: 1257549.000000
Num Path Iterations - Session Mean & Stddev: 12575.490000,0.519519
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f00c8800000 of size 196608 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1978324448.000000
Throughput: 197832444.800000
Num Accesses - Session Mean & Stddev: 19783244.480000,1022.328993
Num Phases Traversed: 1257776.000000
Num Phases Traversed - Session Mean & Stddev: 12577.760000,0.649923
Num Path Iterations: 1257776.000000
Num Path Iterations - Session Mean & Stddev: 12577.760000,0.649923
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f67caa00000 of size 196608 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1975983824.000000
Throughput: 197598382.400000
Num Accesses - Session Mean & Stddev: 19759838.240000,1073.334795
Num Phases Traversed: 1256288.000000
Num Phases Traversed - Session Mean & Stddev: 12562.880000,0.682349
Num Path Iterations: 1256288.000000
Num Path Iterations - Session Mean & Stddev: 12562.880000,0.682349
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f70b2e00000 of size 196608 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974862275.000000
Throughput: 197486227.500000
Num Accesses - Session Mean & Stddev: 19748622.750000,1004.135981
Num Phases Traversed: 1255575.000000
Num Phases Traversed - Session Mean & Stddev: 12555.750000,0.638357
Num Path Iterations: 1255575.000000
Num Path Iterations - Session Mean & Stddev: 12555.750000,0.638357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f20c0c00000 of size 196608 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1976281121.000000
Throughput: 197628112.100000
Num Accesses - Session Mean & Stddev: 19762811.210000,1015.894988
Num Phases Traversed: 1256477.000000
Num Phases Traversed - Session Mean & Stddev: 12564.770000,0.645833
Num Path Iterations: 1256477.000000
Num Path Iterations - Session Mean & Stddev: 12564.770000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe74aa00000 of size 196608 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1975137550.000000
Throughput: 197513755.000000
Num Accesses - Session Mean & Stddev: 19751375.500000,786.500000
Num Phases Traversed: 1255750.000000
Num Phases Traversed - Session Mean & Stddev: 12557.500000,0.500000
Num Path Iterations: 1255750.000000
Num Path Iterations - Session Mean & Stddev: 12557.500000,0.500000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd1be800000 of size 196608 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1976896164.000000
Throughput: 197689616.400000
Num Accesses - Session Mean & Stddev: 19768961.640000,966.595381
Num Phases Traversed: 1256868.000000
Num Phases Traversed - Session Mean & Stddev: 12568.680000,0.614492
Num Path Iterations: 1256868.000000
Num Path Iterations - Session Mean & Stddev: 12568.680000,0.614492
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6205e00000 of size 196608 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1973849263.000000
Throughput: 197384926.300000
Num Accesses - Session Mean & Stddev: 19738492.630000,961.848020
Num Phases Traversed: 1254931.000000
Num Phases Traversed - Session Mean & Stddev: 12549.310000,0.611474
Num Path Iterations: 1254931.000000
Num Path Iterations - Session Mean & Stddev: 12549.310000,0.611474
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fca4e600000 of size 196608 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1977328739.000000
Throughput: 197732873.900000
Num Accesses - Session Mean & Stddev: 19773287.390000,839.899517
Num Phases Traversed: 1257143.000000
Num Phases Traversed - Session Mean & Stddev: 12571.430000,0.533948
Num Path Iterations: 1257143.000000
Num Path Iterations - Session Mean & Stddev: 12571.430000,0.533948
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6299c00000 of size 196608 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1973578707.000000
Throughput: 197357870.700000
Num Accesses - Session Mean & Stddev: 19735787.070000,892.460926
Num Phases Traversed: 1254759.000000
Num Phases Traversed - Session Mean & Stddev: 12547.590000,0.567362
Num Path Iterations: 1254759.000000
Num Path Iterations - Session Mean & Stddev: 12547.590000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1536,0.1 0/1536,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1536,0.1 0/1536,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1536 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff24f000000 of size 196608 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1977506488.000000
Throughput: 197750648.800000
Num Accesses - Session Mean & Stddev: 19775064.880000,841.812227
Num Phases Traversed: 1257256.000000
Num Phases Traversed - Session Mean & Stddev: 12572.560000,0.535164
Num Path Iterations: 1257256.000000
Num Path Iterations - Session Mean & Stddev: 12572.560000,0.535164
+ set +x

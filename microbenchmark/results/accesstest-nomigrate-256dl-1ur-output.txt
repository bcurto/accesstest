++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8ac0c00000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 887689870.000000
Throughput: 88768987.000000
Num Accesses - Session Mean & Stddev: 8876898.700000,271.904781
Num Phases Traversed: 5380038.000000
Num Phases Traversed - Session Mean & Stddev: 53800.380000,1.647908
Num Path Iterations: 5380038.000000
Num Path Iterations - Session Mean & Stddev: 53800.380000,1.647908
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f16a6600000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 886381915.000000
Throughput: 88638191.500000
Num Accesses - Session Mean & Stddev: 8863819.150000,171.306239
Num Phases Traversed: 5372111.000000
Num Phases Traversed - Session Mean & Stddev: 53721.110000,1.038220
Num Path Iterations: 5372111.000000
Num Path Iterations - Session Mean & Stddev: 53721.110000,1.038220
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc16f000000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 886657630.000000
Throughput: 88665763.000000
Num Accesses - Session Mean & Stddev: 8866576.300000,210.502755
Num Phases Traversed: 5373782.000000
Num Phases Traversed - Session Mean & Stddev: 53737.820000,1.275774
Num Path Iterations: 5373782.000000
Num Path Iterations - Session Mean & Stddev: 53737.820000,1.275774
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6386600000 of size 16384 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 890131375.000000
Throughput: 89013137.500000
Num Accesses - Session Mean & Stddev: 8901313.750000,281.347983
Num Phases Traversed: 5394835.000000
Num Phases Traversed - Session Mean & Stddev: 53948.350000,1.705139
Num Path Iterations: 5394835.000000
Num Path Iterations - Session Mean & Stddev: 53948.350000,1.705139
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f09b5400000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 888477250.000000
Throughput: 88847725.000000
Num Accesses - Session Mean & Stddev: 8884772.500000,262.448566
Num Phases Traversed: 5384810.000000
Num Phases Traversed - Session Mean & Stddev: 53848.100000,1.590597
Num Path Iterations: 5384810.000000
Num Path Iterations - Session Mean & Stddev: 53848.100000,1.590597
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0e9a600000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 890644855.000000
Throughput: 89064485.500000
Num Accesses - Session Mean & Stddev: 8906448.550000,179.925672
Num Phases Traversed: 5397947.000000
Num Phases Traversed - Session Mean & Stddev: 53979.470000,1.090459
Num Path Iterations: 5397947.000000
Num Path Iterations - Session Mean & Stddev: 53979.470000,1.090459
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5b4a200000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 886937635.000000
Throughput: 88693763.500000
Num Accesses - Session Mean & Stddev: 8869376.350000,284.159511
Num Phases Traversed: 5375479.000000
Num Phases Traversed - Session Mean & Stddev: 53754.790000,1.722179
Num Path Iterations: 5375479.000000
Num Path Iterations - Session Mean & Stddev: 53754.790000,1.722179
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb10e000000 of size 16384 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 888210940.000000
Throughput: 88821094.000000
Num Accesses - Session Mean & Stddev: 8882109.400000,296.008344
Num Phases Traversed: 5383196.000000
Num Phases Traversed - Session Mean & Stddev: 53831.960000,1.793990
Num Path Iterations: 5383196.000000
Num Path Iterations - Session Mean & Stddev: 53831.960000,1.793990
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa217200000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 890208430.000000
Throughput: 89020843.000000
Num Accesses - Session Mean & Stddev: 8902084.300000,351.554562
Num Phases Traversed: 5395302.000000
Num Phases Traversed - Session Mean & Stddev: 53953.020000,2.130634
Num Path Iterations: 5395302.000000
Num Path Iterations - Session Mean & Stddev: 53953.020000,2.130634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3afb000000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 890667625.000000
Throughput: 89066762.500000
Num Accesses - Session Mean & Stddev: 8906676.250000,195.752874
Num Phases Traversed: 5398085.000000
Num Phases Traversed - Session Mean & Stddev: 53980.850000,1.186381
Num Path Iterations: 5398085.000000
Num Path Iterations - Session Mean & Stddev: 53980.850000,1.186381
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f82e9000000 of size 16384 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 887055280.000000
Throughput: 88705528.000000
Num Accesses - Session Mean & Stddev: 8870552.800000,291.149034
Num Phases Traversed: 5376192.000000
Num Phases Traversed - Session Mean & Stddev: 53761.920000,1.764540
Num Path Iterations: 5376192.000000
Num Path Iterations - Session Mean & Stddev: 53761.920000,1.764540
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f08a6000000 of size 16384 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 884518075.000000
Throughput: 88451807.500000
Num Accesses - Session Mean & Stddev: 8845180.750000,284.236147
Num Phases Traversed: 5360815.000000
Num Phases Traversed - Session Mean & Stddev: 53608.150000,1.722643
Num Path Iterations: 5360815.000000
Num Path Iterations - Session Mean & Stddev: 53608.150000,1.722643
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f950c400000 of size 16384 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 888059965.000000
Throughput: 88805996.500000
Num Accesses - Session Mean & Stddev: 8880599.650000,137.446089
Num Phases Traversed: 5382281.000000
Num Phases Traversed - Session Mean & Stddev: 53822.810000,0.833007
Num Path Iterations: 5382281.000000
Num Path Iterations - Session Mean & Stddev: 53822.810000,0.833007
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2f16200000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 889210840.000000
Throughput: 88921084.000000
Num Accesses - Session Mean & Stddev: 8892108.400000,194.280056
Num Phases Traversed: 5389256.000000
Num Phases Traversed - Session Mean & Stddev: 53892.560000,1.177455
Num Path Iterations: 5389256.000000
Num Path Iterations - Session Mean & Stddev: 53892.560000,1.177455
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f84a3400000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 888283375.000000
Throughput: 88828337.500000
Num Accesses - Session Mean & Stddev: 8882833.750000,290.863692
Num Phases Traversed: 5383635.000000
Num Phases Traversed - Session Mean & Stddev: 53836.350000,1.762810
Num Path Iterations: 5383635.000000
Num Path Iterations - Session Mean & Stddev: 53836.350000,1.762810
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f026f400000 of size 16384 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 888729700.000000
Throughput: 88872970.000000
Num Accesses - Session Mean & Stddev: 8887297.000000,306.917741
Num Phases Traversed: 5386340.000000
Num Phases Traversed - Session Mean & Stddev: 53863.400000,1.860108
Num Path Iterations: 5386340.000000
Num Path Iterations - Session Mean & Stddev: 53863.400000,1.860108
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fca97200000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 888229255.000000
Throughput: 88822925.500000
Num Accesses - Session Mean & Stddev: 8882292.550000,175.016706
Num Phases Traversed: 5383307.000000
Num Phases Traversed - Session Mean & Stddev: 53833.070000,1.060707
Num Path Iterations: 5383307.000000
Num Path Iterations - Session Mean & Stddev: 53833.070000,1.060707
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f174f000000 of size 16384 bytes.
[1.261s] Test is ready to start.
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9e97000000 of size 16384 bytes.
[1.266s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 889613770.000000
Throughput: 88961377.000000
Num Accesses - Session Mean & Stddev: 8896137.700000,262.946021
Num Phases Traversed: 5391698.000000
Num Phases Traversed - Session Mean & Stddev: 53916.980000,1.593612
Num Path Iterations: 5391698.000000
Num Path Iterations - Session Mean & Stddev: 53916.980000,1.593612
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,1 0/128,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,1 0/128,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6cac600000 of size 16384 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 890316340.000000
Throughput: 89031634.000000
Num Accesses - Session Mean & Stddev: 8903163.400000,259.128810
Num Phases Traversed: 5395956.000000
Num Phases Traversed - Session Mean & Stddev: 53959.560000,1.570478
Num Path Iterations: 5395956.000000
Num Path Iterations - Session Mean & Stddev: 53959.560000,1.570478
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff540000000 of size 17301504 bytes.
[3.591s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 468620630.000000
Throughput: 46862063.000000
Num Accesses - Session Mean & Stddev: 4686206.300000,64047.794423
Num Phases Traversed: 3566.000000
Num Phases Traversed - Session Mean & Stddev: 35.660000,0.473709
Num Path Iterations: 3566.000000
Num Path Iterations - Session Mean & Stddev: 35.660000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdc00000000 of size 17301504 bytes.
[3.569s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 465510915.000000
Throughput: 46551091.500000
Num Accesses - Session Mean & Stddev: 4655109.150000,66936.717019
Num Phases Traversed: 3543.000000
Num Phases Traversed - Session Mean & Stddev: 35.430000,0.495076
Num Path Iterations: 3543.000000
Num Path Iterations - Session Mean & Stddev: 35.430000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5c80000000 of size 17301504 bytes.
[3.562s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 460778740.000000
Throughput: 46077874.000000
Num Accesses - Session Mean & Stddev: 4607787.400000,93046.191018
Num Phases Traversed: 3508.000000
Num Phases Traversed - Session Mean & Stddev: 35.080000,0.688186
Num Path Iterations: 3508.000000
Num Path Iterations - Session Mean & Stddev: 35.080000,0.688186
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1c40000000 of size 17301504 bytes.
[3.603s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 460643535.000000
Throughput: 46064353.500000
Num Accesses - Session Mean & Stddev: 4606435.350000,88153.245260
Num Phases Traversed: 3507.000000
Num Phases Traversed - Session Mean & Stddev: 35.070000,0.651997
Num Path Iterations: 3507.000000
Num Path Iterations - Session Mean & Stddev: 35.070000,0.651997
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4240000000 of size 17301504 bytes.
[3.609s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 461589970.000000
Throughput: 46158997.000000
Num Accesses - Session Mean & Stddev: 4615899.700000,74104.181827
Num Phases Traversed: 3514.000000
Num Phases Traversed - Session Mean & Stddev: 35.140000,0.548088
Num Path Iterations: 3514.000000
Num Path Iterations - Session Mean & Stddev: 35.140000,0.548088
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4e00000000 of size 17301504 bytes.
[3.541s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 466051735.000000
Throughput: 46605173.500000
Num Accesses - Session Mean & Stddev: 4660517.350000,67480.705786
Num Phases Traversed: 3547.000000
Num Phases Traversed - Session Mean & Stddev: 35.470000,0.499099
Num Path Iterations: 3547.000000
Num Path Iterations - Session Mean & Stddev: 35.470000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fabc0000000 of size 17301504 bytes.
[3.601s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 459832305.000000
Throughput: 45983230.500000
Num Accesses - Session Mean & Stddev: 4598323.050000,90688.193124
Num Phases Traversed: 3501.000000
Num Phases Traversed - Session Mean & Stddev: 35.010000,0.670746
Num Path Iterations: 3501.000000
Num Path Iterations - Session Mean & Stddev: 35.010000,0.670746
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb600000000 of size 17301504 bytes.
[3.594s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 455911360.000000
Throughput: 45591136.000000
Num Accesses - Session Mean & Stddev: 4559113.600000,60706.894437
Num Phases Traversed: 3472.000000
Num Phases Traversed - Session Mean & Stddev: 34.720000,0.448999
Num Path Iterations: 3472.000000
Num Path Iterations - Session Mean & Stddev: 34.720000,0.448999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efdc0000000 of size 17301504 bytes.
[3.608s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 463753250.000000
Throughput: 46375325.000000
Num Accesses - Session Mean & Stddev: 4637532.500000,61958.714684
Num Phases Traversed: 3530.000000
Num Phases Traversed - Session Mean & Stddev: 35.300000,0.458258
Num Path Iterations: 3530.000000
Num Path Iterations - Session Mean & Stddev: 35.300000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd900000000 of size 17301504 bytes.
[3.569s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 469296655.000000
Throughput: 46929665.500000
Num Accesses - Session Mean & Stddev: 4692966.550000,72293.596397
Num Phases Traversed: 3571.000000
Num Phases Traversed - Session Mean & Stddev: 35.710000,0.534696
Num Path Iterations: 3571.000000
Num Path Iterations - Session Mean & Stddev: 35.710000,0.534696
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6240000000 of size 17301504 bytes.
[3.594s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 458615460.000000
Throughput: 45861546.000000
Num Accesses - Session Mean & Stddev: 4586154.600000,78091.861161
Num Phases Traversed: 3492.000000
Num Phases Traversed - Session Mean & Stddev: 34.920000,0.577581
Num Path Iterations: 3492.000000
Num Path Iterations - Session Mean & Stddev: 34.920000,0.577581
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2f00000000 of size 17301504 bytes.
[3.591s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 460913945.000000
Throughput: 46091394.500000
Num Accesses - Session Mean & Stddev: 4609139.450000,91889.678835
Num Phases Traversed: 3509.000000
Num Phases Traversed - Session Mean & Stddev: 35.090000,0.679632
Num Path Iterations: 3509.000000
Num Path Iterations - Session Mean & Stddev: 35.090000,0.679632
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9f00000000 of size 17301504 bytes.
[3.606s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 459561895.000000
Throughput: 45956189.500000
Num Accesses - Session Mean & Stddev: 4595618.950000,92682.016662
Num Phases Traversed: 3499.000000
Num Phases Traversed - Session Mean & Stddev: 34.990000,0.685493
Num Path Iterations: 3499.000000
Num Path Iterations - Session Mean & Stddev: 34.990000,0.685493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5240000000 of size 17301504 bytes.
[3.596s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 460237920.000000
Throughput: 46023792.000000
Num Accesses - Session Mean & Stddev: 4602379.200000,87455.794681
Num Phases Traversed: 3504.000000
Num Phases Traversed - Session Mean & Stddev: 35.040000,0.646838
Num Path Iterations: 3504.000000
Num Path Iterations - Session Mean & Stddev: 35.040000,0.646838
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6340000000 of size 17301504 bytes.
[3.591s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 459021075.000000
Throughput: 45902107.500000
Num Accesses - Session Mean & Stddev: 4590210.750000,84164.433757
Num Phases Traversed: 3495.000000
Num Phases Traversed - Session Mean & Stddev: 34.950000,0.622495
Num Path Iterations: 3495.000000
Num Path Iterations - Session Mean & Stddev: 34.950000,0.622495
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc340000000 of size 17301504 bytes.
[3.623s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 463077225.000000
Throughput: 46307722.500000
Num Accesses - Session Mean & Stddev: 4630772.250000,58545.482359
Num Phases Traversed: 3525.000000
Num Phases Traversed - Session Mean & Stddev: 35.250000,0.433013
Num Path Iterations: 3525.000000
Num Path Iterations - Session Mean & Stddev: 35.250000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3a80000000 of size 17301504 bytes.
[3.596s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 468079810.000000
Throughput: 46807981.000000
Num Accesses - Session Mean & Stddev: 4680798.100000,65626.674158
Num Phases Traversed: 3562.000000
Num Phases Traversed - Session Mean & Stddev: 35.620000,0.485386
Num Path Iterations: 3562.000000
Num Path Iterations - Session Mean & Stddev: 35.620000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa880000000 of size 17301504 bytes.
[3.585s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 464429275.000000
Throughput: 46442927.500000
Num Accesses - Session Mean & Stddev: 4644292.750000,64488.674864
Num Phases Traversed: 3535.000000
Num Phases Traversed - Session Mean & Stddev: 35.350000,0.476970
Num Path Iterations: 3535.000000
Num Path Iterations - Session Mean & Stddev: 35.350000,0.476970
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb6c0000000 of size 17301504 bytes.
[3.598s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 464970095.000000
Throughput: 46497009.500000
Num Accesses - Session Mean & Stddev: 4649700.950000,65946.230087
Num Phases Traversed: 3539.000000
Num Phases Traversed - Session Mean & Stddev: 35.390000,0.487750
Num Path Iterations: 3539.000000
Num Path Iterations - Session Mean & Stddev: 35.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.1 0/135168,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.1 0/135168,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6000000000 of size 17301504 bytes.
[3.565s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 464294070.000000
Throughput: 46429407.000000
Num Accesses - Session Mean & Stddev: 4642940.700000,64047.794423
Num Phases Traversed: 3534.000000
Num Phases Traversed - Session Mean & Stddev: 35.340000,0.473709
Num Path Iterations: 3534.000000
Num Path Iterations - Session Mean & Stddev: 35.340000,0.473709
+ set +x

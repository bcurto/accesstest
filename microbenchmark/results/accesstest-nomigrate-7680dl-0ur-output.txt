++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9c56400000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831919006.000000
Throughput: 83191900.600000
Num Accesses - Session Mean & Stddev: 8319190.060000,17481.616126
Num Phases Traversed: 214678.000000
Num Phases Traversed - Session Mean & Stddev: 2146.780000,4.509058
Num Path Iterations: 214678.000000
Num Path Iterations - Session Mean & Stddev: 2146.780000,4.509058
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3093800000 of size 491520 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831942268.000000
Throughput: 83194226.800000
Num Accesses - Session Mean & Stddev: 8319422.680000,17370.690717
Num Phases Traversed: 214684.000000
Num Phases Traversed - Session Mean & Stddev: 2146.840000,4.480446
Num Path Iterations: 214684.000000
Num Path Iterations - Session Mean & Stddev: 2146.840000,4.480446
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fafa0a00000 of size 491520 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831833712.000000
Throughput: 83183371.200000
Num Accesses - Session Mean & Stddev: 8318337.120000,18195.649475
Num Phases Traversed: 214656.000000
Num Phases Traversed - Session Mean & Stddev: 2146.560000,4.693229
Num Path Iterations: 214656.000000
Num Path Iterations - Session Mean & Stddev: 2146.560000,4.693229
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4659600000 of size 491520 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831942268.000000
Throughput: 83194226.800000
Num Accesses - Session Mean & Stddev: 8319422.680000,17353.375776
Num Phases Traversed: 214684.000000
Num Phases Traversed - Session Mean & Stddev: 2146.840000,4.475980
Num Path Iterations: 214684.000000
Num Path Iterations - Session Mean & Stddev: 2146.840000,4.475980
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4bffc00000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831802696.000000
Throughput: 83180269.600000
Num Accesses - Session Mean & Stddev: 8318026.960000,17255.742610
Num Phases Traversed: 214648.000000
Num Phases Traversed - Session Mean & Stddev: 2146.480000,4.450798
Num Path Iterations: 214648.000000
Num Path Iterations - Session Mean & Stddev: 2146.480000,4.450798
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7a8c400000 of size 491520 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831884113.000000
Throughput: 83188411.300000
Num Accesses - Session Mean & Stddev: 8318841.130000,17848.480185
Num Phases Traversed: 214669.000000
Num Phases Traversed - Session Mean & Stddev: 2146.690000,4.603683
Num Path Iterations: 214669.000000
Num Path Iterations - Session Mean & Stddev: 2146.690000,4.603683
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb3f1c00000 of size 491520 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831868605.000000
Throughput: 83186860.500000
Num Accesses - Session Mean & Stddev: 8318686.050000,18096.704758
Num Phases Traversed: 214665.000000
Num Phases Traversed - Session Mean & Stddev: 2146.650000,4.667708
Num Path Iterations: 214665.000000
Num Path Iterations - Session Mean & Stddev: 2146.650000,4.667708
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0e68200000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831934514.000000
Throughput: 83193451.400000
Num Accesses - Session Mean & Stddev: 8319345.140000,17315.742804
Num Phases Traversed: 214682.000000
Num Phases Traversed - Session Mean & Stddev: 2146.820000,4.466274
Num Path Iterations: 214682.000000
Num Path Iterations - Session Mean & Stddev: 2146.820000,4.466274
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2878a00000 of size 491520 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 832000423.000000
Throughput: 83200042.300000
Num Accesses - Session Mean & Stddev: 8320004.230000,17377.395619
Num Phases Traversed: 214699.000000
Num Phases Traversed - Session Mean & Stddev: 2146.990000,4.482176
Num Path Iterations: 214699.000000
Num Path Iterations - Session Mean & Stddev: 2146.990000,4.482176
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f15dac00000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831930637.000000
Throughput: 83193063.700000
Num Accesses - Session Mean & Stddev: 8319306.370000,17422.316606
Num Phases Traversed: 214681.000000
Num Phases Traversed - Session Mean & Stddev: 2146.810000,4.493762
Num Path Iterations: 214681.000000
Num Path Iterations - Session Mean & Stddev: 2146.810000,4.493762
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0068400000 of size 491520 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831946145.000000
Throughput: 83194614.500000
Num Accesses - Session Mean & Stddev: 8319461.450000,17237.396628
Num Phases Traversed: 214685.000000
Num Phases Traversed - Session Mean & Stddev: 2146.850000,4.446066
Num Path Iterations: 214685.000000
Num Path Iterations - Session Mean & Stddev: 2146.850000,4.446066
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe60b200000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831926760.000000
Throughput: 83192676.000000
Num Accesses - Session Mean & Stddev: 8319267.600000,17390.408721
Num Phases Traversed: 214680.000000
Num Phases Traversed - Session Mean & Stddev: 2146.800000,4.485532
Num Path Iterations: 214680.000000
Num Path Iterations - Session Mean & Stddev: 2146.800000,4.485532
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4ac4400000 of size 491520 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831938391.000000
Throughput: 83193839.100000
Num Accesses - Session Mean & Stddev: 8319383.910000,17356.277233
Num Phases Traversed: 214683.000000
Num Phases Traversed - Session Mean & Stddev: 2146.830000,4.476729
Num Path Iterations: 214683.000000
Num Path Iterations - Session Mean & Stddev: 2146.830000,4.476729
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc425c00000 of size 491520 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831977161.000000
Throughput: 83197716.100000
Num Accesses - Session Mean & Stddev: 8319771.610000,16928.377876
Num Phases Traversed: 214693.000000
Num Phases Traversed - Session Mean & Stddev: 2146.930000,4.366360
Num Path Iterations: 214693.000000
Num Path Iterations - Session Mean & Stddev: 2146.930000,4.366360
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc6c6e00000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831922883.000000
Throughput: 83192288.300000
Num Accesses - Session Mean & Stddev: 8319228.830000,17436.115165
Num Phases Traversed: 214679.000000
Num Phases Traversed - Session Mean & Stddev: 2146.790000,4.497321
Num Path Iterations: 214679.000000
Num Path Iterations - Session Mean & Stddev: 2146.790000,4.497321
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9094c00000 of size 491520 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831701894.000000
Throughput: 83170189.400000
Num Accesses - Session Mean & Stddev: 8317018.940000,20679.933991
Num Phases Traversed: 214622.000000
Num Phases Traversed - Session Mean & Stddev: 2146.220000,5.334004
Num Path Iterations: 214622.000000
Num Path Iterations - Session Mean & Stddev: 2146.220000,5.334004
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9a2b800000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831907375.000000
Throughput: 83190737.500000
Num Accesses - Session Mean & Stddev: 8319073.750000,17685.028037
Num Phases Traversed: 214675.000000
Num Phases Traversed - Session Mean & Stddev: 2146.750000,4.561524
Num Path Iterations: 214675.000000
Num Path Iterations - Session Mean & Stddev: 2146.750000,4.561524
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb320e00000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831930637.000000
Throughput: 83193063.700000
Num Accesses - Session Mean & Stddev: 8319306.370000,17516.962188
Num Phases Traversed: 214681.000000
Num Phases Traversed - Session Mean & Stddev: 2146.810000,4.518174
Num Path Iterations: 214681.000000
Num Path Iterations - Session Mean & Stddev: 2146.810000,4.518174
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0cf3a00000 of size 491520 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831895744.000000
Throughput: 83189574.400000
Num Accesses - Session Mean & Stddev: 8318957.440000,17775.780753
Num Phases Traversed: 214672.000000
Num Phases Traversed - Session Mean & Stddev: 2146.720000,4.584932
Num Path Iterations: 214672.000000
Num Path Iterations - Session Mean & Stddev: 2146.720000,4.584932
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0 0/3840,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0 0/3840,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3397e00000 of size 491520 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831938391.000000
Throughput: 83193839.100000
Num Accesses - Session Mean & Stddev: 8319383.910000,17304.237167
Num Phases Traversed: 214683.000000
Num Phases Traversed - Session Mean & Stddev: 2146.830000,4.463306
Num Path Iterations: 214683.000000
Num Path Iterations - Session Mean & Stddev: 2146.830000,4.463306
+ set +x

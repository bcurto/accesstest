++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0440000000 of size 31457280 bytes.
[6.015s] Test is ready to start.
Test duration in range [10.000, 10.013] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3f40000000 of size 31457280 bytes.
[6.016s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd300000000 of size 31457280 bytes.
[5.985s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 229820295.000000
Throughput: 22982029.500000
Num Accesses - Session Mean & Stddev: 2298202.950000,117237.696945
Num Phases Traversed: 1035.000000
Num Phases Traversed - Session Mean & Stddev: 10.350000,0.476970
Num Path Iterations: 1035.000000
Num Path Iterations - Session Mean & Stddev: 10.350000,0.476970
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9e00000000 of size 31457280 bytes.
[6.014s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f20c0000000 of size 31457280 bytes.
[6.019s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdec0000000 of size 31457280 bytes.
[5.987s] Test is ready to start.
Test duration in range [10.000, 10.017] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f20c0000000 of size 31457280 bytes.
[6.015s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa640000000 of size 31457280 bytes.
[6.018s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7480000000 of size 31457280 bytes.
[6.018s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff000000000 of size 31457280 bytes.
[6.015s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4b80000000 of size 31457280 bytes.
[6.017s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7e00000000 of size 31457280 bytes.
[6.017s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efd80000000 of size 31457280 bytes.
[6.014s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1340000000 of size 31457280 bytes.
[6.018s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7eff80000000 of size 31457280 bytes.
[6.017s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff5c0000000 of size 31457280 bytes.
[6.018s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1c80000000 of size 31457280 bytes.
[6.015s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9200000000 of size 31457280 bytes.
[6.014s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9400000000 of size 31457280 bytes.
[6.017s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/245760,0.001 0/245760,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/245760,0.001 0/245760,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    245760 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2cc0000000 of size 31457280 bytes.
[6.018s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 230803483.000000
Throughput: 23080348.300000
Num Accesses - Session Mean & Stddev: 2308034.830000,119887.471002
Num Phases Traversed: 1039.000000
Num Phases Traversed - Session Mean & Stddev: 10.390000,0.487750
Num Path Iterations: 1039.000000
Num Path Iterations - Session Mean & Stddev: 10.390000,0.487750
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbc8f400000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2475959731.000000
Throughput: 247595973.100000
Num Accesses - Session Mean & Stddev: 24759597.310000,82.904004
Num Phases Traversed: 12568423.000000
Num Phases Traversed - Session Mean & Stddev: 125684.230000,0.420833
Num Path Iterations: 12568423.000000
Num Path Iterations - Session Mean & Stddev: 125684.230000,0.420833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdbbc800000 of size 20480 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2499996489.000000
Throughput: 249999648.900000
Num Accesses - Session Mean & Stddev: 24999964.890000,95.112449
Num Phases Traversed: 12690437.000000
Num Phases Traversed - Session Mean & Stddev: 126904.370000,0.482804
Num Path Iterations: 12690437.000000
Num Path Iterations - Session Mean & Stddev: 126904.370000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8027e00000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2499715567.000000
Throughput: 249971556.700000
Num Accesses - Session Mean & Stddev: 24997155.670000,67.643042
Num Phases Traversed: 12689011.000000
Num Phases Traversed - Session Mean & Stddev: 126890.110000,0.343366
Num Path Iterations: 12689011.000000
Num Path Iterations - Session Mean & Stddev: 126890.110000,0.343366
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd9b7000000 of size 20480 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2518648843.000000
Throughput: 251864884.300000
Num Accesses - Session Mean & Stddev: 25186488.430000,77.283278
Num Phases Traversed: 12785119.000000
Num Phases Traversed - Session Mean & Stddev: 127851.190000,0.392301
Num Path Iterations: 12785119.000000
Num Path Iterations - Session Mean & Stddev: 127851.190000,0.392301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3357e00000 of size 20480 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2495299812.000000
Throughput: 249529981.200000
Num Accesses - Session Mean & Stddev: 24952998.120000,73.288236
Num Phases Traversed: 12666596.000000
Num Phases Traversed - Session Mean & Stddev: 126665.960000,0.372022
Num Path Iterations: 12666596.000000
Num Path Iterations - Session Mean & Stddev: 126665.960000,0.372022
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb991000000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2474303552.000000
Throughput: 247430355.200000
Num Accesses - Session Mean & Stddev: 24743035.520000,72.221393
Num Phases Traversed: 12560016.000000
Num Phases Traversed - Session Mean & Stddev: 125600.160000,0.366606
Num Path Iterations: 12560016.000000
Num Path Iterations - Session Mean & Stddev: 125600.160000,0.366606
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f45a4800000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2498519974.000000
Throughput: 249851997.400000
Num Accesses - Session Mean & Stddev: 24985199.740000,97.231026
Num Phases Traversed: 12682942.000000
Num Phases Traversed - Session Mean & Stddev: 126829.420000,0.493559
Num Path Iterations: 12682942.000000
Num Path Iterations - Session Mean & Stddev: 126829.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc49d000000 of size 20480 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2483741822.000000
Throughput: 248374182.200000
Num Accesses - Session Mean & Stddev: 24837418.220000,86.410946
Num Phases Traversed: 12607926.000000
Num Phases Traversed - Session Mean & Stddev: 126079.260000,0.438634
Num Path Iterations: 12607926.000000
Num Path Iterations - Session Mean & Stddev: 126079.260000,0.438634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdf21000000 of size 20480 bytes.
[1.257s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2477296967.000000
Throughput: 247729696.700000
Num Accesses - Session Mean & Stddev: 24772969.670000,61.639282
Num Phases Traversed: 12575211.000000
Num Phases Traversed - Session Mean & Stddev: 125752.110000,0.312890
Num Path Iterations: 12575211.000000
Num Path Iterations - Session Mean & Stddev: 125752.110000,0.312890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbb90200000 of size 20480 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2491173450.000000
Throughput: 249117345.000000
Num Accesses - Session Mean & Stddev: 24911734.500000,98.500000
Num Phases Traversed: 12645650.000000
Num Phases Traversed - Session Mean & Stddev: 126456.500000,0.500000
Num Path Iterations: 12645650.000000
Num Path Iterations - Session Mean & Stddev: 126456.500000,0.500000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f223a400000 of size 20480 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2512076923.000000
Throughput: 251207692.300000
Num Accesses - Session Mean & Stddev: 25120769.230000,96.891161
Num Phases Traversed: 12751759.000000
Num Phases Traversed - Session Mean & Stddev: 127517.590000,0.491833
Num Path Iterations: 12751759.000000
Num Path Iterations - Session Mean & Stddev: 127517.590000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc353600000 of size 20480 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2492538266.000000
Throughput: 249253826.600000
Num Accesses - Session Mean & Stddev: 24925382.660000,81.606522
Num Phases Traversed: 12652578.000000
Num Phases Traversed - Session Mean & Stddev: 126525.780000,0.414246
Num Path Iterations: 12652578.000000
Num Path Iterations - Session Mean & Stddev: 126525.780000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3170200000 of size 20480 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2482003100.000000
Throughput: 248200310.000000
Num Accesses - Session Mean & Stddev: 24820031.000000,62.296870
Num Phases Traversed: 12599100.000000
Num Phases Traversed - Session Mean & Stddev: 125991.000000,0.316228
Num Path Iterations: 12599100.000000
Num Path Iterations - Session Mean & Stddev: 125991.000000,0.316228
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f65f2c00000 of size 20480 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2493075288.000000
Throughput: 249307528.800000
Num Accesses - Session Mean & Stddev: 24930752.880000,61.796485
Num Phases Traversed: 12655304.000000
Num Phases Traversed - Session Mean & Stddev: 126553.040000,0.313688
Num Path Iterations: 12655304.000000
Num Path Iterations - Session Mean & Stddev: 126553.040000,0.313688
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff2f1a00000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2502021649.000000
Throughput: 250202164.900000
Num Accesses - Session Mean & Stddev: 25020216.490000,73.999661
Num Phases Traversed: 12700717.000000
Num Phases Traversed - Session Mean & Stddev: 127007.170000,0.375633
Num Path Iterations: 12700717.000000
Num Path Iterations - Session Mean & Stddev: 127007.170000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efd6b200000 of size 20480 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2525364179.000000
Throughput: 252536417.900000
Num Accesses - Session Mean & Stddev: 25253641.790000,63.865686
Num Phases Traversed: 12819207.000000
Num Phases Traversed - Session Mean & Stddev: 128192.070000,0.324191
Num Path Iterations: 12819207.000000
Num Path Iterations - Session Mean & Stddev: 128192.070000,0.324191
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff7cde00000 of size 20480 bytes.
[1.263s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2522911726.000000
Throughput: 252291172.600000
Num Accesses - Session Mean & Stddev: 25229117.260000,97.231026
Num Phases Traversed: 12806758.000000
Num Phases Traversed - Session Mean & Stddev: 128067.580000,0.493559
Num Path Iterations: 12806758.000000
Num Path Iterations - Session Mean & Stddev: 128067.580000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd533a00000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2509235592.000000
Throughput: 250923559.200000
Num Accesses - Session Mean & Stddev: 25092355.920000,94.560000
Num Phases Traversed: 12737336.000000
Num Phases Traversed - Session Mean & Stddev: 127373.360000,0.480000
Num Path Iterations: 12737336.000000
Num Path Iterations - Session Mean & Stddev: 127373.360000,0.480000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2ca5e00000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2506735465.000000
Throughput: 250673546.500000
Num Accesses - Session Mean & Stddev: 25067354.650000,98.006263
Num Phases Traversed: 12724645.000000
Num Phases Traversed - Session Mean & Stddev: 127246.450000,0.497494
Num Path Iterations: 12724645.000000
Num Path Iterations - Session Mean & Stddev: 127246.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.001 0/160,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.001 0/160,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa1d7400000 of size 20480 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2514817784.000000
Throughput: 251481778.400000
Num Accesses - Session Mean & Stddev: 25148177.840000,88.452781
Num Phases Traversed: 12765672.000000
Num Phases Traversed - Session Mean & Stddev: 127656.720000,0.448999
Num Path Iterations: 12765672.000000
Num Path Iterations - Session Mean & Stddev: 127656.720000,0.448999
+ set +x

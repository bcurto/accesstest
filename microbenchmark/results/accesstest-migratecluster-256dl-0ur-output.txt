++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe355800000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2371245625.000000
Throughput: 237124562.500000
Num Accesses - Session Mean & Stddev: 23712456.250000,58.916785
Num Phases Traversed: 14371285.000000
Num Phases Traversed - Session Mean & Stddev: 143712.850000,0.357071
Num Path Iterations: 14371285.000000
Num Path Iterations - Session Mean & Stddev: 143712.850000,0.357071
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f75a4a00000 of size 16384 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2366865700.000000
Throughput: 236686570.000000
Num Accesses - Session Mean & Stddev: 23668657.000000,80.833162
Num Phases Traversed: 14344740.000000
Num Phases Traversed - Session Mean & Stddev: 143447.400000,0.489898
Num Path Iterations: 14344740.000000
Num Path Iterations - Session Mean & Stddev: 143447.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2de2800000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2376965350.000000
Throughput: 237696535.000000
Num Accesses - Session Mean & Stddev: 23769653.500000,82.500000
Num Phases Traversed: 14405950.000000
Num Phases Traversed - Session Mean & Stddev: 144059.500000,0.500000
Num Path Iterations: 14405950.000000
Num Path Iterations - Session Mean & Stddev: 144059.500000,0.500000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fda2c200000 of size 16384 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2356570360.000000
Throughput: 235657036.000000
Num Accesses - Session Mean & Stddev: 23565703.600000,81.903846
Num Phases Traversed: 14282344.000000
Num Phases Traversed - Session Mean & Stddev: 142823.440000,0.496387
Num Path Iterations: 14282344.000000
Num Path Iterations - Session Mean & Stddev: 142823.440000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5275c00000 of size 16384 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2348773120.000000
Throughput: 234877312.000000
Num Accesses - Session Mean & Stddev: 23487731.200000,53.618653
Num Phases Traversed: 14235088.000000
Num Phases Traversed - Session Mean & Stddev: 142350.880000,0.324962
Num Path Iterations: 14235088.000000
Num Path Iterations - Session Mean & Stddev: 142350.880000,0.324962
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9d8f400000 of size 16384 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2371990600.000000
Throughput: 237199060.000000
Num Accesses - Session Mean & Stddev: 23719906.000000,33.000000
Num Phases Traversed: 14375800.000000
Num Phases Traversed - Session Mean & Stddev: 143758.000000,0.200000
Num Path Iterations: 14375800.000000
Num Path Iterations - Session Mean & Stddev: 143758.000000,0.200000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1623a00000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2370210415.000000
Throughput: 237021041.500000
Num Accesses - Session Mean & Stddev: 23702104.150000,51.626810
Num Phases Traversed: 14365011.000000
Num Phases Traversed - Session Mean & Stddev: 143650.110000,0.312890
Num Path Iterations: 14365011.000000
Num Path Iterations - Session Mean & Stddev: 143650.110000,0.312890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f98d4400000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2355659890.000000
Throughput: 235565989.000000
Num Accesses - Session Mean & Stddev: 23556598.900000,72.374650
Num Phases Traversed: 14276826.000000
Num Phases Traversed - Session Mean & Stddev: 142768.260000,0.438634
Num Path Iterations: 14276826.000000
Num Path Iterations - Session Mean & Stddev: 142768.260000,0.438634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcb4d800000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2365587445.000000
Throughput: 236558744.500000
Num Accesses - Session Mean & Stddev: 23655874.450000,42.099258
Num Phases Traversed: 14336993.000000
Num Phases Traversed - Session Mean & Stddev: 143369.930000,0.255147
Num Path Iterations: 14336993.000000
Num Path Iterations - Session Mean & Stddev: 143369.930000,0.255147
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa66c200000 of size 16384 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2373105835.000000
Throughput: 237310583.500000
Num Accesses - Session Mean & Stddev: 23731058.350000,84.440674
Num Phases Traversed: 14382559.000000
Num Phases Traversed - Session Mean & Stddev: 143825.590000,0.511762
Num Path Iterations: 14382559.000000
Num Path Iterations - Session Mean & Stddev: 143825.590000,0.511762
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7387c00000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2352975010.000000
Throughput: 235297501.000000
Num Accesses - Session Mean & Stddev: 23529750.100000,82.235576
Num Phases Traversed: 14260554.000000
Num Phases Traversed - Session Mean & Stddev: 142605.540000,0.498397
Num Path Iterations: 14260554.000000
Num Path Iterations - Session Mean & Stddev: 142605.540000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8501a00000 of size 16384 bytes.
[1.217s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2377987030.000000
Throughput: 237798703.000000
Num Accesses - Session Mean & Stddev: 23779870.300000,81.437154
Num Phases Traversed: 14412142.000000
Num Phases Traversed - Session Mean & Stddev: 144121.420000,0.493559
Num Path Iterations: 14412142.000000
Num Path Iterations - Session Mean & Stddev: 144121.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcb15400000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2357838220.000000
Throughput: 235783822.000000
Num Accesses - Session Mean & Stddev: 23578382.200000,74.084816
Num Phases Traversed: 14290028.000000
Num Phases Traversed - Session Mean & Stddev: 142900.280000,0.448999
Num Path Iterations: 14290028.000000
Num Path Iterations - Session Mean & Stddev: 142900.280000,0.448999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f926b200000 of size 16384 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2388375595.000000
Throughput: 238837559.500000
Num Accesses - Session Mean & Stddev: 23883755.950000,28.146891
Num Phases Traversed: 14475103.000000
Num Phases Traversed - Session Mean & Stddev: 144751.030000,0.170587
Num Path Iterations: 14475103.000000
Num Path Iterations - Session Mean & Stddev: 144751.030000,0.170587
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5dbdc00000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2378072335.000000
Throughput: 237807233.500000
Num Accesses - Session Mean & Stddev: 23780723.350000,81.152495
Num Phases Traversed: 14412659.000000
Num Phases Traversed - Session Mean & Stddev: 144126.590000,0.491833
Num Path Iterations: 14412659.000000
Num Path Iterations - Session Mean & Stddev: 144126.590000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdadb400000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2370104815.000000
Throughput: 237010481.500000
Num Accesses - Session Mean & Stddev: 23701048.150000,74.870739
Num Phases Traversed: 14364371.000000
Num Phases Traversed - Session Mean & Stddev: 143643.710000,0.453762
Num Path Iterations: 14364371.000000
Num Path Iterations - Session Mean & Stddev: 143643.710000,0.453762
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb791600000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2361198445.000000
Throughput: 236119844.500000
Num Accesses - Session Mean & Stddev: 23611984.450000,42.099258
Num Phases Traversed: 14310393.000000
Num Phases Traversed - Session Mean & Stddev: 143103.930000,0.255147
Num Path Iterations: 14310393.000000
Num Path Iterations - Session Mean & Stddev: 143103.930000,0.255147
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5e77000000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2359538545.000000
Throughput: 235953854.500000
Num Accesses - Session Mean & Stddev: 23595385.450000,77.585098
Num Phases Traversed: 14300333.000000
Num Phases Traversed - Session Mean & Stddev: 143003.330000,0.470213
Num Path Iterations: 14300333.000000
Num Path Iterations - Session Mean & Stddev: 143003.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f35bea00000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2379387550.000000
Throughput: 237938755.000000
Num Accesses - Session Mean & Stddev: 23793875.500000,75.612499
Num Phases Traversed: 14420630.000000
Num Phases Traversed - Session Mean & Stddev: 144206.300000,0.458258
Num Path Iterations: 14420630.000000
Num Path Iterations - Session Mean & Stddev: 144206.300000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f65c9400000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2351488030.000000
Throughput: 235148803.000000
Num Accesses - Session Mean & Stddev: 23514880.300000,81.437154
Num Phases Traversed: 14251542.000000
Num Phases Traversed - Session Mean & Stddev: 142515.420000,0.493559
Num Path Iterations: 14251542.000000
Num Path Iterations - Session Mean & Stddev: 142515.420000,0.493559
+ set +x

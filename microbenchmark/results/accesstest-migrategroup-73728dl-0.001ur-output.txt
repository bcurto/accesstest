++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5440000000 of size 4718592 bytes.
[1.572s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823224509.000000
Throughput: 82322450.900000
Num Accesses - Session Mean & Stddev: 8232245.090000,24530.146181
Num Phases Traversed: 22409.000000
Num Phases Traversed - Session Mean & Stddev: 224.090000,0.664756
Num Path Iterations: 22409.000000
Num Path Iterations - Session Mean & Stddev: 224.090000,0.664756
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc7c0000000 of size 4718592 bytes.
[1.603s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824589846.000000
Throughput: 82458984.600000
Num Accesses - Session Mean & Stddev: 8245898.460000,19117.424832
Num Phases Traversed: 22446.000000
Num Phases Traversed - Session Mean & Stddev: 224.460000,0.518073
Num Path Iterations: 22446.000000
Num Path Iterations - Session Mean & Stddev: 224.460000,0.518073
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f57c0000000 of size 4718592 bytes.
[1.604s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824958856.000000
Throughput: 82495885.600000
Num Accesses - Session Mean & Stddev: 8249588.560000,18317.174688
Num Phases Traversed: 22456.000000
Num Phases Traversed - Session Mean & Stddev: 224.560000,0.496387
Num Path Iterations: 22456.000000
Num Path Iterations - Session Mean & Stddev: 224.560000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f51c0000000 of size 4718592 bytes.
[1.611s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824405341.000000
Throughput: 82440534.100000
Num Accesses - Session Mean & Stddev: 8244053.410000,18149.140791
Num Phases Traversed: 22441.000000
Num Phases Traversed - Session Mean & Stddev: 224.410000,0.491833
Num Path Iterations: 22441.000000
Num Path Iterations - Session Mean & Stddev: 224.410000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f19c0000000 of size 4718592 bytes.
[1.603s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824552945.000000
Throughput: 82455294.500000
Num Accesses - Session Mean & Stddev: 8245529.450000,18358.015708
Num Phases Traversed: 22445.000000
Num Phases Traversed - Session Mean & Stddev: 224.450000,0.497494
Num Path Iterations: 22445.000000
Num Path Iterations - Session Mean & Stddev: 224.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1300000000 of size 4718592 bytes.
[1.604s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824552945.000000
Throughput: 82455294.500000
Num Accesses - Session Mean & Stddev: 8245529.450000,19785.956959
Num Phases Traversed: 22445.000000
Num Phases Traversed - Session Mean & Stddev: 224.450000,0.536190
Num Path Iterations: 22445.000000
Num Path Iterations - Session Mean & Stddev: 224.450000,0.536190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0940000000 of size 4718592 bytes.
[1.604s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824220836.000000
Throughput: 82422083.600000
Num Accesses - Session Mean & Stddev: 8242208.360000,21843.397260
Num Phases Traversed: 22436.000000
Num Phases Traversed - Session Mean & Stddev: 224.360000,0.591946
Num Path Iterations: 22436.000000
Num Path Iterations - Session Mean & Stddev: 224.360000,0.591946
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0f80000000 of size 4718592 bytes.
[1.604s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824921955.000000
Throughput: 82492195.500000
Num Accesses - Session Mean & Stddev: 8249219.550000,18358.015708
Num Phases Traversed: 22455.000000
Num Phases Traversed - Session Mean & Stddev: 224.550000,0.497494
Num Path Iterations: 22455.000000
Num Path Iterations - Session Mean & Stddev: 224.550000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4140000000 of size 4718592 bytes.
[1.596s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 824405341.000000
Throughput: 82440534.100000
Num Accesses - Session Mean & Stddev: 8244053.410000,18884.517137
Num Phases Traversed: 22441.000000
Num Phases Traversed - Session Mean & Stddev: 224.410000,0.511762
Num Path Iterations: 22441.000000
Num Path Iterations - Session Mean & Stddev: 224.410000,0.511762
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe300000000 of size 4718592 bytes.
[1.604s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824663648.000000
Throughput: 82466364.800000
Num Accesses - Session Mean & Stddev: 8246636.480000,18435.733691
Num Phases Traversed: 22448.000000
Num Phases Traversed - Session Mean & Stddev: 224.480000,0.499600
Num Path Iterations: 22448.000000
Num Path Iterations - Session Mean & Stddev: 224.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc680000000 of size 4718592 bytes.
[1.604s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 824552945.000000
Throughput: 82455294.500000
Num Accesses - Session Mean & Stddev: 8245529.450000,18358.015708
Num Phases Traversed: 22445.000000
Num Phases Traversed - Session Mean & Stddev: 224.450000,0.497494
Num Path Iterations: 22445.000000
Num Path Iterations - Session Mean & Stddev: 224.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8cc0000000 of size 4718592 bytes.
[1.604s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824368440.000000
Throughput: 82436844.000000
Num Accesses - Session Mean & Stddev: 8243684.400000,18077.724200
Num Phases Traversed: 22440.000000
Num Phases Traversed - Session Mean & Stddev: 224.400000,0.489898
Num Path Iterations: 22440.000000
Num Path Iterations - Session Mean & Stddev: 224.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4400000000 of size 4718592 bytes.
[1.604s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824995757.000000
Throughput: 82499575.700000
Num Accesses - Session Mean & Stddev: 8249957.570000,18268.790316
Num Phases Traversed: 22457.000000
Num Phases Traversed - Session Mean & Stddev: 224.570000,0.495076
Num Path Iterations: 22457.000000
Num Path Iterations - Session Mean & Stddev: 224.570000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc540000000 of size 4718592 bytes.
[1.603s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824626747.000000
Throughput: 82462674.700000
Num Accesses - Session Mean & Stddev: 8246267.470000,19142.338176
Num Phases Traversed: 22447.000000
Num Phases Traversed - Session Mean & Stddev: 224.470000,0.518748
Num Path Iterations: 22447.000000
Num Path Iterations - Session Mean & Stddev: 224.470000,0.518748
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1f00000000 of size 4718592 bytes.
[1.604s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 825106460.000000
Throughput: 82510646.000000
Num Accesses - Session Mean & Stddev: 8251064.600000,18077.724200
Num Phases Traversed: 22460.000000
Num Phases Traversed - Session Mean & Stddev: 224.600000,0.489898
Num Path Iterations: 22460.000000
Num Path Iterations - Session Mean & Stddev: 224.600000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff340000000 of size 4718592 bytes.
[1.603s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824921955.000000
Throughput: 82492195.500000
Num Accesses - Session Mean & Stddev: 8249219.550000,18358.015708
Num Phases Traversed: 22455.000000
Num Phases Traversed - Session Mean & Stddev: 224.550000,0.497494
Num Path Iterations: 22455.000000
Num Path Iterations - Session Mean & Stddev: 224.550000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe5c0000000 of size 4718592 bytes.
[1.611s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824700549.000000
Throughput: 82470054.900000
Num Accesses - Session Mean & Stddev: 8247005.490000,18446.809531
Num Phases Traversed: 22449.000000
Num Phases Traversed - Session Mean & Stddev: 224.490000,0.499900
Num Path Iterations: 22449.000000
Num Path Iterations - Session Mean & Stddev: 224.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd4c0000000 of size 4718592 bytes.
[1.604s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824848153.000000
Throughput: 82484815.300000
Num Accesses - Session Mean & Stddev: 8248481.530000,18417.259156
Num Phases Traversed: 22453.000000
Num Phases Traversed - Session Mean & Stddev: 224.530000,0.499099
Num Path Iterations: 22453.000000
Num Path Iterations - Session Mean & Stddev: 224.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3440000000 of size 4718592 bytes.
[1.603s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 825180262.000000
Throughput: 82518026.200000
Num Accesses - Session Mean & Stddev: 8251802.620000,17911.245169
Num Phases Traversed: 22462.000000
Num Phases Traversed - Session Mean & Stddev: 224.620000,0.485386
Num Path Iterations: 22462.000000
Num Path Iterations - Session Mean & Stddev: 224.620000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.001 0/36864,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.001 0/36864,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff3c0000000 of size 4718592 bytes.
[1.604s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 824442242.000000
Throughput: 82444224.200000
Num Accesses - Session Mean & Stddev: 8244422.420000,18945.707956
Num Phases Traversed: 22442.000000
Num Phases Traversed - Session Mean & Stddev: 224.420000,0.513420
Num Path Iterations: 22442.000000
Num Path Iterations - Session Mean & Stddev: 224.420000,0.513420
+ set +x

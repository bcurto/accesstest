++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4389a00000 of size 49152 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2819298170.000000
Throughput: 281929817.000000
Num Accesses - Session Mean & Stddev: 28192981.700000,192.926437
Num Phases Traversed: 6696770.000000
Num Phases Traversed - Session Mean & Stddev: 66967.700000,0.458258
Num Path Iterations: 6696770.000000
Num Path Iterations - Session Mean & Stddev: 66967.700000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1c4b000000 of size 49152 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2833091814.000000
Throughput: 283309181.400000
Num Accesses - Session Mean & Stddev: 28330918.140000,199.431393
Num Phases Traversed: 6729534.000000
Num Phases Traversed - Session Mean & Stddev: 67295.340000,0.473709
Num Path Iterations: 6729534.000000
Num Path Iterations - Session Mean & Stddev: 67295.340000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcf60800000 of size 49152 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2825270476.000000
Throughput: 282527047.600000
Num Accesses - Session Mean & Stddev: 28252704.760000,208.978904
Num Phases Traversed: 6710956.000000
Num Phases Traversed - Session Mean & Stddev: 67109.560000,0.496387
Num Path Iterations: 6710956.000000
Num Path Iterations - Session Mean & Stddev: 67109.560000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcaa2400000 of size 49152 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2826313293.000000
Throughput: 282631329.300000
Num Accesses - Session Mean & Stddev: 28263132.930000,197.959554
Num Phases Traversed: 6713433.000000
Num Phases Traversed - Session Mean & Stddev: 67134.330000,0.470213
Num Path Iterations: 6713433.000000
Num Path Iterations - Session Mean & Stddev: 67134.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6eb9e00000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2832203504.000000
Throughput: 283220350.400000
Num Accesses - Session Mean & Stddev: 28322035.040000,179.801998
Num Phases Traversed: 6727424.000000
Num Phases Traversed - Session Mean & Stddev: 67274.240000,0.427083
Num Path Iterations: 6727424.000000
Num Path Iterations - Session Mean & Stddev: 67274.240000,0.427083
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f983d000000 of size 49152 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2819413103.000000
Throughput: 281941310.300000
Num Accesses - Session Mean & Stddev: 28194131.030000,208.426891
Num Phases Traversed: 6697043.000000
Num Phases Traversed - Session Mean & Stddev: 66970.430000,0.495076
Num Path Iterations: 6697043.000000
Num Path Iterations - Session Mean & Stddev: 66970.430000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7df0e00000 of size 49152 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2836382350.000000
Throughput: 283638235.000000
Num Accesses - Session Mean & Stddev: 28363823.500000,210.500000
Num Phases Traversed: 6737350.000000
Num Phases Traversed - Session Mean & Stddev: 67373.500000,0.500000
Num Path Iterations: 6737350.000000
Num Path Iterations - Session Mean & Stddev: 67373.500000,0.500000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f336da00000 of size 49152 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2825341204.000000
Throughput: 282534120.400000
Num Accesses - Session Mean & Stddev: 28253412.040000,179.801998
Num Phases Traversed: 6711124.000000
Num Phases Traversed - Session Mean & Stddev: 67111.240000,0.427083
Num Path Iterations: 6711124.000000
Num Path Iterations - Session Mean & Stddev: 67111.240000,0.427083
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f32c5a00000 of size 49152 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2833941813.000000
Throughput: 283394181.300000
Num Accesses - Session Mean & Stddev: 28339418.130000,210.120758
Num Phases Traversed: 6731553.000000
Num Phases Traversed - Session Mean & Stddev: 67315.530000,0.499099
Num Path Iterations: 6731553.000000
Num Path Iterations - Session Mean & Stddev: 67315.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9b63200000 of size 49152 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2831353926.000000
Throughput: 283135392.600000
Num Accesses - Session Mean & Stddev: 28313539.260000,130.713551
Num Phases Traversed: 6725406.000000
Num Phases Traversed - Session Mean & Stddev: 67254.060000,0.310483
Num Path Iterations: 6725406.000000
Num Path Iterations - Session Mean & Stddev: 67254.060000,0.310483
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0505800000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2824286178.000000
Throughput: 282428617.800000
Num Accesses - Session Mean & Stddev: 28242861.780000,161.742918
Num Phases Traversed: 6708618.000000
Num Phases Traversed - Session Mean & Stddev: 67086.180000,0.384187
Num Path Iterations: 6708618.000000
Num Path Iterations - Session Mean & Stddev: 67086.180000,0.384187
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3ab1000000 of size 49152 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2833998227.000000
Throughput: 283399822.700000
Num Accesses - Session Mean & Stddev: 28339982.270000,141.583746
Num Phases Traversed: 6731687.000000
Num Phases Traversed - Session Mean & Stddev: 67316.870000,0.336303
Num Path Iterations: 6731687.000000
Num Path Iterations - Session Mean & Stddev: 67316.870000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe03f800000 of size 49152 bytes.
[1.258s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2821133730.000000
Throughput: 282113373.000000
Num Accesses - Session Mean & Stddev: 28211337.300000,192.926437
Num Phases Traversed: 6701130.000000
Num Phases Traversed - Session Mean & Stddev: 67011.300000,0.458258
Num Path Iterations: 6701130.000000
Num Path Iterations - Session Mean & Stddev: 67011.300000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8270c00000 of size 49152 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2810468958.000000
Throughput: 281046895.800000
Num Accesses - Session Mean & Stddev: 28104689.580000,83.777942
Num Phases Traversed: 6675798.000000
Num Phases Traversed - Session Mean & Stddev: 66757.980000,0.198997
Num Path Iterations: 6675798.000000
Num Path Iterations - Session Mean & Stddev: 66757.980000,0.198997
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f46fc200000 of size 49152 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2827772058.000000
Throughput: 282777205.800000
Num Accesses - Session Mean & Stddev: 28277720.580000,58.940000
Num Phases Traversed: 6716898.000000
Num Phases Traversed - Session Mean & Stddev: 67168.980000,0.140000
Num Path Iterations: 6716898.000000
Num Path Iterations - Session Mean & Stddev: 67168.980000,0.140000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f560be00000 of size 49152 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2816817217.000000
Throughput: 281681721.700000
Num Accesses - Session Mean & Stddev: 28168172.170000,177.170486
Num Phases Traversed: 6690877.000000
Num Phases Traversed - Session Mean & Stddev: 66908.770000,0.420833
Num Path Iterations: 6690877.000000
Num Path Iterations - Session Mean & Stddev: 66908.770000,0.420833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe76f000000 of size 49152 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2831353084.000000
Throughput: 283135308.400000
Num Accesses - Session Mean & Stddev: 28313530.840000,82.498815
Num Phases Traversed: 6725404.000000
Num Phases Traversed - Session Mean & Stddev: 67254.040000,0.195959
Num Path Iterations: 6725404.000000
Num Path Iterations - Session Mean & Stddev: 67254.040000,0.195959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4b46a00000 of size 49152 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2828034762.000000
Throughput: 282803476.200000
Num Accesses - Session Mean & Stddev: 28280347.620000,174.397694
Num Phases Traversed: 6717522.000000
Num Phases Traversed - Session Mean & Stddev: 67175.220000,0.414246
Num Path Iterations: 6717522.000000
Num Path Iterations - Session Mean & Stddev: 67175.220000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa40c800000 of size 49152 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2819149136.000000
Throughput: 281914913.600000
Num Accesses - Session Mean & Stddev: 28191491.360000,154.341149
Num Phases Traversed: 6696416.000000
Num Phases Traversed - Session Mean & Stddev: 66964.160000,0.366606
Num Path Iterations: 6696416.000000
Num Path Iterations - Session Mean & Stddev: 66964.160000,0.366606
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0383c00000 of size 49152 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2829340283.000000
Throughput: 282934028.300000
Num Accesses - Session Mean & Stddev: 28293402.830000,177.170486
Num Phases Traversed: 6720623.000000
Num Phases Traversed - Session Mean & Stddev: 67206.230000,0.420833
Num Path Iterations: 6720623.000000
Num Path Iterations - Session Mean & Stddev: 67206.230000,0.420833
+ set +x

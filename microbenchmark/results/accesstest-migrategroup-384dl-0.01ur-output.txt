++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f73f3000000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2534040362.000000
Throughput: 253404036.200000
Num Accesses - Session Mean & Stddev: 25340403.620000,94.862404
Num Phases Traversed: 11065778.000000
Num Phases Traversed - Session Mean & Stddev: 110657.780000,0.414246
Num Path Iterations: 11065778.000000
Num Path Iterations - Session Mean & Stddev: 110657.780000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1272200000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2547743722.000000
Throughput: 254774372.200000
Num Accesses - Session Mean & Stddev: 25477437.220000,87.978927
Num Phases Traversed: 11125618.000000
Num Phases Traversed - Session Mean & Stddev: 111256.180000,0.384187
Num Path Iterations: 11125618.000000
Num Path Iterations - Session Mean & Stddev: 111256.180000,0.384187
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7febd9200000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2558814040.000000
Throughput: 255881404.000000
Num Accesses - Session Mean & Stddev: 25588140.400000,112.186630
Num Phases Traversed: 11173960.000000
Num Phases Traversed - Session Mean & Stddev: 111739.600000,0.489898
Num Path Iterations: 11173960.000000
Num Path Iterations - Session Mean & Stddev: 111739.600000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f934cc00000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2518180280.000000
Throughput: 251818028.000000
Num Accesses - Session Mean & Stddev: 25181802.800000,91.600000
Num Phases Traversed: 10996520.000000
Num Phases Traversed - Session Mean & Stddev: 109965.200000,0.400000
Num Path Iterations: 10996520.000000
Num Path Iterations - Session Mean & Stddev: 109965.200000,0.400000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f74d7600000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2540885630.000000
Throughput: 254088563.000000
Num Accesses - Session Mean & Stddev: 25408856.300000,104.940983
Num Phases Traversed: 11095670.000000
Num Phases Traversed - Session Mean & Stddev: 110956.700000,0.458258
Num Path Iterations: 11095670.000000
Num Path Iterations - Session Mean & Stddev: 110956.700000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0420400000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2550955218.000000
Throughput: 255095521.800000
Num Accesses - Session Mean & Stddev: 25509552.180000,113.024898
Num Phases Traversed: 11139642.000000
Num Phases Traversed - Session Mean & Stddev: 111396.420000,0.493559
Num Path Iterations: 11139642.000000
Num Path Iterations - Session Mean & Stddev: 111396.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4af9400000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2554157325.000000
Throughput: 255415732.500000
Num Accesses - Session Mean & Stddev: 25541573.250000,99.159909
Num Phases Traversed: 11153625.000000
Num Phases Traversed - Session Mean & Stddev: 111536.250000,0.433013
Num Path Iterations: 11153625.000000
Num Path Iterations - Session Mean & Stddev: 111536.250000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb827a00000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2550342414.000000
Throughput: 255034241.400000
Num Accesses - Session Mean & Stddev: 25503424.140000,108.479309
Num Phases Traversed: 11136966.000000
Num Phases Traversed - Session Mean & Stddev: 111369.660000,0.473709
Num Path Iterations: 11136966.000000
Num Path Iterations - Session Mean & Stddev: 111369.660000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6d65a00000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2546726733.000000
Throughput: 254672673.300000
Num Accesses - Session Mean & Stddev: 25467267.330000,96.370644
Num Phases Traversed: 11121177.000000
Num Phases Traversed - Session Mean & Stddev: 111211.770000,0.420833
Num Path Iterations: 11121177.000000
Num Path Iterations - Session Mean & Stddev: 111211.770000,0.420833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb84a800000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2555370109.000000
Throughput: 255537010.900000
Num Accesses - Session Mean & Stddev: 25553701.090000,93.273586
Num Phases Traversed: 11158921.000000
Num Phases Traversed - Session Mean & Stddev: 111589.210000,0.407308
Num Path Iterations: 11158921.000000
Num Path Iterations - Session Mean & Stddev: 111589.210000,0.407308
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f90b5800000 of size 24576 bytes.
[1.249s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2550364169.000000
Throughput: 255036416.900000
Num Accesses - Session Mean & Stddev: 25503641.690000,111.694735
Num Phases Traversed: 11137061.000000
Num Phases Traversed - Session Mean & Stddev: 111370.610000,0.487750
Num Path Iterations: 11137061.000000
Num Path Iterations - Session Mean & Stddev: 111370.610000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fde46800000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2532695216.000000
Throughput: 253269521.600000
Num Accesses - Session Mean & Stddev: 25326952.160000,78.797299
Num Phases Traversed: 11059904.000000
Num Phases Traversed - Session Mean & Stddev: 110599.040000,0.344093
Num Path Iterations: 11059904.000000
Num Path Iterations - Session Mean & Stddev: 110599.040000,0.344093
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3a1fa00000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2583227043.000000
Throughput: 258322704.300000
Num Accesses - Session Mean & Stddev: 25832270.430000,107.678712
Num Phases Traversed: 11280567.000000
Num Phases Traversed - Session Mean & Stddev: 112805.670000,0.470213
Num Path Iterations: 11280567.000000
Num Path Iterations - Session Mean & Stddev: 112805.670000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb1f0400000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2529017476.000000
Throughput: 252901747.600000
Num Accesses - Session Mean & Stddev: 25290174.760000,113.672611
Num Phases Traversed: 11043844.000000
Num Phases Traversed - Session Mean & Stddev: 110438.440000,0.496387
Num Path Iterations: 11043844.000000
Num Path Iterations - Session Mean & Stddev: 110438.440000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5a87800000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2546618187.000000
Throughput: 254661818.700000
Num Accesses - Session Mean & Stddev: 25466181.870000,88.424844
Num Phases Traversed: 11120703.000000
Num Phases Traversed - Session Mean & Stddev: 111207.030000,0.386135
Num Path Iterations: 11120703.000000
Num Path Iterations - Session Mean & Stddev: 111207.030000,0.386135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcc42e00000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2556062605.000000
Throughput: 255606260.500000
Num Accesses - Session Mean & Stddev: 25560626.050000,113.926062
Num Phases Traversed: 11161945.000000
Num Phases Traversed - Session Mean & Stddev: 111619.450000,0.497494
Num Path Iterations: 11161945.000000
Num Path Iterations - Session Mean & Stddev: 111619.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5549c00000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2545164953.000000
Throughput: 254516495.300000
Num Accesses - Session Mean & Stddev: 25451649.530000,113.372347
Num Phases Traversed: 11114357.000000
Num Phases Traversed - Session Mean & Stddev: 111143.570000,0.495076
Num Path Iterations: 11114357.000000
Num Path Iterations - Session Mean & Stddev: 111143.570000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9d06400000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2525493395.000000
Throughput: 252549339.500000
Num Accesses - Session Mean & Stddev: 25254933.950000,113.926062
Num Phases Traversed: 11028455.000000
Num Phases Traversed - Session Mean & Stddev: 110284.550000,0.497494
Num Path Iterations: 11028455.000000
Num Path Iterations - Session Mean & Stddev: 110284.550000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd6afc00000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2547746699.000000
Throughput: 254774669.900000
Num Accesses - Session Mean & Stddev: 25477466.990000,105.910953
Num Phases Traversed: 11125631.000000
Num Phases Traversed - Session Mean & Stddev: 111256.310000,0.462493
Num Path Iterations: 11125631.000000
Num Path Iterations - Session Mean & Stddev: 111256.310000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.01 0/192,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.01 0/192,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f12ac400000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2534207532.000000
Throughput: 253420753.200000
Num Accesses - Session Mean & Stddev: 25342075.320000,77.183532
Num Phases Traversed: 11066508.000000
Num Phases Traversed - Session Mean & Stddev: 110665.080000,0.337046
Num Path Iterations: 11066508.000000
Num Path Iterations - Session Mean & Stddev: 110665.080000,0.337046
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd186200000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2114982540.000000
Throughput: 211498254.000000
Num Accesses - Session Mean & Stddev: 21149825.400000,896.023348
Num Phases Traversed: 1156460.000000
Num Phases Traversed - Session Mean & Stddev: 11564.600000,0.489898
Num Path Iterations: 1156460.000000
Num Path Iterations - Session Mean & Stddev: 11564.600000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4b51000000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2114872800.000000
Throughput: 211487280.000000
Num Accesses - Session Mean & Stddev: 21148728.000000,775.978982
Num Phases Traversed: 1156400.000000
Num Phases Traversed - Session Mean & Stddev: 11564.000000,0.424264
Num Path Iterations: 1156400.000000
Num Path Iterations - Session Mean & Stddev: 11564.000000,0.424264
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe440c00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2114706361.000000
Throughput: 211470636.100000
Num Accesses - Session Mean & Stddev: 21147063.610000,523.426440
Num Phases Traversed: 1156309.000000
Num Phases Traversed - Session Mean & Stddev: 11563.090000,0.286182
Num Path Iterations: 1156309.000000
Num Path Iterations - Session Mean & Stddev: 11563.090000,0.286182
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe719e00000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2114779521.000000
Throughput: 211477952.100000
Num Accesses - Session Mean & Stddev: 21147795.210000,914.317082
Num Phases Traversed: 1156349.000000
Num Phases Traversed - Session Mean & Stddev: 11563.490000,0.499900
Num Path Iterations: 1156349.000000
Num Path Iterations - Session Mean & Stddev: 11563.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbad2a00000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2114335074.000000
Throughput: 211433507.400000
Num Accesses - Session Mean & Stddev: 21143350.740000,723.322675
Num Phases Traversed: 1156106.000000
Num Phases Traversed - Session Mean & Stddev: 11561.060000,0.395474
Num Path Iterations: 1156106.000000
Num Path Iterations - Session Mean & Stddev: 11561.060000,0.395474
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb411600000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2114693558.000000
Throughput: 211469355.800000
Num Accesses - Session Mean & Stddev: 21146935.580000,775.116303
Num Phases Traversed: 1156302.000000
Num Phases Traversed - Session Mean & Stddev: 11563.020000,0.423792
Num Path Iterations: 1156302.000000
Num Path Iterations - Session Mean & Stddev: 11563.020000,0.423792
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9908400000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2115196533.000000
Throughput: 211519653.300000
Num Accesses - Session Mean & Stddev: 21151965.330000,769.702658
Num Phases Traversed: 1156577.000000
Num Phases Traversed - Session Mean & Stddev: 11565.770000,0.420833
Num Path Iterations: 1156577.000000
Num Path Iterations - Session Mean & Stddev: 11565.770000,0.420833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8b98e00000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2114730138.000000
Throughput: 211473013.800000
Num Accesses - Session Mean & Stddev: 21147301.380000,757.656489
Num Phases Traversed: 1156322.000000
Num Phases Traversed - Session Mean & Stddev: 11563.220000,0.414246
Num Path Iterations: 1156322.000000
Num Path Iterations - Session Mean & Stddev: 11563.220000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fca13e00000 of size 229376 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2114757573.000000
Throughput: 211475757.300000
Num Accesses - Session Mean & Stddev: 21147575.730000,883.049080
Num Phases Traversed: 1156337.000000
Num Phases Traversed - Session Mean & Stddev: 11563.370000,0.482804
Num Path Iterations: 1156337.000000
Num Path Iterations - Session Mean & Stddev: 11563.370000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4242000000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2114927670.000000
Throughput: 211492767.000000
Num Accesses - Session Mean & Stddev: 21149276.700000,838.153095
Num Phases Traversed: 1156430.000000
Num Phases Traversed - Session Mean & Stddev: 11564.300000,0.458258
Num Path Iterations: 1156430.000000
Num Path Iterations - Session Mean & Stddev: 11564.300000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1b0b200000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2114497855.000000
Throughput: 211449785.500000
Num Accesses - Session Mean & Stddev: 21144978.550000,791.980232
Num Phases Traversed: 1156195.000000
Num Phases Traversed - Session Mean & Stddev: 11561.950000,0.433013
Num Path Iterations: 1156195.000000
Num Path Iterations - Session Mean & Stddev: 11561.950000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1d7b600000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2114931328.000000
Throughput: 211493132.800000
Num Accesses - Session Mean & Stddev: 21149313.280000,853.184881
Num Phases Traversed: 1156432.000000
Num Phases Traversed - Session Mean & Stddev: 11564.320000,0.466476
Num Path Iterations: 1156432.000000
Num Path Iterations - Session Mean & Stddev: 11564.320000,0.466476
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd227400000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2115103254.000000
Throughput: 211510325.400000
Num Accesses - Session Mean & Stddev: 21151032.540000,802.262032
Num Phases Traversed: 1156526.000000
Num Phases Traversed - Session Mean & Stddev: 11565.260000,0.438634
Num Path Iterations: 1156526.000000
Num Path Iterations - Session Mean & Stddev: 11565.260000,0.438634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fda3e600000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2114325929.000000
Throughput: 211432592.900000
Num Accesses - Session Mean & Stddev: 21143259.290000,708.132492
Num Phases Traversed: 1156101.000000
Num Phases Traversed - Session Mean & Stddev: 11561.010000,0.387169
Num Path Iterations: 1156101.000000
Num Path Iterations - Session Mean & Stddev: 11561.010000,0.387169
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb4ad400000 of size 229376 bytes.
[1.263s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2114966079.000000
Throughput: 211496607.900000
Num Accesses - Session Mean & Stddev: 21149660.790000,914.317082
Num Phases Traversed: 1156451.000000
Num Phases Traversed - Session Mean & Stddev: 11564.510000,0.499900
Num Path Iterations: 1156451.000000
Num Path Iterations - Session Mean & Stddev: 11564.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f184be00000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2114689900.000000
Throughput: 211468990.000000
Num Accesses - Session Mean & Stddev: 21146899.000000,896.023348
Num Phases Traversed: 1156300.000000
Num Phases Traversed - Session Mean & Stddev: 11563.000000,0.489898
Num Path Iterations: 1156300.000000
Num Path Iterations - Session Mean & Stddev: 11563.000000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdf7a600000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2115317247.000000
Throughput: 211531724.700000
Num Accesses - Session Mean & Stddev: 21153172.470000,905.493550
Num Phases Traversed: 1156643.000000
Num Phases Traversed - Session Mean & Stddev: 11566.430000,0.495076
Num Path Iterations: 1156643.000000
Num Path Iterations - Session Mean & Stddev: 11566.430000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f350dc00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2115361143.000000
Throughput: 211536114.300000
Num Accesses - Session Mean & Stddev: 21153611.430000,860.019061
Num Phases Traversed: 1156667.000000
Num Phases Traversed - Session Mean & Stddev: 11566.670000,0.470213
Num Path Iterations: 1156667.000000
Num Path Iterations - Session Mean & Stddev: 11566.670000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2161800000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2114633201.000000
Throughput: 211463320.100000
Num Accesses - Session Mean & Stddev: 21146332.010000,845.900142
Num Phases Traversed: 1156269.000000
Num Phases Traversed - Session Mean & Stddev: 11562.690000,0.462493
Num Path Iterations: 1156269.000000
Num Path Iterations - Session Mean & Stddev: 11562.690000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6a95400000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2114863655.000000
Throughput: 211486365.500000
Num Accesses - Session Mean & Stddev: 21148636.550000,791.980232
Num Phases Traversed: 1156395.000000
Num Phases Traversed - Session Mean & Stddev: 11563.950000,0.433013
Num Path Iterations: 1156395.000000
Num Path Iterations - Session Mean & Stddev: 11563.950000,0.433013
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdf45400000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086260225.000000
Throughput: 208626022.500000
Num Accesses - Session Mean & Stddev: 20862602.250000,293.149599
Num Phases Traversed: 3081725.000000
Num Phases Traversed - Session Mean & Stddev: 30817.250000,0.433013
Num Path Iterations: 3081725.000000
Num Path Iterations - Session Mean & Stddev: 30817.250000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6113000000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086350266.000000
Throughput: 208635026.600000
Num Accesses - Session Mean & Stddev: 20863502.660000,334.139109
Num Phases Traversed: 3081858.000000
Num Phases Traversed - Session Mean & Stddev: 30818.580000,0.493559
Num Path Iterations: 3081858.000000
Num Path Iterations - Session Mean & Stddev: 30818.580000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9c7de00000 of size 81920 bytes.
[1.275s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086460617.000000
Throughput: 208646061.700000
Num Accesses - Session Mean & Stddev: 20864606.170000,275.747677
Num Phases Traversed: 3082021.000000
Num Phases Traversed - Session Mean & Stddev: 30820.210000,0.407308
Num Path Iterations: 3082021.000000
Num Path Iterations - Session Mean & Stddev: 30820.210000,0.407308
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb6ae800000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2085593380.000000
Throughput: 208559338.000000
Num Accesses - Session Mean & Stddev: 20855933.800000,331.660911
Num Phases Traversed: 3080740.000000
Num Phases Traversed - Session Mean & Stddev: 30807.400000,0.489898
Num Path Iterations: 3080740.000000
Num Path Iterations - Session Mean & Stddev: 30807.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3a74200000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2087389461.000000
Throughput: 208738946.100000
Num Accesses - Session Mean & Stddev: 20873894.610000,172.734530
Num Phases Traversed: 3083393.000000
Num Phases Traversed - Session Mean & Stddev: 30833.930000,0.255147
Num Path Iterations: 3083393.000000
Num Path Iterations - Session Mean & Stddev: 30833.930000,0.255147
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4dd5400000 of size 81920 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2085516879.000000
Throughput: 208551687.900000
Num Accesses - Session Mean & Stddev: 20855168.790000,300.560553
Num Phases Traversed: 3080627.000000
Num Phases Traversed - Session Mean & Stddev: 30806.270000,0.443959
Num Path Iterations: 3080627.000000
Num Path Iterations - Session Mean & Stddev: 30806.270000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe181800000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086350943.000000
Throughput: 208635094.300000
Num Accesses - Session Mean & Stddev: 20863509.430000,332.971148
Num Phases Traversed: 3081859.000000
Num Phases Traversed - Session Mean & Stddev: 30818.590000,0.491833
Num Path Iterations: 3081859.000000
Num Path Iterations - Session Mean & Stddev: 30818.590000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0357000000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2085872304.000000
Throughput: 208587230.400000
Num Accesses - Session Mean & Stddev: 20858723.040000,338.229092
Num Phases Traversed: 3081152.000000
Num Phases Traversed - Session Mean & Stddev: 30811.520000,0.499600
Num Path Iterations: 3081152.000000
Num Path Iterations - Session Mean & Stddev: 30811.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f448dc00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2083644297.000000
Throughput: 208364429.700000
Num Accesses - Session Mean & Stddev: 20836442.970000,330.206707
Num Phases Traversed: 3077861.000000
Num Phases Traversed - Session Mean & Stddev: 30778.610000,0.487750
Num Path Iterations: 3077861.000000
Num Path Iterations - Session Mean & Stddev: 30778.610000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f242d600000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2085531773.000000
Throughput: 208553177.300000
Num Accesses - Session Mean & Stddev: 20855317.730000,338.432293
Num Phases Traversed: 3080649.000000
Num Phases Traversed - Session Mean & Stddev: 30806.490000,0.499900
Num Path Iterations: 3080649.000000
Num Path Iterations - Session Mean & Stddev: 30806.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f27f3a00000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2085124896.000000
Throughput: 208512489.600000
Num Accesses - Session Mean & Stddev: 20851248.960000,338.229092
Num Phases Traversed: 3080048.000000
Num Phases Traversed - Session Mean & Stddev: 30800.480000,0.499600
Num Path Iterations: 3080048.000000
Num Path Iterations - Session Mean & Stddev: 30800.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd714400000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086272411.000000
Throughput: 208627241.100000
Num Accesses - Session Mean & Stddev: 20862724.110000,335.166284
Num Phases Traversed: 3081743.000000
Num Phases Traversed - Session Mean & Stddev: 30817.430000,0.495076
Num Path Iterations: 3081743.000000
Num Path Iterations - Session Mean & Stddev: 30817.430000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdad7400000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2087859976.000000
Throughput: 208785997.600000
Num Accesses - Session Mean & Stddev: 20878599.760000,219.998960
Num Phases Traversed: 3084088.000000
Num Phases Traversed - Session Mean & Stddev: 30840.880000,0.324962
Num Path Iterations: 3084088.000000
Num Path Iterations - Session Mean & Stddev: 30840.880000,0.324962
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6c5a800000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2084918411.000000
Throughput: 208491841.100000
Num Accesses - Session Mean & Stddev: 20849184.110000,335.166284
Num Phases Traversed: 3079743.000000
Num Phases Traversed - Session Mean & Stddev: 30797.430000,0.495076
Num Path Iterations: 3079743.000000
Num Path Iterations - Session Mean & Stddev: 30797.430000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa9f0e00000 of size 81920 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086687412.000000
Throughput: 208668741.200000
Num Accesses - Session Mean & Stddev: 20866874.120000,336.053962
Num Phases Traversed: 3082356.000000
Num Phases Traversed - Session Mean & Stddev: 30823.560000,0.496387
Num Path Iterations: 3082356.000000
Num Path Iterations - Session Mean & Stddev: 30823.560000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5854200000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086216897.000000
Throughput: 208621689.700000
Num Accesses - Session Mean & Stddev: 20862168.970000,330.206707
Num Phases Traversed: 3081661.000000
Num Phases Traversed - Session Mean & Stddev: 30816.610000,0.487750
Num Path Iterations: 3081661.000000
Num Path Iterations - Session Mean & Stddev: 30816.610000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f608fe00000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086313031.000000
Throughput: 208631303.100000
Num Accesses - Session Mean & Stddev: 20863130.310000,115.487549
Num Phases Traversed: 3081803.000000
Num Phases Traversed - Session Mean & Stddev: 30818.030000,0.170587
Num Path Iterations: 3081803.000000
Num Path Iterations - Session Mean & Stddev: 30818.030000,0.170587
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcdb8a00000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086462648.000000
Throughput: 208646264.800000
Num Accesses - Session Mean & Stddev: 20864626.480000,289.135279
Num Phases Traversed: 3082024.000000
Num Phases Traversed - Session Mean & Stddev: 30820.240000,0.427083
Num Path Iterations: 3082024.000000
Num Path Iterations - Session Mean & Stddev: 30820.240000,0.427083
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f69fd000000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2084875760.000000
Throughput: 208487576.000000
Num Accesses - Session Mean & Stddev: 20848757.600000,270.800000
Num Phases Traversed: 3079680.000000
Num Phases Traversed - Session Mean & Stddev: 30796.800000,0.400000
Num Path Iterations: 3079680.000000
Num Path Iterations - Session Mean & Stddev: 30796.800000,0.400000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2d8d200000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2087120692.000000
Throughput: 208712069.200000
Num Accesses - Session Mean & Stddev: 20871206.920000,132.664364
Num Phases Traversed: 3082996.000000
Num Phases Traversed - Session Mean & Stddev: 30829.960000,0.195959
Num Path Iterations: 3082996.000000
Num Path Iterations - Session Mean & Stddev: 30829.960000,0.195959
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fec93400000 of size 61440 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 976253268.000000
Throughput: 97625326.800000
Num Accesses - Session Mean & Stddev: 9762532.680000,19187.446291
Num Phases Traversed: 1888404.000000
Num Phases Traversed - Session Mean & Stddev: 18884.040000,37.113049
Num Path Iterations: 1888404.000000
Num Path Iterations - Session Mean & Stddev: 18884.040000,37.113049
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f00c3e00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 974548202.000000
Throughput: 97454820.200000
Num Accesses - Session Mean & Stddev: 9745482.020000,1056.553728
Num Phases Traversed: 1885106.000000
Num Phases Traversed - Session Mean & Stddev: 18851.060000,2.043624
Num Path Iterations: 1885106.000000
Num Path Iterations - Session Mean & Stddev: 18851.060000,2.043624
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6eb8200000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 974160452.000000
Throughput: 97416045.200000
Num Accesses - Session Mean & Stddev: 9741604.520000,7376.022892
Num Phases Traversed: 1884356.000000
Num Phases Traversed - Session Mean & Stddev: 18843.560000,14.266969
Num Path Iterations: 1884356.000000
Num Path Iterations - Session Mean & Stddev: 18843.560000,14.266969
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2ff4600000 of size 61440 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 975597195.000000
Throughput: 97559719.500000
Num Accesses - Session Mean & Stddev: 9755971.950000,23324.937874
Num Phases Traversed: 1887135.000000
Num Phases Traversed - Session Mean & Stddev: 18871.350000,45.115934
Num Path Iterations: 1887135.000000
Num Path Iterations - Session Mean & Stddev: 18871.350000,45.115934
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4b6ac00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 971237334.000000
Throughput: 97123733.400000
Num Accesses - Session Mean & Stddev: 9712373.340000,8700.659956
Num Phases Traversed: 1878702.000000
Num Phases Traversed - Session Mean & Stddev: 18787.020000,16.829130
Num Path Iterations: 1878702.000000
Num Path Iterations - Session Mean & Stddev: 18787.020000,16.829130
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f38eb400000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 976694786.000000
Throughput: 97669478.600000
Num Accesses - Session Mean & Stddev: 9766947.860000,2843.199184
Num Phases Traversed: 1889258.000000
Num Phases Traversed - Session Mean & Stddev: 18892.580000,5.499418
Num Path Iterations: 1889258.000000
Num Path Iterations - Session Mean & Stddev: 18892.580000,5.499418
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdf96c00000 of size 61440 bytes.
[1.267s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 978201324.000000
Throughput: 97820132.400000
Num Accesses - Session Mean & Stddev: 9782013.240000,8164.371285
Num Phases Traversed: 1892172.000000
Num Phases Traversed - Session Mean & Stddev: 18921.720000,15.791821
Num Path Iterations: 1892172.000000
Num Path Iterations - Session Mean & Stddev: 18921.720000,15.791821
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0b95600000 of size 61440 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 978133597.000000
Throughput: 97813359.700000
Num Accesses - Session Mean & Stddev: 9781335.970000,3071.776286
Num Phases Traversed: 1892041.000000
Num Phases Traversed - Session Mean & Stddev: 18920.410000,5.941540
Num Path Iterations: 1892041.000000
Num Path Iterations - Session Mean & Stddev: 18920.410000,5.941540
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8d01600000 of size 61440 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 976497809.000000
Throughput: 97649780.900000
Num Accesses - Session Mean & Stddev: 9764978.090000,5425.227142
Num Phases Traversed: 1888877.000000
Num Phases Traversed - Session Mean & Stddev: 18888.770000,10.493670
Num Path Iterations: 1888877.000000
Num Path Iterations - Session Mean & Stddev: 18888.770000,10.493670
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f474a600000 of size 61440 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 976975000.000000
Throughput: 97697500.000000
Num Accesses - Session Mean & Stddev: 9769750.000000,19600.914847
Num Phases Traversed: 1889800.000000
Num Phases Traversed - Session Mean & Stddev: 18898.000000,37.912795
Num Path Iterations: 1889800.000000
Num Path Iterations - Session Mean & Stddev: 18898.000000,37.912795
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f39fe200000 of size 61440 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 974942673.000000
Throughput: 97494267.300000
Num Accesses - Session Mean & Stddev: 9749426.730000,1353.503630
Num Phases Traversed: 1885869.000000
Num Phases Traversed - Session Mean & Stddev: 18858.690000,2.617995
Num Path Iterations: 1885869.000000
Num Path Iterations - Session Mean & Stddev: 18858.690000,2.617995
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4731a00000 of size 61440 bytes.
[1.257s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 975961163.000000
Throughput: 97596116.300000
Num Accesses - Session Mean & Stddev: 9759611.630000,24310.865553
Num Phases Traversed: 1887839.000000
Num Phases Traversed - Session Mean & Stddev: 18878.390000,47.022951
Num Path Iterations: 1887839.000000
Num Path Iterations - Session Mean & Stddev: 18878.390000,47.022951
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faee6c00000 of size 61440 bytes.
[1.247s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 974730186.000000
Throughput: 97473018.600000
Num Accesses - Session Mean & Stddev: 9747301.860000,5277.544509
Num Phases Traversed: 1885458.000000
Num Phases Traversed - Session Mean & Stddev: 18854.580000,10.208016
Num Path Iterations: 1885458.000000
Num Path Iterations - Session Mean & Stddev: 18854.580000,10.208016
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb1afe00000 of size 61440 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 973681193.000000
Throughput: 97368119.300000
Num Accesses - Session Mean & Stddev: 9736811.930000,25106.011337
Num Phases Traversed: 1883429.000000
Num Phases Traversed - Session Mean & Stddev: 18834.290000,48.560950
Num Path Iterations: 1883429.000000
Num Path Iterations - Session Mean & Stddev: 18834.290000,48.560950
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7477e00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 975309226.000000
Throughput: 97530922.600000
Num Accesses - Session Mean & Stddev: 9753092.260000,5470.231367
Num Phases Traversed: 1886578.000000
Num Phases Traversed - Session Mean & Stddev: 18865.780000,10.580718
Num Path Iterations: 1886578.000000
Num Path Iterations - Session Mean & Stddev: 18865.780000,10.580718
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2bc6c00000 of size 61440 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 976266710.000000
Throughput: 97626671.000000
Num Accesses - Session Mean & Stddev: 9762667.100000,6888.527867
Num Phases Traversed: 1888430.000000
Num Phases Traversed - Session Mean & Stddev: 18884.300000,13.324038
Num Path Iterations: 1888430.000000
Num Path Iterations - Session Mean & Stddev: 18884.300000,13.324038
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb816200000 of size 61440 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 977381362.000000
Throughput: 97738136.200000
Num Accesses - Session Mean & Stddev: 9773813.620000,5613.210689
Num Phases Traversed: 1890586.000000
Num Phases Traversed - Session Mean & Stddev: 18905.860000,10.857274
Num Path Iterations: 1890586.000000
Num Path Iterations - Session Mean & Stddev: 18905.860000,10.857274
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7effb3c00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 978560122.000000
Throughput: 97856012.200000
Num Accesses - Session Mean & Stddev: 9785601.220000,16170.817869
Num Phases Traversed: 1892866.000000
Num Phases Traversed - Session Mean & Stddev: 18928.660000,31.278178
Num Path Iterations: 1892866.000000
Num Path Iterations - Session Mean & Stddev: 18928.660000,31.278178
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f409e000000 of size 61440 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 978123257.000000
Throughput: 97812325.700000
Num Accesses - Session Mean & Stddev: 9781232.570000,14759.475330
Num Phases Traversed: 1892021.000000
Num Phases Traversed - Session Mean & Stddev: 18920.210000,28.548308
Num Path Iterations: 1892021.000000
Num Path Iterations - Session Mean & Stddev: 18920.210000,28.548308
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.1 0/480,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.1 0/480,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f80e6c00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 977732405.000000
Throughput: 97773240.500000
Num Accesses - Session Mean & Stddev: 9777324.050000,382.545066
Num Phases Traversed: 1891265.000000
Num Phases Traversed - Session Mean & Stddev: 18912.650000,0.739932
Num Path Iterations: 1891265.000000
Num Path Iterations - Session Mean & Stddev: 18912.650000,0.739932
+ set +x

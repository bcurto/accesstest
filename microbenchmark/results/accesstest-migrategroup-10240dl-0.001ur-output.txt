++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa158400000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 826280425.000000
Throughput: 82628042.500000
Num Accesses - Session Mean & Stddev: 8262804.250000,3371.826278
Num Phases Traversed: 160325.000000
Num Phases Traversed - Session Mean & Stddev: 1603.250000,0.653835
Num Path Iterations: 160325.000000
Num Path Iterations - Session Mean & Stddev: 1603.250000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdf73400000 of size 655360 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 825316066.000000
Throughput: 82531606.600000
Num Accesses - Session Mean & Stddev: 8253160.660000,2987.501331
Num Phases Traversed: 160138.000000
Num Phases Traversed - Session Mean & Stddev: 1601.380000,0.579310
Num Path Iterations: 160138.000000
Num Path Iterations - Session Mean & Stddev: 1601.380000,0.579310
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4f38c00000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 826388722.000000
Throughput: 82638872.200000
Num Accesses - Session Mean & Stddev: 8263887.220000,2570.235556
Num Phases Traversed: 160346.000000
Num Phases Traversed - Session Mean & Stddev: 1603.460000,0.498397
Num Path Iterations: 160346.000000
Num Path Iterations - Session Mean & Stddev: 1603.460000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3c54400000 of size 655360 bytes.
[1.292s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 826368094.000000
Throughput: 82636809.400000
Num Accesses - Session Mean & Stddev: 8263680.940000,2841.502320
Num Phases Traversed: 160342.000000
Num Phases Traversed - Session Mean & Stddev: 1603.420000,0.550999
Num Path Iterations: 160342.000000
Num Path Iterations - Session Mean & Stddev: 1603.420000,0.550999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feee3200000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 825981319.000000
Throughput: 82598131.900000
Num Accesses - Session Mean & Stddev: 8259813.190000,3098.923644
Num Phases Traversed: 160267.000000
Num Phases Traversed - Session Mean & Stddev: 1602.670000,0.600916
Num Path Iterations: 160267.000000
Num Path Iterations - Session Mean & Stddev: 1602.670000,0.600916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2a0be00000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 826156657.000000
Throughput: 82615665.700000
Num Accesses - Session Mean & Stddev: 8261566.570000,3609.531624
Num Phases Traversed: 160301.000000
Num Phases Traversed - Session Mean & Stddev: 1603.010000,0.699929
Num Path Iterations: 160301.000000
Num Path Iterations - Session Mean & Stddev: 1603.010000,0.699929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fab09200000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 827487163.000000
Throughput: 82748716.300000
Num Accesses - Session Mean & Stddev: 8274871.630000,2536.384354
Num Phases Traversed: 160559.000000
Num Phases Traversed - Session Mean & Stddev: 1605.590000,0.491833
Num Path Iterations: 160559.000000
Num Path Iterations - Session Mean & Stddev: 1605.590000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fda46e00000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 825501718.000000
Throughput: 82550171.800000
Num Accesses - Session Mean & Stddev: 8255017.180000,3311.741724
Num Phases Traversed: 160174.000000
Num Phases Traversed - Session Mean & Stddev: 1601.740000,0.642184
Num Path Iterations: 160174.000000
Num Path Iterations - Session Mean & Stddev: 1601.740000,0.642184
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdf66600000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 826182442.000000
Throughput: 82618244.200000
Num Accesses - Session Mean & Stddev: 8261824.420000,3559.450910
Num Phases Traversed: 160306.000000
Num Phases Traversed - Session Mean & Stddev: 1603.060000,0.690217
Num Path Iterations: 160306.000000
Num Path Iterations - Session Mean & Stddev: 1603.060000,0.690217
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa2c1200000 of size 655360 bytes.
[1.287s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 825816295.000000
Throughput: 82581629.500000
Num Accesses - Session Mean & Stddev: 8258162.950000,3040.006666
Num Phases Traversed: 160235.000000
Num Phases Traversed - Session Mean & Stddev: 1602.350000,0.589491
Num Path Iterations: 160235.000000
Num Path Iterations - Session Mean & Stddev: 1602.350000,0.589491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fab16600000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 826146343.000000
Throughput: 82614634.300000
Num Accesses - Session Mean & Stddev: 8261463.430000,3609.531624
Num Phases Traversed: 160299.000000
Num Phases Traversed - Session Mean & Stddev: 1602.990000,0.699929
Num Path Iterations: 160299.000000
Num Path Iterations - Session Mean & Stddev: 1602.990000,0.699929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f09a7e00000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 826429978.000000
Throughput: 82642997.800000
Num Accesses - Session Mean & Stddev: 8264299.780000,2671.704286
Num Phases Traversed: 160354.000000
Num Phases Traversed - Session Mean & Stddev: 1603.540000,0.518073
Num Path Iterations: 160354.000000
Num Path Iterations - Session Mean & Stddev: 1603.540000,0.518073
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f28bb800000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 826094773.000000
Throughput: 82609477.300000
Num Accesses - Session Mean & Stddev: 8260947.730000,3565.050176
Num Phases Traversed: 160289.000000
Num Phases Traversed - Session Mean & Stddev: 1602.890000,0.691303
Num Path Iterations: 160289.000000
Num Path Iterations - Session Mean & Stddev: 1602.890000,0.691303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff9b3800000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 825145885.000000
Throughput: 82514588.500000
Num Accesses - Session Mean & Stddev: 8251458.850000,3600.679295
Num Phases Traversed: 160105.000000
Num Phases Traversed - Session Mean & Stddev: 1601.050000,0.698212
Num Path Iterations: 160105.000000
Num Path Iterations - Session Mean & Stddev: 1601.050000,0.698212
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f08e0e00000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 826945678.000000
Throughput: 82694567.800000
Num Accesses - Session Mean & Stddev: 8269456.780000,2570.235556
Num Phases Traversed: 160454.000000
Num Phases Traversed - Session Mean & Stddev: 1604.540000,0.498397
Num Path Iterations: 160454.000000
Num Path Iterations - Session Mean & Stddev: 1604.540000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2166c00000 of size 655360 bytes.
[1.287s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 827296354.000000
Throughput: 82729635.400000
Num Accesses - Session Mean & Stddev: 8272963.540000,3387.956686
Num Phases Traversed: 160522.000000
Num Phases Traversed - Session Mean & Stddev: 1605.220000,0.656963
Num Path Iterations: 160522.000000
Num Path Iterations - Session Mean & Stddev: 1605.220000,0.656963
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f27c3600000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 826940521.000000
Throughput: 82694052.100000
Num Accesses - Session Mean & Stddev: 8269405.210000,2573.854515
Num Phases Traversed: 160453.000000
Num Phases Traversed - Session Mean & Stddev: 1604.530000,0.499099
Num Path Iterations: 160453.000000
Num Path Iterations - Session Mean & Stddev: 1604.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8f1e000000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 825646114.000000
Throughput: 82564611.400000
Num Accesses - Session Mean & Stddev: 8256461.140000,3571.385398
Num Phases Traversed: 160202.000000
Num Phases Traversed - Session Mean & Stddev: 1602.020000,0.692532
Num Path Iterations: 160202.000000
Num Path Iterations - Session Mean & Stddev: 1602.020000,0.692532
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb468600000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 825893650.000000
Throughput: 82589365.000000
Num Accesses - Session Mean & Stddev: 8258936.500000,2578.500000
Num Phases Traversed: 160250.000000
Num Phases Traversed - Session Mean & Stddev: 1602.500000,0.500000
Num Path Iterations: 160250.000000
Num Path Iterations - Session Mean & Stddev: 1602.500000,0.500000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0.001 0/5120,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0.001 0/5120,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f83af600000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 825909121.000000
Throughput: 82590912.100000
Num Accesses - Session Mean & Stddev: 8259091.210000,2675.185983
Num Phases Traversed: 160253.000000
Num Phases Traversed - Session Mean & Stddev: 1602.530000,0.518748
Num Path Iterations: 160253.000000
Num Path Iterations - Session Mean & Stddev: 1602.530000,0.518748
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0efda00000 of size 65536 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2389268413.000000
Throughput: 238926841.300000
Num Accesses - Session Mean & Stddev: 23892684.130000,307.194813
Num Phases Traversed: 4352137.000000
Num Phases Traversed - Session Mean & Stddev: 43521.370000,0.559553
Num Path Iterations: 4352137.000000
Num Path Iterations - Session Mean & Stddev: 43521.370000,0.559553
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7c41a00000 of size 65536 bytes.
[1.263s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2395877275.000000
Throughput: 239587727.500000
Num Accesses - Session Mean & Stddev: 23958772.750000,273.124051
Num Phases Traversed: 4364175.000000
Num Phases Traversed - Session Mean & Stddev: 43641.750000,0.497494
Num Path Iterations: 4364175.000000
Num Path Iterations - Session Mean & Stddev: 43641.750000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3af1200000 of size 65536 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2391984865.000000
Throughput: 239198486.500000
Num Accesses - Session Mean & Stddev: 23919848.650000,294.368455
Num Phases Traversed: 4357085.000000
Num Phases Traversed - Session Mean & Stddev: 43570.850000,0.536190
Num Path Iterations: 4357085.000000
Num Path Iterations - Session Mean & Stddev: 43570.850000,0.536190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f78c7600000 of size 65536 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2386857754.000000
Throughput: 238685775.400000
Num Accesses - Session Mean & Stddev: 23868577.540000,273.620190
Num Phases Traversed: 4347746.000000
Num Phases Traversed - Session Mean & Stddev: 43477.460000,0.498397
Num Path Iterations: 4347746.000000
Num Path Iterations - Session Mean & Stddev: 43477.460000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa104a00000 of size 65536 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2390728753.000000
Throughput: 239072875.300000
Num Accesses - Session Mean & Stddev: 23907287.530000,383.946909
Num Phases Traversed: 4354797.000000
Num Phases Traversed - Session Mean & Stddev: 43547.970000,0.699357
Num Path Iterations: 4354797.000000
Num Path Iterations - Session Mean & Stddev: 43547.970000,0.699357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f42e8600000 of size 65536 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2383748218.000000
Throughput: 238374821.800000
Num Accesses - Session Mean & Stddev: 23837482.180000,367.297002
Num Phases Traversed: 4342082.000000
Num Phases Traversed - Session Mean & Stddev: 43420.820000,0.669029
Num Path Iterations: 4342082.000000
Num Path Iterations - Session Mean & Stddev: 43420.820000,0.669029
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1996400000 of size 65536 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2388125944.000000
Throughput: 238812594.400000
Num Accesses - Session Mean & Stddev: 23881259.440000,272.516433
Num Phases Traversed: 4350056.000000
Num Phases Traversed - Session Mean & Stddev: 43500.560000,0.496387
Num Path Iterations: 4350056.000000
Num Path Iterations - Session Mean & Stddev: 43500.560000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5558800000 of size 65536 bytes.
[1.220s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2392728211.000000
Throughput: 239272821.100000
Num Accesses - Session Mean & Stddev: 23927282.110000,337.489908
Num Phases Traversed: 4358439.000000
Num Phases Traversed - Session Mean & Stddev: 43584.390000,0.614736
Num Path Iterations: 4358439.000000
Num Path Iterations - Session Mean & Stddev: 43584.390000,0.614736
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0d66400000 of size 65536 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2388346642.000000
Throughput: 238834664.200000
Num Accesses - Session Mean & Stddev: 23883466.420000,270.963620
Num Phases Traversed: 4350458.000000
Num Phases Traversed - Session Mean & Stddev: 43504.580000,0.493559
Num Path Iterations: 4350458.000000
Num Path Iterations - Session Mean & Stddev: 43504.580000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f215be00000 of size 65536 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2399959639.000000
Throughput: 239995963.900000
Num Accesses - Session Mean & Stddev: 23999596.390000,379.525411
Num Phases Traversed: 4371611.000000
Num Phases Traversed - Session Mean & Stddev: 43716.110000,0.691303
Num Path Iterations: 4371611.000000
Num Path Iterations - Session Mean & Stddev: 43716.110000,0.691303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9761c00000 of size 65536 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2388736981.000000
Throughput: 238873698.100000
Num Accesses - Session Mean & Stddev: 23887369.810000,297.626870
Num Phases Traversed: 4351169.000000
Num Phases Traversed - Session Mean & Stddev: 43511.690000,0.542125
Num Path Iterations: 4351169.000000
Num Path Iterations - Session Mean & Stddev: 43511.690000,0.542125
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa8ed200000 of size 65536 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2389451779.000000
Throughput: 238945177.900000
Num Accesses - Session Mean & Stddev: 23894517.790000,341.043466
Num Phases Traversed: 4352471.000000
Num Phases Traversed - Session Mean & Stddev: 43524.710000,0.621208
Num Path Iterations: 4352471.000000
Num Path Iterations - Session Mean & Stddev: 43524.710000,0.621208
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feccca00000 of size 65536 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2397393613.000000
Throughput: 239739361.300000
Num Accesses - Session Mean & Stddev: 23973936.130000,316.854341
Num Phases Traversed: 4366937.000000
Num Phases Traversed - Session Mean & Stddev: 43669.370000,0.577148
Num Path Iterations: 4366937.000000
Num Path Iterations - Session Mean & Stddev: 43669.370000,0.577148
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbe33e00000 of size 65536 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2392229719.000000
Throughput: 239222971.900000
Num Accesses - Session Mean & Stddev: 23922297.190000,253.908791
Num Phases Traversed: 4357531.000000
Num Phases Traversed - Session Mean & Stddev: 43575.310000,0.462493
Num Path Iterations: 4357531.000000
Num Path Iterations - Session Mean & Stddev: 43575.310000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f57fbc00000 of size 65536 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2381251915.000000
Throughput: 238125191.500000
Num Accesses - Session Mean & Stddev: 23812519.150000,304.435227
Num Phases Traversed: 4337535.000000
Num Phases Traversed - Session Mean & Stddev: 43375.350000,0.554527
Num Path Iterations: 4337535.000000
Num Path Iterations - Session Mean & Stddev: 43375.350000,0.554527
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe6fe200000 of size 65536 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2399129551.000000
Throughput: 239912955.100000
Num Accesses - Session Mean & Stddev: 23991295.510000,315.328701
Num Phases Traversed: 4370099.000000
Num Phases Traversed - Session Mean & Stddev: 43700.990000,0.574369
Num Path Iterations: 4370099.000000
Num Path Iterations - Session Mean & Stddev: 43700.990000,0.574369
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd50ce00000 of size 65536 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2387429263.000000
Throughput: 238742926.300000
Num Accesses - Session Mean & Stddev: 23874292.630000,377.614636
Num Phases Traversed: 4348787.000000
Num Phases Traversed - Session Mean & Stddev: 43487.870000,0.687823
Num Path Iterations: 4348787.000000
Num Path Iterations - Session Mean & Stddev: 43487.870000,0.687823
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5ac1800000 of size 65536 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2394537715.000000
Throughput: 239453771.500000
Num Accesses - Session Mean & Stddev: 23945377.150000,332.813563
Num Phases Traversed: 4361735.000000
Num Phases Traversed - Session Mean & Stddev: 43617.350000,0.606218
Num Path Iterations: 4361735.000000
Num Path Iterations - Session Mean & Stddev: 43617.350000,0.606218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2839c00000 of size 65536 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2390817691.000000
Throughput: 239081769.100000
Num Accesses - Session Mean & Stddev: 23908176.910000,311.481913
Num Phases Traversed: 4354959.000000
Num Phases Traversed - Session Mean & Stddev: 43549.590000,0.567362
Num Path Iterations: 4354959.000000
Num Path Iterations - Session Mean & Stddev: 43549.590000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/512,0.1 0/512,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/512,0.1 0/512,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    512 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8048c00000 of size 65536 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2390816044.000000
Throughput: 239081604.400000
Num Accesses - Session Mean & Stddev: 23908160.440000,272.516433
Num Phases Traversed: 4354956.000000
Num Phases Traversed - Session Mean & Stddev: 43549.560000,0.496387
Num Path Iterations: 4354956.000000
Num Path Iterations - Session Mean & Stddev: 43549.560000,0.496387
+ set +x

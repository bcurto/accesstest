++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0630800000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2798764111.000000
Throughput: 279876411.100000
Num Accesses - Session Mean & Stddev: 27987641.110000,311.583661
Num Phases Traversed: 6178387.000000
Num Phases Traversed - Session Mean & Stddev: 61783.870000,0.687823
Num Path Iterations: 6178387.000000
Num Path Iterations - Session Mean & Stddev: 61783.870000,0.687823
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5756000000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2783861770.000000
Throughput: 278386177.000000
Num Accesses - Session Mean & Stddev: 27838617.700000,252.219726
Num Phases Traversed: 6145490.000000
Num Phases Traversed - Session Mean & Stddev: 61454.900000,0.556776
Num Path Iterations: 6145490.000000
Num Path Iterations - Session Mean & Stddev: 61454.900000,0.556776
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3c80200000 of size 53248 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2796106813.000000
Throughput: 279610681.300000
Num Accesses - Session Mean & Stddev: 27961068.130000,302.494121
Num Phases Traversed: 6172521.000000
Num Phases Traversed - Session Mean & Stddev: 61725.210000,0.667757
Num Path Iterations: 6172521.000000
Num Path Iterations - Session Mean & Stddev: 61725.210000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc6cac00000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2796019837.000000
Throughput: 279601983.700000
Num Accesses - Session Mean & Stddev: 27960198.370000,288.607576
Num Phases Traversed: 6172329.000000
Num Phases Traversed - Session Mean & Stddev: 61723.290000,0.637103
Num Path Iterations: 6172329.000000
Num Path Iterations - Session Mean & Stddev: 61723.290000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffb66c00000 of size 53248 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2807705425.000000
Throughput: 280770542.500000
Num Accesses - Session Mean & Stddev: 28077054.250000,296.187183
Num Phases Traversed: 6198125.000000
Num Phases Traversed - Session Mean & Stddev: 61981.250000,0.653835
Num Path Iterations: 6198125.000000
Num Path Iterations - Session Mean & Stddev: 61981.250000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8815200000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2769274717.000000
Throughput: 276927471.700000
Num Accesses - Session Mean & Stddev: 27692747.170000,238.804483
Num Phases Traversed: 6113289.000000
Num Phases Traversed - Session Mean & Stddev: 61132.890000,0.527162
Num Path Iterations: 6113289.000000
Num Path Iterations - Session Mean & Stddev: 61132.890000,0.527162
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fecf3400000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2790015322.000000
Throughput: 279001532.200000
Num Accesses - Session Mean & Stddev: 27900153.220000,261.173260
Num Phases Traversed: 6159074.000000
Num Phases Traversed - Session Mean & Stddev: 61590.740000,0.576541
Num Path Iterations: 6159074.000000
Num Path Iterations - Session Mean & Stddev: 61590.740000,0.576541
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f89d0400000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2814393517.000000
Throughput: 281439351.700000
Num Accesses - Session Mean & Stddev: 28143935.170000,211.459313
Num Phases Traversed: 6212889.000000
Num Phases Traversed - Session Mean & Stddev: 62128.890000,0.466798
Num Path Iterations: 6212889.000000
Num Path Iterations - Session Mean & Stddev: 62128.890000,0.466798
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8a6cc00000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2809296361.000000
Throughput: 280929636.100000
Num Accesses - Session Mean & Stddev: 28092963.610000,269.182648
Num Phases Traversed: 6201637.000000
Num Phases Traversed - Session Mean & Stddev: 62016.370000,0.594222
Num Path Iterations: 6201637.000000
Num Path Iterations - Session Mean & Stddev: 62016.370000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc38c600000 of size 53248 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2796468307.000000
Throughput: 279646830.700000
Num Accesses - Session Mean & Stddev: 27964683.070000,305.195618
Num Phases Traversed: 6173319.000000
Num Phases Traversed - Session Mean & Stddev: 61733.190000,0.673721
Num Path Iterations: 6173319.000000
Num Path Iterations - Session Mean & Stddev: 61733.190000,0.673721
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb4fe800000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2795194471.000000
Throughput: 279519447.100000
Num Accesses - Session Mean & Stddev: 27951944.710000,315.510516
Num Phases Traversed: 6170507.000000
Num Phases Traversed - Session Mean & Stddev: 61705.070000,0.696491
Num Path Iterations: 6170507.000000
Num Path Iterations - Session Mean & Stddev: 61705.070000,0.696491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f90d7200000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2808727393.000000
Throughput: 280872739.300000
Num Accesses - Session Mean & Stddev: 28087273.930000,305.195618
Num Phases Traversed: 6200381.000000
Num Phases Traversed - Session Mean & Stddev: 62003.810000,0.673721
Num Path Iterations: 6200381.000000
Num Path Iterations - Session Mean & Stddev: 62003.810000,0.673721
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd4be600000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2791633438.000000
Throughput: 279163343.800000
Num Accesses - Session Mean & Stddev: 27916334.380000,225.774037
Num Phases Traversed: 6162646.000000
Num Phases Traversed - Session Mean & Stddev: 61626.460000,0.498397
Num Path Iterations: 6162646.000000
Num Path Iterations - Session Mean & Stddev: 61626.460000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbf0ea00000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2793993115.000000
Throughput: 279399311.500000
Num Accesses - Session Mean & Stddev: 27939931.150000,242.894190
Num Phases Traversed: 6167855.000000
Num Phases Traversed - Session Mean & Stddev: 61678.550000,0.536190
Num Path Iterations: 6167855.000000
Num Path Iterations - Session Mean & Stddev: 61678.550000,0.536190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f97d9000000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2797474873.000000
Throughput: 279747487.300000
Num Accesses - Session Mean & Stddev: 27974748.730000,257.015130
Num Phases Traversed: 6175541.000000
Num Phases Traversed - Session Mean & Stddev: 61755.410000,0.567362
Num Path Iterations: 6175541.000000
Num Path Iterations - Session Mean & Stddev: 61755.410000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8644800000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2791248388.000000
Throughput: 279124838.800000
Num Accesses - Session Mean & Stddev: 27912483.880000,271.195327
Num Phases Traversed: 6161796.000000
Num Phases Traversed - Session Mean & Stddev: 61617.960000,0.598665
Num Path Iterations: 6161796.000000
Num Path Iterations - Session Mean & Stddev: 61617.960000,0.598665
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1316a00000 of size 53248 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2801600797.000000
Throughput: 280160079.700000
Num Accesses - Session Mean & Stddev: 28016007.970000,226.454695
Num Phases Traversed: 6184649.000000
Num Phases Traversed - Session Mean & Stddev: 61846.490000,0.499900
Num Path Iterations: 6184649.000000
Num Path Iterations - Session Mean & Stddev: 61846.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7dfea00000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2775178213.000000
Throughput: 277517821.300000
Num Accesses - Session Mean & Stddev: 27751782.130000,302.494121
Num Phases Traversed: 6126321.000000
Num Phases Traversed - Session Mean & Stddev: 61263.210000,0.667757
Num Path Iterations: 6126321.000000
Num Path Iterations - Session Mean & Stddev: 61263.210000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5ecd800000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2790650881.000000
Throughput: 279065088.100000
Num Accesses - Session Mean & Stddev: 27906508.810000,299.494330
Num Phases Traversed: 6160477.000000
Num Phases Traversed - Session Mean & Stddev: 61604.770000,0.661135
Num Path Iterations: 6160477.000000
Num Path Iterations - Session Mean & Stddev: 61604.770000,0.661135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.1 0/416,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.1 0/416,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdf59800000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2769306880.000000
Throughput: 276930688.000000
Num Accesses - Session Mean & Stddev: 27693068.800000,221.923771
Num Phases Traversed: 6113360.000000
Num Phases Traversed - Session Mean & Stddev: 61133.600000,0.489898
Num Path Iterations: 6113360.000000
Num Path Iterations - Session Mean & Stddev: 61133.600000,0.489898
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f92b1800000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1524870322.000000
Throughput: 152487032.200000
Num Accesses - Session Mean & Stddev: 15248703.220000,2778.394884
Num Phases Traversed: 833818.000000
Num Phases Traversed - Session Mean & Stddev: 8338.180000,1.519079
Num Path Iterations: 833818.000000
Num Path Iterations - Session Mean & Stddev: 8338.180000,1.519079
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd3cf400000 of size 229376 bytes.
[1.221s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1539059704.000000
Throughput: 153905970.400000
Num Accesses - Session Mean & Stddev: 15390597.040000,4540.342844
Num Phases Traversed: 841576.000000
Num Phases Traversed - Session Mean & Stddev: 8415.760000,2.482418
Num Path Iterations: 841576.000000
Num Path Iterations - Session Mean & Stddev: 8415.760000,2.482418
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbe50a00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1518318844.000000
Throughput: 151831884.400000
Num Accesses - Session Mean & Stddev: 15183188.440000,41078.476794
Num Phases Traversed: 830236.000000
Num Phases Traversed - Session Mean & Stddev: 8302.360000,22.459528
Num Path Iterations: 830236.000000
Num Path Iterations - Session Mean & Stddev: 8302.360000,22.459528
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7098800000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1524027153.000000
Throughput: 152402715.300000
Num Accesses - Session Mean & Stddev: 15240271.530000,15463.986789
Num Phases Traversed: 833357.000000
Num Phases Traversed - Session Mean & Stddev: 8333.570000,8.454886
Num Path Iterations: 833357.000000
Num Path Iterations - Session Mean & Stddev: 8333.570000,8.454886
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1d50600000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1539729118.000000
Throughput: 153972911.800000
Num Accesses - Session Mean & Stddev: 15397291.180000,4204.154534
Num Phases Traversed: 841942.000000
Num Phases Traversed - Session Mean & Stddev: 8419.420000,2.298608
Num Path Iterations: 841942.000000
Num Path Iterations - Session Mean & Stddev: 8419.420000,2.298608
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb828400000 of size 229376 bytes.
[1.267s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1517916464.000000
Throughput: 151791646.400000
Num Accesses - Session Mean & Stddev: 15179164.640000,87038.772078
Num Phases Traversed: 830016.000000
Num Phases Traversed - Session Mean & Stddev: 8300.160000,47.588175
Num Path Iterations: 830016.000000
Num Path Iterations - Session Mean & Stddev: 8300.160000,47.588175
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f756cc00000 of size 229376 bytes.
[1.237s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1519739977.000000
Throughput: 151973997.700000
Num Accesses - Session Mean & Stddev: 15197399.770000,120194.013540
Num Phases Traversed: 831013.000000
Num Phases Traversed - Session Mean & Stddev: 8310.130000,65.715699
Num Path Iterations: 831013.000000
Num Path Iterations - Session Mean & Stddev: 8310.130000,65.715699
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdfba600000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1516767852.000000
Throughput: 151676785.200000
Num Accesses - Session Mean & Stddev: 15167678.520000,2168.425869
Num Phases Traversed: 829388.000000
Num Phases Traversed - Session Mean & Stddev: 8293.880000,1.185580
Num Path Iterations: 829388.000000
Num Path Iterations - Session Mean & Stddev: 8293.880000,1.185580
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7844400000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1532892316.000000
Throughput: 153289231.600000
Num Accesses - Session Mean & Stddev: 15328923.160000,1654.613319
Num Phases Traversed: 838204.000000
Num Phases Traversed - Session Mean & Stddev: 8382.040000,0.904655
Num Path Iterations: 838204.000000
Num Path Iterations - Session Mean & Stddev: 8382.040000,0.904655
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2bb9000000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1536275966.000000
Throughput: 153627596.600000
Num Accesses - Session Mean & Stddev: 15362759.660000,18106.952201
Num Phases Traversed: 840054.000000
Num Phases Traversed - Session Mean & Stddev: 8400.540000,9.899919
Num Path Iterations: 840054.000000
Num Path Iterations - Session Mean & Stddev: 8400.540000,9.899919
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb850a00000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1524502693.000000
Throughput: 152450269.300000
Num Accesses - Session Mean & Stddev: 15245026.930000,113244.807749
Num Phases Traversed: 833617.000000
Num Phases Traversed - Session Mean & Stddev: 8336.170000,61.916243
Num Path Iterations: 833617.000000
Num Path Iterations - Session Mean & Stddev: 8336.170000,61.916243
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1096000000 of size 229376 bytes.
[1.267s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1544963716.000000
Throughput: 154496371.600000
Num Accesses - Session Mean & Stddev: 15449637.160000,1845.750101
Num Phases Traversed: 844804.000000
Num Phases Traversed - Session Mean & Stddev: 8448.040000,1.009158
Num Path Iterations: 844804.000000
Num Path Iterations - Session Mean & Stddev: 8448.040000,1.009158
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7ebea00000 of size 229376 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1549132007.000000
Throughput: 154913200.700000
Num Accesses - Session Mean & Stddev: 15491320.070000,1637.031632
Num Phases Traversed: 847083.000000
Num Phases Traversed - Session Mean & Stddev: 8470.830000,0.895042
Num Path Iterations: 847083.000000
Num Path Iterations - Session Mean & Stddev: 8470.830000,0.895042
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7479800000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1541110013.000000
Throughput: 154111001.300000
Num Accesses - Session Mean & Stddev: 15411100.130000,8596.124881
Num Phases Traversed: 842697.000000
Num Phases Traversed - Session Mean & Stddev: 8426.970000,4.699904
Num Path Iterations: 842697.000000
Num Path Iterations - Session Mean & Stddev: 8426.970000,4.699904
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8908a00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1522556637.000000
Throughput: 152255663.700000
Num Accesses - Session Mean & Stddev: 15225566.370000,47722.150095
Num Phases Traversed: 832553.000000
Num Phases Traversed - Session Mean & Stddev: 8325.530000,26.091936
Num Path Iterations: 832553.000000
Num Path Iterations - Session Mean & Stddev: 8325.530000,26.091936
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f71c4600000 of size 229376 bytes.
[1.233s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1509250662.000000
Throughput: 150925066.200000
Num Accesses - Session Mean & Stddev: 15092506.620000,3589.502784
Num Phases Traversed: 825278.000000
Num Phases Traversed - Session Mean & Stddev: 8252.780000,1.962549
Num Path Iterations: 825278.000000
Num Path Iterations - Session Mean & Stddev: 8252.780000,1.962549
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbc60800000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1533662325.000000
Throughput: 153366232.500000
Num Accesses - Session Mean & Stddev: 15336623.250000,242660.451152
Num Phases Traversed: 838625.000000
Num Phases Traversed - Session Mean & Stddev: 8386.250000,132.673839
Num Path Iterations: 838625.000000
Num Path Iterations - Session Mean & Stddev: 8386.250000,132.673839
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0a72600000 of size 229376 bytes.
[1.236s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1530439627.000000
Throughput: 153043962.700000
Num Accesses - Session Mean & Stddev: 15304396.270000,32639.994968
Num Phases Traversed: 836863.000000
Num Phases Traversed - Session Mean & Stddev: 8368.630000,17.845815
Num Path Iterations: 836863.000000
Num Path Iterations - Session Mean & Stddev: 8368.630000,17.845815
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f25f2000000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1518437729.000000
Throughput: 151843772.900000
Num Accesses - Session Mean & Stddev: 15184377.290000,193131.920204
Num Phases Traversed: 830301.000000
Num Phases Traversed - Session Mean & Stddev: 8303.010000,105.594270
Num Path Iterations: 830301.000000
Num Path Iterations - Session Mean & Stddev: 8303.010000,105.594270
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.01 0/1792,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.01 0/1792,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f179de00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1533779381.000000
Throughput: 153377938.100000
Num Accesses - Session Mean & Stddev: 15337793.810000,109405.054670
Num Phases Traversed: 838689.000000
Num Phases Traversed - Session Mean & Stddev: 8386.890000,59.816870
Num Path Iterations: 838689.000000
Num Path Iterations - Session Mean & Stddev: 8386.890000,59.816870
+ set +x

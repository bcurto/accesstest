++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcea1e00000 of size 524288 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1714500756.000000
Throughput: 171450075.600000
Num Accesses - Session Mean & Stddev: 17145007.560000,2539.694030
Num Phases Traversed: 414932.000000
Num Phases Traversed - Session Mean & Stddev: 4149.320000,0.614492
Num Path Iterations: 414932.000000
Num Path Iterations - Session Mean & Stddev: 4149.320000,0.614492
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fceea000000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1734583003.000000
Throughput: 173458300.300000
Num Accesses - Session Mean & Stddev: 17345830.030000,1443.003080
Num Phases Traversed: 419791.000000
Num Phases Traversed - Session Mean & Stddev: 4197.910000,0.349142
Num Path Iterations: 419791.000000
Num Path Iterations - Session Mean & Stddev: 4197.910000,0.349142
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0f14600000 of size 524288 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1730416939.000000
Throughput: 173041693.900000
Num Accesses - Session Mean & Stddev: 17304169.390000,1552.490360
Num Phases Traversed: 418783.000000
Num Phases Traversed - Session Mean & Stddev: 4187.830000,0.375633
Num Path Iterations: 418783.000000
Num Path Iterations - Session Mean & Stddev: 4187.830000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdda3e00000 of size 524288 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1751602697.000000
Throughput: 175160269.700000
Num Accesses - Session Mean & Stddev: 17516026.970000,2869.087996
Num Phases Traversed: 423909.000000
Num Phases Traversed - Session Mean & Stddev: 4239.090000,0.694190
Num Path Iterations: 423909.000000
Num Path Iterations - Session Mean & Stddev: 4239.090000,0.694190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb224600000 of size 524288 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1723328844.000000
Throughput: 172332884.400000
Num Accesses - Session Mean & Stddev: 17233288.440000,1927.945935
Num Phases Traversed: 417068.000000
Num Phases Traversed - Session Mean & Stddev: 4170.680000,0.466476
Num Path Iterations: 417068.000000
Num Path Iterations - Session Mean & Stddev: 4170.680000,0.466476
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff2d6600000 of size 524288 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1743316032.000000
Throughput: 174331603.200000
Num Accesses - Session Mean & Stddev: 17433160.320000,1931.486701
Num Phases Traversed: 421904.000000
Num Phases Traversed - Session Mean & Stddev: 4219.040000,0.467333
Num Path Iterations: 421904.000000
Num Path Iterations - Session Mean & Stddev: 4219.040000,0.467333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa731e00000 of size 524288 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1728755473.000000
Throughput: 172875547.300000
Num Accesses - Session Mean & Stddev: 17287554.730000,2722.451994
Num Phases Traversed: 418381.000000
Num Phases Traversed - Session Mean & Stddev: 4183.810000,0.658711
Num Path Iterations: 418381.000000
Num Path Iterations - Session Mean & Stddev: 4183.810000,0.658711
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f925ca00000 of size 524288 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1733008330.000000
Throughput: 173300833.000000
Num Accesses - Session Mean & Stddev: 17330083.300000,2833.442046
Num Phases Traversed: 419410.000000
Num Phases Traversed - Session Mean & Stddev: 4194.100000,0.685565
Num Path Iterations: 419410.000000
Num Path Iterations - Session Mean & Stddev: 4194.100000,0.685565
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa53d000000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1740799035.000000
Throughput: 174079903.500000
Num Accesses - Session Mean & Stddev: 17407990.350000,2702.299400
Num Phases Traversed: 421295.000000
Num Phases Traversed - Session Mean & Stddev: 4212.950000,0.653835
Num Path Iterations: 421295.000000
Num Path Iterations - Session Mean & Stddev: 4212.950000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f546e200000 of size 524288 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1746543905.000000
Throughput: 174654390.500000
Num Accesses - Session Mean & Stddev: 17465439.050000,2825.896249
Num Phases Traversed: 422685.000000
Num Phases Traversed - Session Mean & Stddev: 4226.850000,0.683740
Num Path Iterations: 422685.000000
Num Path Iterations - Session Mean & Stddev: 4226.850000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7809e00000 of size 524288 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1726490589.000000
Throughput: 172649058.900000
Num Accesses - Session Mean & Stddev: 17264905.890000,1943.389163
Num Phases Traversed: 417833.000000
Num Phases Traversed - Session Mean & Stddev: 4178.330000,0.470213
Num Path Iterations: 417833.000000
Num Path Iterations - Session Mean & Stddev: 4178.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f12f7000000 of size 524288 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1727556903.000000
Throughput: 172755690.300000
Num Accesses - Session Mean & Stddev: 17275569.030000,1556.885246
Num Phases Traversed: 418091.000000
Num Phases Traversed - Session Mean & Stddev: 4180.910000,0.376696
Num Path Iterations: 418091.000000
Num Path Iterations - Session Mean & Stddev: 4180.910000,0.376696
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3334800000 of size 524288 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1756558164.000000
Throughput: 175655816.400000
Num Accesses - Session Mean & Stddev: 17565581.640000,2844.272826
Num Phases Traversed: 425108.000000
Num Phases Traversed - Session Mean & Stddev: 4251.080000,0.688186
Num Path Iterations: 425108.000000
Num Path Iterations - Session Mean & Stddev: 4251.080000,0.688186
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9649e00000 of size 524288 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1724878719.000000
Throughput: 172487871.900000
Num Accesses - Session Mean & Stddev: 17248787.190000,2046.148082
Num Phases Traversed: 417443.000000
Num Phases Traversed - Session Mean & Stddev: 4174.430000,0.495076
Num Path Iterations: 417443.000000
Num Path Iterations - Session Mean & Stddev: 4174.430000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa14a000000 of size 524288 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1743580544.000000
Throughput: 174358054.400000
Num Accesses - Session Mean & Stddev: 17435805.440000,2539.694030
Num Phases Traversed: 421968.000000
Num Phases Traversed - Session Mean & Stddev: 4219.680000,0.614492
Num Path Iterations: 421968.000000
Num Path Iterations - Session Mean & Stddev: 4219.680000,0.614492
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f83a7e00000 of size 524288 bytes.
[1.293s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1729962309.000000
Throughput: 172996230.900000
Num Accesses - Session Mean & Stddev: 17299623.090000,2669.226945
Num Phases Traversed: 418673.000000
Num Phases Traversed - Session Mean & Stddev: 4186.730000,0.645833
Num Path Iterations: 418673.000000
Num Path Iterations - Session Mean & Stddev: 4186.730000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fedd7800000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1730669052.000000
Throughput: 173066905.200000
Num Accesses - Session Mean & Stddev: 17306690.520000,2211.830855
Num Phases Traversed: 418844.000000
Num Phases Traversed - Session Mean & Stddev: 4188.440000,0.535164
Num Path Iterations: 418844.000000
Num Path Iterations - Session Mean & Stddev: 4188.440000,0.535164
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f55a2400000 of size 524288 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1727751154.000000
Throughput: 172775115.400000
Num Accesses - Session Mean & Stddev: 17277511.540000,2006.102173
Num Phases Traversed: 418138.000000
Num Phases Traversed - Session Mean & Stddev: 4181.380000,0.485386
Num Path Iterations: 418138.000000
Num Path Iterations - Session Mean & Stddev: 4181.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2c36400000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1743729332.000000
Throughput: 174372933.200000
Num Accesses - Session Mean & Stddev: 17437293.320000,2858.650034
Num Phases Traversed: 422004.000000
Num Phases Traversed - Session Mean & Stddev: 4220.040000,0.691665
Num Path Iterations: 422004.000000
Num Path Iterations - Session Mean & Stddev: 4220.040000,0.691665
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0 0/4096,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0 0/4096,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f380ac00000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1753702261.000000
Throughput: 175370226.100000
Num Accesses - Session Mean & Stddev: 17537022.610000,2806.486558
Num Phases Traversed: 424417.000000
Num Phases Traversed - Session Mean & Stddev: 4244.170000,0.679043
Num Path Iterations: 424417.000000
Num Path Iterations - Session Mean & Stddev: 4244.170000,0.679043
+ set +x

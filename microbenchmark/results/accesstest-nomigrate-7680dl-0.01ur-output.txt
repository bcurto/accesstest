++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f25f9e00000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823397360.000000
Throughput: 82339736.000000
Num Accesses - Session Mean & Stddev: 8233973.600000,3197.056102
Num Phases Traversed: 212480.000000
Num Phases Traversed - Session Mean & Stddev: 2124.800000,0.824621
Num Path Iterations: 212480.000000
Num Path Iterations - Session Mean & Stddev: 2124.800000,0.824621
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f157a600000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823238403.000000
Throughput: 82323840.300000
Num Accesses - Session Mean & Stddev: 8232384.030000,2506.293839
Num Phases Traversed: 212439.000000
Num Phases Traversed - Session Mean & Stddev: 2124.390000,0.646452
Num Path Iterations: 212439.000000
Num Path Iterations - Session Mean & Stddev: 2124.390000,0.646452
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb0de400000 of size 491520 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823226772.000000
Throughput: 82322677.200000
Num Accesses - Session Mean & Stddev: 8232267.720000,2928.097673
Num Phases Traversed: 212436.000000
Num Phases Traversed - Session Mean & Stddev: 2124.360000,0.755248
Num Path Iterations: 212436.000000
Num Path Iterations - Session Mean & Stddev: 2124.360000,0.755248
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4b4ec00000 of size 491520 bytes.
[1.292s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823478777.000000
Throughput: 82347877.700000
Num Accesses - Session Mean & Stddev: 8234787.770000,3896.143886
Num Phases Traversed: 212501.000000
Num Phases Traversed - Session Mean & Stddev: 2125.010000,1.004938
Num Path Iterations: 212501.000000
Num Path Iterations - Session Mean & Stddev: 2125.010000,1.004938
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1c9de00000 of size 491520 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823428376.000000
Throughput: 82342837.600000
Num Accesses - Session Mean & Stddev: 8234283.760000,3607.068791
Num Phases Traversed: 212488.000000
Num Phases Traversed - Session Mean & Stddev: 2124.880000,0.930376
Num Path Iterations: 212488.000000
Num Path Iterations - Session Mean & Stddev: 2124.880000,0.930376
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5137c00000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823370221.000000
Throughput: 82337022.100000
Num Accesses - Session Mean & Stddev: 8233702.210000,2995.844977
Num Phases Traversed: 212473.000000
Num Phases Traversed - Session Mean & Stddev: 2124.730000,0.772722
Num Path Iterations: 212473.000000
Num Path Iterations - Session Mean & Stddev: 2124.730000,0.772722
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2e51a00000 of size 491520 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823432253.000000
Throughput: 82343225.300000
Num Accesses - Session Mean & Stddev: 8234322.530000,3632.606247
Num Phases Traversed: 212489.000000
Num Phases Traversed - Session Mean & Stddev: 2124.890000,0.936963
Num Path Iterations: 212489.000000
Num Path Iterations - Session Mean & Stddev: 2124.890000,0.936963
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4567400000 of size 491520 bytes.
[1.283s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823292681.000000
Throughput: 82329268.100000
Num Accesses - Session Mean & Stddev: 8232926.810000,2224.127819
Num Phases Traversed: 212453.000000
Num Phases Traversed - Session Mean & Stddev: 2124.530000,0.573672
Num Path Iterations: 212453.000000
Num Path Iterations - Session Mean & Stddev: 2124.530000,0.573672
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd021400000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823145355.000000
Throughput: 82314535.500000
Num Accesses - Session Mean & Stddev: 8231453.550000,3440.496198
Num Phases Traversed: 212415.000000
Num Phases Traversed - Session Mean & Stddev: 2124.150000,0.887412
Num Path Iterations: 212415.000000
Num Path Iterations - Session Mean & Stddev: 2124.150000,0.887412
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2c2e600000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823265542.000000
Throughput: 82326554.200000
Num Accesses - Session Mean & Stddev: 8232655.420000,2221.761185
Num Phases Traversed: 212446.000000
Num Phases Traversed - Session Mean & Stddev: 2124.460000,0.573062
Num Path Iterations: 212446.000000
Num Path Iterations - Session Mean & Stddev: 2124.460000,0.573062
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0f67200000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823304312.000000
Throughput: 82330431.200000
Num Accesses - Session Mean & Stddev: 8233043.120000,2409.985113
Num Phases Traversed: 212456.000000
Num Phases Traversed - Session Mean & Stddev: 2124.560000,0.621611
Num Path Iterations: 212456.000000
Num Path Iterations - Session Mean & Stddev: 2124.560000,0.621611
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0863400000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823215141.000000
Throughput: 82321514.100000
Num Accesses - Session Mean & Stddev: 8232151.410000,2798.699934
Num Phases Traversed: 212433.000000
Num Phases Traversed - Session Mean & Stddev: 2124.330000,0.721873
Num Path Iterations: 212433.000000
Num Path Iterations - Session Mean & Stddev: 2124.330000,0.721873
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f15e1000000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823238403.000000
Throughput: 82323840.300000
Num Accesses - Session Mean & Stddev: 8232384.030000,2623.500328
Num Phases Traversed: 212439.000000
Num Phases Traversed - Session Mean & Stddev: 2124.390000,0.676683
Num Path Iterations: 212439.000000
Num Path Iterations - Session Mean & Stddev: 2124.390000,0.676683
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff8ad400000 of size 491520 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823250034.000000
Throughput: 82325003.400000
Num Accesses - Session Mean & Stddev: 8232500.340000,2401.237407
Num Phases Traversed: 212442.000000
Num Phases Traversed - Session Mean & Stddev: 2124.420000,0.619355
Num Path Iterations: 212442.000000
Num Path Iterations - Session Mean & Stddev: 2124.420000,0.619355
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f854a400000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823377975.000000
Throughput: 82337797.500000
Num Accesses - Session Mean & Stddev: 8233779.750000,3214.638578
Num Phases Traversed: 212475.000000
Num Phases Traversed - Session Mean & Stddev: 2124.750000,0.829156
Num Path Iterations: 212475.000000
Num Path Iterations - Session Mean & Stddev: 2124.750000,0.829156
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f015f200000 of size 491520 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823412868.000000
Throughput: 82341286.800000
Num Accesses - Session Mean & Stddev: 8234128.680000,3276.925168
Num Phases Traversed: 212484.000000
Num Phases Traversed - Session Mean & Stddev: 2124.840000,0.845222
Num Path Iterations: 212484.000000
Num Path Iterations - Session Mean & Stddev: 2124.840000,0.845222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffbc9400000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823393483.000000
Throughput: 82339348.300000
Num Accesses - Session Mean & Stddev: 8233934.830000,3348.390527
Num Phases Traversed: 212479.000000
Num Phases Traversed - Session Mean & Stddev: 2124.790000,0.863655
Num Path Iterations: 212479.000000
Num Path Iterations - Session Mean & Stddev: 2124.790000,0.863655
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f672b600000 of size 491520 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 823436130.000000
Throughput: 82343613.000000
Num Accesses - Session Mean & Stddev: 8234361.300000,3818.402170
Num Phases Traversed: 212490.000000
Num Phases Traversed - Session Mean & Stddev: 2124.900000,0.984886
Num Path Iterations: 212490.000000
Num Path Iterations - Session Mean & Stddev: 2124.900000,0.984886
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3a8f600000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823215141.000000
Throughput: 82321514.100000
Num Accesses - Session Mean & Stddev: 8232151.410000,2904.129212
Num Phases Traversed: 212433.000000
Num Phases Traversed - Session Mean & Stddev: 2124.330000,0.749066
Num Path Iterations: 212433.000000
Num Path Iterations - Session Mean & Stddev: 2124.330000,0.749066
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.01 0/3840,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.01 0/3840,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2543a00000 of size 491520 bytes.
[1.279s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 823509793.000000
Throughput: 82350979.300000
Num Accesses - Session Mean & Stddev: 8235097.930000,3957.389709
Num Phases Traversed: 212509.000000
Num Phases Traversed - Session Mean & Stddev: 2125.090000,1.020735
Num Path Iterations: 212509.000000
Num Path Iterations - Session Mean & Stddev: 2125.090000,1.020735
+ set +x

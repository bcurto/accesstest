++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f95a7800000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 874098351.000000
Throughput: 87409835.100000
Num Accesses - Session Mean & Stddev: 8740983.510000,2450.009851
Num Phases Traversed: 178451.000000
Num Phases Traversed - Session Mean & Stddev: 1784.510000,0.499900
Num Path Iterations: 178451.000000
Num Path Iterations - Session Mean & Stddev: 1784.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efe3ac00000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 875034442.000000
Throughput: 87503444.200000
Num Accesses - Session Mean & Stddev: 8750344.420000,2609.983824
Num Phases Traversed: 178642.000000
Num Phases Traversed - Session Mean & Stddev: 1786.420000,0.532541
Num Path Iterations: 178642.000000
Num Path Iterations - Session Mean & Stddev: 1786.420000,0.532541
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faf0b800000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 873882707.000000
Throughput: 87388270.700000
Num Accesses - Session Mean & Stddev: 8738827.070000,3413.503400
Num Phases Traversed: 178407.000000
Num Phases Traversed - Session Mean & Stddev: 1784.070000,0.696491
Num Path Iterations: 178407.000000
Num Path Iterations - Session Mean & Stddev: 1784.070000,0.696491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fafe1600000 of size 622592 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 874294391.000000
Throughput: 87429439.100000
Num Accesses - Session Mean & Stddev: 8742943.910000,2410.475028
Num Phases Traversed: 178491.000000
Num Phases Traversed - Session Mean & Stddev: 1784.910000,0.491833
Num Path Iterations: 178491.000000
Num Path Iterations - Session Mean & Stddev: 1784.910000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3e3ea00000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 873794489.000000
Throughput: 87379448.900000
Num Accesses - Session Mean & Stddev: 8737944.890000,1533.472699
Num Phases Traversed: 178389.000000
Num Phases Traversed - Session Mean & Stddev: 1783.890000,0.312890
Num Path Iterations: 178389.000000
Num Path Iterations - Session Mean & Stddev: 1783.890000,0.312890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa62b600000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 873274983.000000
Throughput: 87327498.300000
Num Accesses - Session Mean & Stddev: 8732749.830000,1967.127332
Num Phases Traversed: 178283.000000
Num Phases Traversed - Session Mean & Stddev: 1782.830000,0.401373
Num Path Iterations: 178283.000000
Num Path Iterations - Session Mean & Stddev: 1782.830000,0.401373
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0145000000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 875637265.000000
Throughput: 87563726.500000
Num Accesses - Session Mean & Stddev: 8756372.650000,2337.628013
Num Phases Traversed: 178765.000000
Num Phases Traversed - Session Mean & Stddev: 1787.650000,0.476970
Num Path Iterations: 178765.000000
Num Path Iterations - Session Mean & Stddev: 1787.650000,0.476970
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0dbf400000 of size 622592 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 874603154.000000
Throughput: 87460315.400000
Num Accesses - Session Mean & Stddev: 8746031.540000,2539.077507
Num Phases Traversed: 178554.000000
Num Phases Traversed - Session Mean & Stddev: 1785.540000,0.518073
Num Path Iterations: 178554.000000
Num Path Iterations - Session Mean & Stddev: 1785.540000,0.518073
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f82ce600000 of size 622592 bytes.
[1.287s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 875558849.000000
Throughput: 87555884.900000
Num Accesses - Session Mean & Stddev: 8755588.490000,2450.009851
Num Phases Traversed: 178749.000000
Num Phases Traversed - Session Mean & Stddev: 1787.490000,0.499900
Num Path Iterations: 178749.000000
Num Path Iterations - Session Mean & Stddev: 1787.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2f80a00000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 873745479.000000
Throughput: 87374547.900000
Num Accesses - Session Mean & Stddev: 8737454.790000,3272.679218
Num Phases Traversed: 178379.000000
Num Phases Traversed - Session Mean & Stddev: 1783.790000,0.667757
Num Path Iterations: 178379.000000
Num Path Iterations - Session Mean & Stddev: 1783.790000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5634800000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 874211074.000000
Throughput: 87421107.400000
Num Accesses - Session Mean & Stddev: 8742110.740000,3147.342678
Num Phases Traversed: 178474.000000
Num Phases Traversed - Session Mean & Stddev: 1784.740000,0.642184
Num Path Iterations: 178474.000000
Num Path Iterations - Session Mean & Stddev: 1784.740000,0.642184
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9186400000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 873676865.000000
Throughput: 87367686.500000
Num Accesses - Session Mean & Stddev: 8736768.650000,2971.073353
Num Phases Traversed: 178365.000000
Num Phases Traversed - Session Mean & Stddev: 1783.650000,0.606218
Num Path Iterations: 178365.000000
Num Path Iterations - Session Mean & Stddev: 1783.650000,0.606218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f03c0800000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 874162064.000000
Throughput: 87416206.400000
Num Accesses - Session Mean & Stddev: 8741620.640000,2901.127069
Num Phases Traversed: 178464.000000
Num Phases Traversed - Session Mean & Stddev: 1784.640000,0.591946
Num Path Iterations: 178464.000000
Num Path Iterations - Session Mean & Stddev: 1784.640000,0.591946
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff085c00000 of size 622592 bytes.
[1.292s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 874544342.000000
Throughput: 87454434.200000
Num Accesses - Session Mean & Stddev: 8745443.420000,2700.446553
Num Phases Traversed: 178542.000000
Num Phases Traversed - Session Mean & Stddev: 1785.420000,0.550999
Num Path Iterations: 178542.000000
Num Path Iterations - Session Mean & Stddev: 1785.420000,0.550999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f27a9e00000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 874157163.000000
Throughput: 87415716.300000
Num Accesses - Session Mean & Stddev: 8741571.630000,2912.282907
Num Phases Traversed: 178463.000000
Num Phases Traversed - Session Mean & Stddev: 1784.630000,0.594222
Num Path Iterations: 178463.000000
Num Path Iterations - Session Mean & Stddev: 1784.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2c8f200000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 874897214.000000
Throughput: 87489721.400000
Num Accesses - Session Mean & Stddev: 8748972.140000,3325.464837
Num Phases Traversed: 178614.000000
Num Phases Traversed - Session Mean & Stddev: 1786.140000,0.678528
Num Path Iterations: 178614.000000
Num Path Iterations - Session Mean & Stddev: 1786.140000,0.678528
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6660c00000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 873172062.000000
Throughput: 87317206.200000
Num Accesses - Session Mean & Stddev: 8731720.620000,2753.297876
Num Phases Traversed: 178262.000000
Num Phases Traversed - Session Mean & Stddev: 1782.620000,0.561783
Num Path Iterations: 178262.000000
Num Path Iterations - Session Mean & Stddev: 1782.620000,0.561783
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdeaa400000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 874661966.000000
Throughput: 87466196.600000
Num Accesses - Session Mean & Stddev: 8746619.660000,2958.515757
Num Phases Traversed: 178566.000000
Num Phases Traversed - Session Mean & Stddev: 1785.660000,0.603656
Num Path Iterations: 178566.000000
Num Path Iterations - Session Mean & Stddev: 1785.660000,0.603656
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1cb4800000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 873029933.000000
Throughput: 87302993.300000
Num Accesses - Session Mean & Stddev: 8730299.330000,2304.512530
Num Phases Traversed: 178233.000000
Num Phases Traversed - Session Mean & Stddev: 1782.330000,0.470213
Num Path Iterations: 178233.000000
Num Path Iterations - Session Mean & Stddev: 1782.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.1 0/4864,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.1 0/4864,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc4fe200000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 875176571.000000
Throughput: 87517657.100000
Num Accesses - Session Mean & Stddev: 8751765.710000,3044.542857
Num Phases Traversed: 178671.000000
Num Phases Traversed - Session Mean & Stddev: 1786.710000,0.621208
Num Path Iterations: 178671.000000
Num Path Iterations - Session Mean & Stddev: 1786.710000,0.621208
+ set +x

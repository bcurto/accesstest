++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2fc0000000 of size 3145728 bytes.
[1.504s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803023838.000000
Throughput: 80302383.800000
Num Accesses - Session Mean & Stddev: 8030238.380000,15019.981158
Num Phases Traversed: 32726.000000
Num Phases Traversed - Session Mean & Stddev: 327.260000,0.610246
Num Path Iterations: 32726.000000
Num Path Iterations - Session Mean & Stddev: 327.260000,0.610246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7d40000000 of size 3145728 bytes.
[1.536s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804106810.000000
Throughput: 80410681.000000
Num Accesses - Session Mean & Stddev: 8041068.100000,14139.092042
Num Phases Traversed: 32770.000000
Num Phases Traversed - Session Mean & Stddev: 327.700000,0.574456
Num Path Iterations: 32770.000000
Num Path Iterations - Session Mean & Stddev: 327.700000,0.574456
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0300000000 of size 3145728 bytes.
[1.504s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803836067.000000
Throughput: 80383606.700000
Num Accesses - Session Mean & Stddev: 8038360.670000,13964.488735
Num Phases Traversed: 32759.000000
Num Phases Traversed - Session Mean & Stddev: 327.590000,0.567362
Num Path Iterations: 32759.000000
Num Path Iterations - Session Mean & Stddev: 327.590000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc880000000 of size 3145728 bytes.
[1.536s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804229875.000000
Throughput: 80422987.500000
Num Accesses - Session Mean & Stddev: 8042298.750000,14085.433055
Num Phases Traversed: 32775.000000
Num Phases Traversed - Session Mean & Stddev: 327.750000,0.572276
Num Path Iterations: 32775.000000
Num Path Iterations - Session Mean & Stddev: 327.750000,0.572276
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f54c0000000 of size 3145728 bytes.
[1.532s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804402166.000000
Throughput: 80440216.600000
Num Accesses - Session Mean & Stddev: 8044021.660000,13199.545961
Num Phases Traversed: 32782.000000
Num Phases Traversed - Session Mean & Stddev: 327.820000,0.536284
Num Path Iterations: 32782.000000
Num Path Iterations - Session Mean & Stddev: 327.820000,0.536284
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc940000000 of size 3145728 bytes.
[1.535s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804402166.000000
Throughput: 80440216.600000
Num Accesses - Session Mean & Stddev: 8044021.660000,13199.545961
Num Phases Traversed: 32782.000000
Num Phases Traversed - Session Mean & Stddev: 327.820000,0.536284
Num Path Iterations: 32782.000000
Num Path Iterations - Session Mean & Stddev: 327.820000,0.536284
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9940000000 of size 3145728 bytes.
[1.503s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804082197.000000
Throughput: 80408219.700000
Num Accesses - Session Mean & Stddev: 8040821.970000,14222.395820
Num Phases Traversed: 32769.000000
Num Phases Traversed - Session Mean & Stddev: 327.690000,0.577841
Num Path Iterations: 32769.000000
Num Path Iterations - Session Mean & Stddev: 327.690000,0.577841
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0e40000000 of size 3145728 bytes.
[1.536s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804156036.000000
Throughput: 80415603.600000
Num Accesses - Session Mean & Stddev: 8041560.360000,14385.451021
Num Phases Traversed: 32772.000000
Num Phases Traversed - Session Mean & Stddev: 327.720000,0.584466
Num Path Iterations: 32772.000000
Num Path Iterations - Session Mean & Stddev: 327.720000,0.584466
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4a40000000 of size 3145728 bytes.
[1.504s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 802900773.000000
Throughput: 80290077.300000
Num Accesses - Session Mean & Stddev: 8029007.730000,15681.011646
Num Phases Traversed: 32721.000000
Num Phases Traversed - Session Mean & Stddev: 327.210000,0.637103
Num Path Iterations: 32721.000000
Num Path Iterations - Session Mean & Stddev: 327.210000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1140000000 of size 3145728 bytes.
[1.536s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805165169.000000
Throughput: 80516516.900000
Num Accesses - Session Mean & Stddev: 8051651.690000,8277.436431
Num Phases Traversed: 32813.000000
Num Phases Traversed - Session Mean & Stddev: 328.130000,0.336303
Num Path Iterations: 32813.000000
Num Path Iterations - Session Mean & Stddev: 328.130000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa980000000 of size 3145728 bytes.
[1.536s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804402166.000000
Throughput: 80440216.600000
Num Accesses - Session Mean & Stddev: 8044021.660000,13199.545961
Num Phases Traversed: 32782.000000
Num Phases Traversed - Session Mean & Stddev: 327.820000,0.536284
Num Path Iterations: 32782.000000
Num Path Iterations - Session Mean & Stddev: 327.820000,0.536284
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9a40000000 of size 3145728 bytes.
[1.536s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804402166.000000
Throughput: 80440216.600000
Num Accesses - Session Mean & Stddev: 8044021.660000,12732.321791
Num Phases Traversed: 32782.000000
Num Phases Traversed - Session Mean & Stddev: 327.820000,0.517301
Num Path Iterations: 32782.000000
Num Path Iterations - Session Mean & Stddev: 327.820000,0.517301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcfc0000000 of size 3145728 bytes.
[1.504s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803983745.000000
Throughput: 80398374.500000
Num Accesses - Session Mean & Stddev: 8039837.450000,14509.149518
Num Phases Traversed: 32765.000000
Num Phases Traversed - Session Mean & Stddev: 327.650000,0.589491
Num Path Iterations: 32765.000000
Num Path Iterations - Session Mean & Stddev: 327.650000,0.589491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8e40000000 of size 3145728 bytes.
[1.536s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804820587.000000
Throughput: 80482058.700000
Num Accesses - Session Mean & Stddev: 8048205.870000,8870.929489
Num Phases Traversed: 32799.000000
Num Phases Traversed - Session Mean & Stddev: 327.990000,0.360416
Num Path Iterations: 32799.000000
Num Path Iterations - Session Mean & Stddev: 327.990000,0.360416
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc3c0000000 of size 3145728 bytes.
[1.504s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 802482352.000000
Throughput: 80248235.200000
Num Accesses - Session Mean & Stddev: 8024823.520000,17023.942243
Num Phases Traversed: 32704.000000
Num Phases Traversed - Session Mean & Stddev: 327.040000,0.691665
Num Path Iterations: 32704.000000
Num Path Iterations - Session Mean & Stddev: 327.040000,0.691665
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9680000000 of size 3145728 bytes.
[1.548s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804328327.000000
Throughput: 80432832.700000
Num Accesses - Session Mean & Stddev: 8043283.270000,13613.013970
Num Phases Traversed: 32779.000000
Num Phases Traversed - Session Mean & Stddev: 327.790000,0.553082
Num Path Iterations: 32779.000000
Num Path Iterations - Session Mean & Stddev: 327.790000,0.553082
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fafc0000000 of size 3145728 bytes.
[1.536s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805165169.000000
Throughput: 80516516.900000
Num Accesses - Session Mean & Stddev: 8051651.690000,8277.436431
Num Phases Traversed: 32813.000000
Num Phases Traversed - Session Mean & Stddev: 328.130000,0.336303
Num Path Iterations: 32813.000000
Num Path Iterations - Session Mean & Stddev: 328.130000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efbc0000000 of size 3145728 bytes.
[1.547s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 805066717.000000
Throughput: 80506671.700000
Num Accesses - Session Mean & Stddev: 8050667.170000,7043.791669
Num Phases Traversed: 32809.000000
Num Phases Traversed - Session Mean & Stddev: 328.090000,0.286182
Num Path Iterations: 32809.000000
Num Path Iterations - Session Mean & Stddev: 328.090000,0.286182
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9700000000 of size 3145728 bytes.
[1.504s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804254488.000000
Throughput: 80425448.800000
Num Accesses - Session Mean & Stddev: 8042544.880000,14402.285961
Num Phases Traversed: 32776.000000
Num Phases Traversed - Session Mean & Stddev: 327.760000,0.585150
Num Path Iterations: 32776.000000
Num Path Iterations - Session Mean & Stddev: 327.760000,0.585150
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efc40000000 of size 3145728 bytes.
[1.540s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804426779.000000
Throughput: 80442677.900000
Num Accesses - Session Mean & Stddev: 8044267.790000,13049.533136
Num Phases Traversed: 32783.000000
Num Phases Traversed - Session Mean & Stddev: 327.830000,0.530189
Num Path Iterations: 32783.000000
Num Path Iterations - Session Mean & Stddev: 327.830000,0.530189
+ set +x

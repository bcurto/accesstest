++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6e00000000 of size 6291456 bytes.
[1.672s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830605554.000000
Throughput: 83060555.400000
Num Accesses - Session Mean & Stddev: 8306055.540000,53892.829656
Num Phases Traversed: 16986.000000
Num Phases Traversed - Session Mean & Stddev: 169.860000,1.095628
Num Path Iterations: 16986.000000
Num Path Iterations - Session Mean & Stddev: 169.860000,1.095628
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa1c0000000 of size 6291456 bytes.
[1.671s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830457987.000000
Throughput: 83045798.700000
Num Accesses - Session Mean & Stddev: 8304579.870000,53908.540870
Num Phases Traversed: 16983.000000
Num Phases Traversed - Session Mean & Stddev: 169.830000,1.095947
Num Path Iterations: 16983.000000
Num Path Iterations - Session Mean & Stddev: 169.830000,1.095947
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9000000000 of size 6291456 bytes.
[1.684s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830556365.000000
Throughput: 83055636.500000
Num Accesses - Session Mean & Stddev: 8305563.650000,54051.974507
Num Phases Traversed: 16985.000000
Num Phases Traversed - Session Mean & Stddev: 169.850000,1.098863
Num Path Iterations: 16985.000000
Num Path Iterations - Session Mean & Stddev: 169.850000,1.098863
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe700000000 of size 6291456 bytes.
[1.671s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830359609.000000
Throughput: 83035960.900000
Num Accesses - Session Mean & Stddev: 8303596.090000,53294.641463
Num Phases Traversed: 16981.000000
Num Phases Traversed - Session Mean & Stddev: 169.810000,1.083467
Num Path Iterations: 16981.000000
Num Path Iterations - Session Mean & Stddev: 169.810000,1.083467
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0d00000000 of size 6291456 bytes.
[1.671s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830605554.000000
Throughput: 83060555.400000
Num Accesses - Session Mean & Stddev: 8306055.540000,52066.039513
Num Phases Traversed: 16986.000000
Num Phases Traversed - Session Mean & Stddev: 169.860000,1.058489
Num Path Iterations: 16986.000000
Num Path Iterations - Session Mean & Stddev: 169.860000,1.058489
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5ec0000000 of size 6291456 bytes.
[1.672s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 830507176.000000
Throughput: 83050717.600000
Num Accesses - Session Mean & Stddev: 8305071.760000,50986.006075
Num Phases Traversed: 16984.000000
Num Phases Traversed - Session Mean & Stddev: 169.840000,1.036533
Num Path Iterations: 16984.000000
Num Path Iterations - Session Mean & Stddev: 169.840000,1.036533
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc700000000 of size 6291456 bytes.
[1.671s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830605554.000000
Throughput: 83060555.400000
Num Accesses - Session Mean & Stddev: 8306055.540000,50172.780000
Num Phases Traversed: 16986.000000
Num Phases Traversed - Session Mean & Stddev: 169.860000,1.020000
Num Path Iterations: 16986.000000
Num Path Iterations - Session Mean & Stddev: 169.860000,1.020000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3780000000 of size 6291456 bytes.
[1.672s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830457987.000000
Throughput: 83045798.700000
Num Accesses - Session Mean & Stddev: 8304579.870000,50669.445406
Num Phases Traversed: 16983.000000
Num Phases Traversed - Session Mean & Stddev: 169.830000,1.030097
Num Path Iterations: 16983.000000
Num Path Iterations - Session Mean & Stddev: 169.830000,1.030097
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f34c0000000 of size 6291456 bytes.
[1.672s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 828490427.000000
Throughput: 82849042.700000
Num Accesses - Session Mean & Stddev: 8284904.270000,56622.927654
Num Phases Traversed: 16943.000000
Num Phases Traversed - Session Mean & Stddev: 169.430000,1.151130
Num Path Iterations: 16943.000000
Num Path Iterations - Session Mean & Stddev: 169.430000,1.151130
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb5c0000000 of size 6291456 bytes.
[1.671s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830507176.000000
Throughput: 83050717.600000
Num Accesses - Session Mean & Stddev: 8305071.760000,50027.897283
Num Phases Traversed: 16984.000000
Num Phases Traversed - Session Mean & Stddev: 169.840000,1.017055
Num Path Iterations: 16984.000000
Num Path Iterations - Session Mean & Stddev: 169.840000,1.017055
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f45c0000000 of size 6291456 bytes.
[1.671s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830605554.000000
Throughput: 83060555.400000
Num Accesses - Session Mean & Stddev: 8306055.540000,49198.836816
Num Phases Traversed: 16986.000000
Num Phases Traversed - Session Mean & Stddev: 169.860000,1.000200
Num Path Iterations: 16986.000000
Num Path Iterations - Session Mean & Stddev: 169.860000,1.000200
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0ac0000000 of size 6291456 bytes.
[1.672s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830113664.000000
Throughput: 83011366.400000
Num Accesses - Session Mean & Stddev: 8301136.640000,54827.534256
Num Phases Traversed: 16976.000000
Num Phases Traversed - Session Mean & Stddev: 169.760000,1.114630
Num Path Iterations: 16976.000000
Num Path Iterations - Session Mean & Stddev: 169.760000,1.114630
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8140000000 of size 6291456 bytes.
[1.671s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830408798.000000
Throughput: 83040879.800000
Num Accesses - Session Mean & Stddev: 8304087.980000,53604.726932
Num Phases Traversed: 16982.000000
Num Phases Traversed - Session Mean & Stddev: 169.820000,1.089771
Num Path Iterations: 16982.000000
Num Path Iterations - Session Mean & Stddev: 169.820000,1.089771
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb7c0000000 of size 6291456 bytes.
[1.668s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830703932.000000
Throughput: 83070393.200000
Num Accesses - Session Mean & Stddev: 8307039.320000,50298.007446
Num Phases Traversed: 16988.000000
Num Phases Traversed - Session Mean & Stddev: 169.880000,1.022546
Num Path Iterations: 16988.000000
Num Path Iterations - Session Mean & Stddev: 169.880000,1.022546
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2540000000 of size 6291456 bytes.
[1.671s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830408798.000000
Throughput: 83040879.800000
Num Accesses - Session Mean & Stddev: 8304087.980000,56247.788187
Num Phases Traversed: 16982.000000
Num Phases Traversed - Session Mean & Stddev: 169.820000,1.143503
Num Path Iterations: 16982.000000
Num Path Iterations - Session Mean & Stddev: 169.820000,1.143503
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fedc0000000 of size 6291456 bytes.
[1.671s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830654743.000000
Throughput: 83065474.300000
Num Accesses - Session Mean & Stddev: 8306547.430000,55063.125412
Num Phases Traversed: 16987.000000
Num Phases Traversed - Session Mean & Stddev: 169.870000,1.119419
Num Path Iterations: 16987.000000
Num Path Iterations - Session Mean & Stddev: 169.870000,1.119419
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f39c0000000 of size 6291456 bytes.
[1.670s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830359609.000000
Throughput: 83035960.900000
Num Accesses - Session Mean & Stddev: 8303596.090000,52838.694668
Num Phases Traversed: 16981.000000
Num Phases Traversed - Session Mean & Stddev: 169.810000,1.074197
Num Path Iterations: 16981.000000
Num Path Iterations - Session Mean & Stddev: 169.810000,1.074197
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6500000000 of size 6291456 bytes.
[1.672s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830408798.000000
Throughput: 83040879.800000
Num Accesses - Session Mean & Stddev: 8304087.980000,49863.187966
Num Phases Traversed: 16982.000000
Num Phases Traversed - Session Mean & Stddev: 169.820000,1.013706
Num Path Iterations: 16982.000000
Num Path Iterations - Session Mean & Stddev: 169.820000,1.013706
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb580000000 of size 6291456 bytes.
[1.672s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830507176.000000
Throughput: 83050717.600000
Num Accesses - Session Mean & Stddev: 8305071.760000,54650.727816
Num Phases Traversed: 16984.000000
Num Phases Traversed - Session Mean & Stddev: 169.840000,1.111036
Num Path Iterations: 16984.000000
Num Path Iterations - Session Mean & Stddev: 169.840000,1.111036
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0 0/49152,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0 0/49152,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f15c0000000 of size 6291456 bytes.
[1.668s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830605554.000000
Throughput: 83060555.400000
Num Accesses - Session Mean & Stddev: 8306055.540000,50172.780000
Num Phases Traversed: 16986.000000
Num Phases Traversed - Session Mean & Stddev: 169.860000,1.020000
Num Path Iterations: 16986.000000
Num Path Iterations - Session Mean & Stddev: 169.860000,1.020000
+ set +x

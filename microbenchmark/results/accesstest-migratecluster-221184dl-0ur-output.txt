++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa200000000 of size 14155776 bytes.
[2.571s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 326023763.000000
Throughput: 32602376.300000
Num Accesses - Session Mean & Stddev: 3260237.630000,57388.627140
Num Phases Traversed: 3047.000000
Num Phases Traversed - Session Mean & Stddev: 30.470000,0.518748
Num Path Iterations: 3047.000000
Num Path Iterations - Session Mean & Stddev: 30.470000,0.518748
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1580000000 of size 14155776 bytes.
[2.498s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 327572569.000000
Throughput: 32757256.900000
Num Accesses - Session Mean & Stddev: 3275725.690000,64307.715626
Num Phases Traversed: 3061.000000
Num Phases Traversed - Session Mean & Stddev: 30.610000,0.581292
Num Path Iterations: 3061.000000
Num Path Iterations - Session Mean & Stddev: 30.610000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0740000000 of size 14155776 bytes.
[2.620s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 327572569.000000
Throughput: 32757256.900000
Num Accesses - Session Mean & Stddev: 3275725.690000,64307.715626
Num Phases Traversed: 3061.000000
Num Phases Traversed - Session Mean & Stddev: 30.610000,0.581292
Num Path Iterations: 3061.000000
Num Path Iterations - Session Mean & Stddev: 30.610000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4880000000 of size 14155776 bytes.
[2.556s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 326245021.000000
Throughput: 32624502.100000
Num Accesses - Session Mean & Stddev: 3262450.210000,57473.868371
Num Phases Traversed: 3049.000000
Num Phases Traversed - Session Mean & Stddev: 30.490000,0.519519
Num Path Iterations: 3049.000000
Num Path Iterations - Session Mean & Stddev: 30.490000,0.519519
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fad40000000 of size 14155776 bytes.
[2.595s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 327351311.000000
Throughput: 32735131.100000
Num Accesses - Session Mean & Stddev: 3273513.110000,62766.725889
Num Phases Traversed: 3059.000000
Num Phases Traversed - Session Mean & Stddev: 30.590000,0.567362
Num Path Iterations: 3059.000000
Num Path Iterations - Session Mean & Stddev: 30.590000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd1c0000000 of size 14155776 bytes.
[2.591s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 328236343.000000
Throughput: 32823634.300000
Num Accesses - Session Mean & Stddev: 3282363.430000,68294.929510
Num Phases Traversed: 3067.000000
Num Phases Traversed - Session Mean & Stddev: 30.670000,0.617333
Num Path Iterations: 3067.000000
Num Path Iterations - Session Mean & Stddev: 30.670000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efc80000000 of size 14155776 bytes.
[2.580s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 327793827.000000
Throughput: 32779382.700000
Num Accesses - Session Mean & Stddev: 3277938.270000,65738.205625
Num Phases Traversed: 3063.000000
Num Phases Traversed - Session Mean & Stddev: 30.630000,0.594222
Num Path Iterations: 3063.000000
Num Path Iterations - Session Mean & Stddev: 30.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc640000000 of size 14155776 bytes.
[2.608s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 327793827.000000
Throughput: 32779382.700000
Num Accesses - Session Mean & Stddev: 3277938.270000,65738.205625
Num Phases Traversed: 3063.000000
Num Phases Traversed - Session Mean & Stddev: 30.630000,0.594222
Num Path Iterations: 3063.000000
Num Path Iterations - Session Mean & Stddev: 30.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2d40000000 of size 14155776 bytes.
[2.583s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 330006407.000000
Throughput: 33000640.700000
Num Accesses - Session Mean & Stddev: 3300064.070000,75121.897261
Num Phases Traversed: 3083.000000
Num Phases Traversed - Session Mean & Stddev: 30.830000,0.679043
Num Path Iterations: 3083.000000
Num Path Iterations - Session Mean & Stddev: 30.830000,0.679043
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcf40000000 of size 14155776 bytes.
[2.582s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 327572569.000000
Throughput: 32757256.900000
Num Accesses - Session Mean & Stddev: 3275725.690000,64307.715626
Num Phases Traversed: 3061.000000
Num Phases Traversed - Session Mean & Stddev: 30.610000,0.581292
Num Path Iterations: 3061.000000
Num Path Iterations - Session Mean & Stddev: 30.610000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb680000000 of size 14155776 bytes.
[2.599s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 327793827.000000
Throughput: 32779382.700000
Num Accesses - Session Mean & Stddev: 3277938.270000,65738.205625
Num Phases Traversed: 3063.000000
Num Phases Traversed - Session Mean & Stddev: 30.630000,0.594222
Num Path Iterations: 3063.000000
Num Path Iterations - Session Mean & Stddev: 30.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5a80000000 of size 14155776 bytes.
[2.593s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 327793827.000000
Throughput: 32779382.700000
Num Accesses - Session Mean & Stddev: 3277938.270000,65738.205625
Num Phases Traversed: 3063.000000
Num Phases Traversed - Session Mean & Stddev: 30.630000,0.594222
Num Path Iterations: 3063.000000
Num Path Iterations - Session Mean & Stddev: 30.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5bc0000000 of size 14155776 bytes.
[2.538s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 328015085.000000
Throughput: 32801508.500000
Num Accesses - Session Mean & Stddev: 3280150.850000,67065.267077
Num Phases Traversed: 3065.000000
Num Phases Traversed - Session Mean & Stddev: 30.650000,0.606218
Num Path Iterations: 3065.000000
Num Path Iterations - Session Mean & Stddev: 30.650000,0.606218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1980000000 of size 14155776 bytes.
[2.551s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 329563891.000000
Throughput: 32956389.100000
Num Accesses - Session Mean & Stddev: 3295638.910000,73873.337939
Num Phases Traversed: 3079.000000
Num Phases Traversed - Session Mean & Stddev: 30.790000,0.667757
Num Path Iterations: 3079.000000
Num Path Iterations - Session Mean & Stddev: 30.790000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9280000000 of size 14155776 bytes.
[2.621s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 328236343.000000
Throughput: 32823634.300000
Num Accesses - Session Mean & Stddev: 3282363.430000,68294.929510
Num Phases Traversed: 3067.000000
Num Phases Traversed - Session Mean & Stddev: 30.670000,0.617333
Num Path Iterations: 3067.000000
Num Path Iterations - Session Mean & Stddev: 30.670000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8040000000 of size 14155776 bytes.
[2.551s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 327793827.000000
Throughput: 32779382.700000
Num Accesses - Session Mean & Stddev: 3277938.270000,65738.205625
Num Phases Traversed: 3063.000000
Num Phases Traversed - Session Mean & Stddev: 30.630000,0.594222
Num Path Iterations: 3063.000000
Num Path Iterations - Session Mean & Stddev: 30.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2240000000 of size 14155776 bytes.
[2.591s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 327572569.000000
Throughput: 32757256.900000
Num Accesses - Session Mean & Stddev: 3275725.690000,64307.715626
Num Phases Traversed: 3061.000000
Num Phases Traversed - Session Mean & Stddev: 30.610000,0.581292
Num Path Iterations: 3061.000000
Num Path Iterations - Session Mean & Stddev: 30.610000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fadc0000000 of size 14155776 bytes.
[2.621s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 327793827.000000
Throughput: 32779382.700000
Num Accesses - Session Mean & Stddev: 3277938.270000,65738.205625
Num Phases Traversed: 3063.000000
Num Phases Traversed - Session Mean & Stddev: 30.630000,0.594222
Num Path Iterations: 3063.000000
Num Path Iterations - Session Mean & Stddev: 30.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdac0000000 of size 14155776 bytes.
[2.544s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 324696215.000000
Throughput: 32469621.500000
Num Accesses - Session Mean & Stddev: 3246962.150000,67065.267077
Num Phases Traversed: 3035.000000
Num Phases Traversed - Session Mean & Stddev: 30.350000,0.606218
Num Path Iterations: 3035.000000
Num Path Iterations - Session Mean & Stddev: 30.350000,0.606218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe440000000 of size 14155776 bytes.
[2.575s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 328015085.000000
Throughput: 32801508.500000
Num Accesses - Session Mean & Stddev: 3280150.850000,67065.267077
Num Phases Traversed: 3065.000000
Num Phases Traversed - Session Mean & Stddev: 30.650000,0.606218
Num Path Iterations: 3065.000000
Num Path Iterations - Session Mean & Stddev: 30.650000,0.606218
+ set +x

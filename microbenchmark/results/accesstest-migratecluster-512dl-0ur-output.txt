++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb488c00000 of size 32768 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2742935422.000000
Throughput: 274293542.200000
Num Accesses - Session Mean & Stddev: 27429354.220000,146.030448
Num Phases Traversed: 9361654.000000
Num Phases Traversed - Session Mean & Stddev: 93616.540000,0.498397
Num Path Iterations: 9361654.000000
Num Path Iterations - Session Mean & Stddev: 93616.540000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f62bb000000 of size 32768 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2775752008.000000
Throughput: 277575200.800000
Num Accesses - Session Mean & Stddev: 27757520.080000,145.441375
Num Phases Traversed: 9473656.000000
Num Phases Traversed - Session Mean & Stddev: 94736.560000,0.496387
Num Path Iterations: 9473656.000000
Num Path Iterations - Session Mean & Stddev: 94736.560000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0058400000 of size 32768 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2761188736.000000
Throughput: 276118873.600000
Num Accesses - Session Mean & Stddev: 27611887.360000,146.382753
Num Phases Traversed: 9423952.000000
Num Phases Traversed - Session Mean & Stddev: 94239.520000,0.499600
Num Path Iterations: 9423952.000000
Num Path Iterations - Session Mean & Stddev: 94239.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5747e00000 of size 32768 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2764201362.000000
Throughput: 276420136.200000
Num Accesses - Session Mean & Stddev: 27642013.620000,138.796670
Num Phases Traversed: 9434234.000000
Num Phases Traversed - Session Mean & Stddev: 94342.340000,0.473709
Num Path Iterations: 9434234.000000
Num Path Iterations - Session Mean & Stddev: 94342.340000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff869800000 of size 32768 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2747929314.000000
Throughput: 274792931.400000
Num Accesses - Session Mean & Stddev: 27479293.140000,82.665473
Num Phases Traversed: 9378698.000000
Num Phases Traversed - Session Mean & Stddev: 93786.980000,0.282135
Num Path Iterations: 9378698.000000
Num Path Iterations - Session Mean & Stddev: 93786.980000,0.282135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5105200000 of size 32768 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2752726017.000000
Throughput: 275272601.700000
Num Accesses - Session Mean & Stddev: 27527260.170000,135.510520
Num Phases Traversed: 9395069.000000
Num Phases Traversed - Session Mean & Stddev: 93950.690000,0.462493
Num Path Iterations: 9395069.000000
Num Path Iterations - Session Mean & Stddev: 93950.690000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe889200000 of size 32768 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2759633492.000000
Throughput: 275963349.200000
Num Accesses - Session Mean & Stddev: 27596334.920000,145.441375
Num Phases Traversed: 9418644.000000
Num Phases Traversed - Session Mean & Stddev: 94186.440000,0.496387
Num Path Iterations: 9418644.000000
Num Path Iterations - Session Mean & Stddev: 94186.440000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd2f9c00000 of size 32768 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2726566684.000000
Throughput: 272656668.400000
Num Accesses - Session Mean & Stddev: 27265666.840000,95.213730
Num Phases Traversed: 9305788.000000
Num Phases Traversed - Session Mean & Stddev: 93057.880000,0.324962
Num Path Iterations: 9305788.000000
Num Path Iterations - Session Mean & Stddev: 93057.880000,0.324962
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe58f400000 of size 32768 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2724293004.000000
Throughput: 272429300.400000
Num Accesses - Session Mean & Stddev: 27242930.040000,131.556674
Num Phases Traversed: 9298028.000000
Num Phases Traversed - Session Mean & Stddev: 92980.280000,0.448999
Num Path Iterations: 9298028.000000
Num Path Iterations - Session Mean & Stddev: 92980.280000,0.448999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1fd9400000 of size 32768 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2757746279.000000
Throughput: 275774627.900000
Num Accesses - Session Mean & Stddev: 27577462.790000,64.924463
Num Phases Traversed: 9412203.000000
Num Phases Traversed - Session Mean & Stddev: 94122.030000,0.221585
Num Path Iterations: 9412203.000000
Num Path Iterations - Session Mean & Stddev: 94122.030000,0.221585
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2617600000 of size 32768 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2729331432.000000
Throughput: 272933143.200000
Num Accesses - Session Mean & Stddev: 27293314.320000,125.135357
Num Phases Traversed: 9315224.000000
Num Phases Traversed - Session Mean & Stddev: 93152.240000,0.427083
Num Path Iterations: 9315224.000000
Num Path Iterations - Session Mean & Stddev: 93152.240000,0.427083
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8461200000 of size 32768 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2755881920.000000
Throughput: 275588192.000000
Num Accesses - Session Mean & Stddev: 27558819.200000,143.540099
Num Phases Traversed: 9405840.000000
Num Phases Traversed - Session Mean & Stddev: 94058.400000,0.489898
Num Path Iterations: 9405840.000000
Num Path Iterations - Session Mean & Stddev: 94058.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe05aa00000 of size 32768 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2758303858.000000
Throughput: 275830385.800000
Num Accesses - Session Mean & Stddev: 27583038.580000,69.583645
Num Phases Traversed: 9414106.000000
Num Phases Traversed - Session Mean & Stddev: 94141.060000,0.237487
Num Path Iterations: 9414106.000000
Num Path Iterations - Session Mean & Stddev: 94141.060000,0.237487
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faa1fe00000 of size 32768 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2767605436.000000
Throughput: 276760543.600000
Num Accesses - Session Mean & Stddev: 27676054.360000,146.382753
Num Phases Traversed: 9445852.000000
Num Phases Traversed - Session Mean & Stddev: 94458.520000,0.499600
Num Path Iterations: 9445852.000000
Num Path Iterations - Session Mean & Stddev: 94458.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fba11e00000 of size 32768 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2773426760.000000
Throughput: 277342676.000000
Num Accesses - Session Mean & Stddev: 27734267.600000,117.200000
Num Phases Traversed: 9465720.000000
Num Phases Traversed - Session Mean & Stddev: 94657.200000,0.400000
Num Path Iterations: 9465720.000000
Num Path Iterations - Session Mean & Stddev: 94657.200000,0.400000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2641600000 of size 32768 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2775168059.000000
Throughput: 277516805.900000
Num Accesses - Session Mean & Stddev: 27751680.590000,141.461662
Num Phases Traversed: 9471663.000000
Num Phases Traversed - Session Mean & Stddev: 94716.630000,0.482804
Num Path Iterations: 9471663.000000
Num Path Iterations - Session Mean & Stddev: 94716.630000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6dc0a00000 of size 32768 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2764959353.000000
Throughput: 276495935.300000
Num Accesses - Session Mean & Stddev: 27649593.530000,119.341313
Num Phases Traversed: 9436821.000000
Num Phases Traversed - Session Mean & Stddev: 94368.210000,0.407308
Num Path Iterations: 9436821.000000
Num Path Iterations - Session Mean & Stddev: 94368.210000,0.407308
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4d7b600000 of size 32768 bytes.
[1.240s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2782868099.000000
Throughput: 278286809.900000
Num Accesses - Session Mean & Stddev: 27828680.990000,145.057195
Num Phases Traversed: 9497943.000000
Num Phases Traversed - Session Mean & Stddev: 94979.430000,0.495076
Num Path Iterations: 9497943.000000
Num Path Iterations - Session Mean & Stddev: 94979.430000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fba65800000 of size 32768 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2758581329.000000
Throughput: 275858132.900000
Num Accesses - Session Mean & Stddev: 27585813.290000,146.236062
Num Phases Traversed: 9415053.000000
Num Phases Traversed - Session Mean & Stddev: 94150.530000,0.499099
Num Path Iterations: 9415053.000000
Num Path Iterations - Session Mean & Stddev: 94150.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/256,0 0/256,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/256,0 0/256,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    256 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fba6d800000 of size 32768 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2738512587.000000
Throughput: 273851258.700000
Num Accesses - Session Mean & Stddev: 27385125.870000,144.107158
Num Phases Traversed: 9346559.000000
Num Phases Traversed - Session Mean & Stddev: 93465.590000,0.491833
Num Path Iterations: 9346559.000000
Num Path Iterations - Session Mean & Stddev: 93465.590000,0.491833
+ set +x

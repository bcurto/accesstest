++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7ba8400000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848152457.000000
Throughput: 84815245.700000
Num Accesses - Session Mean & Stddev: 8481524.570000,2426.366259
Num Phases Traversed: 173157.000000
Num Phases Traversed - Session Mean & Stddev: 1731.570000,0.495076
Num Path Iterations: 173157.000000
Num Path Iterations - Session Mean & Stddev: 1731.570000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f10e7000000 of size 622592 bytes.
[1.287s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848554339.000000
Throughput: 84855433.900000
Num Accesses - Session Mean & Stddev: 8485543.390000,2390.462436
Num Phases Traversed: 173239.000000
Num Phases Traversed - Session Mean & Stddev: 1732.390000,0.487750
Num Path Iterations: 173239.000000
Num Path Iterations - Session Mean & Stddev: 1732.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4a75400000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848368101.000000
Throughput: 84836810.100000
Num Accesses - Session Mean & Stddev: 8483681.010000,3430.349911
Num Phases Traversed: 173201.000000
Num Phases Traversed - Session Mean & Stddev: 1732.010000,0.699929
Num Path Iterations: 173201.000000
Num Path Iterations - Session Mean & Stddev: 1732.010000,0.699929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff663400000 of size 622592 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848382804.000000
Throughput: 84838280.400000
Num Accesses - Session Mean & Stddev: 8483828.040000,960.395938
Num Phases Traversed: 173204.000000
Num Phases Traversed - Session Mean & Stddev: 1732.040000,0.195959
Num Path Iterations: 173204.000000
Num Path Iterations - Session Mean & Stddev: 1732.040000,0.195959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f161d200000 of size 622592 bytes.
[1.293s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 847789783.000000
Throughput: 84778978.300000
Num Accesses - Session Mean & Stddev: 8477897.830000,1840.976350
Num Phases Traversed: 173083.000000
Num Phases Traversed - Session Mean & Stddev: 1730.830000,0.375633
Num Path Iterations: 173083.000000
Num Path Iterations - Session Mean & Stddev: 1730.830000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff434a00000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848696468.000000
Throughput: 84869646.800000
Num Accesses - Session Mean & Stddev: 8486964.680000,2286.199619
Num Phases Traversed: 173268.000000
Num Phases Traversed - Session Mean & Stddev: 1732.680000,0.466476
Num Path Iterations: 173268.000000
Num Path Iterations - Session Mean & Stddev: 1732.680000,0.466476
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa2a0600000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 847843694.000000
Throughput: 84784369.400000
Num Accesses - Session Mean & Stddev: 8478436.940000,3162.569388
Num Phases Traversed: 173094.000000
Num Phases Traversed - Session Mean & Stddev: 1730.940000,0.645291
Num Path Iterations: 173094.000000
Num Path Iterations - Session Mean & Stddev: 1730.940000,0.645291
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbad3400000 of size 622592 bytes.
[1.295s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848549438.000000
Throughput: 84854943.800000
Num Accesses - Session Mean & Stddev: 8485494.380000,2378.878962
Num Phases Traversed: 173238.000000
Num Phases Traversed - Session Mean & Stddev: 1732.380000,0.485386
Num Path Iterations: 173238.000000
Num Path Iterations - Session Mean & Stddev: 1732.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f12bc400000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848319091.000000
Throughput: 84831909.100000
Num Accesses - Session Mean & Stddev: 8483190.910000,3402.226051
Num Phases Traversed: 173191.000000
Num Phases Traversed - Session Mean & Stddev: 1731.910000,0.694190
Num Path Iterations: 173191.000000
Num Path Iterations - Session Mean & Stddev: 1731.910000,0.694190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8199a00000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848093645.000000
Throughput: 84809364.500000
Num Accesses - Session Mean & Stddev: 8480936.450000,2438.216715
Num Phases Traversed: 173145.000000
Num Phases Traversed - Session Mean & Stddev: 1731.450000,0.497494
Num Path Iterations: 173145.000000
Num Path Iterations - Session Mean & Stddev: 1731.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f680f000000 of size 622592 bytes.
[1.257s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 843241655.000000
Throughput: 84324165.500000
Num Accesses - Session Mean & Stddev: 8432416.550000,2627.868487
Num Phases Traversed: 172155.000000
Num Phases Traversed - Session Mean & Stddev: 1721.550000,0.536190
Num Path Iterations: 172155.000000
Num Path Iterations - Session Mean & Stddev: 1721.550000,0.536190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2e8a200000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848382804.000000
Throughput: 84838280.400000
Num Accesses - Session Mean & Stddev: 8483828.040000,2183.009051
Num Phases Traversed: 173204.000000
Num Phases Traversed - Session Mean & Stddev: 1732.040000,0.445421
Num Path Iterations: 173204.000000
Num Path Iterations - Session Mean & Stddev: 1732.040000,0.445421
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f38b5800000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 847941714.000000
Throughput: 84794171.400000
Num Accesses - Session Mean & Stddev: 8479417.140000,1700.583441
Num Phases Traversed: 173114.000000
Num Phases Traversed - Session Mean & Stddev: 1731.140000,0.346987
Num Path Iterations: 173114.000000
Num Path Iterations - Session Mean & Stddev: 1731.140000,0.346987
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbacca00000 of size 622592 bytes.
[1.291s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848637656.000000
Throughput: 84863765.600000
Num Accesses - Session Mean & Stddev: 8486376.560000,2622.836443
Num Phases Traversed: 173256.000000
Num Phases Traversed - Session Mean & Stddev: 1732.560000,0.535164
Num Path Iterations: 173256.000000
Num Path Iterations - Session Mean & Stddev: 1732.560000,0.535164
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1ab8c00000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 848221071.000000
Throughput: 84822107.100000
Num Accesses - Session Mean & Stddev: 8482210.710000,2223.887818
Num Phases Traversed: 173171.000000
Num Phases Traversed - Session Mean & Stddev: 1731.710000,0.453762
Num Path Iterations: 173171.000000
Num Path Iterations - Session Mean & Stddev: 1731.710000,0.453762
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe6e3a00000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 847828991.000000
Throughput: 84782899.100000
Num Accesses - Session Mean & Stddev: 8478289.910000,3402.226051
Num Phases Traversed: 173091.000000
Num Phases Traversed - Session Mean & Stddev: 1730.910000,0.694190
Num Path Iterations: 173091.000000
Num Path Iterations - Session Mean & Stddev: 1730.910000,0.694190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7190400000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848529834.000000
Throughput: 84852983.400000
Num Accesses - Session Mean & Stddev: 8485298.340000,2958.515757
Num Phases Traversed: 173234.000000
Num Phases Traversed - Session Mean & Stddev: 1732.340000,0.603656
Num Path Iterations: 173234.000000
Num Path Iterations - Session Mean & Stddev: 1732.340000,0.603656
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4d82e00000 of size 622592 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848181863.000000
Throughput: 84818186.300000
Num Accesses - Session Mean & Stddev: 8481818.630000,2366.223914
Num Phases Traversed: 173163.000000
Num Phases Traversed - Session Mean & Stddev: 1731.630000,0.482804
Num Path Iterations: 173163.000000
Num Path Iterations - Session Mean & Stddev: 1731.630000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5f12c00000 of size 622592 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848422012.000000
Throughput: 84842201.200000
Num Accesses - Session Mean & Stddev: 8484220.120000,3344.191882
Num Phases Traversed: 173212.000000
Num Phases Traversed - Session Mean & Stddev: 1732.120000,0.682349
Num Path Iterations: 173212.000000
Num Path Iterations - Session Mean & Stddev: 1732.120000,0.682349
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4864,0.01 0/4864,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4864,0.01 0/4864,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4864 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6d6d200000 of size 622592 bytes.
[1.287s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 848593547.000000
Throughput: 84859354.700000
Num Accesses - Session Mean & Stddev: 8485935.470000,2446.085123
Num Phases Traversed: 173247.000000
Num Phases Traversed - Session Mean & Stddev: 1732.470000,0.499099
Num Path Iterations: 173247.000000
Num Path Iterations - Session Mean & Stddev: 1732.470000,0.499099
+ set +x

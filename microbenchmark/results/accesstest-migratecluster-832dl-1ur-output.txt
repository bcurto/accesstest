++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffa2fe00000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2775972322.000000
Throughput: 277597232.200000
Num Accesses - Session Mean & Stddev: 27759723.220000,198.701313
Num Phases Traversed: 6128074.000000
Num Phases Traversed - Session Mean & Stddev: 61280.740000,0.438634
Num Path Iterations: 6128074.000000
Num Path Iterations - Session Mean & Stddev: 61280.740000,0.438634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f705be00000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2763742228.000000
Throughput: 276374222.800000
Num Accesses - Session Mean & Stddev: 27637422.280000,193.468658
Num Phases Traversed: 6101076.000000
Num Phases Traversed - Session Mean & Stddev: 61010.760000,0.427083
Num Path Iterations: 6101076.000000
Num Path Iterations - Session Mean & Stddev: 61010.760000,0.427083
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5492a00000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2763909838.000000
Throughput: 276390983.800000
Num Accesses - Session Mean & Stddev: 27639098.380000,225.774037
Num Phases Traversed: 6101446.000000
Num Phases Traversed - Session Mean & Stddev: 61014.460000,0.498397
Num Path Iterations: 6101446.000000
Num Path Iterations - Session Mean & Stddev: 61014.460000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f32b3000000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2777772544.000000
Throughput: 277777254.400000
Num Accesses - Session Mean & Stddev: 27777725.440000,226.318727
Num Phases Traversed: 6132048.000000
Num Phases Traversed - Session Mean & Stddev: 61320.480000,0.499600
Num Path Iterations: 6132048.000000
Num Path Iterations - Session Mean & Stddev: 61320.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f55cfe00000 of size 53248 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2759248921.000000
Throughput: 275924892.100000
Num Accesses - Session Mean & Stddev: 27592489.210000,233.240018
Num Phases Traversed: 6091157.000000
Num Phases Traversed - Session Mean & Stddev: 60911.570000,0.514879
Num Path Iterations: 6091157.000000
Num Path Iterations - Session Mean & Stddev: 60911.570000,0.514879
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4e8de00000 of size 53248 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2785212616.000000
Throughput: 278521261.600000
Num Accesses - Session Mean & Stddev: 27852126.160000,203.396496
Num Phases Traversed: 6148472.000000
Num Phases Traversed - Session Mean & Stddev: 61484.720000,0.448999
Num Path Iterations: 6148472.000000
Num Path Iterations - Session Mean & Stddev: 61484.720000,0.448999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1126400000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2779132903.000000
Throughput: 277913290.300000
Num Accesses - Session Mean & Stddev: 27791329.030000,226.454695
Num Phases Traversed: 6135051.000000
Num Phases Traversed - Session Mean & Stddev: 61350.510000,0.499900
Num Path Iterations: 6135051.000000
Num Path Iterations - Session Mean & Stddev: 61350.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f355b400000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2782204696.000000
Throughput: 278220469.600000
Num Accesses - Session Mean & Stddev: 27822046.960000,211.313697
Num Phases Traversed: 6141832.000000
Num Phases Traversed - Session Mean & Stddev: 61418.320000,0.466476
Num Path Iterations: 6141832.000000
Num Path Iterations - Session Mean & Stddev: 61418.320000,0.466476
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4a67200000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2771731789.000000
Throughput: 277173178.900000
Num Accesses - Session Mean & Stddev: 27717317.890000,152.345456
Num Phases Traversed: 6118713.000000
Num Phases Traversed - Session Mean & Stddev: 61187.130000,0.336303
Num Path Iterations: 6118713.000000
Num Path Iterations - Session Mean & Stddev: 61187.130000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7647000000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2786876032.000000
Throughput: 278687603.200000
Num Accesses - Session Mean & Stddev: 27868760.320000,224.863286
Num Phases Traversed: 6152144.000000
Num Phases Traversed - Session Mean & Stddev: 61521.440000,0.496387
Num Path Iterations: 6152144.000000
Num Path Iterations - Session Mean & Stddev: 61521.440000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f656e200000 of size 53248 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2762805877.000000
Throughput: 276280587.700000
Num Accesses - Session Mean & Stddev: 27628058.770000,144.605661
Num Phases Traversed: 6099009.000000
Num Phases Traversed - Session Mean & Stddev: 60990.090000,0.319218
Num Path Iterations: 6099009.000000
Num Path Iterations - Session Mean & Stddev: 60990.090000,0.319218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff2cbe00000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2783671057.000000
Throughput: 278367105.700000
Num Accesses - Session Mean & Stddev: 27836710.570000,209.509439
Num Phases Traversed: 6145069.000000
Num Phases Traversed - Session Mean & Stddev: 61450.690000,0.462493
Num Path Iterations: 6145069.000000
Num Path Iterations - Session Mean & Stddev: 61450.690000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa02f200000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2778775486.000000
Throughput: 277877548.600000
Num Accesses - Session Mean & Stddev: 27787754.860000,219.880059
Num Phases Traversed: 6134262.000000
Num Phases Traversed - Session Mean & Stddev: 61342.620000,0.485386
Num Path Iterations: 6134262.000000
Num Path Iterations - Session Mean & Stddev: 61342.620000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe61c600000 of size 53248 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2761022416.000000
Throughput: 276102241.600000
Num Accesses - Session Mean & Stddev: 27610224.160000,203.396496
Num Phases Traversed: 6095072.000000
Num Phases Traversed - Session Mean & Stddev: 60950.720000,0.448999
Num Path Iterations: 6095072.000000
Num Path Iterations - Session Mean & Stddev: 60950.720000,0.448999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7798200000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2780881483.000000
Throughput: 278088148.300000
Num Accesses - Session Mean & Stddev: 27808814.830000,141.739060
Num Phases Traversed: 6138911.000000
Num Phases Traversed - Session Mean & Stddev: 61389.110000,0.312890
Num Path Iterations: 6138911.000000
Num Path Iterations - Session Mean & Stddev: 61389.110000,0.312890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe69f000000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2761725925.000000
Throughput: 276172592.500000
Num Accesses - Session Mean & Stddev: 27617259.250000,196.154754
Num Phases Traversed: 6096625.000000
Num Phases Traversed - Session Mean & Stddev: 60966.250000,0.433013
Num Path Iterations: 6096625.000000
Num Path Iterations - Session Mean & Stddev: 60966.250000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0a46600000 of size 53248 bytes.
[1.232s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2758728424.000000
Throughput: 275872842.400000
Num Accesses - Session Mean & Stddev: 27587284.240000,138.591350
Num Phases Traversed: 6090008.000000
Num Phases Traversed - Session Mean & Stddev: 60900.080000,0.305941
Num Path Iterations: 6090008.000000
Num Path Iterations - Session Mean & Stddev: 60900.080000,0.305941
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe384200000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2768351956.000000
Throughput: 276835195.600000
Num Accesses - Session Mean & Stddev: 27683519.560000,226.318727
Num Phases Traversed: 6111252.000000
Num Phases Traversed - Session Mean & Stddev: 61112.520000,0.499600
Num Path Iterations: 6111252.000000
Num Path Iterations - Session Mean & Stddev: 61112.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f285b400000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2773121593.000000
Throughput: 277312159.300000
Num Accesses - Session Mean & Stddev: 27731215.930000,177.712310
Num Phases Traversed: 6121781.000000
Num Phases Traversed - Session Mean & Stddev: 61217.810000,0.392301
Num Path Iterations: 6121781.000000
Num Path Iterations - Session Mean & Stddev: 61217.810000,0.392301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,1 0/416,1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,1 0/416,1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feb86800000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2780199265.000000
Throughput: 278019926.500000
Num Accesses - Session Mean & Stddev: 27801992.650000,98.729061
Num Phases Traversed: 6137405.000000
Num Phases Traversed - Session Mean & Stddev: 61374.050000,0.217945
Num Path Iterations: 6137405.000000
Num Path Iterations - Session Mean & Stddev: 61374.050000,0.217945
+ set +x

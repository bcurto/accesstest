++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4540a00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1975354628.000000
Throughput: 197535462.800000
Num Accesses - Session Mean & Stddev: 19753546.280000,1576.659590
Num Phases Traversed: 843908.000000
Num Phases Traversed - Session Mean & Stddev: 8439.080000,0.673498
Num Path Iterations: 843908.000000
Num Path Iterations - Session Mean & Stddev: 8439.080000,0.673498
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f250be00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1975850920.000000
Throughput: 197585092.000000
Num Accesses - Session Mean & Stddev: 19758509.200000,1517.141397
Num Phases Traversed: 844120.000000
Num Phases Traversed - Session Mean & Stddev: 8441.200000,0.648074
Num Path Iterations: 844120.000000
Num Path Iterations - Session Mean & Stddev: 8441.200000,0.648074
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6ef3800000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1971346836.000000
Throughput: 197134683.600000
Num Accesses - Session Mean & Stddev: 19713468.360000,1584.979751
Num Phases Traversed: 842196.000000
Num Phases Traversed - Session Mean & Stddev: 8421.960000,0.677052
Num Path Iterations: 842196.000000
Num Path Iterations - Session Mean & Stddev: 8421.960000,0.677052
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0ffa200000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974902815.000000
Throughput: 197490281.500000
Num Accesses - Session Mean & Stddev: 19749028.150000,1600.634676
Num Phases Traversed: 843715.000000
Num Phases Traversed - Session Mean & Stddev: 8437.150000,0.683740
Num Path Iterations: 843715.000000
Num Path Iterations - Session Mean & Stddev: 8437.150000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f20cfe00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1976396373.000000
Throughput: 197639637.300000
Num Accesses - Session Mean & Stddev: 19763963.730000,1258.709354
Num Phases Traversed: 844353.000000
Num Phases Traversed - Session Mean & Stddev: 8443.530000,0.537680
Num Path Iterations: 844353.000000
Num Path Iterations - Session Mean & Stddev: 8443.530000,0.537680
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0b53e00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1963911820.000000
Throughput: 196391182.000000
Num Accesses - Session Mean & Stddev: 19639118.200000,1517.141397
Num Phases Traversed: 839020.000000
Num Phases Traversed - Session Mean & Stddev: 8390.200000,0.648074
Num Path Iterations: 839020.000000
Num Path Iterations - Session Mean & Stddev: 8390.200000,0.648074
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f26f3a00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974696807.000000
Throughput: 197469680.700000
Num Accesses - Session Mean & Stddev: 19746968.070000,1475.201541
Num Phases Traversed: 843627.000000
Num Phases Traversed - Session Mean & Stddev: 8436.270000,0.630159
Num Path Iterations: 843627.000000
Num Path Iterations - Session Mean & Stddev: 8436.270000,0.630159
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f56b9400000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1973390529.000000
Throughput: 197339052.900000
Num Accesses - Session Mean & Stddev: 19733905.290000,1392.649075
Num Phases Traversed: 843069.000000
Num Phases Traversed - Session Mean & Stddev: 8430.690000,0.594895
Num Path Iterations: 843069.000000
Num Path Iterations - Session Mean & Stddev: 8430.690000,0.594895
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9599e00000 of size 294912 bytes.
[1.271s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1974207538.000000
Throughput: 197420753.800000
Num Accesses - Session Mean & Stddev: 19742075.380000,1566.197234
Num Phases Traversed: 843418.000000
Num Phases Traversed - Session Mean & Stddev: 8434.180000,0.669029
Num Path Iterations: 843418.000000
Num Path Iterations - Session Mean & Stddev: 8434.180000,0.669029
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0403400000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1975265670.000000
Throughput: 197526567.000000
Num Accesses - Session Mean & Stddev: 19752656.700000,1461.954031
Num Phases Traversed: 843870.000000
Num Phases Traversed - Session Mean & Stddev: 8438.700000,0.624500
Num Path Iterations: 843870.000000
Num Path Iterations - Session Mean & Stddev: 8438.700000,0.624500
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff3ffe00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974287132.000000
Throughput: 197428713.200000
Num Accesses - Session Mean & Stddev: 19742871.320000,1423.204784
Num Phases Traversed: 843452.000000
Num Phases Traversed - Session Mean & Stddev: 8434.520000,0.607947
Num Path Iterations: 843452.000000
Num Path Iterations - Session Mean & Stddev: 8434.520000,0.607947
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9416800000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974268404.000000
Throughput: 197426840.400000
Num Accesses - Session Mean & Stddev: 19742684.040000,1492.376024
Num Phases Traversed: 843444.000000
Num Phases Traversed - Session Mean & Stddev: 8434.440000,0.637495
Num Path Iterations: 843444.000000
Num Path Iterations - Session Mean & Stddev: 8434.440000,0.637495
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8c59e00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974436956.000000
Throughput: 197443695.600000
Num Accesses - Session Mean & Stddev: 19744369.560000,1542.930350
Num Phases Traversed: 843516.000000
Num Phases Traversed - Session Mean & Stddev: 8435.160000,0.659090
Num Path Iterations: 843516.000000
Num Path Iterations - Session Mean & Stddev: 8435.160000,0.659090
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f335fa00000 of size 294912 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974315224.000000
Throughput: 197431522.400000
Num Accesses - Session Mean & Stddev: 19743152.240000,1424.744216
Num Phases Traversed: 843464.000000
Num Phases Traversed - Session Mean & Stddev: 8434.640000,0.608605
Num Path Iterations: 843464.000000
Num Path Iterations - Session Mean & Stddev: 8434.640000,0.608605
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2846200000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974830244.000000
Throughput: 197483024.400000
Num Accesses - Session Mean & Stddev: 19748302.440000,1578.049330
Num Phases Traversed: 843684.000000
Num Phases Traversed - Session Mean & Stddev: 8436.840000,0.674092
Num Path Iterations: 843684.000000
Num Path Iterations - Session Mean & Stddev: 8436.840000,0.674092
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6196000000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1973664426.000000
Throughput: 197366442.600000
Num Accesses - Session Mean & Stddev: 19736644.260000,1588.433622
Num Phases Traversed: 843186.000000
Num Phases Traversed - Session Mean & Stddev: 8431.860000,0.678528
Num Path Iterations: 843186.000000
Num Path Iterations - Session Mean & Stddev: 8431.860000,0.678528
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb49dc00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974738945.000000
Throughput: 197473894.500000
Num Accesses - Session Mean & Stddev: 19747389.450000,1419.155829
Num Phases Traversed: 843645.000000
Num Phases Traversed - Session Mean & Stddev: 8436.450000,0.606218
Num Path Iterations: 843645.000000
Num Path Iterations - Session Mean & Stddev: 8436.450000,0.606218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7a4ae00000 of size 294912 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974411205.000000
Throughput: 197441120.500000
Num Accesses - Session Mean & Stddev: 19744112.050000,1634.514297
Num Phases Traversed: 843505.000000
Num Phases Traversed - Session Mean & Stddev: 8435.050000,0.698212
Num Path Iterations: 843505.000000
Num Path Iterations - Session Mean & Stddev: 8435.050000,0.698212
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe0d0600000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974525914.000000
Throughput: 197452591.400000
Num Accesses - Session Mean & Stddev: 19745259.140000,1381.785041
Num Phases Traversed: 843554.000000
Num Phases Traversed - Session Mean & Stddev: 8435.540000,0.590254
Num Path Iterations: 843554.000000
Num Path Iterations - Session Mean & Stddev: 8435.540000,0.590254
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0417000000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974987091.000000
Throughput: 197498709.100000
Num Accesses - Session Mean & Stddev: 19749870.910000,1384.756412
Num Phases Traversed: 843751.000000
Num Phases Traversed - Session Mean & Stddev: 8437.510000,0.591523
Num Path Iterations: 843751.000000
Num Path Iterations - Session Mean & Stddev: 8437.510000,0.591523
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd461000000 of size 24576 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2616553871.000000
Throughput: 261655387.100000
Num Accesses - Session Mean & Stddev: 26165538.710000,109.800664
Num Phases Traversed: 11426099.000000
Num Phases Traversed - Session Mean & Stddev: 114260.990000,0.479479
Num Path Iterations: 11426099.000000
Num Path Iterations - Session Mean & Stddev: 114260.990000,0.479479
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8e4f600000 of size 24576 bytes.
[1.266s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2623772638.000000
Throughput: 262377263.800000
Num Accesses - Session Mean & Stddev: 26237726.380000,94.862404
Num Phases Traversed: 11457622.000000
Num Phases Traversed - Session Mean & Stddev: 114576.220000,0.414246
Num Path Iterations: 11457622.000000
Num Path Iterations - Session Mean & Stddev: 114576.220000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f40de800000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2649789557.000000
Throughput: 264978955.700000
Num Accesses - Session Mean & Stddev: 26497895.570000,107.678712
Num Phases Traversed: 11571233.000000
Num Phases Traversed - Session Mean & Stddev: 115712.330000,0.470213
Num Path Iterations: 11571233.000000
Num Path Iterations - Session Mean & Stddev: 115712.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa7a5400000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2651250577.000000
Throughput: 265125057.700000
Num Accesses - Session Mean & Stddev: 26512505.770000,77.013486
Num Phases Traversed: 11577613.000000
Num Phases Traversed - Session Mean & Stddev: 115776.130000,0.336303
Num Path Iterations: 11577613.000000
Num Path Iterations - Session Mean & Stddev: 115776.130000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6e00400000 of size 24576 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2626382322.000000
Throughput: 262638232.200000
Num Accesses - Session Mean & Stddev: 26263823.220000,87.978927
Num Phases Traversed: 11469018.000000
Num Phases Traversed - Session Mean & Stddev: 114690.180000,0.384187
Num Path Iterations: 11469018.000000
Num Path Iterations - Session Mean & Stddev: 114690.180000,0.384187
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f83ab200000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2640926112.000000
Throughput: 264092611.200000
Num Accesses - Session Mean & Stddev: 26409261.120000,102.820745
Num Phases Traversed: 11532528.000000
Num Phases Traversed - Session Mean & Stddev: 115325.280000,0.448999
Num Path Iterations: 11532528.000000
Num Path Iterations - Session Mean & Stddev: 115325.280000,0.448999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3b42c00000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2638302688.000000
Throughput: 263830268.800000
Num Accesses - Session Mean & Stddev: 26383026.880000,102.820745
Num Phases Traversed: 11521072.000000
Num Phases Traversed - Session Mean & Stddev: 115210.720000,0.448999
Num Path Iterations: 11521072.000000
Num Path Iterations - Session Mean & Stddev: 115210.720000,0.448999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f020ec00000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2623208153.000000
Throughput: 262320815.300000
Num Accesses - Session Mean & Stddev: 26232081.530000,113.372347
Num Phases Traversed: 11455157.000000
Num Phases Traversed - Session Mean & Stddev: 114551.570000,0.495076
Num Path Iterations: 11455157.000000
Num Path Iterations - Session Mean & Stddev: 114551.570000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9c5d200000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2607268379.000000
Throughput: 260726837.900000
Num Accesses - Session Mean & Stddev: 26072683.790000,114.477098
Num Phases Traversed: 11385551.000000
Num Phases Traversed - Session Mean & Stddev: 113855.510000,0.499900
Num Path Iterations: 11385551.000000
Num Path Iterations - Session Mean & Stddev: 113855.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f40b3a00000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2633259879.000000
Throughput: 263325987.900000
Num Accesses - Session Mean & Stddev: 26332598.790000,114.477098
Num Phases Traversed: 11499051.000000
Num Phases Traversed - Session Mean & Stddev: 114990.510000,0.499900
Num Path Iterations: 11499051.000000
Num Path Iterations - Session Mean & Stddev: 114990.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe2f5e00000 of size 24576 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2626420565.000000
Throughput: 262642056.500000
Num Accesses - Session Mean & Stddev: 26264205.650000,99.159909
Num Phases Traversed: 11469185.000000
Num Phases Traversed - Session Mean & Stddev: 114691.850000,0.433013
Num Path Iterations: 11469185.000000
Num Path Iterations - Session Mean & Stddev: 114691.850000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3b1e000000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2640724134.000000
Throughput: 264072413.400000
Num Accesses - Session Mean & Stddev: 26407241.340000,114.133012
Num Phases Traversed: 11531646.000000
Num Phases Traversed - Session Mean & Stddev: 115316.460000,0.498397
Num Path Iterations: 11531646.000000
Num Path Iterations - Session Mean & Stddev: 115316.460000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9a17400000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2659047569.000000
Throughput: 265904756.900000
Num Accesses - Session Mean & Stddev: 26590475.690000,111.694735
Num Phases Traversed: 11611661.000000
Num Phases Traversed - Session Mean & Stddev: 116116.610000,0.487750
Num Path Iterations: 11611661.000000
Num Path Iterations - Session Mean & Stddev: 116116.610000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f464b000000 of size 24576 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2635604152.000000
Throughput: 263560415.200000
Num Accesses - Session Mean & Stddev: 26356041.520000,74.416192
Num Phases Traversed: 11509288.000000
Num Phases Traversed - Session Mean & Stddev: 115092.880000,0.324962
Num Path Iterations: 11509288.000000
Num Path Iterations - Session Mean & Stddev: 115092.880000,0.324962
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1ad5600000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2633440102.000000
Throughput: 263344010.200000
Num Accesses - Session Mean & Stddev: 26334401.020000,111.153496
Num Phases Traversed: 11499838.000000
Num Phases Traversed - Session Mean & Stddev: 114998.380000,0.485386
Num Path Iterations: 11499838.000000
Num Path Iterations - Session Mean & Stddev: 114998.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efc13800000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2616641120.000000
Throughput: 261664112.000000
Num Accesses - Session Mean & Stddev: 26166411.200000,91.600000
Num Phases Traversed: 11426480.000000
Num Phases Traversed - Session Mean & Stddev: 114264.800000,0.400000
Num Path Iterations: 11426480.000000
Num Path Iterations - Session Mean & Stddev: 114264.800000,0.400000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f39b9e00000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2640700318.000000
Throughput: 264070031.800000
Num Accesses - Session Mean & Stddev: 26407003.180000,113.024898
Num Phases Traversed: 11531542.000000
Num Phases Traversed - Session Mean & Stddev: 115315.420000,0.493559
Num Path Iterations: 11531542.000000
Num Path Iterations - Session Mean & Stddev: 115315.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fec27000000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2650992036.000000
Throughput: 265099203.600000
Num Accesses - Session Mean & Stddev: 26509920.360000,95.633208
Num Phases Traversed: 11576484.000000
Num Phases Traversed - Session Mean & Stddev: 115764.840000,0.417612
Num Path Iterations: 11576484.000000
Num Path Iterations - Session Mean & Stddev: 115764.840000,0.417612
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbd57c00000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2646544627.000000
Throughput: 264654462.700000
Num Accesses - Session Mean & Stddev: 26465446.270000,110.562187
Num Phases Traversed: 11557063.000000
Num Phases Traversed - Session Mean & Stddev: 115570.630000,0.482804
Num Path Iterations: 11557063.000000
Num Path Iterations - Session Mean & Stddev: 115570.630000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0.1 0/192,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0.1 0/192,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f40e0a00000 of size 24576 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2633614142.000000
Throughput: 263361414.200000
Num Accesses - Session Mean & Stddev: 26336141.420000,55.906025
Num Phases Traversed: 11500598.000000
Num Phases Traversed - Session Mean & Stddev: 115005.980000,0.244131
Num Path Iterations: 11500598.000000
Num Path Iterations - Session Mean & Stddev: 115005.980000,0.244131
+ set +x

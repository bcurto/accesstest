++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f75c0000000 of size 25165824 bytes.
[5.075s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 232827780.000000
Throughput: 23282778.000000
Num Accesses - Session Mean & Stddev: 2328277.800000,177460.536759
Num Phases Traversed: 1284.000000
Num Phases Traversed - Session Mean & Stddev: 12.840000,0.902441
Num Path Iterations: 1284.000000
Num Path Iterations - Session Mean & Stddev: 12.840000,0.902441
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa8c0000000 of size 25165824 bytes.
[5.077s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 233614360.000000
Throughput: 23361436.000000
Num Accesses - Session Mean & Stddev: 2336143.600000,180827.881962
Num Phases Traversed: 1288.000000
Num Phases Traversed - Session Mean & Stddev: 12.880000,0.919565
Num Path Iterations: 1288.000000
Num Path Iterations - Session Mean & Stddev: 12.880000,0.919565
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efe40000000 of size 25165824 bytes.
[5.078s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 232434490.000000
Throughput: 23243449.000000
Num Accesses - Session Mean & Stddev: 2324344.900000,174516.205681
Num Phases Traversed: 1282.000000
Num Phases Traversed - Session Mean & Stddev: 12.820000,0.887468
Num Path Iterations: 1282.000000
Num Path Iterations - Session Mean & Stddev: 12.820000,0.887468
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fab00000000 of size 25165824 bytes.
[5.078s] Test is ready to start.
Test duration in range [10.000, 10.014] seconds
Num Accesses: 232827780.000000
Throughput: 23282778.000000
Num Accesses - Session Mean & Stddev: 2328277.800000,163865.589383
Num Phases Traversed: 1284.000000
Num Phases Traversed - Session Mean & Stddev: 12.840000,0.833307
Num Path Iterations: 1284.000000
Num Path Iterations - Session Mean & Stddev: 12.840000,0.833307
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5080000000 of size 25165824 bytes.
[5.077s] Test is ready to start.
Test duration in range [10.000, 10.013] seconds
Num Accesses: 232827780.000000
Throughput: 23282778.000000
Num Accesses - Session Mean & Stddev: 2328277.800000,179626.354490
Num Phases Traversed: 1284.000000
Num Phases Traversed - Session Mean & Stddev: 12.840000,0.913455
Num Path Iterations: 1284.000000
Num Path Iterations - Session Mean & Stddev: 12.840000,0.913455
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3040000000 of size 25165824 bytes.
[5.076s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 232434490.000000
Throughput: 23243449.000000
Num Accesses - Session Mean & Stddev: 2324344.900000,165415.735537
Num Phases Traversed: 1282.000000
Num Phases Traversed - Session Mean & Stddev: 12.820000,0.841190
Num Path Iterations: 1282.000000
Num Path Iterations - Session Mean & Stddev: 12.820000,0.841190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f11c0000000 of size 25165824 bytes.
[5.084s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 232434490.000000
Throughput: 23243449.000000
Num Accesses - Session Mean & Stddev: 2324344.900000,174516.205681
Num Phases Traversed: 1282.000000
Num Phases Traversed - Session Mean & Stddev: 12.820000,0.887468
Num Path Iterations: 1282.000000
Num Path Iterations - Session Mean & Stddev: 12.820000,0.887468
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0cc0000000 of size 25165824 bytes.
[5.078s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 232434490.000000
Throughput: 23243449.000000
Num Accesses - Session Mean & Stddev: 2324344.900000,183165.079989
Num Phases Traversed: 1282.000000
Num Phases Traversed - Session Mean & Stddev: 12.820000,0.931450
Num Path Iterations: 1282.000000
Num Path Iterations - Session Mean & Stddev: 12.820000,0.931450
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faac0000000 of size 25165824 bytes.
[5.079s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 232434490.000000
Throughput: 23243449.000000
Num Accesses - Session Mean & Stddev: 2324344.900000,174516.205681
Num Phases Traversed: 1282.000000
Num Phases Traversed - Session Mean & Stddev: 12.820000,0.887468
Num Path Iterations: 1282.000000
Num Path Iterations - Session Mean & Stddev: 12.820000,0.887468
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff880000000 of size 25165824 bytes.
[5.078s] Test is ready to start.
Test duration in range [10.000, 10.013] seconds
Num Accesses: 233221070.000000
Throughput: 23322107.000000
Num Accesses - Session Mean & Stddev: 2332210.700000,180271.025857
Num Phases Traversed: 1286.000000
Num Phases Traversed - Session Mean & Stddev: 12.860000,0.916733
Num Path Iterations: 1286.000000
Num Path Iterations - Session Mean & Stddev: 12.860000,0.916733
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8280000000 of size 25165824 bytes.
[5.079s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 232041200.000000
Throughput: 23204120.000000
Num Accesses - Session Mean & Stddev: 2320412.000000,166858.815584
Num Phases Traversed: 1280.000000
Num Phases Traversed - Session Mean & Stddev: 12.800000,0.848528
Num Path Iterations: 1280.000000
Num Path Iterations - Session Mean & Stddev: 12.800000,0.848528
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1fc0000000 of size 25165824 bytes.
[5.076s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 232827780.000000
Throughput: 23282778.000000
Num Accesses - Session Mean & Stddev: 2328277.800000,179626.354490
Num Phases Traversed: 1284.000000
Num Phases Traversed - Session Mean & Stddev: 12.840000,0.913455
Num Path Iterations: 1284.000000
Num Path Iterations - Session Mean & Stddev: 12.840000,0.913455
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f84c0000000 of size 25165824 bytes.
[5.013s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 232434490.000000
Throughput: 23243449.000000
Num Accesses - Session Mean & Stddev: 2324344.900000,160672.322826
Num Phases Traversed: 1282.000000
Num Phases Traversed - Session Mean & Stddev: 12.820000,0.817068
Num Path Iterations: 1282.000000
Num Path Iterations - Session Mean & Stddev: 12.820000,0.817068
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbcc0000000 of size 25165824 bytes.
[5.078s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 232827780.000000
Throughput: 23282778.000000
Num Accesses - Session Mean & Stddev: 2328277.800000,175267.957671
Num Phases Traversed: 1284.000000
Num Phases Traversed - Session Mean & Stddev: 12.840000,0.891291
Num Path Iterations: 1284.000000
Num Path Iterations - Session Mean & Stddev: 12.840000,0.891291
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fad00000000 of size 25165824 bytes.
[5.086s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 232434490.000000
Throughput: 23243449.000000
Num Accesses - Session Mean & Stddev: 2324344.900000,170026.867889
Num Phases Traversed: 1282.000000
Num Phases Traversed - Session Mean & Stddev: 12.820000,0.864639
Num Path Iterations: 1282.000000
Num Path Iterations - Session Mean & Stddev: 12.820000,0.864639
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcf00000000 of size 25165824 bytes.
[5.080s] Test is ready to start.
Test duration in range [10.000, 10.016] seconds
Num Accesses: 232827780.000000
Throughput: 23282778.000000
Num Accesses - Session Mean & Stddev: 2328277.800000,183881.476686
Num Phases Traversed: 1284.000000
Num Phases Traversed - Session Mean & Stddev: 12.840000,0.935094
Num Path Iterations: 1284.000000
Num Path Iterations - Session Mean & Stddev: 12.840000,0.935094
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe540000000 of size 25165824 bytes.
[5.075s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 233614360.000000
Throughput: 23361436.000000
Num Accesses - Session Mean & Stddev: 2336143.600000,185055.378565
Num Phases Traversed: 1288.000000
Num Phases Traversed - Session Mean & Stddev: 12.880000,0.941063
Num Path Iterations: 1288.000000
Num Path Iterations - Session Mean & Stddev: 12.880000,0.941063
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6800000000 of size 25165824 bytes.
[5.074s] Test is ready to start.
Test duration in range [10.000, 10.017] seconds
Num Accesses: 233221070.000000
Throughput: 23322107.000000
Num Accesses - Session Mean & Stddev: 2332210.700000,175928.600638
Num Phases Traversed: 1286.000000
Num Phases Traversed - Session Mean & Stddev: 12.860000,0.894651
Num Path Iterations: 1286.000000
Num Path Iterations - Session Mean & Stddev: 12.860000,0.894651
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9d40000000 of size 25165824 bytes.
[5.083s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 233221070.000000
Throughput: 23322107.000000
Num Accesses - Session Mean & Stddev: 2332210.700000,175928.600638
Num Phases Traversed: 1286.000000
Num Phases Traversed - Session Mean & Stddev: 12.860000,0.894651
Num Path Iterations: 1286.000000
Num Path Iterations - Session Mean & Stddev: 12.860000,0.894651
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/196608,0.1 0/196608,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/196608,0.1 0/196608,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    196608 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3100000000 of size 25165824 bytes.
[5.042s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 232827780.000000
Throughput: 23282778.000000
Num Accesses - Session Mean & Stddev: 2328277.800000,183881.476686
Num Phases Traversed: 1284.000000
Num Phases Traversed - Session Mean & Stddev: 12.840000,0.935094
Num Path Iterations: 1284.000000
Num Path Iterations - Session Mean & Stddev: 12.840000,0.935094
+ set +x

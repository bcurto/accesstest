++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f940ce00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2236067493.000000
Throughput: 223606749.300000
Num Accesses - Session Mean & Stddev: 22360674.930000,289.976629
Num Phases Traversed: 3848753.000000
Num Phases Traversed - Session Mean & Stddev: 38487.530000,0.499099
Num Path Iterations: 3848753.000000
Num Path Iterations - Session Mean & Stddev: 38487.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4075000000 of size 69632 bytes.
[1.263s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2234811952.000000
Throughput: 223481195.200000
Num Accesses - Session Mean & Stddev: 22348119.520000,280.809988
Num Phases Traversed: 3846592.000000
Num Phases Traversed - Session Mean & Stddev: 38465.920000,0.483322
Num Path Iterations: 3846592.000000
Num Path Iterations - Session Mean & Stddev: 38465.920000,0.483322
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc36f000000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2235691005.000000
Throughput: 223569100.500000
Num Accesses - Session Mean & Stddev: 22356910.050000,251.580380
Num Phases Traversed: 3848105.000000
Num Phases Traversed - Session Mean & Stddev: 38481.050000,0.433013
Num Path Iterations: 3848105.000000
Num Path Iterations - Session Mean & Stddev: 38481.050000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0e95a00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2235329623.000000
Throughput: 223532962.300000
Num Accesses - Session Mean & Stddev: 22353296.230000,273.193589
Num Phases Traversed: 3847483.000000
Num Phases Traversed - Session Mean & Stddev: 38474.830000,0.470213
Num Path Iterations: 3847483.000000
Num Path Iterations - Session Mean & Stddev: 38474.830000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0592e00000 of size 69632 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2236448629.000000
Throughput: 223644862.900000
Num Accesses - Session Mean & Stddev: 22364486.290000,247.794967
Num Phases Traversed: 3849409.000000
Num Phases Traversed - Session Mean & Stddev: 38494.090000,0.426497
Num Path Iterations: 3849409.000000
Num Path Iterations - Session Mean & Stddev: 38494.090000,0.426497
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3562400000 of size 69632 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2237271325.000000
Throughput: 223727132.500000
Num Accesses - Session Mean & Stddev: 22372713.250000,379.878043
Num Phases Traversed: 3850825.000000
Num Phases Traversed - Session Mean & Stddev: 38508.250000,0.653835
Num Path Iterations: 3850825.000000
Num Path Iterations - Session Mean & Stddev: 38508.250000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb8b3c00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2235654402.000000
Throughput: 223565440.200000
Num Accesses - Session Mean & Stddev: 22356544.020000,286.757493
Num Phases Traversed: 3848042.000000
Num Phases Traversed - Session Mean & Stddev: 38480.420000,0.493559
Num Path Iterations: 3848042.000000
Num Path Iterations - Session Mean & Stddev: 38480.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2763200000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2234218170.000000
Throughput: 223421817.000000
Num Accesses - Session Mean & Stddev: 22342181.700000,312.878075
Num Phases Traversed: 3845570.000000
Num Phases Traversed - Session Mean & Stddev: 38455.700000,0.538516
Num Path Iterations: 3845570.000000
Num Path Iterations - Session Mean & Stddev: 38455.700000,0.538516
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa9c4a00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2235844389.000000
Throughput: 223584438.900000
Num Accesses - Session Mean & Stddev: 22358443.890000,268.708574
Num Phases Traversed: 3848369.000000
Num Phases Traversed - Session Mean & Stddev: 38483.690000,0.462493
Num Path Iterations: 3848369.000000
Num Path Iterations - Session Mean & Stddev: 38483.690000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f024cc00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2234439531.000000
Throughput: 223443953.100000
Num Accesses - Session Mean & Stddev: 22344395.310000,290.441894
Num Phases Traversed: 3845951.000000
Num Phases Traversed - Session Mean & Stddev: 38459.510000,0.499900
Num Path Iterations: 3845951.000000
Num Path Iterations - Session Mean & Stddev: 38459.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0872e00000 of size 69632 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2233438468.000000
Throughput: 223343846.800000
Num Accesses - Session Mean & Stddev: 22334384.680000,260.868353
Num Phases Traversed: 3844228.000000
Num Phases Traversed - Session Mean & Stddev: 38442.280000,0.448999
Num Path Iterations: 3844228.000000
Num Path Iterations - Session Mean & Stddev: 38442.280000,0.448999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3afa800000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2234896197.000000
Throughput: 223489619.700000
Num Accesses - Session Mean & Stddev: 22348961.970000,325.100522
Num Phases Traversed: 3846737.000000
Num Phases Traversed - Session Mean & Stddev: 38467.370000,0.559553
Num Path Iterations: 3846737.000000
Num Path Iterations - Session Mean & Stddev: 38467.370000,0.559553
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe000a00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2235060620.000000
Throughput: 223506062.000000
Num Accesses - Session Mean & Stddev: 22350606.200000,232.400000
Num Phases Traversed: 3847020.000000
Num Phases Traversed - Session Mean & Stddev: 38470.200000,0.400000
Num Path Iterations: 3847020.000000
Num Path Iterations - Session Mean & Stddev: 38470.200000,0.400000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb875a00000 of size 69632 bytes.
[1.232s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2236353345.000000
Throughput: 223635334.500000
Num Accesses - Session Mean & Stddev: 22363533.450000,289.043850
Num Phases Traversed: 3849245.000000
Num Phases Traversed - Session Mean & Stddev: 38492.450000,0.497494
Num Path Iterations: 3849245.000000
Num Path Iterations - Session Mean & Stddev: 38492.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7a78800000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2236090733.000000
Throughput: 223609073.300000
Num Accesses - Session Mean & Stddev: 22360907.330000,249.965080
Num Phases Traversed: 3848793.000000
Num Phases Traversed - Session Mean & Stddev: 38487.930000,0.430232
Num Path Iterations: 3848793.000000
Num Path Iterations - Session Mean & Stddev: 38487.930000,0.430232
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3ec6800000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2237837219.000000
Throughput: 223783721.900000
Num Accesses - Session Mean & Stddev: 22378372.190000,290.441894
Num Phases Traversed: 3851799.000000
Num Phases Traversed - Session Mean & Stddev: 38517.990000,0.499900
Num Path Iterations: 3851799.000000
Num Path Iterations - Session Mean & Stddev: 38517.990000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc3ecc00000 of size 69632 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2234993805.000000
Throughput: 223499380.500000
Num Accesses - Session Mean & Stddev: 22349938.050000,264.658095
Num Phases Traversed: 3846905.000000
Num Phases Traversed - Session Mean & Stddev: 38469.050000,0.455522
Num Path Iterations: 3846905.000000
Num Path Iterations - Session Mean & Stddev: 38469.050000,0.455522
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcf45200000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2234811371.000000
Throughput: 223481137.100000
Num Accesses - Session Mean & Stddev: 22348113.710000,273.687387
Num Phases Traversed: 3846591.000000
Num Phases Traversed - Session Mean & Stddev: 38465.910000,0.471063
Num Path Iterations: 3846591.000000
Num Path Iterations - Session Mean & Stddev: 38465.910000,0.471063
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7908a00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2235258741.000000
Throughput: 223525874.100000
Num Accesses - Session Mean & Stddev: 22352587.410000,283.382713
Num Phases Traversed: 3847361.000000
Num Phases Traversed - Session Mean & Stddev: 38473.610000,0.487750
Num Path Iterations: 3847361.000000
Num Path Iterations - Session Mean & Stddev: 38473.610000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0 0/544,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0 0/544,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fded8200000 of size 69632 bytes.
[1.248s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2235682290.000000
Throughput: 223568229.000000
Num Accesses - Session Mean & Stddev: 22356822.900000,398.313532
Num Phases Traversed: 3848090.000000
Num Phases Traversed - Session Mean & Stddev: 38480.900000,0.685565
Num Path Iterations: 3848090.000000
Num Path Iterations - Session Mean & Stddev: 38480.900000,0.685565
+ set +x

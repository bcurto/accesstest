++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdf1d000000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2179504819.000000
Throughput: 217950481.900000
Num Accesses - Session Mean & Stddev: 21795048.190000,398.271156
Num Phases Traversed: 3751399.000000
Num Phases Traversed - Session Mean & Stddev: 37513.990000,0.685493
Num Path Iterations: 3751399.000000
Num Path Iterations - Session Mean & Stddev: 37513.990000,0.685493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff531e00000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2175053197.000000
Throughput: 217505319.700000
Num Accesses - Session Mean & Stddev: 21750531.970000,335.323082
Num Phases Traversed: 3743737.000000
Num Phases Traversed - Session Mean & Stddev: 37437.370000,0.577148
Num Path Iterations: 3743737.000000
Num Path Iterations - Session Mean & Stddev: 37437.370000,0.577148
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0071e00000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2180099182.000000
Throughput: 218009918.200000
Num Accesses - Session Mean & Stddev: 21800991.820000,372.746707
Num Phases Traversed: 3752422.000000
Num Phases Traversed - Session Mean & Stddev: 37524.220000,0.641561
Num Path Iterations: 3752422.000000
Num Path Iterations - Session Mean & Stddev: 37524.220000,0.641561
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe0eb800000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2175469193.000000
Throughput: 217546919.300000
Num Accesses - Session Mean & Stddev: 21754691.930000,289.976629
Num Phases Traversed: 3744453.000000
Num Phases Traversed - Session Mean & Stddev: 37444.530000,0.499099
Num Path Iterations: 3744453.000000
Num Path Iterations - Session Mean & Stddev: 37444.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe718200000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2183420759.000000
Throughput: 218342075.900000
Num Accesses - Session Mean & Stddev: 21834207.590000,327.583031
Num Phases Traversed: 3758139.000000
Num Phases Traversed - Session Mean & Stddev: 37581.390000,0.563826
Num Path Iterations: 3758139.000000
Num Path Iterations - Session Mean & Stddev: 37581.390000,0.563826
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fac76e00000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2180332744.000000
Throughput: 218033274.400000
Num Accesses - Session Mean & Stddev: 21803327.440000,377.605305
Num Phases Traversed: 3752824.000000
Num Phases Traversed - Session Mean & Stddev: 37528.240000,0.649923
Num Path Iterations: 3752824.000000
Num Path Iterations - Session Mean & Stddev: 37528.240000,0.649923
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe36a600000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2184904633.000000
Throughput: 218490463.300000
Num Accesses - Session Mean & Stddev: 21849046.330000,396.231777
Num Phases Traversed: 3760693.000000
Num Phases Traversed - Session Mean & Stddev: 37606.930000,0.681982
Num Path Iterations: 3760693.000000
Num Path Iterations - Session Mean & Stddev: 37606.930000,0.681982
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff14b800000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2180991598.000000
Throughput: 218099159.800000
Num Accesses - Session Mean & Stddev: 21809915.980000,309.406366
Num Phases Traversed: 3753958.000000
Num Phases Traversed - Session Mean & Stddev: 37539.580000,0.532541
Num Path Iterations: 3753958.000000
Num Path Iterations - Session Mean & Stddev: 37539.580000,0.532541
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fce54e00000 of size 69632 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2179453110.000000
Throughput: 217945311.000000
Num Accesses - Session Mean & Stddev: 21794531.100000,628.447587
Num Phases Traversed: 3751310.000000
Num Phases Traversed - Session Mean & Stddev: 37513.100000,1.081665
Num Path Iterations: 3751310.000000
Num Path Iterations - Session Mean & Stddev: 37513.100000,1.081665
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe583000000 of size 69632 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2183121544.000000
Throughput: 218312154.400000
Num Accesses - Session Mean & Stddev: 21831215.440000,368.557385
Num Phases Traversed: 3757624.000000
Num Phases Traversed - Session Mean & Stddev: 37576.240000,0.634350
Num Path Iterations: 3757624.000000
Num Path Iterations - Session Mean & Stddev: 37576.240000,0.634350
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f52be200000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2179023170.000000
Throughput: 217902317.000000
Num Accesses - Session Mean & Stddev: 21790231.700000,353.408503
Num Phases Traversed: 3750570.000000
Num Phases Traversed - Session Mean & Stddev: 37505.700000,0.608276
Num Path Iterations: 3750570.000000
Num Path Iterations - Session Mean & Stddev: 37505.700000,0.608276
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8170400000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2180591289.000000
Throughput: 218059128.900000
Num Accesses - Session Mean & Stddev: 21805912.890000,355.266179
Num Phases Traversed: 3753269.000000
Num Phases Traversed - Session Mean & Stddev: 37532.690000,0.611474
Num Path Iterations: 3753269.000000
Num Path Iterations - Session Mean & Stddev: 37532.690000,0.611474
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3fc6600000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2178167938.000000
Throughput: 217816793.800000
Num Accesses - Session Mean & Stddev: 21781679.380000,393.882007
Num Phases Traversed: 3749098.000000
Num Phases Traversed - Session Mean & Stddev: 37490.980000,0.677938
Num Path Iterations: 3749098.000000
Num Path Iterations - Session Mean & Stddev: 37490.980000,0.677938
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f45ac800000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2179186431.000000
Throughput: 217918643.100000
Num Accesses - Session Mean & Stddev: 21791864.310000,290.441894
Num Phases Traversed: 3750851.000000
Num Phases Traversed - Session Mean & Stddev: 37508.510000,0.499900
Num Path Iterations: 3750851.000000
Num Path Iterations - Session Mean & Stddev: 37508.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5d84800000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2178887216.000000
Throughput: 217888721.600000
Num Accesses - Session Mean & Stddev: 21788872.160000,313.093779
Num Phases Traversed: 3750336.000000
Num Phases Traversed - Session Mean & Stddev: 37503.360000,0.538888
Num Path Iterations: 3750336.000000
Num Path Iterations - Session Mean & Stddev: 37503.360000,0.538888
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3667200000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2180964291.000000
Throughput: 218096429.100000
Num Accesses - Session Mean & Stddev: 21809642.910000,393.152873
Num Phases Traversed: 3753911.000000
Num Phases Traversed - Session Mean & Stddev: 37539.110000,0.676683
Num Path Iterations: 3753911.000000
Num Path Iterations - Session Mean & Stddev: 37539.110000,0.676683
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc701400000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2182965255.000000
Throughput: 218296525.500000
Num Accesses - Session Mean & Stddev: 21829652.550000,289.043850
Num Phases Traversed: 3757355.000000
Num Phases Traversed - Session Mean & Stddev: 37573.550000,0.497494
Num Path Iterations: 3757355.000000
Num Path Iterations - Session Mean & Stddev: 37573.550000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f06ef800000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2176846744.000000
Throughput: 217684674.400000
Num Accesses - Session Mean & Stddev: 21768467.440000,286.050321
Num Phases Traversed: 3746824.000000
Num Phases Traversed - Session Mean & Stddev: 37468.240000,0.492341
Num Path Iterations: 3746824.000000
Num Path Iterations - Session Mean & Stddev: 37468.240000,0.492341
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3d30800000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2175297217.000000
Throughput: 217529721.700000
Num Accesses - Session Mean & Stddev: 21752972.170000,310.223534
Num Phases Traversed: 3744157.000000
Num Phases Traversed - Session Mean & Stddev: 37441.570000,0.533948
Num Path Iterations: 3744157.000000
Num Path Iterations - Session Mean & Stddev: 37441.570000,0.533948
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.1 0/544,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.1 0/544,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1369e00000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2179331681.000000
Throughput: 217933168.100000
Num Accesses - Session Mean & Stddev: 21793316.810000,353.360742
Num Phases Traversed: 3751101.000000
Num Phases Traversed - Session Mean & Stddev: 37511.010000,0.608194
Num Path Iterations: 3751101.000000
Num Path Iterations - Session Mean & Stddev: 37511.010000,0.608194
+ set +x

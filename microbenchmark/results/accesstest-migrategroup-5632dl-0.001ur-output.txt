++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbf34400000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072584774.000000
Throughput: 207258477.400000
Num Accesses - Session Mean & Stddev: 20725847.740000,1408.122421
Num Phases Traversed: 726558.000000
Num Phases Traversed - Session Mean & Stddev: 7265.580000,0.493559
Num Path Iterations: 726558.000000
Num Path Iterations - Session Mean & Stddev: 7265.580000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa1a6800000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072952811.000000
Throughput: 207295281.100000
Num Accesses - Session Mean & Stddev: 20729528.110000,1253.697929
Num Phases Traversed: 726687.000000
Num Phases Traversed - Session Mean & Stddev: 7266.870000,0.439431
Num Path Iterations: 726687.000000
Num Path Iterations - Session Mean & Stddev: 7266.870000,0.439431
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f617ee00000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072407888.000000
Throughput: 207240788.800000
Num Accesses - Session Mean & Stddev: 20724078.880000,1393.012127
Num Phases Traversed: 726496.000000
Num Phases Traversed - Session Mean & Stddev: 7264.960000,0.488262
Num Path Iterations: 726496.000000
Num Path Iterations - Session Mean & Stddev: 7264.960000,0.488262
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feefc200000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072504890.000000
Throughput: 207250489.000000
Num Accesses - Session Mean & Stddev: 20725048.900000,1307.408846
Num Phases Traversed: 726530.000000
Num Phases Traversed - Session Mean & Stddev: 7265.300000,0.458258
Num Path Iterations: 726530.000000
Num Path Iterations - Session Mean & Stddev: 7265.300000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2782a00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072778778.000000
Throughput: 207277877.800000
Num Accesses - Session Mean & Stddev: 20727787.780000,1693.634616
Num Phases Traversed: 726626.000000
Num Phases Traversed - Session Mean & Stddev: 7266.260000,0.593633
Num Path Iterations: 726626.000000
Num Path Iterations - Session Mean & Stddev: 7266.260000,0.593633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa42ac00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072165383.000000
Throughput: 207216538.300000
Num Accesses - Session Mean & Stddev: 20721653.830000,1799.652861
Num Phases Traversed: 726411.000000
Num Phases Traversed - Session Mean & Stddev: 7264.110000,0.630793
Num Path Iterations: 726411.000000
Num Path Iterations - Session Mean & Stddev: 7264.110000,0.630793
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8484400000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072339416.000000
Throughput: 207233941.600000
Num Accesses - Session Mean & Stddev: 20723394.160000,1459.219557
Num Phases Traversed: 726472.000000
Num Phases Traversed - Session Mean & Stddev: 7264.720000,0.511468
Num Path Iterations: 726472.000000
Num Path Iterations - Session Mean & Stddev: 7264.720000,0.511468
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcefaa00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2070524908.000000
Throughput: 207052490.800000
Num Accesses - Session Mean & Stddev: 20705249.080000,1639.916709
Num Phases Traversed: 725836.000000
Num Phases Traversed - Session Mean & Stddev: 7258.360000,0.574804
Num Path Iterations: 725836.000000
Num Path Iterations - Session Mean & Stddev: 7258.360000,0.574804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa238600000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2071997056.000000
Throughput: 207199705.600000
Num Accesses - Session Mean & Stddev: 20719970.560000,1425.358343
Num Phases Traversed: 726352.000000
Num Phases Traversed - Session Mean & Stddev: 7263.520000,0.499600
Num Path Iterations: 726352.000000
Num Path Iterations - Session Mean & Stddev: 7263.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f90d7e00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2071894348.000000
Throughput: 207189434.800000
Num Accesses - Session Mean & Stddev: 20718943.480000,1791.720344
Num Phases Traversed: 726316.000000
Num Phases Traversed - Session Mean & Stddev: 7263.160000,0.628013
Num Path Iterations: 726316.000000
Num Path Iterations - Session Mean & Stddev: 7263.160000,0.628013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe004800000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2070947152.000000
Throughput: 207094715.200000
Num Accesses - Session Mean & Stddev: 20709471.520000,1923.184424
Num Phases Traversed: 725984.000000
Num Phases Traversed - Session Mean & Stddev: 7259.840000,0.674092
Num Path Iterations: 725984.000000
Num Path Iterations - Session Mean & Stddev: 7259.840000,0.674092
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6181800000 of size 360448 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2071603342.000000
Throughput: 207160334.200000
Num Accesses - Session Mean & Stddev: 20716033.420000,1759.632707
Num Phases Traversed: 726214.000000
Num Phases Traversed - Session Mean & Stddev: 7262.140000,0.616766
Num Path Iterations: 726214.000000
Num Path Iterations - Session Mean & Stddev: 7262.140000,0.616766
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3e70800000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2070581968.000000
Throughput: 207058196.800000
Num Accesses - Session Mean & Stddev: 20705819.680000,1472.546039
Num Phases Traversed: 725856.000000
Num Phases Traversed - Session Mean & Stddev: 7258.560000,0.516140
Num Path Iterations: 725856.000000
Num Path Iterations - Session Mean & Stddev: 7258.560000,0.516140
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6bb9400000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2071141156.000000
Throughput: 207114115.600000
Num Accesses - Session Mean & Stddev: 20711411.560000,1481.363759
Num Phases Traversed: 726052.000000
Num Phases Traversed - Session Mean & Stddev: 7260.520000,0.519230
Num Path Iterations: 726052.000000
Num Path Iterations - Session Mean & Stddev: 7260.520000,0.519230
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7c93c00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2068935787.000000
Throughput: 206893578.700000
Num Accesses - Session Mean & Stddev: 20689357.870000,1817.654338
Num Phases Traversed: 725279.000000
Num Phases Traversed - Session Mean & Stddev: 7252.790000,0.637103
Num Path Iterations: 725279.000000
Num Path Iterations - Session Mean & Stddev: 7252.790000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7261a00000 of size 360448 bytes.
[1.275s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2070296668.000000
Throughput: 207029666.800000
Num Accesses - Session Mean & Stddev: 20702966.680000,1416.191956
Num Phases Traversed: 725756.000000
Num Phases Traversed - Session Mean & Stddev: 7257.560000,0.496387
Num Path Iterations: 725756.000000
Num Path Iterations - Session Mean & Stddev: 7257.560000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fad29200000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072382211.000000
Throughput: 207238221.100000
Num Accesses - Session Mean & Stddev: 20723822.110000,1742.666956
Num Phases Traversed: 726487.000000
Num Phases Traversed - Session Mean & Stddev: 7264.870000,0.610819
Num Path Iterations: 726487.000000
Num Path Iterations - Session Mean & Stddev: 7264.870000,0.610819
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc5caa00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072042704.000000
Throughput: 207204270.400000
Num Accesses - Session Mean & Stddev: 20720427.040000,1448.020469
Num Phases Traversed: 726368.000000
Num Phases Traversed - Session Mean & Stddev: 7263.680000,0.507543
Num Path Iterations: 726368.000000
Num Path Iterations - Session Mean & Stddev: 7263.680000,0.507543
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc0c9000000 of size 360448 bytes.
[1.244s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2072236708.000000
Throughput: 207223670.800000
Num Accesses - Session Mean & Stddev: 20722367.080000,1483.560000
Num Phases Traversed: 726436.000000
Num Phases Traversed - Session Mean & Stddev: 7264.360000,0.520000
Num Path Iterations: 726436.000000
Num Path Iterations - Session Mean & Stddev: 7264.360000,0.520000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.001 0/2816,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.001 0/2816,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f970de00000 of size 360448 bytes.
[1.244s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2070952858.000000
Throughput: 207095285.800000
Num Accesses - Session Mean & Stddev: 20709528.580000,1805.297605
Num Phases Traversed: 725986.000000
Num Phases Traversed - Session Mean & Stddev: 7259.860000,0.632772
Num Path Iterations: 725986.000000
Num Path Iterations - Session Mean & Stddev: 7259.860000,0.632772
+ set +x

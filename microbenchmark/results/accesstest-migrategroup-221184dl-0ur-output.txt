++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f47c0000000 of size 14155776 bytes.
[2.689s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 543520377.000000
Throughput: 54352037.700000
Num Accesses - Session Mean & Stddev: 5435203.770000,37204.912646
Num Phases Traversed: 5013.000000
Num Phases Traversed - Session Mean & Stddev: 50.130000,0.336303
Num Path Iterations: 5013.000000
Num Path Iterations - Session Mean & Stddev: 50.130000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6280000000 of size 14155776 bytes.
[2.658s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 544405409.000000
Throughput: 54440540.900000
Num Accesses - Session Mean & Stddev: 5444054.090000,45060.102961
Num Phases Traversed: 5021.000000
Num Phases Traversed - Session Mean & Stddev: 50.210000,0.407308
Num Path Iterations: 5021.000000
Num Path Iterations - Session Mean & Stddev: 50.210000,0.407308
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7080000000 of size 14155776 bytes.
[2.642s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 545401070.000000
Throughput: 54540107.000000
Num Accesses - Session Mean & Stddev: 5454010.700000,50696.576656
Num Phases Traversed: 5030.000000
Num Phases Traversed - Session Mean & Stddev: 50.300000,0.458258
Num Path Iterations: 5030.000000
Num Path Iterations - Session Mean & Stddev: 50.300000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe480000000 of size 14155776 bytes.
[2.754s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 545732957.000000
Throughput: 54573295.700000
Num Accesses - Session Mean & Stddev: 5457329.570000,52019.162760
Num Phases Traversed: 5033.000000
Num Phases Traversed - Session Mean & Stddev: 50.330000,0.470213
Num Path Iterations: 5033.000000
Num Path Iterations - Session Mean & Stddev: 50.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2d80000000 of size 14155776 bytes.
[2.697s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 545511699.000000
Throughput: 54551169.900000
Num Accesses - Session Mean & Stddev: 5455116.990000,51165.165001
Num Phases Traversed: 5031.000000
Num Phases Traversed - Session Mean & Stddev: 50.310000,0.462493
Num Path Iterations: 5031.000000
Num Path Iterations - Session Mean & Stddev: 50.310000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe400000000 of size 14155776 bytes.
[2.722s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 545732957.000000
Throughput: 54573295.700000
Num Accesses - Session Mean & Stddev: 5457329.570000,52019.162760
Num Phases Traversed: 5033.000000
Num Phases Traversed - Session Mean & Stddev: 50.330000,0.470213
Num Path Iterations: 5033.000000
Num Path Iterations - Session Mean & Stddev: 50.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fda00000000 of size 14155776 bytes.
[2.717s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 545401070.000000
Throughput: 54540107.000000
Num Accesses - Session Mean & Stddev: 5454010.700000,50696.576656
Num Phases Traversed: 5030.000000
Num Phases Traversed - Session Mean & Stddev: 50.300000,0.458258
Num Path Iterations: 5030.000000
Num Path Iterations - Session Mean & Stddev: 50.300000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4fc0000000 of size 14155776 bytes.
[2.681s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 546507360.000000
Throughput: 54650736.000000
Num Accesses - Session Mean & Stddev: 5465073.600000,54196.920151
Num Phases Traversed: 5040.000000
Num Phases Traversed - Session Mean & Stddev: 50.400000,0.489898
Num Path Iterations: 5040.000000
Num Path Iterations - Session Mean & Stddev: 50.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7a40000000 of size 14155776 bytes.
[2.723s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 545843586.000000
Throughput: 54584358.600000
Num Accesses - Session Mean & Stddev: 5458435.860000,52405.927659
Num Phases Traversed: 5034.000000
Num Phases Traversed - Session Mean & Stddev: 50.340000,0.473709
Num Path Iterations: 5034.000000
Num Path Iterations - Session Mean & Stddev: 50.340000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f32c0000000 of size 14155776 bytes.
[2.666s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 545843586.000000
Throughput: 54584358.600000
Num Accesses - Session Mean & Stddev: 5458435.860000,52405.927659
Num Phases Traversed: 5034.000000
Num Phases Traversed - Session Mean & Stddev: 50.340000,0.473709
Num Path Iterations: 5034.000000
Num Path Iterations - Session Mean & Stddev: 50.340000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f58c0000000 of size 14155776 bytes.
[2.721s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 545401070.000000
Throughput: 54540107.000000
Num Accesses - Session Mean & Stddev: 5454010.700000,50696.576656
Num Phases Traversed: 5030.000000
Num Phases Traversed - Session Mean & Stddev: 50.300000,0.458258
Num Path Iterations: 5030.000000
Num Path Iterations - Session Mean & Stddev: 50.300000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff6c0000000 of size 14155776 bytes.
[2.696s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 545290441.000000
Throughput: 54529044.100000
Num Accesses - Session Mean & Stddev: 5452904.410000,50199.242071
Num Phases Traversed: 5029.000000
Num Phases Traversed - Session Mean & Stddev: 50.290000,0.453762
Num Path Iterations: 5029.000000
Num Path Iterations - Session Mean & Stddev: 50.290000,0.453762
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbdc0000000 of size 14155776 bytes.
[2.700s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 546064844.000000
Throughput: 54606484.400000
Num Accesses - Session Mean & Stddev: 5460648.440000,53101.920000
Num Phases Traversed: 5036.000000
Num Phases Traversed - Session Mean & Stddev: 50.360000,0.480000
Num Path Iterations: 5036.000000
Num Path Iterations - Session Mean & Stddev: 50.360000,0.480000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f79c0000000 of size 14155776 bytes.
[2.695s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 544516038.000000
Throughput: 54451603.800000
Num Accesses - Session Mean & Stddev: 5445160.380000,45827.654315
Num Phases Traversed: 5022.000000
Num Phases Traversed - Session Mean & Stddev: 50.220000,0.414246
Num Path Iterations: 5022.000000
Num Path Iterations - Session Mean & Stddev: 50.220000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc340000000 of size 14155776 bytes.
[2.678s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 545622328.000000
Throughput: 54562232.800000
Num Accesses - Session Mean & Stddev: 5456223.280000,51605.790174
Num Phases Traversed: 5032.000000
Num Phases Traversed - Session Mean & Stddev: 50.320000,0.466476
Num Path Iterations: 5032.000000
Num Path Iterations - Session Mean & Stddev: 50.320000,0.466476
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9e00000000 of size 14155776 bytes.
[2.723s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 546064844.000000
Throughput: 54606484.400000
Num Accesses - Session Mean & Stddev: 5460648.440000,53101.920000
Num Phases Traversed: 5036.000000
Num Phases Traversed - Session Mean & Stddev: 50.360000,0.480000
Num Path Iterations: 5036.000000
Num Path Iterations - Session Mean & Stddev: 50.360000,0.480000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6100000000 of size 14155776 bytes.
[2.700s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 545732957.000000
Throughput: 54573295.700000
Num Accesses - Session Mean & Stddev: 5457329.570000,52019.162760
Num Phases Traversed: 5033.000000
Num Phases Traversed - Session Mean & Stddev: 50.330000,0.470213
Num Path Iterations: 5033.000000
Num Path Iterations - Session Mean & Stddev: 50.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5f00000000 of size 14155776 bytes.
[2.650s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 545954215.000000
Throughput: 54595421.500000
Num Accesses - Session Mean & Stddev: 5459542.150000,52766.669957
Num Phases Traversed: 5035.000000
Num Phases Traversed - Session Mean & Stddev: 50.350000,0.476970
Num Path Iterations: 5035.000000
Num Path Iterations - Session Mean & Stddev: 50.350000,0.476970
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1e00000000 of size 14155776 bytes.
[2.688s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 546175473.000000
Throughput: 54617547.300000
Num Accesses - Session Mean & Stddev: 5461754.730000,53412.157810
Num Phases Traversed: 5037.000000
Num Phases Traversed - Session Mean & Stddev: 50.370000,0.482804
Num Path Iterations: 5037.000000
Num Path Iterations - Session Mean & Stddev: 50.370000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fba40000000 of size 14155776 bytes.
[2.640s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 541971571.000000
Throughput: 54197157.100000
Num Accesses - Session Mean & Stddev: 5419715.710000,11007.446518
Num Phases Traversed: 4999.000000
Num Phases Traversed - Session Mean & Stddev: 49.990000,0.099499
Num Path Iterations: 4999.000000
Num Path Iterations - Session Mean & Stddev: 49.990000,0.099499
+ set +x

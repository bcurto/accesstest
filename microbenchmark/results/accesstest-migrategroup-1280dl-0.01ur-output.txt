++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f26a8c00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2029919608.000000
Throughput: 202991960.800000
Num Accesses - Session Mean & Stddev: 20299196.080000,163.604442
Num Phases Traversed: 2998504.000000
Num Phases Traversed - Session Mean & Stddev: 29985.040000,0.241661
Num Path Iterations: 2998504.000000
Num Path Iterations - Session Mean & Stddev: 29985.040000,0.241661
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f658d600000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2027489855.000000
Throughput: 202748985.500000
Num Accesses - Session Mean & Stddev: 20274898.550000,241.737352
Num Phases Traversed: 2994915.000000
Num Phases Traversed - Session Mean & Stddev: 29949.150000,0.357071
Num Path Iterations: 2994915.000000
Num Path Iterations - Session Mean & Stddev: 29949.150000,0.357071
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdb98c00000 of size 81920 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2029080805.000000
Throughput: 202908080.500000
Num Accesses - Session Mean & Stddev: 20290808.050000,322.908420
Num Phases Traversed: 2997265.000000
Num Phases Traversed - Session Mean & Stddev: 29972.650000,0.476970
Num Path Iterations: 2997265.000000
Num Path Iterations - Session Mean & Stddev: 29972.650000,0.476970
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbe15e00000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2026581321.000000
Throughput: 202658132.100000
Num Accesses - Session Mean & Stddev: 20265813.210000,300.560553
Num Phases Traversed: 2993573.000000
Num Phases Traversed - Session Mean & Stddev: 29935.730000,0.443959
Num Path Iterations: 2993573.000000
Num Path Iterations - Session Mean & Stddev: 29935.730000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f69f9c00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2027742376.000000
Throughput: 202774237.600000
Num Accesses - Session Mean & Stddev: 20277423.760000,219.998960
Num Phases Traversed: 2995288.000000
Num Phases Traversed - Session Mean & Stddev: 29952.880000,0.324962
Num Path Iterations: 2995288.000000
Num Path Iterations - Session Mean & Stddev: 29952.880000,0.324962
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff79be00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2027413354.000000
Throughput: 202741335.400000
Num Accesses - Session Mean & Stddev: 20274133.540000,191.005205
Num Phases Traversed: 2994802.000000
Num Phases Traversed - Session Mean & Stddev: 29948.020000,0.282135
Num Path Iterations: 2994802.000000
Num Path Iterations - Session Mean & Stddev: 29948.020000,0.282135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe3b7600000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2030134217.000000
Throughput: 203013421.700000
Num Accesses - Session Mean & Stddev: 20301342.170000,275.747677
Num Phases Traversed: 2998821.000000
Num Phases Traversed - Session Mean & Stddev: 29988.210000,0.407308
Num Path Iterations: 2998821.000000
Num Path Iterations - Session Mean & Stddev: 29988.210000,0.407308
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7b8e000000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2026567781.000000
Throughput: 202656778.100000
Num Accesses - Session Mean & Stddev: 20265677.810000,337.890151
Num Phases Traversed: 2993553.000000
Num Phases Traversed - Session Mean & Stddev: 29935.530000,0.499099
Num Path Iterations: 2993553.000000
Num Path Iterations - Session Mean & Stddev: 29935.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3438a00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2029366499.000000
Throughput: 202936649.900000
Num Accesses - Session Mean & Stddev: 20293664.990000,227.677425
Num Phases Traversed: 2997687.000000
Num Phases Traversed - Session Mean & Stddev: 29976.870000,0.336303
Num Path Iterations: 2997687.000000
Num Path Iterations - Session Mean & Stddev: 29976.870000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1050200000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2029927732.000000
Throughput: 202992773.200000
Num Accesses - Session Mean & Stddev: 20299277.320000,248.192300
Num Phases Traversed: 2998516.000000
Num Phases Traversed - Session Mean & Stddev: 29985.160000,0.366606
Num Path Iterations: 2998516.000000
Num Path Iterations - Session Mean & Stddev: 29985.160000,0.366606
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1f95200000 of size 81920 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2030464593.000000
Throughput: 203046459.300000
Num Accesses - Session Mean & Stddev: 20304645.930000,193.745052
Num Phases Traversed: 2999309.000000
Num Phases Traversed - Session Mean & Stddev: 29993.090000,0.286182
Num Path Iterations: 2999309.000000
Num Path Iterations - Session Mean & Stddev: 29993.090000,0.286182
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efceac00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2028332720.000000
Throughput: 202833272.000000
Num Accesses - Session Mean & Stddev: 20283327.200000,331.660911
Num Phases Traversed: 2996160.000000
Num Phases Traversed - Session Mean & Stddev: 29961.600000,0.489898
Num Path Iterations: 2996160.000000
Num Path Iterations - Session Mean & Stddev: 29961.600000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0c0a800000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2028287361.000000
Throughput: 202828736.100000
Num Accesses - Session Mean & Stddev: 20282873.610000,219.477511
Num Phases Traversed: 2996093.000000
Num Phases Traversed - Session Mean & Stddev: 29960.930000,0.324191
Num Path Iterations: 2996093.000000
Num Path Iterations - Session Mean & Stddev: 29960.930000,0.324191
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fae00800000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2023158409.000000
Throughput: 202315840.900000
Num Accesses - Session Mean & Stddev: 20231584.090000,254.303405
Num Phases Traversed: 2988517.000000
Num Phases Traversed - Session Mean & Stddev: 29885.170000,0.375633
Num Path Iterations: 2988517.000000
Num Path Iterations - Session Mean & Stddev: 29885.170000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f35d0200000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2027549431.000000
Throughput: 202754943.100000
Num Accesses - Session Mean & Stddev: 20275494.310000,115.487549
Num Phases Traversed: 2995003.000000
Num Phases Traversed - Session Mean & Stddev: 29950.030000,0.170587
Num Path Iterations: 2995003.000000
Num Path Iterations - Session Mean & Stddev: 29950.030000,0.170587
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f25d2800000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2027829709.000000
Throughput: 202782970.900000
Num Accesses - Session Mean & Stddev: 20278297.090000,254.303405
Num Phases Traversed: 2995417.000000
Num Phases Traversed - Session Mean & Stddev: 29954.170000,0.375633
Num Path Iterations: 2995417.000000
Num Path Iterations - Session Mean & Stddev: 29954.170000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7000e00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2027173696.000000
Throughput: 202717369.600000
Num Accesses - Session Mean & Stddev: 20271736.960000,338.229092
Num Phases Traversed: 2994448.000000
Num Phases Traversed - Session Mean & Stddev: 29944.480000,0.499600
Num Path Iterations: 2994448.000000
Num Path Iterations - Session Mean & Stddev: 29944.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcee4a00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2029551997.000000
Throughput: 202955199.700000
Num Accesses - Session Mean & Stddev: 20295519.970000,330.206707
Num Phases Traversed: 2997961.000000
Num Phases Traversed - Session Mean & Stddev: 29979.610000,0.487750
Num Path Iterations: 2997961.000000
Num Path Iterations - Session Mean & Stddev: 29979.610000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe12fa00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2027778257.000000
Throughput: 202777825.700000
Num Accesses - Session Mean & Stddev: 20277782.570000,332.971148
Num Phases Traversed: 2995341.000000
Num Phases Traversed - Session Mean & Stddev: 29953.410000,0.491833
Num Path Iterations: 2995341.000000
Num Path Iterations - Session Mean & Stddev: 29953.410000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f00dfc00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2022523383.000000
Throughput: 202252338.300000
Num Accesses - Session Mean & Stddev: 20225233.830000,275.747677
Num Phases Traversed: 2987579.000000
Num Phases Traversed - Session Mean & Stddev: 29875.790000,0.407308
Num Path Iterations: 2987579.000000
Num Path Iterations - Session Mean & Stddev: 29875.790000,0.407308
+ set +x

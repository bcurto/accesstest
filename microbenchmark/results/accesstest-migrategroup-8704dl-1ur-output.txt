++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa6efe00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1327483873.000000
Throughput: 132748387.300000
Num Accesses - Session Mean & Stddev: 13274838.730000,2424.301804
Num Phases Traversed: 302557.000000
Num Phases Traversed - Session Mean & Stddev: 3025.570000,0.552359
Num Path Iterations: 302557.000000
Num Path Iterations - Session Mean & Stddev: 3025.570000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc8ba800000 of size 557056 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1336819276.000000
Throughput: 133681927.600000
Num Accesses - Session Mean & Stddev: 13368192.760000,2958.589708
Num Phases Traversed: 304684.000000
Num Phases Traversed - Session Mean & Stddev: 3046.840000,0.674092
Num Path Iterations: 304684.000000
Num Path Iterations - Session Mean & Stddev: 3046.840000,0.674092
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6553200000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1326588517.000000
Throughput: 132658851.700000
Num Accesses - Session Mean & Stddev: 13265885.170000,2190.546338
Num Phases Traversed: 302353.000000
Num Phases Traversed - Session Mean & Stddev: 3023.530000,0.499099
Num Path Iterations: 302353.000000
Num Path Iterations - Session Mean & Stddev: 3023.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1113e00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1334629165.000000
Throughput: 133462916.500000
Num Accesses - Session Mean & Stddev: 13346291.650000,3000.933616
Num Phases Traversed: 304185.000000
Num Phases Traversed - Session Mean & Stddev: 3041.850000,0.683740
Num Path Iterations: 304185.000000
Num Path Iterations - Session Mean & Stddev: 3041.850000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdd47e00000 of size 557056 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1333799644.000000
Throughput: 133379964.400000
Num Accesses - Session Mean & Stddev: 13337996.440000,2971.583138
Num Phases Traversed: 303996.000000
Num Phases Traversed - Session Mean & Stddev: 3039.960000,0.677052
Num Path Iterations: 303996.000000
Num Path Iterations - Session Mean & Stddev: 3039.960000,0.677052
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb2c4e00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1335875641.000000
Throughput: 133587564.100000
Num Accesses - Session Mean & Stddev: 13358756.410000,2754.600178
Num Phases Traversed: 304469.000000
Num Phases Traversed - Session Mean & Stddev: 3044.690000,0.627615
Num Path Iterations: 304469.000000
Num Path Iterations - Session Mean & Stddev: 3044.690000,0.627615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcc33800000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1332974512.000000
Throughput: 133297451.200000
Num Accesses - Session Mean & Stddev: 13329745.120000,2955.984169
Num Phases Traversed: 303808.000000
Num Phases Traversed - Session Mean & Stddev: 3038.080000,0.673498
Num Path Iterations: 303808.000000
Num Path Iterations - Session Mean & Stddev: 3038.080000,0.673498
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe036e00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1329766153.000000
Throughput: 132976615.300000
Num Accesses - Session Mean & Stddev: 13297661.530000,2901.723214
Num Phases Traversed: 303077.000000
Num Phases Traversed - Session Mean & Stddev: 3030.770000,0.661135
Num Path Iterations: 303077.000000
Num Path Iterations - Session Mean & Stddev: 3030.770000,0.661135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f63f6200000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1332395164.000000
Throughput: 133239516.400000
Num Accesses - Session Mean & Stddev: 13323951.640000,2852.512365
Num Phases Traversed: 303676.000000
Num Phases Traversed - Session Mean & Stddev: 3036.760000,0.649923
Num Path Iterations: 303676.000000
Num Path Iterations - Session Mean & Stddev: 3036.760000,0.649923
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f61aac00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1332798952.000000
Throughput: 133279895.200000
Num Accesses - Session Mean & Stddev: 13327989.520000,2697.003895
Num Phases Traversed: 303768.000000
Num Phases Traversed - Session Mean & Stddev: 3037.680000,0.614492
Num Path Iterations: 303768.000000
Num Path Iterations - Session Mean & Stddev: 3037.680000,0.614492
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe435000000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1329810043.000000
Throughput: 132981004.300000
Num Accesses - Session Mean & Stddev: 13298100.430000,3018.853618
Num Phases Traversed: 303087.000000
Num Phases Traversed - Session Mean & Stddev: 3030.870000,0.687823
Num Path Iterations: 303087.000000
Num Path Iterations - Session Mean & Stddev: 3030.870000,0.687823
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9b8d200000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1333035958.000000
Throughput: 133303595.800000
Num Accesses - Session Mean & Stddev: 13330359.580000,2883.409326
Num Phases Traversed: 303822.000000
Num Phases Traversed - Session Mean & Stddev: 3038.220000,0.656963
Num Path Iterations: 303822.000000
Num Path Iterations - Session Mean & Stddev: 3038.220000,0.656963
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9b32e00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1332013321.000000
Throughput: 133201332.100000
Num Accesses - Session Mean & Stddev: 13320133.210000,1507.032032
Num Phases Traversed: 303589.000000
Num Phases Traversed - Session Mean & Stddev: 3035.890000,0.343366
Num Path Iterations: 303589.000000
Num Path Iterations - Session Mean & Stddev: 3035.890000,0.343366
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5d3f600000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1332579502.000000
Throughput: 133257950.200000
Num Accesses - Session Mean & Stddev: 13325795.020000,2936.368928
Num Phases Traversed: 303718.000000
Num Phases Traversed - Session Mean & Stddev: 3037.180000,0.669029
Num Path Iterations: 303718.000000
Num Path Iterations - Session Mean & Stddev: 3037.180000,0.669029
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3d2e200000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1329954880.000000
Throughput: 132995488.000000
Num Accesses - Session Mean & Stddev: 13299548.800000,2911.333241
Num Phases Traversed: 303120.000000
Num Phases Traversed - Session Mean & Stddev: 3031.200000,0.663325
Num Path Iterations: 303120.000000
Num Path Iterations - Session Mean & Stddev: 3031.200000,0.663325
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f62acc00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1333369522.000000
Throughput: 133336952.200000
Num Accesses - Session Mean & Stddev: 13333695.220000,3039.521139
Num Phases Traversed: 303898.000000
Num Phases Traversed - Session Mean & Stddev: 3038.980000,0.692532
Num Path Iterations: 303898.000000
Num Path Iterations - Session Mean & Stddev: 3038.980000,0.692532
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5ce9600000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1332022099.000000
Throughput: 133202209.900000
Num Accesses - Session Mean & Stddev: 13320220.990000,3046.800681
Num Phases Traversed: 303591.000000
Num Phases Traversed - Session Mean & Stddev: 3035.910000,0.694190
Num Path Iterations: 303591.000000
Num Path Iterations - Session Mean & Stddev: 3035.910000,0.694190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f74ed200000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1332987679.000000
Throughput: 133298767.900000
Num Accesses - Session Mean & Stddev: 13329876.790000,3034.129382
Num Phases Traversed: 303811.000000
Num Phases Traversed - Session Mean & Stddev: 3038.110000,0.691303
Num Path Iterations: 303811.000000
Num Path Iterations - Session Mean & Stddev: 3038.110000,0.691303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbf48600000 of size 557056 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1329818821.000000
Throughput: 132981882.100000
Num Accesses - Session Mean & Stddev: 13298188.210000,2140.734469
Num Phases Traversed: 303089.000000
Num Phases Traversed - Session Mean & Stddev: 3030.890000,0.487750
Num Path Iterations: 303089.000000
Num Path Iterations - Session Mean & Stddev: 3030.890000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,1 0/4352,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,1 0/4352,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fef98800000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1326724576.000000
Throughput: 132672457.600000
Num Accesses - Session Mean & Stddev: 13267245.760000,2958.589708
Num Phases Traversed: 302384.000000
Num Phases Traversed - Session Mean & Stddev: 3023.840000,0.674092
Num Path Iterations: 302384.000000
Num Path Iterations - Session Mean & Stddev: 3023.840000,0.674092
+ set +x

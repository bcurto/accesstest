++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f61c0c00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2586568161.000000
Throughput: 258656816.100000
Num Accesses - Session Mean & Stddev: 25865681.610000,310.673555
Num Phases Traversed: 5003133.000000
Num Phases Traversed - Session Mean & Stddev: 50031.330000,0.600916
Num Path Iterations: 5003133.000000
Num Path Iterations - Session Mean & Stddev: 50031.330000,0.600916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4bee200000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2586956945.000000
Throughput: 258695694.500000
Num Accesses - Session Mean & Stddev: 25869569.450000,345.849429
Num Phases Traversed: 5003885.000000
Num Phases Traversed - Session Mean & Stddev: 50038.850000,0.668954
Num Path Iterations: 5003885.000000
Num Path Iterations - Session Mean & Stddev: 50038.850000,0.668954
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5a93e00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2584391074.000000
Throughput: 258439107.400000
Num Accesses - Session Mean & Stddev: 25843910.740000,331.686829
Num Phases Traversed: 4998922.000000
Num Phases Traversed - Session Mean & Stddev: 49989.220000,0.641561
Num Path Iterations: 4998922.000000
Num Path Iterations - Session Mean & Stddev: 49989.220000,0.641561
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7a7a400000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2586244519.000000
Throughput: 258624451.900000
Num Accesses - Session Mean & Stddev: 25862445.190000,352.584903
Num Phases Traversed: 5002507.000000
Num Phases Traversed - Session Mean & Stddev: 50025.070000,0.681982
Num Path Iterations: 5002507.000000
Num Path Iterations - Session Mean & Stddev: 50025.070000,0.681982
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fef23a00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2588892593.000000
Throughput: 258889259.300000
Num Accesses - Session Mean & Stddev: 25888925.930000,321.164794
Num Phases Traversed: 5007629.000000
Num Phases Traversed - Session Mean & Stddev: 50076.290000,0.621208
Num Path Iterations: 5007629.000000
Num Path Iterations - Session Mean & Stddev: 50076.290000,0.621208
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f06e7e00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2584701791.000000
Throughput: 258470179.100000
Num Accesses - Session Mean & Stddev: 25847017.910000,333.895555
Num Phases Traversed: 4999523.000000
Num Phases Traversed - Session Mean & Stddev: 49995.230000,0.645833
Num Path Iterations: 4999523.000000
Num Path Iterations - Session Mean & Stddev: 49995.230000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f268f800000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2586828729.000000
Throughput: 258682872.900000
Num Accesses - Session Mean & Stddev: 25868287.290000,298.385599
Num Phases Traversed: 5003637.000000
Num Phases Traversed - Session Mean & Stddev: 50036.370000,0.577148
Num Path Iterations: 5003637.000000
Num Path Iterations - Session Mean & Stddev: 50036.370000,0.577148
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1ef3400000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2586325171.000000
Throughput: 258632517.100000
Num Accesses - Session Mean & Stddev: 25863251.710000,298.385599
Num Phases Traversed: 5002663.000000
Num Phases Traversed - Session Mean & Stddev: 50026.630000,0.577148
Num Path Iterations: 5002663.000000
Num Path Iterations - Session Mean & Stddev: 50026.630000,0.577148
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc92da00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2586194887.000000
Throughput: 258619488.700000
Num Accesses - Session Mean & Stddev: 25861948.870000,349.845156
Num Phases Traversed: 5002411.000000
Num Phases Traversed - Session Mean & Stddev: 50024.110000,0.676683
Num Path Iterations: 5002411.000000
Num Path Iterations - Session Mean & Stddev: 50024.110000,0.676683
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb802600000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2586993135.000000
Throughput: 258699313.500000
Num Accesses - Session Mean & Stddev: 25869931.350000,267.394479
Num Phases Traversed: 5003955.000000
Num Phases Traversed - Session Mean & Stddev: 50039.550000,0.517204
Num Path Iterations: 5003955.000000
Num Path Iterations - Session Mean & Stddev: 50039.550000,0.517204
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc5f7800000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2585571385.000000
Throughput: 258557138.500000
Num Accesses - Session Mean & Stddev: 25855713.850000,353.493433
Num Phases Traversed: 5001205.000000
Num Phases Traversed - Session Mean & Stddev: 50012.050000,0.683740
Num Path Iterations: 5001205.000000
Num Path Iterations - Session Mean & Stddev: 50012.050000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc8fc200000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2585718730.000000
Throughput: 258571873.000000
Num Accesses - Session Mean & Stddev: 25857187.300000,346.814143
Num Phases Traversed: 5001490.000000
Num Phases Traversed - Session Mean & Stddev: 50014.900000,0.670820
Num Path Iterations: 5001490.000000
Num Path Iterations - Session Mean & Stddev: 50014.900000,0.670820
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f73adc00000 of size 61440 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2586672595.000000
Throughput: 258667259.500000
Num Accesses - Session Mean & Stddev: 25866725.950000,304.767005
Num Phases Traversed: 5003335.000000
Num Phases Traversed - Session Mean & Stddev: 50033.350000,0.589491
Num Path Iterations: 5003335.000000
Num Path Iterations - Session Mean & Stddev: 50033.350000,0.589491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7d7de00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2585200696.000000
Throughput: 258520069.600000
Num Accesses - Session Mean & Stddev: 25852006.960000,352.774373
Num Phases Traversed: 5000488.000000
Num Phases Traversed - Session Mean & Stddev: 50004.880000,0.682349
Num Path Iterations: 5000488.000000
Num Path Iterations - Session Mean & Stddev: 50004.880000,0.682349
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1d12400000 of size 61440 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2583026711.000000
Throughput: 258302671.100000
Num Accesses - Session Mean & Stddev: 25830267.110000,343.367409
Num Phases Traversed: 4996283.000000
Num Phases Traversed - Session Mean & Stddev: 49962.830000,0.664154
Num Path Iterations: 4996283.000000
Num Path Iterations - Session Mean & Stddev: 49962.830000,0.664154
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7de1200000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2584797436.000000
Throughput: 258479743.600000
Num Accesses - Session Mean & Stddev: 25847974.360000,348.198636
Num Phases Traversed: 4999708.000000
Num Phases Traversed - Session Mean & Stddev: 49997.080000,0.673498
Num Path Iterations: 4999708.000000
Num Path Iterations - Session Mean & Stddev: 49997.080000,0.673498
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f056d400000 of size 61440 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2585443169.000000
Throughput: 258544316.900000
Num Accesses - Session Mean & Stddev: 25854431.690000,276.050890
Num Phases Traversed: 5000957.000000
Num Phases Traversed - Session Mean & Stddev: 50009.570000,0.533948
Num Path Iterations: 5000957.000000
Num Path Iterations - Session Mean & Stddev: 50009.570000,0.533948
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f580ec00000 of size 61440 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2583264531.000000
Throughput: 258326453.100000
Num Accesses - Session Mean & Stddev: 25832645.310000,285.569385
Num Phases Traversed: 4996743.000000
Num Phases Traversed - Session Mean & Stddev: 49967.430000,0.552359
Num Path Iterations: 4996743.000000
Num Path Iterations - Session Mean & Stddev: 49967.430000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f76b1400000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2585094194.000000
Throughput: 258509419.400000
Num Accesses - Session Mean & Stddev: 25850941.940000,338.072147
Num Phases Traversed: 5000282.000000
Num Phases Traversed - Session Mean & Stddev: 50002.820000,0.653911
Num Path Iterations: 5000282.000000
Num Path Iterations - Session Mean & Stddev: 50002.820000,0.653911
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efe63000000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2585533127.000000
Throughput: 258553312.700000
Num Accesses - Session Mean & Stddev: 25855331.270000,316.131867
Num Phases Traversed: 5001131.000000
Num Phases Traversed - Session Mean & Stddev: 50011.310000,0.611474
Num Path Iterations: 5001131.000000
Num Path Iterations - Session Mean & Stddev: 50011.310000,0.611474
+ set +x

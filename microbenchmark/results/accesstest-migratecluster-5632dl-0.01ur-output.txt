++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f10bfe00000 of size 360448 bytes.
[1.245s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2116272763.000000
Throughput: 211627276.300000
Num Accesses - Session Mean & Stddev: 21162727.630000,1294.583135
Num Phases Traversed: 741871.000000
Num Phases Traversed - Session Mean & Stddev: 7418.710000,0.453762
Num Path Iterations: 741871.000000
Num Path Iterations - Session Mean & Stddev: 7418.710000,0.453762
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0437c00000 of size 360448 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2116050229.000000
Throughput: 211605022.900000
Num Accesses - Session Mean & Stddev: 21160502.290000,1987.089411
Num Phases Traversed: 741793.000000
Num Phases Traversed - Session Mean & Stddev: 7417.930000,0.696491
Num Path Iterations: 741793.000000
Num Path Iterations - Session Mean & Stddev: 7417.930000,0.696491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa5e5000000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2115844813.000000
Throughput: 211584481.300000
Num Accesses - Session Mean & Stddev: 21158448.130000,1905.111979
Num Phases Traversed: 741721.000000
Num Phases Traversed - Session Mean & Stddev: 7417.210000,0.667757
Num Path Iterations: 741721.000000
Num Path Iterations - Session Mean & Stddev: 7417.210000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8945600000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2105631073.000000
Throughput: 210563107.300000
Num Accesses - Session Mean & Stddev: 21056310.730000,1618.684694
Num Phases Traversed: 738141.000000
Num Phases Traversed - Session Mean & Stddev: 7381.410000,0.567362
Num Path Iterations: 738141.000000
Num Path Iterations - Session Mean & Stddev: 7381.410000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4519400000 of size 360448 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2116501003.000000
Throughput: 211650100.300000
Num Accesses - Session Mean & Stddev: 21165010.030000,1426.214671
Num Phases Traversed: 741951.000000
Num Phases Traversed - Session Mean & Stddev: 7419.510000,0.499900
Num Path Iterations: 741951.000000
Num Path Iterations - Session Mean & Stddev: 7419.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2aaca00000 of size 360448 bytes.
[1.232s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2115627985.000000
Throughput: 211562798.500000
Num Accesses - Session Mean & Stddev: 21156279.850000,1632.703876
Num Phases Traversed: 741645.000000
Num Phases Traversed - Session Mean & Stddev: 7416.450000,0.572276
Num Path Iterations: 741645.000000
Num Path Iterations - Session Mean & Stddev: 7416.450000,0.572276
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feac7800000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2116098730.000000
Throughput: 211609873.000000
Num Accesses - Session Mean & Stddev: 21160987.300000,1955.918257
Num Phases Traversed: 741810.000000
Num Phases Traversed - Session Mean & Stddev: 7418.100000,0.685565
Num Path Iterations: 741810.000000
Num Path Iterations - Session Mean & Stddev: 7418.100000,0.685565
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff76dc00000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2115876196.000000
Throughput: 211587619.600000
Num Accesses - Session Mean & Stddev: 21158761.960000,1556.389295
Num Phases Traversed: 741732.000000
Num Phases Traversed - Session Mean & Stddev: 7417.320000,0.545527
Num Path Iterations: 741732.000000
Num Path Iterations - Session Mean & Stddev: 7417.320000,0.545527
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fea66600000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2115616573.000000
Throughput: 211561657.300000
Num Accesses - Session Mean & Stddev: 21156165.730000,1618.684694
Num Phases Traversed: 741641.000000
Num Phases Traversed - Session Mean & Stddev: 7416.410000,0.567362
Num Path Iterations: 741641.000000
Num Path Iterations - Session Mean & Stddev: 7416.410000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f53a7200000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2113978951.000000
Throughput: 211397895.100000
Num Accesses - Session Mean & Stddev: 21139789.510000,1761.250973
Num Phases Traversed: 741067.000000
Num Phases Traversed - Session Mean & Stddev: 7410.670000,0.617333
Num Path Iterations: 741067.000000
Num Path Iterations - Session Mean & Stddev: 7410.670000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc933200000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2115088768.000000
Throughput: 211508876.800000
Num Accesses - Session Mean & Stddev: 21150887.680000,1416.191956
Num Phases Traversed: 741456.000000
Num Phases Traversed - Session Mean & Stddev: 7414.560000,0.496387
Num Path Iterations: 741456.000000
Num Path Iterations - Session Mean & Stddev: 7414.560000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7534600000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2116415413.000000
Throughput: 211641541.300000
Num Accesses - Session Mean & Stddev: 21164154.130000,1905.111979
Num Phases Traversed: 741921.000000
Num Phases Traversed - Session Mean & Stddev: 7419.210000,0.667757
Num Path Iterations: 741921.000000
Num Path Iterations - Session Mean & Stddev: 7419.210000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fca67600000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2116315558.000000
Throughput: 211631555.800000
Num Accesses - Session Mean & Stddev: 21163155.580000,1935.839865
Num Phases Traversed: 741886.000000
Num Phases Traversed - Session Mean & Stddev: 7418.860000,0.678528
Num Path Iterations: 741886.000000
Num Path Iterations - Session Mean & Stddev: 7418.860000,0.678528
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f87a6600000 of size 360448 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2113944715.000000
Throughput: 211394471.500000
Num Accesses - Session Mean & Stddev: 21139447.150000,1529.750825
Num Phases Traversed: 741055.000000
Num Phases Traversed - Session Mean & Stddev: 7410.550000,0.536190
Num Path Iterations: 741055.000000
Num Path Iterations - Session Mean & Stddev: 7410.550000,0.536190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe1e8c00000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2112575275.000000
Throughput: 211257527.500000
Num Accesses - Session Mean & Stddev: 21125752.750000,1865.390803
Num Phases Traversed: 740575.000000
Num Phases Traversed - Session Mean & Stddev: 7405.750000,0.653835
Num Path Iterations: 740575.000000
Num Path Iterations - Session Mean & Stddev: 7405.750000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3c11600000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2117045926.000000
Throughput: 211704592.600000
Num Accesses - Session Mean & Stddev: 21170459.260000,1622.953318
Num Phases Traversed: 742142.000000
Num Phases Traversed - Session Mean & Stddev: 7421.420000,0.568859
Num Path Iterations: 742142.000000
Num Path Iterations - Session Mean & Stddev: 7421.420000,0.568859
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fede7a00000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2115836254.000000
Throughput: 211583625.400000
Num Accesses - Session Mean & Stddev: 21158362.540000,1908.740157
Num Phases Traversed: 741718.000000
Num Phases Traversed - Session Mean & Stddev: 7417.180000,0.669029
Num Path Iterations: 741718.000000
Num Path Iterations - Session Mean & Stddev: 7417.180000,0.669029
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0085800000 of size 360448 bytes.
[1.275s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2115502453.000000
Throughput: 211550245.300000
Num Accesses - Session Mean & Stddev: 21155024.530000,1996.896204
Num Phases Traversed: 741601.000000
Num Phases Traversed - Session Mean & Stddev: 7416.010000,0.699929
Num Path Iterations: 741601.000000
Num Path Iterations - Session Mean & Stddev: 7416.010000,0.699929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff3f2a00000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2117431081.000000
Throughput: 211743108.100000
Num Accesses - Session Mean & Stddev: 21174310.810000,1886.219259
Num Phases Traversed: 742277.000000
Num Phases Traversed - Session Mean & Stddev: 7422.770000,0.661135
Num Path Iterations: 742277.000000
Num Path Iterations - Session Mean & Stddev: 7422.770000,0.661135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f120bc00000 of size 360448 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2115416863.000000
Throughput: 211541686.300000
Num Accesses - Session Mean & Stddev: 21154168.630000,1817.654338
Num Phases Traversed: 741571.000000
Num Phases Traversed - Session Mean & Stddev: 7415.710000,0.637103
Num Path Iterations: 741571.000000
Num Path Iterations - Session Mean & Stddev: 7415.710000,0.637103
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f41e9400000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1826746609.000000
Throughput: 182674660.900000
Num Accesses - Session Mean & Stddev: 18267466.090000,2846.668003
Num Phases Traversed: 4032653.000000
Num Phases Traversed - Session Mean & Stddev: 40326.530000,6.284035
Num Path Iterations: 4032653.000000
Num Path Iterations - Session Mean & Stddev: 40326.530000,6.284035
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4849c00000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1825396216.000000
Throughput: 182539621.600000
Num Accesses - Session Mean & Stddev: 18253962.160000,1085.462111
Num Phases Traversed: 4029672.000000
Num Phases Traversed - Session Mean & Stddev: 40296.720000,2.396164
Num Path Iterations: 4029672.000000
Num Path Iterations - Session Mean & Stddev: 40296.720000,2.396164
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcc90400000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1823412982.000000
Throughput: 182341298.200000
Num Accesses - Session Mean & Stddev: 18234129.820000,2480.207061
Num Phases Traversed: 4025294.000000
Num Phases Traversed - Session Mean & Stddev: 40252.940000,5.475071
Num Path Iterations: 4025294.000000
Num Path Iterations - Session Mean & Stddev: 40252.940000,5.475071
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4deb800000 of size 53248 bytes.
[1.262s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1815538030.000000
Throughput: 181553803.000000
Num Accesses - Session Mean & Stddev: 18155380.300000,5337.524283
Num Phases Traversed: 4007910.000000
Num Phases Traversed - Session Mean & Stddev: 40079.100000,11.782614
Num Path Iterations: 4007910.000000
Num Path Iterations - Session Mean & Stddev: 40079.100000,11.782614
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f61c2000000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1822417288.000000
Throughput: 182241728.800000
Num Accesses - Session Mean & Stddev: 18224172.880000,4254.786675
Num Phases Traversed: 4023096.000000
Num Phases Traversed - Session Mean & Stddev: 40230.960000,9.392465
Num Path Iterations: 4023096.000000
Num Path Iterations - Session Mean & Stddev: 40230.960000,9.392465
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff21cc00000 of size 53248 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1817134402.000000
Throughput: 181713440.200000
Num Accesses - Session Mean & Stddev: 18171344.020000,8420.250140
Num Phases Traversed: 4011434.000000
Num Phases Traversed - Session Mean & Stddev: 40114.340000,18.587749
Num Path Iterations: 4011434.000000
Num Path Iterations - Session Mean & Stddev: 40114.340000,18.587749
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9ad7c00000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1825171528.000000
Throughput: 182517152.800000
Num Accesses - Session Mean & Stddev: 18251715.280000,2991.254737
Num Phases Traversed: 4029176.000000
Num Phases Traversed - Session Mean & Stddev: 40291.760000,6.603211
Num Path Iterations: 4029176.000000
Num Path Iterations - Session Mean & Stddev: 40291.760000,6.603211
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f432a600000 of size 53248 bytes.
[1.257s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1824849898.000000
Throughput: 182484989.800000
Num Accesses - Session Mean & Stddev: 18248498.980000,4512.853208
Num Phases Traversed: 4028466.000000
Num Phases Traversed - Session Mean & Stddev: 40284.660000,9.962148
Num Path Iterations: 4028466.000000
Num Path Iterations - Session Mean & Stddev: 40284.660000,9.962148
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feb78800000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1828382392.000000
Throughput: 182838239.200000
Num Accesses - Session Mean & Stddev: 18283823.920000,759.420420
Num Phases Traversed: 4036264.000000
Num Phases Traversed - Session Mean & Stddev: 40362.640000,1.676425
Num Path Iterations: 4036264.000000
Num Path Iterations - Session Mean & Stddev: 40362.640000,1.676425
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3fcd200000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1823754997.000000
Throughput: 182375499.700000
Num Accesses - Session Mean & Stddev: 18237549.970000,4236.698090
Num Phases Traversed: 4026049.000000
Num Phases Traversed - Session Mean & Stddev: 40260.490000,9.352534
Num Path Iterations: 4026049.000000
Num Path Iterations - Session Mean & Stddev: 40260.490000,9.352534
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6e72800000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1824050353.000000
Throughput: 182405035.300000
Num Accesses - Session Mean & Stddev: 18240503.530000,3805.466937
Num Phases Traversed: 4026701.000000
Num Phases Traversed - Session Mean & Stddev: 40267.010000,8.400589
Num Path Iterations: 4026701.000000
Num Path Iterations - Session Mean & Stddev: 40267.010000,8.400589
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8cafa00000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1808251978.000000
Throughput: 180825197.800000
Num Accesses - Session Mean & Stddev: 18082519.780000,12259.425436
Num Phases Traversed: 3991826.000000
Num Phases Traversed - Session Mean & Stddev: 39918.260000,27.062749
Num Path Iterations: 3991826.000000
Num Path Iterations - Session Mean & Stddev: 39918.260000,27.062749
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcbd8400000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1825034722.000000
Throughput: 182503472.200000
Num Accesses - Session Mean & Stddev: 18250347.220000,387.679083
Num Phases Traversed: 4028874.000000
Num Phases Traversed - Session Mean & Stddev: 40288.740000,0.855804
Num Path Iterations: 4028874.000000
Num Path Iterations - Session Mean & Stddev: 40288.740000,0.855804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f369e400000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1826386474.000000
Throughput: 182638647.400000
Num Accesses - Session Mean & Stddev: 18263864.740000,1902.794133
Num Phases Traversed: 4031858.000000
Num Phases Traversed - Session Mean & Stddev: 40318.580000,4.200429
Num Path Iterations: 4031858.000000
Num Path Iterations - Session Mean & Stddev: 40318.580000,4.200429
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb1a1e00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1823308339.000000
Throughput: 182330833.900000
Num Accesses - Session Mean & Stddev: 18233083.390000,1014.264876
Num Phases Traversed: 4025063.000000
Num Phases Traversed - Session Mean & Stddev: 40250.630000,2.238995
Num Path Iterations: 4025063.000000
Num Path Iterations - Session Mean & Stddev: 40250.630000,2.238995
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5a69a00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1825716034.000000
Throughput: 182571603.400000
Num Accesses - Session Mean & Stddev: 18257160.340000,498.629346
Num Phases Traversed: 4030378.000000
Num Phases Traversed - Session Mean & Stddev: 40303.780000,1.100727
Num Path Iterations: 4030378.000000
Num Path Iterations - Session Mean & Stddev: 40303.780000,1.100727
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff99e200000 of size 53248 bytes.
[1.225s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1811980168.000000
Throughput: 181198016.800000
Num Accesses - Session Mean & Stddev: 18119801.680000,2269.814802
Num Phases Traversed: 4000056.000000
Num Phases Traversed - Session Mean & Stddev: 40000.560000,5.010629
Num Path Iterations: 4000056.000000
Num Path Iterations - Session Mean & Stddev: 40000.560000,5.010629
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efdb1000000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1823885008.000000
Throughput: 182388500.800000
Num Accesses - Session Mean & Stddev: 18238850.080000,588.969688
Num Phases Traversed: 4026336.000000
Num Phases Traversed - Session Mean & Stddev: 40263.360000,1.300154
Num Path Iterations: 4026336.000000
Num Path Iterations - Session Mean & Stddev: 40263.360000,1.300154
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6d56e00000 of size 53248 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1823110378.000000
Throughput: 182311037.800000
Num Accesses - Session Mean & Stddev: 18231103.780000,791.441692
Num Phases Traversed: 4024626.000000
Num Phases Traversed - Session Mean & Stddev: 40246.260000,1.747112
Num Path Iterations: 4024626.000000
Num Path Iterations - Session Mean & Stddev: 40246.260000,1.747112
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/416,0.001 0/416,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/416,0.001 0/416,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    416 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f11f8c00000 of size 53248 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1821562024.000000
Throughput: 182156202.400000
Num Accesses - Session Mean & Stddev: 18215620.240000,5815.249768
Num Phases Traversed: 4021208.000000
Num Phases Traversed - Session Mean & Stddev: 40212.080000,12.837196
Num Path Iterations: 4021208.000000
Num Path Iterations - Session Mean & Stddev: 40212.080000,12.837196
+ set +x

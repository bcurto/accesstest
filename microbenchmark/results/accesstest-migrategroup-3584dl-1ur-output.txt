++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc4e4400000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1951694907.000000
Throughput: 195169490.700000
Num Accesses - Session Mean & Stddev: 19516949.070000,734.110567
Num Phases Traversed: 1067183.000000
Num Phases Traversed - Session Mean & Stddev: 10671.830000,0.401373
Num Path Iterations: 1067183.000000
Num Path Iterations - Session Mean & Stddev: 10671.830000,0.401373
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1a95800000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1952684396.000000
Throughput: 195268439.600000
Num Accesses - Session Mean & Stddev: 19526843.960000,781.135045
Num Phases Traversed: 1067724.000000
Num Phases Traversed - Session Mean & Stddev: 10677.240000,0.427083
Num Path Iterations: 1067724.000000
Num Path Iterations - Session Mean & Stddev: 10677.240000,0.427083
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9d1e000000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1953792770.000000
Throughput: 195379277.000000
Num Accesses - Session Mean & Stddev: 19537927.700000,838.153095
Num Phases Traversed: 1068330.000000
Num Phases Traversed - Session Mean & Stddev: 10683.300000,0.458258
Num Path Iterations: 1068330.000000
Num Path Iterations - Session Mean & Stddev: 10683.300000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa02ce00000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1952997155.000000
Throughput: 195299715.500000
Num Accesses - Session Mean & Stddev: 19529971.550000,599.677753
Num Phases Traversed: 1067895.000000
Num Phases Traversed - Session Mean & Stddev: 10678.950000,0.327872
Num Path Iterations: 1067895.000000
Num Path Iterations - Session Mean & Stddev: 10678.950000,0.327872
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f546ca00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1952375295.000000
Throughput: 195237529.500000
Num Accesses - Session Mean & Stddev: 19523752.950000,909.916011
Num Phases Traversed: 1067555.000000
Num Phases Traversed - Session Mean & Stddev: 10675.550000,0.497494
Num Path Iterations: 1067555.000000
Num Path Iterations - Session Mean & Stddev: 10675.550000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd63f200000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1952289332.000000
Throughput: 195228933.200000
Num Accesses - Session Mean & Stddev: 19522893.320000,668.523895
Num Phases Traversed: 1067508.000000
Num Phases Traversed - Session Mean & Stddev: 10675.080000,0.365513
Num Path Iterations: 1067508.000000
Num Path Iterations - Session Mean & Stddev: 10675.080000,0.365513
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1744a00000 of size 229376 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1952461258.000000
Throughput: 195246125.800000
Num Accesses - Session Mean & Stddev: 19524612.580000,363.966404
Num Phases Traversed: 1067602.000000
Num Phases Traversed - Session Mean & Stddev: 10676.020000,0.198997
Num Path Iterations: 1067602.000000
Num Path Iterations - Session Mean & Stddev: 10676.020000,0.198997
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7580a00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1952078997.000000
Throughput: 195207899.700000
Num Accesses - Session Mean & Stddev: 19520789.970000,696.702569
Num Phases Traversed: 1067393.000000
Num Phases Traversed - Session Mean & Stddev: 10673.930000,0.380920
Num Path Iterations: 1067393.000000
Num Path Iterations - Session Mean & Stddev: 10673.930000,0.380920
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa01ee00000 of size 229376 bytes.
[1.236s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 960131821.000000
Throughput: 96013182.100000
Num Accesses - Session Mean & Stddev: 9601318.210000,950.200266
Num Phases Traversed: 525049.000000
Num Phases Traversed - Session Mean & Stddev: 5250.490000,0.519519
Num Path Iterations: 525049.000000
Num Path Iterations - Session Mean & Stddev: 5250.490000,0.519519
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f05efe00000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1953086776.000000
Throughput: 195308677.600000
Num Accesses - Session Mean & Stddev: 19530867.760000,907.891724
Num Phases Traversed: 1067944.000000
Num Phases Traversed - Session Mean & Stddev: 10679.440000,0.496387
Num Path Iterations: 1067944.000000
Num Path Iterations - Session Mean & Stddev: 10679.440000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa56f200000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1952377124.000000
Throughput: 195237712.400000
Num Accesses - Session Mean & Stddev: 19523771.240000,907.891724
Num Phases Traversed: 1067556.000000
Num Phases Traversed - Session Mean & Stddev: 10675.560000,0.496387
Num Path Iterations: 1067556.000000
Num Path Iterations - Session Mean & Stddev: 10675.560000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1f23a00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1952801452.000000
Throughput: 195280145.200000
Num Accesses - Session Mean & Stddev: 19528014.520000,594.354650
Num Phases Traversed: 1067788.000000
Num Phases Traversed - Session Mean & Stddev: 10677.880000,0.324962
Num Path Iterations: 1067788.000000
Num Path Iterations - Session Mean & Stddev: 10677.880000,0.324962
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f34b5e00000 of size 229376 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1953523907.000000
Throughput: 195352390.700000
Num Accesses - Session Mean & Stddev: 19535239.070000,687.032390
Num Phases Traversed: 1068183.000000
Num Phases Traversed - Session Mean & Stddev: 10681.830000,0.375633
Num Path Iterations: 1068183.000000
Num Path Iterations - Session Mean & Stddev: 10681.830000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f00fd200000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1953461721.000000
Throughput: 195346172.100000
Num Accesses - Session Mean & Stddev: 19534617.210000,914.317082
Num Phases Traversed: 1068149.000000
Num Phases Traversed - Session Mean & Stddev: 10681.490000,0.499900
Num Path Iterations: 1068149.000000
Num Path Iterations - Session Mean & Stddev: 10681.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7ec4200000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1952141183.000000
Throughput: 195214118.300000
Num Accesses - Session Mean & Stddev: 19521411.830000,812.001848
Num Phases Traversed: 1067427.000000
Num Phases Traversed - Session Mean & Stddev: 10674.270000,0.443959
Num Path Iterations: 1067427.000000
Num Path Iterations - Session Mean & Stddev: 10674.270000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f560a800000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1952274700.000000
Throughput: 195227470.000000
Num Accesses - Session Mean & Stddev: 19522747.000000,517.319321
Num Phases Traversed: 1067500.000000
Num Phases Traversed - Session Mean & Stddev: 10675.000000,0.282843
Num Path Iterations: 1067500.000000
Num Path Iterations - Session Mean & Stddev: 10675.000000,0.282843
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f475c800000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1952975207.000000
Throughput: 195297520.700000
Num Accesses - Session Mean & Stddev: 19529752.070000,687.032390
Num Phases Traversed: 1067883.000000
Num Phases Traversed - Session Mean & Stddev: 10678.830000,0.375633
Num Path Iterations: 1067883.000000
Num Path Iterations - Session Mean & Stddev: 10678.830000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa43f400000 of size 229376 bytes.
[1.236s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1943544883.000000
Throughput: 194354488.300000
Num Accesses - Session Mean & Stddev: 19435448.830000,3835.278243
Num Phases Traversed: 1062727.000000
Num Phases Traversed - Session Mean & Stddev: 10627.270000,2.096926
Num Path Iterations: 1062727.000000
Num Path Iterations - Session Mean & Stddev: 10627.270000,2.096926
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f572ce00000 of size 229376 bytes.
[1.235s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 958129066.000000
Throughput: 95812906.600000
Num Accesses - Session Mean & Stddev: 9581290.660000,1048.130309
Num Phases Traversed: 523954.000000
Num Phases Traversed - Session Mean & Stddev: 5239.540000,0.573062
Num Path Iterations: 523954.000000
Num Path Iterations - Session Mean & Stddev: 5239.540000,0.573062
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,1 0/1792,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,1 0/1792,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4d3ca00000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1952505154.000000
Throughput: 195250515.400000
Num Accesses - Session Mean & Stddev: 19525051.540000,802.262032
Num Phases Traversed: 1067626.000000
Num Phases Traversed - Session Mean & Stddev: 10676.260000,0.438634
Num Path Iterations: 1067626.000000
Num Path Iterations - Session Mean & Stddev: 10676.260000,0.438634
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f401a000000 of size 73728 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1332240969.000000
Throughput: 133224096.900000
Num Accesses - Session Mean & Stddev: 13322409.690000,2791.210536
Num Phases Traversed: 2173413.000000
Num Phases Traversed - Session Mean & Stddev: 21734.130000,4.553361
Num Path Iterations: 2173413.000000
Num Path Iterations - Session Mean & Stddev: 21734.130000,4.553361
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff537a00000 of size 73728 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1331434261.000000
Throughput: 133143426.100000
Num Accesses - Session Mean & Stddev: 13314342.610000,1950.948943
Num Phases Traversed: 2172097.000000
Num Phases Traversed - Session Mean & Stddev: 21720.970000,3.182625
Num Path Iterations: 2172097.000000
Num Path Iterations - Session Mean & Stddev: 21720.970000,3.182625
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb9a3200000 of size 73728 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1331226454.000000
Throughput: 133122645.400000
Num Accesses - Session Mean & Stddev: 13312264.540000,5075.832484
Num Phases Traversed: 2171758.000000
Num Phases Traversed - Session Mean & Stddev: 21717.580000,8.280314
Num Path Iterations: 2171758.000000
Num Path Iterations - Session Mean & Stddev: 21717.580000,8.280314
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe4cd000000 of size 73728 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1330989223.000000
Throughput: 133098922.300000
Num Accesses - Session Mean & Stddev: 13309892.230000,5129.674689
Num Phases Traversed: 2171371.000000
Num Phases Traversed - Session Mean & Stddev: 21713.710000,8.368148
Num Path Iterations: 2171371.000000
Num Path Iterations - Session Mean & Stddev: 21713.710000,8.368148
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3bd6600000 of size 73728 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1333467582.000000
Throughput: 133346758.200000
Num Accesses - Session Mean & Stddev: 13334675.820000,1368.020441
Num Phases Traversed: 2175414.000000
Num Phases Traversed - Session Mean & Stddev: 21754.140000,2.231681
Num Path Iterations: 2175414.000000
Num Path Iterations - Session Mean & Stddev: 21754.140000,2.231681
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f336b000000 of size 73728 bytes.
[1.262s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1332355600.000000
Throughput: 133235560.000000
Num Accesses - Session Mean & Stddev: 13323556.000000,5637.600733
Num Phases Traversed: 2173600.000000
Num Phases Traversed - Session Mean & Stddev: 21736.000000,9.196739
Num Path Iterations: 2173600.000000
Num Path Iterations - Session Mean & Stddev: 21736.000000,9.196739
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8a7da00000 of size 73728 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1332614899.000000
Throughput: 133261489.900000
Num Accesses - Session Mean & Stddev: 13326148.990000,2437.945006
Num Phases Traversed: 2174023.000000
Num Phases Traversed - Session Mean & Stddev: 21740.230000,3.977072
Num Path Iterations: 2174023.000000
Num Path Iterations - Session Mean & Stddev: 21740.230000,3.977072
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd46be00000 of size 73728 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1335043605.000000
Throughput: 133504360.500000
Num Accesses - Session Mean & Stddev: 13350436.050000,772.355674
Num Phases Traversed: 2177985.000000
Num Phases Traversed - Session Mean & Stddev: 21779.850000,1.259960
Num Path Iterations: 2177985.000000
Num Path Iterations - Session Mean & Stddev: 21779.850000,1.259960
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff4dba00000 of size 73728 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1332298591.000000
Throughput: 133229859.100000
Num Accesses - Session Mean & Stddev: 13322985.910000,4913.381420
Num Phases Traversed: 2173507.000000
Num Phases Traversed - Session Mean & Stddev: 21735.070000,8.015304
Num Path Iterations: 2173507.000000
Num Path Iterations - Session Mean & Stddev: 21735.070000,8.015304
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3b78e00000 of size 73728 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1329989420.000000
Throughput: 132998942.000000
Num Accesses - Session Mean & Stddev: 13299894.200000,3165.122674
Num Phases Traversed: 2169740.000000
Num Phases Traversed - Session Mean & Stddev: 21697.400000,5.163332
Num Path Iterations: 2169740.000000
Num Path Iterations - Session Mean & Stddev: 21697.400000,5.163332
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbfa2a00000 of size 73728 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1331278559.000000
Throughput: 133127855.900000
Num Accesses - Session Mean & Stddev: 13312785.590000,996.968697
Num Phases Traversed: 2171843.000000
Num Phases Traversed - Session Mean & Stddev: 21718.430000,1.626376
Num Path Iterations: 2171843.000000
Num Path Iterations - Session Mean & Stddev: 21718.430000,1.626376
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efd12600000 of size 73728 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1329836170.000000
Throughput: 132983617.000000
Num Accesses - Session Mean & Stddev: 13298361.700000,4625.205852
Num Phases Traversed: 2169490.000000
Num Phases Traversed - Session Mean & Stddev: 21694.900000,7.545197
Num Path Iterations: 2169490.000000
Num Path Iterations - Session Mean & Stddev: 21694.900000,7.545197
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1b85a00000 of size 73728 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1331925887.000000
Throughput: 133192588.700000
Num Accesses - Session Mean & Stddev: 13319258.870000,1338.798175
Num Phases Traversed: 2172899.000000
Num Phases Traversed - Session Mean & Stddev: 21728.990000,2.184010
Num Path Iterations: 2172899.000000
Num Path Iterations - Session Mean & Stddev: 21728.990000,2.184010
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8e83c00000 of size 73728 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1333036030.000000
Throughput: 133303603.000000
Num Accesses - Session Mean & Stddev: 13330360.300000,8219.916295
Num Phases Traversed: 2174710.000000
Num Phases Traversed - Session Mean & Stddev: 21747.100000,13.409325
Num Path Iterations: 2174710.000000
Num Path Iterations - Session Mean & Stddev: 21747.100000,13.409325
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe9ffa00000 of size 73728 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1330425876.000000
Throughput: 133042587.600000
Num Accesses - Session Mean & Stddev: 13304258.760000,1330.308867
Num Phases Traversed: 2170452.000000
Num Phases Traversed - Session Mean & Stddev: 21704.520000,2.170161
Num Path Iterations: 2170452.000000
Num Path Iterations - Session Mean & Stddev: 21704.520000,2.170161
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe7c8600000 of size 73728 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1332144728.000000
Throughput: 133214472.800000
Num Accesses - Session Mean & Stddev: 13321447.280000,2369.893200
Num Phases Traversed: 2173256.000000
Num Phases Traversed - Session Mean & Stddev: 21732.560000,3.866057
Num Path Iterations: 2173256.000000
Num Path Iterations - Session Mean & Stddev: 21732.560000,3.866057
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd265600000 of size 73728 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1331727888.000000
Throughput: 133172788.800000
Num Accesses - Session Mean & Stddev: 13317278.880000,3043.000165
Num Phases Traversed: 2172576.000000
Num Phases Traversed - Session Mean & Stddev: 21725.760000,4.964111
Num Path Iterations: 2172576.000000
Num Path Iterations - Session Mean & Stddev: 21725.760000,4.964111
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbadb600000 of size 73728 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1332288783.000000
Throughput: 133228878.300000
Num Accesses - Session Mean & Stddev: 13322887.830000,3169.980603
Num Phases Traversed: 2173491.000000
Num Phases Traversed - Session Mean & Stddev: 21734.910000,5.171257
Num Path Iterations: 2173491.000000
Num Path Iterations - Session Mean & Stddev: 21734.910000,5.171257
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7177800000 of size 73728 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1331067687.000000
Throughput: 133106768.700000
Num Accesses - Session Mean & Stddev: 13310676.870000,1636.824790
Num Phases Traversed: 2171499.000000
Num Phases Traversed - Session Mean & Stddev: 21714.990000,2.670187
Num Path Iterations: 2171499.000000
Num Path Iterations - Session Mean & Stddev: 21714.990000,2.670187
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/576,0.01 0/576,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/576,0.01 0/576,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    576 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc0a1200000 of size 73728 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1332911591.000000
Throughput: 133291159.100000
Num Accesses - Session Mean & Stddev: 13329115.910000,1357.639320
Num Phases Traversed: 2174507.000000
Num Phases Traversed - Session Mean & Stddev: 21745.070000,2.214746
Num Path Iterations: 2174507.000000
Num Path Iterations - Session Mean & Stddev: 21745.070000,2.214746
+ set +x

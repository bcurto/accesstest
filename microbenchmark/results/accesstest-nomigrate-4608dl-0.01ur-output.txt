++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc1c6400000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1153382708.000000
Throughput: 115338270.800000
Num Accesses - Session Mean & Stddev: 11533827.080000,2393.779817
Num Phases Traversed: 492788.000000
Num Phases Traversed - Session Mean & Stddev: 4927.880000,1.022546
Num Path Iterations: 492788.000000
Num Path Iterations - Session Mean & Stddev: 4927.880000,1.022546
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5521e00000 of size 294912 bytes.
[1.275s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1165225827.000000
Throughput: 116522582.700000
Num Accesses - Session Mean & Stddev: 11652258.270000,1342.967042
Num Phases Traversed: 497847.000000
Num Phases Traversed - Session Mean & Stddev: 4978.470000,0.573672
Num Path Iterations: 497847.000000
Num Path Iterations - Session Mean & Stddev: 4978.470000,0.573672
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9146400000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1164228561.000000
Throughput: 116422856.100000
Num Accesses - Session Mean & Stddev: 11642285.610000,2127.478338
Num Phases Traversed: 497421.000000
Num Phases Traversed - Session Mean & Stddev: 4974.210000,0.908790
Num Path Iterations: 497421.000000
Num Path Iterations - Session Mean & Stddev: 4974.210000,0.908790
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa09fc00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1163404529.000000
Throughput: 116340452.900000
Num Accesses - Session Mean & Stddev: 11634045.290000,1773.452358
Num Phases Traversed: 497069.000000
Num Phases Traversed - Session Mean & Stddev: 4970.690000,0.757562
Num Path Iterations: 497069.000000
Num Path Iterations - Session Mean & Stddev: 4970.690000,0.757562
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f42b5e00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1167974161.000000
Throughput: 116797416.100000
Num Accesses - Session Mean & Stddev: 11679741.610000,2021.816411
Num Phases Traversed: 499021.000000
Num Phases Traversed - Session Mean & Stddev: 4990.210000,0.863655
Num Path Iterations: 499021.000000
Num Path Iterations - Session Mean & Stddev: 4990.210000,0.863655
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f666e600000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1166143499.000000
Throughput: 116614349.900000
Num Accesses - Session Mean & Stddev: 11661434.990000,1549.133645
Num Phases Traversed: 498239.000000
Num Phases Traversed - Session Mean & Stddev: 4982.390000,0.661740
Num Path Iterations: 498239.000000
Num Path Iterations - Session Mean & Stddev: 4982.390000,0.661740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff78ae00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1162131025.000000
Throughput: 116213102.500000
Num Accesses - Session Mean & Stddev: 11621310.250000,2485.762826
Num Phases Traversed: 496525.000000
Num Phases Traversed - Session Mean & Stddev: 4965.250000,1.061838
Num Path Iterations: 496525.000000
Num Path Iterations - Session Mean & Stddev: 4965.250000,1.061838
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbabfc00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1164579711.000000
Throughput: 116457971.100000
Num Accesses - Session Mean & Stddev: 11645797.110000,2461.837273
Num Phases Traversed: 497571.000000
Num Phases Traversed - Session Mean & Stddev: 4975.710000,1.051618
Num Path Iterations: 497571.000000
Num Path Iterations - Session Mean & Stddev: 4975.710000,1.051618
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3fc6800000 of size 294912 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1156025697.000000
Throughput: 115602569.700000
Num Accesses - Session Mean & Stddev: 11560256.970000,2434.077195
Num Phases Traversed: 493917.000000
Num Phases Traversed - Session Mean & Stddev: 4939.170000,1.039760
Num Path Iterations: 493917.000000
Num Path Iterations - Session Mean & Stddev: 4939.170000,1.039760
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa7cfc00000 of size 294912 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1153605103.000000
Throughput: 115360510.300000
Num Accesses - Session Mean & Stddev: 11536051.030000,2015.300536
Num Phases Traversed: 492883.000000
Num Phases Traversed - Session Mean & Stddev: 4928.830000,0.860872
Num Path Iterations: 492883.000000
Num Path Iterations - Session Mean & Stddev: 4928.830000,0.860872
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb27ea00000 of size 294912 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1166368235.000000
Throughput: 116636823.500000
Num Accesses - Session Mean & Stddev: 11663682.350000,2717.476522
Num Phases Traversed: 498335.000000
Num Phases Traversed - Session Mean & Stddev: 4983.350000,1.160819
Num Path Iterations: 498335.000000
Num Path Iterations - Session Mean & Stddev: 4983.350000,1.160819
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbe12e00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1159628496.000000
Throughput: 115962849.600000
Num Accesses - Session Mean & Stddev: 11596284.960000,1665.897445
Num Phases Traversed: 495456.000000
Num Phases Traversed - Session Mean & Stddev: 4954.560000,0.711618
Num Path Iterations: 495456.000000
Num Path Iterations - Session Mean & Stddev: 4954.560000,0.711618
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9921e00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1151528636.000000
Throughput: 115152863.600000
Num Accesses - Session Mean & Stddev: 11515286.360000,2291.789980
Num Phases Traversed: 491996.000000
Num Phases Traversed - Session Mean & Stddev: 4919.960000,0.978979
Num Path Iterations: 491996.000000
Num Path Iterations - Session Mean & Stddev: 4919.960000,0.978979
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f219ba00000 of size 294912 bytes.
[1.271s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1159335871.000000
Throughput: 115933587.100000
Num Accesses - Session Mean & Stddev: 11593358.710000,2600.407415
Num Phases Traversed: 495331.000000
Num Phases Traversed - Session Mean & Stddev: 4953.310000,1.110811
Num Path Iterations: 495331.000000
Num Path Iterations - Session Mean & Stddev: 4953.310000,1.110811
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd31a800000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1171972589.000000
Throughput: 117197258.900000
Num Accesses - Session Mean & Stddev: 11719725.890000,2570.733525
Num Phases Traversed: 500729.000000
Num Phases Traversed - Session Mean & Stddev: 5007.290000,1.098135
Num Path Iterations: 500729.000000
Num Path Iterations - Session Mean & Stddev: 5007.290000,1.098135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f55f2e00000 of size 294912 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1162116979.000000
Throughput: 116211697.900000
Num Accesses - Session Mean & Stddev: 11621169.790000,2579.246616
Num Phases Traversed: 496519.000000
Num Phases Traversed - Session Mean & Stddev: 4965.190000,1.101771
Num Path Iterations: 496519.000000
Num Path Iterations - Session Mean & Stddev: 4965.190000,1.101771
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcf7e400000 of size 294912 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1159153273.000000
Throughput: 115915327.300000
Num Accesses - Session Mean & Stddev: 11591532.730000,1383.172476
Num Phases Traversed: 495253.000000
Num Phases Traversed - Session Mean & Stddev: 4952.530000,0.590847
Num Path Iterations: 495253.000000
Num Path Iterations - Session Mean & Stddev: 4952.530000,0.590847
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f69a9e00000 of size 294912 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1162468129.000000
Throughput: 116246812.900000
Num Accesses - Session Mean & Stddev: 11624681.290000,1710.532673
Num Phases Traversed: 496669.000000
Num Phases Traversed - Session Mean & Stddev: 4966.690000,0.730685
Num Path Iterations: 496669.000000
Num Path Iterations - Session Mean & Stddev: 4966.690000,0.730685
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9aed200000 of size 294912 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1153141585.000000
Throughput: 115314158.500000
Num Accesses - Session Mean & Stddev: 11531415.850000,1969.085780
Num Phases Traversed: 492685.000000
Num Phases Traversed - Session Mean & Stddev: 4926.850000,0.841130
Num Path Iterations: 492685.000000
Num Path Iterations - Session Mean & Stddev: 4926.850000,0.841130
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.01 0/2304,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.01 0/2304,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcb80800000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1158853625.000000
Throughput: 115885362.500000
Num Accesses - Session Mean & Stddev: 11588536.250000,2572.438397
Num Phases Traversed: 495125.000000
Num Phases Traversed - Session Mean & Stddev: 4951.250000,1.098863
Num Path Iterations: 495125.000000
Num Path Iterations - Session Mean & Stddev: 4951.250000,1.098863
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9dc0000000 of size 3145728 bytes.
[1.532s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805312847.000000
Throughput: 80531284.700000
Num Accesses - Session Mean & Stddev: 8053128.470000,19903.076871
Num Phases Traversed: 32819.000000
Num Phases Traversed - Session Mean & Stddev: 328.190000,0.808641
Num Path Iterations: 32819.000000
Num Path Iterations - Session Mean & Stddev: 328.190000,0.808641
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6fc0000000 of size 3145728 bytes.
[1.532s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805214395.000000
Throughput: 80521439.500000
Num Accesses - Session Mean & Stddev: 8052143.950000,22117.488860
Num Phases Traversed: 32815.000000
Num Phases Traversed - Session Mean & Stddev: 328.150000,0.898610
Num Path Iterations: 32815.000000
Num Path Iterations - Session Mean & Stddev: 328.150000,0.898610
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa7c0000000 of size 3145728 bytes.
[1.531s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805189782.000000
Throughput: 80518978.200000
Num Accesses - Session Mean & Stddev: 8051897.820000,22020.039398
Num Phases Traversed: 32814.000000
Num Phases Traversed - Session Mean & Stddev: 328.140000,0.894651
Num Path Iterations: 32814.000000
Num Path Iterations - Session Mean & Stddev: 328.140000,0.894651
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe300000000 of size 3145728 bytes.
[1.532s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805288234.000000
Throughput: 80528823.400000
Num Accesses - Session Mean & Stddev: 8052882.340000,21281.351163
Num Phases Traversed: 32818.000000
Num Phases Traversed - Session Mean & Stddev: 328.180000,0.864639
Num Path Iterations: 32818.000000
Num Path Iterations - Session Mean & Stddev: 328.180000,0.864639
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9140000000 of size 3145728 bytes.
[1.532s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805239008.000000
Throughput: 80523900.800000
Num Accesses - Session Mean & Stddev: 8052390.080000,22211.783627
Num Phases Traversed: 32816.000000
Num Phases Traversed - Session Mean & Stddev: 328.160000,0.902441
Num Path Iterations: 32816.000000
Num Path Iterations - Session Mean & Stddev: 328.160000,0.902441
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbec0000000 of size 3145728 bytes.
[1.532s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805312847.000000
Throughput: 80531284.700000
Num Accesses - Session Mean & Stddev: 8053128.470000,21370.831660
Num Phases Traversed: 32819.000000
Num Phases Traversed - Session Mean & Stddev: 328.190000,0.868274
Num Path Iterations: 32819.000000
Num Path Iterations - Session Mean & Stddev: 328.190000,0.868274
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4500000000 of size 3145728 bytes.
[1.532s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805386686.000000
Throughput: 80538668.600000
Num Accesses - Session Mean & Stddev: 8053866.860000,21620.247495
Num Phases Traversed: 32822.000000
Num Phases Traversed - Session Mean & Stddev: 328.220000,0.878408
Num Path Iterations: 32822.000000
Num Path Iterations - Session Mean & Stddev: 328.220000,0.878408
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9a40000000 of size 3145728 bytes.
[1.532s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805362073.000000
Throughput: 80536207.300000
Num Accesses - Session Mean & Stddev: 8053620.730000,20679.314711
Num Phases Traversed: 32821.000000
Num Phases Traversed - Session Mean & Stddev: 328.210000,0.840179
Num Path Iterations: 32821.000000
Num Path Iterations - Session Mean & Stddev: 328.210000,0.840179
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0bc0000000 of size 3145728 bytes.
[1.531s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805214395.000000
Throughput: 80521439.500000
Num Accesses - Session Mean & Stddev: 8052143.950000,21841.870755
Num Phases Traversed: 32815.000000
Num Phases Traversed - Session Mean & Stddev: 328.150000,0.887412
Num Path Iterations: 32815.000000
Num Path Iterations - Session Mean & Stddev: 328.150000,0.887412
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f42c0000000 of size 3145728 bytes.
[1.532s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805214395.000000
Throughput: 80521439.500000
Num Accesses - Session Mean & Stddev: 8052143.950000,22117.488860
Num Phases Traversed: 32815.000000
Num Phases Traversed - Session Mean & Stddev: 328.150000,0.898610
Num Path Iterations: 32815.000000
Num Path Iterations - Session Mean & Stddev: 328.150000,0.898610
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcf80000000 of size 3145728 bytes.
[1.540s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805288234.000000
Throughput: 80528823.400000
Num Accesses - Session Mean & Stddev: 8052882.340000,21843.257497
Num Phases Traversed: 32818.000000
Num Phases Traversed - Session Mean & Stddev: 328.180000,0.887468
Num Path Iterations: 32818.000000
Num Path Iterations - Session Mean & Stddev: 328.180000,0.887468
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb140000000 of size 3145728 bytes.
[1.531s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805386686.000000
Throughput: 80538668.600000
Num Accesses - Session Mean & Stddev: 8053866.860000,21052.389674
Num Phases Traversed: 32822.000000
Num Phases Traversed - Session Mean & Stddev: 328.220000,0.855336
Num Path Iterations: 32822.000000
Num Path Iterations - Session Mean & Stddev: 328.220000,0.855336
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6840000000 of size 3145728 bytes.
[1.524s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805189782.000000
Throughput: 80518978.200000
Num Accesses - Session Mean & Stddev: 8051897.820000,22830.464762
Num Phases Traversed: 32814.000000
Num Phases Traversed - Session Mean & Stddev: 328.140000,0.927577
Num Path Iterations: 32814.000000
Num Path Iterations - Session Mean & Stddev: 328.140000,0.927577
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9900000000 of size 3145728 bytes.
[1.540s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805189782.000000
Throughput: 80518978.200000
Num Accesses - Session Mean & Stddev: 8051897.820000,24370.598957
Num Phases Traversed: 32814.000000
Num Phases Traversed - Session Mean & Stddev: 328.140000,0.990152
Num Path Iterations: 32814.000000
Num Path Iterations - Session Mean & Stddev: 328.140000,0.990152
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6380000000 of size 3145728 bytes.
[1.531s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805337460.000000
Throughput: 80533746.000000
Num Accesses - Session Mean & Stddev: 8053374.600000,21172.903180
Num Phases Traversed: 32820.000000
Num Phases Traversed - Session Mean & Stddev: 328.200000,0.860233
Num Path Iterations: 32820.000000
Num Path Iterations - Session Mean & Stddev: 328.200000,0.860233
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6940000000 of size 3145728 bytes.
[1.532s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805239008.000000
Throughput: 80523900.800000
Num Accesses - Session Mean & Stddev: 8052390.080000,21377.917245
Num Phases Traversed: 32816.000000
Num Phases Traversed - Session Mean & Stddev: 328.160000,0.868562
Num Path Iterations: 32816.000000
Num Path Iterations - Session Mean & Stddev: 328.160000,0.868562
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2640000000 of size 3145728 bytes.
[1.532s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805239008.000000
Throughput: 80523900.800000
Num Accesses - Session Mean & Stddev: 8052390.080000,22211.783627
Num Phases Traversed: 32816.000000
Num Phases Traversed - Session Mean & Stddev: 328.160000,0.902441
Num Path Iterations: 32816.000000
Num Path Iterations - Session Mean & Stddev: 328.160000,0.902441
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe600000000 of size 3145728 bytes.
[1.531s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805140556.000000
Throughput: 80514055.600000
Num Accesses - Session Mean & Stddev: 8051405.560000,24185.951644
Num Phases Traversed: 32812.000000
Num Phases Traversed - Session Mean & Stddev: 328.120000,0.982649
Num Path Iterations: 32812.000000
Num Path Iterations - Session Mean & Stddev: 328.120000,0.982649
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1500000000 of size 3145728 bytes.
[1.532s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805386686.000000
Throughput: 80538668.600000
Num Accesses - Session Mean & Stddev: 8053866.860000,21620.247495
Num Phases Traversed: 32822.000000
Num Phases Traversed - Session Mean & Stddev: 328.220000,0.878408
Num Path Iterations: 32822.000000
Num Path Iterations - Session Mean & Stddev: 328.220000,0.878408
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/24576,1 0/24576,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/24576,1 0/24576,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    24576 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0dc0000000 of size 3145728 bytes.
[1.532s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804992878.000000
Throughput: 80499287.800000
Num Accesses - Session Mean & Stddev: 8049928.780000,23041.764636
Num Phases Traversed: 32806.000000
Num Phases Traversed - Session Mean & Stddev: 328.060000,0.936162
Num Path Iterations: 32806.000000
Num Path Iterations - Session Mean & Stddev: 328.060000,0.936162
+ set +x

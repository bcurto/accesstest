++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f02c0000000 of size 22020096 bytes.
[4.486s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 238143596.000000
Throughput: 23814359.600000
Num Accesses - Session Mean & Stddev: 2381435.960000,141306.264437
Num Phases Traversed: 1484.000000
Num Phases Traversed - Session Mean & Stddev: 14.840000,0.821219
Num Path Iterations: 1484.000000
Num Path Iterations - Session Mean & Stddev: 14.840000,0.821219
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa200000000 of size 22020096 bytes.
[4.427s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,156535.511159
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.909725
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.909725
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4700000000 of size 22020096 bytes.
[4.486s] Test is ready to start.
Test duration in range [10.000, 10.015] seconds
Num Accesses: 238659803.000000
Throughput: 23865980.300000
Num Accesses - Session Mean & Stddev: 2386598.030000,160780.964229
Num Phases Traversed: 1487.000000
Num Phases Traversed - Session Mean & Stddev: 14.870000,0.934398
Num Path Iterations: 1487.000000
Num Path Iterations - Session Mean & Stddev: 14.870000,0.934398
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f15c0000000 of size 22020096 bytes.
[4.486s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 238143596.000000
Throughput: 23814359.600000
Num Accesses - Session Mean & Stddev: 2381435.960000,155282.143454
Num Phases Traversed: 1484.000000
Num Phases Traversed - Session Mean & Stddev: 14.840000,0.902441
Num Path Iterations: 1484.000000
Num Path Iterations - Session Mean & Stddev: 14.840000,0.902441
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8b40000000 of size 22020096 bytes.
[4.493s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 238143596.000000
Throughput: 23814359.600000
Num Accesses - Session Mean & Stddev: 2381435.960000,151420.720000
Num Phases Traversed: 1484.000000
Num Phases Traversed - Session Mean & Stddev: 14.840000,0.880000
Num Path Iterations: 1484.000000
Num Path Iterations - Session Mean & Stddev: 14.840000,0.880000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0640000000 of size 22020096 bytes.
[4.487s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,148777.508357
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.864639
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.864639
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9840000000 of size 22020096 bytes.
[4.486s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 237455320.000000
Throughput: 23745532.000000
Num Accesses - Session Mean & Stddev: 2374553.200000,143963.254106
Num Phases Traversed: 1480.000000
Num Phases Traversed - Session Mean & Stddev: 14.800000,0.836660
Num Path Iterations: 1480.000000
Num Path Iterations - Session Mean & Stddev: 14.800000,0.836660
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f33c0000000 of size 22020096 bytes.
[4.486s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 237455320.000000
Throughput: 23745532.000000
Num Accesses - Session Mean & Stddev: 2374553.200000,148019.350637
Num Phases Traversed: 1480.000000
Num Phases Traversed - Session Mean & Stddev: 14.800000,0.860233
Num Path Iterations: 1480.000000
Num Path Iterations - Session Mean & Stddev: 14.800000,0.860233
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdd80000000 of size 22020096 bytes.
[4.484s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,144742.659097
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.841190
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.841190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd840000000 of size 22020096 bytes.
[4.435s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 237455320.000000
Throughput: 23745532.000000
Num Accesses - Session Mean & Stddev: 2374553.200000,155815.106533
Num Phases Traversed: 1480.000000
Num Phases Traversed - Session Mean & Stddev: 14.800000,0.905539
Num Path Iterations: 1480.000000
Num Path Iterations - Session Mean & Stddev: 14.800000,0.905539
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2040000000 of size 22020096 bytes.
[4.486s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,142682.453537
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.829216
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.829216
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1b80000000 of size 22020096 bytes.
[4.483s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 238143596.000000
Throughput: 23814359.600000
Num Accesses - Session Mean & Stddev: 2381435.960000,153363.585184
Num Phases Traversed: 1484.000000
Num Phases Traversed - Session Mean & Stddev: 14.840000,0.891291
Num Path Iterations: 1484.000000
Num Path Iterations - Session Mean & Stddev: 14.840000,0.891291
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3280000000 of size 22020096 bytes.
[4.426s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 238143596.000000
Throughput: 23814359.600000
Num Accesses - Session Mean & Stddev: 2381435.960000,160900.616908
Num Phases Traversed: 1484.000000
Num Phases Traversed - Session Mean & Stddev: 14.840000,0.935094
Num Path Iterations: 1484.000000
Num Path Iterations - Session Mean & Stddev: 14.840000,0.935094
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa980000000 of size 22020096 bytes.
[4.484s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 238143596.000000
Throughput: 23814359.600000
Num Accesses - Session Mean & Stddev: 2381435.960000,160900.616908
Num Phases Traversed: 1484.000000
Num Phases Traversed - Session Mean & Stddev: 14.840000,0.935094
Num Path Iterations: 1484.000000
Num Path Iterations - Session Mean & Stddev: 14.840000,0.935094
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa580000000 of size 22020096 bytes.
[4.486s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 238487734.000000
Throughput: 23848773.400000
Num Accesses - Session Mean & Stddev: 2384877.340000,165078.661845
Num Phases Traversed: 1486.000000
Num Phases Traversed - Session Mean & Stddev: 14.860000,0.959375
Num Path Iterations: 1486.000000
Num Path Iterations - Session Mean & Stddev: 14.860000,0.959375
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd600000000 of size 22020096 bytes.
[4.484s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 237455320.000000
Throughput: 23745532.000000
Num Accesses - Session Mean & Stddev: 2374553.200000,150006.276463
Num Phases Traversed: 1480.000000
Num Phases Traversed - Session Mean & Stddev: 14.800000,0.871780
Num Path Iterations: 1480.000000
Num Path Iterations - Session Mean & Stddev: 14.800000,0.871780
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff200000000 of size 22020096 bytes.
[4.453s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 238143596.000000
Throughput: 23814359.600000
Num Accesses - Session Mean & Stddev: 2381435.960000,153363.585184
Num Phases Traversed: 1484.000000
Num Phases Traversed - Session Mean & Stddev: 14.840000,0.891291
Num Path Iterations: 1484.000000
Num Path Iterations - Session Mean & Stddev: 14.840000,0.891291
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3c00000000 of size 22020096 bytes.
[4.479s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,152705.784512
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.887468
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.887468
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2a00000000 of size 22020096 bytes.
[4.485s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 238487734.000000
Throughput: 23848773.400000
Num Accesses - Session Mean & Stddev: 2384877.340000,161451.710942
Num Phases Traversed: 1486.000000
Num Phases Traversed - Session Mean & Stddev: 14.860000,0.938296
Num Path Iterations: 1486.000000
Num Path Iterations - Session Mean & Stddev: 14.860000,0.938296
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.001 0/172032,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.001 0/172032,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe1c0000000 of size 22020096 bytes.
[4.484s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 237455320.000000
Throughput: 23745532.000000
Num Accesses - Session Mean & Stddev: 2374553.200000,139789.516425
Num Phases Traversed: 1480.000000
Num Phases Traversed - Session Mean & Stddev: 14.800000,0.812404
Num Path Iterations: 1480.000000
Num Path Iterations - Session Mean & Stddev: 14.800000,0.812404
+ set +x

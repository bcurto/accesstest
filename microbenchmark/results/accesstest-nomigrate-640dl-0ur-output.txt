++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1cc8400000 of size 40960 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2167168243.000000
Throughput: 216716824.300000
Num Accesses - Session Mean & Stddev: 21671682.430000,563.325186
Num Phases Traversed: 6070599.000000
Num Phases Traversed - Session Mean & Stddev: 60705.990000,1.577942
Num Path Iterations: 6070599.000000
Num Path Iterations - Session Mean & Stddev: 60705.990000,1.577942
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe711c00000 of size 40960 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2169814327.000000
Throughput: 216981432.700000
Num Accesses - Session Mean & Stddev: 21698143.270000,64778.629209
Num Phases Traversed: 6078011.000000
Num Phases Traversed - Session Mean & Stddev: 60780.110000,181.452743
Num Path Iterations: 6078011.000000
Num Path Iterations - Session Mean & Stddev: 60780.110000,181.452743
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3ebae00000 of size 40960 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2174439262.000000
Throughput: 217443926.200000
Num Accesses - Session Mean & Stddev: 21744392.620000,23778.409438
Num Phases Traversed: 6090966.000000
Num Phases Traversed - Session Mean & Stddev: 60909.660000,66.606189
Num Path Iterations: 6090966.000000
Num Path Iterations - Session Mean & Stddev: 60909.660000,66.606189
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa523600000 of size 40960 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2171439748.000000
Throughput: 217143974.800000
Num Accesses - Session Mean & Stddev: 21714397.480000,57534.634808
Num Phases Traversed: 6082564.000000
Num Phases Traversed - Session Mean & Stddev: 60825.640000,161.161442
Num Path Iterations: 6082564.000000
Num Path Iterations - Session Mean & Stddev: 60825.640000,161.161442
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe8f4400000 of size 40960 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2170988857.000000
Throughput: 217098885.700000
Num Accesses - Session Mean & Stddev: 21709888.570000,59123.673002
Num Phases Traversed: 6081301.000000
Num Phases Traversed - Session Mean & Stddev: 60813.010000,165.612529
Num Path Iterations: 6081301.000000
Num Path Iterations - Session Mean & Stddev: 60813.010000,165.612529
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbac0600000 of size 40960 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2169729004.000000
Throughput: 216972900.400000
Num Accesses - Session Mean & Stddev: 21697290.040000,5242.209200
Num Phases Traversed: 6077772.000000
Num Phases Traversed - Session Mean & Stddev: 60777.720000,14.684059
Num Path Iterations: 6077772.000000
Num Path Iterations - Session Mean & Stddev: 60777.720000,14.684059
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5659c00000 of size 40960 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2162661832.000000
Throughput: 216266183.200000
Num Accesses - Session Mean & Stddev: 21626618.320000,2097.580835
Num Phases Traversed: 6057976.000000
Num Phases Traversed - Session Mean & Stddev: 60579.760000,5.875577
Num Path Iterations: 6057976.000000
Num Path Iterations - Session Mean & Stddev: 60579.760000,5.875577
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9b43600000 of size 40960 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2172916657.000000
Throughput: 217291665.700000
Num Accesses - Session Mean & Stddev: 21729166.570000,18813.154485
Num Phases Traversed: 6086701.000000
Num Phases Traversed - Session Mean & Stddev: 60867.010000,52.697912
Num Path Iterations: 6086701.000000
Num Path Iterations - Session Mean & Stddev: 60867.010000,52.697912
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc339a00000 of size 40960 bytes.
[1.251s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2171201986.000000
Throughput: 217120198.600000
Num Accesses - Session Mean & Stddev: 21712019.860000,25142.229319
Num Phases Traversed: 6081898.000000
Num Phases Traversed - Session Mean & Stddev: 60818.980000,70.426413
Num Path Iterations: 6081898.000000
Num Path Iterations - Session Mean & Stddev: 60818.980000,70.426413
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f552e600000 of size 40960 bytes.
[1.217s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2167607353.000000
Throughput: 216760735.300000
Num Accesses - Session Mean & Stddev: 21676073.530000,3370.312571
Num Phases Traversed: 6071829.000000
Num Phases Traversed - Session Mean & Stddev: 60718.290000,9.440651
Num Path Iterations: 6071829.000000
Num Path Iterations - Session Mean & Stddev: 60718.290000,9.440651
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2a91000000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2162085277.000000
Throughput: 216208527.700000
Num Accesses - Session Mean & Stddev: 21620852.770000,5108.442972
Num Phases Traversed: 6056361.000000
Num Phases Traversed - Session Mean & Stddev: 60563.610000,14.309364
Num Path Iterations: 6056361.000000
Num Path Iterations - Session Mean & Stddev: 60563.610000,14.309364
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd015200000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2172653191.000000
Throughput: 217265319.100000
Num Accesses - Session Mean & Stddev: 21726531.910000,32964.099314
Num Phases Traversed: 6085963.000000
Num Phases Traversed - Session Mean & Stddev: 60859.630000,92.336413
Num Path Iterations: 6085963.000000
Num Path Iterations - Session Mean & Stddev: 60859.630000,92.336413
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f99e9c00000 of size 40960 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2173800946.000000
Throughput: 217380094.600000
Num Accesses - Session Mean & Stddev: 21738009.460000,17818.669316
Num Phases Traversed: 6089178.000000
Num Phases Traversed - Session Mean & Stddev: 60891.780000,49.912239
Num Path Iterations: 6089178.000000
Num Path Iterations - Session Mean & Stddev: 60891.780000,49.912239
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f83a1400000 of size 40960 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2165851270.000000
Throughput: 216585127.000000
Num Accesses - Session Mean & Stddev: 21658512.700000,6415.579050
Num Phases Traversed: 6066910.000000
Num Phases Traversed - Session Mean & Stddev: 60669.100000,17.970810
Num Path Iterations: 6066910.000000
Num Path Iterations - Session Mean & Stddev: 60669.100000,17.970810
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6952200000 of size 40960 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2169916786.000000
Throughput: 216991678.600000
Num Accesses - Session Mean & Stddev: 21699167.860000,29811.316155
Num Phases Traversed: 6078298.000000
Num Phases Traversed - Session Mean & Stddev: 60782.980000,83.505087
Num Path Iterations: 6078298.000000
Num Path Iterations - Session Mean & Stddev: 60782.980000,83.505087
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa241000000 of size 40960 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2172493969.000000
Throughput: 217249396.900000
Num Accesses - Session Mean & Stddev: 21724939.690000,44144.404934
Num Phases Traversed: 6085517.000000
Num Phases Traversed - Session Mean & Stddev: 60855.170000,123.653795
Num Path Iterations: 6085517.000000
Num Path Iterations - Session Mean & Stddev: 60855.170000,123.653795
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0aa3800000 of size 40960 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2160629074.000000
Throughput: 216062907.400000
Num Accesses - Session Mean & Stddev: 21606290.740000,4126.209645
Num Phases Traversed: 6052282.000000
Num Phases Traversed - Session Mean & Stddev: 60522.820000,11.558010
Num Path Iterations: 6052282.000000
Num Path Iterations - Session Mean & Stddev: 60522.820000,11.558010
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb815600000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2172900592.000000
Throughput: 217290059.200000
Num Accesses - Session Mean & Stddev: 21729005.920000,13518.126381
Num Phases Traversed: 6086656.000000
Num Phases Traversed - Session Mean & Stddev: 60866.560000,37.865900
Num Path Iterations: 6086656.000000
Num Path Iterations - Session Mean & Stddev: 60866.560000,37.865900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6c4a400000 of size 40960 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2172742798.000000
Throughput: 217274279.800000
Num Accesses - Session Mean & Stddev: 21727427.980000,43234.307207
Num Phases Traversed: 6086214.000000
Num Phases Traversed - Session Mean & Stddev: 60862.140000,121.104502
Num Path Iterations: 6086214.000000
Num Path Iterations - Session Mean & Stddev: 60862.140000,121.104502
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0 0/320,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0 0/320,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2e10800000 of size 40960 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2171738200.000000
Throughput: 217173820.000000
Num Accesses - Session Mean & Stddev: 21717382.000000,26513.013060
Num Phases Traversed: 6083400.000000
Num Phases Traversed - Session Mean & Stddev: 60834.000000,74.266143
Num Path Iterations: 6083400.000000
Num Path Iterations - Session Mean & Stddev: 60834.000000,74.266143
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f620ca00000 of size 131072 bytes.
[1.251s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 997316758.000000
Throughput: 99731675.800000
Num Accesses - Session Mean & Stddev: 9973167.580000,4011.029075
Num Phases Traversed: 940078.000000
Num Phases Traversed - Session Mean & Stddev: 9400.780000,3.780423
Num Path Iterations: 940078.000000
Num Path Iterations - Session Mean & Stddev: 9400.780000,3.780423
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f262a600000 of size 131072 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 997482274.000000
Throughput: 99748227.400000
Num Accesses - Session Mean & Stddev: 9974822.740000,11053.990705
Num Phases Traversed: 940234.000000
Num Phases Traversed - Session Mean & Stddev: 9402.340000,10.418464
Num Path Iterations: 940234.000000
Num Path Iterations - Session Mean & Stddev: 9402.340000,10.418464
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f18f6400000 of size 131072 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 996912517.000000
Throughput: 99691251.700000
Num Accesses - Session Mean & Stddev: 9969125.170000,10068.275071
Num Phases Traversed: 939697.000000
Num Phases Traversed - Session Mean & Stddev: 9396.970000,9.489420
Num Path Iterations: 939697.000000
Num Path Iterations - Session Mean & Stddev: 9396.970000,9.489420
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4384600000 of size 131072 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 996039314.000000
Throughput: 99603931.400000
Num Accesses - Session Mean & Stddev: 9960393.140000,11943.316526
Num Phases Traversed: 938874.000000
Num Phases Traversed - Session Mean & Stddev: 9388.740000,11.256660
Num Path Iterations: 938874.000000
Num Path Iterations - Session Mean & Stddev: 9388.740000,11.256660
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fab20c00000 of size 131072 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 997313575.000000
Throughput: 99731357.500000
Num Accesses - Session Mean & Stddev: 9973135.750000,20454.129054
Num Phases Traversed: 940075.000000
Num Phases Traversed - Session Mean & Stddev: 9400.750000,19.278161
Num Path Iterations: 940075.000000
Num Path Iterations - Session Mean & Stddev: 9400.750000,19.278161
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7441600000 of size 131072 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 994970887.000000
Throughput: 99497088.700000
Num Accesses - Session Mean & Stddev: 9949708.870000,9608.999673
Num Phases Traversed: 937867.000000
Num Phases Traversed - Session Mean & Stddev: 9378.670000,9.056550
Num Path Iterations: 937867.000000
Num Path Iterations - Session Mean & Stddev: 9378.670000,9.056550
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8930a00000 of size 131072 bytes.
[1.251s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 996388383.000000
Throughput: 99638838.300000
Num Accesses - Session Mean & Stddev: 9963883.830000,6619.917324
Num Phases Traversed: 939203.000000
Num Phases Traversed - Session Mean & Stddev: 9392.030000,6.239319
Num Path Iterations: 939203.000000
Num Path Iterations - Session Mean & Stddev: 9392.030000,6.239319
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f18cce00000 of size 131072 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 996492361.000000
Throughput: 99649236.100000
Num Accesses - Session Mean & Stddev: 9964923.610000,7720.544953
Num Phases Traversed: 939301.000000
Num Phases Traversed - Session Mean & Stddev: 9393.010000,7.276668
Num Path Iterations: 939301.000000
Num Path Iterations - Session Mean & Stddev: 9393.010000,7.276668
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4539400000 of size 131072 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 996551777.000000
Throughput: 99655177.700000
Num Accesses - Session Mean & Stddev: 9965517.770000,4040.878082
Num Phases Traversed: 939357.000000
Num Phases Traversed - Session Mean & Stddev: 9393.570000,3.808556
Num Path Iterations: 939357.000000
Num Path Iterations - Session Mean & Stddev: 9393.570000,3.808556
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2901c00000 of size 131072 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 996233477.000000
Throughput: 99623347.700000
Num Accesses - Session Mean & Stddev: 9962334.770000,2349.845543
Num Phases Traversed: 939057.000000
Num Phases Traversed - Session Mean & Stddev: 9390.570000,2.214746
Num Path Iterations: 939057.000000
Num Path Iterations - Session Mean & Stddev: 9390.570000,2.214746
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0b2b200000 of size 131072 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 997482274.000000
Throughput: 99748227.400000
Num Accesses - Session Mean & Stddev: 9974822.740000,23675.387082
Num Phases Traversed: 940234.000000
Num Phases Traversed - Session Mean & Stddev: 9402.340000,22.314220
Num Path Iterations: 940234.000000
Num Path Iterations - Session Mean & Stddev: 9402.340000,22.314220
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4834800000 of size 131072 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 997466359.000000
Throughput: 99746635.900000
Num Accesses - Session Mean & Stddev: 9974663.590000,5750.375299
Num Phases Traversed: 940219.000000
Num Phases Traversed - Session Mean & Stddev: 9402.190000,5.419769
Num Path Iterations: 940219.000000
Num Path Iterations - Session Mean & Stddev: 9402.190000,5.419769
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0142600000 of size 131072 bytes.
[1.251s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 996626047.000000
Throughput: 99662604.700000
Num Accesses - Session Mean & Stddev: 9966260.470000,1080.502313
Num Phases Traversed: 939427.000000
Num Phases Traversed - Session Mean & Stddev: 9394.270000,1.018381
Num Path Iterations: 939427.000000
Num Path Iterations - Session Mean & Stddev: 9394.270000,1.018381
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe038400000 of size 131072 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 997553361.000000
Throughput: 99755336.100000
Num Accesses - Session Mean & Stddev: 9975533.610000,4819.077885
Num Phases Traversed: 940301.000000
Num Phases Traversed - Session Mean & Stddev: 9403.010000,4.542015
Num Path Iterations: 940301.000000
Num Path Iterations - Session Mean & Stddev: 9403.010000,4.542015
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f30b7600000 of size 131072 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 996543289.000000
Throughput: 99654328.900000
Num Accesses - Session Mean & Stddev: 9965432.890000,17489.126346
Num Phases Traversed: 939349.000000
Num Phases Traversed - Session Mean & Stddev: 9393.490000,16.483625
Num Path Iterations: 939349.000000
Num Path Iterations - Session Mean & Stddev: 9393.490000,16.483625
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efc83400000 of size 131072 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 996742757.000000
Throughput: 99674275.700000
Num Accesses - Session Mean & Stddev: 9967427.570000,2366.077578
Num Phases Traversed: 939537.000000
Num Phases Traversed - Session Mean & Stddev: 9395.370000,2.230045
Num Path Iterations: 939537.000000
Num Path Iterations - Session Mean & Stddev: 9395.370000,2.230045
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1ba3400000 of size 131072 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 995990508.000000
Throughput: 99599050.800000
Num Accesses - Session Mean & Stddev: 9959905.080000,6438.233327
Num Phases Traversed: 938828.000000
Num Phases Traversed - Session Mean & Stddev: 9388.280000,6.068080
Num Path Iterations: 938828.000000
Num Path Iterations - Session Mean & Stddev: 9388.280000,6.068080
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd957200000 of size 131072 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 996670609.000000
Throughput: 99667060.900000
Num Accesses - Session Mean & Stddev: 9966706.090000,36910.509562
Num Phases Traversed: 939469.000000
Num Phases Traversed - Session Mean & Stddev: 9394.690000,34.788416
Num Path Iterations: 939469.000000
Num Path Iterations - Session Mean & Stddev: 9394.690000,34.788416
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0f2a400000 of size 131072 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 997044081.000000
Throughput: 99704408.100000
Num Accesses - Session Mean & Stddev: 9970440.810000,6234.302742
Num Phases Traversed: 939821.000000
Num Phases Traversed - Session Mean & Stddev: 9398.210000,5.875874
Num Path Iterations: 939821.000000
Num Path Iterations - Session Mean & Stddev: 9398.210000,5.875874
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f262ae00000 of size 131072 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 996214379.000000
Throughput: 99621437.900000
Num Accesses - Session Mean & Stddev: 9962143.790000,6024.191317
Num Phases Traversed: 939039.000000
Num Phases Traversed - Session Mean & Stddev: 9390.390000,5.677843
Num Path Iterations: 939039.000000
Num Path Iterations - Session Mean & Stddev: 9390.390000,5.677843
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8d7e400000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 942297079.000000
Throughput: 94229707.900000
Num Accesses - Session Mean & Stddev: 9422970.790000,2514.696054
Num Phases Traversed: 402619.000000
Num Phases Traversed - Session Mean & Stddev: 4026.190000,1.074197
Num Path Iterations: 402619.000000
Num Path Iterations - Session Mean & Stddev: 4026.190000,1.074197
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f759de00000 of size 294912 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 943853844.000000
Throughput: 94385384.400000
Num Accesses - Session Mean & Stddev: 9438538.440000,2086.512695
Num Phases Traversed: 403284.000000
Num Phases Traversed - Session Mean & Stddev: 4032.840000,0.891291
Num Path Iterations: 403284.000000
Num Path Iterations - Session Mean & Stddev: 4032.840000,0.891291
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5d00e00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 941941247.000000
Throughput: 94194124.700000
Num Accesses - Session Mean & Stddev: 9419412.470000,1753.563705
Num Phases Traversed: 402467.000000
Num Phases Traversed - Session Mean & Stddev: 4024.670000,0.749066
Num Path Iterations: 402467.000000
Num Path Iterations - Session Mean & Stddev: 4024.670000,0.749066
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f47be600000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 943128134.000000
Throughput: 94312813.400000
Num Accesses - Session Mean & Stddev: 9431281.340000,2619.828988
Num Phases Traversed: 402974.000000
Num Phases Traversed - Session Mean & Stddev: 4029.740000,1.119107
Num Path Iterations: 402974.000000
Num Path Iterations - Session Mean & Stddev: 4029.740000,1.119107
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f24a6000000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 944174561.000000
Throughput: 94417456.100000
Num Accesses - Session Mean & Stddev: 9441745.610000,2127.478338
Num Phases Traversed: 403421.000000
Num Phases Traversed - Session Mean & Stddev: 4034.210000,0.908790
Num Path Iterations: 403421.000000
Num Path Iterations - Session Mean & Stddev: 4034.210000,0.908790
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4f0be00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 944432071.000000
Throughput: 94443207.100000
Num Accesses - Session Mean & Stddev: 9444320.710000,2492.807780
Num Phases Traversed: 403531.000000
Num Phases Traversed - Session Mean & Stddev: 4035.310000,1.064847
Num Path Iterations: 403531.000000
Num Path Iterations - Session Mean & Stddev: 4035.310000,1.064847
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc224e00000 of size 294912 bytes.
[1.240s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 943500353.000000
Throughput: 94350035.300000
Num Accesses - Session Mean & Stddev: 9435003.530000,3176.435951
Num Phases Traversed: 403133.000000
Num Phases Traversed - Session Mean & Stddev: 4031.330000,1.356871
Num Path Iterations: 403133.000000
Num Path Iterations - Session Mean & Stddev: 4031.330000,1.356871
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5b49e00000 of size 294912 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 943266253.000000
Throughput: 94326625.300000
Num Accesses - Session Mean & Stddev: 9432662.530000,1814.992261
Num Phases Traversed: 403033.000000
Num Phases Traversed - Session Mean & Stddev: 4030.330000,0.775306
Num Path Iterations: 403033.000000
Num Path Iterations - Session Mean & Stddev: 4030.330000,0.775306
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2cf8400000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 944057511.000000
Throughput: 94405751.100000
Num Accesses - Session Mean & Stddev: 9440575.110000,2695.608387
Num Phases Traversed: 403371.000000
Num Phases Traversed - Session Mean & Stddev: 4033.710000,1.151477
Num Path Iterations: 403371.000000
Num Path Iterations - Session Mean & Stddev: 4033.710000,1.151477
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff9c0200000 of size 294912 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 941669691.000000
Throughput: 94166969.100000
Num Accesses - Session Mean & Stddev: 9416696.910000,2638.069150
Num Phases Traversed: 402351.000000
Num Phases Traversed - Session Mean & Stddev: 4023.510000,1.126898
Num Path Iterations: 402351.000000
Num Path Iterations - Session Mean & Stddev: 4023.510000,1.126898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb110c00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 941997431.000000
Throughput: 94199743.100000
Num Accesses - Session Mean & Stddev: 9419974.310000,2198.422119
Num Phases Traversed: 402491.000000
Num Phases Traversed - Session Mean & Stddev: 4024.910000,0.939095
Num Path Iterations: 402491.000000
Num Path Iterations - Session Mean & Stddev: 4024.910000,0.939095
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9153c00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 941449637.000000
Throughput: 94144963.700000
Num Accesses - Session Mean & Stddev: 9414496.370000,1526.324819
Num Phases Traversed: 402257.000000
Num Phases Traversed - Session Mean & Stddev: 4022.570000,0.651997
Num Path Iterations: 402257.000000
Num Path Iterations - Session Mean & Stddev: 4022.570000,0.651997
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa02ac00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 943729771.000000
Throughput: 94372977.100000
Num Accesses - Session Mean & Stddev: 9437297.710000,1773.452358
Num Phases Traversed: 403231.000000
Num Phases Traversed - Session Mean & Stddev: 4032.310000,0.757562
Num Path Iterations: 403231.000000
Num Path Iterations - Session Mean & Stddev: 4032.310000,0.757562
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efc7b000000 of size 294912 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 942297079.000000
Throughput: 94229707.900000
Num Accesses - Session Mean & Stddev: 9422970.790000,2188.428104
Num Phases Traversed: 402619.000000
Num Phases Traversed - Session Mean & Stddev: 4026.190000,0.934826
Num Path Iterations: 402619.000000
Num Path Iterations - Session Mean & Stddev: 4026.190000,0.934826
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8eabc00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 942917444.000000
Throughput: 94291744.400000
Num Accesses - Session Mean & Stddev: 9429174.440000,2033.303712
Num Phases Traversed: 402884.000000
Num Phases Traversed - Session Mean & Stddev: 4028.840000,0.868562
Num Path Iterations: 402884.000000
Num Path Iterations - Session Mean & Stddev: 4028.840000,0.868562
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4365c00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 940471099.000000
Throughput: 94047109.900000
Num Accesses - Session Mean & Stddev: 9404710.990000,2267.147007
Num Phases Traversed: 401839.000000
Num Phases Traversed - Session Mean & Stddev: 4018.390000,0.968452
Num Path Iterations: 401839.000000
Num Path Iterations - Session Mean & Stddev: 4018.390000,0.968452
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f81be600000 of size 294912 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 943322437.000000
Throughput: 94332243.700000
Num Accesses - Session Mean & Stddev: 9433224.370000,2275.832501
Num Phases Traversed: 403057.000000
Num Phases Traversed - Session Mean & Stddev: 4030.570000,0.972163
Num Path Iterations: 403057.000000
Num Path Iterations - Session Mean & Stddev: 4030.570000,0.972163
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feb7d400000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 942016159.000000
Throughput: 94201615.900000
Num Accesses - Session Mean & Stddev: 9420161.590000,2352.559411
Num Phases Traversed: 402499.000000
Num Phases Traversed - Session Mean & Stddev: 4024.990000,1.004938
Num Path Iterations: 402499.000000
Num Path Iterations - Session Mean & Stddev: 4024.990000,1.004938
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f092a800000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 942938513.000000
Throughput: 94293851.300000
Num Accesses - Session Mean & Stddev: 9429385.130000,2202.407027
Num Phases Traversed: 402893.000000
Num Phases Traversed - Session Mean & Stddev: 4028.930000,0.940798
Num Path Iterations: 402893.000000
Num Path Iterations - Session Mean & Stddev: 4028.930000,0.940798
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2304,0.1 0/2304,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2304,0.1 0/2304,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff305a00000 of size 294912 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 943668905.000000
Throughput: 94366890.500000
Num Accesses - Session Mean & Stddev: 9436689.050000,2180.399910
Num Phases Traversed: 403205.000000
Num Phases Traversed - Session Mean & Stddev: 4032.050000,0.931397
Num Path Iterations: 403205.000000
Num Path Iterations - Session Mean & Stddev: 4032.050000,0.931397
+ set +x

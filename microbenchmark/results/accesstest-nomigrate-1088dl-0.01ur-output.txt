++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2886e00000 of size 69632 bytes.
[1.232s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1261656706.000000
Throughput: 126165670.600000
Num Accesses - Session Mean & Stddev: 12616567.060000,8679.922219
Num Phases Traversed: 2171626.000000
Num Phases Traversed - Session Mean & Stddev: 21716.260000,14.939625
Num Path Iterations: 2171626.000000
Num Path Iterations - Session Mean & Stddev: 21716.260000,14.939625
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3688200000 of size 69632 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1262978481.000000
Throughput: 126297848.100000
Num Accesses - Session Mean & Stddev: 12629784.810000,2362.170522
Num Phases Traversed: 2173901.000000
Num Phases Traversed - Session Mean & Stddev: 21739.010000,4.065698
Num Path Iterations: 2173901.000000
Num Path Iterations - Session Mean & Stddev: 21739.010000,4.065698
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd814200000 of size 69632 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1263865087.000000
Throughput: 126386508.700000
Num Accesses - Session Mean & Stddev: 12638650.870000,971.695360
Num Phases Traversed: 2175427.000000
Num Phases Traversed - Session Mean & Stddev: 21754.270000,1.672453
Num Path Iterations: 2175427.000000
Num Path Iterations - Session Mean & Stddev: 21754.270000,1.672453
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f83d0a00000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1261712482.000000
Throughput: 126171248.200000
Num Accesses - Session Mean & Stddev: 12617124.820000,3101.234107
Num Phases Traversed: 2171722.000000
Num Phases Traversed - Session Mean & Stddev: 21717.220000,5.337752
Num Path Iterations: 2171722.000000
Num Path Iterations - Session Mean & Stddev: 21717.220000,5.337752
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6956800000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1264405417.000000
Throughput: 126440541.700000
Num Accesses - Session Mean & Stddev: 12644054.170000,1241.370098
Num Phases Traversed: 2176357.000000
Num Phases Traversed - Session Mean & Stddev: 21763.570000,2.136609
Num Path Iterations: 2176357.000000
Num Path Iterations - Session Mean & Stddev: 21763.570000,2.136609
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb461000000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1261631142.000000
Throughput: 126163114.200000
Num Accesses - Session Mean & Stddev: 12616311.420000,611.459372
Num Phases Traversed: 2171582.000000
Num Phases Traversed - Session Mean & Stddev: 21715.820000,1.052426
Num Path Iterations: 2171582.000000
Num Path Iterations - Session Mean & Stddev: 21715.820000,1.052426
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8753200000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1259845729.000000
Throughput: 125984572.900000
Num Accesses - Session Mean & Stddev: 12598457.290000,1170.956458
Num Phases Traversed: 2168509.000000
Num Phases Traversed - Session Mean & Stddev: 21685.090000,2.015416
Num Path Iterations: 2168509.000000
Num Path Iterations - Session Mean & Stddev: 21685.090000,2.015416
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0ad1400000 of size 69632 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1261706091.000000
Throughput: 126170609.100000
Num Accesses - Session Mean & Stddev: 12617060.910000,752.591258
Num Phases Traversed: 2171711.000000
Num Phases Traversed - Session Mean & Stddev: 21717.110000,1.295338
Num Path Iterations: 2171711.000000
Num Path Iterations - Session Mean & Stddev: 21717.110000,1.295338
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7135a00000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1259814355.000000
Throughput: 125981435.500000
Num Accesses - Session Mean & Stddev: 12598143.550000,429.900741
Num Phases Traversed: 2168455.000000
Num Phases Traversed - Session Mean & Stddev: 21684.550000,0.739932
Num Path Iterations: 2168455.000000
Num Path Iterations - Session Mean & Stddev: 21684.550000,0.739932
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f68a7c00000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1263465359.000000
Throughput: 126346535.900000
Num Accesses - Session Mean & Stddev: 12634653.590000,656.787029
Num Phases Traversed: 2174739.000000
Num Phases Traversed - Session Mean & Stddev: 21747.390000,1.130442
Num Path Iterations: 2174739.000000
Num Path Iterations - Session Mean & Stddev: 21747.390000,1.130442
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5376a00000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1263000559.000000
Throughput: 126300055.900000
Num Accesses - Session Mean & Stddev: 12630005.590000,1474.172297
Num Phases Traversed: 2173939.000000
Num Phases Traversed - Session Mean & Stddev: 21739.390000,2.537302
Num Path Iterations: 2173939.000000
Num Path Iterations - Session Mean & Stddev: 21739.390000,2.537302
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdd0d800000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1260808446.000000
Throughput: 126080844.600000
Num Accesses - Session Mean & Stddev: 12608084.460000,2597.297451
Num Phases Traversed: 2170166.000000
Num Phases Traversed - Session Mean & Stddev: 21701.660000,4.470391
Num Path Iterations: 2170166.000000
Num Path Iterations - Session Mean & Stddev: 21701.660000,4.470391
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0ea7600000 of size 69632 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1264650599.000000
Throughput: 126465059.900000
Num Accesses - Session Mean & Stddev: 12646505.990000,1589.637660
Num Phases Traversed: 2176779.000000
Num Phases Traversed - Session Mean & Stddev: 21767.790000,2.736037
Num Path Iterations: 2176779.000000
Num Path Iterations - Session Mean & Stddev: 21767.790000,2.736037
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6b6d400000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1265375106.000000
Throughput: 126537510.600000
Num Accesses - Session Mean & Stddev: 12653751.060000,2231.554368
Num Phases Traversed: 2178026.000000
Num Phases Traversed - Session Mean & Stddev: 21780.260000,3.840885
Num Path Iterations: 2178026.000000
Num Path Iterations - Session Mean & Stddev: 21780.260000,3.840885
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f24db000000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1264413551.000000
Throughput: 126441355.100000
Num Accesses - Session Mean & Stddev: 12644135.510000,1557.459338
Num Phases Traversed: 2176371.000000
Num Phases Traversed - Session Mean & Stddev: 21763.710000,2.680653
Num Path Iterations: 2176371.000000
Num Path Iterations - Session Mean & Stddev: 21763.710000,2.680653
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff673600000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1267484136.000000
Throughput: 126748413.600000
Num Accesses - Session Mean & Stddev: 12674841.360000,1915.221358
Num Phases Traversed: 2181656.000000
Num Phases Traversed - Session Mean & Stddev: 21816.560000,3.296422
Num Path Iterations: 2181656.000000
Num Path Iterations - Session Mean & Stddev: 21816.560000,3.296422
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcc58800000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1260874099.000000
Throughput: 126087409.900000
Num Accesses - Session Mean & Stddev: 12608740.990000,980.135118
Num Phases Traversed: 2170279.000000
Num Phases Traversed - Session Mean & Stddev: 21702.790000,1.686980
Num Path Iterations: 2170279.000000
Num Path Iterations - Session Mean & Stddev: 21702.790000,1.686980
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3f2ce00000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1261634047.000000
Throughput: 126163404.700000
Num Accesses - Session Mean & Stddev: 12616340.470000,3175.530430
Num Phases Traversed: 2171587.000000
Num Phases Traversed - Session Mean & Stddev: 21715.870000,5.465629
Num Path Iterations: 2171587.000000
Num Path Iterations - Session Mean & Stddev: 21715.870000,5.465629
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8989200000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1262256879.000000
Throughput: 126225687.900000
Num Accesses - Session Mean & Stddev: 12622568.790000,1888.196368
Num Phases Traversed: 2172659.000000
Num Phases Traversed - Session Mean & Stddev: 21726.590000,3.249908
Num Path Iterations: 2172659.000000
Num Path Iterations - Session Mean & Stddev: 21726.590000,3.249908
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/544,0.01 0/544,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/544,0.01 0/544,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    544 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8685600000 of size 69632 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1263525783.000000
Throughput: 126352578.300000
Num Accesses - Session Mean & Stddev: 12635257.830000,1072.114248
Num Phases Traversed: 2174843.000000
Num Phases Traversed - Session Mean & Stddev: 21748.430000,1.845291
Num Path Iterations: 2174843.000000
Num Path Iterations - Session Mean & Stddev: 21748.430000,1.845291
+ set +x

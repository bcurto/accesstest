++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f04fc600000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947141985.000000
Throughput: 194714198.500000
Num Accesses - Session Mean & Stddev: 19471419.850000,1404.708307
Num Phases Traversed: 933981.000000
Num Phases Traversed - Session Mean & Stddev: 9339.810000,0.673721
Num Path Iterations: 933981.000000
Num Path Iterations - Session Mean & Stddev: 9339.810000,0.673721
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff540c00000 of size 262144 bytes.
[1.236s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1947394270.000000
Throughput: 194739427.000000
Num Accesses - Session Mean & Stddev: 19473942.700000,1413.500835
Num Phases Traversed: 934102.000000
Num Phases Traversed - Session Mean & Stddev: 9341.020000,0.677938
Num Path Iterations: 934102.000000
Num Path Iterations - Session Mean & Stddev: 9341.020000,0.677938
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0e59200000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947654895.000000
Throughput: 194765489.500000
Num Accesses - Session Mean & Stddev: 19476548.950000,1346.561379
Num Phases Traversed: 934227.000000
Num Phases Traversed - Session Mean & Stddev: 9342.270000,0.645833
Num Path Iterations: 934227.000000
Num Path Iterations - Session Mean & Stddev: 9342.270000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f37c0200000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1946764600.000000
Throughput: 194676460.000000
Num Accesses - Session Mean & Stddev: 19467646.000000,1444.530374
Num Phases Traversed: 933800.000000
Num Phases Traversed - Session Mean & Stddev: 9338.000000,0.692820
Num Path Iterations: 933800.000000
Num Path Iterations - Session Mean & Stddev: 9338.000000,0.692820
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2177400000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947269170.000000
Throughput: 194726917.000000
Num Accesses - Session Mean & Stddev: 19472691.700000,1148.833108
Num Phases Traversed: 934042.000000
Num Phases Traversed - Session Mean & Stddev: 9340.420000,0.550999
Num Path Iterations: 934042.000000
Num Path Iterations - Session Mean & Stddev: 9340.420000,0.550999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0773600000 of size 262144 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947340060.000000
Throughput: 194734006.000000
Num Accesses - Session Mean & Stddev: 19473400.600000,1355.089606
Num Phases Traversed: 934076.000000
Num Phases Traversed - Session Mean & Stddev: 9340.760000,0.649923
Num Path Iterations: 934076.000000
Num Path Iterations - Session Mean & Stddev: 9340.760000,0.649923
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7532400000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947056500.000000
Throughput: 194705650.000000
Num Accesses - Session Mean & Stddev: 19470565.000000,1179.454111
Num Phases Traversed: 933940.000000
Num Phases Traversed - Session Mean & Stddev: 9339.400000,0.565685
Num Path Iterations: 933940.000000
Num Path Iterations - Session Mean & Stddev: 9339.400000,0.565685
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fae29800000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947206620.000000
Throughput: 194720662.000000
Num Accesses - Session Mean & Stddev: 19472066.200000,1422.697424
Num Phases Traversed: 934012.000000
Num Phases Traversed - Session Mean & Stddev: 9340.120000,0.682349
Num Path Iterations: 934012.000000
Num Path Iterations - Session Mean & Stddev: 9340.120000,0.682349
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9e7e600000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1946612395.000000
Throughput: 194661239.500000
Num Accesses - Session Mean & Stddev: 19466123.950000,1346.561379
Num Phases Traversed: 933727.000000
Num Phases Traversed - Session Mean & Stddev: 9337.270000,0.645833
Num Path Iterations: 933727.000000
Num Path Iterations - Session Mean & Stddev: 9337.270000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe252a00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947156580.000000
Throughput: 194715658.000000
Num Accesses - Session Mean & Stddev: 19471565.800000,1422.697424
Num Phases Traversed: 933988.000000
Num Phases Traversed - Session Mean & Stddev: 9339.880000,0.682349
Num Path Iterations: 933988.000000
Num Path Iterations - Session Mean & Stddev: 9339.880000,0.682349
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fea3e400000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947400525.000000
Throughput: 194740052.500000
Num Accesses - Session Mean & Stddev: 19474005.250000,1455.772025
Num Phases Traversed: 934105.000000
Num Phases Traversed - Session Mean & Stddev: 9341.050000,0.698212
Num Path Iterations: 934105.000000
Num Path Iterations - Session Mean & Stddev: 9341.050000,0.698212
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f793ea00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1946827150.000000
Throughput: 194682715.000000
Num Accesses - Session Mean & Stddev: 19468271.500000,1302.082083
Num Phases Traversed: 933830.000000
Num Phases Traversed - Session Mean & Stddev: 9338.300000,0.624500
Num Path Iterations: 933830.000000
Num Path Iterations - Session Mean & Stddev: 9338.300000,0.624500
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0940200000 of size 262144 bytes.
[1.236s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947152410.000000
Throughput: 194715241.000000
Num Accesses - Session Mean & Stddev: 19471524.100000,1474.907248
Num Phases Traversed: 933986.000000
Num Phases Traversed - Session Mean & Stddev: 9339.860000,0.707390
Num Path Iterations: 933986.000000
Num Path Iterations - Session Mean & Stddev: 9339.860000,0.707390
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbfe0800000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947018970.000000
Throughput: 194701897.000000
Num Accesses - Session Mean & Stddev: 19470189.700000,1369.767247
Num Phases Traversed: 933922.000000
Num Phases Traversed - Session Mean & Stddev: 9339.220000,0.656963
Num Path Iterations: 933922.000000
Num Path Iterations - Session Mean & Stddev: 9339.220000,0.656963
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4001a00000 of size 262144 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947381760.000000
Throughput: 194738176.000000
Num Accesses - Session Mean & Stddev: 19473817.600000,1442.120813
Num Phases Traversed: 934096.000000
Num Phases Traversed - Session Mean & Stddev: 9340.960000,0.691665
Num Path Iterations: 934096.000000
Num Path Iterations - Session Mean & Stddev: 9340.960000,0.691665
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe27cc00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947304615.000000
Throughput: 194730461.500000
Num Accesses - Session Mean & Stddev: 19473046.150000,1145.612163
Num Phases Traversed: 934059.000000
Num Phases Traversed - Session Mean & Stddev: 9340.590000,0.549454
Num Path Iterations: 934059.000000
Num Path Iterations - Session Mean & Stddev: 9340.590000,0.549454
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe635200000 of size 262144 bytes.
[1.267s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947365080.000000
Throughput: 194736508.000000
Num Accesses - Session Mean & Stddev: 19473650.800000,1422.697424
Num Phases Traversed: 934088.000000
Num Phases Traversed - Session Mean & Stddev: 9340.880000,0.682349
Num Path Iterations: 934088.000000
Num Path Iterations - Session Mean & Stddev: 9340.880000,0.682349
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4f3a400000 of size 262144 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947194110.000000
Throughput: 194719411.000000
Num Accesses - Session Mean & Stddev: 19471941.100000,1439.103189
Num Phases Traversed: 934006.000000
Num Phases Traversed - Session Mean & Stddev: 9340.060000,0.690217
Num Path Iterations: 934006.000000
Num Path Iterations - Session Mean & Stddev: 9340.060000,0.690217
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f520ca00000 of size 262144 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947413035.000000
Throughput: 194741303.500000
Num Accesses - Session Mean & Stddev: 19474130.350000,1410.884236
Num Phases Traversed: 934111.000000
Num Phases Traversed - Session Mean & Stddev: 9341.110000,0.676683
Num Path Iterations: 934111.000000
Num Path Iterations - Session Mean & Stddev: 9341.110000,0.676683
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2048,1 0/2048,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2048,1 0/2048,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2048 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feaa7600000 of size 262144 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1947260830.000000
Throughput: 194726083.000000
Num Accesses - Session Mean & Stddev: 19472608.300000,1207.861213
Num Phases Traversed: 934038.000000
Num Phases Traversed - Session Mean & Stddev: 9340.380000,0.579310
Num Path Iterations: 934038.000000
Num Path Iterations - Session Mean & Stddev: 9340.380000,0.579310
+ set +x

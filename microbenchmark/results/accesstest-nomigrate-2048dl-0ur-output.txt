++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f216e200000 of size 131072 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120263243.000000
Throughput: 212026324.300000
Num Accesses - Session Mean & Stddev: 21202632.430000,979.975808
Num Phases Traversed: 1998463.000000
Num Phases Traversed - Session Mean & Stddev: 19984.630000,0.923634
Num Path Iterations: 1998463.000000
Num Path Iterations - Session Mean & Stddev: 19984.630000,0.923634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f597e400000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2122876486.000000
Throughput: 212287648.600000
Num Accesses - Session Mean & Stddev: 21228764.860000,1233.865909
Num Phases Traversed: 2000926.000000
Num Phases Traversed - Session Mean & Stddev: 20009.260000,1.162927
Num Path Iterations: 2000926.000000
Num Path Iterations - Session Mean & Stddev: 20009.260000,1.162927
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faf11600000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2122260045.000000
Throughput: 212226004.500000
Num Accesses - Session Mean & Stddev: 21222600.450000,7791.490180
Num Phases Traversed: 2000345.000000
Num Phases Traversed - Session Mean & Stddev: 20003.450000,7.343535
Num Path Iterations: 2000345.000000
Num Path Iterations - Session Mean & Stddev: 20003.450000,7.343535
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7bf8400000 of size 131072 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2121993734.000000
Throughput: 212199373.400000
Num Accesses - Session Mean & Stddev: 21219937.340000,13642.677800
Num Phases Traversed: 2000094.000000
Num Phases Traversed - Session Mean & Stddev: 20000.940000,12.858320
Num Path Iterations: 2000094.000000
Num Path Iterations - Session Mean & Stddev: 20000.940000,12.858320
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3d13e00000 of size 131072 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2119357149.000000
Throughput: 211935714.900000
Num Accesses - Session Mean & Stddev: 21193571.490000,1051.354103
Num Phases Traversed: 1997609.000000
Num Phases Traversed - Session Mean & Stddev: 19976.090000,0.990909
Num Path Iterations: 1997609.000000
Num Path Iterations - Session Mean & Stddev: 19976.090000,0.990909
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe5a4e00000 of size 131072 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120402234.000000
Throughput: 212040223.400000
Num Accesses - Session Mean & Stddev: 21204022.340000,1521.490277
Num Phases Traversed: 1998594.000000
Num Phases Traversed - Session Mean & Stddev: 19985.940000,1.434015
Num Path Iterations: 1998594.000000
Num Path Iterations - Session Mean & Stddev: 19985.940000,1.434015
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efcbec00000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2119885527.000000
Throughput: 211988552.700000
Num Accesses - Session Mean & Stddev: 21198855.270000,1135.369155
Num Phases Traversed: 1998107.000000
Num Phases Traversed - Session Mean & Stddev: 19981.070000,1.070093
Num Path Iterations: 1998107.000000
Num Path Iterations - Session Mean & Stddev: 19981.070000,1.070093
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe364400000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120497724.000000
Throughput: 212049772.400000
Num Accesses - Session Mean & Stddev: 21204977.240000,969.175733
Num Phases Traversed: 1998684.000000
Num Phases Traversed - Session Mean & Stddev: 19986.840000,0.913455
Num Path Iterations: 1998684.000000
Num Path Iterations - Session Mean & Stddev: 19986.840000,0.913455
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2c56000000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2119902503.000000
Throughput: 211990250.300000
Num Accesses - Session Mean & Stddev: 21199025.030000,1131.396195
Num Phases Traversed: 1998123.000000
Num Phases Traversed - Session Mean & Stddev: 19981.230000,1.066349
Num Path Iterations: 1998123.000000
Num Path Iterations - Session Mean & Stddev: 19981.230000,1.066349
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb6d7400000 of size 131072 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120457406.000000
Throughput: 212045740.600000
Num Accesses - Session Mean & Stddev: 21204574.060000,2217.566147
Num Phases Traversed: 1998646.000000
Num Phases Traversed - Session Mean & Stddev: 19986.460000,2.090072
Num Path Iterations: 1998646.000000
Num Path Iterations - Session Mean & Stddev: 19986.460000,2.090072
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fab76400000 of size 131072 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120332208.000000
Throughput: 212033220.800000
Num Accesses - Session Mean & Stddev: 21203322.080000,1325.867985
Num Phases Traversed: 1998528.000000
Num Phases Traversed - Session Mean & Stddev: 19985.280000,1.249640
Num Path Iterations: 1998528.000000
Num Path Iterations - Session Mean & Stddev: 19985.280000,1.249640
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3656200000 of size 131072 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2119346539.000000
Throughput: 211934653.900000
Num Accesses - Session Mean & Stddev: 21193465.390000,1598.522473
Num Phases Traversed: 1997599.000000
Num Phases Traversed - Session Mean & Stddev: 19975.990000,1.506619
Num Path Iterations: 1997599.000000
Num Path Iterations - Session Mean & Stddev: 19975.990000,1.506619
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa002400000 of size 131072 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120831939.000000
Throughput: 212083193.900000
Num Accesses - Session Mean & Stddev: 21208319.390000,1097.455647
Num Phases Traversed: 1998999.000000
Num Phases Traversed - Session Mean & Stddev: 19989.990000,1.034360
Num Path Iterations: 1998999.000000
Num Path Iterations - Session Mean & Stddev: 19989.990000,1.034360
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4b8e600000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2121115226.000000
Throughput: 212111522.600000
Num Accesses - Session Mean & Stddev: 21211152.260000,2276.679550
Num Phases Traversed: 1999266.000000
Num Phases Traversed - Session Mean & Stddev: 19992.660000,2.145787
Num Path Iterations: 1999266.000000
Num Path Iterations - Session Mean & Stddev: 19992.660000,2.145787
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2aa8600000 of size 131072 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2121940684.000000
Throughput: 212194068.400000
Num Accesses - Session Mean & Stddev: 21219406.840000,4781.144310
Num Phases Traversed: 2000044.000000
Num Phases Traversed - Session Mean & Stddev: 20000.440000,4.506262
Num Path Iterations: 2000044.000000
Num Path Iterations - Session Mean & Stddev: 20000.440000,4.506262
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faba8a00000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120370404.000000
Throughput: 212037040.400000
Num Accesses - Session Mean & Stddev: 21203704.040000,1223.788878
Num Phases Traversed: 1998564.000000
Num Phases Traversed - Session Mean & Stddev: 19985.640000,1.153430
Num Path Iterations: 1998564.000000
Num Path Iterations - Session Mean & Stddev: 19985.640000,1.153430
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb1db200000 of size 131072 bytes.
[1.233s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120061653.000000
Throughput: 212006165.300000
Num Accesses - Session Mean & Stddev: 21200616.530000,1461.371037
Num Phases Traversed: 1998273.000000
Num Phases Traversed - Session Mean & Stddev: 19982.730000,1.377353
Num Path Iterations: 1998273.000000
Num Path Iterations - Session Mean & Stddev: 19982.730000,1.377353
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8038e00000 of size 131072 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2121502491.000000
Throughput: 212150249.100000
Num Accesses - Session Mean & Stddev: 21215024.910000,13200.558469
Num Phases Traversed: 1999631.000000
Num Phases Traversed - Session Mean & Stddev: 19996.310000,12.441620
Num Path Iterations: 1999631.000000
Num Path Iterations - Session Mean & Stddev: 19996.310000,12.441620
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa573e00000 of size 131072 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120278097.000000
Throughput: 212027809.700000
Num Accesses - Session Mean & Stddev: 21202780.970000,1037.992085
Num Phases Traversed: 1998477.000000
Num Phases Traversed - Session Mean & Stddev: 19984.770000,0.978315
Num Path Iterations: 1998477.000000
Num Path Iterations - Session Mean & Stddev: 19984.770000,0.978315
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0 0/1024,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0 0/1024,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f58a8c00000 of size 131072 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2122672774.000000
Throughput: 212267277.400000
Num Accesses - Session Mean & Stddev: 21226727.740000,7486.210293
Num Phases Traversed: 2000734.000000
Num Phases Traversed - Session Mean & Stddev: 20007.340000,7.055806
Num Path Iterations: 2000734.000000
Num Path Iterations - Session Mean & Stddev: 20007.340000,7.055806
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1963e00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1245584935.000000
Throughput: 124558493.500000
Num Accesses - Session Mean & Stddev: 12455849.350000,14179.638086
Num Phases Traversed: 2409355.000000
Num Phases Traversed - Session Mean & Stddev: 24093.550000,27.426766
Num Path Iterations: 2409355.000000
Num Path Iterations - Session Mean & Stddev: 24093.550000,27.426766
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7d44a00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1244284163.000000
Throughput: 124428416.300000
Num Accesses - Session Mean & Stddev: 12442841.630000,4863.590772
Num Phases Traversed: 2406839.000000
Num Phases Traversed - Session Mean & Stddev: 24068.390000,9.407332
Num Path Iterations: 2406839.000000
Num Path Iterations - Session Mean & Stddev: 24068.390000,9.407332
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb1bfc00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1246472107.000000
Throughput: 124647210.700000
Num Accesses - Session Mean & Stddev: 12464721.070000,7602.641013
Num Phases Traversed: 2411071.000000
Num Phases Traversed - Session Mean & Stddev: 24110.710000,14.705302
Num Path Iterations: 2411071.000000
Num Path Iterations - Session Mean & Stddev: 24110.710000,14.705302
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2989400000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1244005500.000000
Throughput: 124400550.000000
Num Accesses - Session Mean & Stddev: 12440055.000000,2254.736521
Num Phases Traversed: 2406300.000000
Num Phases Traversed - Session Mean & Stddev: 24063.000000,4.361192
Num Path Iterations: 2406300.000000
Num Path Iterations - Session Mean & Stddev: 24063.000000,4.361192
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fee3fc00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1245655247.000000
Throughput: 124565524.700000
Num Accesses - Session Mean & Stddev: 12456552.470000,7038.834910
Num Phases Traversed: 2409491.000000
Num Phases Traversed - Session Mean & Stddev: 24094.910000,13.614768
Num Path Iterations: 2409491.000000
Num Path Iterations - Session Mean & Stddev: 24094.910000,13.614768
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f43ab200000 of size 61440 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1251098740.000000
Throughput: 125109874.000000
Num Accesses - Session Mean & Stddev: 12510987.400000,29639.028786
Num Phases Traversed: 2420020.000000
Num Phases Traversed - Session Mean & Stddev: 24200.200000,57.328876
Num Path Iterations: 2420020.000000
Num Path Iterations - Session Mean & Stddev: 24200.200000,57.328876
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3d0fc00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1250896593.000000
Throughput: 125089659.300000
Num Accesses - Session Mean & Stddev: 12508965.930000,16636.686464
Num Phases Traversed: 2419629.000000
Num Phases Traversed - Session Mean & Stddev: 24196.290000,32.179277
Num Path Iterations: 2419629.000000
Num Path Iterations - Session Mean & Stddev: 24196.290000,32.179277
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa5bb800000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1249135174.000000
Throughput: 124913517.400000
Num Accesses - Session Mean & Stddev: 12491351.740000,12782.260156
Num Phases Traversed: 2416222.000000
Num Phases Traversed - Session Mean & Stddev: 24162.220000,24.723907
Num Path Iterations: 2416222.000000
Num Path Iterations - Session Mean & Stddev: 24162.220000,24.723907
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3520200000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1246527943.000000
Throughput: 124652794.300000
Num Accesses - Session Mean & Stddev: 12465279.430000,36611.288107
Num Phases Traversed: 2411179.000000
Num Phases Traversed - Session Mean & Stddev: 24111.790000,70.814871
Num Path Iterations: 2411179.000000
Num Path Iterations - Session Mean & Stddev: 24111.790000,70.814871
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffbd7600000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1247143173.000000
Throughput: 124714317.300000
Num Accesses - Session Mean & Stddev: 12471431.730000,9673.481712
Num Phases Traversed: 2412369.000000
Num Phases Traversed - Session Mean & Stddev: 24123.690000,18.710796
Num Path Iterations: 2412369.000000
Num Path Iterations - Session Mean & Stddev: 24123.690000,18.710796
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f72b3000000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1247785287.000000
Throughput: 124778528.700000
Num Accesses - Session Mean & Stddev: 12477852.870000,22871.029966
Num Phases Traversed: 2413611.000000
Num Phases Traversed - Session Mean & Stddev: 24136.110000,44.237969
Num Path Iterations: 2413611.000000
Num Path Iterations - Session Mean & Stddev: 24136.110000,44.237969
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe9df400000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1245976304.000000
Throughput: 124597630.400000
Num Accesses - Session Mean & Stddev: 12459763.040000,8050.033663
Num Phases Traversed: 2410112.000000
Num Phases Traversed - Session Mean & Stddev: 24101.120000,15.570665
Num Path Iterations: 2410112.000000
Num Path Iterations - Session Mean & Stddev: 24101.120000,15.570665
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0da4800000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1247450788.000000
Throughput: 124745078.800000
Num Accesses - Session Mean & Stddev: 12474507.880000,11325.609261
Num Phases Traversed: 2412964.000000
Num Phases Traversed - Session Mean & Stddev: 24129.640000,21.906401
Num Path Iterations: 2412964.000000
Num Path Iterations - Session Mean & Stddev: 24129.640000,21.906401
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa562e00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1246504678.000000
Throughput: 124650467.800000
Num Accesses - Session Mean & Stddev: 12465046.780000,10822.285807
Num Phases Traversed: 2411134.000000
Num Phases Traversed - Session Mean & Stddev: 24111.340000,20.932855
Num Path Iterations: 2411134.000000
Num Path Iterations - Session Mean & Stddev: 24111.340000,20.932855
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f00a5c00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1245336775.000000
Throughput: 124533677.500000
Num Accesses - Session Mean & Stddev: 12453367.750000,8074.598290
Num Phases Traversed: 2408875.000000
Num Phases Traversed - Session Mean & Stddev: 24088.750000,15.618179
Num Path Iterations: 2408875.000000
Num Path Iterations - Session Mean & Stddev: 24088.750000,15.618179
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fceeca00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1246623588.000000
Throughput: 124662358.800000
Num Accesses - Session Mean & Stddev: 12466235.880000,21782.888069
Num Phases Traversed: 2411364.000000
Num Phases Traversed - Session Mean & Stddev: 24113.640000,42.133246
Num Path Iterations: 2411364.000000
Num Path Iterations - Session Mean & Stddev: 24113.640000,42.133246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcf2de00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1244747395.000000
Throughput: 124474739.500000
Num Accesses - Session Mean & Stddev: 12447473.950000,875.471991
Num Phases Traversed: 2407735.000000
Num Phases Traversed - Session Mean & Stddev: 24077.350000,1.693369
Num Path Iterations: 2407735.000000
Num Path Iterations - Session Mean & Stddev: 24077.350000,1.693369
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe1b7600000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1248585603.000000
Throughput: 124858560.300000
Num Accesses - Session Mean & Stddev: 12485856.030000,4077.484966
Num Phases Traversed: 2415159.000000
Num Phases Traversed - Session Mean & Stddev: 24151.590000,7.886818
Num Path Iterations: 2415159.000000
Num Path Iterations - Session Mean & Stddev: 24151.590000,7.886818
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc74be00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1251458055.000000
Throughput: 125145805.500000
Num Accesses - Session Mean & Stddev: 12514580.550000,24661.861905
Num Phases Traversed: 2420715.000000
Num Phases Traversed - Session Mean & Stddev: 24207.150000,47.701861
Num Path Iterations: 2420715.000000
Num Path Iterations - Session Mean & Stddev: 24207.150000,47.701861
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.01 0/480,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.01 0/480,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0b75000000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1249665099.000000
Throughput: 124966509.900000
Num Accesses - Session Mean & Stddev: 12496650.990000,32653.898035
Num Phases Traversed: 2417247.000000
Num Phases Traversed - Session Mean & Stddev: 24172.470000,63.160344
Num Path Iterations: 2417247.000000
Num Path Iterations - Session Mean & Stddev: 24172.470000,63.160344
+ set +x

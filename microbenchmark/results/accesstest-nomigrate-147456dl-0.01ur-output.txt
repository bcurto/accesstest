++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1880000000 of size 9437184 bytes.
[1.823s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829708820.000000
Throughput: 82970882.000000
Num Accesses - Session Mean & Stddev: 8297088.200000,79090.391317
Num Phases Traversed: 11348.000000
Num Phases Traversed - Session Mean & Stddev: 113.480000,1.072194
Num Path Iterations: 11348.000000
Num Path Iterations - Session Mean & Stddev: 113.480000,1.072194
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2680000000 of size 9437184 bytes.
[1.824s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 830151410.000000
Throughput: 83015141.000000
Num Accesses - Session Mean & Stddev: 8301514.100000,79734.471672
Num Phases Traversed: 11354.000000
Num Phases Traversed - Session Mean & Stddev: 113.540000,1.080926
Num Path Iterations: 11354.000000
Num Path Iterations - Session Mean & Stddev: 113.540000,1.080926
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8540000000 of size 9437184 bytes.
[1.831s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829708820.000000
Throughput: 82970882.000000
Num Accesses - Session Mean & Stddev: 8297088.200000,83115.841003
Num Phases Traversed: 11348.000000
Num Phases Traversed - Session Mean & Stddev: 113.480000,1.126765
Num Path Iterations: 11348.000000
Num Path Iterations - Session Mean & Stddev: 113.480000,1.126765
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa800000000 of size 9437184 bytes.
[1.823s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829635055.000000
Throughput: 82963505.500000
Num Accesses - Session Mean & Stddev: 8296350.550000,86311.354471
Num Phases Traversed: 11347.000000
Num Phases Traversed - Session Mean & Stddev: 113.470000,1.170085
Num Path Iterations: 11347.000000
Num Path Iterations - Session Mean & Stddev: 113.470000,1.170085
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd580000000 of size 9437184 bytes.
[1.824s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830003880.000000
Throughput: 83000388.000000
Num Accesses - Session Mean & Stddev: 8300038.800000,85057.154544
Num Phases Traversed: 11352.000000
Num Phases Traversed - Session Mean & Stddev: 113.520000,1.153083
Num Path Iterations: 11352.000000
Num Path Iterations - Session Mean & Stddev: 113.520000,1.153083
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f78c0000000 of size 9437184 bytes.
[1.835s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 829413760.000000
Throughput: 82941376.000000
Num Accesses - Session Mean & Stddev: 8294137.600000,84311.813846
Num Phases Traversed: 11344.000000
Num Phases Traversed - Session Mean & Stddev: 113.440000,1.142979
Num Path Iterations: 11344.000000
Num Path Iterations - Session Mean & Stddev: 113.440000,1.142979
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0d40000000 of size 9437184 bytes.
[1.823s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829561290.000000
Throughput: 82956129.000000
Num Accesses - Session Mean & Stddev: 8295612.900000,83728.961536
Num Phases Traversed: 11346.000000
Num Phases Traversed - Session Mean & Stddev: 113.460000,1.135077
Num Path Iterations: 11346.000000
Num Path Iterations - Session Mean & Stddev: 113.460000,1.135077
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0240000000 of size 9437184 bytes.
[1.824s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829708820.000000
Throughput: 82970882.000000
Num Accesses - Session Mean & Stddev: 8297088.200000,83115.841003
Num Phases Traversed: 11348.000000
Num Phases Traversed - Session Mean & Stddev: 113.480000,1.126765
Num Path Iterations: 11348.000000
Num Path Iterations - Session Mean & Stddev: 113.480000,1.126765
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5500000000 of size 9437184 bytes.
[1.792s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829708820.000000
Throughput: 82970882.000000
Num Accesses - Session Mean & Stddev: 8297088.200000,83115.841003
Num Phases Traversed: 11348.000000
Num Phases Traversed - Session Mean & Stddev: 113.480000,1.126765
Num Path Iterations: 11348.000000
Num Path Iterations - Session Mean & Stddev: 113.480000,1.126765
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f53c0000000 of size 9437184 bytes.
[1.822s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829782585.000000
Throughput: 82978258.500000
Num Accesses - Session Mean & Stddev: 8297825.850000,83125.660348
Num Phases Traversed: 11349.000000
Num Phases Traversed - Session Mean & Stddev: 113.490000,1.126898
Num Path Iterations: 11349.000000
Num Path Iterations - Session Mean & Stddev: 113.490000,1.126898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faa80000000 of size 9437184 bytes.
[1.824s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829930115.000000
Throughput: 82993011.500000
Num Accesses - Session Mean & Stddev: 8299301.150000,87587.978283
Num Phases Traversed: 11351.000000
Num Phases Traversed - Session Mean & Stddev: 113.510000,1.187392
Num Path Iterations: 11351.000000
Num Path Iterations - Session Mean & Stddev: 113.510000,1.187392
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0400000000 of size 9437184 bytes.
[1.835s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 829856350.000000
Throughput: 82985635.000000
Num Accesses - Session Mean & Stddev: 8298563.500000,82471.777180
Num Phases Traversed: 11350.000000
Num Phases Traversed - Session Mean & Stddev: 113.500000,1.118034
Num Path Iterations: 11350.000000
Num Path Iterations - Session Mean & Stddev: 113.500000,1.118034
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe180000000 of size 9437184 bytes.
[1.824s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829708820.000000
Throughput: 82970882.000000
Num Accesses - Session Mean & Stddev: 8297088.200000,82458.580640
Num Phases Traversed: 11348.000000
Num Phases Traversed - Session Mean & Stddev: 113.480000,1.117855
Num Path Iterations: 11348.000000
Num Path Iterations - Session Mean & Stddev: 113.480000,1.117855
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa100000000 of size 9437184 bytes.
[1.823s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830003880.000000
Throughput: 83000388.000000
Num Accesses - Session Mean & Stddev: 8300038.800000,83115.841003
Num Phases Traversed: 11352.000000
Num Phases Traversed - Session Mean & Stddev: 113.520000,1.126765
Num Path Iterations: 11352.000000
Num Path Iterations - Session Mean & Stddev: 113.520000,1.126765
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5800000000 of size 9437184 bytes.
[1.819s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830003880.000000
Throughput: 83000388.000000
Num Accesses - Session Mean & Stddev: 8300038.800000,82458.580640
Num Phases Traversed: 11352.000000
Num Phases Traversed - Session Mean & Stddev: 113.520000,1.117855
Num Path Iterations: 11352.000000
Num Path Iterations - Session Mean & Stddev: 113.520000,1.117855
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff700000000 of size 9437184 bytes.
[1.826s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829708820.000000
Throughput: 82970882.000000
Num Accesses - Session Mean & Stddev: 8297088.200000,76288.845716
Num Phases Traversed: 11348.000000
Num Phases Traversed - Session Mean & Stddev: 113.480000,1.034215
Num Path Iterations: 11348.000000
Num Path Iterations - Session Mean & Stddev: 113.480000,1.034215
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3080000000 of size 9437184 bytes.
[1.824s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 829930115.000000
Throughput: 82993011.500000
Num Accesses - Session Mean & Stddev: 8299301.150000,79785.637089
Num Phases Traversed: 11351.000000
Num Phases Traversed - Session Mean & Stddev: 113.510000,1.081619
Num Path Iterations: 11351.000000
Num Path Iterations - Session Mean & Stddev: 113.510000,1.081619
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1140000000 of size 9437184 bytes.
[1.823s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829561290.000000
Throughput: 82956129.000000
Num Accesses - Session Mean & Stddev: 8295612.900000,79734.471672
Num Phases Traversed: 11346.000000
Num Phases Traversed - Session Mean & Stddev: 113.460000,1.080926
Num Path Iterations: 11346.000000
Num Path Iterations - Session Mean & Stddev: 113.460000,1.080926
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9e00000000 of size 9437184 bytes.
[1.824s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 829856350.000000
Throughput: 82985635.000000
Num Accesses - Session Mean & Stddev: 8298563.500000,86339.718891
Num Phases Traversed: 11350.000000
Num Phases Traversed - Session Mean & Stddev: 113.500000,1.170470
Num Path Iterations: 11350.000000
Num Path Iterations - Session Mean & Stddev: 113.500000,1.170470
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/73728,0.01 0/73728,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/73728,0.01 0/73728,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    73728 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4600000000 of size 9437184 bytes.
[1.828s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 830003880.000000
Throughput: 83000388.000000
Num Accesses - Session Mean & Stddev: 8300038.800000,85694.486658
Num Phases Traversed: 11352.000000
Num Phases Traversed - Session Mean & Stddev: 113.520000,1.161723
Num Path Iterations: 11352.000000
Num Path Iterations - Session Mean & Stddev: 113.520000,1.161723
+ set +x

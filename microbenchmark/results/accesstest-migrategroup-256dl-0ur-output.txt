++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd42ba00000 of size 16384 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2289674740.000000
Throughput: 228967474.000000
Num Accesses - Session Mean & Stddev: 22896747.400000,76.400524
Num Phases Traversed: 13876916.000000
Num Phases Traversed - Session Mean & Stddev: 138769.160000,0.463033
Num Path Iterations: 13876916.000000
Num Path Iterations - Session Mean & Stddev: 138769.160000,0.463033
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f260fa00000 of size 16384 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2290011175.000000
Throughput: 229001117.500000
Num Accesses - Session Mean & Stddev: 22900111.750000,82.086464
Num Phases Traversed: 13878955.000000
Num Phases Traversed - Session Mean & Stddev: 138789.550000,0.497494
Num Path Iterations: 13878955.000000
Num Path Iterations - Session Mean & Stddev: 138789.550000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8db8200000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2304157945.000000
Throughput: 230415794.500000
Num Accesses - Session Mean & Stddev: 23041579.450000,81.687499
Num Phases Traversed: 13964693.000000
Num Phases Traversed - Session Mean & Stddev: 139646.930000,0.495076
Num Path Iterations: 13964693.000000
Num Path Iterations - Session Mean & Stddev: 139646.930000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f745e400000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2293075720.000000
Throughput: 229307572.000000
Num Accesses - Session Mean & Stddev: 22930757.200000,74.084816
Num Phases Traversed: 13897528.000000
Num Phases Traversed - Session Mean & Stddev: 138975.280000,0.448999
Num Path Iterations: 13897528.000000
Num Path Iterations - Session Mean & Stddev: 138975.280000,0.448999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff46a800000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2296050835.000000
Throughput: 229605083.500000
Num Accesses - Session Mean & Stddev: 22960508.350000,81.152495
Num Phases Traversed: 13915559.000000
Num Phases Traversed - Session Mean & Stddev: 139155.590000,0.491833
Num Path Iterations: 13915559.000000
Num Path Iterations - Session Mean & Stddev: 139155.590000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7bb1200000 of size 16384 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2297993215.000000
Throughput: 229799321.500000
Num Accesses - Session Mean & Stddev: 22979932.150000,79.799295
Num Phases Traversed: 13927331.000000
Num Phases Traversed - Session Mean & Stddev: 139273.310000,0.483632
Num Path Iterations: 13927331.000000
Num Path Iterations - Session Mean & Stddev: 139273.310000,0.483632
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa005000000 of size 16384 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2303130160.000000
Throughput: 230313016.000000
Num Accesses - Session Mean & Stddev: 23031301.600000,79.200000
Num Phases Traversed: 13958464.000000
Num Phases Traversed - Session Mean & Stddev: 139584.640000,0.480000
Num Path Iterations: 13958464.000000
Num Path Iterations - Session Mean & Stddev: 139584.640000,0.480000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbc99200000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2301457720.000000
Throughput: 230145772.000000
Num Accesses - Session Mean & Stddev: 23014577.200000,74.084816
Num Phases Traversed: 13948328.000000
Num Phases Traversed - Session Mean & Stddev: 139483.280000,0.448999
Num Path Iterations: 13948328.000000
Num Path Iterations - Session Mean & Stddev: 139483.280000,0.448999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f593e000000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2293105585.000000
Throughput: 229310558.500000
Num Accesses - Session Mean & Stddev: 22931055.850000,70.372065
Num Phases Traversed: 13897709.000000
Num Phases Traversed - Session Mean & Stddev: 138977.090000,0.426497
Num Path Iterations: 13897709.000000
Num Path Iterations - Session Mean & Stddev: 138977.090000,0.426497
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa470200000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2299582660.000000
Throughput: 229958266.000000
Num Accesses - Session Mean & Stddev: 22995826.600000,79.200000
Num Phases Traversed: 13936964.000000
Num Phases Traversed - Session Mean & Stddev: 139369.640000,0.480000
Num Path Iterations: 13936964.000000
Num Path Iterations - Session Mean & Stddev: 139369.640000,0.480000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbe46400000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2289699655.000000
Throughput: 228969965.500000
Num Accesses - Session Mean & Stddev: 22896996.550000,77.585098
Num Phases Traversed: 13877067.000000
Num Phases Traversed - Session Mean & Stddev: 138770.670000,0.470213
Num Path Iterations: 13877067.000000
Num Path Iterations - Session Mean & Stddev: 138770.670000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5a65a00000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2305877245.000000
Throughput: 230587724.500000
Num Accesses - Session Mean & Stddev: 23058772.450000,79.662711
Num Phases Traversed: 13975113.000000
Num Phases Traversed - Session Mean & Stddev: 139751.130000,0.482804
Num Path Iterations: 13975113.000000
Num Path Iterations - Session Mean & Stddev: 139751.130000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0709600000 of size 16384 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2299248700.000000
Throughput: 229924870.000000
Num Accesses - Session Mean & Stddev: 22992487.000000,80.833162
Num Phases Traversed: 13934940.000000
Num Phases Traversed - Session Mean & Stddev: 139349.400000,0.489898
Num Path Iterations: 13934940.000000
Num Path Iterations - Session Mean & Stddev: 139349.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0430200000 of size 16384 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2296387600.000000
Throughput: 229638760.000000
Num Accesses - Session Mean & Stddev: 22963876.000000,80.833162
Num Phases Traversed: 13917600.000000
Num Phases Traversed - Session Mean & Stddev: 139176.000000,0.489898
Num Path Iterations: 13917600.000000
Num Path Iterations - Session Mean & Stddev: 139176.000000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6dc8800000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2293671535.000000
Throughput: 229367153.500000
Num Accesses - Session Mean & Stddev: 22936715.350000,80.478739
Num Phases Traversed: 13901139.000000
Num Phases Traversed - Session Mean & Stddev: 139011.390000,0.487750
Num Path Iterations: 13901139.000000
Num Path Iterations - Session Mean & Stddev: 139011.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f75e2400000 of size 16384 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2300852995.000000
Throughput: 230085299.500000
Num Accesses - Session Mean & Stddev: 23008529.950000,79.662711
Num Phases Traversed: 13944663.000000
Num Phases Traversed - Session Mean & Stddev: 139446.630000,0.482804
Num Path Iterations: 13944663.000000
Num Path Iterations - Session Mean & Stddev: 139446.630000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5a20e00000 of size 16384 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2305575295.000000
Throughput: 230557529.500000
Num Accesses - Session Mean & Stddev: 23055752.950000,66.226486
Num Phases Traversed: 13973283.000000
Num Phases Traversed - Session Mean & Stddev: 139732.830000,0.401373
Num Path Iterations: 13973283.000000
Num Path Iterations - Session Mean & Stddev: 139732.830000,0.401373
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd86b800000 of size 16384 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2294663680.000000
Throughput: 229466368.000000
Num Accesses - Session Mean & Stddev: 22946636.800000,82.433974
Num Phases Traversed: 13907152.000000
Num Phases Traversed - Session Mean & Stddev: 139071.520000,0.499600
Num Path Iterations: 13907152.000000
Num Path Iterations - Session Mean & Stddev: 139071.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3b51000000 of size 16384 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2294070010.000000
Throughput: 229407001.000000
Num Accesses - Session Mean & Stddev: 22940700.100000,82.235576
Num Phases Traversed: 13903554.000000
Num Phases Traversed - Session Mean & Stddev: 139035.540000,0.498397
Num Path Iterations: 13903554.000000
Num Path Iterations - Session Mean & Stddev: 139035.540000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/128,0 0/128,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/128,0 0/128,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    128 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8d26a00000 of size 16384 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2284776550.000000
Throughput: 228477655.000000
Num Accesses - Session Mean & Stddev: 22847765.500000,720.541984
Num Phases Traversed: 13847230.000000
Num Phases Traversed - Session Mean & Stddev: 138472.300000,4.366921
Num Path Iterations: 13847230.000000
Num Path Iterations - Session Mean & Stddev: 138472.300000,4.366921
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0619200000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2864067310.000000
Throughput: 286406731.000000
Num Accesses - Session Mean & Stddev: 28640673.100000,163.052599
Num Phases Traversed: 6803110.000000
Num Phases Traversed - Session Mean & Stddev: 68031.100000,0.387298
Num Path Iterations: 6803110.000000
Num Path Iterations - Session Mean & Stddev: 68031.100000,0.387298
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff19a600000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2866947371.000000
Throughput: 286694737.100000
Num Accesses - Session Mean & Stddev: 28669473.710000,210.457896
Num Phases Traversed: 6809951.000000
Num Phases Traversed - Session Mean & Stddev: 68099.510000,0.499900
Num Path Iterations: 6809951.000000
Num Path Iterations - Session Mean & Stddev: 68099.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f454ba00000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2866387441.000000
Throughput: 286638744.100000
Num Accesses - Session Mean & Stddev: 28663874.410000,171.476768
Num Phases Traversed: 6808621.000000
Num Phases Traversed - Session Mean & Stddev: 68086.210000,0.407308
Num Path Iterations: 6808621.000000
Num Path Iterations - Session Mean & Stddev: 68086.210000,0.407308
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9716a00000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2850031170.000000
Throughput: 285003117.000000
Num Accesses - Session Mean & Stddev: 28500311.700000,192.926437
Num Phases Traversed: 6769770.000000
Num Phases Traversed - Session Mean & Stddev: 67697.700000,0.458258
Num Path Iterations: 6769770.000000
Num Path Iterations - Session Mean & Stddev: 67697.700000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2555a00000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2869541994.000000
Throughput: 286954199.400000
Num Accesses - Session Mean & Stddev: 28695419.940000,157.748649
Num Phases Traversed: 6816114.000000
Num Phases Traversed - Session Mean & Stddev: 68161.140000,0.374700
Num Path Iterations: 6816114.000000
Num Path Iterations - Session Mean & Stddev: 68161.140000,0.374700
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2386000000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2863848390.000000
Throughput: 286384839.000000
Num Accesses - Session Mean & Stddev: 28638483.900000,126.300000
Num Phases Traversed: 6802590.000000
Num Phases Traversed - Session Mean & Stddev: 68025.900000,0.300000
Num Path Iterations: 6802590.000000
Num Path Iterations - Session Mean & Stddev: 68025.900000,0.300000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feb2b200000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2872473838.000000
Throughput: 287247383.800000
Num Accesses - Session Mean & Stddev: 28724738.380000,174.397694
Num Phases Traversed: 6823078.000000
Num Phases Traversed - Session Mean & Stddev: 68230.780000,0.414246
Num Path Iterations: 6823078.000000
Num Path Iterations - Session Mean & Stddev: 68230.780000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9ede000000 of size 49152 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2863170159.000000
Throughput: 286317015.900000
Num Accesses - Session Mean & Stddev: 28631701.590000,171.476768
Num Phases Traversed: 6800979.000000
Num Phases Traversed - Session Mean & Stddev: 68009.790000,0.407308
Num Path Iterations: 6800979.000000
Num Path Iterations - Session Mean & Stddev: 68009.790000,0.407308
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc3b5200000 of size 49152 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2856143669.000000
Throughput: 285614366.900000
Num Accesses - Session Mean & Stddev: 28561436.690000,131.726588
Num Phases Traversed: 6784289.000000
Num Phases Traversed - Session Mean & Stddev: 67842.890000,0.312890
Num Path Iterations: 6784289.000000
Num Path Iterations - Session Mean & Stddev: 67842.890000,0.312890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4766400000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2859834155.000000
Throughput: 285983415.500000
Num Accesses - Session Mean & Stddev: 28598341.550000,209.444856
Num Phases Traversed: 6793055.000000
Num Phases Traversed - Session Mean & Stddev: 67930.550000,0.497494
Num Path Iterations: 6793055.000000
Num Path Iterations - Session Mean & Stddev: 67930.550000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fca89600000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2870772577.000000
Throughput: 287077257.700000
Num Accesses - Session Mean & Stddev: 28707725.770000,203.260614
Num Phases Traversed: 6819037.000000
Num Phases Traversed - Session Mean & Stddev: 68190.370000,0.482804
Num Path Iterations: 6819037.000000
Num Path Iterations - Session Mean & Stddev: 68190.370000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f60bda00000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2857179750.000000
Throughput: 285717975.000000
Num Accesses - Session Mean & Stddev: 28571797.500000,210.500000
Num Phases Traversed: 6786750.000000
Num Phases Traversed - Session Mean & Stddev: 67867.500000,0.500000
Num Path Iterations: 6786750.000000
Num Path Iterations - Session Mean & Stddev: 67867.500000,0.500000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f73b9400000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2853799962.000000
Throughput: 285379996.200000
Num Accesses - Session Mean & Stddev: 28537999.620000,174.397694
Num Phases Traversed: 6778722.000000
Num Phases Traversed - Session Mean & Stddev: 67787.220000,0.414246
Num Path Iterations: 6778722.000000
Num Path Iterations - Session Mean & Stddev: 67787.220000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f09c0a00000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2867150293.000000
Throughput: 286715029.300000
Num Accesses - Session Mean & Stddev: 28671502.930000,197.959554
Num Phases Traversed: 6810433.000000
Num Phases Traversed - Session Mean & Stddev: 68104.330000,0.470213
Num Path Iterations: 6810433.000000
Num Path Iterations - Session Mean & Stddev: 68104.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f44c9800000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2847462649.000000
Throughput: 284746264.900000
Num Accesses - Session Mean & Stddev: 28474626.490000,194.709655
Num Phases Traversed: 6763669.000000
Num Phases Traversed - Session Mean & Stddev: 67636.690000,0.462493
Num Path Iterations: 6763669.000000
Num Path Iterations - Session Mean & Stddev: 67636.690000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1f3de00000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2852049444.000000
Throughput: 285204944.400000
Num Accesses - Session Mean & Stddev: 28520494.440000,202.080000
Num Phases Traversed: 6774564.000000
Num Phases Traversed - Session Mean & Stddev: 67745.640000,0.480000
Num Path Iterations: 6774564.000000
Num Path Iterations - Session Mean & Stddev: 67745.640000,0.480000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1c6e200000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2866687193.000000
Throughput: 286668719.300000
Num Accesses - Session Mean & Stddev: 28666871.930000,197.959554
Num Phases Traversed: 6809333.000000
Num Phases Traversed - Session Mean & Stddev: 68093.330000,0.470213
Num Path Iterations: 6809333.000000
Num Path Iterations - Session Mean & Stddev: 68093.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f82c6c00000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2859220758.000000
Throughput: 285922075.800000
Num Accesses - Session Mean & Stddev: 28592207.580000,145.595411
Num Phases Traversed: 6791598.000000
Num Phases Traversed - Session Mean & Stddev: 67915.980000,0.345832
Num Path Iterations: 6791598.000000
Num Path Iterations - Session Mean & Stddev: 67915.980000,0.345832
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f71c9800000 of size 49152 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2857151122.000000
Throughput: 285715112.200000
Num Accesses - Session Mean & Stddev: 28571511.220000,161.742918
Num Phases Traversed: 6786682.000000
Num Phases Traversed - Session Mean & Stddev: 67866.820000,0.384187
Num Path Iterations: 6786682.000000
Num Path Iterations - Session Mean & Stddev: 67866.820000,0.384187
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/384,0.01 0/384,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/384,0.01 0/384,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    384 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f632ba00000 of size 49152 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2864811217.000000
Throughput: 286481121.700000
Num Accesses - Session Mean & Stddev: 28648112.170000,177.170486
Num Phases Traversed: 6804877.000000
Num Phases Traversed - Session Mean & Stddev: 68048.770000,0.420833
Num Path Iterations: 6804877.000000
Num Path Iterations - Session Mean & Stddev: 68048.770000,0.420833
+ set +x

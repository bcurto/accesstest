++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7a7f600000 of size 524288 bytes.
[1.293s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1740418799.000000
Throughput: 174041879.900000
Num Accesses - Session Mean & Stddev: 17404187.990000,2890.441850
Num Phases Traversed: 421203.000000
Num Phases Traversed - Session Mean & Stddev: 4212.030000,0.699357
Num Path Iterations: 421203.000000
Num Path Iterations - Session Mean & Stddev: 4212.030000,0.699357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa4ff200000 of size 524288 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1733834930.000000
Throughput: 173383493.000000
Num Accesses - Session Mean & Stddev: 17338349.300000,2833.442046
Num Phases Traversed: 419610.000000
Num Phases Traversed - Session Mean & Stddev: 4196.100000,0.685565
Num Path Iterations: 419610.000000
Num Path Iterations - Session Mean & Stddev: 4196.100000,0.685565
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb3f7200000 of size 524288 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1739199564.000000
Throughput: 173919956.400000
Num Accesses - Session Mean & Stddev: 17391995.640000,2844.272826
Num Phases Traversed: 420908.000000
Num Phases Traversed - Session Mean & Stddev: 4209.080000,0.688186
Num Path Iterations: 420908.000000
Num Path Iterations - Session Mean & Stddev: 4209.080000,0.688186
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbcf7e00000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1736761094.000000
Throughput: 173676109.400000
Num Accesses - Session Mean & Stddev: 17367610.940000,2765.097466
Num Phases Traversed: 420318.000000
Num Phases Traversed - Session Mean & Stddev: 4203.180000,0.669029
Num Path Iterations: 420318.000000
Num Path Iterations - Session Mean & Stddev: 4203.180000,0.669029
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd027200000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1729234901.000000
Throughput: 172923490.100000
Num Accesses - Session Mean & Stddev: 17292349.010000,2890.441850
Num Phases Traversed: 418497.000000
Num Phases Traversed - Session Mean & Stddev: 4184.970000,0.699357
Num Path Iterations: 418497.000000
Num Path Iterations - Session Mean & Stddev: 4184.970000,0.699357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8d7f200000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1742985392.000000
Throughput: 174298539.200000
Num Accesses - Session Mean & Stddev: 17429853.920000,2686.132058
Num Phases Traversed: 421824.000000
Num Phases Traversed - Session Mean & Stddev: 4218.240000,0.649923
Num Path Iterations: 421824.000000
Num Path Iterations - Session Mean & Stddev: 4218.240000,0.649923
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0251000000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1736331262.000000
Throughput: 173633126.200000
Num Accesses - Session Mean & Stddev: 17363312.620000,2804.355472
Num Phases Traversed: 420214.000000
Num Phases Traversed - Session Mean & Stddev: 4202.140000,0.678528
Num Path Iterations: 420214.000000
Num Path Iterations - Session Mean & Stddev: 4202.140000,0.678528
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f32aa200000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1735244283.000000
Throughput: 173524428.300000
Num Accesses - Session Mean & Stddev: 17352442.830000,2066.086659
Num Phases Traversed: 419951.000000
Num Phases Traversed - Session Mean & Stddev: 4199.510000,0.499900
Num Path Iterations: 419951.000000
Num Path Iterations - Session Mean & Stddev: 4199.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3565000000 of size 524288 bytes.
[1.248s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1738575481.000000
Throughput: 173857548.100000
Num Accesses - Session Mean & Stddev: 17385754.810000,2282.898008
Num Phases Traversed: 420757.000000
Num Phases Traversed - Session Mean & Stddev: 4207.570000,0.552359
Num Path Iterations: 420757.000000
Num Path Iterations - Session Mean & Stddev: 4207.570000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f735c800000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1746362053.000000
Throughput: 174636205.300000
Num Accesses - Session Mean & Stddev: 17463620.530000,2344.908461
Num Phases Traversed: 422641.000000
Num Phases Traversed - Session Mean & Stddev: 4226.410000,0.567362
Num Path Iterations: 422641.000000
Num Path Iterations - Session Mean & Stddev: 4226.410000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb2baa00000 of size 524288 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1736740429.000000
Throughput: 173674042.900000
Num Accesses - Session Mean & Stddev: 17367404.290000,2842.771019
Num Phases Traversed: 420313.000000
Num Phases Traversed - Session Mean & Stddev: 4203.130000,0.687823
Num Path Iterations: 420313.000000
Num Path Iterations - Session Mean & Stddev: 4203.130000,0.687823
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2cc7400000 of size 524288 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1754830570.000000
Throughput: 175483057.000000
Num Accesses - Session Mean & Stddev: 17548305.700000,2833.442046
Num Phases Traversed: 424690.000000
Num Phases Traversed - Session Mean & Stddev: 4246.900000,0.685565
Num Path Iterations: 424690.000000
Num Path Iterations - Session Mean & Stddev: 4246.900000,0.685565
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7ffc400000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1712206941.000000
Throughput: 171220694.100000
Num Accesses - Session Mean & Stddev: 17122069.410000,1739.300757
Num Phases Traversed: 414377.000000
Num Phases Traversed - Session Mean & Stddev: 4143.770000,0.420833
Num Path Iterations: 414377.000000
Num Path Iterations - Session Mean & Stddev: 4143.770000,0.420833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f354ce00000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1724081050.000000
Throughput: 172408105.000000
Num Accesses - Session Mean & Stddev: 17240810.500000,2066.500000
Num Phases Traversed: 417250.000000
Num Phases Traversed - Session Mean & Stddev: 4172.500000,0.500000
Num Path Iterations: 417250.000000
Num Path Iterations - Session Mean & Stddev: 4172.500000,0.500000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc40f400000 of size 524288 bytes.
[1.283s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1749073301.000000
Throughput: 174907330.100000
Num Accesses - Session Mean & Stddev: 17490733.010000,2890.441850
Num Phases Traversed: 423297.000000
Num Phases Traversed - Session Mean & Stddev: 4232.970000,0.699357
Num Path Iterations: 423297.000000
Num Path Iterations - Session Mean & Stddev: 4232.970000,0.699357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f980b400000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1756306051.000000
Throughput: 175630605.100000
Num Accesses - Session Mean & Stddev: 17563060.510000,2143.987526
Num Phases Traversed: 425047.000000
Num Phases Traversed - Session Mean & Stddev: 4250.470000,0.518748
Num Path Iterations: 425047.000000
Num Path Iterations - Session Mean & Stddev: 4250.470000,0.518748
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f04de800000 of size 524288 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1728627350.000000
Throughput: 172862735.000000
Num Accesses - Session Mean & Stddev: 17286273.500000,2066.500000
Num Phases Traversed: 418350.000000
Num Phases Traversed - Session Mean & Stddev: 4183.500000,0.500000
Num Path Iterations: 418350.000000
Num Path Iterations - Session Mean & Stddev: 4183.500000,0.500000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6472200000 of size 524288 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1736054351.000000
Throughput: 173605435.100000
Num Accesses - Session Mean & Stddev: 17360543.510000,2062.776946
Num Phases Traversed: 420147.000000
Num Phases Traversed - Session Mean & Stddev: 4201.470000,0.499099
Num Path Iterations: 420147.000000
Num Path Iterations - Session Mean & Stddev: 4201.470000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fca67a00000 of size 524288 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1732537168.000000
Throughput: 173253716.800000
Num Accesses - Session Mean & Stddev: 17325371.680000,2858.650034
Num Phases Traversed: 419296.000000
Num Phases Traversed - Session Mean & Stddev: 4192.960000,0.691665
Num Path Iterations: 419296.000000
Num Path Iterations - Session Mean & Stddev: 4192.960000,0.691665
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4096,0.001 0/4096,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4096,0.001 0/4096,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4096 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa3cfe00000 of size 524288 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1725077103.000000
Throughput: 172507710.300000
Num Accesses - Session Mean & Stddev: 17250771.030000,2869.087996
Num Phases Traversed: 417491.000000
Num Phases Traversed - Session Mean & Stddev: 4174.910000,0.694190
Num Path Iterations: 417491.000000
Num Path Iterations - Session Mean & Stddev: 4174.910000,0.694190
+ set +x

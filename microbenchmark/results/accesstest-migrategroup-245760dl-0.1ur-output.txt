++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7e00000000 of size 15728640 bytes.
[3.150s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 503345215.000000
Throughput: 50334521.500000
Num Accesses - Session Mean & Stddev: 5033452.150000,61150.435403
Num Phases Traversed: 4195.000000
Num Phases Traversed - Session Mean & Stddev: 41.950000,0.497494
Num Path Iterations: 4195.000000
Num Path Iterations - Session Mean & Stddev: 41.950000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3980000000 of size 15728640 bytes.
[3.184s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 502853547.000000
Throughput: 50285354.700000
Num Accesses - Session Mean & Stddev: 5028535.470000,35176.603446
Num Phases Traversed: 4191.000000
Num Phases Traversed - Session Mean & Stddev: 41.910000,0.286182
Num Path Iterations: 4191.000000
Num Path Iterations - Session Mean & Stddev: 41.910000,0.286182
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f93c0000000 of size 15728640 bytes.
[3.158s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 501009792.000000
Throughput: 50100979.200000
Num Accesses - Session Mean & Stddev: 5010097.920000,52495.777100
Num Phases Traversed: 4176.000000
Num Phases Traversed - Session Mean & Stddev: 41.760000,0.427083
Num Path Iterations: 4176.000000
Num Path Iterations - Session Mean & Stddev: 41.760000,0.427083
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4540000000 of size 15728640 bytes.
[3.176s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 502607713.000000
Throughput: 50260771.300000
Num Accesses - Session Mean & Stddev: 5026077.130000,38459.470254
Num Phases Traversed: 4189.000000
Num Phases Traversed - Session Mean & Stddev: 41.890000,0.312890
Num Path Iterations: 4189.000000
Num Path Iterations - Session Mean & Stddev: 41.890000,0.312890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbc00000000 of size 15728640 bytes.
[3.174s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 503345215.000000
Throughput: 50334521.500000
Num Accesses - Session Mean & Stddev: 5033452.150000,53224.622278
Num Phases Traversed: 4195.000000
Num Phases Traversed - Session Mean & Stddev: 41.950000,0.433013
Num Path Iterations: 4195.000000
Num Path Iterations - Session Mean & Stddev: 41.950000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0a00000000 of size 15728640 bytes.
[3.114s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 501009792.000000
Throughput: 50100979.200000
Num Accesses - Session Mean & Stddev: 5010097.920000,52495.777100
Num Phases Traversed: 4176.000000
Num Phases Traversed - Session Mean & Stddev: 41.760000,0.427083
Num Path Iterations: 4176.000000
Num Path Iterations - Session Mean & Stddev: 41.760000,0.427083
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1200000000 of size 15728640 bytes.
[3.171s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 502484796.000000
Throughput: 50248479.600000
Num Accesses - Session Mean & Stddev: 5024847.960000,39943.297143
Num Phases Traversed: 4188.000000
Num Phases Traversed - Session Mean & Stddev: 41.880000,0.324962
Num Path Iterations: 4188.000000
Num Path Iterations - Session Mean & Stddev: 41.880000,0.324962
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcc40000000 of size 15728640 bytes.
[3.182s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 501255626.000000
Throughput: 50125562.600000
Num Accesses - Session Mean & Stddev: 5012556.260000,50917.912893
Num Phases Traversed: 4178.000000
Num Phases Traversed - Session Mean & Stddev: 41.780000,0.414246
Num Path Iterations: 4178.000000
Num Path Iterations - Session Mean & Stddev: 41.780000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fea80000000 of size 15728640 bytes.
[3.175s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 504328551.000000
Throughput: 50432855.100000
Num Accesses - Session Mean & Stddev: 5043285.510000,58833.474438
Num Phases Traversed: 4203.000000
Num Phases Traversed - Session Mean & Stddev: 42.030000,0.478644
Num Path Iterations: 4203.000000
Num Path Iterations - Session Mean & Stddev: 42.030000,0.478644
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efe40000000 of size 15728640 bytes.
[3.146s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 503468132.000000
Throughput: 50346813.200000
Num Accesses - Session Mean & Stddev: 5034681.320000,64855.463507
Num Phases Traversed: 4196.000000
Num Phases Traversed - Session Mean & Stddev: 41.960000,0.527636
Num Path Iterations: 4196.000000
Num Path Iterations - Session Mean & Stddev: 41.960000,0.527636
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5080000000 of size 15728640 bytes.
[3.174s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 502484796.000000
Throughput: 50248479.600000
Num Accesses - Session Mean & Stddev: 5024847.960000,39943.297143
Num Phases Traversed: 4188.000000
Num Phases Traversed - Session Mean & Stddev: 41.880000,0.324962
Num Path Iterations: 4188.000000
Num Path Iterations - Session Mean & Stddev: 41.880000,0.324962
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f84c0000000 of size 15728640 bytes.
[3.191s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 502853547.000000
Throughput: 50285354.700000
Num Accesses - Session Mean & Stddev: 5028535.470000,52423.776275
Num Phases Traversed: 4191.000000
Num Phases Traversed - Session Mean & Stddev: 41.910000,0.426497
Num Path Iterations: 4191.000000
Num Path Iterations - Session Mean & Stddev: 41.910000,0.426497
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fde80000000 of size 15728640 bytes.
[3.122s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 503713966.000000
Throughput: 50371396.600000
Num Accesses - Session Mean & Stddev: 5037139.660000,62627.387584
Num Phases Traversed: 4198.000000
Num Phases Traversed - Session Mean & Stddev: 41.980000,0.509510
Num Path Iterations: 4198.000000
Num Path Iterations - Session Mean & Stddev: 41.980000,0.509510
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4240000000 of size 15728640 bytes.
[3.160s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 502361879.000000
Throughput: 50236187.900000
Num Accesses - Session Mean & Stddev: 5023618.790000,41337.409248
Num Phases Traversed: 4187.000000
Num Phases Traversed - Session Mean & Stddev: 41.870000,0.336303
Num Path Iterations: 4187.000000
Num Path Iterations - Session Mean & Stddev: 41.870000,0.336303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1140000000 of size 15728640 bytes.
[3.150s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 502238962.000000
Throughput: 50223896.200000
Num Accesses - Session Mean & Stddev: 5022389.620000,42650.604946
Num Phases Traversed: 4186.000000
Num Phases Traversed - Session Mean & Stddev: 41.860000,0.346987
Num Path Iterations: 4186.000000
Num Path Iterations - Session Mean & Stddev: 41.860000,0.346987
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4180000000 of size 15728640 bytes.
[3.202s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 504697302.000000
Throughput: 50469730.200000
Num Accesses - Session Mean & Stddev: 5046973.020000,57179.529865
Num Phases Traversed: 4206.000000
Num Phases Traversed - Session Mean & Stddev: 42.060000,0.465188
Num Path Iterations: 4206.000000
Num Path Iterations - Session Mean & Stddev: 42.060000,0.465188
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fec40000000 of size 15728640 bytes.
[3.153s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 502730630.000000
Throughput: 50273063.000000
Num Accesses - Session Mean & Stddev: 5027306.300000,36875.100000
Num Phases Traversed: 4190.000000
Num Phases Traversed - Session Mean & Stddev: 41.900000,0.300000
Num Path Iterations: 4190.000000
Num Path Iterations - Session Mean & Stddev: 41.900000,0.300000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2640000000 of size 15728640 bytes.
[3.153s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 503591049.000000
Throughput: 50359104.900000
Num Accesses - Session Mean & Stddev: 5035910.490000,50545.646510
Num Phases Traversed: 4197.000000
Num Phases Traversed - Session Mean & Stddev: 41.970000,0.411218
Num Path Iterations: 4197.000000
Num Path Iterations - Session Mean & Stddev: 41.970000,0.411218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4bc0000000 of size 15728640 bytes.
[3.181s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 504082717.000000
Throughput: 50408271.700000
Num Accesses - Session Mean & Stddev: 5040827.170000,68426.250056
Num Phases Traversed: 4201.000000
Num Phases Traversed - Session Mean & Stddev: 42.010000,0.556687
Num Path Iterations: 4201.000000
Num Path Iterations - Session Mean & Stddev: 42.010000,0.556687
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/122880,0.1 0/122880,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/122880,0.1 0/122880,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    122880 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fca00000000 of size 15728640 bytes.
[3.166s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 503713966.000000
Throughput: 50371396.600000
Num Accesses - Session Mean & Stddev: 5037139.660000,62627.387584
Num Phases Traversed: 4198.000000
Num Phases Traversed - Session Mean & Stddev: 41.980000,0.509510
Num Path Iterations: 4198.000000
Num Path Iterations - Session Mean & Stddev: 41.980000,0.509510
+ set +x

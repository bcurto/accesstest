++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff780000000 of size 17301504 bytes.
[3.392s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,151260.499333
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.118749
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.118749
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0140000000 of size 17301504 bytes.
[3.370s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,141261.728485
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.044797
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.044797
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f21c0000000 of size 17301504 bytes.
[3.381s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 254185500.000000
Throughput: 25418550.000000
Num Accesses - Session Mean & Stddev: 2541855.000000,135205.000000
Num Phases Traversed: 1980.000000
Num Phases Traversed - Session Mean & Stddev: 19.800000,1.000000
Num Path Iterations: 1980.000000
Num Path Iterations - Session Mean & Stddev: 19.800000,1.000000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3340000000 of size 17301504 bytes.
[3.377s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 254455910.000000
Throughput: 25445591.000000
Num Accesses - Session Mean & Stddev: 2544559.100000,147342.436416
Num Phases Traversed: 1982.000000
Num Phases Traversed - Session Mean & Stddev: 19.820000,1.089771
Num Path Iterations: 1982.000000
Num Path Iterations - Session Mean & Stddev: 19.820000,1.089771
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1c40000000 of size 17301504 bytes.
[3.327s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 254455910.000000
Throughput: 25445591.000000
Num Accesses - Session Mean & Stddev: 2544559.100000,146096.494579
Num Phases Traversed: 1982.000000
Num Phases Traversed - Session Mean & Stddev: 19.820000,1.080555
Num Path Iterations: 1982.000000
Num Path Iterations - Session Mean & Stddev: 19.820000,1.080555
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4f00000000 of size 17301504 bytes.
[3.359s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 253644680.000000
Throughput: 25364468.000000
Num Accesses - Session Mean & Stddev: 2536446.800000,157813.833661
Num Phases Traversed: 1976.000000
Num Phases Traversed - Session Mean & Stddev: 19.760000,1.167219
Num Path Iterations: 1976.000000
Num Path Iterations - Session Mean & Stddev: 19.760000,1.167219
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe300000000 of size 17301504 bytes.
[3.387s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 253644680.000000
Throughput: 25364468.000000
Num Accesses - Session Mean & Stddev: 2536446.800000,150703.546912
Num Phases Traversed: 1976.000000
Num Phases Traversed - Session Mean & Stddev: 19.760000,1.114630
Num Path Iterations: 1976.000000
Num Path Iterations - Session Mean & Stddev: 19.760000,1.114630
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe400000000 of size 17301504 bytes.
[3.374s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 254726320.000000
Throughput: 25472632.000000
Num Accesses - Session Mean & Stddev: 2547263.200000,146521.303551
Num Phases Traversed: 1984.000000
Num Phases Traversed - Session Mean & Stddev: 19.840000,1.083697
Num Path Iterations: 1984.000000
Num Path Iterations - Session Mean & Stddev: 19.840000,1.083697
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8040000000 of size 17301504 bytes.
[3.383s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,146346.531549
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.082405
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.082405
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fce40000000 of size 17301504 bytes.
[3.378s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,147590.362616
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.091604
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.091604
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f04c0000000 of size 17301504 bytes.
[3.389s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 255537550.000000
Throughput: 25553755.000000
Num Accesses - Session Mean & Stddev: 2555375.500000,143724.886461
Num Phases Traversed: 1990.000000
Num Phases Traversed - Session Mean & Stddev: 19.900000,1.063015
Num Path Iterations: 1990.000000
Num Path Iterations - Session Mean & Stddev: 19.900000,1.063015
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6b80000000 of size 17301504 bytes.
[3.348s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,135986.927947
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.005783
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.005783
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc300000000 of size 17301504 bytes.
[3.399s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 253644680.000000
Throughput: 25364468.000000
Num Accesses - Session Mean & Stddev: 2536446.800000,148257.692451
Num Phases Traversed: 1976.000000
Num Phases Traversed - Session Mean & Stddev: 19.760000,1.096540
Num Path Iterations: 1976.000000
Num Path Iterations - Session Mean & Stddev: 19.760000,1.096540
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc700000000 of size 17301504 bytes.
[3.371s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,148823.798424
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.100727
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.100727
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f96c0000000 of size 17301504 bytes.
[3.384s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,146346.531549
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.082405
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.082405
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4580000000 of size 17301504 bytes.
[3.377s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 252563040.000000
Throughput: 25256304.000000
Num Accesses - Session Mean & Stddev: 2525630.400000,144207.399143
Num Phases Traversed: 1968.000000
Num Phases Traversed - Session Mean & Stddev: 19.680000,1.066583
Num Path Iterations: 1968.000000
Num Path Iterations - Session Mean & Stddev: 19.680000,1.066583
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff440000000 of size 17301504 bytes.
[3.371s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,153658.564159
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.136486
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.136486
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd300000000 of size 17301504 bytes.
[3.375s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,146346.531549
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.082405
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.082405
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4200000000 of size 17301504 bytes.
[3.382s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,141261.728485
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.044797
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.044797
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0 0/135168,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0 0/135168,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe0c0000000 of size 17301504 bytes.
[3.385s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 255267140.000000
Throughput: 25526714.000000
Num Accesses - Session Mean & Stddev: 2552671.400000,136924.687551
Num Phases Traversed: 1988.000000
Num Phases Traversed - Session Mean & Stddev: 19.880000,1.012719
Num Path Iterations: 1988.000000
Num Path Iterations - Session Mean & Stddev: 19.880000,1.012719
+ set +x

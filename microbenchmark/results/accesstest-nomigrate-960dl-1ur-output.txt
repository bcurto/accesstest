++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4347a00000 of size 61440 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 869647351.000000
Throughput: 86964735.100000
Num Accesses - Session Mean & Stddev: 8696473.510000,481.976006
Num Phases Traversed: 1682203.000000
Num Phases Traversed - Session Mean & Stddev: 16822.030000,0.932255
Num Path Iterations: 1682203.000000
Num Path Iterations - Session Mean & Stddev: 16822.030000,0.932255
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3d6a200000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 868366225.000000
Throughput: 86836622.500000
Num Accesses - Session Mean & Stddev: 8683662.250000,548.970261
Num Phases Traversed: 1679725.000000
Num Phases Traversed - Session Mean & Stddev: 16797.250000,1.061838
Num Path Iterations: 1679725.000000
Num Path Iterations - Session Mean & Stddev: 16797.250000,1.061838
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8dfb400000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 868786029.000000
Throughput: 86878602.900000
Num Accesses - Session Mean & Stddev: 8687860.290000,578.739878
Num Phases Traversed: 1680537.000000
Num Phases Traversed - Session Mean & Stddev: 16805.370000,1.119419
Num Path Iterations: 1680537.000000
Num Path Iterations - Session Mean & Stddev: 16805.370000,1.119419
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7be3600000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 869087957.000000
Throughput: 86908795.700000
Num Accesses - Session Mean & Stddev: 8690879.570000,581.688185
Num Phases Traversed: 1681121.000000
Num Phases Traversed - Session Mean & Stddev: 16811.210000,1.125122
Num Path Iterations: 1681121.000000
Num Path Iterations - Session Mean & Stddev: 16811.210000,1.125122
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f81d2400000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 868291777.000000
Throughput: 86829177.700000
Num Accesses - Session Mean & Stddev: 8682917.770000,574.289036
Num Phases Traversed: 1679581.000000
Num Phases Traversed - Session Mean & Stddev: 16795.810000,1.110811
Num Path Iterations: 1679581.000000
Num Path Iterations - Session Mean & Stddev: 16795.810000,1.110811
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9681e00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 867125942.000000
Throughput: 86712594.200000
Num Accesses - Session Mean & Stddev: 8671259.420000,550.161852
Num Phases Traversed: 1677326.000000
Num Phases Traversed - Session Mean & Stddev: 16773.260000,1.064143
Num Path Iterations: 1677326.000000
Num Path Iterations - Session Mean & Stddev: 16773.260000,1.064143
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9981800000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 867690506.000000
Throughput: 86769050.600000
Num Accesses - Session Mean & Stddev: 8676905.060000,470.327946
Num Phases Traversed: 1678418.000000
Num Phases Traversed - Session Mean & Stddev: 16784.180000,0.909725
Num Path Iterations: 1678418.000000
Num Path Iterations - Session Mean & Stddev: 16784.180000,0.909725
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f543cc00000 of size 61440 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 867049943.000000
Throughput: 86704994.300000
Num Accesses - Session Mean & Stddev: 8670499.430000,543.686403
Num Phases Traversed: 1677179.000000
Num Phases Traversed - Session Mean & Stddev: 16771.790000,1.051618
Num Path Iterations: 1677179.000000
Num Path Iterations - Session Mean & Stddev: 16771.790000,1.051618
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0eefe00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 867679132.000000
Throughput: 86767913.200000
Num Accesses - Session Mean & Stddev: 8676791.320000,521.734720
Num Phases Traversed: 1678396.000000
Num Phases Traversed - Session Mean & Stddev: 16783.960000,1.009158
Num Path Iterations: 1678396.000000
Num Path Iterations - Session Mean & Stddev: 16783.960000,1.009158
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd585600000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 868011563.000000
Throughput: 86801156.300000
Num Accesses - Session Mean & Stddev: 8680115.630000,334.215609
Num Phases Traversed: 1679039.000000
Num Phases Traversed - Session Mean & Stddev: 16790.390000,0.646452
Num Path Iterations: 1679039.000000
Num Path Iterations - Session Mean & Stddev: 16790.390000,0.646452
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f684c600000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 868072569.000000
Throughput: 86807256.900000
Num Accesses - Session Mean & Stddev: 8680725.690000,572.237498
Num Phases Traversed: 1679157.000000
Num Phases Traversed - Session Mean & Stddev: 16791.570000,1.106842
Num Path Iterations: 1679157.000000
Num Path Iterations - Session Mean & Stddev: 16791.570000,1.106842
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd04ce00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 868319178.000000
Throughput: 86831917.800000
Num Accesses - Session Mean & Stddev: 8683191.780000,408.528153
Num Phases Traversed: 1679634.000000
Num Phases Traversed - Session Mean & Stddev: 16796.340000,0.790190
Num Path Iterations: 1679634.000000
Num Path Iterations - Session Mean & Stddev: 16796.340000,0.790190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f23a9000000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 867786668.000000
Throughput: 86778666.800000
Num Accesses - Session Mean & Stddev: 8677866.680000,490.033098
Num Phases Traversed: 1678604.000000
Num Phases Traversed - Session Mean & Stddev: 16786.040000,0.947840
Num Path Iterations: 1678604.000000
Num Path Iterations - Session Mean & Stddev: 16786.040000,0.947840
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd8faa00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 867839402.000000
Throughput: 86783940.200000
Num Accesses - Session Mean & Stddev: 8678394.020000,551.132670
Num Phases Traversed: 1678706.000000
Num Phases Traversed - Session Mean & Stddev: 16787.060000,1.066021
Num Path Iterations: 1678706.000000
Num Path Iterations - Session Mean & Stddev: 16787.060000,1.066021
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f15e2a00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 870021142.000000
Throughput: 87002114.200000
Num Accesses - Session Mean & Stddev: 8700211.420000,596.771199
Num Phases Traversed: 1682926.000000
Num Phases Traversed - Session Mean & Stddev: 16829.260000,1.154296
Num Path Iterations: 1682926.000000
Num Path Iterations - Session Mean & Stddev: 16829.260000,1.154296
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8480400000 of size 61440 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 867396850.000000
Throughput: 86739685.000000
Num Accesses - Session Mean & Stddev: 8673968.500000,346.814143
Num Phases Traversed: 1677850.000000
Num Phases Traversed - Session Mean & Stddev: 16778.500000,0.670820
Num Path Iterations: 1677850.000000
Num Path Iterations - Session Mean & Stddev: 16778.500000,0.670820
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fedc0400000 of size 61440 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 865523759.000000
Throughput: 86552375.900000
Num Accesses - Session Mean & Stddev: 8655237.590000,4142.363898
Num Phases Traversed: 1674227.000000
Num Phases Traversed - Session Mean & Stddev: 16742.270000,8.012309
Num Path Iterations: 1674227.000000
Num Path Iterations - Session Mean & Stddev: 16742.270000,8.012309
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcf1d600000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 865020718.000000
Throughput: 86502071.800000
Num Accesses - Session Mean & Stddev: 8650207.180000,470.555212
Num Phases Traversed: 1673254.000000
Num Phases Traversed - Session Mean & Stddev: 16732.540000,0.910165
Num Path Iterations: 1673254.000000
Num Path Iterations - Session Mean & Stddev: 16732.540000,0.910165
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f46f9000000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 866980665.000000
Throughput: 86698066.500000
Num Accesses - Session Mean & Stddev: 8669806.650000,295.866773
Num Phases Traversed: 1677045.000000
Num Phases Traversed - Session Mean & Stddev: 16770.450000,0.572276
Num Path Iterations: 1677045.000000
Num Path Iterations - Session Mean & Stddev: 16770.450000,0.572276
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,1 0/480,1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,1 0/480,1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fad85a00000 of size 61440 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 867710152.000000
Throughput: 86771015.200000
Num Accesses - Session Mean & Stddev: 8677101.520000,321.372789
Num Phases Traversed: 1678456.000000
Num Phases Traversed - Session Mean & Stddev: 16784.560000,0.621611
Num Path Iterations: 1678456.000000
Num Path Iterations - Session Mean & Stddev: 16784.560000,0.621611
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f01e2400000 of size 229376 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1009869647.000000
Throughput: 100986964.700000
Num Accesses - Session Mean & Stddev: 10098696.470000,5264.566099
Num Phases Traversed: 552243.000000
Num Phases Traversed - Session Mean & Stddev: 5522.430000,2.878385
Num Path Iterations: 552243.000000
Num Path Iterations - Session Mean & Stddev: 5522.430000,2.878385
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5505000000 of size 229376 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1010409202.000000
Throughput: 101040920.200000
Num Accesses - Session Mean & Stddev: 10104092.020000,3036.140000
Num Phases Traversed: 552538.000000
Num Phases Traversed - Session Mean & Stddev: 5525.380000,1.660000
Num Path Iterations: 552538.000000
Num Path Iterations - Session Mean & Stddev: 5525.380000,1.660000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f72d5600000 of size 229376 bytes.
[1.248s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1007159069.000000
Throughput: 100715906.900000
Num Accesses - Session Mean & Stddev: 10071590.690000,3037.406814
Num Phases Traversed: 550761.000000
Num Phases Traversed - Session Mean & Stddev: 5507.610000,1.660693
Num Path Iterations: 550761.000000
Num Path Iterations - Session Mean & Stddev: 5507.610000,1.660693
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1a7da00000 of size 229376 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1007674847.000000
Throughput: 100767484.700000
Num Accesses - Session Mean & Stddev: 10076748.470000,15987.290988
Num Phases Traversed: 551043.000000
Num Phases Traversed - Session Mean & Stddev: 5510.430000,8.741001
Num Path Iterations: 551043.000000
Num Path Iterations - Session Mean & Stddev: 5510.430000,8.741001
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f27e5400000 of size 229376 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1006859113.000000
Throughput: 100685911.300000
Num Accesses - Session Mean & Stddev: 10068591.130000,3140.931428
Num Phases Traversed: 550597.000000
Num Phases Traversed - Session Mean & Stddev: 5505.970000,1.717294
Num Path Iterations: 550597.000000
Num Path Iterations - Session Mean & Stddev: 5505.970000,1.717294
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fba81400000 of size 229376 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1010257395.000000
Throughput: 101025739.500000
Num Accesses - Session Mean & Stddev: 10102573.950000,11493.267768
Num Phases Traversed: 552455.000000
Num Phases Traversed - Session Mean & Stddev: 5524.550000,6.283908
Num Path Iterations: 552455.000000
Num Path Iterations - Session Mean & Stddev: 5524.550000,6.283908
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0ea2c00000 of size 229376 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1009390449.000000
Throughput: 100939044.900000
Num Accesses - Session Mean & Stddev: 10093904.490000,5712.192765
Num Phases Traversed: 551981.000000
Num Phases Traversed - Session Mean & Stddev: 5519.810000,3.123123
Num Path Iterations: 551981.000000
Num Path Iterations - Session Mean & Stddev: 5519.810000,3.123123
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f498f600000 of size 229376 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1007870550.000000
Throughput: 100787055.000000
Num Accesses - Session Mean & Stddev: 10078705.500000,42229.308835
Num Phases Traversed: 551150.000000
Num Phases Traversed - Session Mean & Stddev: 5511.500000,23.088742
Num Path Iterations: 551150.000000
Num Path Iterations - Session Mean & Stddev: 5511.500000,23.088742
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdbf3800000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1007749836.000000
Throughput: 100774983.600000
Num Accesses - Session Mean & Stddev: 10077498.360000,6259.991308
Num Phases Traversed: 551084.000000
Num Phases Traversed - Session Mean & Stddev: 5510.840000,3.422631
Num Path Iterations: 551084.000000
Num Path Iterations - Session Mean & Stddev: 5510.840000,3.422631
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd684400000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1011149947.000000
Throughput: 101114994.700000
Num Accesses - Session Mean & Stddev: 10111499.470000,8905.013893
Num Phases Traversed: 552943.000000
Num Phases Traversed - Session Mean & Stddev: 5529.430000,4.868788
Num Path Iterations: 552943.000000
Num Path Iterations - Session Mean & Stddev: 5529.430000,4.868788
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4cb6800000 of size 229376 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1004349725.000000
Throughput: 100434972.500000
Num Accesses - Session Mean & Stddev: 10043497.250000,8521.550270
Num Phases Traversed: 549225.000000
Num Phases Traversed - Session Mean & Stddev: 5492.250000,4.659131
Num Path Iterations: 549225.000000
Num Path Iterations - Session Mean & Stddev: 5492.250000,4.659131
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f954fc00000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1009218523.000000
Throughput: 100921852.300000
Num Accesses - Session Mean & Stddev: 10092185.230000,2817.550606
Num Phases Traversed: 551887.000000
Num Phases Traversed - Session Mean & Stddev: 5518.870000,1.540487
Num Path Iterations: 551887.000000
Num Path Iterations - Session Mean & Stddev: 5518.870000,1.540487
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcc56c00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1009708695.000000
Throughput: 100970869.500000
Num Accesses - Session Mean & Stddev: 10097086.950000,5201.407174
Num Phases Traversed: 552155.000000
Num Phases Traversed - Session Mean & Stddev: 5521.550000,2.843853
Num Path Iterations: 552155.000000
Num Path Iterations - Session Mean & Stddev: 5521.550000,2.843853
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7386e00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1007332824.000000
Throughput: 100733282.400000
Num Accesses - Session Mean & Stddev: 10073328.240000,3202.787380
Num Phases Traversed: 550856.000000
Num Phases Traversed - Session Mean & Stddev: 5508.560000,1.751114
Num Path Iterations: 550856.000000
Num Path Iterations - Session Mean & Stddev: 5508.560000,1.751114
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f424f400000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1007682163.000000
Throughput: 100768216.300000
Num Accesses - Session Mean & Stddev: 10076821.630000,27456.275214
Num Phases Traversed: 551047.000000
Num Phases Traversed - Session Mean & Stddev: 5510.470000,15.011632
Num Path Iterations: 551047.000000
Num Path Iterations - Session Mean & Stddev: 5510.470000,15.011632
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2339400000 of size 229376 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1008633243.000000
Throughput: 100863324.300000
Num Accesses - Session Mean & Stddev: 10086332.430000,5607.935910
Num Phases Traversed: 551567.000000
Num Phases Traversed - Session Mean & Stddev: 5515.670000,3.066121
Num Path Iterations: 551567.000000
Num Path Iterations - Session Mean & Stddev: 5515.670000,3.066121
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbfe8e00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1007956513.000000
Throughput: 100795651.300000
Num Accesses - Session Mean & Stddev: 10079565.130000,5923.560363
Num Phases Traversed: 551197.000000
Num Phases Traversed - Session Mean & Stddev: 5511.970000,3.238688
Num Path Iterations: 551197.000000
Num Path Iterations - Session Mean & Stddev: 5511.970000,3.238688
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe8f0200000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1010685381.000000
Throughput: 101068538.100000
Num Accesses - Session Mean & Stddev: 10106853.810000,20973.606426
Num Phases Traversed: 552689.000000
Num Phases Traversed - Session Mean & Stddev: 5526.890000,11.467253
Num Path Iterations: 552689.000000
Num Path Iterations - Session Mean & Stddev: 5526.890000,11.467253
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f71d8200000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1009800145.000000
Throughput: 100980014.500000
Num Accesses - Session Mean & Stddev: 10098001.450000,11361.535419
Num Phases Traversed: 552205.000000
Num Phases Traversed - Session Mean & Stddev: 5522.050000,6.211884
Num Path Iterations: 552205.000000
Num Path Iterations - Session Mean & Stddev: 5522.050000,6.211884
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.1 0/1792,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.1 0/1792,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe73a000000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1011157263.000000
Throughput: 101115726.300000
Num Accesses - Session Mean & Stddev: 10111572.630000,2171.123924
Num Phases Traversed: 552947.000000
Num Phases Traversed - Session Mean & Stddev: 5529.470000,1.187055
Num Path Iterations: 552947.000000
Num Path Iterations - Session Mean & Stddev: 5529.470000,1.187055
+ set +x

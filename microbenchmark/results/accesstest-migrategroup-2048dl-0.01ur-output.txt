++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcee4200000 of size 131072 bytes.
[1.255s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2042977881.000000
Throughput: 204297788.100000
Num Accesses - Session Mean & Stddev: 20429778.810000,432.154040
Num Phases Traversed: 1925621.000000
Num Phases Traversed - Session Mean & Stddev: 19256.210000,0.407308
Num Path Iterations: 1925621.000000
Num Path Iterations - Session Mean & Stddev: 19256.210000,0.407308
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0e56800000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2044535429.000000
Throughput: 204453542.900000
Num Accesses - Session Mean & Stddev: 20445354.290000,495.272254
Num Phases Traversed: 1927089.000000
Num Phases Traversed - Session Mean & Stddev: 19270.890000,0.466798
Num Path Iterations: 1927089.000000
Num Path Iterations - Session Mean & Stddev: 19270.890000,0.466798
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faa35a00000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2045099881.000000
Throughput: 204509988.100000
Num Accesses - Session Mean & Stddev: 20450998.810000,481.441537
Num Phases Traversed: 1927621.000000
Num Phases Traversed - Session Mean & Stddev: 19276.210000,0.453762
Num Path Iterations: 1927621.000000
Num Path Iterations - Session Mean & Stddev: 19276.210000,0.453762
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa878e00000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2044438878.000000
Throughput: 204443887.800000
Num Accesses - Session Mean & Stddev: 20444388.780000,636.246235
Num Phases Traversed: 1926998.000000
Num Phases Traversed - Session Mean & Stddev: 19269.980000,0.599667
Num Path Iterations: 1926998.000000
Num Path Iterations - Session Mean & Stddev: 19269.980000,0.599667
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbe5be00000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2045560355.000000
Throughput: 204556035.500000
Num Accesses - Session Mean & Stddev: 20455603.550000,527.840835
Num Phases Traversed: 1928055.000000
Num Phases Traversed - Session Mean & Stddev: 19280.550000,0.497494
Num Path Iterations: 1928055.000000
Num Path Iterations - Session Mean & Stddev: 19280.550000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5017e00000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2043427745.000000
Throughput: 204342774.500000
Num Accesses - Session Mean & Stddev: 20434277.450000,527.840835
Num Phases Traversed: 1926045.000000
Num Phases Traversed - Session Mean & Stddev: 19260.450000,0.497494
Num Path Iterations: 1926045.000000
Num Path Iterations - Session Mean & Stddev: 19260.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f172d800000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2043284510.000000
Throughput: 204328451.000000
Num Accesses - Session Mean & Stddev: 20432845.100000,351.893890
Num Phases Traversed: 1925910.000000
Num Phases Traversed - Session Mean & Stddev: 19259.100000,0.331662
Num Path Iterations: 1925910.000000
Num Path Iterations - Session Mean & Stddev: 19259.100000,0.331662
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6ec7e00000 of size 131072 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2044487684.000000
Throughput: 204448768.400000
Num Accesses - Session Mean & Stddev: 20444876.840000,526.666550
Num Phases Traversed: 1927044.000000
Num Phases Traversed - Session Mean & Stddev: 19270.440000,0.496387
Num Path Iterations: 1927044.000000
Num Path Iterations - Session Mean & Stddev: 19270.440000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8eb8a00000 of size 131072 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2041985846.000000
Throughput: 204198584.600000
Num Accesses - Session Mean & Stddev: 20419858.460000,368.153240
Num Phases Traversed: 1924686.000000
Num Phases Traversed - Session Mean & Stddev: 19246.860000,0.346987
Num Path Iterations: 1924686.000000
Num Path Iterations - Session Mean & Stddev: 19246.860000,0.346987
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff799800000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2044056918.000000
Throughput: 204405691.800000
Num Accesses - Session Mean & Stddev: 20440569.180000,514.995017
Num Phases Traversed: 1926638.000000
Num Phases Traversed - Session Mean & Stddev: 19266.380000,0.485386
Num Path Iterations: 1926638.000000
Num Path Iterations - Session Mean & Stddev: 19266.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4403400000 of size 131072 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2044412353.000000
Throughput: 204441235.300000
Num Accesses - Session Mean & Stddev: 20444123.530000,471.040985
Num Phases Traversed: 1926973.000000
Num Phases Traversed - Session Mean & Stddev: 19269.730000,0.443959
Num Path Iterations: 1926973.000000
Num Path Iterations - Session Mean & Stddev: 19269.730000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff8fca00000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2045226140.000000
Throughput: 204522614.000000
Num Accesses - Session Mean & Stddev: 20452261.400000,519.781723
Num Phases Traversed: 1927740.000000
Num Phases Traversed - Session Mean & Stddev: 19277.400000,0.489898
Num Path Iterations: 1927740.000000
Num Path Iterations - Session Mean & Stddev: 19277.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f439da00000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2044001746.000000
Throughput: 204400174.600000
Num Accesses - Session Mean & Stddev: 20440017.460000,619.027809
Num Phases Traversed: 1926586.000000
Num Phases Traversed - Session Mean & Stddev: 19265.860000,0.583438
Num Path Iterations: 1926586.000000
Num Path Iterations - Session Mean & Stddev: 19265.860000,0.583438
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f49f8a00000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2042995918.000000
Throughput: 204299591.800000
Num Accesses - Session Mean & Stddev: 20429959.180000,514.995017
Num Phases Traversed: 1925638.000000
Num Phases Traversed - Session Mean & Stddev: 19256.380000,0.485386
Num Path Iterations: 1925638.000000
Num Path Iterations - Session Mean & Stddev: 19256.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb52a200000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2043500954.000000
Throughput: 204350095.400000
Num Accesses - Session Mean & Stddev: 20435009.540000,397.556573
Num Phases Traversed: 1926114.000000
Num Phases Traversed - Session Mean & Stddev: 19261.140000,0.374700
Num Path Iterations: 1926114.000000
Num Path Iterations - Session Mean & Stddev: 19261.140000,0.374700
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8a18c00000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2045352399.000000
Throughput: 204535239.900000
Num Accesses - Session Mean & Stddev: 20453523.990000,521.835137
Num Phases Traversed: 1927859.000000
Num Phases Traversed - Session Mean & Stddev: 19278.590000,0.491833
Num Path Iterations: 1927859.000000
Num Path Iterations - Session Mean & Stddev: 19278.590000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbf89a00000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2044515270.000000
Throughput: 204451527.000000
Num Accesses - Session Mean & Stddev: 20445152.700000,551.311772
Num Phases Traversed: 1927070.000000
Num Phases Traversed - Session Mean & Stddev: 19270.700000,0.519615
Num Path Iterations: 1927070.000000
Num Path Iterations - Session Mean & Stddev: 19270.700000,0.519615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa57aa00000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2045729054.000000
Throughput: 204572905.400000
Num Accesses - Session Mean & Stddev: 20457290.540000,498.105319
Num Phases Traversed: 1928214.000000
Num Phases Traversed - Session Mean & Stddev: 19282.140000,0.469468
Num Path Iterations: 1928214.000000
Num Path Iterations - Session Mean & Stddev: 19282.140000,0.469468
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f819cc00000 of size 131072 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2044767788.000000
Throughput: 204476778.800000
Num Accesses - Session Mean & Stddev: 20447677.880000,574.901457
Num Phases Traversed: 1927308.000000
Num Phases Traversed - Session Mean & Stddev: 19273.080000,0.541849
Num Path Iterations: 1927308.000000
Num Path Iterations - Session Mean & Stddev: 19273.080000,0.541849
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.01 0/1024,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.01 0/1024,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8cda200000 of size 131072 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2044107846.000000
Throughput: 204410784.600000
Num Accesses - Session Mean & Stddev: 20441078.460000,520.214695
Num Phases Traversed: 1926686.000000
Num Phases Traversed - Session Mean & Stddev: 19266.860000,0.490306
Num Path Iterations: 1926686.000000
Num Path Iterations - Session Mean & Stddev: 19266.860000,0.490306
+ set +x

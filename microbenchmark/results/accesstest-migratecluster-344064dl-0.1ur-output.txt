++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6740000000 of size 22020096 bytes.
[4.500s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 242273252.000000
Throughput: 24227325.200000
Num Accesses - Session Mean & Stddev: 2422732.520000,71693.122377
Num Phases Traversed: 1508.000000
Num Phases Traversed - Session Mean & Stddev: 15.080000,0.416653
Num Path Iterations: 1508.000000
Num Path Iterations - Session Mean & Stddev: 15.080000,0.416653
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3600000000 of size 22020096 bytes.
[4.485s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 242617390.000000
Throughput: 24261739.000000
Num Accesses - Session Mean & Stddev: 2426173.900000,75003.138232
Num Phases Traversed: 1510.000000
Num Phases Traversed - Session Mean & Stddev: 15.100000,0.435890
Num Path Iterations: 1510.000000
Num Path Iterations - Session Mean & Stddev: 15.100000,0.435890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe9c0000000 of size 22020096 bytes.
[4.497s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 242789459.000000
Throughput: 24278945.900000
Num Accesses - Session Mean & Stddev: 2427894.590000,72575.595632
Num Phases Traversed: 1511.000000
Num Phases Traversed - Session Mean & Stddev: 15.110000,0.421782
Num Path Iterations: 1511.000000
Num Path Iterations - Session Mean & Stddev: 15.110000,0.421782
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1200000000 of size 22020096 bytes.
[4.484s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 242789459.000000
Throughput: 24278945.900000
Num Accesses - Session Mean & Stddev: 2427894.590000,72575.595632
Num Phases Traversed: 1511.000000
Num Phases Traversed - Session Mean & Stddev: 15.110000,0.421782
Num Path Iterations: 1511.000000
Num Path Iterations - Session Mean & Stddev: 15.110000,0.421782
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff540000000 of size 22020096 bytes.
[4.487s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 242789459.000000
Throughput: 24278945.900000
Num Accesses - Session Mean & Stddev: 2427894.590000,72575.595632
Num Phases Traversed: 1511.000000
Num Phases Traversed - Session Mean & Stddev: 15.110000,0.421782
Num Path Iterations: 1511.000000
Num Path Iterations - Session Mean & Stddev: 15.110000,0.421782
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7c40000000 of size 22020096 bytes.
[4.489s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 242961528.000000
Throughput: 24296152.800000
Num Accesses - Session Mean & Stddev: 2429615.280000,74129.593856
Num Phases Traversed: 1512.000000
Num Phases Traversed - Session Mean & Stddev: 15.120000,0.430813
Num Path Iterations: 1512.000000
Num Path Iterations - Session Mean & Stddev: 15.120000,0.430813
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3b40000000 of size 22020096 bytes.
[4.487s] Test is ready to start.
Test duration in range [10.000, 10.010] seconds
Num Accesses: 242789459.000000
Throughput: 24278945.900000
Num Accesses - Session Mean & Stddev: 2427894.590000,72575.595632
Num Phases Traversed: 1511.000000
Num Phases Traversed - Session Mean & Stddev: 15.110000,0.421782
Num Path Iterations: 1511.000000
Num Path Iterations - Session Mean & Stddev: 15.110000,0.421782
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdb00000000 of size 22020096 bytes.
[4.491s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 242789459.000000
Throughput: 24278945.900000
Num Accesses - Session Mean & Stddev: 2427894.590000,72575.595632
Num Phases Traversed: 1511.000000
Num Phases Traversed - Session Mean & Stddev: 15.110000,0.421782
Num Path Iterations: 1511.000000
Num Path Iterations - Session Mean & Stddev: 15.110000,0.421782
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3600000000 of size 22020096 bytes.
[4.485s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 242445321.000000
Throughput: 24244532.100000
Num Accesses - Session Mean & Stddev: 2424453.210000,73386.974624
Num Phases Traversed: 1509.000000
Num Phases Traversed - Session Mean & Stddev: 15.090000,0.426497
Num Path Iterations: 1509.000000
Num Path Iterations - Session Mean & Stddev: 15.090000,0.426497
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1fc0000000 of size 22020096 bytes.
[4.491s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 242789459.000000
Throughput: 24278945.900000
Num Accesses - Session Mean & Stddev: 2427894.590000,72575.595632
Num Phases Traversed: 1511.000000
Num Phases Traversed - Session Mean & Stddev: 15.110000,0.421782
Num Path Iterations: 1511.000000
Num Path Iterations - Session Mean & Stddev: 15.110000,0.421782
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1500000000 of size 22020096 bytes.
[4.485s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 242445321.000000
Throughput: 24244532.100000
Num Accesses - Session Mean & Stddev: 2424453.210000,73386.974624
Num Phases Traversed: 1509.000000
Num Phases Traversed - Session Mean & Stddev: 15.090000,0.426497
Num Path Iterations: 1509.000000
Num Path Iterations - Session Mean & Stddev: 15.090000,0.426497
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb0c0000000 of size 22020096 bytes.
[4.491s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 242789459.000000
Throughput: 24278945.900000
Num Accesses - Session Mean & Stddev: 2427894.590000,72575.595632
Num Phases Traversed: 1511.000000
Num Phases Traversed - Session Mean & Stddev: 15.110000,0.421782
Num Path Iterations: 1511.000000
Num Path Iterations - Session Mean & Stddev: 15.110000,0.421782
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1f80000000 of size 22020096 bytes.
[4.490s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 242789459.000000
Throughput: 24278945.900000
Num Accesses - Session Mean & Stddev: 2427894.590000,72575.595632
Num Phases Traversed: 1511.000000
Num Phases Traversed - Session Mean & Stddev: 15.110000,0.421782
Num Path Iterations: 1511.000000
Num Path Iterations - Session Mean & Stddev: 15.110000,0.421782
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7dc0000000 of size 22020096 bytes.
[4.489s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 242617390.000000
Throughput: 24261739.000000
Num Accesses - Session Mean & Stddev: 2426173.900000,70945.866189
Num Phases Traversed: 1510.000000
Num Phases Traversed - Session Mean & Stddev: 15.100000,0.412311
Num Path Iterations: 1510.000000
Num Path Iterations - Session Mean & Stddev: 15.100000,0.412311
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc440000000 of size 22020096 bytes.
[4.489s] Test is ready to start.
Test duration in range [10.000, 10.013] seconds
Num Accesses: 242617390.000000
Throughput: 24261739.000000
Num Accesses - Session Mean & Stddev: 2426173.900000,70945.866189
Num Phases Traversed: 1510.000000
Num Phases Traversed - Session Mean & Stddev: 15.100000,0.412311
Num Path Iterations: 1510.000000
Num Path Iterations - Session Mean & Stddev: 15.100000,0.412311
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe080000000 of size 22020096 bytes.
[4.488s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 242789459.000000
Throughput: 24278945.900000
Num Accesses - Session Mean & Stddev: 2427894.590000,72575.595632
Num Phases Traversed: 1511.000000
Num Phases Traversed - Session Mean & Stddev: 15.110000,0.421782
Num Path Iterations: 1511.000000
Num Path Iterations - Session Mean & Stddev: 15.110000,0.421782
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe500000000 of size 22020096 bytes.
[4.485s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 242961528.000000
Throughput: 24296152.800000
Num Accesses - Session Mean & Stddev: 2429615.280000,74129.593856
Num Phases Traversed: 1512.000000
Num Phases Traversed - Session Mean & Stddev: 15.120000,0.430813
Num Path Iterations: 1512.000000
Num Path Iterations - Session Mean & Stddev: 15.120000,0.430813
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9340000000 of size 22020096 bytes.
[4.485s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 242789459.000000
Throughput: 24278945.900000
Num Accesses - Session Mean & Stddev: 2427894.590000,72575.595632
Num Phases Traversed: 1511.000000
Num Phases Traversed - Session Mean & Stddev: 15.110000,0.421782
Num Path Iterations: 1511.000000
Num Path Iterations - Session Mean & Stddev: 15.110000,0.421782
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7f80000000 of size 22020096 bytes.
[4.484s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 242101183.000000
Throughput: 24210118.300000
Num Accesses - Session Mean & Stddev: 2421011.830000,74029.675231
Num Phases Traversed: 1507.000000
Num Phases Traversed - Session Mean & Stddev: 15.070000,0.430232
Num Path Iterations: 1507.000000
Num Path Iterations - Session Mean & Stddev: 15.070000,0.430232
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.1 0/172032,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.1 0/172032,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc5c0000000 of size 22020096 bytes.
[4.428s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 242445321.000000
Throughput: 24244532.100000
Num Accesses - Session Mean & Stddev: 2424453.210000,69235.057805
Num Phases Traversed: 1509.000000
Num Phases Traversed - Session Mean & Stddev: 15.090000,0.402368
Num Path Iterations: 1509.000000
Num Path Iterations - Session Mean & Stddev: 15.090000,0.402368
+ set +x

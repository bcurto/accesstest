++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2905400000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2740578200.000000
Throughput: 274057820.000000
Num Accesses - Session Mean & Stddev: 27405782.000000,162.369948
Num Phases Traversed: 8432648.000000
Num Phases Traversed - Session Mean & Stddev: 84326.480000,0.499600
Num Path Iterations: 8432648.000000
Num Path Iterations - Session Mean & Stddev: 84326.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8b59c00000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2776498500.000000
Throughput: 277649850.000000
Num Accesses - Session Mean & Stddev: 27764985.000000,145.924638
Num Phases Traversed: 8543172.000000
Num Phases Traversed - Session Mean & Stddev: 85431.720000,0.448999
Num Path Iterations: 8543172.000000
Num Path Iterations - Session Mean & Stddev: 85431.720000,0.448999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f643e600000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2761398675.000000
Throughput: 276139867.500000
Num Accesses - Session Mean & Stddev: 27613986.750000,101.689171
Num Phases Traversed: 8496711.000000
Num Phases Traversed - Session Mean & Stddev: 84967.110000,0.312890
Num Path Iterations: 8496711.000000
Num Path Iterations - Session Mean & Stddev: 84967.110000,0.312890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa9c7200000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2753195350.000000
Throughput: 275319535.000000
Num Accesses - Session Mean & Stddev: 27531953.500000,148.933710
Num Phases Traversed: 8471470.000000
Num Phases Traversed - Session Mean & Stddev: 84714.700000,0.458258
Num Path Iterations: 8471470.000000
Num Path Iterations - Session Mean & Stddev: 84714.700000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f77da200000 of size 36864 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2767588950.000000
Throughput: 276758895.000000
Num Accesses - Session Mean & Stddev: 27675889.500000,466.870164
Num Phases Traversed: 8515758.000000
Num Phases Traversed - Session Mean & Stddev: 85157.580000,1.436524
Num Path Iterations: 8515758.000000
Num Path Iterations - Session Mean & Stddev: 85157.580000,1.436524
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd58da00000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2764908675.000000
Throughput: 276490867.500000
Num Accesses - Session Mean & Stddev: 27649086.750000,101.689171
Num Phases Traversed: 8507511.000000
Num Phases Traversed - Session Mean & Stddev: 85075.110000,0.312890
Num Path Iterations: 8507511.000000
Num Path Iterations - Session Mean & Stddev: 85075.110000,0.312890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1d24400000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2754540850.000000
Throughput: 275454085.000000
Num Accesses - Session Mean & Stddev: 27545408.500000,97.500000
Num Phases Traversed: 8475610.000000
Num Phases Traversed - Session Mean & Stddev: 84756.100000,0.300000
Num Path Iterations: 8475610.000000
Num Path Iterations - Session Mean & Stddev: 84756.100000,0.300000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8755400000 of size 36864 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2767446925.000000
Throughput: 276744692.500000
Num Accesses - Session Mean & Stddev: 27674469.250000,2839.352600
Num Phases Traversed: 8515321.000000
Num Phases Traversed - Session Mean & Stddev: 85153.210000,8.736470
Num Path Iterations: 8515321.000000
Num Path Iterations - Session Mean & Stddev: 85153.210000,8.736470
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0ebb200000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2773291400.000000
Throughput: 277329140.000000
Num Accesses - Session Mean & Stddev: 27732914.000000,63.686733
Num Phases Traversed: 8533304.000000
Num Phases Traversed - Session Mean & Stddev: 85333.040000,0.195959
Num Path Iterations: 8533304.000000
Num Path Iterations - Session Mean & Stddev: 85333.040000,0.195959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc9a2600000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2760448700.000000
Throughput: 276044870.000000
Num Accesses - Session Mean & Stddev: 27604487.000000,105.612499
Num Phases Traversed: 8493788.000000
Num Phases Traversed - Session Mean & Stddev: 84937.880000,0.324962
Num Path Iterations: 8493788.000000
Num Path Iterations - Session Mean & Stddev: 84937.880000,0.324962
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7febe1e00000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2744087875.000000
Throughput: 274408787.500000
Num Accesses - Session Mean & Stddev: 27440878.750000,162.207236
Num Phases Traversed: 8443447.000000
Num Phases Traversed - Session Mean & Stddev: 84434.470000,0.499099
Num Path Iterations: 8443447.000000
Num Path Iterations - Session Mean & Stddev: 84434.470000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9980200000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2766812200.000000
Throughput: 276681220.000000
Num Accesses - Session Mean & Stddev: 27668122.000000,151.604749
Num Phases Traversed: 8513368.000000
Num Phases Traversed - Session Mean & Stddev: 85133.680000,0.466476
Num Path Iterations: 8513368.000000
Num Path Iterations - Session Mean & Stddev: 85133.680000,0.466476
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f074de00000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2763323650.000000
Throughput: 276332365.000000
Num Accesses - Session Mean & Stddev: 27633236.500000,153.955351
Num Phases Traversed: 8502634.000000
Num Phases Traversed - Session Mean & Stddev: 85026.340000,0.473709
Num Path Iterations: 8502634.000000
Num Path Iterations - Session Mean & Stddev: 85026.340000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4914e00000 of size 36864 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2756302350.000000
Throughput: 275630235.000000
Num Accesses - Session Mean & Stddev: 27563023.500000,148.933710
Num Phases Traversed: 8481030.000000
Num Phases Traversed - Session Mean & Stddev: 84810.300000,0.458258
Num Path Iterations: 8481030.000000
Num Path Iterations - Session Mean & Stddev: 84810.300000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1644200000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2743038775.000000
Throughput: 274303877.500000
Num Accesses - Session Mean & Stddev: 27430387.750000,127.497794
Num Phases Traversed: 8440219.000000
Num Phases Traversed - Session Mean & Stddev: 84402.190000,0.392301
Num Path Iterations: 8440219.000000
Num Path Iterations - Session Mean & Stddev: 84402.190000,0.392301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f88ee200000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2766063075.000000
Throughput: 276606307.500000
Num Accesses - Session Mean & Stddev: 27660630.750000,156.911400
Num Phases Traversed: 8511063.000000
Num Phases Traversed - Session Mean & Stddev: 85110.630000,0.482804
Num Path Iterations: 8511063.000000
Num Path Iterations - Session Mean & Stddev: 85110.630000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0796a00000 of size 36864 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2761056125.000000
Throughput: 276105612.500000
Num Accesses - Session Mean & Stddev: 27610561.250000,160.899619
Num Phases Traversed: 8495657.000000
Num Phases Traversed - Session Mean & Stddev: 84956.570000,0.495076
Num Path Iterations: 8495657.000000
Num Path Iterations - Session Mean & Stddev: 84956.570000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f54a4a00000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2758332625.000000
Throughput: 275833262.500000
Num Accesses - Session Mean & Stddev: 27583326.250000,136.770565
Num Phases Traversed: 8487277.000000
Num Phases Traversed - Session Mean & Stddev: 84872.770000,0.420833
Num Path Iterations: 8487277.000000
Num Path Iterations - Session Mean & Stddev: 84872.770000,0.420833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff186a00000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2764436775.000000
Throughput: 276443677.500000
Num Accesses - Session Mean & Stddev: 27644367.750000,159.845824
Num Phases Traversed: 8506059.000000
Num Phases Traversed - Session Mean & Stddev: 85060.590000,0.491833
Num Path Iterations: 8506059.000000
Num Path Iterations - Session Mean & Stddev: 85060.590000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/288,0.001 0/288,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/288,0.001 0/288,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    288 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2befa00000 of size 36864 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2763471200.000000
Throughput: 276347120.000000
Num Accesses - Session Mean & Stddev: 27634712.000000,105.612499
Num Phases Traversed: 8503088.000000
Num Phases Traversed - Session Mean & Stddev: 85030.880000,0.324962
Num Path Iterations: 8503088.000000
Num Path Iterations - Session Mean & Stddev: 85030.880000,0.324962
+ set +x

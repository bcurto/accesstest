++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb54bc00000 of size 327680 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2120762240.000000
Throughput: 212076224.000000
Num Accesses - Session Mean & Stddev: 21207622.400000,1600.898317
Num Phases Traversed: 816720.000000
Num Phases Traversed - Session Mean & Stddev: 8167.200000,0.616441
Num Path Iterations: 816720.000000
Num Path Iterations - Session Mean & Stddev: 8167.200000,0.616441
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f01a9a00000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2120541495.000000
Throughput: 212054149.500000
Num Accesses - Session Mean & Stddev: 21205414.950000,1343.178844
Num Phases Traversed: 816635.000000
Num Phases Traversed - Session Mean & Stddev: 8166.350000,0.517204
Num Path Iterations: 816635.000000
Num Path Iterations - Session Mean & Stddev: 8166.350000,0.517204
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe762e00000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2120622002.000000
Throughput: 212062200.200000
Num Accesses - Session Mean & Stddev: 21206220.020000,1384.958454
Num Phases Traversed: 816666.000000
Num Phases Traversed - Session Mean & Stddev: 8166.660000,0.533292
Num Path Iterations: 816666.000000
Num Path Iterations - Session Mean & Stddev: 8166.660000,0.533292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f45f7600000 of size 327680 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2120910269.000000
Throughput: 212091026.900000
Num Accesses - Session Mean & Stddev: 21209102.690000,1551.911226
Num Phases Traversed: 816777.000000
Num Phases Traversed - Session Mean & Stddev: 8167.770000,0.597578
Num Path Iterations: 816777.000000
Num Path Iterations - Session Mean & Stddev: 8167.770000,0.597578
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa151800000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2120562271.000000
Throughput: 212056227.100000
Num Accesses - Session Mean & Stddev: 21205622.710000,1434.475230
Num Phases Traversed: 816643.000000
Num Phases Traversed - Session Mean & Stddev: 8166.430000,0.552359
Num Path Iterations: 816643.000000
Num Path Iterations - Session Mean & Stddev: 8166.430000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f914c400000 of size 327680 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120899881.000000
Throughput: 212089988.100000
Num Accesses - Session Mean & Stddev: 21208998.810000,1677.227770
Num Phases Traversed: 816773.000000
Num Phases Traversed - Session Mean & Stddev: 8167.730000,0.645833
Num Path Iterations: 816773.000000
Num Path Iterations - Session Mean & Stddev: 8167.730000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbb09e00000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2120949224.000000
Throughput: 212094922.400000
Num Accesses - Session Mean & Stddev: 21209492.240000,1629.294136
Num Phases Traversed: 816792.000000
Num Phases Traversed - Session Mean & Stddev: 8167.920000,0.627375
Num Path Iterations: 816792.000000
Num Path Iterations - Session Mean & Stddev: 8167.920000,0.627375
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb68c600000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2121310207.000000
Throughput: 212131020.700000
Num Accesses - Session Mean & Stddev: 21213102.070000,1407.899785
Num Phases Traversed: 816931.000000
Num Phases Traversed - Session Mean & Stddev: 8169.310000,0.542125
Num Path Iterations: 816931.000000
Num Path Iterations - Session Mean & Stddev: 8169.310000,0.542125
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcbc2e00000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120396063.000000
Throughput: 212039606.300000
Num Accesses - Session Mean & Stddev: 21203960.630000,1734.166074
Num Phases Traversed: 816579.000000
Num Phases Traversed - Session Mean & Stddev: 8165.790000,0.667757
Num Path Iterations: 816579.000000
Num Path Iterations - Session Mean & Stddev: 8165.790000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fed44400000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120705106.000000
Throughput: 212070510.600000
Num Accesses - Session Mean & Stddev: 21207051.060000,1682.246717
Num Phases Traversed: 816698.000000
Num Phases Traversed - Session Mean & Stddev: 8166.980000,0.647765
Num Path Iterations: 816698.000000
Num Path Iterations - Session Mean & Stddev: 8166.980000,0.647765
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9c02000000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121014149.000000
Throughput: 212101414.900000
Num Accesses - Session Mean & Stddev: 21210141.490000,1516.745829
Num Phases Traversed: 816817.000000
Num Phases Traversed - Session Mean & Stddev: 8168.170000,0.584038
Num Path Iterations: 816817.000000
Num Path Iterations - Session Mean & Stddev: 8168.170000,0.584038
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f812f200000 of size 327680 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2121021940.000000
Throughput: 212102194.000000
Num Accesses - Session Mean & Stddev: 21210219.400000,1642.487017
Num Phases Traversed: 816820.000000
Num Phases Traversed - Session Mean & Stddev: 8168.200000,0.632456
Num Path Iterations: 816820.000000
Num Path Iterations - Session Mean & Stddev: 8168.200000,0.632456
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe763400000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120858329.000000
Throughput: 212085832.900000
Num Accesses - Session Mean & Stddev: 21208583.290000,1434.475230
Num Phases Traversed: 816757.000000
Num Phases Traversed - Session Mean & Stddev: 8167.570000,0.552359
Num Path Iterations: 816757.000000
Num Path Iterations - Session Mean & Stddev: 8167.570000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffab5a00000 of size 327680 bytes.
[1.241s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2120401257.000000
Throughput: 212040125.700000
Num Accesses - Session Mean & Stddev: 21204012.570000,1825.120162
Num Phases Traversed: 816581.000000
Num Phases Traversed - Session Mean & Stddev: 8165.810000,0.702780
Num Path Iterations: 816581.000000
Num Path Iterations - Session Mean & Stddev: 8165.810000,0.702780
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f698fc00000 of size 327680 bytes.
[1.244s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2120510331.000000
Throughput: 212051033.100000
Num Accesses - Session Mean & Stddev: 21205103.310000,1677.227770
Num Phases Traversed: 816623.000000
Num Phases Traversed - Session Mean & Stddev: 8166.230000,0.645833
Num Path Iterations: 816623.000000
Num Path Iterations - Session Mean & Stddev: 8166.230000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f21cee00000 of size 327680 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120681733.000000
Throughput: 212068173.300000
Num Accesses - Session Mean & Stddev: 21206817.330000,1553.648603
Num Phases Traversed: 816689.000000
Num Phases Traversed - Session Mean & Stddev: 8166.890000,0.598247
Num Path Iterations: 816689.000000
Num Path Iterations - Session Mean & Stddev: 8166.890000,0.598247
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f203f400000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120790807.000000
Throughput: 212079080.700000
Num Accesses - Session Mean & Stddev: 21207908.070000,1629.914938
Num Phases Traversed: 816731.000000
Num Phases Traversed - Session Mean & Stddev: 8167.310000,0.627615
Num Path Iterations: 816731.000000
Num Path Iterations - Session Mean & Stddev: 8167.310000,0.627615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc24ac00000 of size 327680 bytes.
[1.244s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120227258.000000
Throughput: 212022725.800000
Num Accesses - Session Mean & Stddev: 21202272.580000,1683.849620
Num Phases Traversed: 816514.000000
Num Phases Traversed - Session Mean & Stddev: 8165.140000,0.648383
Num Path Iterations: 816514.000000
Num Path Iterations - Session Mean & Stddev: 8165.140000,0.648383
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff023e00000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2120471376.000000
Throughput: 212047137.600000
Num Accesses - Session Mean & Stddev: 21204713.760000,1629.294136
Num Phases Traversed: 816608.000000
Num Phases Traversed - Session Mean & Stddev: 8166.080000,0.627375
Num Path Iterations: 816608.000000
Num Path Iterations - Session Mean & Stddev: 8166.080000,0.627375
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0 0/2560,0' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0 0/2560,0 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdce7a00000 of size 327680 bytes.
[1.277s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2120715494.000000
Throughput: 212071549.400000
Num Accesses - Session Mean & Stddev: 21207154.940000,1513.407181
Num Phases Traversed: 816702.000000
Num Phases Traversed - Session Mean & Stddev: 8167.020000,0.582752
Num Path Iterations: 816702.000000
Num Path Iterations - Session Mean & Stddev: 8167.020000,0.582752
+ set +x

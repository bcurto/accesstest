++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1016200000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831019542.000000
Throughput: 83101954.200000
Num Accesses - Session Mean & Stddev: 8310195.420000,16920.962078
Num Phases Traversed: 214446.000000
Num Phases Traversed - Session Mean & Stddev: 2144.460000,4.364447
Num Path Iterations: 214446.000000
Num Path Iterations - Session Mean & Stddev: 2144.460000,4.364447
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3af9600000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 830899355.000000
Throughput: 83089935.500000
Num Accesses - Session Mean & Stddev: 8308993.550000,18829.411974
Num Phases Traversed: 214415.000000
Num Phases Traversed - Session Mean & Stddev: 2144.150000,4.856696
Num Path Iterations: 214415.000000
Num Path Iterations - Session Mean & Stddev: 2144.150000,4.856696
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fda02000000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831162991.000000
Throughput: 83116299.100000
Num Accesses - Session Mean & Stddev: 8311629.910000,15834.638170
Num Phases Traversed: 214483.000000
Num Phases Traversed - Session Mean & Stddev: 2144.830000,4.084250
Num Path Iterations: 214483.000000
Num Path Iterations - Session Mean & Stddev: 2144.830000,4.084250
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0edd800000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 830961387.000000
Throughput: 83096138.700000
Num Accesses - Session Mean & Stddev: 8309613.870000,15446.767902
Num Phases Traversed: 214431.000000
Num Phases Traversed - Session Mean & Stddev: 2144.310000,3.984206
Num Path Iterations: 214431.000000
Num Path Iterations - Session Mean & Stddev: 2144.310000,3.984206
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f97a5400000 of size 491520 bytes.
[1.283s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831035050.000000
Throughput: 83103505.000000
Num Accesses - Session Mean & Stddev: 8310350.500000,15160.011953
Num Phases Traversed: 214450.000000
Num Phases Traversed - Session Mean & Stddev: 2144.500000,3.910243
Num Path Iterations: 214450.000000
Num Path Iterations - Session Mean & Stddev: 2144.500000,3.910243
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0031400000 of size 491520 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831190130.000000
Throughput: 83119013.000000
Num Accesses - Session Mean & Stddev: 8311901.300000,16580.692067
Num Phases Traversed: 214490.000000
Num Phases Traversed - Session Mean & Stddev: 2144.900000,4.276681
Num Path Iterations: 214490.000000
Num Path Iterations - Session Mean & Stddev: 2144.900000,4.276681
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe25ee00000 of size 491520 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831089328.000000
Throughput: 83108932.800000
Num Accesses - Session Mean & Stddev: 8310893.280000,16653.238199
Num Phases Traversed: 214464.000000
Num Phases Traversed - Session Mean & Stddev: 2144.640000,4.295393
Num Path Iterations: 214464.000000
Num Path Iterations - Session Mean & Stddev: 2144.640000,4.295393
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f940c800000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831190130.000000
Throughput: 83119013.000000
Num Accesses - Session Mean & Stddev: 8311901.300000,17221.038737
Num Phases Traversed: 214490.000000
Num Phases Traversed - Session Mean & Stddev: 2144.900000,4.441846
Num Path Iterations: 214490.000000
Num Path Iterations - Session Mean & Stddev: 2144.900000,4.441846
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f70e7800000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831159114.000000
Throughput: 83115911.400000
Num Accesses - Session Mean & Stddev: 8311591.140000,17263.580549
Num Phases Traversed: 214482.000000
Num Phases Traversed - Session Mean & Stddev: 2144.820000,4.452819
Num Path Iterations: 214482.000000
Num Path Iterations - Session Mean & Stddev: 2144.820000,4.452819
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5d0ce00000 of size 491520 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831116467.000000
Throughput: 83111646.700000
Num Accesses - Session Mean & Stddev: 8311164.670000,16221.388386
Num Phases Traversed: 214471.000000
Num Phases Traversed - Session Mean & Stddev: 2144.710000,4.184005
Num Path Iterations: 214471.000000
Num Path Iterations - Session Mean & Stddev: 2144.710000,4.184005
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7702200000 of size 491520 bytes.
[1.283s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831151360.000000
Throughput: 83115136.000000
Num Accesses - Session Mean & Stddev: 8311513.600000,16603.340202
Num Phases Traversed: 214480.000000
Num Phases Traversed - Session Mean & Stddev: 2144.800000,4.282523
Num Path Iterations: 214480.000000
Num Path Iterations - Session Mean & Stddev: 2144.800000,4.282523
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2339600000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831108713.000000
Throughput: 83110871.300000
Num Accesses - Session Mean & Stddev: 8311087.130000,17187.795270
Num Phases Traversed: 214469.000000
Num Phases Traversed - Session Mean & Stddev: 2144.690000,4.433272
Num Path Iterations: 214469.000000
Num Path Iterations - Session Mean & Stddev: 2144.690000,4.433272
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdb2a800000 of size 491520 bytes.
[1.249s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 830810184.000000
Throughput: 83081018.400000
Num Accesses - Session Mean & Stddev: 8308101.840000,16654.682288
Num Phases Traversed: 214392.000000
Num Phases Traversed - Session Mean & Stddev: 2143.920000,4.295765
Num Path Iterations: 214392.000000
Num Path Iterations - Session Mean & Stddev: 2143.920000,4.295765
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcc44c00000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831069943.000000
Throughput: 83106994.300000
Num Accesses - Session Mean & Stddev: 8310699.430000,16944.707818
Num Phases Traversed: 214459.000000
Num Phases Traversed - Session Mean & Stddev: 2144.590000,4.370572
Num Path Iterations: 214459.000000
Num Path Iterations - Session Mean & Stddev: 2144.590000,4.370572
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7f7fe00000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831062189.000000
Throughput: 83106218.900000
Num Accesses - Session Mean & Stddev: 8310621.890000,15549.186894
Num Phases Traversed: 214457.000000
Num Phases Traversed - Session Mean & Stddev: 2144.570000,4.010623
Num Path Iterations: 214457.000000
Num Path Iterations - Session Mean & Stddev: 2144.570000,4.010623
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd8ef000000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831019542.000000
Throughput: 83101954.200000
Num Accesses - Session Mean & Stddev: 8310195.420000,16983.030227
Num Phases Traversed: 214446.000000
Num Phases Traversed - Session Mean & Stddev: 2144.460000,4.380457
Num Path Iterations: 214446.000000
Num Path Iterations - Session Mean & Stddev: 2144.460000,4.380457
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f64bf400000 of size 491520 bytes.
[1.283s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831197884.000000
Throughput: 83119788.400000
Num Accesses - Session Mean & Stddev: 8311978.840000,17135.111947
Num Phases Traversed: 214492.000000
Num Phases Traversed - Session Mean & Stddev: 2144.920000,4.419683
Num Path Iterations: 214492.000000
Num Path Iterations - Session Mean & Stddev: 2144.920000,4.419683
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcc60e00000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831236654.000000
Throughput: 83123665.400000
Num Accesses - Session Mean & Stddev: 8312366.540000,18076.800934
Num Phases Traversed: 214502.000000
Num Phases Traversed - Session Mean & Stddev: 2145.020000,4.662574
Num Path Iterations: 214502.000000
Num Path Iterations - Session Mean & Stddev: 2145.020000,4.662574
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1241600000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 830895478.000000
Throughput: 83089547.800000
Num Accesses - Session Mean & Stddev: 8308954.780000,14107.379644
Num Phases Traversed: 214414.000000
Num Phases Traversed - Session Mean & Stddev: 2144.140000,3.638736
Num Path Iterations: 214414.000000
Num Path Iterations - Session Mean & Stddev: 2144.140000,3.638736
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3840,0.001 0/3840,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3840,0.001 0/3840,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3840 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcd5d200000 of size 491520 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831042804.000000
Throughput: 83104280.400000
Num Accesses - Session Mean & Stddev: 8310428.040000,17071.835837
Num Phases Traversed: 214452.000000
Num Phases Traversed - Session Mean & Stddev: 2144.520000,4.403362
Num Path Iterations: 214452.000000
Num Path Iterations - Session Mean & Stddev: 2144.520000,4.403362
+ set +x

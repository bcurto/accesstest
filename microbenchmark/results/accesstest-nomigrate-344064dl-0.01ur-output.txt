++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8880000000 of size 22020096 bytes.
[4.480s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,144742.659097
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.841190
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.841190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9000000000 of size 22020096 bytes.
[4.431s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 236422906.000000
Throughput: 23642290.600000
Num Accesses - Session Mean & Stddev: 2364229.060000,145232.759797
Num Phases Traversed: 1474.000000
Num Phases Traversed - Session Mean & Stddev: 14.740000,0.844038
Num Path Iterations: 1474.000000
Num Path Iterations - Session Mean & Stddev: 14.740000,0.844038
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1e40000000 of size 22020096 bytes.
[4.485s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 237455320.000000
Throughput: 23745532.000000
Num Accesses - Session Mean & Stddev: 2374553.200000,151967.226051
Num Phases Traversed: 1480.000000
Num Phases Traversed - Session Mean & Stddev: 14.800000,0.883176
Num Path Iterations: 1480.000000
Num Path Iterations - Session Mean & Stddev: 14.800000,0.883176
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff580000000 of size 22020096 bytes.
[4.487s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,152705.784512
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.887468
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.887468
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4040000000 of size 22020096 bytes.
[4.481s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 238143596.000000
Throughput: 23814359.600000
Num Accesses - Session Mean & Stddev: 2381435.960000,153363.585184
Num Phases Traversed: 1484.000000
Num Phases Traversed - Session Mean & Stddev: 14.840000,0.891291
Num Path Iterations: 1484.000000
Num Path Iterations - Session Mean & Stddev: 14.840000,0.891291
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd3c0000000 of size 22020096 bytes.
[4.485s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,156535.511159
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.909725
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.909725
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbb00000000 of size 22020096 bytes.
[4.485s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 238143596.000000
Throughput: 23814359.600000
Num Accesses - Session Mean & Stddev: 2381435.960000,155282.143454
Num Phases Traversed: 1484.000000
Num Phases Traversed - Session Mean & Stddev: 14.840000,0.902441
Num Path Iterations: 1484.000000
Num Path Iterations - Session Mean & Stddev: 14.840000,0.902441
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8d00000000 of size 22020096 bytes.
[4.485s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 238143596.000000
Throughput: 23814359.600000
Num Accesses - Session Mean & Stddev: 2381435.960000,155282.143454
Num Phases Traversed: 1484.000000
Num Phases Traversed - Session Mean & Stddev: 14.840000,0.902441
Num Path Iterations: 1484.000000
Num Path Iterations - Session Mean & Stddev: 14.840000,0.902441
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f10c0000000 of size 22020096 bytes.
[4.487s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,152705.784512
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.887468
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.887468
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3ac0000000 of size 22020096 bytes.
[4.481s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 238143596.000000
Throughput: 23814359.600000
Num Accesses - Session Mean & Stddev: 2381435.960000,155282.143454
Num Phases Traversed: 1484.000000
Num Phases Traversed - Session Mean & Stddev: 14.840000,0.902441
Num Path Iterations: 1484.000000
Num Path Iterations - Session Mean & Stddev: 14.840000,0.902441
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4540000000 of size 22020096 bytes.
[4.490s] Test is ready to start.
Test duration in range [10.000, 10.012] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,152705.784512
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.887468
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.887468
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9340000000 of size 22020096 bytes.
[4.484s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,148777.508357
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.864639
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.864639
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efb80000000 of size 22020096 bytes.
[4.428s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 237455320.000000
Throughput: 23745532.000000
Num Accesses - Session Mean & Stddev: 2374553.200000,151967.226051
Num Phases Traversed: 1480.000000
Num Phases Traversed - Session Mean & Stddev: 14.800000,0.883176
Num Path Iterations: 1480.000000
Num Path Iterations - Session Mean & Stddev: 14.800000,0.883176
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faa80000000 of size 22020096 bytes.
[4.487s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,148777.508357
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.864639
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.864639
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f79c0000000 of size 22020096 bytes.
[4.428s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,148777.508357
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.864639
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.864639
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f51c0000000 of size 22020096 bytes.
[4.483s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 238143596.000000
Throughput: 23814359.600000
Num Accesses - Session Mean & Stddev: 2381435.960000,157177.284908
Num Phases Traversed: 1484.000000
Num Phases Traversed - Session Mean & Stddev: 14.840000,0.913455
Num Path Iterations: 1484.000000
Num Path Iterations - Session Mean & Stddev: 14.840000,0.913455
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc240000000 of size 22020096 bytes.
[4.493s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 238143596.000000
Throughput: 23814359.600000
Num Accesses - Session Mean & Stddev: 2381435.960000,157177.284908
Num Phases Traversed: 1484.000000
Num Phases Traversed - Session Mean & Stddev: 14.840000,0.913455
Num Path Iterations: 1484.000000
Num Path Iterations - Session Mean & Stddev: 14.840000,0.913455
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f20c0000000 of size 22020096 bytes.
[4.481s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,148777.508357
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.864639
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.864639
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc080000000 of size 22020096 bytes.
[4.483s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 237799458.000000
Throughput: 23779945.800000
Num Accesses - Session Mean & Stddev: 2377994.580000,156535.511159
Num Phases Traversed: 1482.000000
Num Phases Traversed - Session Mean & Stddev: 14.820000,0.909725
Num Path Iterations: 1482.000000
Num Path Iterations - Session Mean & Stddev: 14.820000,0.909725
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/172032,0.01 0/172032,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/172032,0.01 0/172032,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    172032 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4a40000000 of size 22020096 bytes.
[4.483s] Test is ready to start.
Test duration in range [10.000, 10.013] seconds
Num Accesses: 238143596.000000
Throughput: 23814359.600000
Num Accesses - Session Mean & Stddev: 2381435.960000,164539.716032
Num Phases Traversed: 1484.000000
Num Phases Traversed - Session Mean & Stddev: 14.840000,0.956243
Num Path Iterations: 1484.000000
Num Path Iterations - Session Mean & Stddev: 14.840000,0.956243
+ set +x

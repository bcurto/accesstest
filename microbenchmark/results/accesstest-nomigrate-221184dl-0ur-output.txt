++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6c00000000 of size 14155776 bytes.
[2.612s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 383440214.000000
Throughput: 38344021.400000
Num Accesses - Session Mean & Stddev: 3834402.140000,120397.698770
Num Phases Traversed: 3566.000000
Num Phases Traversed - Session Mean & Stddev: 35.660000,1.088301
Num Path Iterations: 3566.000000
Num Path Iterations - Session Mean & Stddev: 35.660000,1.088301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa8c0000000 of size 14155776 bytes.
[2.577s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 383218956.000000
Throughput: 38321895.600000
Num Accesses - Session Mean & Stddev: 3832189.560000,119684.013177
Num Phases Traversed: 3564.000000
Num Phases Traversed - Session Mean & Stddev: 35.640000,1.081850
Num Path Iterations: 3564.000000
Num Path Iterations - Session Mean & Stddev: 35.640000,1.081850
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe400000000 of size 14155776 bytes.
[2.582s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 383882730.000000
Throughput: 38388273.000000
Num Accesses - Session Mean & Stddev: 3838827.300000,121691.900000
Num Phases Traversed: 3570.000000
Num Phases Traversed - Session Mean & Stddev: 35.700000,1.100000
Num Path Iterations: 3570.000000
Num Path Iterations - Session Mean & Stddev: 35.700000,1.100000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fddc0000000 of size 14155776 bytes.
[2.553s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 385210278.000000
Throughput: 38521027.800000
Num Accesses - Session Mean & Stddev: 3852102.780000,117475.288520
Num Phases Traversed: 3582.000000
Num Phases Traversed - Session Mean & Stddev: 35.820000,1.061885
Num Path Iterations: 3582.000000
Num Path Iterations - Session Mean & Stddev: 35.820000,1.061885
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe3c0000000 of size 14155776 bytes.
[2.604s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 383882730.000000
Throughput: 38388273.000000
Num Accesses - Session Mean & Stddev: 3838827.300000,119663.559616
Num Phases Traversed: 3570.000000
Num Phases Traversed - Session Mean & Stddev: 35.700000,1.081665
Num Path Iterations: 3570.000000
Num Path Iterations - Session Mean & Stddev: 35.700000,1.081665
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe4c0000000 of size 14155776 bytes.
[2.609s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 384546504.000000
Throughput: 38454650.400000
Num Accesses - Session Mean & Stddev: 3845465.040000,112949.987730
Num Phases Traversed: 3576.000000
Num Phases Traversed - Session Mean & Stddev: 35.760000,1.020980
Num Path Iterations: 3576.000000
Num Path Iterations - Session Mean & Stddev: 35.760000,1.020980
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f53c0000000 of size 14155776 bytes.
[2.568s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 384767762.000000
Throughput: 38476776.200000
Num Accesses - Session Mean & Stddev: 3847677.620000,121772.330875
Num Phases Traversed: 3578.000000
Num Phases Traversed - Session Mean & Stddev: 35.780000,1.100727
Num Path Iterations: 3578.000000
Num Path Iterations - Session Mean & Stddev: 35.780000,1.100727
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc4c0000000 of size 14155776 bytes.
[2.579s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 385431536.000000
Throughput: 38543153.600000
Num Accesses - Session Mean & Stddev: 3854315.360000,113598.261588
Num Phases Traversed: 3584.000000
Num Phases Traversed - Session Mean & Stddev: 35.840000,1.026840
Num Path Iterations: 3584.000000
Num Path Iterations - Session Mean & Stddev: 35.840000,1.026840
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2cc0000000 of size 14155776 bytes.
[2.616s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 384989020.000000
Throughput: 38498902.000000
Num Accesses - Session Mean & Stddev: 3849890.200000,117078.728717
Num Phases Traversed: 3580.000000
Num Phases Traversed - Session Mean & Stddev: 35.800000,1.058301
Num Path Iterations: 3580.000000
Num Path Iterations - Session Mean & Stddev: 35.800000,1.058301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f93c0000000 of size 14155776 bytes.
[2.507s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 383440214.000000
Throughput: 38344021.400000
Num Accesses - Session Mean & Stddev: 3834402.140000,114135.677123
Num Phases Traversed: 3566.000000
Num Phases Traversed - Session Mean & Stddev: 35.660000,1.031698
Num Path Iterations: 3566.000000
Num Path Iterations - Session Mean & Stddev: 35.660000,1.031698
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3f80000000 of size 14155776 bytes.
[2.588s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 384325246.000000
Throughput: 38432524.600000
Num Accesses - Session Mean & Stddev: 3843252.460000,119786.228597
Num Phases Traversed: 3574.000000
Num Phases Traversed - Session Mean & Stddev: 35.740000,1.082774
Num Path Iterations: 3574.000000
Num Path Iterations - Session Mean & Stddev: 35.740000,1.082774
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6dc0000000 of size 14155776 bytes.
[2.625s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 383218956.000000
Throughput: 38321895.600000
Num Accesses - Session Mean & Stddev: 3832189.560000,121712.012702
Num Phases Traversed: 3564.000000
Num Phases Traversed - Session Mean & Stddev: 35.640000,1.100182
Num Path Iterations: 3564.000000
Num Path Iterations - Session Mean & Stddev: 35.640000,1.100182
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f25c0000000 of size 14155776 bytes.
[2.554s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 384103988.000000
Throughput: 38410398.800000
Num Accesses - Session Mean & Stddev: 3841039.880000,113985.456180
Num Phases Traversed: 3572.000000
Num Phases Traversed - Session Mean & Stddev: 35.720000,1.030340
Num Path Iterations: 3572.000000
Num Path Iterations - Session Mean & Stddev: 35.720000,1.030340
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efe00000000 of size 14155776 bytes.
[2.543s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 384989020.000000
Throughput: 38498902.000000
Num Accesses - Session Mean & Stddev: 3849890.200000,116028.674064
Num Phases Traversed: 3580.000000
Num Phases Traversed - Session Mean & Stddev: 35.800000,1.048809
Num Path Iterations: 3580.000000
Num Path Iterations - Session Mean & Stddev: 35.800000,1.048809
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9640000000 of size 14155776 bytes.
[2.589s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 383440214.000000
Throughput: 38344021.400000
Num Accesses - Session Mean & Stddev: 3834402.140000,115202.987397
Num Phases Traversed: 3566.000000
Num Phases Traversed - Session Mean & Stddev: 35.660000,1.041345
Num Path Iterations: 3566.000000
Num Path Iterations - Session Mean & Stddev: 35.660000,1.041345
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f28c0000000 of size 14155776 bytes.
[2.555s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 385431536.000000
Throughput: 38543153.600000
Num Accesses - Session Mean & Stddev: 3854315.360000,118863.125485
Num Phases Traversed: 3584.000000
Num Phases Traversed - Session Mean & Stddev: 35.840000,1.074430
Num Path Iterations: 3584.000000
Num Path Iterations - Session Mean & Stddev: 35.840000,1.074430
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7200000000 of size 14155776 bytes.
[2.618s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 383882730.000000
Throughput: 38388273.000000
Num Accesses - Session Mean & Stddev: 3838827.300000,121691.900000
Num Phases Traversed: 3570.000000
Num Phases Traversed - Session Mean & Stddev: 35.700000,1.100000
Num Path Iterations: 3570.000000
Num Path Iterations - Session Mean & Stddev: 35.700000,1.100000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6380000000 of size 14155776 bytes.
[2.618s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 384989020.000000
Throughput: 38498902.000000
Num Accesses - Session Mean & Stddev: 3849890.200000,118119.448994
Num Phases Traversed: 3580.000000
Num Phases Traversed - Session Mean & Stddev: 35.800000,1.067708
Num Path Iterations: 3580.000000
Num Path Iterations - Session Mean & Stddev: 35.800000,1.067708
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc500000000 of size 14155776 bytes.
[2.563s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 383661472.000000
Throughput: 38366147.200000
Num Accesses - Session Mean & Stddev: 3836614.720000,119027.755931
Num Phases Traversed: 3568.000000
Num Phases Traversed - Session Mean & Stddev: 35.680000,1.075918
Num Path Iterations: 3568.000000
Num Path Iterations - Session Mean & Stddev: 35.680000,1.075918
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/110592,0 0/110592,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/110592,0 0/110592,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    110592 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3200000000 of size 14155776 bytes.
[2.599s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 384325246.000000
Throughput: 38432524.600000
Num Accesses - Session Mean & Stddev: 3843252.460000,122813.138955
Num Phases Traversed: 3574.000000
Num Phases Traversed - Session Mean & Stddev: 35.740000,1.110135
Num Path Iterations: 3574.000000
Num Path Iterations - Session Mean & Stddev: 35.740000,1.110135
+ set +x

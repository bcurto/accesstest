++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2783600000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1116838207.000000
Throughput: 111683820.700000
Num Accesses - Session Mean & Stddev: 11168382.070000,2608.041151
Num Phases Traversed: 254563.000000
Num Phases Traversed - Session Mean & Stddev: 2545.630000,0.594222
Num Path Iterations: 254563.000000
Num Path Iterations - Session Mean & Stddev: 2545.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdc72200000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1120551301.000000
Throughput: 112055130.100000
Num Accesses - Session Mean & Stddev: 11205513.010000,2982.905961
Num Phases Traversed: 255409.000000
Num Phases Traversed - Session Mean & Stddev: 2554.090000,0.679632
Num Path Iterations: 255409.000000
Num Path Iterations - Session Mean & Stddev: 2554.090000,0.679632
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcef3e00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1121837278.000000
Throughput: 112183727.800000
Num Accesses - Session Mean & Stddev: 11218372.780000,2975.470103
Num Phases Traversed: 255702.000000
Num Phases Traversed - Session Mean & Stddev: 2557.020000,0.677938
Num Path Iterations: 255702.000000
Num Path Iterations - Session Mean & Stddev: 2557.020000,0.677938
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7497e00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1121288653.000000
Throughput: 112128865.300000
Num Accesses - Session Mean & Stddev: 11212886.530000,2901.723214
Num Phases Traversed: 255577.000000
Num Phases Traversed - Session Mean & Stddev: 2555.770000,0.661135
Num Path Iterations: 255577.000000
Num Path Iterations - Session Mean & Stddev: 2555.770000,0.661135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd16be00000 of size 557056 bytes.
[1.296s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1118172463.000000
Throughput: 111817246.300000
Num Accesses - Session Mean & Stddev: 11181724.630000,2709.474420
Num Phases Traversed: 254867.000000
Num Phases Traversed - Session Mean & Stddev: 2548.670000,0.617333
Num Path Iterations: 254867.000000
Num Path Iterations - Session Mean & Stddev: 2548.670000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5085e00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1119208267.000000
Throughput: 111920826.700000
Num Accesses - Session Mean & Stddev: 11192082.670000,3069.477203
Num Phases Traversed: 255103.000000
Num Phases Traversed - Session Mean & Stddev: 2551.030000,0.699357
Num Path Iterations: 255103.000000
Num Path Iterations - Session Mean & Stddev: 2551.030000,0.699357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7ad9800000 of size 557056 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1107858313.000000
Throughput: 110785831.300000
Num Accesses - Session Mean & Stddev: 11078583.130000,2980.321679
Num Phases Traversed: 252517.000000
Num Phases Traversed - Session Mean & Stddev: 2525.170000,0.679043
Num Path Iterations: 252517.000000
Num Path Iterations - Session Mean & Stddev: 2525.170000,0.679043
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2b08c00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1115038717.000000
Throughput: 111503871.700000
Num Accesses - Session Mean & Stddev: 11150387.170000,2276.787140
Num Phases Traversed: 254153.000000
Num Phases Traversed - Session Mean & Stddev: 2541.530000,0.518748
Num Path Iterations: 254153.000000
Num Path Iterations - Session Mean & Stddev: 2541.530000,0.518748
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5d69a00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1112146366.000000
Throughput: 111214636.600000
Num Accesses - Session Mean & Stddev: 11121463.660000,3029.363980
Num Phases Traversed: 253494.000000
Num Phases Traversed - Session Mean & Stddev: 2534.940000,0.690217
Num Path Iterations: 253494.000000
Num Path Iterations - Session Mean & Stddev: 2534.940000,0.690217
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f463cc00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1112519431.000000
Throughput: 111251943.100000
Num Accesses - Session Mean & Stddev: 11125194.310000,2864.305922
Num Phases Traversed: 253579.000000
Num Phases Traversed - Session Mean & Stddev: 2535.790000,0.652610
Num Path Iterations: 253579.000000
Num Path Iterations - Session Mean & Stddev: 2535.790000,0.652610
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7eff37000000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1116289582.000000
Throughput: 111628958.200000
Num Accesses - Session Mean & Stddev: 11162895.820000,2542.591302
Num Phases Traversed: 254438.000000
Num Phases Traversed - Session Mean & Stddev: 2544.380000,0.579310
Num Path Iterations: 254438.000000
Num Path Iterations - Session Mean & Stddev: 2544.380000,0.579310
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1637a00000 of size 557056 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1117417555.000000
Throughput: 111741755.500000
Num Accesses - Session Mean & Stddev: 11174175.550000,3000.933616
Num Phases Traversed: 254695.000000
Num Phases Traversed - Session Mean & Stddev: 2546.950000,0.683740
Num Path Iterations: 254695.000000
Num Path Iterations - Session Mean & Stddev: 2546.950000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5eb2a00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1116807484.000000
Throughput: 111680748.400000
Num Accesses - Session Mean & Stddev: 11168074.840000,2348.832717
Num Phases Traversed: 254556.000000
Num Phases Traversed - Session Mean & Stddev: 2545.560000,0.535164
Num Path Iterations: 254556.000000
Num Path Iterations - Session Mean & Stddev: 2545.560000,0.535164
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2144c00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1113621070.000000
Throughput: 111362107.000000
Num Accesses - Session Mean & Stddev: 11136210.700000,2740.929621
Num Phases Traversed: 253830.000000
Num Phases Traversed - Session Mean & Stddev: 2538.300000,0.624500
Num Path Iterations: 253830.000000
Num Path Iterations - Session Mean & Stddev: 2538.300000,0.624500
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8d6e600000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1114700764.000000
Throughput: 111470076.400000
Num Accesses - Session Mean & Stddev: 11147007.640000,2852.512365
Num Phases Traversed: 254076.000000
Num Phases Traversed - Session Mean & Stddev: 2540.760000,0.649923
Num Path Iterations: 254076.000000
Num Path Iterations - Session Mean & Stddev: 2540.760000,0.649923
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f61a5800000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1113950245.000000
Throughput: 111395024.500000
Num Accesses - Session Mean & Stddev: 11139502.450000,3000.933616
Num Phases Traversed: 253905.000000
Num Phases Traversed - Session Mean & Stddev: 2539.050000,0.683740
Num Path Iterations: 253905.000000
Num Path Iterations - Session Mean & Stddev: 2539.050000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8dfbe00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1113708850.000000
Throughput: 111370885.000000
Num Accesses - Session Mean & Stddev: 11137088.500000,2443.691779
Num Phases Traversed: 253850.000000
Num Phases Traversed - Session Mean & Stddev: 2538.500000,0.556776
Num Path Iterations: 253850.000000
Num Path Iterations - Session Mean & Stddev: 2538.500000,0.556776
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8a45e00000 of size 557056 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1115192332.000000
Throughput: 111519233.200000
Num Accesses - Session Mean & Stddev: 11151923.320000,3471.474271
Num Phases Traversed: 254188.000000
Num Phases Traversed - Session Mean & Stddev: 2541.880000,0.790949
Num Path Iterations: 254188.000000
Num Path Iterations - Session Mean & Stddev: 2541.880000,0.790949
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fec20c00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1116522199.000000
Throughput: 111652219.900000
Num Accesses - Session Mean & Stddev: 11165221.990000,3046.800681
Num Phases Traversed: 254491.000000
Num Phases Traversed - Session Mean & Stddev: 2544.910000,0.694190
Num Path Iterations: 254491.000000
Num Path Iterations - Session Mean & Stddev: 2544.910000,0.694190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0 0/4352,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0 0/4352,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7279e00000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1114551538.000000
Throughput: 111455153.800000
Num Accesses - Session Mean & Stddev: 11145515.380000,2718.346912
Num Phases Traversed: 254042.000000
Num Phases Traversed - Session Mean & Stddev: 2540.420000,0.619355
Num Path Iterations: 254042.000000
Num Path Iterations - Session Mean & Stddev: 2540.420000,0.619355
+ set +x

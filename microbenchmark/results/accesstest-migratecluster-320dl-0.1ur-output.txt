++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7b57800000 of size 20480 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2536888876.000000
Throughput: 253688887.600000
Num Accesses - Session Mean & Stddev: 25368888.760000,53.444760
Num Phases Traversed: 12877708.000000
Num Phases Traversed - Session Mean & Stddev: 128777.080000,0.271293
Num Path Iterations: 12877708.000000
Num Path Iterations - Session Mean & Stddev: 128777.080000,0.271293
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3567a00000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2583709866.000000
Throughput: 258370986.600000
Num Accesses - Session Mean & Stddev: 25837098.660000,81.606522
Num Phases Traversed: 13115378.000000
Num Phases Traversed - Session Mean & Stddev: 131153.780000,0.414246
Num Path Iterations: 13115378.000000
Num Path Iterations - Session Mean & Stddev: 131153.780000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0692400000 of size 20480 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2567981977.000000
Throughput: 256798197.700000
Num Accesses - Session Mean & Stddev: 25679819.770000,96.891161
Num Phases Traversed: 13035541.000000
Num Phases Traversed - Session Mean & Stddev: 130355.410000,0.491833
Num Path Iterations: 13035541.000000
Num Path Iterations - Session Mean & Stddev: 130355.410000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7276200000 of size 20480 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2565564787.000000
Throughput: 256556478.700000
Num Accesses - Session Mean & Stddev: 25655647.870000,93.632009
Num Phases Traversed: 13023271.000000
Num Phases Traversed - Session Mean & Stddev: 130232.710000,0.475289
Num Path Iterations: 13023271.000000
Num Path Iterations - Session Mean & Stddev: 130232.710000,0.475289
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3d40400000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2582907288.000000
Throughput: 258290728.800000
Num Accesses - Session Mean & Stddev: 25829072.880000,61.796485
Num Phases Traversed: 13111304.000000
Num Phases Traversed - Session Mean & Stddev: 131113.040000,0.313688
Num Path Iterations: 13111304.000000
Num Path Iterations - Session Mean & Stddev: 131113.040000,0.313688
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbd9b800000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2592402885.000000
Throughput: 259240288.500000
Num Accesses - Session Mean & Stddev: 25924028.850000,70.343070
Num Phases Traversed: 13159505.000000
Num Phases Traversed - Session Mean & Stddev: 131595.050000,0.357071
Num Path Iterations: 13159505.000000
Num Path Iterations - Session Mean & Stddev: 131595.050000,0.357071
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7214a00000 of size 20480 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2563891666.000000
Throughput: 256389166.600000
Num Accesses - Session Mean & Stddev: 25638916.660000,81.606522
Num Phases Traversed: 13014778.000000
Num Phases Traversed - Session Mean & Stddev: 130147.780000,0.414246
Num Path Iterations: 13014778.000000
Num Path Iterations - Session Mean & Stddev: 130147.780000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9a7f200000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2545415824.000000
Throughput: 254541582.400000
Num Accesses - Session Mean & Stddev: 25454158.240000,53.444760
Num Phases Traversed: 12920992.000000
Num Phases Traversed - Session Mean & Stddev: 129209.920000,0.271293
Num Path Iterations: 12920992.000000
Num Path Iterations - Session Mean & Stddev: 129209.920000,0.271293
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffb59400000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2599967094.000000
Throughput: 259996709.400000
Num Accesses - Session Mean & Stddev: 25999670.940000,39.202505
Num Phases Traversed: 13197902.000000
Num Phases Traversed - Session Mean & Stddev: 131979.020000,0.198997
Num Path Iterations: 13197902.000000
Num Path Iterations - Session Mean & Stddev: 131979.020000,0.198997
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffb5b400000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2582553673.000000
Throughput: 258255367.300000
Num Accesses - Session Mean & Stddev: 25825536.730000,62.885905
Num Phases Traversed: 13109509.000000
Num Phases Traversed - Session Mean & Stddev: 131095.090000,0.319218
Num Path Iterations: 13109509.000000
Num Path Iterations - Session Mean & Stddev: 131095.090000,0.319218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc212a00000 of size 20480 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2574969173.000000
Throughput: 257496917.300000
Num Accesses - Session Mean & Stddev: 25749691.730000,56.377807
Num Phases Traversed: 13071009.000000
Num Phases Traversed - Session Mean & Stddev: 130710.090000,0.286182
Num Path Iterations: 13071009.000000
Num Path Iterations - Session Mean & Stddev: 130710.090000,0.286182
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7facf2a00000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2532010171.000000
Throughput: 253201017.100000
Num Accesses - Session Mean & Stddev: 25320101.710000,97.529923
Num Phases Traversed: 12852943.000000
Num Phases Traversed - Session Mean & Stddev: 128529.430000,0.495076
Num Path Iterations: 12852943.000000
Num Path Iterations - Session Mean & Stddev: 128529.430000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fabaf000000 of size 20480 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2550757676.000000
Throughput: 255075767.600000
Num Accesses - Session Mean & Stddev: 25507576.760000,53.444760
Num Phases Traversed: 12948108.000000
Num Phases Traversed - Session Mean & Stddev: 129481.080000,0.271293
Num Path Iterations: 12948108.000000
Num Path Iterations - Session Mean & Stddev: 129481.080000,0.271293
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f821dc00000 of size 20480 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2566729648.000000
Throughput: 256672964.800000
Num Accesses - Session Mean & Stddev: 25667296.480000,72.221393
Num Phases Traversed: 13029184.000000
Num Phases Traversed - Session Mean & Stddev: 130291.840000,0.366606
Num Path Iterations: 13029184.000000
Num Path Iterations - Session Mean & Stddev: 130291.840000,0.366606
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd57c800000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2540840499.000000
Throughput: 254084049.900000
Num Accesses - Session Mean & Stddev: 25408404.990000,92.631905
Num Phases Traversed: 12897767.000000
Num Phases Traversed - Session Mean & Stddev: 128977.670000,0.470213
Num Path Iterations: 12897767.000000
Num Path Iterations - Session Mean & Stddev: 128977.670000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6619c00000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2565665848.000000
Throughput: 256566584.800000
Num Accesses - Session Mean & Stddev: 25656658.480000,72.221393
Num Phases Traversed: 13023784.000000
Num Phases Traversed - Session Mean & Stddev: 130237.840000,0.366606
Num Path Iterations: 13023784.000000
Num Path Iterations - Session Mean & Stddev: 130237.840000,0.366606
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb43fe00000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2554690781.000000
Throughput: 255469078.100000
Num Accesses - Session Mean & Stddev: 25546907.810000,87.460013
Num Phases Traversed: 12968073.000000
Num Phases Traversed - Session Mean & Stddev: 129680.730000,0.443959
Num Path Iterations: 12968073.000000
Num Path Iterations - Session Mean & Stddev: 129680.730000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1556600000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2547556426.000000
Throughput: 254755642.600000
Num Accesses - Session Mean & Stddev: 25475564.260000,97.231026
Num Phases Traversed: 12931858.000000
Num Phases Traversed - Session Mean & Stddev: 129318.580000,0.493559
Num Path Iterations: 12931858.000000
Num Path Iterations - Session Mean & Stddev: 129318.580000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f74b4800000 of size 20480 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2590308381.000000
Throughput: 259030838.100000
Num Accesses - Session Mean & Stddev: 25903083.810000,87.460013
Num Phases Traversed: 13148873.000000
Num Phases Traversed - Session Mean & Stddev: 131488.730000,0.443959
Num Path Iterations: 13148873.000000
Num Path Iterations - Session Mean & Stddev: 131488.730000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/160,0.1 0/160,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/160,0.1 0/160,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    160 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f98c3e00000 of size 20480 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2551831326.000000
Throughput: 255183132.600000
Num Accesses - Session Mean & Stddev: 25518313.260000,97.231026
Num Phases Traversed: 12953558.000000
Num Phases Traversed - Session Mean & Stddev: 129535.580000,0.493559
Num Path Iterations: 12953558.000000
Num Path Iterations - Session Mean & Stddev: 129535.580000,0.493559
+ set +x

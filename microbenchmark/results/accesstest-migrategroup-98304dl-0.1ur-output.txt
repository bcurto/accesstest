++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcc00000000 of size 6291456 bytes.
[1.680s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803699171.000000
Throughput: 80369917.100000
Num Accesses - Session Mean & Stddev: 8036991.710000,23991.931598
Num Phases Traversed: 16439.000000
Num Phases Traversed - Session Mean & Stddev: 164.390000,0.487750
Num Path Iterations: 16439.000000
Num Path Iterations - Session Mean & Stddev: 164.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1b40000000 of size 6291456 bytes.
[1.680s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803699171.000000
Throughput: 80369917.100000
Num Accesses - Session Mean & Stddev: 8036991.710000,23991.931598
Num Phases Traversed: 16439.000000
Num Phases Traversed - Session Mean & Stddev: 164.390000,0.487750
Num Path Iterations: 16439.000000
Num Path Iterations - Session Mean & Stddev: 164.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2400000000 of size 6291456 bytes.
[1.679s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 803649982.000000
Throughput: 80364998.200000
Num Accesses - Session Mean & Stddev: 8036499.820000,23875.673793
Num Phases Traversed: 16438.000000
Num Phases Traversed - Session Mean & Stddev: 164.380000,0.485386
Num Path Iterations: 16438.000000
Num Path Iterations - Session Mean & Stddev: 164.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6540000000 of size 6291456 bytes.
[1.680s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803649982.000000
Throughput: 80364998.200000
Num Accesses - Session Mean & Stddev: 8036499.820000,23875.673793
Num Phases Traversed: 16438.000000
Num Phases Traversed - Session Mean & Stddev: 164.380000,0.485386
Num Path Iterations: 16438.000000
Num Path Iterations - Session Mean & Stddev: 164.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd9c0000000 of size 6291456 bytes.
[1.679s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803699171.000000
Throughput: 80369917.100000
Num Accesses - Session Mean & Stddev: 8036991.710000,23991.931598
Num Phases Traversed: 16439.000000
Num Phases Traversed - Session Mean & Stddev: 164.390000,0.487750
Num Path Iterations: 16439.000000
Num Path Iterations - Session Mean & Stddev: 164.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff600000000 of size 6291456 bytes.
[1.679s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803649982.000000
Throughput: 80364998.200000
Num Accesses - Session Mean & Stddev: 8036499.820000,23875.673793
Num Phases Traversed: 16438.000000
Num Phases Traversed - Session Mean & Stddev: 164.380000,0.485386
Num Path Iterations: 16438.000000
Num Path Iterations - Session Mean & Stddev: 164.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0b80000000 of size 6291456 bytes.
[1.680s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 803649982.000000
Throughput: 80364998.200000
Num Accesses - Session Mean & Stddev: 8036499.820000,23875.673793
Num Phases Traversed: 16438.000000
Num Phases Traversed - Session Mean & Stddev: 164.380000,0.485386
Num Path Iterations: 16438.000000
Num Path Iterations - Session Mean & Stddev: 164.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fee40000000 of size 6291456 bytes.
[1.680s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803649982.000000
Throughput: 80364998.200000
Num Accesses - Session Mean & Stddev: 8036499.820000,23875.673793
Num Phases Traversed: 16438.000000
Num Phases Traversed - Session Mean & Stddev: 164.380000,0.485386
Num Path Iterations: 16438.000000
Num Path Iterations - Session Mean & Stddev: 164.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5800000000 of size 6291456 bytes.
[1.679s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803797549.000000
Throughput: 80379754.900000
Num Accesses - Session Mean & Stddev: 8037975.490000,24192.788444
Num Phases Traversed: 16441.000000
Num Phases Traversed - Session Mean & Stddev: 164.410000,0.491833
Num Path Iterations: 16441.000000
Num Path Iterations - Session Mean & Stddev: 164.410000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fda00000000 of size 6291456 bytes.
[1.679s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803748360.000000
Throughput: 80374836.000000
Num Accesses - Session Mean & Stddev: 8037483.600000,24097.590192
Num Phases Traversed: 16440.000000
Num Phases Traversed - Session Mean & Stddev: 164.400000,0.489898
Num Path Iterations: 16440.000000
Num Path Iterations - Session Mean & Stddev: 164.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdd40000000 of size 6291456 bytes.
[1.679s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803699171.000000
Throughput: 80369917.100000
Num Accesses - Session Mean & Stddev: 8036991.710000,23991.931598
Num Phases Traversed: 16439.000000
Num Phases Traversed - Session Mean & Stddev: 164.390000,0.487750
Num Path Iterations: 16439.000000
Num Path Iterations - Session Mean & Stddev: 164.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd8c0000000 of size 6291456 bytes.
[1.680s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803748360.000000
Throughput: 80374836.000000
Num Accesses - Session Mean & Stddev: 8037483.600000,24097.590192
Num Phases Traversed: 16440.000000
Num Phases Traversed - Session Mean & Stddev: 164.400000,0.489898
Num Path Iterations: 16440.000000
Num Path Iterations - Session Mean & Stddev: 164.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fed80000000 of size 6291456 bytes.
[1.679s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803797549.000000
Throughput: 80379754.900000
Num Accesses - Session Mean & Stddev: 8037975.490000,24192.788444
Num Phases Traversed: 16441.000000
Num Phases Traversed - Session Mean & Stddev: 164.410000,0.491833
Num Path Iterations: 16441.000000
Num Path Iterations - Session Mean & Stddev: 164.410000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5540000000 of size 6291456 bytes.
[1.679s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803699171.000000
Throughput: 80369917.100000
Num Accesses - Session Mean & Stddev: 8036991.710000,23991.931598
Num Phases Traversed: 16439.000000
Num Phases Traversed - Session Mean & Stddev: 164.390000,0.487750
Num Path Iterations: 16439.000000
Num Path Iterations - Session Mean & Stddev: 164.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb340000000 of size 6291456 bytes.
[1.679s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803748360.000000
Throughput: 80374836.000000
Num Accesses - Session Mean & Stddev: 8037483.600000,24097.590192
Num Phases Traversed: 16440.000000
Num Phases Traversed - Session Mean & Stddev: 164.400000,0.489898
Num Path Iterations: 16440.000000
Num Path Iterations - Session Mean & Stddev: 164.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9280000000 of size 6291456 bytes.
[1.679s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803649982.000000
Throughput: 80364998.200000
Num Accesses - Session Mean & Stddev: 8036499.820000,23875.673793
Num Phases Traversed: 16438.000000
Num Phases Traversed - Session Mean & Stddev: 164.380000,0.485386
Num Path Iterations: 16438.000000
Num Path Iterations - Session Mean & Stddev: 164.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6100000000 of size 6291456 bytes.
[1.679s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803649982.000000
Throughput: 80364998.200000
Num Accesses - Session Mean & Stddev: 8036499.820000,23875.673793
Num Phases Traversed: 16438.000000
Num Phases Traversed - Session Mean & Stddev: 164.380000,0.485386
Num Path Iterations: 16438.000000
Num Path Iterations - Session Mean & Stddev: 164.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe440000000 of size 6291456 bytes.
[1.679s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803699171.000000
Throughput: 80369917.100000
Num Accesses - Session Mean & Stddev: 8036991.710000,23991.931598
Num Phases Traversed: 16439.000000
Num Phases Traversed - Session Mean & Stddev: 164.390000,0.487750
Num Path Iterations: 16439.000000
Num Path Iterations - Session Mean & Stddev: 164.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f18c0000000 of size 6291456 bytes.
[1.679s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803748360.000000
Throughput: 80374836.000000
Num Accesses - Session Mean & Stddev: 8037483.600000,24097.590192
Num Phases Traversed: 16440.000000
Num Phases Traversed - Session Mean & Stddev: 164.400000,0.489898
Num Path Iterations: 16440.000000
Num Path Iterations - Session Mean & Stddev: 164.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/49152,0.1 0/49152,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/49152,0.1 0/49152,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    49152 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f24c0000000 of size 6291456 bytes.
[1.680s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 803699171.000000
Throughput: 80369917.100000
Num Accesses - Session Mean & Stddev: 8036991.710000,23991.931598
Num Phases Traversed: 16439.000000
Num Phases Traversed - Session Mean & Stddev: 164.390000,0.487750
Num Path Iterations: 16439.000000
Num Path Iterations - Session Mean & Stddev: 164.390000,0.487750
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff8f5a00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2030010326.000000
Throughput: 203001032.600000
Num Accesses - Session Mean & Stddev: 20300103.260000,328.606623
Num Phases Traversed: 2998638.000000
Num Phases Traversed - Session Mean & Stddev: 29986.380000,0.485386
Num Path Iterations: 2998638.000000
Num Path Iterations - Session Mean & Stddev: 29986.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3857e00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2032983710.000000
Throughput: 203298371.000000
Num Accesses - Session Mean & Stddev: 20329837.100000,310.240375
Num Phases Traversed: 3003030.000000
Num Phases Traversed - Session Mean & Stddev: 30030.300000,0.458258
Num Path Iterations: 3003030.000000
Num Path Iterations - Session Mean & Stddev: 30030.300000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f36bb200000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2026927268.000000
Throughput: 202692726.800000
Num Accesses - Session Mean & Stddev: 20269272.680000,248.192300
Num Phases Traversed: 2994084.000000
Num Phases Traversed - Session Mean & Stddev: 29940.840000,0.366606
Num Path Iterations: 2994084.000000
Num Path Iterations - Session Mean & Stddev: 29940.840000,0.366606
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f08d7a00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2030070579.000000
Throughput: 203007057.900000
Num Accesses - Session Mean & Stddev: 20300705.790000,300.560553
Num Phases Traversed: 2998727.000000
Num Phases Traversed - Session Mean & Stddev: 29987.270000,0.443959
Num Path Iterations: 2998727.000000
Num Path Iterations - Session Mean & Stddev: 29987.270000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2b14a00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2029541165.000000
Throughput: 202954116.500000
Num Accesses - Session Mean & Stddev: 20295411.650000,336.803247
Num Phases Traversed: 2997945.000000
Num Phases Traversed - Session Mean & Stddev: 29979.450000,0.497494
Num Path Iterations: 2997945.000000
Num Path Iterations - Session Mean & Stddev: 29979.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5187e00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2024544228.000000
Throughput: 202454422.800000
Num Accesses - Session Mean & Stddev: 20245442.280000,324.960000
Num Phases Traversed: 2990564.000000
Num Phases Traversed - Session Mean & Stddev: 29905.640000,0.480000
Num Path Iterations: 2990564.000000
Num Path Iterations - Session Mean & Stddev: 29905.640000,0.480000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f607ae00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2028923741.000000
Throughput: 202892374.100000
Num Accesses - Session Mean & Stddev: 20289237.410000,318.334010
Num Phases Traversed: 2997033.000000
Num Phases Traversed - Session Mean & Stddev: 29970.330000,0.470213
Num Path Iterations: 2997033.000000
Num Path Iterations - Session Mean & Stddev: 29970.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb2d6800000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2030500474.000000
Throughput: 203050047.400000
Num Accesses - Session Mean & Stddev: 20305004.740000,328.606623
Num Phases Traversed: 2999362.000000
Num Phases Traversed - Session Mean & Stddev: 29993.620000,0.485386
Num Path Iterations: 2999362.000000
Num Path Iterations - Session Mean & Stddev: 29993.620000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0862800000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2029905391.000000
Throughput: 202990539.100000
Num Accesses - Session Mean & Stddev: 20299053.910000,254.303405
Num Phases Traversed: 2998483.000000
Num Phases Traversed - Session Mean & Stddev: 29984.830000,0.375633
Num Path Iterations: 2998483.000000
Num Path Iterations - Session Mean & Stddev: 29984.830000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6b3e400000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2028955560.000000
Throughput: 202895556.000000
Num Accesses - Session Mean & Stddev: 20289555.600000,270.800000
Num Phases Traversed: 2997080.000000
Num Phases Traversed - Session Mean & Stddev: 29970.800000,0.400000
Num Path Iterations: 2997080.000000
Num Path Iterations - Session Mean & Stddev: 29970.800000,0.400000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6d1b600000 of size 81920 bytes.
[1.263s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2028776155.000000
Throughput: 202877615.500000
Num Accesses - Session Mean & Stddev: 20287761.550000,241.737352
Num Phases Traversed: 2996815.000000
Num Phases Traversed - Session Mean & Stddev: 29968.150000,0.357071
Num Path Iterations: 2996815.000000
Num Path Iterations - Session Mean & Stddev: 29968.150000,0.357071
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa3e6600000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2030274356.000000
Throughput: 203027435.600000
Num Accesses - Session Mean & Stddev: 20302743.560000,303.972246
Num Phases Traversed: 2999028.000000
Num Phases Traversed - Session Mean & Stddev: 29990.280000,0.448999
Num Path Iterations: 2999028.000000
Num Path Iterations - Session Mean & Stddev: 29990.280000,0.448999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fba05600000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2030620303.000000
Throughput: 203062030.300000
Num Accesses - Session Mean & Stddev: 20306203.030000,330.206707
Num Phases Traversed: 2999539.000000
Num Phases Traversed - Session Mean & Stddev: 29995.390000,0.487750
Num Path Iterations: 2999539.000000
Num Path Iterations - Session Mean & Stddev: 29995.390000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f77bd600000 of size 81920 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2030224258.000000
Throughput: 203022425.800000
Num Accesses - Session Mean & Stddev: 20302242.580000,630.882639
Num Phases Traversed: 2998954.000000
Num Phases Traversed - Session Mean & Stddev: 29989.540000,0.931880
Num Path Iterations: 2998954.000000
Num Path Iterations - Session Mean & Stddev: 29989.540000,0.931880
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc1c9600000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2031066446.000000
Throughput: 203106644.600000
Num Accesses - Session Mean & Stddev: 20310664.460000,94.780000
Num Phases Traversed: 3000198.000000
Num Phases Traversed - Session Mean & Stddev: 30001.980000,0.140000
Num Path Iterations: 3000198.000000
Num Path Iterations - Session Mean & Stddev: 30001.980000,0.140000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9418400000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2029178970.000000
Throughput: 202917897.000000
Num Accesses - Session Mean & Stddev: 20291789.700000,203.100000
Num Phases Traversed: 2997410.000000
Num Phases Traversed - Session Mean & Stddev: 29974.100000,0.300000
Num Path Iterations: 2997410.000000
Num Path Iterations - Session Mean & Stddev: 29974.100000,0.300000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f37fa800000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2031784743.000000
Throughput: 203178474.300000
Num Accesses - Session Mean & Stddev: 20317847.430000,332.971148
Num Phases Traversed: 3001259.000000
Num Phases Traversed - Session Mean & Stddev: 30012.590000,0.491833
Num Path Iterations: 3001259.000000
Num Path Iterations - Session Mean & Stddev: 30012.590000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa94d600000 of size 81920 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2028638047.000000
Throughput: 202863804.700000
Num Accesses - Session Mean & Stddev: 20286380.470000,211.826365
Num Phases Traversed: 2996611.000000
Num Phases Traversed - Session Mean & Stddev: 29966.110000,0.312890
Num Path Iterations: 2996611.000000
Num Path Iterations - Session Mean & Stddev: 29966.110000,0.312890
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcc80400000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2030267586.000000
Throughput: 203026758.600000
Num Accesses - Session Mean & Stddev: 20302675.860000,260.094907
Num Phases Traversed: 2999018.000000
Num Phases Traversed - Session Mean & Stddev: 29990.180000,0.384187
Num Path Iterations: 2999018.000000
Num Path Iterations - Session Mean & Stddev: 29990.180000,0.384187
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0 0/640,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0 0/640,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0fa3400000 of size 81920 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2028084938.000000
Throughput: 202808493.800000
Num Accesses - Session Mean & Stddev: 20280849.380000,187.126523
Num Phases Traversed: 2995794.000000
Num Phases Traversed - Session Mean & Stddev: 29957.940000,0.276405
Num Path Iterations: 2995794.000000
Num Path Iterations - Session Mean & Stddev: 29957.940000,0.276405
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff8e2400000 of size 360448 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2067158368.000000
Throughput: 206715836.800000
Num Accesses - Session Mean & Stddev: 20671583.680000,1526.821541
Num Phases Traversed: 724656.000000
Num Phases Traversed - Session Mean & Stddev: 7246.560000,0.535164
Num Path Iterations: 724656.000000
Num Path Iterations - Session Mean & Stddev: 7246.560000,0.535164
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc61ec00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2068573456.000000
Throughput: 206857345.600000
Num Accesses - Session Mean & Stddev: 20685734.560000,1425.358343
Num Phases Traversed: 725152.000000
Num Phases Traversed - Session Mean & Stddev: 7251.520000,0.499600
Num Path Iterations: 725152.000000
Num Path Iterations - Session Mean & Stddev: 7251.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f55ba200000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2066878774.000000
Throughput: 206687877.400000
Num Accesses - Session Mean & Stddev: 20668787.740000,1408.122421
Num Phases Traversed: 724558.000000
Num Phases Traversed - Session Mean & Stddev: 7245.580000,0.493559
Num Path Iterations: 724558.000000
Num Path Iterations - Session Mean & Stddev: 7245.580000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6622000000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2067192604.000000
Throughput: 206719260.400000
Num Accesses - Session Mean & Stddev: 20671926.040000,1657.688752
Num Phases Traversed: 724668.000000
Num Phases Traversed - Session Mean & Stddev: 7246.680000,0.581034
Num Path Iterations: 724668.000000
Num Path Iterations - Session Mean & Stddev: 7246.680000,0.581034
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0b00400000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2067780322.000000
Throughput: 206778032.200000
Num Accesses - Session Mean & Stddev: 20677803.220000,1693.634616
Num Phases Traversed: 724874.000000
Num Phases Traversed - Session Mean & Stddev: 7248.740000,0.593633
Num Path Iterations: 724874.000000
Num Path Iterations - Session Mean & Stddev: 7248.740000,0.593633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4929c00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2066094199.000000
Throughput: 206609419.900000
Num Accesses - Session Mean & Stddev: 20660941.990000,1937.310948
Num Phases Traversed: 724283.000000
Num Phases Traversed - Session Mean & Stddev: 7242.830000,0.679043
Num Path Iterations: 724283.000000
Num Path Iterations - Session Mean & Stddev: 7242.830000,0.679043
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f90fb000000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2066419441.000000
Throughput: 206641944.100000
Num Accesses - Session Mean & Stddev: 20664194.410000,1868.878333
Num Phases Traversed: 724397.000000
Num Phases Traversed - Session Mean & Stddev: 7243.970000,0.655057
Num Path Iterations: 724397.000000
Num Path Iterations - Session Mean & Stddev: 7243.970000,0.655057
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f833a000000 of size 360448 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2067126985.000000
Throughput: 206712698.500000
Num Accesses - Session Mean & Stddev: 20671269.850000,1529.750825
Num Phases Traversed: 724645.000000
Num Phases Traversed - Session Mean & Stddev: 7246.450000,0.536190
Num Path Iterations: 724645.000000
Num Path Iterations - Session Mean & Stddev: 7246.450000,0.536190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f034ae00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2066995747.000000
Throughput: 206699574.700000
Num Accesses - Session Mean & Stddev: 20669957.470000,1870.619659
Num Phases Traversed: 724599.000000
Num Phases Traversed - Session Mean & Stddev: 7245.990000,0.655668
Num Path Iterations: 724599.000000
Num Path Iterations - Session Mean & Stddev: 7245.990000,0.655668
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fee0de00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2067540670.000000
Throughput: 206754067.000000
Num Accesses - Session Mean & Stddev: 20675406.700000,1913.850582
Num Phases Traversed: 724790.000000
Num Phases Traversed - Session Mean & Stddev: 7247.900000,0.670820
Num Path Iterations: 724790.000000
Num Path Iterations - Session Mean & Stddev: 7247.900000,0.670820
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8aaf600000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2068088446.000000
Throughput: 206808844.600000
Num Accesses - Session Mean & Stddev: 20680884.460000,1865.608964
Num Phases Traversed: 724982.000000
Num Phases Traversed - Session Mean & Stddev: 7249.820000,0.653911
Num Path Iterations: 724982.000000
Num Path Iterations - Session Mean & Stddev: 7249.820000,0.653911
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8fa2400000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2067654790.000000
Throughput: 206765479.000000
Num Accesses - Session Mean & Stddev: 20676547.900000,1638.923723
Num Phases Traversed: 724830.000000
Num Phases Traversed - Session Mean & Stddev: 7248.300000,0.574456
Num Path Iterations: 724830.000000
Num Path Iterations - Session Mean & Stddev: 7248.300000,0.574456
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f52c1600000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2066895892.000000
Throughput: 206689589.200000
Num Accesses - Session Mean & Stddev: 20668958.920000,1483.560000
Num Phases Traversed: 724564.000000
Num Phases Traversed - Session Mean & Stddev: 7245.640000,0.520000
Num Path Iterations: 724564.000000
Num Path Iterations - Session Mean & Stddev: 7245.640000,0.520000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff38aa00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2066924422.000000
Throughput: 206692442.200000
Num Accesses - Session Mean & Stddev: 20669244.220000,1832.150308
Num Phases Traversed: 724574.000000
Num Phases Traversed - Session Mean & Stddev: 7245.740000,0.642184
Num Path Iterations: 724574.000000
Num Path Iterations - Session Mean & Stddev: 7245.740000,0.642184
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcdab800000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2066881627.000000
Throughput: 206688162.700000
Num Accesses - Session Mean & Stddev: 20668816.270000,1460.056025
Num Phases Traversed: 724559.000000
Num Phases Traversed - Session Mean & Stddev: 7245.590000,0.511762
Num Path Iterations: 724559.000000
Num Path Iterations - Session Mean & Stddev: 7245.590000,0.511762
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd7b7c00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2066256820.000000
Throughput: 206625682.000000
Num Accesses - Session Mean & Stddev: 20662568.200000,1454.750267
Num Phases Traversed: 724340.000000
Num Phases Traversed - Session Mean & Stddev: 7243.400000,0.509902
Num Path Iterations: 724340.000000
Num Path Iterations - Session Mean & Stddev: 7243.400000,0.509902
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f74de000000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2068219684.000000
Throughput: 206821968.400000
Num Accesses - Session Mean & Stddev: 20682196.840000,1566.813988
Num Phases Traversed: 725028.000000
Num Phases Traversed - Session Mean & Stddev: 7250.280000,0.549181
Num Path Iterations: 725028.000000
Num Path Iterations - Session Mean & Stddev: 7250.280000,0.549181
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb17ec00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2067369490.000000
Throughput: 206736949.000000
Num Accesses - Session Mean & Stddev: 20673694.900000,1735.412150
Num Phases Traversed: 724730.000000
Num Phases Traversed - Session Mean & Stddev: 7247.300000,0.608276
Num Path Iterations: 724730.000000
Num Path Iterations - Session Mean & Stddev: 7247.300000,0.608276
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8ba8800000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2066724712.000000
Throughput: 206672471.200000
Num Accesses - Session Mean & Stddev: 20667247.120000,1973.319271
Num Phases Traversed: 724504.000000
Num Phases Traversed - Session Mean & Stddev: 7245.040000,0.691665
Num Path Iterations: 724504.000000
Num Path Iterations - Session Mean & Stddev: 7245.040000,0.691665
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2816,0.01 0/2816,0.01' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2816,0.01 0/2816,0.01 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2816 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fce0da00000 of size 360448 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2067626260.000000
Throughput: 206762626.000000
Num Accesses - Session Mean & Stddev: 20676262.600000,1711.800000
Num Phases Traversed: 724820.000000
Num Phases Traversed - Session Mean & Stddev: 7248.200000,0.600000
Num Path Iterations: 724820.000000
Num Path Iterations - Session Mean & Stddev: 7248.200000,0.600000
+ set +x

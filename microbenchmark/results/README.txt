accesstest-XXX-YYYdl-ZZZur-output.txt
- XXX:
  - nomigrate: no migration is performed
  - migratecluster: migration is performed but work stealing across cores is not performed
- YYY: number of data lines in working set. Convert YYY to bytes by multiplying by 64.
- ZZZ: decimal update ratio

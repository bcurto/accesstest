++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe080000000 of size 7864320 bytes.
[1.716s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811865362.000000
Throughput: 81186536.200000
Num Accesses - Session Mean & Stddev: 8118653.620000,32320.769029
Num Phases Traversed: 13306.000000
Num Phases Traversed - Session Mean & Stddev: 133.060000,0.525738
Num Path Iterations: 13306.000000
Num Path Iterations - Session Mean & Stddev: 133.060000,0.525738
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9200000000 of size 7864320 bytes.
[1.747s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 811865362.000000
Throughput: 81186536.200000
Num Accesses - Session Mean & Stddev: 8118653.620000,31129.466427
Num Phases Traversed: 13306.000000
Num Phases Traversed - Session Mean & Stddev: 133.060000,0.506360
Num Path Iterations: 13306.000000
Num Path Iterations - Session Mean & Stddev: 133.060000,0.506360
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fda40000000 of size 7864320 bytes.
[1.747s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811742408.000000
Throughput: 81174240.800000
Num Accesses - Session Mean & Stddev: 8117424.080000,34689.592313
Num Phases Traversed: 13304.000000
Num Phases Traversed - Session Mean & Stddev: 133.040000,0.564269
Num Path Iterations: 13304.000000
Num Path Iterations - Session Mean & Stddev: 133.040000,0.564269
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7fc0000000 of size 7864320 bytes.
[1.748s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 811250592.000000
Throughput: 81125059.200000
Num Accesses - Session Mean & Stddev: 8112505.920000,22870.766048
Num Phases Traversed: 13296.000000
Num Phases Traversed - Session Mean & Stddev: 132.960000,0.372022
Num Path Iterations: 13296.000000
Num Path Iterations - Session Mean & Stddev: 132.960000,0.372022
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8e40000000 of size 7864320 bytes.
[1.747s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811988316.000000
Throughput: 81198831.600000
Num Accesses - Session Mean & Stddev: 8119883.160000,30959.026144
Num Phases Traversed: 13308.000000
Num Phases Traversed - Session Mean & Stddev: 133.080000,0.503587
Num Path Iterations: 13308.000000
Num Path Iterations - Session Mean & Stddev: 133.080000,0.503587
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2080000000 of size 7864320 bytes.
[1.748s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811189115.000000
Throughput: 81118911.500000
Num Accesses - Session Mean & Stddev: 8111891.150000,21951.679775
Num Phases Traversed: 13295.000000
Num Phases Traversed - Session Mean & Stddev: 132.950000,0.357071
Num Path Iterations: 13295.000000
Num Path Iterations - Session Mean & Stddev: 132.950000,0.357071
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9ac0000000 of size 7864320 bytes.
[1.748s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 811865362.000000
Throughput: 81186536.200000
Num Accesses - Session Mean & Stddev: 8118653.620000,29890.721795
Num Phases Traversed: 13306.000000
Num Phases Traversed - Session Mean & Stddev: 133.060000,0.486210
Num Path Iterations: 13306.000000
Num Path Iterations - Session Mean & Stddev: 133.060000,0.486210
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff8c0000000 of size 7864320 bytes.
[1.746s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 811680931.000000
Throughput: 81168093.100000
Num Accesses - Session Mean & Stddev: 8116809.310000,30683.120814
Num Phases Traversed: 13303.000000
Num Phases Traversed - Session Mean & Stddev: 133.030000,0.499099
Num Path Iterations: 13303.000000
Num Path Iterations - Session Mean & Stddev: 133.030000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbc00000000 of size 7864320 bytes.
[1.747s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 811865362.000000
Throughput: 81186536.200000
Num Accesses - Session Mean & Stddev: 8118653.620000,28598.370913
Num Phases Traversed: 13306.000000
Num Phases Traversed - Session Mean & Stddev: 133.060000,0.465188
Num Path Iterations: 13306.000000
Num Path Iterations - Session Mean & Stddev: 133.060000,0.465188
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe9c0000000 of size 7864320 bytes.
[1.747s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 811619454.000000
Throughput: 81161945.400000
Num Accesses - Session Mean & Stddev: 8116194.540000,35825.850321
Num Phases Traversed: 13302.000000
Num Phases Traversed - Session Mean & Stddev: 133.020000,0.582752
Num Path Iterations: 13302.000000
Num Path Iterations - Session Mean & Stddev: 133.020000,0.582752
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3200000000 of size 7864320 bytes.
[1.748s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 811803885.000000
Throughput: 81180388.500000
Num Accesses - Session Mean & Stddev: 8118038.850000,29322.660143
Num Phases Traversed: 13305.000000
Num Phases Traversed - Session Mean & Stddev: 133.050000,0.476970
Num Path Iterations: 13305.000000
Num Path Iterations - Session Mean & Stddev: 133.050000,0.476970
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1c00000000 of size 7864320 bytes.
[1.747s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811435023.000000
Throughput: 81143502.300000
Num Accesses - Session Mean & Stddev: 8114350.230000,25340.160177
Num Phases Traversed: 13299.000000
Num Phases Traversed - Session Mean & Stddev: 132.990000,0.412189
Num Path Iterations: 13299.000000
Num Path Iterations - Session Mean & Stddev: 132.990000,0.412189
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8000000000 of size 7864320 bytes.
[1.748s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 811865362.000000
Throughput: 81186536.200000
Num Accesses - Session Mean & Stddev: 8118653.620000,28598.370913
Num Phases Traversed: 13306.000000
Num Phases Traversed - Session Mean & Stddev: 133.060000,0.465188
Num Path Iterations: 13306.000000
Num Path Iterations - Session Mean & Stddev: 133.060000,0.465188
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcac0000000 of size 7864320 bytes.
[1.746s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811680931.000000
Throughput: 81168093.100000
Num Accesses - Session Mean & Stddev: 8116809.310000,34179.221679
Num Phases Traversed: 13303.000000
Num Phases Traversed - Session Mean & Stddev: 133.030000,0.555968
Num Path Iterations: 13303.000000
Num Path Iterations - Session Mean & Stddev: 133.030000,0.555968
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4bc0000000 of size 7864320 bytes.
[1.748s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 811988316.000000
Throughput: 81198831.600000
Num Accesses - Session Mean & Stddev: 8119883.160000,29713.176693
Num Phases Traversed: 13308.000000
Num Phases Traversed - Session Mean & Stddev: 133.080000,0.483322
Num Path Iterations: 13308.000000
Num Path Iterations - Session Mean & Stddev: 133.080000,0.483322
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6340000000 of size 7864320 bytes.
[1.747s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 811496500.000000
Throughput: 81149650.000000
Num Accesses - Session Mean & Stddev: 8114965.000000,34776.642870
Num Phases Traversed: 13300.000000
Num Phases Traversed - Session Mean & Stddev: 133.000000,0.565685
Num Path Iterations: 13300.000000
Num Path Iterations - Session Mean & Stddev: 133.000000,0.565685
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0500000000 of size 7864320 bytes.
[1.716s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 811803885.000000
Throughput: 81180388.500000
Num Accesses - Session Mean & Stddev: 8118038.850000,34090.645640
Num Phases Traversed: 13305.000000
Num Phases Traversed - Session Mean & Stddev: 133.050000,0.554527
Num Path Iterations: 13305.000000
Num Path Iterations - Session Mean & Stddev: 133.050000,0.554527
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0bc0000000 of size 7864320 bytes.
[1.715s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811680931.000000
Throughput: 81168093.100000
Num Accesses - Session Mean & Stddev: 8116809.310000,30683.120814
Num Phases Traversed: 13303.000000
Num Phases Traversed - Session Mean & Stddev: 133.030000,0.499099
Num Path Iterations: 13303.000000
Num Path Iterations - Session Mean & Stddev: 133.030000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0180000000 of size 7864320 bytes.
[1.748s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 811435023.000000
Throughput: 81143502.300000
Num Accesses - Session Mean & Stddev: 8114350.230000,23802.001748
Num Phases Traversed: 13299.000000
Num Phases Traversed - Session Mean & Stddev: 132.990000,0.387169
Num Path Iterations: 13299.000000
Num Path Iterations - Session Mean & Stddev: 132.990000,0.387169
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/61440,0.1 0/61440,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/61440,0.1 0/61440,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    61440 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5c80000000 of size 7864320 bytes.
[1.746s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811865362.000000
Throughput: 81186536.200000
Num Accesses - Session Mean & Stddev: 8118653.620000,29890.721795
Num Phases Traversed: 13306.000000
Num Phases Traversed - Session Mean & Stddev: 133.060000,0.486210
Num Path Iterations: 13306.000000
Num Path Iterations - Session Mean & Stddev: 133.060000,0.486210
+ set +x

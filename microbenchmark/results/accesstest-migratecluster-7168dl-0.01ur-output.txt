++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7eff9de00000 of size 458752 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086442026.000000
Throughput: 208644202.600000
Num Accesses - Session Mean & Stddev: 20864420.260000,2499.277050
Num Phases Traversed: 576306.000000
Num Phases Traversed - Session Mean & Stddev: 5763.060000,0.690217
Num Path Iterations: 576306.000000
Num Path Iterations - Session Mean & Stddev: 5763.060000,0.690217
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f67f0800000 of size 458752 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2082252529.000000
Throughput: 208225252.900000
Num Accesses - Session Mean & Stddev: 20822525.290000,1810.137864
Num Phases Traversed: 575149.000000
Num Phases Traversed - Session Mean & Stddev: 5751.490000,0.499900
Num Path Iterations: 575149.000000
Num Path Iterations - Session Mean & Stddev: 5751.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4e3c200000 of size 458752 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2086673770.000000
Throughput: 208667377.000000
Num Accesses - Session Mean & Stddev: 20866737.700000,2261.313775
Num Phases Traversed: 576370.000000
Num Phases Traversed - Session Mean & Stddev: 5763.700000,0.624500
Num Path Iterations: 576370.000000
Num Path Iterations - Session Mean & Stddev: 5763.700000,0.624500
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff242c00000 of size 458752 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2089320721.000000
Throughput: 208932072.100000
Num Accesses - Session Mean & Stddev: 20893207.210000,2534.441344
Num Phases Traversed: 577101.000000
Num Phases Traversed - Session Mean & Stddev: 5771.010000,0.699929
Num Path Iterations: 577101.000000
Num Path Iterations - Session Mean & Stddev: 5771.010000,0.699929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f762ec00000 of size 458752 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2084964658.000000
Throughput: 208496465.800000
Num Accesses - Session Mean & Stddev: 20849646.580000,2507.656879
Num Phases Traversed: 575898.000000
Num Phases Traversed - Session Mean & Stddev: 5758.980000,0.692532
Num Path Iterations: 575898.000000
Num Path Iterations - Session Mean & Stddev: 5758.980000,0.692532
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4118a00000 of size 458752 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2087789038.000000
Throughput: 208778903.800000
Num Accesses - Session Mean & Stddev: 20877890.380000,2378.861966
Num Phases Traversed: 576678.000000
Num Phases Traversed - Session Mean & Stddev: 5766.780000,0.656963
Num Path Iterations: 576678.000000
Num Path Iterations - Session Mean & Stddev: 5766.780000,0.656963
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbe80a00000 of size 458752 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2085413662.000000
Throughput: 208541366.200000
Num Accesses - Session Mean & Stddev: 20854136.620000,2378.861966
Num Phases Traversed: 576022.000000
Num Phases Traversed - Session Mean & Stddev: 5760.220000,0.656963
Num Path Iterations: 576022.000000
Num Path Iterations - Session Mean & Stddev: 5760.220000,0.656963
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb609c00000 of size 458752 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2087282098.000000
Throughput: 208728209.800000
Num Accesses - Session Mean & Stddev: 20872820.980000,1757.584314
Num Phases Traversed: 576538.000000
Num Phases Traversed - Session Mean & Stddev: 5765.380000,0.485386
Num Path Iterations: 576538.000000
Num Path Iterations - Session Mean & Stddev: 5765.380000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8fe4e00000 of size 458752 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2089519876.000000
Throughput: 208951987.600000
Num Accesses - Session Mean & Stddev: 20895198.760000,1937.827129
Num Phases Traversed: 577156.000000
Num Phases Traversed - Session Mean & Stddev: 5771.560000,0.535164
Num Path Iterations: 577156.000000
Num Path Iterations - Session Mean & Stddev: 5771.560000,0.535164
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9e4f800000 of size 458752 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2057169862.000000
Throughput: 205716986.200000
Num Accesses - Session Mean & Stddev: 20571698.620000,2378.861966
Num Phases Traversed: 568222.000000
Num Phases Traversed - Session Mean & Stddev: 5682.220000,0.656963
Num Path Iterations: 568222.000000
Num Path Iterations - Session Mean & Stddev: 5682.220000,0.656963
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0708800000 of size 458752 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2074007512.000000
Throughput: 207400751.200000
Num Accesses - Session Mean & Stddev: 20740075.120000,2294.697153
Num Phases Traversed: 572872.000000
Num Phases Traversed - Session Mean & Stddev: 5728.720000,0.633719
Num Path Iterations: 572872.000000
Num Path Iterations - Session Mean & Stddev: 5728.720000,0.633719
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd19b800000 of size 458752 bytes.
[1.248s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2078845168.000000
Throughput: 207884516.800000
Num Accesses - Session Mean & Stddev: 20788451.680000,2491.921583
Num Phases Traversed: 574208.000000
Num Phases Traversed - Session Mean & Stddev: 5742.080000,0.688186
Num Path Iterations: 574208.000000
Num Path Iterations - Session Mean & Stddev: 5742.080000,0.688186
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb916200000 of size 458752 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2064647227.000000
Throughput: 206464722.700000
Num Accesses - Session Mean & Stddev: 20646472.270000,1416.824702
Num Phases Traversed: 570287.000000
Num Phases Traversed - Session Mean & Stddev: 5702.870000,0.391280
Num Path Iterations: 570287.000000
Num Path Iterations - Session Mean & Stddev: 5702.870000,0.391280
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f73f3800000 of size 458752 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2080738951.000000
Throughput: 208073895.100000
Num Accesses - Session Mean & Stddev: 20807389.510000,2214.146014
Num Phases Traversed: 574731.000000
Num Phases Traversed - Session Mean & Stddev: 5747.310000,0.611474
Num Path Iterations: 574731.000000
Num Path Iterations - Session Mean & Stddev: 5747.310000,0.611474
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6b5b200000 of size 458752 bytes.
[1.293s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2085837319.000000
Throughput: 208583731.900000
Num Accesses - Session Mean & Stddev: 20858373.190000,2104.857120
Num Phases Traversed: 576139.000000
Num Phases Traversed - Session Mean & Stddev: 5761.390000,0.581292
Num Path Iterations: 576139.000000
Num Path Iterations - Session Mean & Stddev: 5761.390000,0.581292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f832c800000 of size 458752 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086840336.000000
Throughput: 208684033.600000
Num Accesses - Session Mean & Stddev: 20868403.360000,2093.927590
Num Phases Traversed: 576416.000000
Num Phases Traversed - Session Mean & Stddev: 5764.160000,0.578273
Num Path Iterations: 576416.000000
Num Path Iterations - Session Mean & Stddev: 5764.160000,0.578273
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f29f1200000 of size 458752 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2068814998.000000
Throughput: 206881499.800000
Num Accesses - Session Mean & Stddev: 20688149.980000,2097.681272
Num Phases Traversed: 571438.000000
Num Phases Traversed - Session Mean & Stddev: 5714.380000,0.579310
Num Path Iterations: 571438.000000
Num Path Iterations - Session Mean & Stddev: 5714.380000,0.579310
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f69fea00000 of size 458752 bytes.
[1.280s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2073739558.000000
Throughput: 207373955.800000
Num Accesses - Session Mean & Stddev: 20737395.580000,2047.066307
Num Phases Traversed: 572798.000000
Num Phases Traversed - Session Mean & Stddev: 5727.980000,0.565332
Num Path Iterations: 572798.000000
Num Path Iterations - Session Mean & Stddev: 5727.980000,0.565332
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa43ca00000 of size 458752 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2083585057.000000
Throughput: 208358505.700000
Num Accesses - Session Mean & Stddev: 20835850.570000,2458.816314
Num Phases Traversed: 575517.000000
Num Phases Traversed - Session Mean & Stddev: 5755.170000,0.679043
Num Path Iterations: 575517.000000
Num Path Iterations - Session Mean & Stddev: 5755.170000,0.679043
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/3584,0.01 0/3584,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/3584,0.01 0/3584,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    3584 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa09f800000 of size 458752 bytes.
[1.281s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2087441422.000000
Throughput: 208744142.200000
Num Accesses - Session Mean & Stddev: 20874414.220000,2422.554543
Num Phases Traversed: 576582.000000
Num Phases Traversed - Session Mean & Stddev: 5765.820000,0.669029
Num Path Iterations: 576582.000000
Num Path Iterations - Session Mean & Stddev: 5765.820000,0.669029
+ set +x

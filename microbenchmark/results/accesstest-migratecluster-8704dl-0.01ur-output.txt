++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9170600000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1142518246.000000
Throughput: 114251824.600000
Num Accesses - Session Mean & Stddev: 11425182.460000,2978.058594
Num Phases Traversed: 260414.000000
Num Phases Traversed - Session Mean & Stddev: 2604.140000,0.678528
Num Path Iterations: 260414.000000
Num Path Iterations - Session Mean & Stddev: 2604.140000,0.678528
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f34b8600000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1142443633.000000
Throughput: 114244363.300000
Num Accesses - Session Mean & Stddev: 11424436.330000,3069.477203
Num Phases Traversed: 260397.000000
Num Phases Traversed - Session Mean & Stddev: 2603.970000,0.699357
Num Path Iterations: 260397.000000
Num Path Iterations - Session Mean & Stddev: 2603.970000,0.699357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fca95000000 of size 557056 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1144493296.000000
Throughput: 114449329.600000
Num Accesses - Session Mean & Stddev: 11444932.960000,2598.050746
Num Phases Traversed: 260864.000000
Num Phases Traversed - Session Mean & Stddev: 2608.640000,0.591946
Num Path Iterations: 260864.000000
Num Path Iterations - Session Mean & Stddev: 2608.640000,0.591946
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faa1a200000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1148658457.000000
Throughput: 114865845.700000
Num Accesses - Session Mean & Stddev: 11486584.570000,3018.853618
Num Phases Traversed: 261813.000000
Num Phases Traversed - Session Mean & Stddev: 2618.130000,0.687823
Num Path Iterations: 261813.000000
Num Path Iterations - Session Mean & Stddev: 2618.130000,0.687823
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0f89000000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1138765651.000000
Throughput: 113876565.100000
Num Accesses - Session Mean & Stddev: 11387656.510000,2490.153214
Num Phases Traversed: 259559.000000
Num Phases Traversed - Session Mean & Stddev: 2595.590000,0.567362
Num Path Iterations: 259559.000000
Num Path Iterations - Session Mean & Stddev: 2595.590000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f945f200000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1145726605.000000
Throughput: 114572660.500000
Num Accesses - Session Mean & Stddev: 11457266.050000,2183.499931
Num Phases Traversed: 261145.000000
Num Phases Traversed - Session Mean & Stddev: 2611.450000,0.497494
Num Path Iterations: 261145.000000
Num Path Iterations - Session Mean & Stddev: 2611.450000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc952200000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1141785283.000000
Throughput: 114178528.300000
Num Accesses - Session Mean & Stddev: 11417852.830000,2276.787140
Num Phases Traversed: 260247.000000
Num Phases Traversed - Session Mean & Stddev: 2602.470000,0.518748
Num Path Iterations: 260247.000000
Num Path Iterations - Session Mean & Stddev: 2602.470000,0.518748
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3d3a400000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1142908867.000000
Throughput: 114290886.700000
Num Accesses - Session Mean & Stddev: 11429088.670000,748.707313
Num Phases Traversed: 260503.000000
Num Phases Traversed - Session Mean & Stddev: 2605.030000,0.170587
Num Path Iterations: 260503.000000
Num Path Iterations - Session Mean & Stddev: 2605.030000,0.170587
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efcc9e00000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1140758257.000000
Throughput: 114075825.700000
Num Accesses - Session Mean & Stddev: 11407582.570000,3018.853618
Num Phases Traversed: 260013.000000
Num Phases Traversed - Session Mean & Stddev: 2600.130000,0.687823
Num Path Iterations: 260013.000000
Num Path Iterations - Session Mean & Stddev: 2600.130000,0.687823
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6779400000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1140082351.000000
Throughput: 114008235.100000
Num Accesses - Session Mean & Stddev: 11400823.510000,2490.153214
Num Phases Traversed: 259859.000000
Num Phases Traversed - Session Mean & Stddev: 2598.590000,0.567362
Num Path Iterations: 259859.000000
Num Path Iterations - Session Mean & Stddev: 2598.590000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc130800000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1147363702.000000
Throughput: 114736370.200000
Num Accesses - Session Mean & Stddev: 11473637.020000,2936.368928
Num Phases Traversed: 261518.000000
Num Phases Traversed - Session Mean & Stddev: 2615.180000,0.669029
Num Path Iterations: 261518.000000
Num Path Iterations - Session Mean & Stddev: 2615.180000,0.669029
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f67ac600000 of size 557056 bytes.
[1.283s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1143009814.000000
Throughput: 114300981.400000
Num Accesses - Session Mean & Stddev: 11430098.140000,2818.544585
Num Phases Traversed: 260526.000000
Num Phases Traversed - Session Mean & Stddev: 2605.260000,0.642184
Num Path Iterations: 260526.000000
Num Path Iterations - Session Mean & Stddev: 2605.260000,0.642184
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7d24800000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1136641375.000000
Throughput: 113664137.500000
Num Accesses - Session Mean & Stddev: 11366413.750000,2869.681119
Num Phases Traversed: 259075.000000
Num Phases Traversed - Session Mean & Stddev: 2590.750000,0.653835
Num Path Iterations: 259075.000000
Num Path Iterations - Session Mean & Stddev: 2590.750000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f23aa400000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1125545983.000000
Throughput: 112554598.300000
Num Accesses - Session Mean & Stddev: 11255459.830000,2276.787140
Num Phases Traversed: 256547.000000
Num Phases Traversed - Session Mean & Stddev: 2565.470000,0.518748
Num Path Iterations: 256547.000000
Num Path Iterations - Session Mean & Stddev: 2565.470000,0.518748
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f58dca00000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1144330903.000000
Throughput: 114433090.300000
Num Accesses - Session Mean & Stddev: 11443309.030000,2834.560140
Num Phases Traversed: 260827.000000
Num Phases Traversed - Session Mean & Stddev: 2608.270000,0.645833
Num Path Iterations: 260827.000000
Num Path Iterations - Session Mean & Stddev: 2608.270000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3a14a00000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1142294407.000000
Throughput: 114229440.700000
Num Accesses - Session Mean & Stddev: 11422944.070000,2608.041151
Num Phases Traversed: 260363.000000
Num Phases Traversed - Session Mean & Stddev: 2603.630000,0.594222
Num Path Iterations: 260363.000000
Num Path Iterations - Session Mean & Stddev: 2603.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fde95a00000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1125861991.000000
Throughput: 112586199.100000
Num Accesses - Session Mean & Stddev: 11258619.910000,2956.961515
Num Phases Traversed: 256619.000000
Num Phases Traversed - Session Mean & Stddev: 2566.190000,0.673721
Num Path Iterations: 256619.000000
Num Path Iterations - Session Mean & Stddev: 2566.190000,0.673721
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6095000000 of size 557056 bytes.
[1.284s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1145472043.000000
Throughput: 114547204.300000
Num Accesses - Session Mean & Stddev: 11454720.430000,3018.853618
Num Phases Traversed: 261087.000000
Num Phases Traversed - Session Mean & Stddev: 2610.870000,0.687823
Num Path Iterations: 261087.000000
Num Path Iterations - Session Mean & Stddev: 2610.870000,0.687823
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f618ae00000 of size 557056 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1145072644.000000
Throughput: 114507264.400000
Num Accesses - Session Mean & Stddev: 11450726.440000,3035.716187
Num Phases Traversed: 260996.000000
Num Phases Traversed - Session Mean & Stddev: 2609.960000,0.691665
Num Path Iterations: 260996.000000
Num Path Iterations - Session Mean & Stddev: 2609.960000,0.691665
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/4352,0.01 0/4352,0.01' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/4352,0.01 0/4352,0.01 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    4352 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff854800000 of size 557056 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1138993879.000000
Throughput: 113899387.900000
Num Accesses - Session Mean & Stddev: 11389938.790000,3158.555674
Num Phases Traversed: 259611.000000
Num Phases Traversed - Session Mean & Stddev: 2596.110000,0.719653
Num Path Iterations: 259611.000000
Num Path Iterations - Session Mean & Stddev: 2596.110000,0.719653
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8f8a400000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805032125.000000
Throughput: 80503212.500000
Num Accesses - Session Mean & Stddev: 8050321.250000,4629.674253
Num Phases Traversed: 65417.000000
Num Phases Traversed - Session Mean & Stddev: 654.170000,0.375633
Num Path Iterations: 65417.000000
Num Path Iterations - Session Mean & Stddev: 654.170000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f98e5e00000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805106075.000000
Throughput: 80510607.500000
Num Accesses - Session Mean & Stddev: 8051060.750000,6940.408755
Num Phases Traversed: 65423.000000
Num Phases Traversed - Session Mean & Stddev: 654.230000,0.563116
Num Path Iterations: 65423.000000
Num Path Iterations - Session Mean & Stddev: 654.230000,0.563116
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f04f1800000 of size 1572864 bytes.
[1.331s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804871900.000000
Throughput: 80487190.000000
Num Accesses - Session Mean & Stddev: 8048719.000000,3451.000000
Num Phases Traversed: 65404.000000
Num Phases Traversed - Session Mean & Stddev: 654.040000,0.280000
Num Path Iterations: 65404.000000
Num Path Iterations - Session Mean & Stddev: 654.040000,0.280000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd049400000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805044450.000000
Throughput: 80504445.000000
Num Accesses - Session Mean & Stddev: 8050444.500000,5045.729159
Num Phases Traversed: 65418.000000
Num Phases Traversed - Session Mean & Stddev: 654.180000,0.409390
Num Path Iterations: 65418.000000
Num Path Iterations - Session Mean & Stddev: 654.180000,0.409390
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2685a00000 of size 1572864 bytes.
[1.331s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805217000.000000
Throughput: 80521700.000000
Num Accesses - Session Mean & Stddev: 8052170.000000,7573.609707
Num Phases Traversed: 65432.000000
Num Phases Traversed - Session Mean & Stddev: 654.320000,0.614492
Num Path Iterations: 65432.000000
Num Path Iterations - Session Mean & Stddev: 654.320000,0.614492
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3a96600000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804921200.000000
Throughput: 80492120.000000
Num Accesses - Session Mean & Stddev: 8049212.000000,4830.393773
Num Phases Traversed: 65408.000000
Num Phases Traversed - Session Mean & Stddev: 654.080000,0.391918
Num Path Iterations: 65408.000000
Num Path Iterations - Session Mean & Stddev: 654.080000,0.391918
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f79b2a00000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804822600.000000
Throughput: 80482260.000000
Num Accesses - Session Mean & Stddev: 8048226.000000,0.000000
Num Phases Traversed: 65400.000000
Num Phases Traversed - Session Mean & Stddev: 654.000000,0.000000
Num Path Iterations: 65400.000000
Num Path Iterations - Session Mean & Stddev: 654.000000,0.000000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4893800000 of size 1572864 bytes.
[1.300s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805315600.000000
Throughput: 80531560.000000
Num Accesses - Session Mean & Stddev: 8053156.000000,6972.072862
Num Phases Traversed: 65440.000000
Num Phases Traversed - Session Mean & Stddev: 654.400000,0.565685
Num Path Iterations: 65440.000000
Num Path Iterations - Session Mean & Stddev: 654.400000,0.565685
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f04e9000000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804773300.000000
Throughput: 80477330.000000
Num Accesses - Session Mean & Stddev: 8047733.000000,3866.201430
Num Phases Traversed: 65396.000000
Num Phases Traversed - Session Mean & Stddev: 653.960000,0.313688
Num Path Iterations: 65396.000000
Num Path Iterations - Session Mean & Stddev: 653.960000,0.313688
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f41bb400000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805377225.000000
Throughput: 80537722.500000
Num Accesses - Session Mean & Stddev: 8053772.250000,6374.539567
Num Phases Traversed: 65445.000000
Num Phases Traversed - Session Mean & Stddev: 654.450000,0.517204
Num Path Iterations: 65445.000000
Num Path Iterations - Session Mean & Stddev: 654.450000,0.517204
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff2c7000000 of size 1572864 bytes.
[1.331s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805562100.000000
Throughput: 80556210.000000
Num Accesses - Session Mean & Stddev: 8055621.000000,6972.072862
Num Phases Traversed: 65460.000000
Num Phases Traversed - Session Mean & Stddev: 654.600000,0.565685
Num Path Iterations: 65460.000000
Num Path Iterations - Session Mean & Stddev: 654.600000,0.565685
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4861600000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804810275.000000
Throughput: 80481027.500000
Num Accesses - Session Mean & Stddev: 8048102.750000,2131.191729
Num Phases Traversed: 65399.000000
Num Phases Traversed - Session Mean & Stddev: 653.990000,0.172916
Num Path Iterations: 65399.000000
Num Path Iterations - Session Mean & Stddev: 653.990000,0.172916
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff898400000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804613075.000000
Throughput: 80461307.500000
Num Accesses - Session Mean & Stddev: 8046130.750000,4629.674253
Num Phases Traversed: 65383.000000
Num Phases Traversed - Session Mean & Stddev: 653.830000,0.375633
Num Path Iterations: 65383.000000
Num Path Iterations - Session Mean & Stddev: 653.830000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8b99400000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804760975.000000
Throughput: 80476097.500000
Num Accesses - Session Mean & Stddev: 8047609.750000,2686.171474
Num Phases Traversed: 65395.000000
Num Phases Traversed - Session Mean & Stddev: 653.950000,0.217945
Num Path Iterations: 65395.000000
Num Path Iterations - Session Mean & Stddev: 653.950000,0.217945
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1ebf200000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 804908875.000000
Throughput: 80490887.500000
Num Accesses - Session Mean & Stddev: 8049088.750000,4359.288209
Num Phases Traversed: 65407.000000
Num Phases Traversed - Session Mean & Stddev: 654.070000,0.353695
Num Path Iterations: 65407.000000
Num Path Iterations - Session Mean & Stddev: 654.070000,0.353695
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff740400000 of size 1572864 bytes.
[1.340s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805500475.000000
Throughput: 80550047.500000
Num Accesses - Session Mean & Stddev: 8055004.750000,6608.545013
Num Phases Traversed: 65455.000000
Num Phases Traversed - Session Mean & Stddev: 654.550000,0.536190
Num Path Iterations: 65455.000000
Num Path Iterations - Session Mean & Stddev: 654.550000,0.536190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0d01a00000 of size 1572864 bytes.
[1.331s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805389550.000000
Throughput: 80538955.000000
Num Accesses - Session Mean & Stddev: 8053895.500000,6385.254087
Num Phases Traversed: 65446.000000
Num Phases Traversed - Session Mean & Stddev: 654.460000,0.518073
Num Path Iterations: 65446.000000
Num Path Iterations - Session Mean & Stddev: 654.460000,0.518073
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd37aa00000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805401875.000000
Throughput: 80540187.500000
Num Accesses - Session Mean & Stddev: 8054018.750000,6151.397499
Num Phases Traversed: 65447.000000
Num Phases Traversed - Session Mean & Stddev: 654.470000,0.499099
Num Path Iterations: 65447.000000
Num Path Iterations - Session Mean & Stddev: 654.470000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3ab4c00000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 805451175.000000
Throughput: 80545117.500000
Num Accesses - Session Mean & Stddev: 8054511.750000,6161.267377
Num Phases Traversed: 65451.000000
Num Phases Traversed - Session Mean & Stddev: 654.510000,0.499900
Num Path Iterations: 65451.000000
Num Path Iterations - Session Mean & Stddev: 654.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/12288,0.1 0/12288,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/12288,0.1 0/12288,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    12288 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0f6e600000 of size 1572864 bytes.
[1.332s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 804871900.000000
Throughput: 80487190.000000
Num Accesses - Session Mean & Stddev: 8048719.000000,3866.201430
Num Phases Traversed: 65404.000000
Num Phases Traversed - Session Mean & Stddev: 654.040000,0.313688
Num Path Iterations: 65404.000000
Num Path Iterations - Session Mean & Stddev: 654.040000,0.313688
+ set +x

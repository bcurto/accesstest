++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3040000000 of size 17301504 bytes.
[3.400s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 332469195.000000
Throughput: 33246919.500000
Num Accesses - Session Mean & Stddev: 3324691.950000,76710.222219
Num Phases Traversed: 2559.000000
Num Phases Traversed - Session Mean & Stddev: 25.590000,0.567362
Num Path Iterations: 2559.000000
Num Path Iterations - Session Mean & Stddev: 25.590000,0.567362
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f50c0000000 of size 17301504 bytes.
[3.340s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 324086485.000000
Throughput: 32408648.500000
Num Accesses - Session Mean & Stddev: 3240864.850000,94556.542552
Num Phases Traversed: 2497.000000
Num Phases Traversed - Session Mean & Stddev: 24.970000,0.699357
Num Path Iterations: 2497.000000
Num Path Iterations - Session Mean & Stddev: 24.970000,0.699357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6dc0000000 of size 17301504 bytes.
[3.344s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 314892545.000000
Throughput: 31489254.500000
Num Accesses - Session Mean & Stddev: 3148925.450000,86139.486433
Num Phases Traversed: 2429.000000
Num Phases Traversed - Session Mean & Stddev: 24.290000,0.637103
Num Path Iterations: 2429.000000
Num Path Iterations - Session Mean & Stddev: 24.290000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5d80000000 of size 17301504 bytes.
[3.351s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 314081315.000000
Throughput: 31408131.500000
Num Accesses - Session Mean & Stddev: 3140813.150000,89388.810005
Num Phases Traversed: 2423.000000
Num Phases Traversed - Session Mean & Stddev: 24.230000,0.661135
Num Path Iterations: 2423.000000
Num Path Iterations - Session Mean & Stddev: 24.230000,0.661135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffb80000000 of size 17301504 bytes.
[3.361s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 332198785.000000
Throughput: 33219878.500000
Num Accesses - Session Mean & Stddev: 3321987.850000,74681.641699
Num Phases Traversed: 2557.000000
Num Phases Traversed - Session Mean & Stddev: 25.570000,0.552359
Num Path Iterations: 2557.000000
Num Path Iterations - Session Mean & Stddev: 25.570000,0.552359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6380000000 of size 17301504 bytes.
[3.340s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 326790585.000000
Throughput: 32679058.500000
Num Accesses - Session Mean & Stddev: 3267905.850000,57537.631127
Num Phases Traversed: 2517.000000
Num Phases Traversed - Session Mean & Stddev: 25.170000,0.425558
Num Path Iterations: 2517.000000
Num Path Iterations - Session Mean & Stddev: 25.170000,0.425558
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f55c0000000 of size 17301504 bytes.
[3.341s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 333010015.000000
Throughput: 33301001.500000
Num Accesses - Session Mean & Stddev: 3330100.150000,80341.809937
Num Phases Traversed: 2563.000000
Num Phases Traversed - Session Mean & Stddev: 25.630000,0.594222
Num Path Iterations: 2563.000000
Num Path Iterations - Session Mean & Stddev: 25.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f55c0000000 of size 17301504 bytes.
[3.392s] Test is ready to start.
Test duration in range [10.000, 10.005] seconds
Num Accesses: 333010015.000000
Throughput: 33301001.500000
Num Accesses - Session Mean & Stddev: 3330100.150000,80341.809937
Num Phases Traversed: 2563.000000
Num Phases Traversed - Session Mean & Stddev: 25.630000,0.594222
Num Path Iterations: 2563.000000
Num Path Iterations - Session Mean & Stddev: 25.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe900000000 of size 17301504 bytes.
[3.384s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 319489515.000000
Throughput: 31948951.500000
Num Accesses - Session Mean & Stddev: 3194895.150000,80341.809937
Num Phases Traversed: 2463.000000
Num Phases Traversed - Session Mean & Stddev: 24.630000,0.594222
Num Path Iterations: 2463.000000
Num Path Iterations - Session Mean & Stddev: 24.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f70c0000000 of size 17301504 bytes.
[3.349s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 328683455.000000
Throughput: 32868345.500000
Num Accesses - Session Mean & Stddev: 3286834.550000,62531.398946
Num Phases Traversed: 2531.000000
Num Phases Traversed - Session Mean & Stddev: 25.310000,0.462493
Num Path Iterations: 2531.000000
Num Path Iterations - Session Mean & Stddev: 25.310000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0280000000 of size 17301504 bytes.
[3.338s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 332874810.000000
Throughput: 33287481.000000
Num Accesses - Session Mean & Stddev: 3328748.100000,65626.674158
Num Phases Traversed: 2562.000000
Num Phases Traversed - Session Mean & Stddev: 25.620000,0.485386
Num Path Iterations: 2562.000000
Num Path Iterations - Session Mean & Stddev: 25.620000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8c80000000 of size 17301504 bytes.
[3.342s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 331387555.000000
Throughput: 33138755.500000
Num Accesses - Session Mean & Stddev: 3313875.550000,67588.978148
Num Phases Traversed: 2551.000000
Num Phases Traversed - Session Mean & Stddev: 25.510000,0.499900
Num Path Iterations: 2551.000000
Num Path Iterations - Session Mean & Stddev: 25.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa8c0000000 of size 17301504 bytes.
[3.350s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 333010015.000000
Throughput: 33301001.500000
Num Accesses - Session Mean & Stddev: 3330100.150000,80341.809937
Num Phases Traversed: 2563.000000
Num Phases Traversed - Session Mean & Stddev: 25.630000,0.594222
Num Path Iterations: 2563.000000
Num Path Iterations - Session Mean & Stddev: 25.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd100000000 of size 17301504 bytes.
[3.338s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 315433365.000000
Throughput: 31543336.500000
Num Accesses - Session Mean & Stddev: 3154333.650000,83466.504663
Num Phases Traversed: 2433.000000
Num Phases Traversed - Session Mean & Stddev: 24.330000,0.617333
Num Path Iterations: 2433.000000
Num Path Iterations - Session Mean & Stddev: 24.330000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe300000000 of size 17301504 bytes.
[3.337s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 331928375.000000
Throughput: 33192837.500000
Num Accesses - Session Mean & Stddev: 3319283.750000,72495.604744
Num Phases Traversed: 2555.000000
Num Phases Traversed - Session Mean & Stddev: 25.550000,0.536190
Num Path Iterations: 2555.000000
Num Path Iterations - Session Mean & Stddev: 25.550000,0.536190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5fc0000000 of size 17301504 bytes.
[3.354s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 315162955.000000
Throughput: 31516295.500000
Num Accesses - Session Mean & Stddev: 3151629.550000,84856.622715
Num Phases Traversed: 2431.000000
Num Phases Traversed - Session Mean & Stddev: 24.310000,0.627615
Num Path Iterations: 2431.000000
Num Path Iterations - Session Mean & Stddev: 24.310000,0.627615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3cc0000000 of size 17301504 bytes.
[3.327s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 304076145.000000
Throughput: 30407614.500000
Num Accesses - Session Mean & Stddev: 3040761.450000,67588.978148
Num Phases Traversed: 2349.000000
Num Phases Traversed - Session Mean & Stddev: 23.490000,0.499900
Num Path Iterations: 2349.000000
Num Path Iterations - Session Mean & Stddev: 23.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd8c0000000 of size 17301504 bytes.
[3.346s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 341933545.000000
Throughput: 34193354.500000
Num Accesses - Session Mean & Stddev: 3419335.450000,86139.486433
Num Phases Traversed: 2629.000000
Num Phases Traversed - Session Mean & Stddev: 26.290000,0.637103
Num Path Iterations: 2629.000000
Num Path Iterations - Session Mean & Stddev: 26.290000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5f00000000 of size 17301504 bytes.
[3.412s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 333010015.000000
Throughput: 33301001.500000
Num Accesses - Session Mean & Stddev: 3330100.150000,80341.809937
Num Phases Traversed: 2563.000000
Num Phases Traversed - Session Mean & Stddev: 25.630000,0.594222
Num Path Iterations: 2563.000000
Num Path Iterations - Session Mean & Stddev: 25.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdf80000000 of size 17301504 bytes.
[3.349s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 315162955.000000
Throughput: 31516295.500000
Num Accesses - Session Mean & Stddev: 3151629.550000,84856.622715
Num Phases Traversed: 2431.000000
Num Phases Traversed - Session Mean & Stddev: 24.310000,0.627615
Num Path Iterations: 2431.000000
Num Path Iterations - Session Mean & Stddev: 24.310000,0.627615
+ set +x

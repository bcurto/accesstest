++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efe00000000 of size 17301504 bytes.
[3.402s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,145092.037879
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.073126
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.073126
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2e00000000 of size 17301504 bytes.
[3.343s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 254726320.000000
Throughput: 25472632.000000
Num Accesses - Session Mean & Stddev: 2547263.200000,142729.355329
Num Phases Traversed: 1984.000000
Num Phases Traversed - Session Mean & Stddev: 19.840000,1.055651
Num Path Iterations: 1984.000000
Num Path Iterations - Session Mean & Stddev: 19.840000,1.055651
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efe40000000 of size 17301504 bytes.
[3.358s] Test is ready to start.
Test duration in range [10.000, 10.008] seconds
Num Accesses: 253644680.000000
Throughput: 25364468.000000
Num Accesses - Session Mean & Stddev: 2536446.800000,140665.192311
Num Phases Traversed: 1976.000000
Num Phases Traversed - Session Mean & Stddev: 19.760000,1.040385
Num Path Iterations: 1976.000000
Num Path Iterations - Session Mean & Stddev: 19.760000,1.040385
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb900000000 of size 17301504 bytes.
[3.401s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 253644680.000000
Throughput: 25364468.000000
Num Accesses - Session Mean & Stddev: 2536446.800000,141958.811521
Num Phases Traversed: 1976.000000
Num Phases Traversed - Session Mean & Stddev: 19.760000,1.049952
Num Path Iterations: 1976.000000
Num Path Iterations - Session Mean & Stddev: 19.760000,1.049952
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcfc0000000 of size 17301504 bytes.
[3.389s] Test is ready to start.
Test duration in range [10.000, 10.009] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,138649.414905
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.025475
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.025475
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f15c0000000 of size 17301504 bytes.
[3.396s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 254455910.000000
Throughput: 25445591.000000
Num Accesses - Session Mean & Stddev: 2544559.100000,147342.436416
Num Phases Traversed: 1982.000000
Num Phases Traversed - Session Mean & Stddev: 19.820000,1.089771
Num Path Iterations: 1982.000000
Num Path Iterations - Session Mean & Stddev: 19.820000,1.089771
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5bc0000000 of size 17301504 bytes.
[3.407s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 253644680.000000
Throughput: 25364468.000000
Num Accesses - Session Mean & Stddev: 2536446.800000,143240.748423
Num Phases Traversed: 1976.000000
Num Phases Traversed - Session Mean & Stddev: 19.760000,1.059434
Num Path Iterations: 1976.000000
Num Path Iterations - Session Mean & Stddev: 19.760000,1.059434
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9a40000000 of size 17301504 bytes.
[3.338s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,147590.362616
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.091604
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.091604
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9a00000000 of size 17301504 bytes.
[3.395s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,146346.531549
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.082405
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.082405
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fed40000000 of size 17301504 bytes.
[3.404s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,142549.934321
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.054324
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.054324
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7780000000 of size 17301504 bytes.
[3.399s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 254455910.000000
Throughput: 25445591.000000
Num Accesses - Session Mean & Stddev: 2544559.100000,144839.835294
Num Phases Traversed: 1982.000000
Num Phases Traversed - Session Mean & Stddev: 19.820000,1.071261
Num Path Iterations: 1982.000000
Num Path Iterations - Session Mean & Stddev: 19.820000,1.071261
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc9c0000000 of size 17301504 bytes.
[3.398s] Test is ready to start.
Test duration in range [10.000, 10.007] seconds
Num Accesses: 254185500.000000
Throughput: 25418550.000000
Num Accesses - Session Mean & Stddev: 2541855.000000,154157.418351
Num Phases Traversed: 1980.000000
Num Phases Traversed - Session Mean & Stddev: 19.800000,1.140175
Num Path Iterations: 1980.000000
Num Path Iterations - Session Mean & Stddev: 19.800000,1.140175
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6a80000000 of size 17301504 bytes.
[3.396s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 255267140.000000
Throughput: 25526714.000000
Num Accesses - Session Mean & Stddev: 2552671.400000,140872.969665
Num Phases Traversed: 1988.000000
Num Phases Traversed - Session Mean & Stddev: 19.880000,1.041921
Num Path Iterations: 1988.000000
Num Path Iterations - Session Mean & Stddev: 19.880000,1.041921
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f17c0000000 of size 17301504 bytes.
[3.398s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,154843.670132
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.145251
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.145251
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f23c0000000 of size 17301504 bytes.
[3.388s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 254185500.000000
Throughput: 25418550.000000
Num Accesses - Session Mean & Stddev: 2541855.000000,135205.000000
Num Phases Traversed: 1980.000000
Num Phases Traversed - Session Mean & Stddev: 19.800000,1.000000
Num Path Iterations: 1980.000000
Num Path Iterations - Session Mean & Stddev: 19.800000,1.000000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd2c0000000 of size 17301504 bytes.
[3.402s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 253374270.000000
Throughput: 25337427.000000
Num Accesses - Session Mean & Stddev: 2533742.700000,147639.897896
Num Phases Traversed: 1974.000000
Num Phases Traversed - Session Mean & Stddev: 19.740000,1.091971
Num Path Iterations: 1974.000000
Num Path Iterations - Session Mean & Stddev: 19.740000,1.091971
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff440000000 of size 17301504 bytes.
[3.388s] Test is ready to start.
Test duration in range [10.000, 10.011] seconds
Num Accesses: 253644680.000000
Throughput: 25364468.000000
Num Accesses - Session Mean & Stddev: 2536446.800000,148257.692451
Num Phases Traversed: 1976.000000
Num Phases Traversed - Session Mean & Stddev: 19.760000,1.096540
Num Path Iterations: 1976.000000
Num Path Iterations - Session Mean & Stddev: 19.760000,1.096540
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f00c0000000 of size 17301504 bytes.
[3.392s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 253644680.000000
Throughput: 25364468.000000
Num Accesses - Session Mean & Stddev: 2536446.800000,143240.748423
Num Phases Traversed: 1976.000000
Num Phases Traversed - Session Mean & Stddev: 19.760000,1.059434
Num Path Iterations: 1976.000000
Num Path Iterations - Session Mean & Stddev: 19.760000,1.059434
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2740000000 of size 17301504 bytes.
[3.383s] Test is ready to start.
Test duration in range [10.000, 10.006] seconds
Num Accesses: 254185500.000000
Throughput: 25418550.000000
Num Accesses - Session Mean & Stddev: 2541855.000000,143087.522405
Num Phases Traversed: 1980.000000
Num Phases Traversed - Session Mean & Stddev: 19.800000,1.058301
Num Path Iterations: 1980.000000
Num Path Iterations - Session Mean & Stddev: 19.800000,1.058301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/135168,0.001 0/135168,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/135168,0.001 0/135168,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    135168 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4a00000000 of size 17301504 bytes.
[3.395s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 253915090.000000
Throughput: 25391509.000000
Num Accesses - Session Mean & Stddev: 2539150.900000,138649.414905
Num Phases Traversed: 1978.000000
Num Phases Traversed - Session Mean & Stddev: 19.780000,1.025475
Num Path Iterations: 1978.000000
Num Path Iterations - Session Mean & Stddev: 19.780000,1.025475
+ set +x

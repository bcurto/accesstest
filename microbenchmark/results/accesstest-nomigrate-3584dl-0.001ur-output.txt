++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa75f800000 of size 229376 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1915579473.000000
Throughput: 191557947.300000
Num Accesses - Session Mean & Stddev: 19155794.730000,1310.130969
Num Phases Traversed: 1047437.000000
Num Phases Traversed - Session Mean & Stddev: 10474.370000,0.716310
Num Path Iterations: 1047437.000000
Num Path Iterations - Session Mean & Stddev: 10474.370000,0.716310
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8d98400000 of size 229376 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1918449174.000000
Throughput: 191844917.400000
Num Accesses - Session Mean & Stddev: 19184491.740000,7641.718720
Num Phases Traversed: 1049006.000000
Num Phases Traversed - Session Mean & Stddev: 10490.060000,4.178086
Num Path Iterations: 1049006.000000
Num Path Iterations - Session Mean & Stddev: 10490.060000,4.178086
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7db9400000 of size 229376 bytes.
[1.251s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1916051355.000000
Throughput: 191605135.500000
Num Accesses - Session Mean & Stddev: 19160513.550000,2629.883223
Num Phases Traversed: 1047695.000000
Num Phases Traversed - Session Mean & Stddev: 10476.950000,1.437880
Num Path Iterations: 1047695.000000
Num Path Iterations - Session Mean & Stddev: 10476.950000,1.437880
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f72ea200000 of size 229376 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1915235621.000000
Throughput: 191523562.100000
Num Accesses - Session Mean & Stddev: 19152356.210000,2363.518129
Num Phases Traversed: 1047249.000000
Num Phases Traversed - Session Mean & Stddev: 10472.490000,1.292246
Num Path Iterations: 1047249.000000
Num Path Iterations - Session Mean & Stddev: 10472.490000,1.292246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1942c00000 of size 229376 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1916942078.000000
Throughput: 191694207.800000
Num Accesses - Session Mean & Stddev: 19169420.780000,1959.336258
Num Phases Traversed: 1048182.000000
Num Phases Traversed - Session Mean & Stddev: 10481.820000,1.071261
Num Path Iterations: 1048182.000000
Num Path Iterations - Session Mean & Stddev: 10481.820000,1.071261
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7985800000 of size 229376 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1928574518.000000
Throughput: 192857451.800000
Num Accesses - Session Mean & Stddev: 19285745.180000,72747.393801
Num Phases Traversed: 1054542.000000
Num Phases Traversed - Session Mean & Stddev: 10545.420000,39.774409
Num Path Iterations: 1054542.000000
Num Path Iterations - Session Mean & Stddev: 10545.420000,39.774409
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7eaa000000 of size 229376 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1917049989.000000
Throughput: 191704998.900000
Num Accesses - Session Mean & Stddev: 19170499.890000,1736.972325
Num Phases Traversed: 1048241.000000
Num Phases Traversed - Session Mean & Stddev: 10482.410000,0.949684
Num Path Iterations: 1048241.000000
Num Path Iterations - Session Mean & Stddev: 10482.410000,0.949684
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f280ca00000 of size 229376 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1912470173.000000
Throughput: 191247017.300000
Num Accesses - Session Mean & Stddev: 19124701.730000,2047.418252
Num Phases Traversed: 1045737.000000
Num Phases Traversed - Session Mean & Stddev: 10457.370000,1.119419
Num Path Iterations: 1045737.000000
Num Path Iterations - Session Mean & Stddev: 10457.370000,1.119419
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3497e00000 of size 229376 bytes.
[1.267s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1915930641.000000
Throughput: 191593064.100000
Num Accesses - Session Mean & Stddev: 19159306.410000,2288.736088
Num Phases Traversed: 1047629.000000
Num Phases Traversed - Session Mean & Stddev: 10476.290000,1.251359
Num Path Iterations: 1047629.000000
Num Path Iterations - Session Mean & Stddev: 10476.290000,1.251359
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1532400000 of size 229376 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1918480267.000000
Throughput: 191848026.700000
Num Accesses - Session Mean & Stddev: 19184802.670000,1505.012519
Num Phases Traversed: 1049023.000000
Num Phases Traversed - Session Mean & Stddev: 10490.230000,0.822861
Num Path Iterations: 1049023.000000
Num Path Iterations - Session Mean & Stddev: 10490.230000,0.822861
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f50bd600000 of size 229376 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1917174361.000000
Throughput: 191717436.100000
Num Accesses - Session Mean & Stddev: 19171743.610000,6066.630610
Num Phases Traversed: 1048309.000000
Num Phases Traversed - Session Mean & Stddev: 10483.090000,3.316911
Num Path Iterations: 1048309.000000
Num Path Iterations - Session Mean & Stddev: 10483.090000,3.316911
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f75ea400000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1916953052.000000
Throughput: 191695305.200000
Num Accesses - Session Mean & Stddev: 19169530.520000,1905.674062
Num Phases Traversed: 1048188.000000
Num Phases Traversed - Session Mean & Stddev: 10481.880000,1.041921
Num Path Iterations: 1048188.000000
Num Path Iterations - Session Mean & Stddev: 10481.880000,1.041921
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f40d2600000 of size 229376 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1921666385.000000
Throughput: 192166638.500000
Num Accesses - Session Mean & Stddev: 19216663.850000,1993.106542
Num Phases Traversed: 1050765.000000
Num Phases Traversed - Session Mean & Stddev: 10507.650000,1.089725
Num Path Iterations: 1050765.000000
Num Path Iterations - Session Mean & Stddev: 10507.650000,1.089725
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f915f800000 of size 229376 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1919985534.000000
Throughput: 191998553.400000
Num Accesses - Session Mean & Stddev: 19199855.340000,3161.790348
Num Phases Traversed: 1049846.000000
Num Phases Traversed - Session Mean & Stddev: 10498.460000,1.728699
Num Path Iterations: 1049846.000000
Num Path Iterations - Session Mean & Stddev: 10498.460000,1.728699
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7eff6cc00000 of size 229376 bytes.
[1.263s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1915723964.000000
Throughput: 191572396.400000
Num Accesses - Session Mean & Stddev: 19157239.640000,3013.357292
Num Phases Traversed: 1047516.000000
Num Phases Traversed - Session Mean & Stddev: 10475.160000,1.647544
Num Path Iterations: 1047516.000000
Num Path Iterations - Session Mean & Stddev: 10475.160000,1.647544
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3671e00000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1917968147.000000
Throughput: 191796814.700000
Num Accesses - Session Mean & Stddev: 19179681.470000,2073.395575
Num Phases Traversed: 1048743.000000
Num Phases Traversed - Session Mean & Stddev: 10487.430000,1.133623
Num Path Iterations: 1048743.000000
Num Path Iterations - Session Mean & Stddev: 10487.430000,1.133623
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd36b400000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1917631611.000000
Throughput: 191763161.100000
Num Accesses - Session Mean & Stddev: 19176316.110000,2086.904228
Num Phases Traversed: 1048559.000000
Num Phases Traversed - Session Mean & Stddev: 10485.590000,1.141008
Num Path Iterations: 1048559.000000
Num Path Iterations - Session Mean & Stddev: 10485.590000,1.141008
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdebc400000 of size 229376 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1919539258.000000
Throughput: 191953925.800000
Num Accesses - Session Mean & Stddev: 19195392.580000,2982.784431
Num Phases Traversed: 1049602.000000
Num Phases Traversed - Session Mean & Stddev: 10496.020000,1.630828
Num Path Iterations: 1049602.000000
Num Path Iterations - Session Mean & Stddev: 10496.020000,1.630828
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6633400000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1910496682.000000
Throughput: 191049668.200000
Num Accesses - Session Mean & Stddev: 19104966.820000,2135.783376
Num Phases Traversed: 1044658.000000
Num Phases Traversed - Session Mean & Stddev: 10446.580000,1.167733
Num Path Iterations: 1044658.000000
Num Path Iterations - Session Mean & Stddev: 10446.580000,1.167733
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1792,0.001 0/1792,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1792,0.001 0/1792,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1792 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe9a5600000 of size 229376 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1915058208.000000
Throughput: 191505820.800000
Num Accesses - Session Mean & Stddev: 19150582.080000,2028.129269
Num Phases Traversed: 1047152.000000
Num Phases Traversed - Session Mean & Stddev: 10471.520000,1.108873
Num Path Iterations: 1047152.000000
Num Path Iterations - Session Mean & Stddev: 10471.520000,1.108873
+ set +x

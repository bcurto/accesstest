++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc96b000000 of size 57344 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2762182285.000000
Throughput: 276218228.500000
Num Accesses - Session Mean & Stddev: 27621822.850000,323.862359
Num Phases Traversed: 5695321.000000
Num Phases Traversed - Session Mean & Stddev: 56953.210000,0.667757
Num Path Iterations: 5695321.000000
Num Path Iterations - Session Mean & Stddev: 56953.210000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff381200000 of size 57344 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2746879565.000000
Throughput: 274687956.500000
Num Accesses - Session Mean & Stddev: 27468795.650000,304.393048
Num Phases Traversed: 5663769.000000
Num Phases Traversed - Session Mean & Stddev: 56637.690000,0.627615
Num Path Iterations: 5663769.000000
Num Path Iterations - Session Mean & Stddev: 56637.690000,0.627615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffa76600000 of size 57344 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2759466770.000000
Throughput: 275946677.000000
Num Accesses - Session Mean & Stddev: 27594667.700000,325.925774
Num Phases Traversed: 5689722.000000
Num Phases Traversed - Session Mean & Stddev: 56897.220000,0.672012
Num Path Iterations: 5689722.000000
Num Path Iterations - Session Mean & Stddev: 56897.220000,0.672012
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3056400000 of size 57344 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2769277835.000000
Throughput: 276927783.500000
Num Accesses - Session Mean & Stddev: 27692778.350000,242.451495
Num Phases Traversed: 5709951.000000
Num Phases Traversed - Session Mean & Stddev: 57099.510000,0.499900
Num Path Iterations: 5709951.000000
Num Path Iterations - Session Mean & Stddev: 57099.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9d0aa00000 of size 57344 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2754263205.000000
Throughput: 275426320.500000
Num Accesses - Session Mean & Stddev: 27542632.050000,337.798235
Num Phases Traversed: 5678993.000000
Num Phases Traversed - Session Mean & Stddev: 56789.930000,0.696491
Num Path Iterations: 5678993.000000
Num Path Iterations - Session Mean & Stddev: 56789.930000,0.696491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2ac3800000 of size 57344 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2756932645.000000
Throughput: 275693264.500000
Num Accesses - Session Mean & Stddev: 27569326.450000,339.188071
Num Phases Traversed: 5684497.000000
Num Phases Traversed - Session Mean & Stddev: 56844.970000,0.699357
Num Path Iterations: 5684497.000000
Num Path Iterations - Session Mean & Stddev: 56844.970000,0.699357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f72f4800000 of size 57344 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2760658415.000000
Throughput: 276065841.500000
Num Accesses - Session Mean & Stddev: 27606584.150000,323.862359
Num Phases Traversed: 5692179.000000
Num Phases Traversed - Session Mean & Stddev: 56921.790000,0.667757
Num Path Iterations: 5692179.000000
Num Path Iterations - Session Mean & Stddev: 56921.790000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f646be00000 of size 57344 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2759457555.000000
Throughput: 275945755.500000
Num Accesses - Session Mean & Stddev: 27594575.550000,339.188071
Num Phases Traversed: 5689703.000000
Num Phases Traversed - Session Mean & Stddev: 56897.030000,0.699357
Num Path Iterations: 5689703.000000
Num Path Iterations - Session Mean & Stddev: 56897.030000,0.699357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f37e3e00000 of size 57344 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2748676005.000000
Throughput: 274867600.500000
Num Accesses - Session Mean & Stddev: 27486760.050000,313.228906
Num Phases Traversed: 5667473.000000
Num Phases Traversed - Session Mean & Stddev: 56674.730000,0.645833
Num Path Iterations: 5667473.000000
Num Path Iterations - Session Mean & Stddev: 56674.730000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9cf5e00000 of size 57344 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2744063655.000000
Throughput: 274406365.500000
Num Accesses - Session Mean & Stddev: 27440636.550000,288.197758
Num Phases Traversed: 5657963.000000
Num Phases Traversed - Session Mean & Stddev: 56579.630000,0.594222
Num Path Iterations: 5657963.000000
Num Path Iterations - Session Mean & Stddev: 56579.630000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3cc6a00000 of size 57344 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2757799825.000000
Throughput: 275779982.500000
Num Accesses - Session Mean & Stddev: 27577998.250000,331.613763
Num Phases Traversed: 5686285.000000
Num Phases Traversed - Session Mean & Stddev: 56862.850000,0.683740
Num Path Iterations: 5686285.000000
Num Path Iterations - Session Mean & Stddev: 56862.850000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbc60c00000 of size 57344 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2753480415.000000
Throughput: 275348041.500000
Num Accesses - Session Mean & Stddev: 27534804.150000,323.862359
Num Phases Traversed: 5677379.000000
Num Phases Traversed - Session Mean & Stddev: 56773.790000,0.667757
Num Path Iterations: 5677379.000000
Num Path Iterations - Session Mean & Stddev: 56773.790000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2f68600000 of size 57344 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2764274575.000000
Throughput: 276427457.500000
Num Accesses - Session Mean & Stddev: 27642745.750000,294.015625
Num Phases Traversed: 5699635.000000
Num Phases Traversed - Session Mean & Stddev: 56996.350000,0.606218
Num Path Iterations: 5699635.000000
Num Path Iterations - Session Mean & Stddev: 56996.350000,0.606218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efcbe600000 of size 57344 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2773014275.000000
Throughput: 277301427.500000
Num Accesses - Session Mean & Stddev: 27730142.750000,260.052278
Num Phases Traversed: 5717655.000000
Num Phases Traversed - Session Mean & Stddev: 57176.550000,0.536190
Num Path Iterations: 5717655.000000
Num Path Iterations - Session Mean & Stddev: 57176.550000,0.536190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9c26200000 of size 57344 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2759810635.000000
Throughput: 275981063.500000
Num Accesses - Session Mean & Stddev: 27598106.350000,304.393048
Num Phases Traversed: 5690431.000000
Num Phases Traversed - Session Mean & Stddev: 56904.310000,0.627615
Num Path Iterations: 5690431.000000
Num Path Iterations - Session Mean & Stddev: 56904.310000,0.627615
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f074d200000 of size 57344 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2753924675.000000
Throughput: 275392467.500000
Num Accesses - Session Mean & Stddev: 27539246.750000,338.632821
Num Phases Traversed: 5678295.000000
Num Phases Traversed - Session Mean & Stddev: 56782.950000,0.698212
Num Path Iterations: 5678295.000000
Num Path Iterations - Session Mean & Stddev: 56782.950000,0.698212
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6613800000 of size 57344 bytes.
[1.261s] Test is ready to start.
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5255a00000 of size 57344 bytes.
[1.267s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2760417855.000000
Throughput: 276041785.500000
Num Accesses - Session Mean & Stddev: 27604178.550000,329.336071
Num Phases Traversed: 5691683.000000
Num Phases Traversed - Session Mean & Stddev: 56916.830000,0.679043
Num Path Iterations: 5691683.000000
Num Path Iterations - Session Mean & Stddev: 56916.830000,0.679043
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f927ec00000 of size 57344 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2706685190.000000
Throughput: 270668519.000000
Num Accesses - Session Mean & Stddev: 27066851.900000,341.709950
Num Phases Traversed: 5580894.000000
Num Phases Traversed - Session Mean & Stddev: 55808.940000,0.704557
Num Path Iterations: 5580894.000000
Num Path Iterations - Session Mean & Stddev: 55808.940000,0.704557
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/448,0.1 0/448,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/448,0.1 0/448,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    448 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbacfe00000 of size 57344 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2756569865.000000
Throughput: 275656986.500000
Num Accesses - Session Mean & Stddev: 27565698.650000,242.451495
Num Phases Traversed: 5683749.000000
Num Phases Traversed - Session Mean & Stddev: 56837.490000,0.499900
Num Path Iterations: 5683749.000000
Num Path Iterations - Session Mean & Stddev: 56837.490000,0.499900
+ set +x

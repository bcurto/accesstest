++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f57b0c00000 of size 12288 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1135453020.000000
Throughput: 113545302.000000
Num Accesses - Session Mean & Stddev: 11354530.200000,59469.685498
Num Phases Traversed: 8537340.000000
Num Phases Traversed - Session Mean & Stddev: 85373.400000,447.140492
Num Path Iterations: 8537340.000000
Num Path Iterations - Session Mean & Stddev: 85373.400000,447.140492
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2750200000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1132462648.000000
Throughput: 113246264.800000
Num Accesses - Session Mean & Stddev: 11324626.480000,78129.639180
Num Phases Traversed: 8514856.000000
Num Phases Traversed - Session Mean & Stddev: 85148.560000,587.440896
Num Path Iterations: 8514856.000000
Num Path Iterations - Session Mean & Stddev: 85148.560000,587.440896
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcfea400000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1135025159.000000
Throughput: 113502515.900000
Num Accesses - Session Mean & Stddev: 11350251.590000,30237.788270
Num Phases Traversed: 8534123.000000
Num Phases Traversed - Session Mean & Stddev: 85341.230000,227.351792
Num Path Iterations: 8534123.000000
Num Path Iterations - Session Mean & Stddev: 85341.230000,227.351792
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f025a800000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1132893967.000000
Throughput: 113289396.700000
Num Accesses - Session Mean & Stddev: 11328939.670000,35054.982828
Num Phases Traversed: 8518099.000000
Num Phases Traversed - Session Mean & Stddev: 85180.990000,263.571299
Num Path Iterations: 8518099.000000
Num Path Iterations - Session Mean & Stddev: 85180.990000,263.571299
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2aee400000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1134772858.000000
Throughput: 113477285.800000
Num Accesses - Session Mean & Stddev: 11347728.580000,68598.810138
Num Phases Traversed: 8532226.000000
Num Phases Traversed - Session Mean & Stddev: 85322.260000,515.780527
Num Path Iterations: 8532226.000000
Num Path Iterations - Session Mean & Stddev: 85322.260000,515.780527
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feb3e600000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1130100435.000000
Throughput: 113010043.500000
Num Accesses - Session Mean & Stddev: 11301004.350000,29878.735654
Num Phases Traversed: 8497095.000000
Num Phases Traversed - Session Mean & Stddev: 84970.950000,224.652148
Num Path Iterations: 8497095.000000
Num Path Iterations - Session Mean & Stddev: 84970.950000,224.652148
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7434c00000 of size 12288 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1133146534.000000
Throughput: 113314653.400000
Num Accesses - Session Mean & Stddev: 11331465.340000,26924.765054
Num Phases Traversed: 8519998.000000
Num Phases Traversed - Session Mean & Stddev: 85199.980000,202.441843
Num Path Iterations: 8519998.000000
Num Path Iterations - Session Mean & Stddev: 85199.980000,202.441843
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9cda800000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1129606606.000000
Throughput: 112960660.600000
Num Accesses - Session Mean & Stddev: 11296066.060000,76650.685218
Num Phases Traversed: 8493382.000000
Num Phases Traversed - Session Mean & Stddev: 84933.820000,576.320941
Num Path Iterations: 8493382.000000
Num Path Iterations - Session Mean & Stddev: 84933.820000,576.320941
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f273ae00000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1133755009.000000
Throughput: 113375500.900000
Num Accesses - Session Mean & Stddev: 11337550.090000,55539.637050
Num Phases Traversed: 8524573.000000
Num Phases Traversed - Session Mean & Stddev: 85245.730000,417.591256
Num Path Iterations: 8524573.000000
Num Path Iterations - Session Mean & Stddev: 85245.730000,417.591256
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f797f800000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1132323397.000000
Throughput: 113232339.700000
Num Accesses - Session Mean & Stddev: 11323233.970000,27367.684375
Num Phases Traversed: 8513809.000000
Num Phases Traversed - Session Mean & Stddev: 85138.090000,205.772063
Num Path Iterations: 8513809.000000
Num Path Iterations - Session Mean & Stddev: 85138.090000,205.772063
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f690ea00000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1135655446.000000
Throughput: 113565544.600000
Num Accesses - Session Mean & Stddev: 11356554.460000,13244.727121
Num Phases Traversed: 8538862.000000
Num Phases Traversed - Session Mean & Stddev: 85388.620000,99.584414
Num Path Iterations: 8538862.000000
Num Path Iterations - Session Mean & Stddev: 85388.620000,99.584414
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb79c400000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1140199790.000000
Throughput: 114019979.000000
Num Accesses - Session Mean & Stddev: 11401997.900000,8845.339960
Num Phases Traversed: 8573030.000000
Num Phases Traversed - Session Mean & Stddev: 85730.300000,66.506315
Num Path Iterations: 8573030.000000
Num Path Iterations - Session Mean & Stddev: 85730.300000,66.506315
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc075000000 of size 12288 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1132425142.000000
Throughput: 113242514.200000
Num Accesses - Session Mean & Stddev: 11324251.420000,12949.360026
Num Phases Traversed: 8514574.000000
Num Phases Traversed - Session Mean & Stddev: 85145.740000,97.363609
Num Path Iterations: 8514574.000000
Num Path Iterations - Session Mean & Stddev: 85145.740000,97.363609
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f588c800000 of size 12288 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1125109876.000000
Throughput: 112510987.600000
Num Accesses - Session Mean & Stddev: 11251098.760000,52011.091893
Num Phases Traversed: 8459572.000000
Num Phases Traversed - Session Mean & Stddev: 84595.720000,391.060841
Num Path Iterations: 8459572.000000
Num Path Iterations - Session Mean & Stddev: 84595.720000,391.060841
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f77efe00000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1135949775.000000
Throughput: 113594977.500000
Num Accesses - Session Mean & Stddev: 11359497.750000,11171.182574
Num Phases Traversed: 8541075.000000
Num Phases Traversed - Session Mean & Stddev: 85410.750000,83.993854
Num Path Iterations: 8541075.000000
Num Path Iterations - Session Mean & Stddev: 85410.750000,83.993854
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1b72e00000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1135657441.000000
Throughput: 113565744.100000
Num Accesses - Session Mean & Stddev: 11356574.410000,43862.209739
Num Phases Traversed: 8538877.000000
Num Phases Traversed - Session Mean & Stddev: 85388.770000,329.791051
Num Path Iterations: 8538877.000000
Num Path Iterations - Session Mean & Stddev: 85388.770000,329.791051
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7c6ce00000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1134950546.000000
Throughput: 113495054.600000
Num Accesses - Session Mean & Stddev: 11349505.460000,60996.566024
Num Phases Traversed: 8533562.000000
Num Phases Traversed - Session Mean & Stddev: 85335.620000,458.620797
Num Path Iterations: 8533562.000000
Num Path Iterations - Session Mean & Stddev: 85335.620000,458.620797
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa525200000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1132410911.000000
Throughput: 113241091.100000
Num Accesses - Session Mean & Stddev: 11324109.110000,30306.839632
Num Phases Traversed: 8514467.000000
Num Phases Traversed - Session Mean & Stddev: 85144.670000,227.870975
Num Path Iterations: 8514467.000000
Num Path Iterations - Session Mean & Stddev: 85144.670000,227.870975
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc9be000000 of size 12288 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1133066601.000000
Throughput: 113306660.100000
Num Accesses - Session Mean & Stddev: 11330666.010000,42925.949160
Num Phases Traversed: 8519397.000000
Num Phases Traversed - Session Mean & Stddev: 85193.970000,322.751497
Num Path Iterations: 8519397.000000
Num Path Iterations - Session Mean & Stddev: 85193.970000,322.751497
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0.1 0/96,0.1' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0.1 0/96,0.1 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f27ffa00000 of size 12288 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1136261926.000000
Throughput: 113626192.600000
Num Accesses - Session Mean & Stddev: 11362619.260000,124142.269222
Num Phases Traversed: 8543422.000000
Num Phases Traversed - Session Mean & Stddev: 85434.220000,933.400520
Num Path Iterations: 8543422.000000
Num Path Iterations - Session Mean & Stddev: 85434.220000,933.400520
+ set +x

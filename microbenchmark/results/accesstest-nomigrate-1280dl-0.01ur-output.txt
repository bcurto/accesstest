++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcb9ba00000 of size 81920 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1322715930.000000
Throughput: 132271593.000000
Num Accesses - Session Mean & Stddev: 13227159.300000,2308.758599
Num Phases Traversed: 1953890.000000
Num Phases Traversed - Session Mean & Stddev: 19538.900000,3.410279
Num Path Iterations: 1953890.000000
Num Path Iterations - Session Mean & Stddev: 19538.900000,3.410279
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efe4b000000 of size 81920 bytes.
[1.267s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1324291309.000000
Throughput: 132429130.900000
Num Accesses - Session Mean & Stddev: 13242913.090000,4652.182234
Num Phases Traversed: 1956217.000000
Num Phases Traversed - Session Mean & Stddev: 19562.170000,6.871761
Num Path Iterations: 1956217.000000
Num Path Iterations - Session Mean & Stddev: 19562.170000,6.871761
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f601b200000 of size 81920 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1323483648.000000
Throughput: 132348364.800000
Num Accesses - Session Mean & Stddev: 13234836.480000,2971.281967
Num Phases Traversed: 1955024.000000
Num Phases Traversed - Session Mean & Stddev: 19550.240000,4.388895
Num Path Iterations: 1955024.000000
Num Path Iterations - Session Mean & Stddev: 19550.240000,4.388895
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fccdf000000 of size 81920 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1323651544.000000
Throughput: 132365154.400000
Num Accesses - Session Mean & Stddev: 13236515.440000,1244.943913
Num Phases Traversed: 1955272.000000
Num Phases Traversed - Session Mean & Stddev: 19552.720000,1.838913
Num Path Iterations: 1955272.000000
Num Path Iterations - Session Mean & Stddev: 19552.720000,1.838913
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa6e3400000 of size 81920 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1322857423.000000
Throughput: 132285742.300000
Num Accesses - Session Mean & Stddev: 13228574.230000,2217.616815
Num Phases Traversed: 1954099.000000
Num Phases Traversed - Session Mean & Stddev: 19540.990000,3.275653
Num Path Iterations: 1954099.000000
Num Path Iterations - Session Mean & Stddev: 19540.990000,3.275653
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3730e00000 of size 81920 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1323028027.000000
Throughput: 132302802.700000
Num Accesses - Session Mean & Stddev: 13230280.270000,2029.860063
Num Phases Traversed: 1954351.000000
Num Phases Traversed - Session Mean & Stddev: 19543.510000,2.998316
Num Path Iterations: 1954351.000000
Num Path Iterations - Session Mean & Stddev: 19543.510000,2.998316
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4f3a400000 of size 81920 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1322070749.000000
Throughput: 132207074.900000
Num Accesses - Session Mean & Stddev: 13220707.490000,2163.552613
Num Phases Traversed: 1952937.000000
Num Phases Traversed - Session Mean & Stddev: 19529.370000,3.195794
Num Path Iterations: 1952937.000000
Num Path Iterations - Session Mean & Stddev: 19529.370000,3.195794
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f45dbe00000 of size 81920 bytes.
[1.231s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1321625283.000000
Throughput: 132162528.300000
Num Accesses - Session Mean & Stddev: 13216252.830000,1063.105583
Num Phases Traversed: 1952279.000000
Num Phases Traversed - Session Mean & Stddev: 19522.790000,1.570318
Num Path Iterations: 1952279.000000
Num Path Iterations - Session Mean & Stddev: 19522.790000,1.570318
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1d65e00000 of size 81920 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1322126263.000000
Throughput: 132212626.300000
Num Accesses - Session Mean & Stddev: 13221262.630000,1077.664072
Num Phases Traversed: 1953019.000000
Num Phases Traversed - Session Mean & Stddev: 19530.190000,1.591823
Num Path Iterations: 1953019.000000
Num Path Iterations - Session Mean & Stddev: 19530.190000,1.591823
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7520400000 of size 81920 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1324479515.000000
Throughput: 132447951.500000
Num Accesses - Session Mean & Stddev: 13244795.150000,1502.802764
Num Phases Traversed: 1956495.000000
Num Phases Traversed - Session Mean & Stddev: 19564.950000,2.219797
Num Path Iterations: 1956495.000000
Num Path Iterations - Session Mean & Stddev: 19564.950000,2.219797
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0a70800000 of size 81920 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1324236472.000000
Throughput: 132423647.200000
Num Accesses - Session Mean & Stddev: 13242364.720000,2640.334718
Num Phases Traversed: 1956136.000000
Num Phases Traversed - Session Mean & Stddev: 19561.360000,3.900051
Num Path Iterations: 1956136.000000
Num Path Iterations - Session Mean & Stddev: 19561.360000,3.900051
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe3b3a00000 of size 81920 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1322601517.000000
Throughput: 132260151.700000
Num Accesses - Session Mean & Stddev: 13226015.170000,2762.118517
Num Phases Traversed: 1953721.000000
Num Phases Traversed - Session Mean & Stddev: 19537.210000,4.079939
Num Path Iterations: 1953721.000000
Num Path Iterations - Session Mean & Stddev: 19537.210000,4.079939
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3267400000 of size 81920 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1323659668.000000
Throughput: 132365966.800000
Num Accesses - Session Mean & Stddev: 13236596.680000,906.876815
Num Phases Traversed: 1955284.000000
Num Phases Traversed - Session Mean & Stddev: 19552.840000,1.339552
Num Path Iterations: 1955284.000000
Num Path Iterations - Session Mean & Stddev: 19552.840000,1.339552
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3c9b400000 of size 81920 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1323928437.000000
Throughput: 132392843.700000
Num Accesses - Session Mean & Stddev: 13239284.370000,3090.110120
Num Phases Traversed: 1955681.000000
Num Phases Traversed - Session Mean & Stddev: 19556.810000,4.564417
Num Path Iterations: 1955681.000000
Num Path Iterations - Session Mean & Stddev: 19556.810000,4.564417
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe4ffc00000 of size 81920 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1323734815.000000
Throughput: 132373481.500000
Num Accesses - Session Mean & Stddev: 13237348.150000,2343.975019
Num Phases Traversed: 1955395.000000
Num Phases Traversed - Session Mean & Stddev: 19553.950000,3.462297
Num Path Iterations: 1955395.000000
Num Path Iterations - Session Mean & Stddev: 19553.950000,3.462297
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0322e00000 of size 81920 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1323322522.000000
Throughput: 132332252.200000
Num Accesses - Session Mean & Stddev: 13233225.220000,2317.714221
Num Phases Traversed: 1954786.000000
Num Phases Traversed - Session Mean & Stddev: 19547.860000,3.423507
Num Path Iterations: 1954786.000000
Num Path Iterations - Session Mean & Stddev: 19547.860000,3.423507
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9882600000 of size 81920 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1323863445.000000
Throughput: 132386344.500000
Num Accesses - Session Mean & Stddev: 13238634.450000,2832.701002
Num Phases Traversed: 1955585.000000
Num Phases Traversed - Session Mean & Stddev: 19555.850000,4.184196
Num Path Iterations: 1955585.000000
Num Path Iterations - Session Mean & Stddev: 19555.850000,4.184196
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fce7d200000 of size 81920 bytes.
[1.257s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1324673814.000000
Throughput: 132467381.400000
Num Accesses - Session Mean & Stddev: 13246738.140000,1331.402734
Num Phases Traversed: 1956782.000000
Num Phases Traversed - Session Mean & Stddev: 19567.820000,1.966621
Num Path Iterations: 1956782.000000
Num Path Iterations - Session Mean & Stddev: 19567.820000,1.966621
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1555400000 of size 81920 bytes.
[1.249s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1324558724.000000
Throughput: 132455872.400000
Num Accesses - Session Mean & Stddev: 13245587.240000,2115.622292
Num Phases Traversed: 1956612.000000
Num Phases Traversed - Session Mean & Stddev: 19566.120000,3.124996
Num Path Iterations: 1956612.000000
Num Path Iterations - Session Mean & Stddev: 19566.120000,3.124996
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/640,0.01 0/640,0.01' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/640,0.01 0/640,0.01 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    640 sess-global datalocs, 0.010000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0913e00000 of size 81920 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1324653504.000000
Throughput: 132465350.400000
Num Accesses - Session Mean & Stddev: 13246535.040000,1261.038579
Num Phases Traversed: 1956752.000000
Num Phases Traversed - Session Mean & Stddev: 19567.520000,1.862686
Num Path Iterations: 1956752.000000
Num Path Iterations - Session Mean & Stddev: 19567.520000,1.862686
+ set +x

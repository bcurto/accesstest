++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa665e00000 of size 131072 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1967054843.000000
Throughput: 196705484.300000
Num Accesses - Session Mean & Stddev: 19670548.430000,512.255371
Num Phases Traversed: 1854063.000000
Num Phases Traversed - Session Mean & Stddev: 18540.630000,0.482804
Num Path Iterations: 1854063.000000
Num Path Iterations - Session Mean & Stddev: 18540.630000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f30d3200000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1975807032.000000
Throughput: 197580703.200000
Num Accesses - Session Mean & Stddev: 19758070.320000,708.252270
Num Phases Traversed: 1862312.000000
Num Phases Traversed - Session Mean & Stddev: 18623.120000,0.667533
Num Path Iterations: 1862312.000000
Num Path Iterations - Session Mean & Stddev: 18623.120000,0.667533
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f144f200000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1971008129.000000
Throughput: 197100812.900000
Num Accesses - Session Mean & Stddev: 19710081.290000,717.960755
Num Phases Traversed: 1857789.000000
Num Phases Traversed - Session Mean & Stddev: 18577.890000,0.676683
Num Path Iterations: 1857789.000000
Num Path Iterations - Session Mean & Stddev: 18577.890000,0.676683
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5753c00000 of size 131072 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1972662228.000000
Throughput: 197266222.800000
Num Accesses - Session Mean & Stddev: 19726622.280000,570.971805
Num Phases Traversed: 1859348.000000
Num Phases Traversed - Session Mean & Stddev: 18593.480000,0.538145
Num Path Iterations: 1859348.000000
Num Path Iterations - Session Mean & Stddev: 18593.480000,0.538145
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa275200000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1970604949.000000
Throughput: 197060494.900000
Num Accesses - Session Mean & Stddev: 19706049.490000,620.390208
Num Phases Traversed: 1857409.000000
Num Phases Traversed - Session Mean & Stddev: 18574.090000,0.584722
Num Path Iterations: 1857409.000000
Num Path Iterations - Session Mean & Stddev: 18574.090000,0.584722
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3ceac00000 of size 131072 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1975398547.000000
Throughput: 197539854.700000
Num Accesses - Session Mean & Stddev: 19753985.470000,685.228596
Num Phases Traversed: 1861927.000000
Num Phases Traversed - Session Mean & Stddev: 18619.270000,0.645833
Num Path Iterations: 1861927.000000
Num Path Iterations - Session Mean & Stddev: 18619.270000,0.645833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efe3a400000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1966529648.000000
Throughput: 196652964.800000
Num Accesses - Session Mean & Stddev: 19665296.480000,634.474451
Num Phases Traversed: 1853568.000000
Num Phases Traversed - Session Mean & Stddev: 18535.680000,0.597997
Num Path Iterations: 1853568.000000
Num Path Iterations - Session Mean & Stddev: 18535.680000,0.597997
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd503c00000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1975184225.000000
Throughput: 197518422.500000
Num Accesses - Session Mean & Stddev: 19751842.250000,677.297060
Num Phases Traversed: 1861725.000000
Num Phases Traversed - Session Mean & Stddev: 18617.250000,0.638357
Num Path Iterations: 1861725.000000
Num Path Iterations - Session Mean & Stddev: 18617.250000,0.638357
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f802e600000 of size 131072 bytes.
[1.263s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974165665.000000
Throughput: 197416566.500000
Num Accesses - Session Mean & Stddev: 19741656.650000,643.197067
Num Phases Traversed: 1860765.000000
Num Phases Traversed - Session Mean & Stddev: 18607.650000,0.606218
Num Path Iterations: 1860765.000000
Num Path Iterations - Session Mean & Stddev: 18607.650000,0.606218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcca7400000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1971374174.000000
Throughput: 197137417.400000
Num Accesses - Session Mean & Stddev: 19713741.740000,502.605006
Num Phases Traversed: 1858134.000000
Num Phases Traversed - Session Mean & Stddev: 18581.340000,0.473709
Num Path Iterations: 1858134.000000
Num Path Iterations - Session Mean & Stddev: 18581.340000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4d82400000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974198556.000000
Throughput: 197419855.600000
Num Accesses - Session Mean & Stddev: 19741985.560000,718.352634
Num Phases Traversed: 1860796.000000
Num Phases Traversed - Session Mean & Stddev: 18607.960000,0.677052
Num Path Iterations: 1860796.000000
Num Path Iterations - Session Mean & Stddev: 18607.960000,0.677052
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f178e000000 of size 131072 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1971983188.000000
Throughput: 197198318.800000
Num Accesses - Session Mean & Stddev: 19719831.880000,574.901457
Num Phases Traversed: 1858708.000000
Num Phases Traversed - Session Mean & Stddev: 18587.080000,0.541849
Num Path Iterations: 1858708.000000
Num Path Iterations - Session Mean & Stddev: 18587.080000,0.541849
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbd4ae00000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1972262231.000000
Throughput: 197226223.100000
Num Accesses - Session Mean & Stddev: 19722622.310000,675.966089
Num Phases Traversed: 1858971.000000
Num Phases Traversed - Session Mean & Stddev: 18589.710000,0.637103
Num Path Iterations: 1858971.000000
Num Path Iterations - Session Mean & Stddev: 18589.710000,0.637103
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2028a00000 of size 131072 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1973687154.000000
Throughput: 197368715.400000
Num Accesses - Session Mean & Stddev: 19736871.540000,368.153240
Num Phases Traversed: 1860314.000000
Num Phases Traversed - Session Mean & Stddev: 18603.140000,0.346987
Num Path Iterations: 1860314.000000
Num Path Iterations - Session Mean & Stddev: 18603.140000,0.346987
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9aaf600000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1976562464.000000
Throughput: 197656246.400000
Num Accesses - Session Mean & Stddev: 19765624.640000,673.045415
Num Phases Traversed: 1863024.000000
Num Phases Traversed - Session Mean & Stddev: 18630.240000,0.634350
Num Path Iterations: 1863024.000000
Num Path Iterations - Session Mean & Stddev: 18630.240000,0.634350
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0653400000 of size 131072 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1971767805.000000
Throughput: 197176780.500000
Num Accesses - Session Mean & Stddev: 19717678.050000,407.484782
Num Phases Traversed: 1858505.000000
Num Phases Traversed - Session Mean & Stddev: 18585.050000,0.384057
Num Path Iterations: 1858505.000000
Num Path Iterations - Session Mean & Stddev: 18585.050000,0.384057
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4c03e00000 of size 131072 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1975762470.000000
Throughput: 197576247.000000
Num Accesses - Session Mean & Stddev: 19757624.700000,486.211281
Num Phases Traversed: 1862270.000000
Num Phases Traversed - Session Mean & Stddev: 18622.700000,0.458258
Num Path Iterations: 1862270.000000
Num Path Iterations - Session Mean & Stddev: 18622.700000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdf68c00000 of size 131072 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1971365686.000000
Throughput: 197136568.600000
Num Accesses - Session Mean & Stddev: 19713656.860000,465.390933
Num Phases Traversed: 1858126.000000
Num Phases Traversed - Session Mean & Stddev: 18581.260000,0.438634
Num Path Iterations: 1858126.000000
Num Path Iterations - Session Mean & Stddev: 18581.260000,0.438634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9eba000000 of size 131072 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1974836217.000000
Throughput: 197483621.700000
Num Accesses - Session Mean & Stddev: 19748362.170000,626.888508
Num Phases Traversed: 1861397.000000
Num Phases Traversed - Session Mean & Stddev: 18613.970000,0.590847
Num Path Iterations: 1861397.000000
Num Path Iterations - Session Mean & Stddev: 18613.970000,0.590847
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/1024,0.1 0/1024,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/1024,0.1 0/1024,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    1024 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1f87400000 of size 131072 bytes.
[1.265s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1972592202.000000
Throughput: 197259220.200000
Num Accesses - Session Mean & Stddev: 19725922.020000,709.839925
Num Phases Traversed: 1859282.000000
Num Phases Traversed - Session Mean & Stddev: 18592.820000,0.669029
Num Path Iterations: 1859282.000000
Num Path Iterations - Session Mean & Stddev: 18592.820000,0.669029
+ set +x

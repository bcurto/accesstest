++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd184e0b000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2695971481.000000
Throughput: 269597148.100000
Num Accesses - Session Mean & Stddev: 26959714.810000,18385.185617
Num Phases Traversed: 39072149.000000
Num Phases Traversed - Session Mean & Stddev: 390721.490000,266.451965
Num Path Iterations: 39072149.000000
Num Path Iterations - Session Mean & Stddev: 390721.490000,266.451965
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff30a501000 of size 4096 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2682742111.000000
Throughput: 268274211.100000
Num Accesses - Session Mean & Stddev: 26827421.110000,456801.393011
Num Phases Traversed: 38880419.000000
Num Phases Traversed - Session Mean & Stddev: 388804.190000,6620.310044
Num Path Iterations: 38880419.000000
Num Path Iterations - Session Mean & Stddev: 388804.190000,6620.310044
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcdce4f6000 of size 4096 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2631948244.000000
Throughput: 263194824.400000
Num Accesses - Session Mean & Stddev: 26319482.440000,35886.944603
Num Phases Traversed: 38144276.000000
Num Phases Traversed - Session Mean & Stddev: 381442.760000,520.100646
Num Path Iterations: 38144276.000000
Num Path Iterations - Session Mean & Stddev: 381442.760000,520.100646
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3af28d3000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2707825957.000000
Throughput: 270782595.700000
Num Accesses - Session Mean & Stddev: 27078259.570000,12493.903203
Num Phases Traversed: 39243953.000000
Num Phases Traversed - Session Mean & Stddev: 392439.530000,181.071061
Num Path Iterations: 39243953.000000
Num Path Iterations - Session Mean & Stddev: 392439.530000,181.071061
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa8eafc1000 of size 4096 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2688125422.000000
Throughput: 268812542.200000
Num Accesses - Session Mean & Stddev: 26881254.220000,224724.732512
Num Phases Traversed: 38958438.000000
Num Phases Traversed - Session Mean & Stddev: 389584.380000,3256.880181
Num Path Iterations: 38958438.000000
Num Path Iterations - Session Mean & Stddev: 389584.380000,3256.880181
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb4ee770000 of size 4096 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2699893234.000000
Throughput: 269989323.400000
Num Accesses - Session Mean & Stddev: 26998932.340000,11302.427554
Num Phases Traversed: 39128986.000000
Num Phases Traversed - Session Mean & Stddev: 391289.860000,163.803298
Num Path Iterations: 39128986.000000
Num Path Iterations - Session Mean & Stddev: 391289.860000,163.803298
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f98c51c8000 of size 4096 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2686794757.000000
Throughput: 268679475.700000
Num Accesses - Session Mean & Stddev: 26867947.570000,5341.682629
Num Phases Traversed: 38939153.000000
Num Phases Traversed - Session Mean & Stddev: 389391.530000,77.415690
Num Path Iterations: 38939153.000000
Num Path Iterations - Session Mean & Stddev: 389391.530000,77.415690
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f811f9b7000 of size 4096 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2688338149.000000
Throughput: 268833814.900000
Num Accesses - Session Mean & Stddev: 26883381.490000,297.308341
Num Phases Traversed: 38961521.000000
Num Phases Traversed - Session Mean & Stddev: 389615.210000,4.308817
Num Path Iterations: 38961521.000000
Num Path Iterations - Session Mean & Stddev: 389615.210000,4.308817
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe967c65000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2708060488.000000
Throughput: 270806048.800000
Num Accesses - Session Mean & Stddev: 27080604.880000,17713.767513
Num Phases Traversed: 39247352.000000
Num Phases Traversed - Session Mean & Stddev: 392473.520000,256.721268
Num Path Iterations: 39247352.000000
Num Path Iterations - Session Mean & Stddev: 392473.520000,256.721268
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f61d8478000 of size 4096 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2692020403.000000
Throughput: 269202040.300000
Num Accesses - Session Mean & Stddev: 26920204.030000,48286.931263
Num Phases Traversed: 39014887.000000
Num Phases Traversed - Session Mean & Stddev: 390148.870000,699.810598
Num Path Iterations: 39014887.000000
Num Path Iterations - Session Mean & Stddev: 390148.870000,699.810598
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fca0d5bd000 of size 4096 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2699226694.000000
Throughput: 269922669.400000
Num Accesses - Session Mean & Stddev: 26992266.940000,2068.426662
Num Phases Traversed: 39119326.000000
Num Phases Traversed - Session Mean & Stddev: 391193.260000,29.977198
Num Path Iterations: 39119326.000000
Num Path Iterations - Session Mean & Stddev: 391193.260000,29.977198
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2ca2386000 of size 4096 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2667812926.000000
Throughput: 266781292.600000
Num Accesses - Session Mean & Stddev: 26678129.260000,11460.986904
Num Phases Traversed: 38664054.000000
Num Phases Traversed - Session Mean & Stddev: 386640.540000,166.101259
Num Path Iterations: 38664054.000000
Num Path Iterations - Session Mean & Stddev: 386640.540000,166.101259
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2db4003000 of size 4096 bytes.
[1.227s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2657809996.000000
Throughput: 265780999.600000
Num Accesses - Session Mean & Stddev: 26578099.960000,68234.139925
Num Phases Traversed: 38519084.000000
Num Phases Traversed - Session Mean & Stddev: 385190.840000,988.900579
Num Path Iterations: 38519084.000000
Num Path Iterations - Session Mean & Stddev: 385190.840000,988.900579
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9ca20a5000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2681518051.000000
Throughput: 268151805.100000
Num Accesses - Session Mean & Stddev: 26815180.510000,48676.098093
Num Phases Traversed: 38862679.000000
Num Phases Traversed - Session Mean & Stddev: 388626.790000,705.450697
Num Path Iterations: 38862679.000000
Num Path Iterations - Session Mean & Stddev: 388626.790000,705.450697
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8a54e3d000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2682881284.000000
Throughput: 268288128.400000
Num Accesses - Session Mean & Stddev: 26828812.840000,26562.359086
Num Phases Traversed: 38882436.000000
Num Phases Traversed - Session Mean & Stddev: 388824.360000,384.961726
Num Path Iterations: 38882436.000000
Num Path Iterations - Session Mean & Stddev: 388824.360000,384.961726
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f88c538e000 of size 4096 bytes.
[1.227s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2691591913.000000
Throughput: 269159191.300000
Num Accesses - Session Mean & Stddev: 26915919.130000,151766.204383
Num Phases Traversed: 39008677.000000
Num Phases Traversed - Session Mean & Stddev: 390086.770000,2199.510208
Num Path Iterations: 39008677.000000
Num Path Iterations - Session Mean & Stddev: 390086.770000,2199.510208
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fae184e8000 of size 4096 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2691934360.000000
Throughput: 269193436.000000
Num Accesses - Session Mean & Stddev: 26919343.600000,74968.533341
Num Phases Traversed: 39013640.000000
Num Phases Traversed - Session Mean & Stddev: 390136.400000,1086.500483
Num Path Iterations: 39013640.000000
Num Path Iterations - Session Mean & Stddev: 390136.400000,1086.500483
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f73e6681000 of size 4096 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2675475997.000000
Throughput: 267547599.700000
Num Accesses - Session Mean & Stddev: 26754759.970000,2936.639595
Num Phases Traversed: 38775113.000000
Num Phases Traversed - Session Mean & Stddev: 387751.130000,42.559994
Num Path Iterations: 38775113.000000
Num Path Iterations - Session Mean & Stddev: 387751.130000,42.559994
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feec9215000 of size 4096 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2680774162.000000
Throughput: 268077416.200000
Num Accesses - Session Mean & Stddev: 26807741.620000,36143.641067
Num Phases Traversed: 38851898.000000
Num Phases Traversed - Session Mean & Stddev: 388518.980000,523.820885
Num Path Iterations: 38851898.000000
Num Path Iterations - Session Mean & Stddev: 388518.980000,523.820885
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,0 0/32,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,0 0/32,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3f0c500000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2713361620.000000
Throughput: 271336162.000000
Num Accesses - Session Mean & Stddev: 27133616.200000,131661.682427
Num Phases Traversed: 39324180.000000
Num Phases Traversed - Session Mean & Stddev: 393241.800000,1908.140325
Num Path Iterations: 39324180.000000
Num Path Iterations - Session Mean & Stddev: 393241.800000,1908.140325
+ set +x

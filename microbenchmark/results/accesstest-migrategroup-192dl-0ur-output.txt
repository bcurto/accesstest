++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f81ffe00000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2127023614.000000
Throughput: 212702361.400000
Num Accesses - Session Mean & Stddev: 21270236.140000,65.643281
Num Phases Traversed: 15992758.000000
Num Phases Traversed - Session Mean & Stddev: 159927.580000,0.493559
Num Path Iterations: 15992758.000000
Num Path Iterations - Session Mean & Stddev: 159927.580000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4d62e00000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2119749046.000000
Throughput: 211974904.600000
Num Accesses - Session Mean & Stddev: 21197490.460000,64.556397
Num Phases Traversed: 15938062.000000
Num Phases Traversed - Session Mean & Stddev: 159380.620000,0.485386
Num Path Iterations: 15938062.000000
Num Path Iterations - Session Mean & Stddev: 159380.620000,0.485386
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9820a00000 of size 12288 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2123365449.000000
Throughput: 212336544.900000
Num Accesses - Session Mean & Stddev: 21233654.490000,66.380192
Num Phases Traversed: 15965253.000000
Num Phases Traversed - Session Mean & Stddev: 159652.530000,0.499099
Num Path Iterations: 15965253.000000
Num Path Iterations - Session Mean & Stddev: 159652.530000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9d6b600000 of size 12288 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1166966838.000000
Throughput: 116696683.800000
Num Accesses - Session Mean & Stddev: 11669668.380000,2582.668232
Num Phases Traversed: 8774286.000000
Num Phases Traversed - Session Mean & Stddev: 87742.860000,19.418558
Num Path Iterations: 8774286.000000
Num Path Iterations - Session Mean & Stddev: 87742.860000,19.418558
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f26b0e00000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2104620030.000000
Throughput: 210462003.000000
Num Accesses - Session Mean & Stddev: 21046200.300000,80.900742
Num Phases Traversed: 15824310.000000
Num Phases Traversed - Session Mean & Stddev: 158243.100000,0.608276
Num Path Iterations: 15824310.000000
Num Path Iterations - Session Mean & Stddev: 158243.100000,0.608276
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff576800000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2105676848.000000
Throughput: 210567684.800000
Num Accesses - Session Mean & Stddev: 21056768.480000,66.019464
Num Phases Traversed: 15832256.000000
Num Phases Traversed - Session Mean & Stddev: 158322.560000,0.496387
Num Path Iterations: 15832256.000000
Num Path Iterations - Session Mean & Stddev: 158322.560000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f512a000000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2110278515.000000
Throughput: 211027851.500000
Num Accesses - Session Mean & Stddev: 21102785.150000,66.166665
Num Phases Traversed: 15866855.000000
Num Phases Traversed - Session Mean & Stddev: 158668.550000,0.497494
Num Path Iterations: 15866855.000000
Num Path Iterations - Session Mean & Stddev: 158668.550000,0.497494
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f67a6800000 of size 12288 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2112412234.000000
Throughput: 211241223.400000
Num Accesses - Session Mean & Stddev: 21124122.340000,75.189124
Num Phases Traversed: 15882898.000000
Num Phases Traversed - Session Mean & Stddev: 158828.980000,0.565332
Num Path Iterations: 15882898.000000
Num Path Iterations - Session Mean & Stddev: 158828.980000,0.565332
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd135a00000 of size 12288 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2106770241.000000
Throughput: 210677024.100000
Num Accesses - Session Mean & Stddev: 21067702.410000,72.494151
Num Phases Traversed: 15840477.000000
Num Phases Traversed - Session Mean & Stddev: 158404.770000,0.545069
Num Path Iterations: 15840477.000000
Num Path Iterations - Session Mean & Stddev: 158404.770000,0.545069
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff3b6800000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2116442932.000000
Throughput: 211644293.200000
Num Accesses - Session Mean & Stddev: 21164429.320000,79.622469
Num Phases Traversed: 15913204.000000
Num Phases Traversed - Session Mean & Stddev: 159132.040000,0.598665
Num Path Iterations: 15913204.000000
Num Path Iterations - Session Mean & Stddev: 159132.040000,0.598665
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f950d200000 of size 12288 bytes.
[1.252s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2123232582.000000
Throughput: 212323258.200000
Num Accesses - Session Mean & Stddev: 21232325.820000,66.286858
Num Phases Traversed: 15964254.000000
Num Phases Traversed - Session Mean & Stddev: 159642.540000,0.498397
Num Path Iterations: 15964254.000000
Num Path Iterations - Session Mean & Stddev: 159642.540000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2823600000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2107241061.000000
Throughput: 210724106.100000
Num Accesses - Session Mean & Stddev: 21072410.610000,67.960267
Num Phases Traversed: 15844017.000000
Num Phases Traversed - Session Mean & Stddev: 158440.170000,0.510979
Num Path Iterations: 15844017.000000
Num Path Iterations - Session Mean & Stddev: 158440.170000,0.510979
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9708a00000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2118304134.000000
Throughput: 211830413.400000
Num Accesses - Session Mean & Stddev: 21183041.340000,70.326698
Num Phases Traversed: 15927198.000000
Num Phases Traversed - Session Mean & Stddev: 159271.980000,0.528772
Num Path Iterations: 15927198.000000
Num Path Iterations - Session Mean & Stddev: 159271.980000,0.528772
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f30c4a00000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2123478632.000000
Throughput: 212347863.200000
Num Accesses - Session Mean & Stddev: 21234786.320000,70.175620
Num Phases Traversed: 15966104.000000
Num Phases Traversed - Session Mean & Stddev: 159661.040000,0.527636
Num Path Iterations: 15966104.000000
Num Path Iterations - Session Mean & Stddev: 159661.040000,0.527636
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0b2b800000 of size 12288 bytes.
[1.227s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1172326605.000000
Throughput: 117232660.500000
Num Accesses - Session Mean & Stddev: 11723266.050000,94.747916
Num Phases Traversed: 8814585.000000
Num Phases Traversed - Session Mean & Stddev: 88145.850000,0.712390
Num Path Iterations: 8814585.000000
Num Path Iterations - Session Mean & Stddev: 88145.850000,0.712390
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb4c8a00000 of size 12288 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2127016432.000000
Throughput: 212701643.200000
Num Accesses - Session Mean & Stddev: 21270164.320000,81.813921
Num Phases Traversed: 15992704.000000
Num Phases Traversed - Session Mean & Stddev: 159927.040000,0.615142
Num Path Iterations: 15992704.000000
Num Path Iterations - Session Mean & Stddev: 159927.040000,0.615142
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f45d9200000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2125509409.000000
Throughput: 212550940.900000
Num Accesses - Session Mean & Stddev: 21255094.090000,59.046608
Num Phases Traversed: 15981373.000000
Num Phases Traversed - Session Mean & Stddev: 159813.730000,0.443959
Num Path Iterations: 15981373.000000
Num Path Iterations - Session Mean & Stddev: 159813.730000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffb0aa00000 of size 12288 bytes.
[1.256s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2120601443.000000
Throughput: 212060144.300000
Num Accesses - Session Mean & Stddev: 21206014.430000,63.213488
Num Phases Traversed: 15944471.000000
Num Phases Traversed - Session Mean & Stddev: 159444.710000,0.475289
Num Path Iterations: 15944471.000000
Num Path Iterations - Session Mean & Stddev: 159444.710000,0.475289
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f132e600000 of size 12288 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2116056700.000000
Throughput: 211605670.000000
Num Accesses - Session Mean & Stddev: 21160567.000000,72.847100
Num Phases Traversed: 15910300.000000
Num Phases Traversed - Session Mean & Stddev: 159103.000000,0.547723
Num Path Iterations: 15910300.000000
Num Path Iterations - Session Mean & Stddev: 159103.000000,0.547723
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/96,0 0/96,0' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/96,0 0/96,0 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    96 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f98ebe00000 of size 12288 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1167195066.000000
Throughput: 116719506.600000
Num Accesses - Session Mean & Stddev: 11671950.660000,97.698436
Num Phases Traversed: 8776002.000000
Num Phases Traversed - Session Mean & Stddev: 87760.020000,0.734575
Num Path Iterations: 8776002.000000
Num Path Iterations - Session Mean & Stddev: 87760.020000,0.734575
+ set +x

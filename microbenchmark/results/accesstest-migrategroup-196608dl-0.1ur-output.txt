++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0e00000000 of size 12582912 bytes.
[2.126s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 697237790.000000
Throughput: 69723779.000000
Num Accesses - Session Mean & Stddev: 6972377.900000,58179.320195
Num Phases Traversed: 7190.000000
Num Phases Traversed - Session Mean & Stddev: 71.900000,0.591608
Num Path Iterations: 7190.000000
Num Path Iterations - Session Mean & Stddev: 71.900000,0.591608
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8740000000 of size 12582912 bytes.
[2.130s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 704416683.000000
Throughput: 70441668.300000
Num Accesses - Session Mean & Stddev: 7044166.830000,47479.458471
Num Phases Traversed: 7263.000000
Num Phases Traversed - Session Mean & Stddev: 72.630000,0.482804
Num Path Iterations: 7263.000000
Num Path Iterations - Session Mean & Stddev: 72.630000,0.482804
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0cc0000000 of size 12582912 bytes.
[2.117s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 701564794.000000
Throughput: 70156479.400000
Num Accesses - Session Mean & Stddev: 7015647.940000,52444.435632
Num Phases Traversed: 7234.000000
Num Phases Traversed - Session Mean & Stddev: 72.340000,0.533292
Num Path Iterations: 7234.000000
Num Path Iterations - Session Mean & Stddev: 72.340000,0.533292
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4a40000000 of size 12582912 bytes.
[2.126s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 700974748.000000
Throughput: 70097474.800000
Num Accesses - Session Mean & Stddev: 7009747.480000,55768.972140
Num Phases Traversed: 7228.000000
Num Phases Traversed - Session Mean & Stddev: 72.280000,0.567098
Num Path Iterations: 7228.000000
Num Path Iterations - Session Mean & Stddev: 72.280000,0.567098
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1b80000000 of size 12582912 bytes.
[2.097s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 726838431.000000
Throughput: 72683843.100000
Num Accesses - Session Mean & Stddev: 7268384.310000,66835.715442
Num Phases Traversed: 7491.000000
Num Phases Traversed - Session Mean & Stddev: 74.910000,0.679632
Num Path Iterations: 7491.000000
Num Path Iterations - Session Mean & Stddev: 74.910000,0.679632
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9340000000 of size 12582912 bytes.
[2.130s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 699007928.000000
Throughput: 69900792.800000
Num Accesses - Session Mean & Stddev: 6990079.280000,35944.947138
Num Phases Traversed: 7208.000000
Num Phases Traversed - Session Mean & Stddev: 72.080000,0.365513
Num Path Iterations: 7208.000000
Num Path Iterations - Session Mean & Stddev: 72.080000,0.365513
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc5c0000000 of size 12582912 bytes.
[2.126s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 700089679.000000
Throughput: 70008967.900000
Num Accesses - Session Mean & Stddev: 7000896.790000,38579.263291
Num Phases Traversed: 7219.000000
Num Phases Traversed - Session Mean & Stddev: 72.190000,0.392301
Num Path Iterations: 7219.000000
Num Path Iterations - Session Mean & Stddev: 72.190000,0.392301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd2c0000000 of size 12582912 bytes.
[2.126s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 703334932.000000
Throughput: 70333493.200000
Num Accesses - Session Mean & Stddev: 7033349.320000,49131.147853
Num Phases Traversed: 7252.000000
Num Phases Traversed - Session Mean & Stddev: 72.520000,0.499600
Num Path Iterations: 7252.000000
Num Path Iterations - Session Mean & Stddev: 72.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff900000000 of size 12582912 bytes.
[2.177s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 700679725.000000
Throughput: 70067972.500000
Num Accesses - Session Mean & Stddev: 7006797.250000,61216.778818
Num Phases Traversed: 7225.000000
Num Phases Traversed - Session Mean & Stddev: 72.250000,0.622495
Num Path Iterations: 7225.000000
Num Path Iterations - Session Mean & Stddev: 72.250000,0.622495
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6580000000 of size 12582912 bytes.
[2.140s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 699106269.000000
Throughput: 69910626.900000
Num Accesses - Session Mean & Stddev: 6991062.690000,28143.400502
Num Phases Traversed: 7209.000000
Num Phases Traversed - Session Mean & Stddev: 72.090000,0.286182
Num Path Iterations: 7209.000000
Num Path Iterations - Session Mean & Stddev: 72.090000,0.286182
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efcc0000000 of size 12582912 bytes.
[2.137s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 696254380.000000
Throughput: 69625438.000000
Num Accesses - Session Mean & Stddev: 6962543.800000,52037.165936
Num Phases Traversed: 7180.000000
Num Phases Traversed - Session Mean & Stddev: 71.800000,0.529150
Num Path Iterations: 7180.000000
Num Path Iterations - Session Mean & Stddev: 71.800000,0.529150
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3540000000 of size 12582912 bytes.
[2.166s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 700286361.000000
Throughput: 70028636.100000
Num Accesses - Session Mean & Stddev: 7002863.610000,40055.099344
Num Phases Traversed: 7221.000000
Num Phases Traversed - Session Mean & Stddev: 72.210000,0.407308
Num Path Iterations: 7221.000000
Num Path Iterations - Session Mean & Stddev: 72.210000,0.407308
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7e40000000 of size 12582912 bytes.
[2.154s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 704220001.000000
Throughput: 70422000.100000
Num Accesses - Session Mean & Stddev: 7042200.010000,47965.816449
Num Phases Traversed: 7261.000000
Num Phases Traversed - Session Mean & Stddev: 72.610000,0.487750
Num Path Iterations: 7261.000000
Num Path Iterations - Session Mean & Stddev: 72.610000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa700000000 of size 12582912 bytes.
[2.151s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 705596775.000000
Throughput: 70559677.500000
Num Accesses - Session Mean & Stddev: 7055967.750000,42582.902117
Num Phases Traversed: 7275.000000
Num Phases Traversed - Session Mean & Stddev: 72.750000,0.433013
Num Path Iterations: 7275.000000
Num Path Iterations - Session Mean & Stddev: 72.750000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa740000000 of size 12582912 bytes.
[2.150s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 698614564.000000
Throughput: 69861456.400000
Num Accesses - Session Mean & Stddev: 6986145.640000,63610.741501
Num Phases Traversed: 7204.000000
Num Phases Traversed - Session Mean & Stddev: 72.040000,0.646838
Num Path Iterations: 7204.000000
Num Path Iterations - Session Mean & Stddev: 72.040000,0.646838
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcc80000000 of size 12582912 bytes.
[2.163s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 699696315.000000
Throughput: 69969631.500000
Num Accesses - Session Mean & Stddev: 6996963.150000,42582.902117
Num Phases Traversed: 7215.000000
Num Phases Traversed - Session Mean & Stddev: 72.150000,0.433013
Num Path Iterations: 7215.000000
Num Path Iterations - Session Mean & Stddev: 72.150000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9c80000000 of size 12582912 bytes.
[2.142s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 699991338.000000
Throughput: 69999133.800000
Num Accesses - Session Mean & Stddev: 6999913.380000,37781.378438
Num Phases Traversed: 7218.000000
Num Phases Traversed - Session Mean & Stddev: 72.180000,0.384187
Num Path Iterations: 7218.000000
Num Path Iterations - Session Mean & Stddev: 72.180000,0.384187
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faa00000000 of size 12582912 bytes.
[2.139s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 732345527.000000
Throughput: 73234552.700000
Num Accesses - Session Mean & Stddev: 7323455.270000,49081.913300
Num Phases Traversed: 7547.000000
Num Phases Traversed - Session Mean & Stddev: 75.470000,0.499099
Num Path Iterations: 7547.000000
Num Path Iterations - Session Mean & Stddev: 75.470000,0.499099
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd8c0000000 of size 12582912 bytes.
[2.125s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 697729495.000000
Throughput: 69772949.500000
Num Accesses - Session Mean & Stddev: 6977294.950000,65785.645438
Num Phases Traversed: 7195.000000
Num Phases Traversed - Session Mean & Stddev: 71.950000,0.668954
Num Path Iterations: 7195.000000
Num Path Iterations - Session Mean & Stddev: 71.950000,0.668954
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0.1 0/98304,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0.1 0/98304,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f19c0000000 of size 12582912 bytes.
[2.134s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 699401292.000000
Throughput: 69940129.200000
Num Accesses - Session Mean & Stddev: 6994012.920000,31957.042430
Num Phases Traversed: 7212.000000
Num Phases Traversed - Session Mean & Stddev: 72.120000,0.324962
Num Path Iterations: 7212.000000
Num Path Iterations - Session Mean & Stddev: 72.120000,0.324962
+ set +x

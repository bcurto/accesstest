++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5a05800000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3045904597.000000
Throughput: 304590459.700000
Num Accesses - Session Mean & Stddev: 30459045.970000,152305.754095
Num Phases Traversed: 13300993.000000
Num Phases Traversed - Session Mean & Stddev: 133009.930000,665.090629
Num Path Iterations: 13300993.000000
Num Path Iterations - Session Mean & Stddev: 133009.930000,665.090629
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff85de00000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3046016578.000000
Throughput: 304601657.800000
Num Accesses - Session Mean & Stddev: 30460165.780000,146404.474355
Num Phases Traversed: 13301482.000000
Num Phases Traversed - Session Mean & Stddev: 133014.820000,639.320849
Num Path Iterations: 13301482.000000
Num Path Iterations - Session Mean & Stddev: 133014.820000,639.320849
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe9e9600000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3046762431.000000
Throughput: 304676243.100000
Num Accesses - Session Mean & Stddev: 30467624.310000,169361.696121
Num Phases Traversed: 13304739.000000
Num Phases Traversed - Session Mean & Stddev: 133047.390000,739.570725
Num Path Iterations: 13304739.000000
Num Path Iterations - Session Mean & Stddev: 133047.390000,739.570725
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f576be00000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3044315566.000000
Throughput: 304431556.600000
Num Accesses - Session Mean & Stddev: 30443155.660000,162466.504102
Num Phases Traversed: 13294054.000000
Num Phases Traversed - Session Mean & Stddev: 132940.540000,709.460717
Num Path Iterations: 13294054.000000
Num Path Iterations - Session Mean & Stddev: 132940.540000,709.460717
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb7f5600000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3044907073.000000
Throughput: 304490707.300000
Num Accesses - Session Mean & Stddev: 30449070.730000,161539.069360
Num Phases Traversed: 13296637.000000
Num Phases Traversed - Session Mean & Stddev: 132966.370000,705.410783
Num Path Iterations: 13296637.000000
Num Path Iterations - Session Mean & Stddev: 132966.370000,705.410783
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3d7ae00000 of size 24576 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3037622125.000000
Throughput: 303762212.500000
Num Accesses - Session Mean & Stddev: 30376221.250000,56950.503284
Num Phases Traversed: 13264825.000000
Num Phases Traversed - Session Mean & Stddev: 132648.250000,248.692154
Num Path Iterations: 13264825.000000
Num Path Iterations - Session Mean & Stddev: 132648.250000,248.692154
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f618d000000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3046060088.000000
Throughput: 304606008.800000
Num Accesses - Session Mean & Stddev: 30460600.880000,181519.317096
Num Phases Traversed: 13301672.000000
Num Phases Traversed - Session Mean & Stddev: 133016.720000,792.660773
Num Path Iterations: 13301672.000000
Num Path Iterations - Session Mean & Stddev: 133016.720000,792.660773
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6e49800000 of size 24576 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3033122733.000000
Throughput: 303312273.300000
Num Accesses - Session Mean & Stddev: 30331227.330000,30061.391897
Num Phases Traversed: 13245177.000000
Num Phases Traversed - Session Mean & Stddev: 132451.770000,131.272454
Num Path Iterations: 13245177.000000
Num Path Iterations - Session Mean & Stddev: 132451.770000,131.272454
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbd9d800000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3045172026.000000
Throughput: 304517202.600000
Num Accesses - Session Mean & Stddev: 30451720.260000,169821.955872
Num Phases Traversed: 13297794.000000
Num Phases Traversed - Session Mean & Stddev: 132977.940000,741.580593
Num Path Iterations: 13297794.000000
Num Path Iterations - Session Mean & Stddev: 132977.940000,741.580593
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f13ec400000 of size 24576 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3032724502.000000
Throughput: 303272450.200000
Num Accesses - Session Mean & Stddev: 30327245.020000,2095.263654
Num Phases Traversed: 13243438.000000
Num Phases Traversed - Session Mean & Stddev: 132434.380000,9.149623
Num Path Iterations: 13243438.000000
Num Path Iterations - Session Mean & Stddev: 132434.380000,9.149623
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2c05200000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3045054091.000000
Throughput: 304505409.100000
Num Accesses - Session Mean & Stddev: 30450540.910000,183495.567441
Num Phases Traversed: 13297279.000000
Num Phases Traversed - Session Mean & Stddev: 132972.790000,801.290688
Num Path Iterations: 13297279.000000
Num Path Iterations - Session Mean & Stddev: 132972.790000,801.290688
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe909600000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3046496104.000000
Throughput: 304649610.400000
Num Accesses - Session Mean & Stddev: 30464961.040000,170916.604579
Num Phases Traversed: 13303576.000000
Num Phases Traversed - Session Mean & Stddev: 133035.760000,746.360719
Num Path Iterations: 13303576.000000
Num Path Iterations - Session Mean & Stddev: 133035.760000,746.360719
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8b69c00000 of size 24576 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3019602115.000000
Throughput: 301960211.500000
Num Accesses - Session Mean & Stddev: 30196021.150000,54170.368694
Num Phases Traversed: 13186135.000000
Num Phases Traversed - Session Mean & Stddev: 131861.350000,236.551828
Num Path Iterations: 13186135.000000
Num Path Iterations - Session Mean & Stddev: 131861.350000,236.551828
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f56e5400000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3049176091.000000
Throughput: 304917609.100000
Num Accesses - Session Mean & Stddev: 30491760.910000,168505.181570
Num Phases Traversed: 13315279.000000
Num Phases Traversed - Session Mean & Stddev: 133152.790000,735.830487
Num Path Iterations: 13315279.000000
Num Path Iterations - Session Mean & Stddev: 133152.790000,735.830487
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff184a00000 of size 24576 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3034376966.000000
Throughput: 303437696.600000
Num Accesses - Session Mean & Stddev: 30343769.660000,69039.291437
Num Phases Traversed: 13250654.000000
Num Phases Traversed - Session Mean & Stddev: 132506.540000,301.481622
Num Path Iterations: 13250654.000000
Num Path Iterations - Session Mean & Stddev: 132506.540000,301.481622
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f85ab800000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3044682195.000000
Throughput: 304468219.500000
Num Accesses - Session Mean & Stddev: 30446821.950000,163636.713086
Num Phases Traversed: 13295655.000000
Num Phases Traversed - Session Mean & Stddev: 132956.550000,714.570800
Num Path Iterations: 13295655.000000
Num Path Iterations - Session Mean & Stddev: 132956.550000,714.570800
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fae38200000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3041478027.000000
Throughput: 304147802.700000
Num Accesses - Session Mean & Stddev: 30414780.270000,159954.372777
Num Phases Traversed: 13281663.000000
Num Phases Traversed - Session Mean & Stddev: 132816.630000,698.490711
Num Path Iterations: 13281663.000000
Num Path Iterations - Session Mean & Stddev: 132816.630000,698.490711
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff57a800000 of size 24576 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3047526375.000000
Throughput: 304752637.500000
Num Accesses - Session Mean & Stddev: 30475263.750000,233747.281569
Num Phases Traversed: 13308075.000000
Num Phases Traversed - Session Mean & Stddev: 133080.750000,1020.730487
Num Path Iterations: 13308075.000000
Num Path Iterations - Session Mean & Stddev: 133080.750000,1020.730487
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5b6ba00000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3044713339.000000
Throughput: 304471333.900000
Num Accesses - Session Mean & Stddev: 30447133.390000,200368.260992
Num Phases Traversed: 13295791.000000
Num Phases Traversed - Session Mean & Stddev: 132957.910000,874.970572
Num Path Iterations: 13295791.000000
Num Path Iterations - Session Mean & Stddev: 132957.910000,874.970572
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/192,0 0/192,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/192,0 0/192,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    192 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7bae400000 of size 24576 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 3049983545.000000
Throughput: 304998354.500000
Num Accesses - Session Mean & Stddev: 30499835.450000,181159.772019
Num Phases Traversed: 13318805.000000
Num Phases Traversed - Session Mean & Stddev: 133188.050000,791.090708
Num Path Iterations: 13318805.000000
Num Path Iterations - Session Mean & Stddev: 133188.050000,791.090708
+ set +x

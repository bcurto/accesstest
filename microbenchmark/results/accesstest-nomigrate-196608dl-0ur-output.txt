++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8740000000 of size 12582912 bytes.
[2.054s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 628595772.000000
Throughput: 62859577.200000
Num Accesses - Session Mean & Stddev: 6285957.720000,95020.003418
Num Phases Traversed: 6492.000000
Num Phases Traversed - Session Mean & Stddev: 64.920000,0.966230
Num Path Iterations: 6492.000000
Num Path Iterations - Session Mean & Stddev: 64.920000,0.966230
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2840000000 of size 12582912 bytes.
[2.075s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 628595772.000000
Throughput: 62859577.200000
Num Accesses - Session Mean & Stddev: 6285957.720000,92962.158744
Num Phases Traversed: 6492.000000
Num Phases Traversed - Session Mean & Stddev: 64.920000,0.945304
Num Path Iterations: 6492.000000
Num Path Iterations - Session Mean & Stddev: 64.920000,0.945304
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2600000000 of size 12582912 bytes.
[2.055s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 626038906.000000
Throughput: 62603890.600000
Num Accesses - Session Mean & Stddev: 6260389.060000,77708.060098
Num Phases Traversed: 6466.000000
Num Phases Traversed - Session Mean & Stddev: 64.660000,0.790190
Num Path Iterations: 6466.000000
Num Path Iterations - Session Mean & Stddev: 64.660000,0.790190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feac0000000 of size 12582912 bytes.
[2.036s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 626825634.000000
Throughput: 62682563.400000
Num Accesses - Session Mean & Stddev: 6268256.340000,83003.532485
Num Phases Traversed: 6474.000000
Num Phases Traversed - Session Mean & Stddev: 64.740000,0.844038
Num Path Iterations: 6474.000000
Num Path Iterations - Session Mean & Stddev: 64.740000,0.844038
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2ac0000000 of size 12582912 bytes.
[2.082s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 629579182.000000
Throughput: 62957918.200000
Num Accesses - Session Mean & Stddev: 6295791.820000,95324.848614
Num Phases Traversed: 6502.000000
Num Phases Traversed - Session Mean & Stddev: 65.020000,0.969330
Num Path Iterations: 6502.000000
Num Path Iterations - Session Mean & Stddev: 65.020000,0.969330
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2380000000 of size 12582912 bytes.
[2.113s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 628005726.000000
Throughput: 62800572.600000
Num Accesses - Session Mean & Stddev: 6280057.260000,90152.472495
Num Phases Traversed: 6486.000000
Num Phases Traversed - Session Mean & Stddev: 64.860000,0.916733
Num Path Iterations: 6486.000000
Num Path Iterations - Session Mean & Stddev: 64.860000,0.916733
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5ac0000000 of size 12582912 bytes.
[2.054s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 640396692.000000
Throughput: 64039669.200000
Num Accesses - Session Mean & Stddev: 6403966.920000,96634.732485
Num Phases Traversed: 6612.000000
Num Phases Traversed - Session Mean & Stddev: 66.120000,0.982649
Num Path Iterations: 6612.000000
Num Path Iterations - Session Mean & Stddev: 66.120000,0.982649
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0940000000 of size 12582912 bytes.
[2.082s] Test is ready to start.
Test duration in range [10.000, 10.004] seconds
Num Accesses: 627218998.000000
Throughput: 62721899.800000
Num Accesses - Session Mean & Stddev: 6272189.980000,81782.825810
Num Phases Traversed: 6478.000000
Num Phases Traversed - Session Mean & Stddev: 64.780000,0.831625
Num Path Iterations: 6478.000000
Num Path Iterations - Session Mean & Stddev: 64.780000,0.831625
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8bc0000000 of size 12582912 bytes.
[2.041s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 638626554.000000
Throughput: 63862655.400000
Num Accesses - Session Mean & Stddev: 6386265.540000,101076.179141
Num Phases Traversed: 6594.000000
Num Phases Traversed - Session Mean & Stddev: 65.940000,1.027813
Num Path Iterations: 6594.000000
Num Path Iterations - Session Mean & Stddev: 65.940000,1.027813
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6f40000000 of size 12582912 bytes.
[2.051s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 640593374.000000
Throughput: 64059337.400000
Num Accesses - Session Mean & Stddev: 6405933.740000,90152.472495
Num Phases Traversed: 6614.000000
Num Phases Traversed - Session Mean & Stddev: 66.140000,0.916733
Num Path Iterations: 6614.000000
Num Path Iterations - Session Mean & Stddev: 66.140000,0.916733
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fefc0000000 of size 12582912 bytes.
[2.068s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 639806646.000000
Throughput: 63980664.600000
Num Accesses - Session Mean & Stddev: 6398066.460000,88855.868924
Num Phases Traversed: 6606.000000
Num Phases Traversed - Session Mean & Stddev: 66.060000,0.903549
Num Path Iterations: 6606.000000
Num Path Iterations - Session Mean & Stddev: 66.060000,0.903549
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2800000000 of size 12582912 bytes.
[2.058s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 635873006.000000
Throughput: 63587300.600000
Num Accesses - Session Mean & Stddev: 6358730.060000,107024.650813
Num Phases Traversed: 6566.000000
Num Phases Traversed - Session Mean & Stddev: 65.660000,1.088301
Num Path Iterations: 6566.000000
Num Path Iterations - Session Mean & Stddev: 65.660000,1.088301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc300000000 of size 12582912 bytes.
[2.051s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 643150240.000000
Throughput: 64315024.000000
Num Accesses - Session Mean & Stddev: 6431502.400000,63732.252102
Num Phases Traversed: 6640.000000
Num Phases Traversed - Session Mean & Stddev: 66.400000,0.648074
Num Path Iterations: 6640.000000
Num Path Iterations - Session Mean & Stddev: 66.400000,0.648074
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9680000000 of size 12582912 bytes.
[2.064s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 635873006.000000
Throughput: 63587300.600000
Num Accesses - Session Mean & Stddev: 6358730.060000,108816.882757
Num Phases Traversed: 6566.000000
Num Phases Traversed - Session Mean & Stddev: 65.660000,1.106526
Num Path Iterations: 6566.000000
Num Path Iterations - Session Mean & Stddev: 65.660000,1.106526
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd640000000 of size 12582912 bytes.
[2.061s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 644330332.000000
Throughput: 64433033.200000
Num Accesses - Session Mean & Stddev: 6443303.320000,51061.617042
Num Phases Traversed: 6652.000000
Num Phases Traversed - Session Mean & Stddev: 66.520000,0.519230
Num Path Iterations: 6652.000000
Num Path Iterations - Session Mean & Stddev: 66.520000,0.519230
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7c40000000 of size 12582912 bytes.
[2.042s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 654951160.000000
Throughput: 65495116.000000
Num Accesses - Session Mean & Stddev: 6549511.600000,111260.140750
Num Phases Traversed: 6760.000000
Num Phases Traversed - Session Mean & Stddev: 67.600000,1.131371
Num Path Iterations: 6760.000000
Num Path Iterations - Session Mean & Stddev: 67.600000,1.131371
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8340000000 of size 12582912 bytes.
[2.030s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 656524616.000000
Throughput: 65652461.600000
Num Accesses - Session Mean & Stddev: 6565246.160000,109613.827202
Num Phases Traversed: 6776.000000
Num Phases Traversed - Session Mean & Stddev: 67.760000,1.114630
Num Path Iterations: 6776.000000
Num Path Iterations - Session Mean & Stddev: 67.760000,1.114630
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8100000000 of size 12582912 bytes.
[2.093s] Test is ready to start.
Test duration in range [10.000, 10.003] seconds
Num Accesses: 636069688.000000
Throughput: 63606968.800000
Num Accesses - Session Mean & Stddev: 6360696.880000,103028.332140
Num Phases Traversed: 6568.000000
Num Phases Traversed - Session Mean & Stddev: 65.680000,1.047664
Num Path Iterations: 6568.000000
Num Path Iterations - Session Mean & Stddev: 65.680000,1.047664
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb840000000 of size 12582912 bytes.
[2.038s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 633906186.000000
Throughput: 63390618.600000
Num Accesses - Session Mean & Stddev: 6339061.860000,107205.222311
Num Phases Traversed: 6546.000000
Num Phases Traversed - Session Mean & Stddev: 65.460000,1.090138
Num Path Iterations: 6546.000000
Num Path Iterations - Session Mean & Stddev: 65.460000,1.090138
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/98304,0 0/98304,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/98304,0 0/98304,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    98304 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2d00000000 of size 12582912 bytes.
[2.083s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 627022316.000000
Throughput: 62702231.600000
Num Accesses - Session Mean & Stddev: 6270223.160000,81237.047192
Num Phases Traversed: 6476.000000
Num Phases Traversed - Session Mean & Stddev: 64.760000,0.826075
Num Path Iterations: 6476.000000
Num Path Iterations - Session Mean & Stddev: 64.760000,0.826075
+ set +x

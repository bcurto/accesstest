++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8b24800000 of size 77824 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086813105.000000
Throughput: 208681310.500000
Num Accesses - Session Mean & Stddev: 20868131.050000,298.308142
Num Phases Traversed: 3235469.000000
Num Phases Traversed - Session Mean & Stddev: 32354.690000,0.462493
Num Path Iterations: 3235469.000000
Num Path Iterations - Session Mean & Stddev: 32354.690000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb31d600000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086635085.000000
Throughput: 208663508.500000
Num Accesses - Session Mean & Stddev: 20866350.850000,228.133135
Num Phases Traversed: 3235193.000000
Num Phases Traversed - Session Mean & Stddev: 32351.930000,0.353695
Num Path Iterations: 3235193.000000
Num Path Iterations - Session Mean & Stddev: 32351.930000,0.353695
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f20ea000000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2087182045.000000
Throughput: 208718204.500000
Num Accesses - Session Mean & Stddev: 20871820.450000,317.232482
Num Phases Traversed: 3236041.000000
Num Phases Traversed - Session Mean & Stddev: 32360.410000,0.491833
Num Path Iterations: 3236041.000000
Num Path Iterations - Session Mean & Stddev: 32360.410000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd08ee00000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2086423525.000000
Throughput: 208642352.500000
Num Accesses - Session Mean & Stddev: 20864235.250000,307.645392
Num Phases Traversed: 3234865.000000
Num Phases Traversed - Session Mean & Stddev: 32348.650000,0.476970
Num Path Iterations: 3234865.000000
Num Path Iterations - Session Mean & Stddev: 32348.650000,0.476970
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f248b800000 of size 77824 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2089659490.000000
Throughput: 208965949.000000
Num Accesses - Session Mean & Stddev: 20896594.900000,247.800908
Num Phases Traversed: 3239882.000000
Num Phases Traversed - Session Mean & Stddev: 32398.820000,0.384187
Num Path Iterations: 3239882.000000
Num Path Iterations - Session Mean & Stddev: 32398.820000,0.384187
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0b2a200000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2087098840.000000
Throughput: 208709884.000000
Num Accesses - Session Mean & Stddev: 20870988.400000,228.588582
Num Phases Traversed: 3235912.000000
Num Phases Traversed - Session Mean & Stddev: 32359.120000,0.354401
Num Path Iterations: 3235912.000000
Num Path Iterations - Session Mean & Stddev: 32359.120000,0.354401
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f591b000000 of size 77824 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086457065.000000
Throughput: 208645706.500000
Num Accesses - Session Mean & Stddev: 20864570.650000,242.283156
Num Phases Traversed: 3234917.000000
Num Phases Traversed - Session Mean & Stddev: 32349.170000,0.375633
Num Path Iterations: 3234917.000000
Num Path Iterations - Session Mean & Stddev: 32349.170000,0.375633
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5ea0800000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2087775445.000000
Throughput: 208777544.500000
Num Accesses - Session Mean & Stddev: 20877754.450000,314.598709
Num Phases Traversed: 3236961.000000
Num Phases Traversed - Session Mean & Stddev: 32369.610000,0.487750
Num Path Iterations: 3236961.000000
Num Path Iterations - Session Mean & Stddev: 32369.610000,0.487750
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffa5d000000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086035880.000000
Throughput: 208603588.000000
Num Accesses - Session Mean & Stddev: 20860358.800000,309.600000
Num Phases Traversed: 3234264.000000
Num Phases Traversed - Session Mean & Stddev: 32342.640000,0.480000
Num Path Iterations: 3234264.000000
Num Path Iterations - Session Mean & Stddev: 32342.640000,0.480000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd9fe400000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2085864955.000000
Throughput: 208586495.500000
Num Accesses - Session Mean & Stddev: 20858649.550000,232.468595
Num Phases Traversed: 3233999.000000
Num Phases Traversed - Session Mean & Stddev: 32339.990000,0.360416
Num Path Iterations: 3233999.000000
Num Path Iterations - Session Mean & Stddev: 32339.990000,0.360416
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f072c200000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2090730190.000000
Throughput: 209073019.000000
Num Accesses - Session Mean & Stddev: 20907301.900000,318.345237
Num Phases Traversed: 3241542.000000
Num Phases Traversed - Session Mean & Stddev: 32415.420000,0.493559
Num Path Iterations: 3241542.000000
Num Path Iterations - Session Mean & Stddev: 32415.420000,0.493559
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe17ac00000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2087264605.000000
Throughput: 208726460.500000
Num Accesses - Session Mean & Stddev: 20872646.050000,298.308142
Num Phases Traversed: 3236169.000000
Num Phases Traversed - Session Mean & Stddev: 32361.690000,0.462493
Num Path Iterations: 3236169.000000
Num Path Iterations - Session Mean & Stddev: 32361.690000,0.462493
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe04a800000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2090963680.000000
Throughput: 209096368.000000
Num Accesses - Session Mean & Stddev: 20909636.800000,221.939992
Num Phases Traversed: 3241904.000000
Num Phases Traversed - Session Mean & Stddev: 32419.040000,0.344093
Num Path Iterations: 3241904.000000
Num Path Iterations - Session Mean & Stddev: 32419.040000,0.344093
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcf0ee00000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2084233750.000000
Throughput: 208423375.000000
Num Accesses - Session Mean & Stddev: 20842337.500000,295.576132
Num Phases Traversed: 3231470.000000
Num Phases Traversed - Session Mean & Stddev: 32314.700000,0.458258
Num Path Iterations: 3231470.000000
Num Path Iterations - Session Mean & Stddev: 32314.700000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7809200000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2083428790.000000
Throughput: 208342879.000000
Num Accesses - Session Mean & Stddev: 20834287.900000,267.188866
Num Phases Traversed: 3230222.000000
Num Phases Traversed - Session Mean & Stddev: 32302.220000,0.414246
Num Path Iterations: 3230222.000000
Num Path Iterations - Session Mean & Stddev: 32302.220000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faa4ae00000 of size 77824 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2084474980.000000
Throughput: 208447498.000000
Num Accesses - Session Mean & Stddev: 20844749.800000,320.169580
Num Phases Traversed: 3231844.000000
Num Phases Traversed - Session Mean & Stddev: 32318.440000,0.496387
Num Path Iterations: 3231844.000000
Num Path Iterations - Session Mean & Stddev: 32318.440000,0.496387
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faab6000000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086925335.000000
Throughput: 208692533.500000
Num Accesses - Session Mean & Stddev: 20869253.350000,319.323860
Num Phases Traversed: 3235643.000000
Num Phases Traversed - Session Mean & Stddev: 32356.430000,0.495076
Num Path Iterations: 3235643.000000
Num Path Iterations - Session Mean & Stddev: 32356.430000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2c53600000 of size 77824 bytes.
[1.253s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086153270.000000
Throughput: 208615327.000000
Num Accesses - Session Mean & Stddev: 20861532.700000,321.466343
Num Phases Traversed: 3234446.000000
Num Phases Traversed - Session Mean & Stddev: 32344.460000,0.498397
Num Path Iterations: 3234446.000000
Num Path Iterations - Session Mean & Stddev: 32344.460000,0.498397
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0104c00000 of size 77824 bytes.
[1.263s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2086027495.000000
Throughput: 208602749.500000
Num Accesses - Session Mean & Stddev: 20860274.950000,322.435494
Num Phases Traversed: 3234251.000000
Num Phases Traversed - Session Mean & Stddev: 32342.510000,0.499900
Num Path Iterations: 3234251.000000
Num Path Iterations - Session Mean & Stddev: 32342.510000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/608,0.001 0/608,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/608,0.001 0/608,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    608 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f454b400000 of size 77824 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2086347415.000000
Throughput: 208634741.500000
Num Accesses - Session Mean & Stddev: 20863474.150000,321.918977
Num Phases Traversed: 3234747.000000
Num Phases Traversed - Session Mean & Stddev: 32347.470000,0.499099
Num Path Iterations: 3234747.000000
Num Path Iterations - Session Mean & Stddev: 32347.470000,0.499099
+ set +x

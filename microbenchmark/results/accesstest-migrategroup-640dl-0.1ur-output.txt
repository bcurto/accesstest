++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff2f6600000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2798029726.000000
Throughput: 279802972.600000
Num Accesses - Session Mean & Stddev: 27980297.260000,233.446337
Num Phases Traversed: 7837718.000000
Num Phases Traversed - Session Mean & Stddev: 78377.180000,0.653911
Num Path Iterations: 7837718.000000
Num Path Iterations - Session Mean & Stddev: 78377.180000,0.653911
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fef87c00000 of size 40960 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2816227087.000000
Throughput: 281622708.700000
Num Accesses - Session Mean & Stddev: 28162270.870000,134.480531
Num Phases Traversed: 7888691.000000
Num Phases Traversed - Session Mean & Stddev: 78886.910000,0.376696
Num Path Iterations: 7888691.000000
Num Path Iterations - Session Mean & Stddev: 78886.910000,0.376696
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f55f6600000 of size 40960 bytes.
[1.269s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2805315739.000000
Throughput: 280531573.900000
Num Accesses - Session Mean & Stddev: 28053157.390000,166.340548
Num Phases Traversed: 7858127.000000
Num Phases Traversed - Session Mean & Stddev: 78581.270000,0.465940
Num Path Iterations: 7858127.000000
Num Path Iterations - Session Mean & Stddev: 78581.270000,0.465940
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f456fc00000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2816347039.000000
Throughput: 281634703.900000
Num Accesses - Session Mean & Stddev: 28163470.390000,158.493526
Num Phases Traversed: 7889027.000000
Num Phases Traversed - Session Mean & Stddev: 78890.270000,0.443959
Num Path Iterations: 7889027.000000
Num Path Iterations - Session Mean & Stddev: 78890.270000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdbf8e00000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2813952283.000000
Throughput: 281395228.300000
Num Accesses - Session Mean & Stddev: 28139522.830000,140.051423
Num Phases Traversed: 7882319.000000
Num Phases Traversed - Session Mean & Stddev: 78823.190000,0.392301
Num Path Iterations: 7882319.000000
Num Path Iterations - Session Mean & Stddev: 78823.190000,0.392301
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6917c00000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2809095298.000000
Throughput: 280909529.800000
Num Accesses - Session Mean & Stddev: 28090952.980000,123.874370
Num Phases Traversed: 7868714.000000
Num Phases Traversed - Session Mean & Stddev: 78687.140000,0.346987
Num Path Iterations: 7868714.000000
Num Path Iterations - Session Mean & Stddev: 78687.140000,0.346987
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbdc3600000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2808269914.000000
Throughput: 280826991.400000
Num Accesses - Session Mean & Stddev: 28082699.140000,174.747762
Num Phases Traversed: 7866402.000000
Num Phases Traversed - Session Mean & Stddev: 78664.020000,0.489490
Num Path Iterations: 7866402.000000
Num Path Iterations - Session Mean & Stddev: 78664.020000,0.489490
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1e3f400000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2805244339.000000
Throughput: 280524433.900000
Num Accesses - Session Mean & Stddev: 28052443.390000,158.493526
Num Phases Traversed: 7857927.000000
Num Phases Traversed - Session Mean & Stddev: 78579.270000,0.443959
Num Path Iterations: 7857927.000000
Num Path Iterations - Session Mean & Stddev: 78579.270000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fea41c00000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2807127157.000000
Throughput: 280712715.700000
Num Accesses - Session Mean & Stddev: 28071271.570000,138.219409
Num Phases Traversed: 7863201.000000
Num Phases Traversed - Session Mean & Stddev: 78632.010000,0.387169
Num Path Iterations: 7863201.000000
Num Path Iterations - Session Mean & Stddev: 78632.010000,0.387169
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6448e00000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2804341129.000000
Throughput: 280434112.900000
Num Accesses - Session Mean & Stddev: 28043411.290000,146.804720
Num Phases Traversed: 7855397.000000
Num Phases Traversed - Session Mean & Stddev: 78553.970000,0.411218
Num Path Iterations: 7855397.000000
Num Path Iterations - Session Mean & Stddev: 78553.970000,0.411218
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f91a9800000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2832755830.000000
Throughput: 283275583.000000
Num Accesses - Session Mean & Stddev: 28327558.300000,118.403505
Num Phases Traversed: 7934990.000000
Num Phases Traversed - Session Mean & Stddev: 79349.900000,0.331662
Num Path Iterations: 7934990.000000
Num Path Iterations - Session Mean & Stddev: 79349.900000,0.331662
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efcd6600000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2818871386.000000
Throughput: 281887138.600000
Num Accesses - Session Mean & Stddev: 28188713.860000,71.042103
Num Phases Traversed: 7896098.000000
Num Phases Traversed - Session Mean & Stddev: 78960.980000,0.198997
Num Path Iterations: 7896098.000000
Num Path Iterations - Session Mean & Stddev: 78960.980000,0.198997
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3ae4a00000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2790929710.000000
Throughput: 279092971.000000
Num Accesses - Session Mean & Stddev: 27909297.100000,171.211185
Num Phases Traversed: 7817830.000000
Num Phases Traversed - Session Mean & Stddev: 78178.300000,0.479583
Num Path Iterations: 7817830.000000
Num Path Iterations - Session Mean & Stddev: 78178.300000,0.479583
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff556400000 of size 40960 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2809057813.000000
Throughput: 280905781.300000
Num Accesses - Session Mean & Stddev: 28090578.130000,160.411823
Num Phases Traversed: 7868609.000000
Num Phases Traversed - Session Mean & Stddev: 78686.090000,0.449333
Num Path Iterations: 7868609.000000
Num Path Iterations - Session Mean & Stddev: 78686.090000,0.449333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe2a7800000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2793793564.000000
Throughput: 279379356.400000
Num Accesses - Session Mean & Stddev: 27937935.640000,178.357143
Num Phases Traversed: 7825852.000000
Num Phases Traversed - Session Mean & Stddev: 78258.520000,0.499600
Num Path Iterations: 7825852.000000
Num Path Iterations - Session Mean & Stddev: 78258.520000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f93fd600000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2809685776.000000
Throughput: 280968577.600000
Num Accesses - Session Mean & Stddev: 28096857.760000,166.531986
Num Phases Traversed: 7870368.000000
Num Phases Traversed - Session Mean & Stddev: 78703.680000,0.466476
Num Path Iterations: 7870368.000000
Num Path Iterations - Session Mean & Stddev: 78703.680000,0.466476
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faf1f000000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2831180746.000000
Throughput: 283118074.600000
Num Accesses - Session Mean & Stddev: 28311807.460000,147.885930
Num Phases Traversed: 7930578.000000
Num Phases Traversed - Session Mean & Stddev: 79305.780000,0.414246
Num Path Iterations: 7930578.000000
Num Path Iterations - Session Mean & Stddev: 79305.780000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe9c5600000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2812050901.000000
Throughput: 281205090.100000
Num Accesses - Session Mean & Stddev: 28120509.010000,161.678044
Num Phases Traversed: 7876993.000000
Num Phases Traversed - Session Mean & Stddev: 78769.930000,0.452880
Num Path Iterations: 7876993.000000
Num Path Iterations - Session Mean & Stddev: 78769.930000,0.452880
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7fcdc00000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2812388980.000000
Throughput: 281238898.000000
Num Accesses - Session Mean & Stddev: 28123889.800000,174.893568
Num Phases Traversed: 7877940.000000
Num Phases Traversed - Session Mean & Stddev: 78779.400000,0.489898
Num Path Iterations: 7877940.000000
Num Path Iterations - Session Mean & Stddev: 78779.400000,0.489898
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/320,0.1 0/320,0.1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/320,0.1 0/320,0.1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    320 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4994200000 of size 40960 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2806761946.000000
Throughput: 280676194.600000
Num Accesses - Session Mean & Stddev: 28067619.460000,171.805670
Num Phases Traversed: 7862178.000000
Num Phases Traversed - Session Mean & Stddev: 78621.780000,0.481248
Num Path Iterations: 7862178.000000
Num Path Iterations - Session Mean & Stddev: 78621.780000,0.481248
+ set +x

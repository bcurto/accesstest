++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbc34c00000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1907458325.000000
Throughput: 190745832.500000
Num Accesses - Session Mean & Stddev: 19074583.250000,43.734283
Num Phases Traversed: 18885825.000000
Num Phases Traversed - Session Mean & Stddev: 188858.250000,0.433013
Num Path Iterations: 18885825.000000
Num Path Iterations - Session Mean & Stddev: 188858.250000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fceafc00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1931954057.000000
Throughput: 193195405.700000
Num Accesses - Session Mean & Stddev: 19319540.570000,50.002651
Num Phases Traversed: 19128357.000000
Num Phases Traversed - Session Mean & Stddev: 191283.570000,0.495076
Num Path Iterations: 19128357.000000
Num Path Iterations - Session Mean & Stddev: 191283.570000,0.495076
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5c5c400000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1922753664.000000
Throughput: 192275366.400000
Num Accesses - Session Mean & Stddev: 19227536.640000,48.480000
Num Phases Traversed: 19037264.000000
Num Phases Traversed - Session Mean & Stddev: 190372.640000,0.480000
Num Path Iterations: 19037264.000000
Num Path Iterations - Session Mean & Stddev: 190372.640000,0.480000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4a31600000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1898674759.000000
Throughput: 189867475.900000
Num Accesses - Session Mean & Stddev: 18986747.590000,49.675164
Num Phases Traversed: 18798859.000000
Num Phases Traversed - Session Mean & Stddev: 187988.590000,0.491833
Num Path Iterations: 18798859.000000
Num Path Iterations - Session Mean & Stddev: 187988.590000,0.491833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ffaafa00000 of size 8192 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1906786978.000000
Throughput: 190678697.800000
Num Accesses - Session Mean & Stddev: 19067869.780000,41.838877
Num Phases Traversed: 18879178.000000
Num Phases Traversed - Session Mean & Stddev: 188791.780000,0.414246
Num Path Iterations: 18879178.000000
Num Path Iterations - Session Mean & Stddev: 188791.780000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0d0d400000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1908296625.000000
Throughput: 190829662.500000
Num Accesses - Session Mean & Stddev: 19082966.250000,43.734283
Num Phases Traversed: 18894125.000000
Num Phases Traversed - Session Mean & Stddev: 188941.250000,0.433013
Num Path Iterations: 18894125.000000
Num Path Iterations - Session Mean & Stddev: 188941.250000,0.433013
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0ac9c00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1928099998.000000
Throughput: 192809999.800000
Num Accesses - Session Mean & Stddev: 19280999.980000,14.140000
Num Phases Traversed: 19090198.000000
Num Phases Traversed - Session Mean & Stddev: 190901.980000,0.140000
Num Path Iterations: 19090198.000000
Num Path Iterations - Session Mean & Stddev: 190901.980000,0.140000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7705200000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1917619834.000000
Throughput: 191761983.400000
Num Accesses - Session Mean & Stddev: 19176198.340000,47.844586
Num Phases Traversed: 18986434.000000
Num Phases Traversed - Session Mean & Stddev: 189864.340000,0.473709
Num Path Iterations: 18986434.000000
Num Path Iterations - Session Mean & Stddev: 189864.340000,0.473709
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb7f0400000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1913550948.000000
Throughput: 191355094.800000
Num Accesses - Session Mean & Stddev: 19135509.480000,50.459584
Num Phases Traversed: 18946148.000000
Num Phases Traversed - Session Mean & Stddev: 189461.480000,0.499600
Num Path Iterations: 18946148.000000
Num Path Iterations - Session Mean & Stddev: 189461.480000,0.499600
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbe89c00000 of size 8192 bytes.
[1.257s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1933135050.000000
Throughput: 193313505.000000
Num Accesses - Session Mean & Stddev: 19331350.500000,50.500000
Num Phases Traversed: 19140050.000000
Num Phases Traversed - Session Mean & Stddev: 191400.500000,0.500000
Num Path Iterations: 19140050.000000
Num Path Iterations - Session Mean & Stddev: 191400.500000,0.500000
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f41e5a00000 of size 8192 bytes.
[1.259s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1920018079.000000
Throughput: 192001807.900000
Num Accesses - Session Mean & Stddev: 19200180.790000,41.138132
Num Phases Traversed: 19010179.000000
Num Phases Traversed - Session Mean & Stddev: 190101.790000,0.407308
Num Path Iterations: 19010179.000000
Num Path Iterations - Session Mean & Stddev: 190101.790000,0.407308
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faa6c200000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1938283626.000000
Throughput: 193828362.600000
Num Accesses - Session Mean & Stddev: 19382836.260000,44.302059
Num Phases Traversed: 19191026.000000
Num Phases Traversed - Session Mean & Stddev: 191910.260000,0.438634
Num Path Iterations: 19191026.000000
Num Path Iterations - Session Mean & Stddev: 191910.260000,0.438634
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f565be00000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1920454298.000000
Throughput: 192045429.800000
Num Accesses - Session Mean & Stddev: 19204542.980000,20.098746
Num Phases Traversed: 19014498.000000
Num Phases Traversed - Session Mean & Stddev: 190144.980000,0.198997
Num Path Iterations: 19014498.000000
Num Path Iterations - Session Mean & Stddev: 190144.980000,0.198997
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f04e7e00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1910994133.000000
Throughput: 191099413.300000
Num Accesses - Session Mean & Stddev: 19109941.330000,47.491484
Num Phases Traversed: 18920833.000000
Num Phases Traversed - Session Mean & Stddev: 189208.330000,0.470213
Num Path Iterations: 18920833.000000
Num Path Iterations - Session Mean & Stddev: 189208.330000,0.470213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f632ba00000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1926926277.000000
Throughput: 192692627.700000
Num Accesses - Session Mean & Stddev: 19269262.770000,42.504083
Num Phases Traversed: 19078577.000000
Num Phases Traversed - Session Mean & Stddev: 190785.770000,0.420833
Num Path Iterations: 19078577.000000
Num Path Iterations - Session Mean & Stddev: 190785.770000,0.420833
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2c9ac00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1909708504.000000
Throughput: 190970850.400000
Num Accesses - Session Mean & Stddev: 19097085.040000,19.791877
Num Phases Traversed: 18908104.000000
Num Phases Traversed - Session Mean & Stddev: 189081.040000,0.195959
Num Path Iterations: 18908104.000000
Num Path Iterations - Session Mean & Stddev: 189081.040000,0.195959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fef0a000000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1921128978.000000
Throughput: 192112897.800000
Num Accesses - Session Mean & Stddev: 19211289.780000,41.838877
Num Phases Traversed: 19021178.000000
Num Phases Traversed - Session Mean & Stddev: 190211.780000,0.414246
Num Path Iterations: 19021178.000000
Num Path Iterations - Session Mean & Stddev: 190211.780000,0.414246
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f64c9a00000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1921215030.000000
Throughput: 192121503.000000
Num Accesses - Session Mean & Stddev: 19212150.300000,46.284015
Num Phases Traversed: 19022030.000000
Num Phases Traversed - Session Mean & Stddev: 190220.300000,0.458258
Num Path Iterations: 19022030.000000
Num Path Iterations - Session Mean & Stddev: 190220.300000,0.458258
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4e2be00000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1925735386.000000
Throughput: 192573538.600000
Num Accesses - Session Mean & Stddev: 19257353.860000,35.045690
Num Phases Traversed: 19066786.000000
Num Phases Traversed - Session Mean & Stddev: 190667.860000,0.346987
Num Path Iterations: 19066786.000000
Num Path Iterations - Session Mean & Stddev: 190667.860000,0.346987
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbb84400000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1897180565.000000
Throughput: 189718056.500000
Num Accesses - Session Mean & Stddev: 18971805.650000,48.173930
Num Phases Traversed: 18784065.000000
Num Phases Traversed - Session Mean & Stddev: 187840.650000,0.476970
Num Path Iterations: 18784065.000000
Num Path Iterations - Session Mean & Stddev: 187840.650000,0.476970
+ set +x

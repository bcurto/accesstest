++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fea42600000 of size 327680 bytes.
[1.264s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 838389610.000000
Throughput: 83838961.000000
Num Accesses - Session Mean & Stddev: 8383896.100000,16111.868371
Num Phases Traversed: 322930.000000
Num Phases Traversed - Session Mean & Stddev: 3229.300000,6.204031
Num Path Iterations: 322930.000000
Num Path Iterations - Session Mean & Stddev: 3229.300000,6.204031
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6270400000 of size 327680 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 838431162.000000
Throughput: 83843116.200000
Num Accesses - Session Mean & Stddev: 8384311.620000,13229.006860
Num Phases Traversed: 322946.000000
Num Phases Traversed - Session Mean & Stddev: 3229.460000,5.093957
Num Path Iterations: 322946.000000
Num Path Iterations - Session Mean & Stddev: 3229.460000,5.093957
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff605e00000 of size 327680 bytes.
[1.275s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 838301312.000000
Throughput: 83830131.200000
Num Accesses - Session Mean & Stddev: 8383013.120000,14795.699547
Num Phases Traversed: 322896.000000
Num Phases Traversed - Session Mean & Stddev: 3228.960000,5.697227
Num Path Iterations: 322896.000000
Num Path Iterations - Session Mean & Stddev: 3228.960000,5.697227
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fae61400000 of size 327680 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 838249372.000000
Throughput: 83824937.200000
Num Accesses - Session Mean & Stddev: 8382493.720000,15192.427803
Num Phases Traversed: 322876.000000
Num Phases Traversed - Session Mean & Stddev: 3228.760000,5.849991
Num Path Iterations: 322876.000000
Num Path Iterations - Session Mean & Stddev: 3228.760000,5.849991
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc6f8200000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 838096149.000000
Throughput: 83809614.900000
Num Accesses - Session Mean & Stddev: 8380961.490000,11889.921425
Num Phases Traversed: 322817.000000
Num Phases Traversed - Session Mean & Stddev: 3228.170000,4.578329
Num Path Iterations: 322817.000000
Num Path Iterations - Session Mean & Stddev: 3228.170000,4.578329
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2079a00000 of size 327680 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 838394804.000000
Throughput: 83839480.400000
Num Accesses - Session Mean & Stddev: 8383948.040000,12091.426737
Num Phases Traversed: 322932.000000
Num Phases Traversed - Session Mean & Stddev: 3229.320000,4.655921
Num Path Iterations: 322932.000000
Num Path Iterations - Session Mean & Stddev: 3229.320000,4.655921
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f46bb000000 of size 327680 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 838064985.000000
Throughput: 83806498.500000
Num Accesses - Session Mean & Stddev: 8380649.850000,15639.784315
Num Phases Traversed: 322805.000000
Num Phases Traversed - Session Mean & Stddev: 3228.050000,6.022250
Num Path Iterations: 322805.000000
Num Path Iterations - Session Mean & Stddev: 3228.050000,6.022250
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0e14c00000 of size 327680 bytes.
[1.244s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 835657566.000000
Throughput: 83565756.600000
Num Accesses - Session Mean & Stddev: 8356575.660000,9251.883120
Num Phases Traversed: 321878.000000
Num Phases Traversed - Session Mean & Stddev: 3218.780000,3.562527
Num Path Iterations: 321878.000000
Num Path Iterations - Session Mean & Stddev: 3218.780000,3.562527
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff564800000 of size 327680 bytes.
[1.275s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 837984478.000000
Throughput: 83798447.800000
Num Accesses - Session Mean & Stddev: 8379844.780000,13311.341443
Num Phases Traversed: 322774.000000
Num Phases Traversed - Session Mean & Stddev: 3227.740000,5.125661
Num Path Iterations: 322774.000000
Num Path Iterations - Session Mean & Stddev: 3227.740000,5.125661
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9598c00000 of size 327680 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 838363640.000000
Throughput: 83836364.000000
Num Accesses - Session Mean & Stddev: 8383636.400000,15231.797977
Num Phases Traversed: 322920.000000
Num Phases Traversed - Session Mean & Stddev: 3229.200000,5.865151
Num Path Iterations: 322920.000000
Num Path Iterations - Session Mean & Stddev: 3229.200000,5.865151
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3311000000 of size 327680 bytes.
[1.276s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 838111731.000000
Throughput: 83811173.100000
Num Accesses - Session Mean & Stddev: 8381117.310000,13882.926580
Num Phases Traversed: 322823.000000
Num Phases Traversed - Session Mean & Stddev: 3228.230000,5.345755
Num Path Iterations: 322823.000000
Num Path Iterations - Session Mean & Stddev: 3228.230000,5.345755
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f06eda00000 of size 327680 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 838503878.000000
Throughput: 83850387.800000
Num Accesses - Session Mean & Stddev: 8385038.780000,13867.201851
Num Phases Traversed: 322974.000000
Num Phases Traversed - Session Mean & Stddev: 3229.740000,5.339700
Num Path Iterations: 322974.000000
Num Path Iterations - Session Mean & Stddev: 3229.740000,5.339700
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f658ae00000 of size 327680 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 838394804.000000
Throughput: 83839480.400000
Num Accesses - Session Mean & Stddev: 8383948.040000,14332.428669
Num Phases Traversed: 322932.000000
Num Phases Traversed - Session Mean & Stddev: 3229.320000,5.518840
Num Path Iterations: 322932.000000
Num Path Iterations - Session Mean & Stddev: 3229.320000,5.518840
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe68c600000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 838109134.000000
Throughput: 83810913.400000
Num Accesses - Session Mean & Stddev: 8381091.340000,13774.288229
Num Phases Traversed: 322822.000000
Num Phases Traversed - Session Mean & Stddev: 3228.220000,5.303923
Num Path Iterations: 322822.000000
Num Path Iterations - Session Mean & Stddev: 3228.220000,5.303923
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fec36800000 of size 327680 bytes.
[1.275s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 838033821.000000
Throughput: 83803382.100000
Num Accesses - Session Mean & Stddev: 8380338.210000,13053.664919
Num Phases Traversed: 322793.000000
Num Phases Traversed - Session Mean & Stddev: 3227.930000,5.026440
Num Path Iterations: 322793.000000
Num Path Iterations - Session Mean & Stddev: 3227.930000,5.026440
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f65d7200000 of size 327680 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 838329879.000000
Throughput: 83832987.900000
Num Accesses - Session Mean & Stddev: 8383298.790000,15219.638680
Num Phases Traversed: 322907.000000
Num Phases Traversed - Session Mean & Stddev: 3229.070000,5.860469
Num Path Iterations: 322907.000000
Num Path Iterations - Session Mean & Stddev: 3229.070000,5.860469
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdb3be00000 of size 327680 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 838288327.000000
Throughput: 83828832.700000
Num Accesses - Session Mean & Stddev: 8382883.270000,13429.754690
Num Phases Traversed: 322891.000000
Num Phases Traversed - Session Mean & Stddev: 3228.910000,5.171257
Num Path Iterations: 322891.000000
Num Path Iterations - Session Mean & Stddev: 3228.910000,5.171257
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efc50800000 of size 327680 bytes.
[1.285s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 838277939.000000
Throughput: 83827793.900000
Num Accesses - Session Mean & Stddev: 8382779.390000,15216.979616
Num Phases Traversed: 322887.000000
Num Phases Traversed - Session Mean & Stddev: 3228.870000,5.859445
Num Path Iterations: 322887.000000
Num Path Iterations - Session Mean & Stddev: 3228.870000,5.859445
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9049800000 of size 327680 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 838036418.000000
Throughput: 83803641.800000
Num Accesses - Session Mean & Stddev: 8380364.180000,12328.613020
Num Phases Traversed: 322794.000000
Num Phases Traversed - Session Mean & Stddev: 3227.940000,4.747252
Num Path Iterations: 322794.000000
Num Path Iterations - Session Mean & Stddev: 3227.940000,4.747252
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/2560,0.001 0/2560,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/2560,0.001 0/2560,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    2560 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb394200000 of size 327680 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 838106537.000000
Throughput: 83810653.700000
Num Accesses - Session Mean & Stddev: 8381065.370000,14738.037923
Num Phases Traversed: 322821.000000
Num Phases Traversed - Session Mean & Stddev: 3228.210000,5.675024
Num Path Iterations: 322821.000000
Num Path Iterations - Session Mean & Stddev: 3228.210000,5.675024
+ set +x

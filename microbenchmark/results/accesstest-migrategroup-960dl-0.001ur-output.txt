++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8724400000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2561731481.000000
Throughput: 256173148.100000
Num Accesses - Session Mean & Stddev: 25617314.810000,360.085953
Num Phases Traversed: 4955093.000000
Num Phases Traversed - Session Mean & Stddev: 49550.930000,0.696491
Num Path Iterations: 4955093.000000
Num Path Iterations - Session Mean & Stddev: 49550.930000,0.696491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faef6e00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2559651590.000000
Throughput: 255965159.000000
Num Accesses - Session Mean & Stddev: 25596515.900000,322.866397
Num Phases Traversed: 4951070.000000
Num Phases Traversed - Session Mean & Stddev: 49510.700000,0.624500
Num Path Iterations: 4951070.000000
Num Path Iterations - Session Mean & Stddev: 49510.700000,0.624500
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb38b800000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 2560497402.000000
Throughput: 256049740.200000
Num Accesses - Session Mean & Stddev: 25604974.020000,356.842374
Num Phases Traversed: 4952706.000000
Num Phases Traversed - Session Mean & Stddev: 49527.060000,0.690217
Num Path Iterations: 4952706.000000
Num Path Iterations - Session Mean & Stddev: 49527.060000,0.690217
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fab1f400000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2562067014.000000
Throughput: 256206701.400000
Num Accesses - Session Mean & Stddev: 25620670.140000,284.866531
Num Phases Traversed: 4955742.000000
Num Phases Traversed - Session Mean & Stddev: 49557.420000,0.550999
Num Path Iterations: 4955742.000000
Num Path Iterations - Session Mean & Stddev: 49557.420000,0.550999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f14fa200000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2563366752.000000
Throughput: 256336675.200000
Num Accesses - Session Mean & Stddev: 25633667.520000,276.679543
Num Phases Traversed: 4958256.000000
Num Phases Traversed - Session Mean & Stddev: 49582.560000,0.535164
Num Path Iterations: 4958256.000000
Num Path Iterations - Session Mean & Stddev: 49582.560000,0.535164
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3d01400000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2560227528.000000
Throughput: 256022752.800000
Num Accesses - Session Mean & Stddev: 25602275.280000,348.505555
Num Phases Traversed: 4952184.000000
Num Phases Traversed - Session Mean & Stddev: 49521.840000,0.674092
Num Path Iterations: 4952184.000000
Num Path Iterations - Session Mean & Stddev: 49521.840000,0.674092
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f78c9c00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2563356929.000000
Throughput: 256335692.900000
Num Accesses - Session Mean & Stddev: 25633569.290000,307.212867
Num Phases Traversed: 4958237.000000
Num Phases Traversed - Session Mean & Stddev: 49582.370000,0.594222
Num Path Iterations: 4958237.000000
Num Path Iterations - Session Mean & Stddev: 49582.370000,0.594222
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2a56400000 of size 61440 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2559011544.000000
Throughput: 255901154.400000
Num Accesses - Session Mean & Stddev: 25590115.440000,317.692188
Num Phases Traversed: 4949832.000000
Num Phases Traversed - Session Mean & Stddev: 49498.320000,0.614492
Num Path Iterations: 4949832.000000
Num Path Iterations - Session Mean & Stddev: 49498.320000,0.614492
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7efd5d800000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2560015558.000000
Throughput: 256001555.800000
Num Accesses - Session Mean & Stddev: 25600155.580000,332.009011
Num Phases Traversed: 4951774.000000
Num Phases Traversed - Session Mean & Stddev: 49517.740000,0.642184
Num Path Iterations: 4951774.000000
Num Path Iterations - Session Mean & Stddev: 49517.740000,0.642184
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8a9de00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2560894458.000000
Throughput: 256089445.800000
Num Accesses - Session Mean & Stddev: 25608944.580000,332.009011
Num Phases Traversed: 4953474.000000
Num Phases Traversed - Session Mean & Stddev: 49534.740000,0.642184
Num Path Iterations: 4953474.000000
Num Path Iterations - Session Mean & Stddev: 49534.740000,0.642184
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fedc9000000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2560021245.000000
Throughput: 256002124.500000
Num Accesses - Session Mean & Stddev: 25600212.450000,353.493433
Num Phases Traversed: 4951785.000000
Num Phases Traversed - Session Mean & Stddev: 49517.850000,0.683740
Num Path Iterations: 4951785.000000
Num Path Iterations - Session Mean & Stddev: 49517.850000,0.683740
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7faff8400000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2562239175.000000
Throughput: 256223917.500000
Num Accesses - Session Mean & Stddev: 25622391.750000,338.032613
Num Phases Traversed: 4956075.000000
Num Phases Traversed - Session Mean & Stddev: 49560.750000,0.653835
Num Path Iterations: 4956075.000000
Num Path Iterations - Session Mean & Stddev: 49560.750000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd24e800000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2560770378.000000
Throughput: 256077037.800000
Num Accesses - Session Mean & Stddev: 25607703.780000,312.089909
Num Phases Traversed: 4953234.000000
Num Phases Traversed - Session Mean & Stddev: 49532.340000,0.603656
Num Path Iterations: 4953234.000000
Num Path Iterations - Session Mean & Stddev: 49532.340000,0.603656
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6417c00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2561642557.000000
Throughput: 256164255.700000
Num Accesses - Session Mean & Stddev: 25616425.570000,345.230597
Num Phases Traversed: 4954921.000000
Num Phases Traversed - Session Mean & Stddev: 49549.210000,0.667757
Num Path Iterations: 4954921.000000
Num Path Iterations - Session Mean & Stddev: 49549.210000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb216a00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2558959844.000000
Throughput: 255895984.400000
Num Accesses - Session Mean & Stddev: 25589598.440000,317.692188
Num Phases Traversed: 4949732.000000
Num Phases Traversed - Session Mean & Stddev: 49497.320000,0.614492
Num Path Iterations: 4949732.000000
Num Path Iterations - Session Mean & Stddev: 49497.320000,0.614492
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe3c4400000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2560068809.000000
Throughput: 256006880.900000
Num Accesses - Session Mean & Stddev: 25600688.090000,341.806995
Num Phases Traversed: 4951877.000000
Num Phases Traversed - Session Mean & Stddev: 49518.770000,0.661135
Num Path Iterations: 4951877.000000
Num Path Iterations - Session Mean & Stddev: 49518.770000,0.661135
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6a36c00000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2558588121.000000
Throughput: 255858812.100000
Num Accesses - Session Mean & Stddev: 25585881.210000,355.604311
Num Phases Traversed: 4949013.000000
Num Phases Traversed - Session Mean & Stddev: 49490.130000,0.687823
Num Path Iterations: 4949013.000000
Num Path Iterations - Session Mean & Stddev: 49490.130000,0.687823
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3cad200000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2562386520.000000
Throughput: 256238652.000000
Num Accesses - Session Mean & Stddev: 25623865.200000,292.459365
Num Phases Traversed: 4956360.000000
Num Phases Traversed - Session Mean & Stddev: 49563.600000,0.565685
Num Path Iterations: 4956360.000000
Num Path Iterations - Session Mean & Stddev: 49563.600000,0.565685
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdf8d600000 of size 61440 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2557512761.000000
Throughput: 255751276.100000
Num Accesses - Session Mean & Stddev: 25575127.610000,319.161147
Num Phases Traversed: 4946933.000000
Num Phases Traversed - Session Mean & Stddev: 49469.330000,0.617333
Num Path Iterations: 4946933.000000
Num Path Iterations - Session Mean & Stddev: 49469.330000,0.617333
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/480,0.001 0/480,0.001' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/480,0.001 0/480,0.001 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    480 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd0e2000000 of size 61440 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 2561333391.000000
Throughput: 256133339.100000
Num Accesses - Session Mean & Stddev: 25613333.910000,341.806995
Num Phases Traversed: 4954323.000000
Num Phases Traversed - Session Mean & Stddev: 49543.230000,0.661135
Num Path Iterations: 4954323.000000
Num Path Iterations - Session Mean & Stddev: 49543.230000,0.661135
+ set +x

++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3b12e00000 of size 655360 bytes.
[1.287s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831200203.000000
Throughput: 83120020.300000
Num Accesses - Session Mean & Stddev: 8312002.030000,18178.626158
Num Phases Traversed: 161279.000000
Num Phases Traversed - Session Mean & Stddev: 1612.790000,3.525039
Num Path Iterations: 161279.000000
Num Path Iterations - Session Mean & Stddev: 1612.790000,3.525039
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6f6cc00000 of size 655360 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831323971.000000
Throughput: 83132397.100000
Num Accesses - Session Mean & Stddev: 8313239.710000,18841.831890
Num Phases Traversed: 161303.000000
Num Phases Traversed - Session Mean & Stddev: 1613.030000,3.653642
Num Path Iterations: 161303.000000
Num Path Iterations - Session Mean & Stddev: 1613.030000,3.653642
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fccdd200000 of size 655360 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831220831.000000
Throughput: 83122083.100000
Num Accesses - Session Mean & Stddev: 8312208.310000,18277.254869
Num Phases Traversed: 161283.000000
Num Phases Traversed - Session Mean & Stddev: 1612.830000,3.544164
Num Path Iterations: 161283.000000
Num Path Iterations - Session Mean & Stddev: 1612.830000,3.544164
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5bb2400000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831174418.000000
Throughput: 83117441.800000
Num Accesses - Session Mean & Stddev: 8311744.180000,18830.042441
Num Phases Traversed: 161274.000000
Num Phases Traversed - Session Mean & Stddev: 1612.740000,3.651356
Num Path Iterations: 161274.000000
Num Path Iterations - Session Mean & Stddev: 1612.740000,3.651356
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1540600000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831349756.000000
Throughput: 83134975.600000
Num Accesses - Session Mean & Stddev: 8313497.560000,19111.260333
Num Phases Traversed: 161308.000000
Num Phases Traversed - Session Mean & Stddev: 1613.080000,3.705887
Num Path Iterations: 161308.000000
Num Path Iterations - Session Mean & Stddev: 1613.080000,3.705887
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0cbde00000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831303343.000000
Throughput: 83130334.300000
Num Accesses - Session Mean & Stddev: 8313033.430000,19643.973568
Num Phases Traversed: 161299.000000
Num Phases Traversed - Session Mean & Stddev: 1612.990000,3.809186
Num Path Iterations: 161299.000000
Num Path Iterations - Session Mean & Stddev: 1612.990000,3.809186
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9b31c00000 of size 655360 bytes.
[1.287s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831117691.000000
Throughput: 83111769.100000
Num Accesses - Session Mean & Stddev: 8311176.910000,17467.772252
Num Phases Traversed: 161263.000000
Num Phases Traversed - Session Mean & Stddev: 1612.630000,3.387196
Num Path Iterations: 161263.000000
Num Path Iterations - Session Mean & Stddev: 1612.630000,3.387196
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4e52c00000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831313657.000000
Throughput: 83131365.700000
Num Accesses - Session Mean & Stddev: 8313136.570000,19494.483187
Num Phases Traversed: 161301.000000
Num Phases Traversed - Session Mean & Stddev: 1613.010000,3.780198
Num Path Iterations: 161301.000000
Num Path Iterations - Session Mean & Stddev: 1613.010000,3.780198
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff883000000 of size 655360 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831246616.000000
Throughput: 83124661.600000
Num Accesses - Session Mean & Stddev: 8312466.160000,18396.544929
Num Phases Traversed: 161288.000000
Num Phases Traversed - Session Mean & Stddev: 1612.880000,3.567296
Num Path Iterations: 161288.000000
Num Path Iterations - Session Mean & Stddev: 1612.880000,3.567296
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f18fe600000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831370384.000000
Throughput: 83137038.400000
Num Accesses - Session Mean & Stddev: 8313703.840000,19258.201814
Num Phases Traversed: 161312.000000
Num Phases Traversed - Session Mean & Stddev: 1613.120000,3.734381
Num Path Iterations: 161312.000000
Num Path Iterations - Session Mean & Stddev: 1613.120000,3.734381
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7fc1000000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831262087.000000
Throughput: 83126208.700000
Num Accesses - Session Mean & Stddev: 8312620.870000,19130.940909
Num Phases Traversed: 161291.000000
Num Phases Traversed - Session Mean & Stddev: 1612.910000,3.709703
Num Path Iterations: 161291.000000
Num Path Iterations - Session Mean & Stddev: 1612.910000,3.709703
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdb70800000 of size 655360 bytes.
[1.296s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831256930.000000
Throughput: 83125693.000000
Num Accesses - Session Mean & Stddev: 8312569.300000,19108.755343
Num Phases Traversed: 161290.000000
Num Phases Traversed - Session Mean & Stddev: 1612.900000,3.705401
Num Path Iterations: 161290.000000
Num Path Iterations - Session Mean & Stddev: 1612.900000,3.705401
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3eed400000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831231145.000000
Throughput: 83123114.500000
Num Accesses - Session Mean & Stddev: 8312311.450000,18684.786719
Num Phases Traversed: 161285.000000
Num Phases Traversed - Session Mean & Stddev: 1612.850000,3.623189
Num Path Iterations: 161285.000000
Num Path Iterations - Session Mean & Stddev: 1612.850000,3.623189
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f49ff200000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831236302.000000
Throughput: 83123630.200000
Num Accesses - Session Mean & Stddev: 8312363.020000,18608.411212
Num Phases Traversed: 161286.000000
Num Phases Traversed - Session Mean & Stddev: 1612.860000,3.608379
Num Path Iterations: 161286.000000
Num Path Iterations - Session Mean & Stddev: 1612.860000,3.608379
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff485400000 of size 655360 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831256930.000000
Throughput: 83125693.000000
Num Accesses - Session Mean & Stddev: 8312569.300000,18715.008077
Num Phases Traversed: 161290.000000
Num Phases Traversed - Session Mean & Stddev: 1612.900000,3.629049
Num Path Iterations: 161290.000000
Num Path Iterations - Session Mean & Stddev: 1612.900000,3.629049
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc13b200000 of size 655360 bytes.
[1.289s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831262087.000000
Throughput: 83126208.700000
Num Accesses - Session Mean & Stddev: 8312620.870000,19200.322002
Num Phases Traversed: 161291.000000
Num Phases Traversed - Session Mean & Stddev: 1612.910000,3.723157
Num Path Iterations: 161291.000000
Num Path Iterations - Session Mean & Stddev: 1612.910000,3.723157
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7effa2800000 of size 655360 bytes.
[1.300s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831293029.000000
Throughput: 83129302.900000
Num Accesses - Session Mean & Stddev: 8312930.290000,19329.533964
Num Phases Traversed: 161297.000000
Num Phases Traversed - Session Mean & Stddev: 1612.970000,3.748213
Num Path Iterations: 161297.000000
Num Path Iterations - Session Mean & Stddev: 1612.970000,3.748213
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fec6f200000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831236302.000000
Throughput: 83123630.200000
Num Accesses - Session Mean & Stddev: 8312363.020000,18508.098686
Num Phases Traversed: 161286.000000
Num Phases Traversed - Session Mean & Stddev: 1612.860000,3.588927
Num Path Iterations: 161286.000000
Num Path Iterations - Session Mean & Stddev: 1612.860000,3.588927
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f814ec00000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 831200203.000000
Throughput: 83120020.300000
Num Accesses - Session Mean & Stddev: 8312002.030000,18324.338428
Num Phases Traversed: 161279.000000
Num Phases Traversed - Session Mean & Stddev: 1612.790000,3.553294
Num Path Iterations: 161279.000000
Num Path Iterations - Session Mean & Stddev: 1612.790000,3.553294
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/5120,0 0/5120,0' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/5120,0 0/5120,0 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    5120 sess-global datalocs, 0.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa99fe00000 of size 655360 bytes.
[1.288s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 831256930.000000
Throughput: 83125693.000000
Num Accesses - Session Mean & Stddev: 8312569.300000,18225.453794
Num Phases Traversed: 161290.000000
Num Phases Traversed - Session Mean & Stddev: 1612.900000,3.534119
Num Path Iterations: 161290.000000
Num Path Iterations - Session Mean & Stddev: 1612.900000,3.534119
+ set +x

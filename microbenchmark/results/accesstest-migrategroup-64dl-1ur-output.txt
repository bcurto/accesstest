++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9f24c43000 of size 4096 bytes.
[1.258s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1512687721.000000
Throughput: 151268772.100000
Num Accesses - Session Mean & Stddev: 15126877.210000,27.763391
Num Phases Traversed: 21923109.000000
Num Phases Traversed - Session Mean & Stddev: 219231.090000,0.402368
Num Path Iterations: 21923109.000000
Num Path Iterations - Session Mean & Stddev: 219231.090000,0.402368
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f49a3a5b000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1509175207.000000
Throughput: 150917520.700000
Num Accesses - Session Mean & Stddev: 15091752.070000,47.258704
Num Phases Traversed: 21872203.000000
Num Phases Traversed - Session Mean & Stddev: 218722.030000,0.684909
Num Path Iterations: 21872203.000000
Num Path Iterations - Session Mean & Stddev: 218722.030000,0.684909
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdbf266a000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1506217315.000000
Throughput: 150621731.500000
Num Accesses - Session Mean & Stddev: 15062173.150000,40.674900
Num Phases Traversed: 21829335.000000
Num Phases Traversed - Session Mean & Stddev: 218293.350000,0.589491
Num Path Iterations: 21829335.000000
Num Path Iterations - Session Mean & Stddev: 218293.350000,0.589491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f80b9a6a000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1507359196.000000
Throughput: 150735919.600000
Num Accesses - Session Mean & Stddev: 15073591.960000,48.516372
Num Phases Traversed: 21845884.000000
Num Phases Traversed - Session Mean & Stddev: 218458.840000,0.703136
Num Path Iterations: 21845884.000000
Num Path Iterations - Session Mean & Stddev: 218458.840000,0.703136
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa128127000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1524846487.000000
Throughput: 152484648.700000
Num Accesses - Session Mean & Stddev: 15248464.870000,33.598112
Num Phases Traversed: 22099323.000000
Num Phases Traversed - Session Mean & Stddev: 220993.230000,0.486929
Num Path Iterations: 22099323.000000
Num Path Iterations - Session Mean & Stddev: 220993.230000,0.486929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5a01df6000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1533853195.000000
Throughput: 153385319.500000
Num Accesses - Session Mean & Stddev: 15338531.950000,36.997128
Num Phases Traversed: 22229855.000000
Num Phases Traversed - Session Mean & Stddev: 222298.550000,0.536190
Num Path Iterations: 22229855.000000
Num Path Iterations - Session Mean & Stddev: 222298.550000,0.536190
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f092b2ac000 of size 4096 bytes.
[1.272s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1536634792.000000
Throughput: 153663479.200000
Num Accesses - Session Mean & Stddev: 15366347.920000,40.091316
Num Phases Traversed: 22270168.000000
Num Phases Traversed - Session Mean & Stddev: 222701.680000,0.581034
Num Path Iterations: 22270168.000000
Num Path Iterations - Session Mean & Stddev: 222701.680000,0.581034
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa6605f6000 of size 4096 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1524276202.000000
Throughput: 152427620.200000
Num Accesses - Session Mean & Stddev: 15242762.020000,38.018937
Num Phases Traversed: 22091058.000000
Num Phases Traversed - Session Mean & Stddev: 220910.580000,0.550999
Num Path Iterations: 22091058.000000
Num Path Iterations - Session Mean & Stddev: 220910.580000,0.550999
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3e03cc8000 of size 4096 bytes.
[1.229s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1533895837.000000
Throughput: 153389583.700000
Num Accesses - Session Mean & Stddev: 15338958.370000,492.357912
Num Phases Traversed: 22230473.000000
Num Phases Traversed - Session Mean & Stddev: 222304.730000,7.135622
Num Path Iterations: 22230473.000000
Num Path Iterations - Session Mean & Stddev: 222304.730000,7.135622
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f92c7376000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1505583619.000000
Throughput: 150558361.900000
Num Accesses - Session Mean & Stddev: 15055836.190000,38.411377
Num Phases Traversed: 21820151.000000
Num Phases Traversed - Session Mean & Stddev: 218201.510000,0.556687
Num Path Iterations: 21820151.000000
Num Path Iterations - Session Mean & Stddev: 218201.510000,0.556687
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f25005bb000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1505691949.000000
Throughput: 150569194.900000
Num Accesses - Session Mean & Stddev: 15056919.490000,46.075263
Num Phases Traversed: 21821721.000000
Num Phases Traversed - Session Mean & Stddev: 218217.210000,0.667757
Num Path Iterations: 21821721.000000
Num Path Iterations - Session Mean & Stddev: 218217.210000,0.667757
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f68a5d8b000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1519574542.000000
Throughput: 151957454.200000
Num Accesses - Session Mean & Stddev: 15195745.420000,46.163011
Num Phases Traversed: 22022918.000000
Num Phases Traversed - Session Mean & Stddev: 220229.180000,0.669029
Num Path Iterations: 22022918.000000
Num Path Iterations - Session Mean & Stddev: 220229.180000,0.669029
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fb94876b000 of size 4096 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 894595864.000000
Throughput: 89459586.400000
Num Accesses - Session Mean & Stddev: 8945958.640000,36.926283
Num Phases Traversed: 12965256.000000
Num Phases Traversed - Session Mean & Stddev: 129652.560000,0.535164
Num Path Iterations: 12965256.000000
Num Path Iterations - Session Mean & Stddev: 129652.560000,0.535164
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2fc36ac000 of size 4096 bytes.
[1.273s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1518376081.000000
Throughput: 151837608.100000
Num Accesses - Session Mean & Stddev: 15183760.810000,34.493099
Num Phases Traversed: 22005549.000000
Num Phases Traversed - Session Mean & Stddev: 220055.490000,0.499900
Num Path Iterations: 22005549.000000
Num Path Iterations - Session Mean & Stddev: 220055.490000,0.499900
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5e97677000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1534874326.000000
Throughput: 153487432.600000
Num Accesses - Session Mean & Stddev: 15348743.260000,35.747061
Num Phases Traversed: 22244654.000000
Num Phases Traversed - Session Mean & Stddev: 222446.540000,0.518073
Num Path Iterations: 22244654.000000
Num Path Iterations - Session Mean & Stddev: 222446.540000,0.518073
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.001s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.001s] Creating global buffer at 0x7f53146f2000 of size 4096 bytes.
[1.228s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1505296510.000000
Throughput: 150529651.000000
Num Accesses - Session Mean & Stddev: 15052965.100000,3130.980969
Num Phases Traversed: 21815990.000000
Num Phases Traversed - Session Mean & Stddev: 218159.900000,45.376536
Num Path Iterations: 21815990.000000
Num Path Iterations - Session Mean & Stddev: 218159.900000,45.376536
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f9e78a5d000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 1533916744.000000
Throughput: 153391674.400000
Num Accesses - Session Mean & Stddev: 15339167.440000,45.894078
Num Phases Traversed: 22230776.000000
Num Phases Traversed - Session Mean & Stddev: 222307.760000,0.665132
Num Path Iterations: 22230776.000000
Num Path Iterations - Session Mean & Stddev: 222307.760000,0.665132
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f456cfbe000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1530422722.000000
Throughput: 153042272.200000
Num Accesses - Session Mean & Stddev: 15304227.220000,38.763018
Num Phases Traversed: 22180138.000000
Num Phases Traversed - Session Mean & Stddev: 221801.380000,0.561783
Num Path Iterations: 22180138.000000
Num Path Iterations - Session Mean & Stddev: 221801.380000,0.561783
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd0ebc06000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1523369473.000000
Throughput: 152336947.300000
Num Accesses - Session Mean & Stddev: 15233694.730000,45.826598
Num Phases Traversed: 22077917.000000
Num Phases Traversed - Session Mean & Stddev: 220779.170000,0.664154
Num Path Iterations: 22077917.000000
Num Path Iterations - Session Mean & Stddev: 220779.170000,0.664154
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/32,1 0/32,1' '-g 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/32,1 0/32,1 fibre -g 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    32 sess-global datalocs, 1.000000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f62562cd000 of size 4096 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1507330216.000000
Throughput: 150733021.600000
Num Accesses - Session Mean & Stddev: 15073302.160000,45.267366
Num Phases Traversed: 21845464.000000
Num Phases Traversed - Session Mean & Stddev: 218454.640000,0.656049
Num Path Iterations: 21845464.000000
Num Path Iterations - Session Mean & Stddev: 218454.640000,0.656049
+ set +x

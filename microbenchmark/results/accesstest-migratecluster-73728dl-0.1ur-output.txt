++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fe580000000 of size 4718592 bytes.
[1.600s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 811416189.000000
Throughput: 81141618.900000
Num Accesses - Session Mean & Stddev: 8114161.890000,23276.898084
Num Phases Traversed: 22089.000000
Num Phases Traversed - Session Mean & Stddev: 220.890000,0.630793
Num Path Iterations: 22089.000000
Num Path Iterations - Session Mean & Stddev: 220.890000,0.630793
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5e40000000 of size 4718592 bytes.
[1.600s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811563793.000000
Throughput: 81156379.300000
Num Accesses - Session Mean & Stddev: 8115637.930000,24618.802973
Num Phases Traversed: 22093.000000
Num Phases Traversed - Session Mean & Stddev: 220.930000,0.667158
Num Path Iterations: 22093.000000
Num Path Iterations - Session Mean & Stddev: 220.930000,0.667158
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2540000000 of size 4718592 bytes.
[1.572s] Test is ready to start.
Test duration in range [10.000, 10.002] seconds
Num Accesses: 810899575.000000
Throughput: 81089957.500000
Num Accesses - Session Mean & Stddev: 8108995.750000,24127.159487
Num Phases Traversed: 22075.000000
Num Phases Traversed - Session Mean & Stddev: 220.750000,0.653835
Num Path Iterations: 22075.000000
Num Path Iterations - Session Mean & Stddev: 220.750000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f5c40000000 of size 4718592 bytes.
[1.600s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811526892.000000
Throughput: 81152689.200000
Num Accesses - Session Mean & Stddev: 8115268.920000,23731.675459
Num Phases Traversed: 22092.000000
Num Phases Traversed - Session Mean & Stddev: 220.920000,0.643117
Num Path Iterations: 22092.000000
Num Path Iterations - Session Mean & Stddev: 220.920000,0.643117
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f70c0000000 of size 4718592 bytes.
[1.600s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810825773.000000
Throughput: 81082577.300000
Num Accesses - Session Mean & Stddev: 8108257.730000,16382.547945
Num Phases Traversed: 22073.000000
Num Phases Traversed - Session Mean & Stddev: 220.730000,0.443959
Num Path Iterations: 22073.000000
Num Path Iterations - Session Mean & Stddev: 220.730000,0.443959
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3600000000 of size 4718592 bytes.
[1.603s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810899575.000000
Throughput: 81089957.500000
Num Accesses - Session Mean & Stddev: 8108995.750000,21752.818687
Num Phases Traversed: 22075.000000
Num Phases Traversed - Session Mean & Stddev: 220.750000,0.589491
Num Path Iterations: 22075.000000
Num Path Iterations - Session Mean & Stddev: 220.750000,0.589491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4780000000 of size 4718592 bytes.
[1.600s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810936476.000000
Throughput: 81093647.600000
Num Accesses - Session Mean & Stddev: 8109364.760000,20952.490483
Num Phases Traversed: 22076.000000
Num Phases Traversed - Session Mean & Stddev: 220.760000,0.567803
Num Path Iterations: 22076.000000
Num Path Iterations - Session Mean & Stddev: 220.760000,0.567803
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f4240000000 of size 4718592 bytes.
[1.600s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810973377.000000
Throughput: 81097337.700000
Num Accesses - Session Mean & Stddev: 8109733.770000,19424.792953
Num Phases Traversed: 22077.000000
Num Phases Traversed - Session Mean & Stddev: 220.770000,0.526403
Num Path Iterations: 22077.000000
Num Path Iterations - Session Mean & Stddev: 220.770000,0.526403
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fa380000000 of size 4718592 bytes.
[1.600s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810788872.000000
Throughput: 81078887.200000
Num Accesses - Session Mean & Stddev: 8107888.720000,21567.363919
Num Phases Traversed: 22072.000000
Num Phases Traversed - Session Mean & Stddev: 220.720000,0.584466
Num Path Iterations: 22072.000000
Num Path Iterations - Session Mean & Stddev: 220.720000,0.584466
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8000000000 of size 4718592 bytes.
[1.600s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811268585.000000
Throughput: 81126858.500000
Num Accesses - Session Mean & Stddev: 8112685.850000,21752.818687
Num Phases Traversed: 22085.000000
Num Phases Traversed - Session Mean & Stddev: 220.850000,0.589491
Num Path Iterations: 22085.000000
Num Path Iterations - Session Mean & Stddev: 220.850000,0.589491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f1dc0000000 of size 4718592 bytes.
[1.596s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810862674.000000
Throughput: 81086267.400000
Num Accesses - Session Mean & Stddev: 8108626.740000,21274.954652
Num Phases Traversed: 22074.000000
Num Phases Traversed - Session Mean & Stddev: 220.740000,0.576541
Num Path Iterations: 22074.000000
Num Path Iterations - Session Mean & Stddev: 220.740000,0.576541
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fecc0000000 of size 4718592 bytes.
[1.603s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811231684.000000
Throughput: 81123168.400000
Num Accesses - Session Mean & Stddev: 8112316.840000,21338.862740
Num Phases Traversed: 22084.000000
Num Phases Traversed - Session Mean & Stddev: 220.840000,0.578273
Num Path Iterations: 22084.000000
Num Path Iterations - Session Mean & Stddev: 220.840000,0.578273
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f20c0000000 of size 4718592 bytes.
[1.600s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811010278.000000
Throughput: 81101027.800000
Num Accesses - Session Mean & Stddev: 8110102.780000,17758.546346
Num Phases Traversed: 22078.000000
Num Phases Traversed - Session Mean & Stddev: 220.780000,0.481248
Num Path Iterations: 22078.000000
Num Path Iterations - Session Mean & Stddev: 220.780000,0.481248
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f83c0000000 of size 4718592 bytes.
[1.560s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 810899575.000000
Throughput: 81089957.500000
Num Accesses - Session Mean & Stddev: 8108995.750000,24127.159487
Num Phases Traversed: 22075.000000
Num Phases Traversed - Session Mean & Stddev: 220.750000,0.653835
Num Path Iterations: 22075.000000
Num Path Iterations - Session Mean & Stddev: 220.750000,0.653835
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff700000000 of size 4718592 bytes.
[1.600s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811268585.000000
Throughput: 81126858.500000
Num Accesses - Session Mean & Stddev: 8112685.850000,21752.818687
Num Phases Traversed: 22085.000000
Num Phases Traversed - Session Mean & Stddev: 220.850000,0.589491
Num Path Iterations: 22085.000000
Num Path Iterations - Session Mean & Stddev: 220.850000,0.589491
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8300000000 of size 4718592 bytes.
[1.592s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811194783.000000
Throughput: 81119478.300000
Num Accesses - Session Mean & Stddev: 8111947.830000,24507.931872
Num Phases Traversed: 22083.000000
Num Phases Traversed - Session Mean & Stddev: 220.830000,0.664154
Num Path Iterations: 22083.000000
Num Path Iterations - Session Mean & Stddev: 220.830000,0.664154
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f90c0000000 of size 4718592 bytes.
[1.600s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811305486.000000
Throughput: 81130548.600000
Num Accesses - Session Mean & Stddev: 8113054.860000,21529.448805
Num Phases Traversed: 22086.000000
Num Phases Traversed - Session Mean & Stddev: 220.860000,0.583438
Num Path Iterations: 22086.000000
Num Path Iterations - Session Mean & Stddev: 220.860000,0.583438
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3200000000 of size 4718592 bytes.
[1.607s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811342387.000000
Throughput: 81134238.700000
Num Accesses - Session Mean & Stddev: 8113423.870000,24839.060575
Num Phases Traversed: 22087.000000
Num Phases Traversed - Session Mean & Stddev: 220.870000,0.673127
Num Path Iterations: 22087.000000
Num Path Iterations - Session Mean & Stddev: 220.870000,0.673127
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdc40000000 of size 4718592 bytes.
[1.600s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811305486.000000
Throughput: 81130548.600000
Num Accesses - Session Mean & Stddev: 8113054.860000,21529.448805
Num Phases Traversed: 22086.000000
Num Phases Traversed - Session Mean & Stddev: 220.860000,0.583438
Num Path Iterations: 22086.000000
Num Path Iterations - Session Mean & Stddev: 220.860000,0.583438
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/36864,0.1 0/36864,0.1' '-c 1,1'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/36864,0.1 0/36864,0.1 fibre -c 1,1
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    36864 sess-global datalocs, 0.100000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f7d80000000 of size 4718592 bytes.
[1.600s] Test is ready to start.
Test duration in range [10.000, 10.001] seconds
Num Accesses: 811305486.000000
Throughput: 81130548.600000
Num Accesses - Session Mean & Stddev: 8113054.860000,21529.448805
Num Phases Traversed: 22086.000000
Num Phases Traversed - Session Mean & Stddev: 220.860000,0.583438
Num Path Iterations: 22086.000000
Num Path Iterations - Session Mean & Stddev: 220.860000,0.583438
+ set +x

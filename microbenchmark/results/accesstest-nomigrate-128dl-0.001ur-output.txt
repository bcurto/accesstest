++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fcb71000000 of size 8192 bytes.
[1.268s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1422153628.000000
Throughput: 142215362.800000
Num Accesses - Session Mean & Stddev: 14221536.280000,84688.565743
Num Phases Traversed: 14080828.000000
Num Phases Traversed - Session Mean & Stddev: 140808.280000,838.500651
Num Path Iterations: 14080828.000000
Num Path Iterations - Session Mean & Stddev: 140808.280000,838.500651
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f987be00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1422846488.000000
Throughput: 142284648.800000
Num Accesses - Session Mean & Stddev: 14228464.880000,82858.423164
Num Phases Traversed: 14087688.000000
Num Phases Traversed - Session Mean & Stddev: 140876.880000,820.380427
Num Path Iterations: 14087688.000000
Num Path Iterations - Session Mean & Stddev: 140876.880000,820.380427
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f65f2600000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1415673367.000000
Throughput: 141567336.700000
Num Accesses - Session Mean & Stddev: 14156733.670000,79829.449356
Num Phases Traversed: 14016667.000000
Num Phases Traversed - Session Mean & Stddev: 140166.670000,790.390588
Num Path Iterations: 14016667.000000
Num Path Iterations - Session Mean & Stddev: 140166.670000,790.390588
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f03d2200000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1414949298.000000
Throughput: 141494929.800000
Num Accesses - Session Mean & Stddev: 14149492.980000,78329.593916
Num Phases Traversed: 14009498.000000
Num Phases Traversed - Session Mean & Stddev: 140094.980000,775.540534
Num Path Iterations: 14009498.000000
Num Path Iterations - Session Mean & Stddev: 140094.980000,775.540534
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f273ce00000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1431941033.000000
Throughput: 143194103.300000
Num Accesses - Session Mean & Stddev: 14319410.330000,39479.968678
Num Phases Traversed: 14177733.000000
Num Phases Traversed - Session Mean & Stddev: 141777.330000,390.890779
Num Path Iterations: 14177733.000000
Num Path Iterations - Session Mean & Stddev: 141777.330000,390.890779
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fdcbe600000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1422306239.000000
Throughput: 142230623.900000
Num Accesses - Session Mean & Stddev: 14223062.390000,83897.725262
Num Phases Traversed: 14082339.000000
Num Phases Traversed - Session Mean & Stddev: 140823.390000,830.670547
Num Path Iterations: 14082339.000000
Num Path Iterations - Session Mean & Stddev: 140823.390000,830.670547
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7ff333400000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1422191604.000000
Throughput: 142219160.400000
Num Accesses - Session Mean & Stddev: 14221916.040000,27021.699839
Num Phases Traversed: 14081204.000000
Num Phases Traversed - Session Mean & Stddev: 140812.040000,267.541583
Num Path Iterations: 14081204.000000
Num Path Iterations - Session Mean & Stddev: 140812.040000,267.541583
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7feb0f200000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1423728016.000000
Throughput: 142372801.600000
Num Accesses - Session Mean & Stddev: 14237280.160000,123563.440222
Num Phases Traversed: 14096416.000000
Num Phases Traversed - Session Mean & Stddev: 140964.160000,1223.400398
Num Path Iterations: 14096416.000000
Num Path Iterations - Session Mean & Stddev: 140964.160000,1223.400398
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f6387e00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1423018491.000000
Throughput: 142301849.100000
Num Accesses - Session Mean & Stddev: 14230184.910000,123778.561358
Num Phases Traversed: 14089391.000000
Num Phases Traversed - Session Mean & Stddev: 140893.910000,1225.530310
Num Path Iterations: 14089391.000000
Num Path Iterations - Session Mean & Stddev: 140893.910000,1225.530310
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fd491000000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1425317049.000000
Throughput: 142531704.900000
Num Accesses - Session Mean & Stddev: 14253170.490000,27897.404862
Num Phases Traversed: 14112149.000000
Num Phases Traversed - Session Mean & Stddev: 141121.490000,276.211929
Num Path Iterations: 14112149.000000
Num Path Iterations - Session Mean & Stddev: 141121.490000,276.211929
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fc330800000 of size 8192 bytes.
[1.217s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1414702050.000000
Throughput: 141470205.000000
Num Accesses - Session Mean & Stddev: 14147020.500000,235538.068367
Num Phases Traversed: 14007050.000000
Num Phases Traversed - Session Mean & Stddev: 140070.500000,2332.060083
Num Path Iterations: 14007050.000000
Num Path Iterations - Session Mean & Stddev: 140070.500000,2332.060083
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f965a800000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1436431695.000000
Throughput: 143643169.500000
Num Accesses - Session Mean & Stddev: 14364316.950000,72193.860890
Num Phases Traversed: 14222195.000000
Num Phases Traversed - Session Mean & Stddev: 142221.950000,714.790702
Num Path Iterations: 14222195.000000
Num Path Iterations - Session Mean & Stddev: 142221.950000,714.790702
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fada7200000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1417582772.000000
Throughput: 141758277.200000
Num Accesses - Session Mean & Stddev: 14175827.720000,81650.482792
Num Phases Traversed: 14035572.000000
Num Phases Traversed - Session Mean & Stddev: 140355.720000,808.420622
Num Path Iterations: 14035572.000000
Num Path Iterations - Session Mean & Stddev: 140355.720000,808.420622
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f0dba400000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1422281797.000000
Throughput: 142228179.700000
Num Accesses - Session Mean & Stddev: 14222817.970000,47743.821638
Num Phases Traversed: 14082097.000000
Num Phases Traversed - Session Mean & Stddev: 140820.970000,472.711105
Num Path Iterations: 14082097.000000
Num Path Iterations - Session Mean & Stddev: 140820.970000,472.711105
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f85b8e00000 of size 8192 bytes.
[1.261s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1428136868.000000
Throughput: 142813686.800000
Num Accesses - Session Mean & Stddev: 14281368.680000,40850.578115
Num Phases Traversed: 14140068.000000
Num Phases Traversed - Session Mean & Stddev: 141400.680000,404.461169
Num Path Iterations: 14140068.000000
Num Path Iterations - Session Mean & Stddev: 141400.680000,404.461169
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7fbb6be00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1418040605.000000
Throughput: 141804060.500000
Num Accesses - Session Mean & Stddev: 14180406.050000,90672.805408
Num Phases Traversed: 14040105.000000
Num Phases Traversed - Session Mean & Stddev: 140401.050000,897.750549
Num Path Iterations: 14040105.000000
Num Path Iterations - Session Mean & Stddev: 140401.050000,897.750549
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f3e8fe00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1427349775.000000
Throughput: 142734977.500000
Num Accesses - Session Mean & Stddev: 14273497.750000,185152.220616
Num Phases Traversed: 14132275.000000
Num Phases Traversed - Session Mean & Stddev: 141322.750000,1833.190303
Num Path Iterations: 14132275.000000
Num Path Iterations - Session Mean & Stddev: 141322.750000,1833.190303
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f8f2aa00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1419094641.000000
Throughput: 141909464.100000
Num Accesses - Session Mean & Stddev: 14190946.410000,11850.662702
Num Phases Traversed: 14050541.000000
Num Phases Traversed - Session Mean & Stddev: 140505.410000,117.333294
Num Path Iterations: 14050541.000000
Num Path Iterations - Session Mean & Stddev: 140505.410000,117.333294
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f2b9e000000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1429844879.000000
Throughput: 142984487.900000
Num Accesses - Session Mean & Stddev: 14298448.790000,25245.193134
Num Phases Traversed: 14156979.000000
Num Phases Traversed - Session Mean & Stddev: 141569.790000,249.952407
Num Path Iterations: 14156979.000000
Num Path Iterations - Session Mean & Stddev: 141569.790000,249.952407
+ set +x
++ printf 'taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u %s fibre %s\n' '0/64,0.001 0/64,0.001' '-w 2'
+ taskset -c 1-2 timeout 100 /home/bcurto/mem-access-simulator/accesstest/accesstest -v -s 100 -d 10 --sess-distr u 0/64,0.001 0/64,0.001 fibre -w 2
[0.000s] Creating path with 2 phases.
  Phase 0:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
  Phase 1:
    0 sess-local datalocs, 0.000000 update ratio, inf access count scalar /
    64 sess-global datalocs, 0.001000 update ratio, inf access count scalar
[0.000s] Creating global buffer at 0x7f242ca00000 of size 8192 bytes.
[1.260s] Test is ready to start.
Test duration in range [10.000, 10.000] seconds
Num Accesses: 1423970113.000000
Throughput: 142397011.300000
Num Accesses - Session Mean & Stddev: 14239701.130000,171511.154273
Num Phases Traversed: 14098813.000000
Num Phases Traversed - Session Mean & Stddev: 140988.130000,1698.130240
Num Path Iterations: 14098813.000000
Num Path Iterations - Session Mean & Stddev: 140988.130000,1698.130240
+ set +x

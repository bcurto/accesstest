Enter a function name whose access statistics will be computed.
Use `@list` to list function names. Further usage information can be found using `@list help`.
Use `@set` to update profiler's state. Further usage information can be found using `@set help`.
> 
== Overall Stats ==
  Num Data Locations: 96796
  Num Samples: 7464354


== Phase __x64_sys_writev ==
  Num Data Locations: 11689 (12.08%)
  Num Samples: 420480 (5.63%)

  Phase Local  Session Local 
    Num Data Locations: 1381 (1.43%)
    Num Samples: 4283 (0.06%)
    Update Ratio: 35.51%

  Phase Local  Session Global
    Num Data Locations: 478 (0.49%)
    Num Samples: 22172 (0.30%)
    Update Ratio: 35.41%

  Phase Global Session Local 
    Num Data Locations: 2231 (2.30%)
    Num Samples: 40818 (0.55%)
    Update Ratio: 9.34%

  Phase Global Session Global
    Num Data Locations: 7599 (7.85%)
    Num Samples: 353207 (4.73%)
    Update Ratio: 19.41%


== Everything Else ==
  Num Data Locations: 94937 (98.08%)
  Num Samples: 7043874 (94.37%)

  Phase Local  Session Local 
    Num Data Locations: 65652 (67.83%)
    Num Samples: 3154708 (42.26%)
    Update Ratio: 48.07%

  Phase Local  Session Global
    Num Data Locations: 19455 (20.10%)
    Num Samples: 2325759 (31.16%)
    Update Ratio: 9.96%

  Phase Global Session Local 
    Num Data Locations: 2231 (2.30%)
    Num Samples: 48948 (0.66%)
    Update Ratio: 21.91%

  Phase Global Session Global
    Num Data Locations: 7599 (7.85%)
    Num Samples: 1514459 (20.29%)
    Update Ratio: 16.75%


Phase Characteristics: 1381,0.355125,4283/478,0.354140,22172 65652,0.480734,3154708/19455,0.099598,2325759 2231/7599:0,0.093415,0.194121,40818,353207/1,0.219069,0.167476,48948,1514459


> 
== Overall Stats ==
  Num Data Locations: 96796
  Num Samples: 7464354


== Phase __x64_sys_openat ==
  Num Data Locations: 16544 (17.09%)
  Num Samples: 829131 (11.11%)

  Phase Local  Session Local 
    Num Data Locations: 3033 (3.13%)
    Num Samples: 3065 (0.04%)
    Update Ratio: 61.14%

  Phase Local  Session Global
    Num Data Locations: 3772 (3.90%)
    Num Samples: 154499 (2.07%)
    Update Ratio: 36.14%

  Phase Global Session Local 
    Num Data Locations: 581 (0.60%)
    Num Samples: 2565 (0.03%)
    Update Ratio: 0.31%

  Phase Global Session Global
    Num Data Locations: 9158 (9.46%)
    Num Samples: 669002 (8.96%)
    Update Ratio: 19.85%


== Everything Else ==
  Num Data Locations: 89991 (92.97%)
  Num Samples: 6635223 (88.89%)

  Phase Local  Session Local 
    Num Data Locations: 65650 (67.82%)
    Num Samples: 3214593 (43.07%)
    Update Ratio: 47.43%

  Phase Local  Session Global
    Num Data Locations: 14602 (15.09%)
    Num Samples: 1876833 (25.14%)
    Update Ratio: 7.90%

  Phase Global Session Local 
    Num Data Locations: 581 (0.60%)
    Num Samples: 28534 (0.38%)
    Update Ratio: 21.00%

  Phase Global Session Global
    Num Data Locations: 9158 (9.46%)
    Num Samples: 1515263 (20.30%)
    Update Ratio: 14.83%


Phase Characteristics: 3033,0.611419,3065/3772,0.361446,154499 65650,0.474324,3214593/14602,0.079042,1876833 581/9158:0,0.003119,0.198458,2565,669002/1,0.209960,0.148312,28534,1515263


> 
== Overall Stats ==
  Num Data Locations: 96796
  Num Samples: 7464354


== Phase __x64_sys_read ==
  Num Data Locations: 15241 (15.75%)
  Num Samples: 269303 (3.61%)

  Phase Local  Session Local 
    Num Data Locations: 4449 (4.60%)
    Num Samples: 4905 (0.07%)
    Update Ratio: 21.20%

  Phase Local  Session Global
    Num Data Locations: 1563 (1.61%)
    Num Samples: 10564 (0.14%)
    Update Ratio: 18.01%

  Phase Global Session Local 
    Num Data Locations: 2353 (2.43%)
    Num Samples: 35922 (0.48%)
    Update Ratio: 18.91%

  Phase Global Session Global
    Num Data Locations: 6876 (7.10%)
    Num Samples: 217912 (2.92%)
    Update Ratio: 22.54%


== Everything Else ==
  Num Data Locations: 90784 (93.79%)
  Num Samples: 7195051 (96.39%)

  Phase Local  Session Local 
    Num Data Locations: 62462 (64.53%)
    Num Samples: 3058545 (40.98%)
    Update Ratio: 49.42%

  Phase Local  Session Global
    Num Data Locations: 19093 (19.72%)
    Num Samples: 2880815 (38.59%)
    Update Ratio: 10.88%

  Phase Global Session Local 
    Num Data Locations: 2353 (2.43%)
    Num Samples: 149385 (2.00%)
    Update Ratio: 8.96%

  Phase Global Session Global
    Num Data Locations: 6876 (7.10%)
    Num Samples: 1106306 (14.82%)
    Update Ratio: 17.84%


Phase Characteristics: 4449,0.212029,4905/1563,0.180140,10564 62462,0.494160,3058545/19093,0.108757,2880815 2353/6876:0,0.189132,0.225403,35922,217912/1,0.089601,0.178398,149385,1106306


> 
== Overall Stats ==
  Num Data Locations: 96796
  Num Samples: 7464354


== Phase __x64_sys_mmap ==
  Num Data Locations: 5287 (5.46%)
  Num Samples: 267955 (3.59%)

  Phase Local  Session Local 
    Num Data Locations: 474 (0.49%)
    Num Samples: 485 (0.01%)
    Update Ratio: 36.70%

  Phase Local  Session Global
    Num Data Locations: 213 (0.22%)
    Num Samples: 26287 (0.35%)
    Update Ratio: 9.50%

  Phase Global Session Local 
    Num Data Locations: 13 (0.01%)
    Num Samples: 13 (0.00%)
    Update Ratio: 38.46%

  Phase Global Session Global
    Num Data Locations: 4587 (4.74%)
    Num Samples: 241170 (3.23%)
    Update Ratio: 13.30%


== Everything Else ==
  Num Data Locations: 96109 (99.29%)
  Num Samples: 7196399 (96.41%)

  Phase Local  Session Local 
    Num Data Locations: 68777 (71.05%)
    Num Samples: 3248246 (43.52%)
    Update Ratio: 47.18%

  Phase Local  Session Global
    Num Data Locations: 22732 (23.48%)
    Num Samples: 2494895 (33.42%)
    Update Ratio: 11.54%

  Phase Global Session Local 
    Num Data Locations: 13 (0.01%)
    Num Samples: 13 (0.00%)
    Update Ratio: 30.77%

  Phase Global Session Global
    Num Data Locations: 4587 (4.74%)
    Num Samples: 1453245 (19.47%)
    Update Ratio: 16.46%


Phase Characteristics: 474,0.367010,485/213,0.094990,26287 68777,0.471776,3248246/22732,0.115399,2494895 13/4587:0,0.384615,0.132973,13,241170/1,0.307692,0.164610,13,1453245


> 
Enter a function name whose access statistics will be computed.
Use `@list` to list function names. Further usage information can be found using `@list help`.
Use `@set` to update profiler's state. Further usage information can be found using `@set help`.
> 
== Overall Stats ==
  Num Data Locations: 99149
  Num Samples: 7205744


== Phase __x64_sys_writev ==
  Num Data Locations: 11220 (11.32%)
  Num Samples: 406007 (5.63%)

  Phase Local  Session Local 
    Num Data Locations: 1553 (1.57%)
    Num Samples: 4901 (0.07%)
    Update Ratio: 34.40%

  Phase Local  Session Global
    Num Data Locations: 427 (0.43%)
    Num Samples: 20499 (0.28%)
    Update Ratio: 38.48%

  Phase Global Session Local 
    Num Data Locations: 2263 (2.28%)
    Num Samples: 44631 (0.62%)
    Update Ratio: 9.02%

  Phase Global Session Global
    Num Data Locations: 6977 (7.04%)
    Num Samples: 335976 (4.66%)
    Update Ratio: 20.58%


== Everything Else ==
  Num Data Locations: 97169 (98.00%)
  Num Samples: 6799737 (94.37%)

  Phase Local  Session Local 
    Num Data Locations: 68578 (69.17%)
    Num Samples: 3063560 (42.52%)
    Update Ratio: 48.48%

  Phase Local  Session Global
    Num Data Locations: 19351 (19.52%)
    Num Samples: 2233819 (31.00%)
    Update Ratio: 9.98%

  Phase Global Session Local 
    Num Data Locations: 2263 (2.28%)
    Num Samples: 47665 (0.66%)
    Update Ratio: 22.52%

  Phase Global Session Global
    Num Data Locations: 6977 (7.04%)
    Num Samples: 1454693 (20.19%)
    Update Ratio: 17.63%


Phase Characteristics: 1553,0.344011,4901/427,0.384799,20499 68578,0.484802,3063560/19351,0.099753,2233819 2263/6977:0,0.090162,0.205791,44631,335976/1,0.225197,0.176348,47665,1454693


> 
== Overall Stats ==
  Num Data Locations: 99149
  Num Samples: 7205744


== Phase __x64_sys_openat ==
  Num Data Locations: 18420 (18.58%)
  Num Samples: 802491 (11.14%)

  Phase Local  Session Local 
    Num Data Locations: 4638 (4.68%)
    Num Samples: 4680 (0.06%)
    Update Ratio: 55.21%

  Phase Local  Session Global
    Num Data Locations: 4073 (4.11%)
    Num Samples: 159244 (2.21%)
    Update Ratio: 36.42%

  Phase Global Session Local 
    Num Data Locations: 612 (0.62%)
    Num Samples: 2820 (0.04%)
    Update Ratio: 0.57%

  Phase Global Session Global
    Num Data Locations: 9097 (9.18%)
    Num Samples: 635747 (8.82%)
    Update Ratio: 20.01%


== Everything Else ==
  Num Data Locations: 90438 (91.21%)
  Num Samples: 6403253 (88.86%)

  Phase Local  Session Local 
    Num Data Locations: 67144 (67.72%)
    Num Samples: 3126837 (43.39%)
    Update Ratio: 47.76%

  Phase Local  Session Global
    Num Data Locations: 13585 (13.70%)
    Num Samples: 1800716 (24.99%)
    Update Ratio: 8.16%

  Phase Global Session Local 
    Num Data Locations: 612 (0.62%)
    Num Samples: 26420 (0.37%)
    Update Ratio: 20.97%

  Phase Global Session Global
    Num Data Locations: 9097 (9.18%)
    Num Samples: 1449280 (20.11%)
    Update Ratio: 15.47%


Phase Characteristics: 4638,0.552137,4680/4073,0.364183,159244 67144,0.477647,3126837/13585,0.081650,1800716 612/9097:0,0.005674,0.200109,2820,635747/1,0.209727,0.154663,26420,1449280


> 
== Overall Stats ==
  Num Data Locations: 99149
  Num Samples: 7205744


== Phase __x64_sys_read ==
  Num Data Locations: 13773 (13.89%)
  Num Samples: 255104 (3.54%)

  Phase Local  Session Local 
    Num Data Locations: 4106 (4.14%)
    Num Samples: 4439 (0.06%)
    Update Ratio: 21.87%

  Phase Local  Session Global
    Num Data Locations: 1426 (1.44%)
    Num Samples: 9711 (0.13%)
    Update Ratio: 15.95%

  Phase Global Session Local 
    Num Data Locations: 2392 (2.41%)
    Num Samples: 35446 (0.49%)
    Update Ratio: 19.71%

  Phase Global Session Global
    Num Data Locations: 5849 (5.90%)
    Num Samples: 205508 (2.85%)
    Update Ratio: 23.63%


== Everything Else ==
  Num Data Locations: 93617 (94.42%)
  Num Samples: 6950640 (96.46%)

  Phase Local  Session Local 
    Num Data Locations: 65896 (66.46%)
    Num Samples: 2962122 (41.11%)
    Update Ratio: 49.96%

  Phase Local  Session Global
    Num Data Locations: 19480 (19.65%)
    Num Samples: 2777039 (38.54%)
    Update Ratio: 11.00%

  Phase Global Session Local 
    Num Data Locations: 2392 (2.41%)
    Num Samples: 158750 (2.20%)
    Update Ratio: 8.73%

  Phase Global Session Global
    Num Data Locations: 5849 (5.90%)
    Num Samples: 1052729 (14.61%)
    Update Ratio: 19.08%


Phase Characteristics: 4106,0.218743,4439/1426,0.159510,9711 65896,0.499589,2962122/19480,0.109977,2777039 2392/5849:0,0.197089,0.236268,35446,205508/1,0.087326,0.190815,158750,1052729


> 
== Overall Stats ==
  Num Data Locations: 99149
  Num Samples: 7205744


== Phase __x64_sys_mmap ==
  Num Data Locations: 5765 (5.81%)
  Num Samples: 253908 (3.52%)

  Phase Local  Session Local 
    Num Data Locations: 749 (0.76%)
    Num Samples: 752 (0.01%)
    Update Ratio: 30.98%

  Phase Local  Session Global
    Num Data Locations: 193 (0.19%)
    Num Samples: 26066 (0.36%)
    Update Ratio: 7.78%

  Phase Global Session Local 
    Num Data Locations: 22 (0.02%)
    Num Samples: 22 (0.00%)
    Update Ratio: 13.64%

  Phase Global Session Global
    Num Data Locations: 4801 (4.84%)
    Num Samples: 227068 (3.15%)
    Update Ratio: 14.77%


== Everything Else ==
  Num Data Locations: 98207 (99.05%)
  Num Samples: 6951836 (96.48%)

  Phase Local  Session Local 
    Num Data Locations: 71623 (72.24%)
    Num Samples: 3159961 (43.85%)
    Update Ratio: 47.51%

  Phase Local  Session Global
    Num Data Locations: 21761 (21.95%)
    Num Samples: 2428716 (33.71%)
    Update Ratio: 12.20%

  Phase Global Session Local 
    Num Data Locations: 22 (0.02%)
    Num Samples: 22 (0.00%)
    Update Ratio: 22.73%

  Phase Global Session Global
    Num Data Locations: 4801 (4.84%)
    Num Samples: 1363137 (18.92%)
    Update Ratio: 16.46%


Phase Characteristics: 749,0.309840,752/193,0.077764,26066 71623,0.475140,3159961/21761,0.122046,2428716 22/4801:0,0.136364,0.147718,22,227068/1,0.227273,0.164625,22,1363137


> 
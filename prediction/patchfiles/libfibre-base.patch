diff --git a/src/libfibre/Cluster.h b/src/libfibre/Cluster.h
index 10a09af..d591400 100644
--- a/src/libfibre/Cluster.h
+++ b/src/libfibre/Cluster.h
@@ -198,17 +198,38 @@ public:
     return ringCount;
   }
 
+  size_t getGroupWorkersSysIDs(size_t g, pthread_t* tid = nullptr, size_t cnt = 0) {
+    RASSERT(g < workerGroups.size(), g, workerGroups.size());
+    for (size_t i = 0; i < cnt && i < workerGroups[g].size(); i += 1) {
+      getGroupWorkerSysID(g, i, &tid[i]);
+    }
+    return workerGroups[g].size();
+  }
+
+  size_t getGroupWorkerSysID(size_t g, size_t idx, pthread_t* tid = nullptr) {
+    RASSERT(g < workerGroups.size(), g, workerGroups.size());
+    if (idx < workerGroups[g].size()) {
+      if (tid) *tid = getGroupWorker(g, idx).sysThreadId;
+      return 1;
+    } else {
+      return 0;
+    }
+  }
+
   void formWorkerGroups(const std::vector<size_t>& groupSizes) {
     ScopedLock<WorkerLock> sl(ringLock);
     size_t cnt = ringCount;
     BaseProcessor* procGroupStart = placeProc;
     workerGroups.resize(groupSizes.size());
+    stats->workerGroups.resize(groupSizes.size());
     for (size_t g = 0; g < groupSizes.size(); g += 1) {
       RASSERT(cnt >= groupSizes[g], cnt, groupSizes[g]);
       BaseProcessor* proc = procGroupStart;
       workerGroups[g].resize(groupSizes[g]);
+      stats->workerGroups[g].resize(groupSizes[g]);
       for (size_t w = 0;;) {
         workerGroups[g][w] = reinterpret_cast<Worker*>(proc);
+        stats->workerGroups[g][w] = proc;
         if (++w >= groupSizes[g]) break;
         BaseProcessor* prev = proc;
         proc = ProcessorRingGlobal::next(*proc);
diff --git a/src/runtime/BaseProcessor.cc b/src/runtime/BaseProcessor.cc
index b376e8f..75d1990 100644
--- a/src/runtime/BaseProcessor.cc
+++ b/src/runtime/BaseProcessor.cc
@@ -41,6 +41,7 @@ inline Fred* BaseProcessor::tryLocal() {
 #if TESTING_LOADBALANCING
 inline Fred* BaseProcessor::tryStage() {
   Fred* f = scheduler.getStaging(_friend<BaseProcessor>()).readyQueue.tryDequeue();
+  stats->takeAttempt.count();
   if (f) {
     DBG::outl(DBG::Level::Scheduling, "tryStage: ", FmtHex(this), ' ', FmtHex(f));
     if (f->checkAffinity(*this, _friend<BaseProcessor>())) {
@@ -58,6 +59,8 @@ inline Fred* BaseProcessor::trySteal() {
   for (;;) {
     victim = local ? ProcessorRingLocal::next(*victim) : ProcessorRingGlobal::next(*victim);
     Fred* f = victim->readyQueue.tryDequeue();
+    stats->takeAttempt.count();
+    victim->readyQueue.countTakeAttempt();
     if (f) {
       DBG::outl(DBG::Level::Scheduling, "trySteal: ", FmtHex(this), "<-", FmtHex(victim), ' ', FmtHex(f));
       if (victim == this) {
diff --git a/src/runtime/BaseProcessor.h b/src/runtime/BaseProcessor.h
index 0a2af5a..3bd0575 100644
--- a/src/runtime/BaseProcessor.h
+++ b/src/runtime/BaseProcessor.h
@@ -48,6 +48,8 @@ class ReadyQueue {
 public:
   ReadyQueue(BaseProcessor& bp) { stats = new FredStats::ReadyQueueStats(this, &bp); }
 
+  void countTakeAttempt() { stats->takeAttempt.count(); }
+
   Fred* dequeue() {
 #if TESTING_LOADBALANCING
     ScopedLock<WorkerLock> sl(readyLock);
diff --git a/src/runtime/Stats.cc b/src/runtime/Stats.cc
index f873021..6f0b243 100644
--- a/src/runtime/Stats.cc
+++ b/src/runtime/Stats.cc
@@ -149,6 +149,13 @@ void ClusterStats::print(ostream& os) const {
   if (totalClusterStats && this != totalClusterStats) totalClusterStats->aggregate(*this);
   Base::print(os);
   os << " pause:" << pause;
+  for (const std::vector<cptr_t>& workerGroup : workerGroups) {
+    os << " (";
+    for (const cptr_t& worker : workerGroup) {
+      os << " " << FmtHex(worker);
+    }
+    os << " )";
+  }
 }
 
 void IdleManagerStats::print(ostream& os) const {
@@ -164,6 +171,7 @@ void ProcessorStats::print(ostream& os) const {
   os << " S:"  << start;
   os << " D:"  << deq;
   if (handover)     os << " H:"  << handover;
+  if (takeAttempt)  os << " TA:" << takeAttempt;
   if (borrowLocal)  os << " BL:" << borrowLocal;
   if (borrowStage)  os << " BS:" << borrowStage;
   if (borrowGlobal) os << " BG:" << borrowGlobal;
@@ -178,6 +186,7 @@ void ReadyQueueStats::print(ostream& os) const {
   if (totalReadyQueueStats && this != totalReadyQueueStats) totalReadyQueueStats->aggregate(*this);
   Base::print(os);
   os << queue;
+  os << " TA:" << takeAttempt;
 }
 
 #else
diff --git a/src/runtime/Stats.h b/src/runtime/Stats.h
index d11aee6..ddc42d0 100644
--- a/src/runtime/Stats.h
+++ b/src/runtime/Stats.h
@@ -19,6 +19,7 @@
 
 #include "runtime/Container.h"
 
+#include <vector>
 #include <ostream>
 using std::ostream;
 
@@ -319,6 +320,7 @@ struct TimerStats : public Base {
 
 struct ClusterStats : public Base {
   Counter pause;
+  std::vector<std::vector<cptr_t>> workerGroups;
   ClusterStats(cptr_t o, cptr_t p, const char* n = "Cluster     ") : Base(o, p, n, 2) {}
   void print(ostream& os) const;
   void aggregate(const ClusterStats& x) {
@@ -349,6 +351,7 @@ struct ProcessorStats : public Base {
   Counter start;
   Counter deq;
   Counter handover;
+  Counter takeAttempt;
   Counter borrowLocal;
   Counter borrowGlobal;
   Counter borrowStage;
@@ -364,6 +367,7 @@ struct ProcessorStats : public Base {
     start.aggregate(x.start);
     deq.aggregate(x.deq);
     handover.aggregate(x.handover);
+    takeAttempt.aggregate(x.takeAttempt);
     borrowLocal.aggregate(x.borrowLocal);
     borrowGlobal.aggregate(x.borrowGlobal);
     borrowStage.aggregate(x.borrowStage);
@@ -378,6 +382,7 @@ struct ProcessorStats : public Base {
     start.reset();
     deq.reset();
     handover.reset();
+    takeAttempt.reset();
     borrowLocal.reset();
     borrowGlobal.reset();
     borrowStage.reset();
@@ -390,13 +395,16 @@ struct ProcessorStats : public Base {
 };
 
 struct ReadyQueueStats : public Base {
+  Counter takeAttempt;
   Queue queue;
   ReadyQueueStats(cptr_t o, cptr_t p, const char* n = "ReadyQueue") : Base(o, p, n, 0) {}
   void print(ostream& os) const;
   void aggregate(const ReadyQueueStats& x) {
+    takeAttempt.aggregate(x.takeAttempt);
     queue.aggregate(x.queue);
   }
   virtual void reset() {
+    takeAttempt.reset();
     queue.reset();
   }
 };

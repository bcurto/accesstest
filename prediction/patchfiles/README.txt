Overview
--------
Apply patch file to directory specified in the filename.
Patchfiles with "base" in their name are always applied.
Patchfiles with "addon" in name are optionally applied on top of base patchfiles if you want to delegate the corresponding operations.

Syscalls:
 - open:  libfibre repo branch migrator     / patchfiles in migrate dir and migrate/open dir
 - write: libfibre repo branch migrating-io / patchfiles in migrate-io dir
 - read:  libfibre repo branch migrating-io / patchfiles in migrate-io dir
 - mmap:  libfibre repo branch migrator     / patchfiles in migrate dir and migrate/mmap dir



Build
-----
sudo apt install libnghttp2-dev libpcre3-dev libxml2-dev libexpat1-dev

git clone https://github.com/apache/httpd --depth 1 --branch 2.4.51  httpd
git clone https://github.com/apache/apr httpd/srclib/apr
cd httpd/srclib/apr; git checkout 09ecd1c; cd -
cd httpd

git clone https://git.uwaterloo.ca/bcurto/libfibre.git
cd libfibre
git checkout BRANCH  ## see overview for branch based on operation to delegate ##
patch -p1 < libfibre-base.patch
cd -

patch -p1 < httpd-base.patch
patch -p1 -d srclib/apr < apr-base.patch
PATCH  ## see overview for patch files to apply based on operation to delegate ##

INSTALLDIR=$HOME/tmp/apache
./configure2.sh libfibre  --with-mpm=worker --with-included-apr \
  --enable-http2 --with-libxml2=$(xml2-config --cflags|cut -c3-) \
  --prefix=$INSTALLDIR

make -j $(nproc)
make install

patch -p1 -d $INSTALLDIR/conf < apacheconf-base.patch

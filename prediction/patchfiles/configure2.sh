#!/bin/sh
[ -z "$1" ] && {
  echo "usage: $0 <path to libfibre>"
  exit 1
}

cd $1
PTLF=$(pwd)
cd -
shift

[ "$(uname)" = "Linux" ] && MAKE=make || MAKE=gmake
[ "$(uname)" = "Linux" ] && SED=sed || SED=gsed

$MAKE -j $(nproc) -C $PTLF lib

for cf in server/mpm/*/config3.m4; do
	$SED -i -e "s|AC_CHECK_FUNCS(pthread_kill)||" $cf
done

./buildconf

[ "$(uname)" = "Linux" ] &&  FLIBS="-lfibre" || FLIBS="-lfibre -lexecinfo"

LDFLAGS="-L$PTLF/src -Wl,-rpath=$PTLF/src" LIBS="$FLIBS" \
CFLAGS="-I$PTLF/src -DSIGPROCMASK_SETS_THREAD_MASK -g -O2" \
ac_cv_func_poll=no \
./configure $*

$SED -i -e "s|-levent ||" Makefile

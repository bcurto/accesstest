Scripts for validating preditions from Accessprof characterizations:
 - nomigrate.txt: To be run on configurations of Apache that don't perform delegation.
 - migrate.txt: To be run on configurations of Apache that perform delegation.
